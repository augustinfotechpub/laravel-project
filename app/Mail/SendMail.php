<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
/**
 * Class SendMail
 */
class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    public $data;

    /**
     * Create a new message instance.
     * @param string $view
     * @param string $subject
     * @param array $data
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        // $this->view = $view;
        // $this->subject = $subject;  
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build()
    {
       return $this->subject('subscription alert mail')->view('emails.subscription_alert');
    }
}
