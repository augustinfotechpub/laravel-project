<?php

namespace App\Listeners\Front;

use APP\Events\Front\CancelSubscriptionAlertNotificationEmail;
use App\Services\EmailService;
use Illuminate\Support\Facades\Mail;

class CancelSubscriptionAlertNotificationEmailHandler
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function handle(CancelSubscriptionAlertNotificationEmail $event)
    {
        $user = $event->data;
        $mail_params_array = [
            'to' => $user->email,
            'from' => env('MAIL_FROM_ADDRESS'),
            'from_name' => env('APP_NAME'),
        ];


        $content_var_values = [
            'APP_NAME' => env('APP_NAME'),
            'APP_URL' => env('APP_URL'),
            'USER_NAME' => $user->first_name,
            'USER_EMAIL' => $user->email,
            
        ];


        $this->emailService->sendEmail($mail_params_array,'user_cancel_subscription_survey',$content_var_values);

    }
}
