<?php

namespace App\Listeners\Front;

use APP\Events\Front\CancelSubscriptionAdminEmail;
use App\Services\EmailService;
use Illuminate\Support\Facades\Mail;

class CancelSubscriptionAdminEmailHandler
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function handle(CancelSubscriptionAdminEmail $event)
    {
        $admin = $event->data;
        $user_data = $event->userdata;

        $mail_params_array = [
            'to' => $admin->email,
            'from' => env('MAIL_FROM_ADDRESS'),
            'from_name' => env('APP_NAME'),
        ];


        $content_var_values = [
            'APP_NAME' => env('APP_NAME'),
            'APP_URL' => env('APP_URL'),
            'ADMIN_NAME' => $admin->first_name.' '.$admin->last_name,
            'ADMIN_EMAIL' => $admin->email,
            'USER_NAME' => $user_data->first_name,
            'USER_EMAIL' => $user_data->email,
        ];
        $this->emailService->sendEmail($mail_params_array,'admin_cancel_subscription_survey',$content_var_values);
    }
}
?>