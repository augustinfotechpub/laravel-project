<?php

namespace App\Listeners\Front;

use App\Events\Front\EmailVerification;
use App\Events\Front\WelcomeEmail;
use App\Services\EmailService;
use Illuminate\Support\Facades\Mail;

class WelcomeEmailHandler
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function handle(WelcomeEmail $event)
    {
        $user = $event->data['user'];
        $subscriptionData = $event->data['subscription'];

        $mail_params_array = [
            'to' => $user->email,
            'from' => env('MAIL_FROM_ADDRESS'),
            'from_name' => env('APP_NAME'),
        ];


        $content_var_values = [
            'APP_NAME' => env('APP_NAME'),
            'APP_URL' => env('APP_URL'),
            'USER_NAME' => $user->first_name,
            'USER_EMAIL' => $user->email,
            'SUBSCRIPTION_PLAN_TITLE'=>$subscriptionData['plan_title'],
            'SUBSCRIPTION_PLAN_PRICE'=>$subscriptionData['price'],
            'SUBSCRIPTION_PLAN_DURATION'=>$subscriptionData['duration'],
        ];


        $this->emailService->sendEmail($mail_params_array,'user_welcome_email',$content_var_values);

    }
}
