<?php

namespace App\Listeners\Front;

use APP\Events\Front\SubscriptionAlertEmail;
use App\Services\EmailService;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Models\CustomerSubscription;
use App\Models\User;


class SubscriptionAlertEmailHandler
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function handle(SubscriptionAlertEmail $event)
    {

        $user = $event->data;
        $subscriptionData = $event->data['subscription'];
        
        $mail_params_array = [
            'to' =>$user['email'],
            'from' => env('MAIL_FROM_ADDRESS'),
            'from_name' => env('APP_NAME'),
        ];


        $content_var_values = [
            'APP_NAME' => env('APP_NAME'),
            'APP_URL' => env('APP_URL'),
            'USER_NAME' => $user['first_name'],
            'USER_EMAIL' => $user['email'],
            'EXPIRED_DATE'=> $user['expired_date'],
        ];
        
        $this->emailService->sendEmail($mail_params_array,'user_subscription_alert',$content_var_values);
        
    }
}
