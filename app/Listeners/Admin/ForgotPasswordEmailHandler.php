<?php

namespace App\Listeners\Admin;

use App\Events\Admin\ForgotPasswordEmail;
use App\Interfaces\EmailServiceInterface;

class ForgotPasswordEmailHandler
{
    protected $emailService;
    public function __construct(EmailServiceInterface $emailService)
    {
        $this->emailService =$emailService;
    }

    public function handle(ForgotPasswordEmail $event)
    {

        $data = $event->data;

        $mail_params_array = [
            'to' => $data['email'],
            'from' => env('MAIL_FROM_ADDRESS'),
            'from_name' => env('APP_NAME'),
        ];

        $content_var_values = [
            'APP_NAME' => env('APP_NAME'),
            'APP_URL' => env('APP_URL'),
            'USER_NAME' => $data['name'],
            'USER_EMAIL' => $data['email'],
            'RESET_LINK' => $data['reset_link'],
        ];

        $this->emailService->sendEmail($mail_params_array,'admin_forgot_password',$content_var_values);
    }
}
