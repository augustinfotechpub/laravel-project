<?php


namespace App\Interfaces\Admin;


interface DefinitionItemsInterface
{
    public function store($definition_id, $request);

    public function update($request, $definition_id);

    public function delete($id);
}
