<?php


namespace App\Interfaces\Admin;


interface FinancialStatementEntriesInterface
{
    public function getByStatementId($id, $limit = 10);

    public function store(array $data);
}
