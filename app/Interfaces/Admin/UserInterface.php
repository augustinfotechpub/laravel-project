<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Interfaces\Admin;

use Illuminate\Http\Request;

interface UserInterface
{
    public function getAllAjax();

    public function getById($id);

    public function delete($id);

    public function getLatestUsers($limit);

    public function getActivateUsers();
}
