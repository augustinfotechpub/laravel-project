<?php


namespace App\Interfaces\Admin;


interface DefinitionItemTagsInterface
{
   public function store($definition_id,$definition_item_id,$request);
   public function update($request,$definition_id,$definition_item_id);
    public function delete($id);
}
