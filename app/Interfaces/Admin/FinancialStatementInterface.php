<?php


namespace App\Interfaces\Admin;


interface FinancialStatementInterface
{
    public function storeStatement(array $data);

    public function getPeriods();
}
