<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Interfaces\Admin;

use Illuminate\Http\Request;

interface SubscriptionPlanInterface
{
    public function getAll();
    public function getById($id);
    public function store(Request $request);
    public function update(Request $request,$id);
    public function delete($id);

}
