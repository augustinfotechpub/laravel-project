<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Interfaces\Admin;

use Illuminate\Http\Request;

interface SiteConfigurationInterface
{
    public function getAll();
    public function update(Request $request);
}
