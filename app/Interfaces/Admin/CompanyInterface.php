<?php


namespace App\Interfaces\Admin;


use Illuminate\Http\Request;

interface CompanyInterface
{
    public function getAllAjax(Request $request);

    public function delete($id);

    public function getById($id);

    public function update($id, Request $request);

    public function getAll();

    public function searchCompany($profile,$keyword);

    public function getCompaniesListAfterId($id);
}
