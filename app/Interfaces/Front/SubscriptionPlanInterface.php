<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Interfaces\Front;


interface SubscriptionPlanInterface
{
    public function getAll();

    public function getCurrentPlan($current_plan_id);

    public function getById($id);




}
