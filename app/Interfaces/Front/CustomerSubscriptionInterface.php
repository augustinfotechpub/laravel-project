<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Interfaces\Front;

use App\Models\SubscriptionPlan;
use Stripe\Subscription;

interface CustomerSubscriptionInterface
{
    public function store($request);

    public function getCustomerSubscription($user_id);

    public function getCustomerSubscriptionRequest(Subscription $requestData, SubscriptionPlan $plan, $user_id);

    public function makeSubscriptionSocialiteSignup($requestData);

    public function checkSubscription($id);
}
