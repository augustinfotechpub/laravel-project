<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Interfaces\Front;


interface UserInterface
{
    public function getById($id);

    public function store($requestData);

    public function update($requestData, $id);


}
