<?php

namespace App\Interfaces;

interface EmailServiceInterface
{
    public function sendEmail($mail_params_array, $template, $dynamic_data);
    public function sendMail($content_data, $to, $from, $from_name, $subject);
}
