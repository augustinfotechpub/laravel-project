<?php


namespace App\Traits;


trait FinancialsTrait
{
    public function getTimeSeries($array, $index = 'date')
    {
        return array_column($array, $index);
    }

    public function getDataSerieses($dataArray, $ignorableIndexes = [])
    {
        $dataArray = array_reverse($dataArray);
        if (isset($dataArray[0])) {
            $i = 0;
            foreach ($dataArray[0] as $index => $statement) {
                if (in_array($i, $ignorableIndexes)) {
                    $i++;
                    continue;
                }
                foreach ($dataArray as $arr) {
                    $array[$index][] = ['time' => $arr['date'], 'value' => chartNumberInMillion($arr[$index])];
                }
                $i++;
            }
        }
        return $array ?? [];
    }

    public function getIntervals(array $array, array $ignorableIndexes = [])
    {
        $i = 0;
        foreach ($array as $key => $value) {
            if (in_array($i, $ignorableIndexes)) {
                $i++;
                continue;
            }
            $arr[] = ucfirst(strUpperToSpace($key));
            $i++;
        }
        return $arr ?? [];
    }
}
