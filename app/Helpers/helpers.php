<?php

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Models\Setting;
use Illuminate\Support\Facades\Crypt;

if (!function_exists('formatDate')) {
    // function formatDate($date, $format = 'M d, Y h:i A')
    function formatDate($date, $format = 'm/d/Y')
    {
        try {
            if ($date != null) {
                return Carbon::parse($date)->format($format);
            }
            return null;
        } catch (\Exception $e) {
            return '--';
        }
    }
}

// Price Format front side
if (!function_exists('formatPrice')) {
    function formatPrice($price)
    {
        return $price == "" ? '$' . sprintf("%.2f", 0) : '$' . sprintf("%.2f", $price);
    }
}

if (!function_exists('cleanString')) {
    function cleanString($string)
    {
        return preg_replace("/[^0-9]/", "", $string);
    }
}


if (!function_exists('decimal')) {
    function decimal($value): float
    {
        return $value == "" ? sprintf("%.2f", 0) : sprintf("%.2f", $value);
    }
}

if (!function_exists('getMathDiv')) {
    function getMathDiv($numerator, $denominator): string
    {
        return '<div class="formula">
                    <div class="numerator">' . $numerator . '</div>
                    <br>
                    <div class="denominator">' . $denominator . '</div>
                </div>';
    }
}

//if (!function_exists('currencyFormat')) {
//    function currencyFormat(float $num): string
//    {
//        $negetive = '';
//        if ($num < 0) {
//            $num = ltrim($num, '-');
//            $negetive = '-';
//        }
//        $units = ['', 'K', 'M', 'B', 'T', 'QD', 'QT'];
//        for ($i = 0; $num >= 1000; $i++) {
//            $num /= 1000;
//        }
//        return $negetive . round($num, 1) . $units[$i];
//    }
//}


if (!function_exists('currencyInMillion')) {
    function currencyInMillion($num): string
    {
        $negetiveSign = '';
        if ($num < 0) {
            $num = ltrim($num, '-');
            $negetiveSign = '-';
        }

        if( $num == 0 ){
            return '--';    
        }

        $amount = (string)number_format(round(($num / 1000), 2), 0);

        if( $amount == '0' ){
            return '--';
        }

        return  $negetiveSign . $amount;
    }
}
if (!function_exists('numberInMillion')) {
    function numberInMillion($num): string
    {
        return number_format(round(($num / 1000), 2), 2) . 'M';
    }
}

if (!function_exists('currencyInTrillion')) {
    function currencyInTrillion($num): string
    {
        return number_format(round(($num / 1000000000000), 2), 1) . 'T';
    }
}
if (!function_exists('currencyInBillion')) {
    function currencyInBillion($num): string
    {
        return number_format(round(($num / 1000000000), 2), 2) . 'B';
    }
}

if (!function_exists('chartNumberInMillion')) {
    function chartNumberInMillion($num): string
    {
        $negetive = '';
        if ($num < 0) {
            $num = ltrim($num, '-');
            $negetive = '-';
        }
        return $negetive . round(( (int)$num / 1000000), 2);
    }
}

if (!function_exists('currencyFormat')) {
    function currencyFormat($num)
    {
        return '$' . number_format($num, 2, '.', ',');
    }
}


// Price Format front side
if (!function_exists('getRGBColor')) {
    function getRGBColor()
    {
        $hash = md5('color' . random_int(10, 20)); // modify 'color' to get a different palette
        return array(
            hexdec(substr($hash, 0, 2)), // r
            hexdec(substr($hash, 2, 2)), // g
            hexdec(substr($hash, 4, 2))); //b
    }
}

// Price Format front side
if (!function_exists('getRGBColorString')) {
    function getRGBColorString()
    {
        $hash = md5('color' . random_int(10, 20)); // modify 'color' to get a different palette
        $arr = array(
            hexdec(substr($hash, 0, 2)), // r
            hexdec(substr($hash, 2, 2)), // g
            hexdec(substr($hash, 4, 2))); //b
        return 'rgba(' . $arr[0] . ',' . $arr[1] . ',' . $arr[2] . ')';
    }
}

# Random Color Generator
if (!function_exists('getHaxColor')) {
    function getHaxColor()
    {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }
}

function xmlToArray($xml, $options = array())
{
    $defaults = array(
        'namespaceSeparator' => ':',//you may want this to be something other than a colon
        'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
        'alwaysArray' => array(),   //array of xml tag names which should always become arrays
        'autoArray' => true,        //only create arrays for tags which appear more than once
        'textContent' => '$',       //key used for the text content of elements
        'autoText' => true,         //skip textContent key if node has no attributes or child nodes
        'keySearch' => false,       //optional search and replace on tag and attribute names
        'keyReplace' => false       //replace values for above search values (as passed to str_replace())
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = null; //add base (empty) namespace

    //get attributes from all namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
            //replace characters in attribute name
            if ($options['keySearch']) $attributeName =
                str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
            $attributeKey = $options['attributePrefix']
                . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                . $attributeName;
            $attributesArray[$attributeKey] = (string)$attribute;
        }
    }

    //get child nodes from all namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->children($namespace) as $childXml) {
            //recurse into child nodes
            $childArray = xmlToArray($childXml, $options);
//            list($childTagName, $childProperties) = each($childArray);
            foreach ($childArray as $childTagName => $childProperties) {
                list($childTagName, $childProperties) = [$childTagName, $childProperties];
            }
            //replace characters in tag name
            if ($options['keySearch']) $childTagName =
                str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
            //add namespace prefix, if any
            if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

            if (!isset($tagsArray[$childTagName])) {
                //only entry with this key
                //test if tags of this type should always be arrays, no matter the element count
                $tagsArray[$childTagName] =
                    in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                        ? array($childProperties) : $childProperties;
            } elseif (
                is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                === range(0, count($tagsArray[$childTagName]) - 1)
            ) {
                //key already exists and is integer indexed array
                $tagsArray[$childTagName][] = $childProperties;
            } else {
                //key exists so convert to integer indexed array with previous value in position 0
                $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
            }
        }
    }

    //get text content of node
    $textContentArray = array();
    $plainText = trim((string)$xml);
    if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

    //stick it all together
    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
        ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

    //return node as array
    return array(
        $xml->getName() => $propertiesArray
    );
}

function get_string_between(string $string, string $start, string $end)
{
    $startIndex = strpos($string, $start);
    $withoutLeft = substr($string, $startIndex);
    return substr($withoutLeft, 0, strpos($withoutLeft, $end)) . $end;
}

function getFillingFileUrl(): string
{
    return "storage/fillings/";
}

function getFillingFilePath(): string
{
    return "app/public/fillings/";
}

function getUnderscoreConjoined($text)
{
    $string = strtolower($text);
    return str_replace(' ', '_', $string);
}

function strUpperToSpace($string)
{
    return preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
}

/*
|--------------------------------------------------------------------------
| Excel To Array
|--------------------------------------------------------------------------
| Helper function to convert excel sheet to key value array
| Input: path to excel file, set wether excel first row are headers
| Dependencies: PHPExcel.php include needed
*/
function excelToArray($filePath, $header = true)
{
    //Create excel reader after determining the file type
    $inputFileName = $filePath;
    /**  Identify the type of $inputFileName  **/
    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
    /**  Create a new Reader of the type that has been identified  **/
    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    /** Set read type to read cell data onl **/
    $objReader->setReadDataOnly(true);
    /**  Load $inputFileName to a PHPExcel Object  **/
    $objPHPExcel = $objReader->load($inputFileName);
    //Get worksheet and built array with first row as header
    $objWorksheets = $objPHPExcel->getAllSheets();
    //excel with first row header, use header as key
    if ($header) {

        foreach ($objWorksheets as $objWorksheet) {
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $headingsArray = $objWorksheet->rangeToArray('A1:' . $highestColumn . '1', null, true, true, true);
            $headingsArray = $headingsArray[1];

            $r = -1;
            $namedDataArray = array();
            $sheets = [];
            for ($row = 2; $row <= $highestRow; ++$row) {
                $dataRow = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true, true);
                if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                    ++$r;
                    foreach ($headingsArray as $columnKey => $columnHeading) {
                        $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                    }
                }
            }
            $sheets[] = $namedDataArray;
            unset($namedDataArray);
        }
        dd($sheets ?? []);
    } else {
        //excel sheet with no header
        $namedDataArray = $objWorksheets->toArray(null, true, true, true);
    }

    return $namedDataArray;
}


/**
 * Function that groups an array of associative arrays by some key.
 *
 * @param {String} $key Property to sort by.
 * @param {Array} $data Array that stores multiple associative arrays.
 * @return array
 */
function group_by($key, $data)
{
    $result = array();

    foreach ($data as $val) {
        if (array_key_exists($key, $val)) {
            $result[$val[$key]][] = $val;
        } else {
            $result[""][] = $val;
        }
    }

    return $result;
}

function currentRouteName()
{
    return \Illuminate\Support\Facades\Route::currentRouteName();
}

function getProfileSymbol()
{
    try {
        $profile = session('profile');
        return $profile['exchangeShortName'] . ':' . $profile['symbol'];
    } catch (Exception $e) {
        getProfileSymbol();
    }
}

function moneyFormat($amount)
{
    $formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
    return $formatter->formatCurrency($amount, 'USD');
}

function amountRatio($amount): string
{

    if(empty($amount) || ($amount == '0') || ($amount == '0.00') || ($amount == '0%') ){
        return "--";
    }
    return $amount;
}

function amount($amount): string
{

    if(empty($amount) || ($amount == '0') ){
        return "--";
    }

    return "<span style='color:" . ($amount < 0 ? 'red' : 'green') . "'>" . currencyInMillion($amount) . "</span>";
}

function formatAmount($amount, $zero = false): string
{
    if ($zero == true && ($amount == 0 || empty($amount))) {
        $amount = 0;
        return "<span style='color:" . ($amount < 0 ? 'red' : 'green') . "'>" . number_format($amount,2,'.','') . "</span>";
    }
    if( !empty($amount) && ($amount != '0') ){
    return "<span style='color:" . ($amount < 0 ? 'red' : 'green') . "'>" . number_format($amount,2,'.','') . "</span>";
    } else{
        return '--';
    }
}

function percentage($numbers): string
{
    if( empty($numbers) ){
        return '--';
    }
    $returnNumber = number_format((float)$numbers, 2) . '%';

    if( $returnNumber == '0.00%' || empty($returnNumber) ){
        return '--';
    }

    return $returnNumber;
}

function greenColor(int $gradient): string
{
    switch ($gradient) {
        case 1:
            return "#eafaea"; // light
        case 2:
            return "#d2f7d2"; // average
        case 3:
            return "#a7f5a7"; // dark
    }
}

function filterUniqueKey($string): string
{
    $string = str_replace('&nbsp;', '', $string);
    $string = strtolower($string);
    return Str::slug($string);
}
const KEYS_FOR_NEXT_BLANK_ROW = [
    'income-statement' => [
        'interestIncomeFromOthers',
        'generalAndAdministrativeExpenses',
        'interestOnOtherBorrowedFunds',
        'lessReinsuranceCeded',
        'lessReinsuranceRecoverable',
        'DividendsPaidToPolicyholders',
        'reinsuranceIncomeOrExpense',
        'incomeTaxExpense',
        'netIncome',
        'epsdiluted',
    ],

    ///// For Balance Sheet
    'balance-sheet' => [
        'policyLoans',
        'moneyMarketInvestments',
        'propertyPlantEquipmentNet',
        'otherNonCurrentAssets',
        'otherSubtractionsFromLoans',
        'accruedInterestReceivable',
        'municipalBondsAndGovernmentSecurities',
        'TotalAccumulatedDepreciationAndDepletion',
        'netPropertyPlantAndEquipment',
        'reinsurancePayable',
        'totalDebt',
    ],

    ////// For Cashflow statements
    'cash-flow' => [
        'otherWorkingCapital',
        'accountsPayables',
        'otherFinancingActivites',
        'cashAtBeginningOfPeriod',
    ],
];


function getAlertId($identifier, $midFix = ""): string
{
    return $identifier . $midFix . '_' . Str::random(10);
}

function getCurrentTime(){
    $mytime = Carbon::now();
    return $mytime->toDateTimeString();
}

function getExpiryStatus(){

    $currentDate = Carbon::now();
    $specifiedDate = date("Y-m-d H:i:s", strtotime(config("app.BetaExpiry")));
    $setting = Setting::where('key', 'beta_expiry')->first();

    if(($currentDate < $specifiedDate) && @$setting->value == '1' ){
        return true;
    }

    return false;
}

if( !function_exists('createMailEnc') ){
    function createMailEnc($mail = null){

        if( is_null($mail) || empty($mail) ){
            return false;
        }

        try {
            return Crypt::encryptString($mail);
        } catch (Exception $e) {
            return null;
        }

        return null;
        
    }
}

if( !function_exists('decryptMailEnc') ){
    function decryptMailEnc($mail = null){

        if( is_null($mail) || empty($mail) ){
            return false;
        }

        try {
            return Crypt::decryptString($mail);   
        } catch (Exception $e) {
            return null;
        }

        return null;

    }
}