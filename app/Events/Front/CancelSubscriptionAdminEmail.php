<?php

namespace App\Events\Front;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CancelSubscriptionAdminEmail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    public $userdata;

    public function __construct($data , $userdata)
    {
        $this->data = $data;
        $this->userdata = $userdata;
    }
}
