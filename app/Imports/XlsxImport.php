<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use App\Models\DefinitionItem;
use App\Models\Definition;
use App\Models\DefinitionItemSic;
use App\Models\DefinitionItemSymbol;
use App\Models\DefinitionItemTag;
use Illuminate\Support\Facades\DB;


class XlsxImport implements ToCollection, WithHeadingRow, SkipsEmptyRows
{

    private $rowData = [], $lastDefinationID = null, $parentActive = 0, $lastDefinationItemID = null;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {   
        $this->rowData = $collection->toArray();

        if( !array_key_exists('title', $this->rowData[0]) || !array_key_exists('sub_title', $this->rowData[0]) || !array_key_exists('content', $this->rowData[0]) || !array_key_exists('type', $this->rowData[0]) || !array_key_exists('sic_codes', $this->rowData[0]) || !array_key_exists('tickers', $this->rowData[0]) || !array_key_exists('keywords', $this->rowData[0]) ){
            return redirect()->route('admin.alerts.list')->withErrors(['Invalid file']);
        }   

        foreach ($this->rowData as $rowKey => $rowValue) {
            if( !empty($rowValue['title']) ) {
                $titleexists = Definition::where('title', $rowValue['title'])->first();
                if (!$titleexists) {
                    $this->lastDefinationID = Definition::query()->create([
                        'title' => $rowValue['title']
                    ]);
                } else {
                    $this->lastDefinationID = $titleexists;
                }
                $this->parentActive = 1;
            }

            $this->storeDefinationItem($rowValue);
            $this->storeDefinationItemSymbol($rowValue);
            $this->storeDefinationItemTag($rowValue);
            $this->storeDefinationItemSic($rowValue);

        }
        return true;

    }

    private function storeDefinationItem($rowValue){

        $contentRemoveChat = ['"', "'"];
        $removeKeywordsChar = ["‘", "’"];
        
        $content = trim($rowValue['content']);
        $content = trim($content, '"');
        $content = trim($content, "'");
        $content = trim($content, "‘");
        $content = trim($content, "’");
        
        if( !empty($this->lastDefinationID->id) && $this->parentActive == 1 ){
            $this->lastDefinationItemID = DefinitionItem::create([
                'definition_id' => $this->lastDefinationID->id,
                'sub_title' => $rowValue['sub_title'],
                'type' =>  ($rowValue['type'] ? strtolower( str_replace(' ', '_', $rowValue['type'])) : null ),
                'content' => $content
            ]);
        }
    }

    private function storeDefinationItemSic($rowValue){
        // sleep(1);
        $sicRow = trim($rowValue['sic_codes']);

        $arraySicRow = explode(',', $sicRow);
        $sicSQLArr = [];
        $currentTime = date('Y-m-d H:i:s');


        if( isset($arraySicRow[0]) && !empty(trim($arraySicRow[0])) ) {

            foreach ($arraySicRow as $value) {
                //if (strpos($value, '-') == false) {
                if (preg_match('/.+-.+/', $value) == 1) {
                    $explodedSic = explode('-', $value);
                    $startIndex = trim($explodedSic[0]);
                    $endIndex = trim($explodedSic[1]);
                    $i = 0;
                    while($startIndex <= $endIndex) {

                        $i++;
                        $sicSQLArr[] = [
                            'definition_id' => $this->lastDefinationID->id,
                            'definition_item_id' => $this->lastDefinationItemID->id,
                            'sic' => trim($startIndex),
                            'created_at' => $currentTime,
                            'updated_at' => $currentTime
                        ];
                        if( $i >= 99) {
                            $this->saveDefItemSic($sicSQLArr);
                            $sicSQLArr = [];
                            $i = 0;
                        }

                        $startIndex++;
                    }
                    
                    if( !empty($sicSQLArr) ){
                        $this->saveDefItemSic($sicSQLArr);
                    }
                   /* DefinitionItemSic::create([
                        'definition_id' => $this->lastDefinationID->id,
                        'definition_item_id' => $this->lastDefinationItemID->id,
                        'sic' => trim($value),
                    ]);*/
                }
                else {
                    $sicSQLArr = [];
                    $sicSQLArr[] = [
                        'definition_id' => $this->lastDefinationID->id,
                        'definition_item_id' => $this->lastDefinationItemID->id,
                        'sic' => trim($value),
                        'created_at' => $currentTime,
                        'updated_at' => $currentTime
                    ];

                    $this->saveDefItemSic($sicSQLArr);

                    /*$explodedSic = explode('-', $value);
                    for ($i=$explodedSic[0]; $i <= $explodedSic[1] ; $i++) { 
                        DefinitionItemSic::create([
                            'definition_id' => $this->lastDefinationID->id,
                            'definition_item_id' => $this->lastDefinationItemID->id,
                            'sic' => $i,
                        ]);
                    }*/
                }

            }
            
            return true;
        }
    }


    private function saveDefItemSic($arr){
        try {
            //DB::enableQueryLog();
            $saveData = DB::table('definition_item_sics')->insert($arr);
            //dd(DB::getQueryLog());

            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }

    private function storeDefinationItemSymbol($rowValue){

        $symbol = $rowValue['tickers'];
        if( empty($symbol) ){
            return false;
        }

        if (strpos($symbol, ',') == false) {
            DefinitionItemSymbol::create([
                'definition_id' => $this->lastDefinationID->id,
                'definition_item_id' => $this->lastDefinationItemID->id,
                'symbol' => trim($symbol),
            ]);
        } else {

            $symbolArray = explode(',', $symbol);

            foreach ($symbolArray as $key => $value) {
                DefinitionItemSymbol::create([
                    'definition_id' => $this->lastDefinationID->id,
                    'definition_item_id' => $this->lastDefinationItemID->id,
                    'symbol' => trim($value),
                ]);
            }
        }
    }

    private function storeDefinationItemTag($rowValue){
        $keywords = $rowValue['keywords'];

        $removeKeywordsChar = ['"', "'", "‘", "’"];
        $removeKeywordsWords = ['and', 'or'];

        if( empty($keywords) ){
            return false;
        }

        if (strpos($keywords, ',') == false) {

            if( trim($keywords) == 'keywords' ){
                if( !empty($rowValue['title']) ){
                    DefinitionItemTag::create([
                        'definition_id' => $this->lastDefinationID->id,
                        'definition_item_id' => $this->lastDefinationItemID->id,
                        'keyword' => trim($rowValue['title']),
                    ]);
                }
            } else {
                DefinitionItemTag::create([
                    'definition_id' => $this->lastDefinationID->id,
                    'definition_item_id' => $this->lastDefinationItemID->id,
                    'keyword' => trim($keywords),
                ]);
            }

            
        } else {

            $keywordArray = str_getcsv($keywords);
            $titleInserted = true;
            foreach ($keywordArray as $key => $value) {

                $value = str_replace($removeKeywordsChar, '', $value);
                // $value = $this->str_replace_first('and', '', $value);
                // $value = $this->str_replace_first('or', '', $value);
                // $value = str_replace('or ', '', $value);
                $value = preg_replace( "/\r|\n/", "", $value );

                if( trim($value) == 'keywords' ){
                    if( !empty($rowValue['title']) && !in_array($rowValue['title'], $keywordArray) && $titleInserted ){
                        DefinitionItemTag::create([
                            'definition_id' => $this->lastDefinationID->id,
                            'definition_item_id' => $this->lastDefinationItemID->id,
                            'keyword' => trim($rowValue['title']),
                        ]);
                        
                        $titleInserted = false;
                    }
                }
                    DefinitionItemTag::create([
                        'definition_id' => $this->lastDefinationID->id,
                        'definition_item_id' => $this->lastDefinationItemID->id,
                        'keyword' => trim($value),
                    ]);
            }
        }
    }

    public function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $content, 1);
    }
}
