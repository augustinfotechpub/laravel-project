<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\BetaExpiry::class,
        Commands\SubscriptionAlertNotify::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $schedule->command('sync:financial-statements --all')->sundays()->at('1:00');
        $schedule->command('sync:ownerships')->thursdays()->at('1:00');
        $schedule->command('beta:update')->everyFiveMinutes();
        $schedule->command('sync:cik-keywords')->everySixHours();
        $schedule->command('subscription:alert_email')->at('1:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
