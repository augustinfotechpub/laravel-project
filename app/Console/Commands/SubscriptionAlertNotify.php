<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Carbon;
use App\Mail\MarkdownMail;
use App\Models\CustomerSubscription;
use Mail;
use Illuminate\Support\Facades\DB;

class SubscriptionAlertNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:alert_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription Expire Alert Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $email;

    public function __construct()
    {
       parent::__construct();
       //$this->email = $email;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        try {
                $currentDate = date("Y-m-d H:i:s");
               // $specifiedDate = date("Y-m-d H:i:s", strtotime(config("app.SubscriptionAlertNotify")));

                $mytime = Carbon::now();
                $users = User::where('id', 'email')->first();
                echo $users;
                
                if($currentDate == $users){

                    $where = ['end_date'=>'2022-06-03'];

                    $userData =  User::where($where)->get()->toArray();

                    $data = [];
                    foreach ($userData as $key => $value) {
                        $this->info(' Send mail to '. $value['email']);
                        
                        $data['USER_NAME'] = $value['first_name'] . ' ' . $value['last_name'];

                        $data['encrypt_tokne'] = createMailEnc($value['email']);
                        Mail::to($value['email'])->send(new MarkdownMail('emails.subscription_alert', $data));
                    }

                    if($user){
                        $this->info('The command was successful Executed!');
                    }
                }
        } catch (\Throwable $th) {
            $this->error('Something went wrong!');
        }
    }
}
