<?php

namespace App\Console\Commands;

use App\Models\MapperCikName;
use App\Services\FinancialModelingPrep;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncCikNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:cik-names';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Cik And names from FMP and seed in database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('');
        $json = file_get_contents("https://financialmodelingprep.com/api/v3/cik_list?apikey=46be2cf4b2f0ab3f96c95d8c4ce689d0");
        $array = json_decode($json);
        $this->info('|===============================================|');
        $this->info('| Syncing Started. it will took minute a while  |');
        $this->info('|===============================================|');
        $bar = $this->output->createProgressBar(count($array));
        $bar->start();
        foreach ($array as $cikName) {
            DB::table('mapper_cik_names')->insertOrIgnore([
                'cik' => $cikName->cik,
                'name' => $cikName->name
            ]);
            $bar->advance();
        }
        $bar->finish();
        $this->info('CIK names Successfully Added');
        return 0;
    }
}
