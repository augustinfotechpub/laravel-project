<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Definition;
use App\Models\DefinitionItemTag;
use App\Models\DefinitionItemTagMap;
use App\Models\DefinitionItem;
use App\Models\Symbol;
use App\Models\DefinitionItemSymbol;
use App\Models\DefinitionItemSic;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;


class getKeywords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:keywords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get keywords and symbol.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
    */
    public function handle()
    {
        $this->performScript();
        //$this->getCompanyInFindDesc('personal computers');
        //die
        // $getcompany = $this->getCompanyCikOrTicker();

      //  $data = $this->updateOrCreateTickerOrCik('1029','367','563','1711014','BRK-B2');
       // print_r($data);
        //die;
    }
    /*
    * Keyword listing funtion 
    *
    */
    protected function performScript()
    {
        $get_keywork_cnt = Setting::where('key', '=', 'keyword_sync_by_ticker')->first('value');
        if(empty($get_keywork_cnt))
        {
            $get_keywork_cnt = 0;
        }else{
            $get_keywork_cnt = $get_keywork_cnt->value;
        }
        $limit = 1000;
        $getallkeywords = $this->getAllKeywordList($get_keywork_cnt,100);
        echo "Current batch no. = ".$get_keywork_cnt ."<br>";
        if( isset($getallkeywords) )
        {
            foreach($getallkeywords as $keywords_data )
            {
                $get_company_list = $this->getCompanyInFindDesc( $keywords_data->keyword );
                if( is_array($get_company_list) && !empty($get_company_list) )
                {
                    foreach( $get_company_list as $company_data )
                    {
                        echo "Defination ID : $keywords_data->definition_id <br>";
                        $this->updateOrCreateTickerOrCik($keywords_data->tag_id,$keywords_data->definition_id,$keywords_data->definition_item_id,$company_data->cik,$company_data->symbol);
                    }                    
                }
            }
            $get_keywork_cnt = $get_keywork_cnt + $limit;
            $setting = Setting::firstOrCreate(['key' => 'keyword_sync_by_ticker']);
            $setting->update(['value' => $get_keywork_cnt]);
        }
        echo "Done";
        die;
    }

    protected function getAllKeywordList($offset,$limit){

        $getKeywordList = DB::table('definition_item_tags')
        ->join('definition_items','definition_item_tags.definition_id','=','definition_items.definition_id')
        ->select('definition_item_tags.id AS tag_id','definition_item_tags.keyword','definition_item_tags.definition_id','definition_item_tags.definition_item_id')
        ->skip($offset)->take($limit)
        ->orderBy('tag_id')
        ->get()
        ->toArray();
        return $getKeywordList;
    }
    protected function getCompanyCikOrTicker(){
        $getCompanyCik = DB::table('definition_item_tag_maps')
        ->join('definition_items','definition_item_tag_maps.definition_id','=','definition_items.definition_id')
        ->select('cik','ticker')->first();
        return $getCompanyCik;

    }
    protected function getCompanyInFindDesc( $keyword )
    {
        $companyDetails = DB::table("companies as c")
        ->join("symbols as s", function($join){
                $join->on("c.id", "=", "s.company_id");
        })
        ->select("c.id","c.sic", "c.cik", "c.name", "s.symbol")
        ->orWhere('c.description',"like","%$keyword%")
        ->orWhere('c.sic_description',"like","%$keyword%")
        ->orWhere('c.eod_description',"like","%$keyword%")
        ->get()->toArray();

        return $companyDetails;
    }
    protected function updateOrCreateTickerOrCik($definition_item_tags_id,$definition_id,$definition_item_id,$cik,$ticker){

        $getTicker = [
            'definition_id' => $definition_id,
            'definition_item_id' => $definition_item_id,
            'symbol' => $ticker,
        ];
        DefinitionItemSymbol::updateOrCreate($getTicker,$getTicker);
        // $getMapId = DB::table('definition_item_tag_maps')
        // ->where('definition_item_tags_id','=',$definition_item_tags_id)
        // ->where('definition_id','=',$definition_id)
        // ->where('definition_item_id','=',$definition_item_id)
        // ->where('cik','=',$cik)
        // ->orWhere('ticker','=',"$ticker")
        // ->select('*')
        // ->first();

        // $setTicker = [
        //     'definition_item_tags_id' => $definition_item_tags_id,
        //     'definition_id' => $definition_id,
        //     'definition_item_id' =>$definition_item_id,
        //     'cik' => $cik, 
        //     'ticker' => $ticker,
        // ];
        // $getTicker = array();
        // if(isset($getMapId->id) && isset($getMapId->id))
        // {
        //     $setTicker['id'] = $getTicker['id'] = $getMapId->id;
        // }
        // definition_item_symbols
        // print_r($getMapId);
        // die;
        
        // DefinitionItemTagMap::query()->updateOrCreate($getTicker,$setTicker);
    }
}