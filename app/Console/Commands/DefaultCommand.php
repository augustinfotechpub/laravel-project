<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;

class DefaultCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trim-companies-cik';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $companies = Company::query()->whereRaw('LENGTH(cik) = 10')->get();
        $this->newLine();
        $this->info('10 digits cik number companies data syncing started.');
        $this->newLine();
        $bar = $this->output->createProgressBar($companies->count());
        $bar->start();
        foreach ($companies as $company) {
            try {
                $company->update(['cik' => ltrim($company->cik, 0)]);
            } catch (\Throwable $e) {
                $this->error($e->getMessage(), $e->getLine());
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('');
        $this->info('Companies CIK trimming done');
        return 0;
    }
}
