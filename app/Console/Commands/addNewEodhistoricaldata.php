<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Company;
use App\Models\Exchange;
use App\Models\Symbol;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class addNewEodhistoricaldata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:Eodhistoricaldata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get companies data like sic name sortName etc.';

    protected $fmp;
    protected $company_id;
    protected $secEdgarService;

    /**
     * Create a new command instance.
     *
     * @param FinancialModelingPrep $fmp
     */
    public function __construct(FinancialModelingPrep $fmp, SecEdgarService $secEdgarService)
    {
        $this->fmp = $fmp;
        $this->secEdgarService = $secEdgarService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->newLine();
         $companiesJson = $this->secEdgarService->httpRequest("https://eodhistoricaldata.com/api/fundamentals/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX");
         $companiesArray = json_decode($companiesJson, true);
         echo "<pre>";
         print_r($companiesArray);
         die;
         
        foreach($companiesArray['General'] as $key =>$value)
        {
            $newcompaniesArray=[
                'category'=>$value['HomeCategory'],

            ];
            print_r($newcompaniesArray);

        //     print_r($key);
        //     echo ':';
        //     print_r($value);
        //     echo '<br>';
         }
//         print_r($companiesArray);
         die();

         foreach ($companiesArray as $key => $value)
        { 
           $newcompaniesArray=[

               'category'=>$value['HomeCategory'],
               'fiscal_year_end'=>$value['FiscalYearEnd'],
               'date_first_added'=>$value['IPODate'],
                ];
                
              //die();                   
            }
                 $company = $this->companyUpdate($newcompaniesArray); // Update or Create Company Profile
                 
}
             protected function companyUpdate($companiesArray)
             {
                 Company::query()->update([
                     'cik' => ltrim($companiesArray['cik']),

                 ], [
                    // 'name' => $companiesArray['name'],
                     //'ticker' => $companiesArray['ticker'],
                     //'cik' => ltrim($companiesArray['cik']),
                    //  'entity_type' => $companiesArray['entityType'],
                    //  'sic' => $companiesArray['sic'],
                    //  'sic_description' => $companiesArray['sicDescription'],
                    //  'ein' => $companiesArray['ein'],
                    //  'description' => $companiesArray['description'],
                    //  'website' => $companiesArray['website'],
                    //  'investor_website' => $companiesArray['investorWebsite'],
                        'category' => $companiesArray['category'] ?? null,
                       'fiscal_year_end' => $companiesArray['fiscal_year_end'],
                    //  'mailing_address_id' => $companiesArray[0] ?? null,
                    //  'business_address_id' => $companiesArray[1] ?? null,
                    //  'phone' => $companiesArray['phone'] ?? null,
                        'date_first_added' => $companiesArray['date_first_added'] ?? null,
                 ]);
                 return Company::query()->where('cik', ltrim($companiesArray['cik']))->get();
             }
         
}