<?php

namespace App\Console\Commands;

use App\Interfaces\Admin\CompanyInterface;
use App\Models\Company;
use App\Models\Ownership;
use App\Models\OwnershipInterest;
use App\Models\AnalystRatings;
use App\Models\Funds;
use App\Services\JswService;
use App\Services\SecEdgarService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncOwnerships extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:ownerships {--company-id= : Company id} {--from-company-id= : Start from company id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Companies Ownerships details.';
    protected $jswService, $companyRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CompanyInterface $companyRepository)
    {
        parent::__construct();
        $this->jswService = new JswService();
        $this->companyRepository = $companyRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('|===============================================|');
        $this->info('|          Ownership Syncing Started.           |');
        $this->info('|===============================================|');

        $fromCompanyId = $this->option('from-company-id');
        $companyId = $this->option('company-id');
        if (!is_null($fromCompanyId)) {
            $companies = Company::query()->where('id', '>=', $fromCompanyId)->get();
        } elseif (!is_null($companyId)) {
            $companies = Company::query()->where('id', $companyId)->get();
        } else {
            $companies = Company::query()->get();
        }
        $bar = $this->output->createProgressBar($companies->count());
        $bar->start();
        foreach ($companies as $company) {
            try {
                $companyCik = ltrim($company->cik, 0);
                $companyTicker=$company->tickers;
                $ownerships = $this->jswService->getEodOwnership($companyTicker[0]);
                $this->storeOwnershipHolder($ownerships->original["Holder"],$companyCik,$companyTicker[0]);
                $this->storeOwnershipOfficer($ownerships->original["Officers"],$companyCik,$companyTicker[0]);
                $this->storeOwnershipAnalystRatings($ownerships->original["AnalystRatings"],$companyCik,$companyTicker[0]);
                $this->storeOwnershipFunds($ownerships->original["Funds"],$companyCik,$companyTicker[0]);
            } catch (\Exception $e) {
                $this->error($e->getMessage(), $e->getLine());
            }
            $bar->advance();
        }
        $bar->finish();
        $this->newLine();
        $this->info('Ownership Syncing Done.');
        return 0;
    }

    protected function storeOwnershipHolder($ownerships,$cik,$ticker)
    {
        if($ownerships!="")
        {
            DB::table('ownershipinterest')->where('company_cik',$cik)->delete();
        }
        foreach ($ownerships as $ownership) {
            try {
                OwnershipInterest::updateOrCreate([
                    'company_cik'=>$cik,
                    'company_ticker'=>$ticker,
                    'holder_name' => $ownership["name"],
                    'holder_date' => $ownership["date"],
                    'totalShares' => $ownership["totalShares"],
                    'totalAssets' => $ownership["totalAssets"],
                    'currentShares' => $ownership["currentShares"],
                    'change' => $ownership["change"],
                    'change_p' => $ownership["change_p"],
                ]); 
            }
             catch (\Exception $e) {   
                $this->error($e->getMessage(), $e->getLine());
            }
        }
    }

    protected function storeOwnershipOfficer($ownerships,$cik,$ticker)
    {
        if($ownerships!="")
        {
            DB::table('ownerships')->where('companyCik',$cik)->delete();
        }
        foreach ($ownerships as $ownership) {
            try {
                Ownership::updateOrCreate([
                    'company_ticker'=>$ticker,
                    'name' => $ownership["Name"],
                    'position' => $ownership["Title"],
                    'yearBorn' => $ownership["YearBorn"],
                    'type'=>"officer",
                    'companyCik'=>$cik,
                ]);
            } catch (\Exception $e) {
                $this->error($e->getMessage(), $e->getLine());
            }
        }
    }

    protected function storeOwnershipAnalystRatings($ownerships,$cik,$ticker)
    {
        if($ownerships!="")
        {
            DB::table('analystratings')->where('company_cik',$cik)->delete();
        }
            try {
                AnalystRatings::updateOrCreate([
                    'company_cik'=>$cik,
                    'company_ticker'=>$ticker,
                    'Rating' => $ownerships["Rating"],
                    'TargetPrice' => $ownerships["TargetPrice"],
                    'StrongBuy' => $ownerships["StrongBuy"],
                    'Buy' => $ownerships["Buy"],
                    'Hold' => $ownerships["Hold"],
                    'Sell' => $ownerships["Sell"],
                    'StrongSell' => $ownerships["StrongSell"],
                ]); 
            }
             catch (\Exception $e) {   
                $this->error($e->getMessage(), $e->getLine());
            }
    }

    protected function storeOwnershipFunds($ownerships,$cik,$ticker)
    {
        if($ownerships!="")
        {
            DB::table('funds')->where('company_cik',$cik)->delete();
        }
        foreach ($ownerships as $ownership) {
            try {
                Funds::updateOrCreate([
                    'company_cik'=>$cik,
                    'company_ticker'=>$ticker,
                    'holder_name' => $ownership["name"],
                    'date' => $ownership["date"],
                    'totalShares' => $ownership["totalShares"],
                    'totalAssets' => $ownership["totalAssets"],
                    'currentShares' => $ownership["currentShares"],
                    'change' => $ownership["change"],
                    'change_p' => $ownership["change_p"],
                ]); 
            }
             catch (\Exception $e) {   
                $this->error($e->getMessage(), $e->getLine());
            }
        }
    }
}
