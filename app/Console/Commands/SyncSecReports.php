<?php

namespace App\Console\Commands;

use App\Interfaces\Admin\CompanyInterface;
use App\Interfaces\Admin\FinancialStatementInterface;
use App\Models\BalanceSheetStatement;
use App\Models\CashFlowStatement;
use App\Models\Company;
use App\Models\ComprehensiveIncomeStatement;
use App\Models\IncomeStatement;
use App\Models\ShareholdersEquityStatement;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncSecReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:sec-reports {--all} {--company-id= : Company id} {--from-company-id= : Start from company id} {--cik10digits= : cik10Digits only}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all existing companies financial statements.';

    protected $companyRepository;
    protected $fmp, $secEdgarService;
    protected $financialStatementRepository;
    protected $ticker, $period;
    protected $company_id, $cik;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        CompanyInterface $companyRepository,
        FinancialModelingPrep $fmp,
        FinancialStatementInterface $financialStatementRepository,
        SecEdgarService $secEdgarService
    )
    {
        parent::__construct();
        $this->companyRepository = $companyRepository;
        $this->fmp = $fmp;
        $this->financialStatementRepository = $financialStatementRepository;
        $this->secEdgarService = $secEdgarService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Throwable
     */
    public function handle(): int
    {
        $this->info('');
        $fromCompanyId = $this->option('from-company-id');
        $companyId = $this->option('company-id');
        $cik10digits = $this->option('cik10digits');
        if (!is_null($fromCompanyId)) {
            $companies = Company::query()->where('id', '>=', $fromCompanyId)->get();
        } elseif (!is_null($companyId)) {
            $companies = Company::query()->where('id', $companyId)->get();
        } elseif (!is_null($cik10digits)) {
            $companies = Company::query()->whereRaw('LENGTH(cik) = 10')->get();
            $this->newLine();
            $this->info('10 digits cik number companies data syncing started.');
            $this->newLine();
        } else {
            $companies = Company::query()->get();
        }
        $periods = ['FY' => 'Fiscal Year', 'quarter' => 'Quarter'];

        $this->info('|===============================================|');
        $this->info('|      Syncing Started financial statements.    |');
        $this->info('|===============================================|');
        $bar = $this->output->createProgressBar($companies->count());
        $bar->start();
        foreach ($companies as $company) {
            try {
                $this->company_id = $company->id;
                ComprehensiveIncomeStatement::query()->where('company_id', $this->company_id)->delete();
                ShareholdersEquityStatement::query()->where('company_id', $this->company_id)->delete();
                $this->ticker = $company->tickers[0];
                $this->cik = ltrim($company->cik, 0);
                foreach (array_keys($periods) as $period) {
                    $this->period = $period;
                    $this->storeComprehensiveIncome();
                    $this->storeShareholdersEquity();
                }
            } catch (\Throwable $e) {
                $this->error($e->getMessage(), $e->getLine());
            }
            $bar->advance();
        }
        $bar->finish();
        $this->info('');
        $this->info('Companies Successfully Added');
        return 0;
    }

    protected function storeComprehensiveIncome()
    {
        $reports = $this->get10KQReports('comprehensive');
        foreach ($reports as $report) {
            ComprehensiveIncomeStatement::query()->create([
                'company_id' => $this->company_id,
                'symbol' => $this->ticker,
                'period' => $this->period,
                'title' => $report['title'],
                'html' => $report['html']
            ]);
        }
    }

    protected function storeShareholdersEquity()
    {
        $reports = $this->get10KQReports('shareholder');
        foreach ($reports as $report) {
            ShareholdersEquityStatement::query()->create([
                'company_id' => $this->company_id,
                'symbol' => $this->ticker,
                'period' => $this->period,
                'title' => $report['title'],
                'html' => $report['html']
            ]);
        }
    }

    protected function get10KQReports($searchString): array
    {
        $formType = $this->period == 'FY' ? '10-K' : '10-Q';
        $reports = $this->secEdgarService->getReports($this->cik, [$searchString], $formType);
        // Attempt to get Data from 20-F or 40-F Form type filing if entity type is foreign operating.
        if (empty($reports)) {
            $reports = $this->secEdgarService->getReports($this->cik, [$searchString], '20-F');
            if (empty($reports))
                $reports = $this->secEdgarService->getReports($this->cik, [$searchString], '40-F');
        }
        return $reports;
    }
}
