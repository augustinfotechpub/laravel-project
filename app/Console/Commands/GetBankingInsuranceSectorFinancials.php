<?php

namespace App\Console\Commands;

use App\Models\BalanceSheetStatement;
use App\Models\CashFlowStatement;
use App\Models\Company;
use App\Models\IncomeStatement;
use App\Models\Sic;
use App\Services\SecEdgarService;
use App\Services\ZacksService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GetBankingInsuranceSectorFinancials extends Command
{

    public $zacksService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-bank-insurance-financials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->zacksService = new ZacksService();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $companies = Company::query()->whereHas('symbols')->whereIn('sic', Sic::BANKING_INSURANCE_SICS)->get();
        $this->line('|===============================================|');
        $this->line('| Syncing Started. It will took many hours      |');
        $this->line('|===============================================|');

        $bar = $this->output->createProgressBar($companies->count());
        $bar->start();

        foreach ($companies as $company) {
            $symbol = $company->symbols->first()->symbol;
            $this->storeIncomeStatements($symbol);
            $this->storeBalanceSheetStatements($symbol);
            $this->storeCashFlowStatements($symbol);
            $bar->advance();
        }
        $bar->finish();
        $this->info('Statements Successfully Updated');
        return 0;
    }

    protected function storeIncomeStatements($symbol)
    {
        $rows = $this->zacksService->getFinancials($symbol, 'A', 'incomeStatement');
        foreach ($rows as $date => $row) {
            try {
                $year = date('Y', strtotime($date));
                IncomeStatement::query()->whereYear('date', $year)->where(['period' => 'FY', 'symbol' => $symbol])->update($row);
            } catch (\Exception $e) {
                $this->error("Symbol.: " . $symbol);
                $this->error("Line No.: " . $e->getLine());
                $this->error("Error Message: " . $e->getMessage());
                $this->info("Income Statement API Response: ");
                $this->error(json_encode($row));
                $this->error("Year :" . ($year ?? '--'));
            }
        }
    }

    protected function storeBalanceSheetStatements($symbol)
    {
        $rows = $this->zacksService->getFinancials($symbol, 'A', 'balanceSheet');
        foreach ($rows as $date => $row) {
            try {
                $year = date('Y', strtotime($date));
                BalanceSheetStatement::query()->whereYear('date', $year)->where(['period' => 'FY', 'symbol' => $symbol])->update($row);
            } catch (\Exception $e) {
                $this->error("Symbol.: " . $symbol);
                $this->error("Line No.: " . $e->getLine());
                $this->error("Error Message: " . $e->getMessage());
                $this->info("Balance Sheet API Response: ");
                $this->error(json_encode($row));
                $this->error("Year :" . ($year ?? '--'));
            }
        }
    }

    protected function storeCashFlowStatements($symbol)
    {
        $rows = $this->zacksService->getFinancials($symbol, 'A', 'cashFlow');
        foreach ($rows as $date => $row) {
            try {
                $year = date('Y', strtotime($date));
                CashFlowStatement::query()->whereYear('date', $year)->where(['period' => 'FY', 'symbol' => $symbol])->update($row);
            } catch (\Exception $e) {
                dd($e->getMessage(), $row);
                $this->error("Cash Flow Symbol.: " . $symbol);
                $this->error("Line No.: " . $e->getLine());
                $this->error("Error Message: " . $e->getMessage());
                $this->info("Cash Flow API Response: ");
                $this->error(json_encode($row));
                $this->error("Year :" . ($year ?? '--'));
            }
        }
    }
}
