<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Company;
use App\Models\Exchange;
use App\Models\Symbol;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class addNewCompanies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:newcompanies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get companies data like sic name sortName etc.';

    protected $fmp;
    protected $company_id;
    protected $secEdgarService;

    /**
     * Create a new command instance.
     *
     * @param FinancialModelingPrep $fmp
     */
    public function __construct(FinancialModelingPrep $fmp, SecEdgarService $secEdgarService)
    {
        $this->fmp = $fmp;
        $this->secEdgarService = $secEdgarService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->newLine();
        $companiesJson = $this->secEdgarService->httpRequest("https://www.sec.gov/files/company_tickers.json");
        $companiesArray = json_decode($companiesJson, true);

         foreach ($companiesArray as $key => $value)
          {
             $newcompaniesArray=[
                 'cik'=>$value['cik_str'],
                 'ticker'=>$value['ticker'],
                 'name'=>$value['title'],
                  ];
                //  die();
                $company = $this->companyUpdateOrCreate($newcompaniesArray); // Update or Create Company Profile

                 }

       }
             protected function companyUpdateOrCreate($companiesArray)
             {
                 Company::query()->updateOrCreate([
                     'cik' => ltrim($companiesArray['cik']),
                 ], [
                     'name' => $companiesArray['name'],
                     'ticker' => $companiesArray['ticker'],
                     'cik' => ltrim($companiesArray['cik']),
                    //  'entity_type' => $companiesArray['entityType'],
                    //  'sic' => $companiesArray['sic'],
                    //  'sic_description' => $companiesArray['sicDescription'],
                    //  'ein' => $companiesArray['ein'],
                    //  'description' => $companiesArray['description'],
                    //  'website' => $companiesArray['website'],
                    //  'investor_website' => $companiesArray['investorWebsite'],
                      //  'category' => $companiesArray['category'] ?? null,
                      // 'fiscal_year_end' => $companiesArray['fiscalYearEnd'],
                    //  'mailing_address_id' => $companiesArray[0] ?? null,
                    //  'business_address_id' => $companiesArray[1] ?? null,
                    //  'phone' => $companiesArray['phone'] ?? null,
                      //  'date_first_added' => $companiesArray['firstAdded'] ?? null,
                 ]);
                 return Company::query()->where('cik', ltrim($companiesArray['cik']))->get();
             }
         
}