<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Carbon;
use App\Models\Setting;
use App\Mail\MarkdownMail;
use Mail;

class BetaExpiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'beta:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To Expire the beta users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
                $currentDate = date("Y-m-d H:i:s");
                $specifiedDate = date("Y-m-d H:i:s", strtotime(config("app.BetaExpiry")));

                $mytime = Carbon::now();
                $setting = Setting::where('key', 'beta_expiry')->first();
                
                if($currentDate > $specifiedDate && @$setting->value == '1'){

                    $where = ['is_beta'=>'1'];

                    $userData =  User::where($where)->get()->toArray();

                    $data = [];
                    foreach ($userData as $key => $value) {
                        $this->info(' Send mail to '. $value['email']);
                        
                        $data['USER_NAME'] = $value['first_name'] . ' ' . $value['last_name'];

                        $data['encrypt_tokne'] = createMailEnc($value['email']);
                        Mail::to($value['email'])->send(new MarkdownMail('emails.beta_expiry_email', 'Beta expired | Fnnfrastructure', $data));
                    }

                    
                    $user =  User::where($where)->where('email', '!=','jamall@clairvoyantcapital.com')->update(['is_beta' => '0', 'is_active' => '0']);

                    $setting->value = 0;
                    $setting->save();

                    if($user){
                        $this->info('The command was successful Executed!');
                    }
                }
        } catch (\Throwable $th) {
            $this->error('Something went wrong!');
        }
    }
}
