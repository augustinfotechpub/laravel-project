<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\DefinitionItemTag;
use App\Models\DefinitionItemTagMap;
use Illuminate\Console\Command;

class SyncKeywords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:cik-keywords {--alert-id= : Alert id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Cik And keywords from FMP and seed in database.';

//    private $valuesToCheck =array('definition_item_tags_id','definition_id','definition_item_id','cik');
//    private $valuesToUpdate =array('ticker');
    // private $token = config('services.edgar_sec_api.api_key');

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('|===============================================|');
        $this->info('|          Keyword Syncing Started.             |');
        $this->info('|===============================================|');

        $alertId = $this->option('alert-id');

        if (!is_null($alertId)) {
            $definitionKeywords = DefinitionItemTag::select('id','definition_id','definition_item_id','keyword')->whereNull('is_synced')->where('definition_id', $alertId)->get()->toArray();
        } else {
            $definitionKeywords = DefinitionItemTag::select('id','definition_id','definition_item_id','keyword')->whereNull('is_synced')->get()->toArray();
        }

        if( empty($definitionKeywords) ){
            $this->info('No keywords to sync .. exit');
            return false;
        }

        $dataToSave = array();
        $bar = $this->output->createProgressBar(count($definitionKeywords));
        $bar->start();
//        foreach ($definitionKeywords as $definitionKeyword){
            foreach ($definitionKeywords as $keyword){
                $res = $this->curlRequestKeywordSearch($keyword['keyword']);
                $resDecoded = json_decode($res,true);
                $out = array();

                if ( !isset($resDecoded['filings']) || empty(@$resDecoded['filings'])) {
                    continue;
                }

                foreach ($resDecoded['filings'] as $row) {
                    $out[$row['cik']] = $row;
                }
                $uniqueValues = array_values($out);
                foreach ($uniqueValues as $value){
                    $makeData = array(
                        'definition_item_tags_id'=>$keyword['id'],
                        'definition_id'=>$keyword['definition_id'],
                        'definition_item_id'=>$keyword['definition_item_id'],
                        'cik'=>$value['cik'],
                        'ticker'=>$value['ticker'],
                    );
                    $dataToSave[]= $makeData;
                }
                $bar->advance();
            }
//        }
        $this->newLine();
        $this->info('Updating DB...');
        $this->saveData($dataToSave);
        $bar->finish();
        $this->newLine();
        $this->info('Keyword Syncing Done.');
        return 0;
    }

    /**
     * @return bool|string
     */
    private function curlRequestKeywordSearch($keyword){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sec-api.io/full-text-search?token='.config('services.edgar_sec_api.api_key'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "query": "'.$keyword.'"
                }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    /**
     * @param $data
     */
    private function saveData($data){
        // DefinitionItemTagMap::truncate();
        // DefinitionItemTagMap::insert($data);

        foreach ($data as $ke => $val) {
            DefinitionItemTagMap::query()->updateOrCreate(['cik' => $val['cik'], 'definition_item_id' => $val['definition_item_id'], 'definition_id' => $val['definition_id'], 'definition_item_tags_id' => $val['definition_item_tags_id'] ], $val); 
        }

        foreach ($data as $key => $value) {
            $DefinitionItemTag = DefinitionItemTag::where('id', $value['definition_item_tags_id'])->first();
            $DefinitionItemTag->is_synced = '1';
            $DefinitionItemTag->save();
        }

    }

}
