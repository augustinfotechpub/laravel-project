<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Company;
use App\Models\Exchange;
use App\Models\Symbol;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class addCompanyDescription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:newcompaniesdescription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get companies data like description website phone etc.';

    protected $fmp;
    protected $company_id;
    protected $secEdgarService;

    /**
     * Create a new command instance.
     *
     * @param FinancialModelingPrep $fmp
     */
    public function __construct(FinancialModelingPrep $fmp, SecEdgarService $secEdgarService)
    {
        $this->fmp = $fmp;
        $this->secEdgarService = $secEdgarService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->newLine();
        $companiesJson = $this->secEdgarService->httpRequest("https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=46be2cf4b2f0ab3f96c95d8c4ce689d0");
        $companiesArray = json_decode($companiesJson, true);
        // echo '<pre>';
        // print_r ($companiesArray);
     
        // foreach ($companiesArray as $comdesc)
        // {
        //     $newcompaniesArray = [
        //     $companiesArray['description'] = $comdesc['description'],
        //     $companiesArray['website'] = $comdesc['website'],
        //     $companiesArray['phone'] = $comdesc['phone'],
        //     ];
        //     echo '<pre>';
        //     print_r ($newcompaniesArray);
        //     die();
        //     $company = $this->companyUpdate($newcompaniesArray);
        //     //die();

        // }
        foreach ($companiesArray as $key => $value)
        {
            // echo 12;
            // die();
            $newcompaniesArray=[
                'description'=>$value['description'],
                'website'=>$value['website'],
                'phone'=>$value['phone'],
                ];
                echo '<pre>'; 
                print_r ($newcompaniesArray);
                die();
                //$company = $this->companyUpdate($newcompaniesArray); // Update or Create Company Profile
        }
        die;
    }
        protected function companyUpdate($companiesArray)
        {
            Company::query()->update([
                'cik' => ltrim($companiesArray['cik']),
                // 'description' => ltrim($companiesArray['description']),
                // 'website' => ltrim($companiesArray['website']),
                // 'phone' => ltrim($companiesArray['phone']),
            ],
            [
                    // 'name' => $companiesArray['name'],
                    // 'ticker' => $companiesArray['ticker'],
                    // 'cik' => ltrim($companiesArray['cik'], 0),
                    // 'entity_type' => $companiesArray['entityType'],
                    // 'sic' => $companiesArray['sic'],
                    // 'sic_description' => $companiesArray['sicDescription'],
                    // 'ein' => $companiesArray['ein'],
                    'description' => $companiesArray['description'],
                    'website' => $companiesArray['website'],
                    // 'investor_website' => $companiesArray['investorWebsite'],
                    // 'category' => $companiesArray['category'] ?? null,
                    // 'fiscal_year_end' => $companiesArray['fiscalYearEnd'],
                    // 'mailing_address_id' => $companiesArray[0] ?? null,
                    // 'business_address_id' => $companiesArray[1] ?? null,
                    'phone' => $companiesArray['phone'] ?? null,
                    // 'date_first_added' => $companiesArray['firstAdded'] ?? null,
            ]);
          //  return Company::query()->where('cik', ltrim($companiesArray['cik']))->get();
        }     
}