<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Company;
use App\Models\Exchange;
use App\Models\Symbol;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class syncCompanies2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync2:companies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get companies data like sic name sortName etc.';

    protected $fmp;
    protected $company_id;
    protected $secEdgarService;

    /**
     * Create a new command instance.
     *
     * @param FinancialModelingPrep $fmp
     */
    public function __construct(FinancialModelingPrep $fmp, SecEdgarService $secEdgarService)
    {
        $this->fmp = $fmp;
        $this->secEdgarService = $secEdgarService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->newLine();
        $companiesJson = $this->secEdgarService->httpRequest("https://www.sec.gov/files/company_tickers.json");
        $companiesArray = json_decode($companiesJson, true);
        foreach($companiesArray as $comdata)
        {   
            $profileArray = array();
            $profileArray['cik'] = $comdata['cik_str'];
            $profileArray['name'] = $comdata['title'];
            $profileArray['ticker'] = $comdata['ticker'];
            $profileArray['description'] = $profileArray['website'] = $profileArray['phone'] = $profileArray['category'] = $profileArray['fiscalYearEnd'] = $profileArray['date_first_added']  = $profileArray['entityType'] = $profileArray['ein'] = $profileArray['sic'] = $profileArray['sicDescription'] = '';
            $profileArray['investorWebsite'] = $profileArray['mailing_address_id']  = $profileArray['business_address_id'] = '';
            $company = $this->companyUpdatebyticker($profileArray); // Update or Create Company Profile
        }
        

        foreach($companiesArray as $comdata)
        {
            
            $profileArray = array();
            $profileArray['cik'] = $comdata['cik_str'];
            $profileArray['name'] = $comdata['title'];
            $profileArray['ticker'] = $comdata['ticker'];

            $profileArray['description'] = $profileArray['website'] = $profileArray['phone'] = $profileArray['category'] = $profileArray['fiscalYearEnd'] = $profileArray['date_first_added']  = $profileArray['entityType'] = $profileArray['ein'] = $profileArray['sic'] = $profileArray['sicDescription'] = $profileArray['financial_description'] = $profileArray['eod_description']= '';

            //$companiesJson = $this->secEdgarService->httpRequest("https://financialmodelingprep.com/api/v3/profile/".$comdata['ticker']."?apikey=46be2cf4b2f0ab3f96c95d8c4ce689d0");
            $companiesJson = $this->secEdgarService->httpRequest("https://financialmodelingprep.com/api/v3/profile/".$comdata['ticker']."?apikey=7c7f7727399cca3486e121267cfa6b0a");
            $get_fin_api_data = json_decode($companiesJson, true);
            if( isset($get_fin_api_data) && isset($get_fin_api_data[0]) && !empty($get_fin_api_data[0]) )
            {
                $profileArray['description'] = $get_fin_api_data[0]['description'];
                $profileArray['website'] = $get_fin_api_data[0]['website'];
                $profileArray['phone'] = $get_fin_api_data[0]['phone'];
            }
            $companiesJson = $this->secEdgarService->httpRequest("https://eodhistoricaldata.com/api/fundamentals/".$comdata['ticker'].".US?api_token=60ec6a9aeff734.69550837");
            $get_eod_api_data = json_decode($companiesJson, true);

            // if( isset($get_eod_api_data['General']) && !empty($get_eod_api_data['General']) )
            // {
            //     $this->company_id = Company::query()->where('cik','=',($get_eod_api_data['General']['CIK']))->first();
               
            //     $profileArray['eod_description'] = $get_eod_api_data['General']['Description'];
            //     $profileArray['category'] = $get_eod_api_data['General']['HomeCategory'];
            //     $profileArray['fiscalYearEnd'] = $get_eod_api_data['General']['FiscalYearEnd'];
            //     $profileArray['date_first_added'] = $get_eod_api_data['General']['IPODate'];
            //     $profileArray['entityType'] = isset($get_eod_api_data['General']['Type']) ? $get_eod_api_data['General']['Type'] : '';
            //     $profileArray['ein'] = $get_eod_api_data['General']['EmployerIdNumber'];
                
            //    $profileArray['addresses']['company_id'] = $this->company_id;
            //     $profileArray['addresses']['street1'] = $get_eod_api_data['General']['AddressData']['Street']; 
            //     // $profileArray['addresses']['street2']
            //     $profileArray['addresses']['city'] = $get_eod_api_data['General']['AddressData']['City']; 
            //     $profileArray['addresses']['zip_code'] = $get_eod_api_data['General']['AddressData']['ZIP']; 
            //     //$profileArray['addresses']['state_or_country'] = $get_eod_api_data['General']['AddressData']['State'];
            //     $profileArray['addresses']['state_or_country_description'] = $get_eod_api_data['General']['AddressData']['Country'];
            //     $profileArray['Listings'] = $get_eod_api_data['General']['Listings'];
            //}
            //die;
            //$companiesJson = $this->secEdgarService->httpRequest("https://api.sec-api.io/mapping/cik/".$comdata['cik_str']."?token=ded44d839efe06e326469c0051685f188b50c2fffadb5254392644d66e55e13c");
            //$companiesJson = $this->secEdgarService->httpRequest("https://api.sec-api.io/mapping/cik/".$comdata['cik_str']."?token=307dd1c292d46bff9eea570e1f972d607380037f73bbdd48f03b9cc334aab549");
            $companiesJson = $this->secEdgarService->httpRequest("https://api.sec-api.io/mapping/cik/".$comdata['cik_str']."?token=".config('services.edgar_sec_api.api_key'));
            $get_sec_api_data = json_decode($companiesJson, true);
            if(isset($get_sec_api_data['status']) && $get_sec_api_data['status'] == 429)
            {
                break;
            }
            if( isset($get_sec_api_data) && isset($get_sec_api_data[0]) && !empty($get_sec_api_data[0]) )
            {
                $profileArray['sic'] = $get_sec_api_data[0]['sic'];
                $profileArray['sicDescription'] = $get_sec_api_data[0]['sicIndustry'];
            }
            $profileArray['investorWebsite'] = '';
            $profileArray['mailing_address_id'] = '';
            $profileArray['business_address_id'] = '';
            $this->storeAddresses($profileArray); // Update OR Create Company Addresses
         //   $this->updateOrCreateExchangeAndSymbols($profileArray);  // Update or Create exchange symbol
            $this->companyUpdateOrCreate($profileArray); // Update or Create Company Profile
        }
       die;
        // print_r($companiesArray);
        $this->line('|===============================================|');
        $this->line('| Syncing Started. It will took minute a while  |');
        $this->line('|===============================================|');
        $bar = $this->output->createProgressBar(count (array($companiesArray)));
    
        $bar->start();
        $chunk_company = array_chunk($companiesArray,9);
        $setting = Setting::where('key', 'sync_company_counter')->first();
        // $setting->value = 2;
        // $setting->save();
        // die;
        // echo "<pre>";
        // print_r($chunk_company);
        // die;
       if ( isset($chunk_company[$setting->value]) && is_array($chunk_company[$setting->value]) )
       {    
            foreach ($chunk_company[$setting->value] as $company) {
                try {
                    DB::beginTransaction();
                    $cik = ltrim($company['cik_str']);
                    $cik = sprintf("%010d", (int)$cik);
                    // echo "https://data.sec.gov/submissions/CIK$cik.json";
                    // die;
                    $profileJson = $this->secEdgarService->httpRequest("https://data.sec.gov/submissions/CIK$cik.json");
                    // print_r($profileJson);
                    // die;
                    
                    if (is_null($profileJson))
                        continue;
                    $profileArray = json_decode($profileJson, true);
                    if (!isset($profileArray['cik']))
                        continue;
                    unset($profileArray['filings']);
                    $company = $this->companyUpdateOrCreate($profileArray); // Update or Create Company Profile

                    if( !isset($company->id) || empty($company->id) ){
                        continue;
                    }

                    $this->company_id = $company->id;
                    $this->storeAddresses($profileArray); // Update OR Create Company Addresses
                    $this->updateOrCreateExchangeAndSymbols($profileArray);  // Update or Create Exchange And Symbols
                    DB::commit();
                } catch (\Throwable $e) {
                    DB::rollBack();
                    $this->error($e->getMessage() . " in Line Number: " . $e->getLine());
                    // $this->error($profileArray ?? []);
                    continue;
                }
            }
            $bar->advance();
        }
        $bar->finish();
        echo $this->info('Companies Successfully Added');
        return 0;
    }

    protected function updateOrCreateExchangeAndSymbols($profileArray): void
    {
        if( isset($profileArray['Listings']) && is_array($profileArray['Listings']) )
        {
            foreach ($profileArray['Listings'] as $key => $exchange_data) {
                if (is_null($exchange_data))
                    continue;
                Exchange::query()->updateOrCreate(['short_name' => $exchange_data['Code']], ['short_name' => $exchange_data['Code'], 'full_name' => $exchange_data['Name']]);  // Update or Create Exchange
                $exchange = Exchange::query()->where('short_name', $exchange_data['Code'])->first(); // GetExchange Details
                $symbolData = [
                    'company_id' => $this->company_id,
                    'company_cik' => $profileArray['cik'],
                    'exchange_id' => $exchange->id,
                    'symbol' => $exchange_data['Code'],

                ];
                Symbol::query()->updateOrCreate($symbolData, $symbolData);
            }
        }   
    }


    protected function storeAddresses($profileArray): void
    {
        try {
            foreach ($profileArray['addresses'] as $type => $address) {
                $addressArr = [
                    'company_id' => $this->company_id,
                    'street1' => $address['street1'],
                    'street2' => $address['street2'],
                    'city' => $address['city'],
                    'zip_code' => $address['zipCode'],
                    'type' => $type,
                    'state_or_country' => $address['stateOrCountry'],
                    'state_or_country_description' => $address['stateOrCountryDescription'],
                ];
            try {
                Address::query()->updateOrCreate($profileArray['addresses'], $profileArray['addresses']);
            } catch (\Exception $e) 
            {

            }
        }
        } catch (\Exception $e) {

        }
    }

    protected function companyUpdateOrCreate($profileArray)
    {
        Company::query()->updateOrCreate([
            'cik' => ltrim($profileArray['cik'], 0),
        ], [
            'name' => $profileArray['name'],
            'ticker' => $profileArray['ticker'],
            'cik' => ltrim($profileArray['cik'], 0),
            'entity_type' => $profileArray['entityType'],
            'sic' => $profileArray['sic'],
            'sic_description' => $profileArray['sicDescription'],
            'ein' => $profileArray['ein'],
            'description' => $profileArray['description'],
            'website' => $profileArray['website'],
            'investor_website' => $profileArray['investorWebsite'],
            'category' => $profileArray['category'] ?? null,
            'fiscal_year_end' => $profileArray['fiscalYearEnd'],
            'mailing_address_id' => $addresses[0] ?? null,
            'business_address_id' => $addresses[1] ?? null,
            'phone' => $profileArray['phone'] ?? null,
            'date_first_added' => $profileArray['firstAdded'] ?? null,
            'eod_description' => $profileArray['eod_description'],
        ]);
        return Company::query()->where('cik', ltrim($profileArray['cik']))->first();
    }

    protected function companyUpdatebyticker($profileArray)
    {
        Company::query()->updateOrCreate([
            'ticker' => $profileArray['ticker']
        ], [
            'name' => $profileArray['name'],
            'ticker' => $profileArray['ticker'],
            'cik' => ltrim($profileArray['cik'], 0),
            'entity_type' => $profileArray['entityType'],
            'sic' => $profileArray['sic'],
            'sic_description' => $profileArray['sicDescription'],
            'ein' => $profileArray['ein'],
            'description' => $profileArray['description'],
            'website' => $profileArray['website'],
            'investor_website' => $profileArray['investorWebsite'],
            'category' => $profileArray['category'] ?? null,
            'fiscal_year_end' => $profileArray['fiscalYearEnd'],
            'mailing_address_id' => $addresses[0] ?? null,
            'business_address_id' => $addresses[1] ?? null,
            'phone' => $profileArray['phone'] ?? null,
            'date_first_added' => $profileArray['firstAdded'] ?? null,
        ]);
        return Company::query()->where('cik', ltrim($profileArray['cik']))->first();
    }
}
