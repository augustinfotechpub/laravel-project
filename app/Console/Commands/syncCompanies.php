<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Company;
use App\Models\Exchange;
use App\Models\Symbol;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Doctrine\DBAL\Schema\Schema;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;
use Ixudra\Curl\Facades\Curl;

class syncCompanies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:companies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get companies data like sic name sortName etc.';

    protected $fmp;
    protected $company_id;
    protected $secEdgarService;

    /**
     * Create a new command instance.
     *
     * @param FinancialModelingPrep $fmp
     */
    public function __construct(FinancialModelingPrep $fmp, SecEdgarService $secEdgarService)
    {
        $this->fmp = $fmp;
        $this->secEdgarService = $secEdgarService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //$get_setting_company = Setting::where('key', '=', 'sync_company')->first();

        $this->newLine();
        $companiesJson = $this->secEdgarService->httpRequest("https://www.sec.gov/files/company_tickers.json");
        $companiesArray = json_decode($companiesJson, true);

        $get_setting_company = Setting::where('key', '=', 'sync_company')->first();
        if( isset($companiesArray) && !empty($companiesArray) && !empty($get_setting_company) )
        {
            $get_count = count($companiesArray);
            $update_count = 0;
            foreach($companiesArray as $comdata)
            {
                $profileArray = array();
                $profileArray['cik'] = $comdata['cik_str'];
                $profileArray['name'] = $comdata['title'];
                $profileArray['ticker'] = $comdata['ticker'];


                $profileArray['description'] = $profileArray['website'] = $profileArray['phone'] = $profileArray['category'] = $profileArray['fiscalYearEnd'] = $profileArray['date_first_added']  = $profileArray['entityType'] = $profileArray['ein'] = $profileArray['sic'] = $profileArray['sicDescription'] = '';
                $profileArray['investorWebsite'] = $profileArray['mailing_address_id']  = $profileArray['business_address_id'] = '';
                $company = $this->companyUpdateOrCreate($profileArray); // Update or Create Company Profile
                if(isset($company) && isset($company->id) )
                {
                    $this->company_id = $company->id;
                    $this->updateOrCreateExchangeAndSymbols($profileArray);
                }
                $update_count++;
            }

            if($get_count == $update_count)
            {
                $setting = Setting::firstOrCreate(['key' => 'sync_company']);
                $setting->update(['value' => 'success']);
                echo $company->name. " Data sync successfully";
            }
        }
        else
        {
            $this->line('|===============================================|');
            $this->line('| Syncing Started. It will took minute a while  |');
            $this->line('|===============================================|');
            //$bar = $this->output->createProgressBar(count($companiesArray));
            //$bar->start();
            $company_data = Setting::where('key', '=', 'company_offset')->first();
            if( empty( $company_data ) )
            {
                $company_offset = 0;
            }else{
                $company_offset = $company_data->value;
            }
            $companiesArray = Company::query()->get()->toArray();
            if(isset($companiesArray[$company_offset]))
            {
                $profileArray = $companiesArray[$company_offset];
                //foreach ($companiesArray as $profileArray ) {
            
                $this->company_id = $profileArray['id'];
                //try {
                    //DB::beginTransaction();
                    $cik = ltrim($profileArray['cik']);
                    $cik = sprintf("%010d", (int)$cik);
                    /*
                    $profileJson = $this->secEdgarService->httpRequest("https://data.sec.gov/submissions/CIK$cik.json");
                    if (is_null($profileJson))
                        continue;
                    $profileArray = json_decode($profileJson, true);
                    if (!isset($profileArray['cik']))
                        continue;
                    unset($profileArray['filings']);
                    */
                    /*
                    * Financial Model grep
                    */
                    if( isset($profileArray['symbols']) && is_array($profileArray['symbols']) && !empty($profileArray['symbols']) )
                    {
                        foreach( $profileArray['symbols'] as $symbol_data )
                        {
                            $get_financial_data = array();
                            $profileArray['investorWebsite'] = '';
                            $get_financial_data = $this->get_Financial_model_API( $symbol_data['symbol'] );
                            if($get_financial_data)
                            {
                                $profileArray = array_merge($profileArray , $get_financial_data);
                            }
                            $profileArray = $this->get_eod_historical_data_API( $symbol_data['symbol'] , $profileArray );
                            $profileArray = $this->get_sec_api_io_callback($symbol_data['company_cik'],$profileArray);
                            break;
                        }
                    }
                    $company = $this->companyUpdateOrCreate($profileArray); // Update or Create Company Profile
                    if( !isset($company->id) || empty($company->id) ){
                        //continue;
                    }

                    $this->company_id = $company->id;
                    $this->storeAddresses($profileArray); // Update OR Create Company Addresses
                    $company_offset++;
                    $setting = Setting::firstOrCreate(['key' => 'company_offset']);
                    $setting->update(['value' => $company_offset]);
                    echo $company->name." sync successfully.";
            }
            //$bar->finish();
            $this->info('Companies Successfully Added');
            return 0;
        }
    }
    /*
    * Sec io API
    *
    */
    function get_sec_api_io_callback($sic, $profileArray)
    {
        $new_arr_sec = array();
        $companiesJson = $this->secEdgarService->httpRequest("https://api.sec-api.io/mapping/cik/$sic?token=8253b4ab871ac6b3ef73b882ff4206abda8d61213187d6e9ecf1c0c281c24d82");
        $get_sec_data = json_decode($companiesJson, true);
        if(isset($get_sec_data) && is_array($get_sec_data) && !empty($get_sec_data) && isset($get_sec_data[0]))
        {
            $profileArray['sic'] = $get_sec_data[0]['sic'];
            $profileArray['sic_description'] = $get_sec_data[0]['sicIndustry'];
        }
        return $profileArray;
    }
    /*
    * Protected update multiple ticket Id in symbal table
    *
    */
    protected function get_Financial_model_API($ticker)
    {
        $set_data = array();
        $companiesJson = $this->secEdgarService->httpRequest("https://financialmodelingprep.com/api/v3/profile/".$ticker."?apikey=7c7f7727399cca3486e121267cfa6b0a");
        $get_fin_api_data = json_decode($companiesJson, true);
        $set_data['financial_description'] = $set_data['website'] = $set_data['phone'] = $set_data['exchange'] = $set_data['exchangeShortName'] = $set_data['firstAdded'] = '';
        if( isset($get_fin_api_data) && isset($get_fin_api_data[0]) && !empty($get_fin_api_data[0]) )
        {
            $set_data['financial_description'] = isset($get_fin_api_data[0]['description']) ? $get_fin_api_data[0]['description'] :'';
            $set_data['website'] = isset($get_fin_api_data[0]['website']) ? $get_fin_api_data[0]['website'] : '';
            $set_data['phone'] = isset($get_fin_api_data[0]['phone']) ? $get_fin_api_data[0]['phone'] : '';
            $set_data['exchange'] = isset($get_fin_api_data[0]['exchange']) ? $get_fin_api_data[0]['exchange'] : '';
            $set_data['exchangeShortName'] = isset($get_fin_api_data[0]['exchangeShortName']) ? $get_fin_api_data[0]['exchangeShortName'] : '';
            $set_data['firstAdded'] = isset($get_fin_api_data[0]['ipoDate']) ? $get_fin_api_data[0]['ipoDate'] : '';
        }
        return $set_data;
    }
    /*
    * Protected update multiple ticket Id in historical data
    *
    */
    protected function get_eod_historical_data_API( $ticker, $profileArray)
    {
        $companiesJson = $this->secEdgarService->httpRequest("https://eodhistoricaldata.com/api/fundamentals/".$ticker.".US?api_token=60ec6a9aeff734.69550837");
        $get_eod_api_data = json_decode($companiesJson, true);
        if( isset($get_eod_api_data['General']) && !empty($get_eod_api_data['General']) )
        {
            $profileArray['eod_description'] = isset($get_eod_api_data['General']['Description']) ? $get_eod_api_data['General']['Description'] :'';
            $profileArray['category'] = isset($get_eod_api_data['General']['HomeCategory']) ? $get_eod_api_data['General']['HomeCategory'] : '';
            $profileArray['fiscal_year_end'] = isset($get_eod_api_data['General']['FiscalYearEnd']) ? $get_eod_api_data['General']['FiscalYearEnd'] : '';
            $profileArray['date_first_added'] = isset($get_eod_api_data['General']['IPODate']) ? $get_eod_api_data['General']['IPODate'] : '';
            $profileArray['entityType'] = isset($get_eod_api_data['General']['Type']) ? $get_eod_api_data['General']['Type'] : '';
            $profileArray['ein'] = isset($get_eod_api_data['General']['EmployerIdNumber']) ? $get_eod_api_data['General']['EmployerIdNumber'] : '';
            
            // profile Address
            $profileArray['addresses']['office']['company_id'] = $this->company_id;
            $profileArray['addresses']['office']['street1'] = isset($get_eod_api_data['General']['AddressData']['Street']) ? $get_eod_api_data['General']['AddressData']['Street'] : ''; 
            $profileArray['addresses']['office']['type'] = '';
            $profileArray['addresses']['office']['city'] = isset($get_eod_api_data['General']['AddressData']['City']) ? $get_eod_api_data['General']['AddressData']['City'] : ''; 
            $profileArray['addresses']['office']['zip_code'] = isset($get_eod_api_data['General']['AddressData']['ZIP']) ? $get_eod_api_data['General']['AddressData']['ZIP'] : ''; 
            $profileArray['addresses']['office']['state_or_country'] = isset($get_eod_api_data['General']['AddressData']['State']) ? $get_eod_api_data['General']['AddressData']['State'] : '';
            $profileArray['addresses']['office']['state_or_country_description'] = isset($get_eod_api_data['General']['AddressData']['Country']) ? $get_eod_api_data['General']['AddressData']['Country'] :'';
            //$this->storeAddresses($profileArray);
            $profileArray['Listings'] = isset($get_eod_api_data['General']['Listings']) ? $get_eod_api_data['General']['Listings'] : '';


            $this->updateOrCreateExchangeAndSymbols_eod_historical($profileArray);
        }
        return $profileArray;
    }
    protected function updateOrCreateExchangeAndSymbols_eod_historical($profileArray): void
    {
        if( isset($profileArray['Listings']) )
        {
            if( is_array($profileArray['Listings']) && !empty($profileArray['Listings']) )
            {
                foreach($profileArray['Listings'] as $exchange_data)
                {
                    if(isset($exchange_data['Code']) && !empty($exchange_data['Code']))
                    {
                        Exchange::query()->updateOrCreate(['short_name' => $exchange_data['Code']], ['short_name' => $exchange_data['Code'], 'full_name' => $exchange_data['Name']]);  // Update or Create Exchange
                        $exchange = Exchange::query()->where('short_name', $exchange_data['Code'])->first(); // GetExchange Details
                        $symbolData = [
                            'symbol' => $exchange_data['Code'],
                            'company_id' => $this->company_id,
                            'company_cik' => $profileArray['cik'],
                        ];
                        $symbolData_update = [
                            'symbol' => $exchange_data['Code'],
                            'company_id' => $this->company_id,
                            'company_cik' => $profileArray['cik'],
                            'exchange_id' => $exchange->id,
                        ];
                        Symbol::query()->updateOrCreate($symbolData, $symbolData_update);
                    }
                }
            }
        }
    }
    /*
    * Create and Update exchange code
    *
    */
    protected function updateOrCreateExchangeAndSymbols($profileArray): void
    {
        $symbol = $profileArray['ticker'];
        // foreach ($profileArray['tickers'] as $key => $symbol) {
            if( isset($profileArray['exchanges']) && isset($profileArray['exchanges'][$key]) && !empty($profileArray['exchanges'][$key]) ) 
            {
                $exchange = $profileArray['exchanges'][$key];
                Exchange::query()->updateOrCreate(['short_name' => $exchange], ['short_name' => $exchange, 'full_name' => $exchange]);  // Update or Create Exchange
                $exchange = Exchange::query()->where('short_name', $exchange)->first(); // GetExchange Details
                $symbolData = [
                    'symbol' => $symbol,
                    'company_id' => $this->company_id,
                    'company_cik' => $profileArray['cik'],
                    'exchange_id' => $exchange->id,
                ];
            }else{
                $symbolData = [
                    'symbol' => $symbol,
                    'company_id' => $this->company_id,
                    'company_cik' => $profileArray['cik'],
                    'exchange_id' => 0,
                ];
            }
            Symbol::query()->updateOrCreate($symbolData, $symbolData);
        // }
    }
    /*
    * Store address
    *
    */
    protected function storeAddresses($profileArray): void
    {
        try {
            foreach ($profileArray['addresses'] as $type => $address) {
                $addressArr = [
                    'company_id' => $this->company_id,
                    'street1' => $address['street1'],
                    'street2' => isset($address['street2']) ? $address['street2'] : '',
                    'city' => $address['city'],
                    'zip_code' => $address['zip_code'],
                    'type' => $type,
                    'state_or_country' => $address['state_or_country'],
                    'state_or_country_description' => $address['state_or_country_description'],
                ];
                try {
                    Address::query()->updateOrCreate(array('company_id' => $this->company_id), $addressArr);
                } catch (\Exception $e) {

                }
            }
        } catch (\Exception $e) {

        }
    }
    /* 
    * Company create update code
    *
    */
    protected function companyUpdateOrCreate($profileArray)
    {
        Company::query()->updateOrCreate([
            'name' => trim($profileArray['name']),
        ], [
            'name' => $profileArray['name'],
            'cik' => ltrim($profileArray['cik'], 0),
            'entity_type' => isset($profileArray['entityType']) ? $profileArray['entityType'] : '',
            'sic' => isset($profileArray['sic']) ? $profileArray['sic'] : '',
            'sic_description' => isset($profileArray['sic_description']) ? $profileArray['sic_description'] : '',
            'ein' => isset($profileArray['ein']) ? $profileArray['ein'] : '',
            'description' => isset($profileArray['financial_description']) ? $profileArray['financial_description'] :'',
            'website' => isset($profileArray['website']) ? $profileArray['website'] : '',
            'investor_website' => isset($profileArray['website']) ? $profileArray['website'] : '',
            'category' => isset($profileArray['category'] ) ? $profileArray['category'] :  null,
            'fiscal_year_end' => isset($profileArray['fiscal_year_end']) ? $profileArray['fiscal_year_end'] : '',
            'mailing_address_id' => $profileArray['mailing_address_id'] ? $profileArray['mailing_address_id'] : null,
            'business_address_id' => $profileArray['business_address_id'] ? $profileArray['business_address_id']: null,
            'phone' => isset($profileArray['phone']) ? $profileArray['phone'] : null,
            'date_first_added' => isset($profileArray['firstAdded']) && !empty($profileArray['firstAdded']) ? $profileArray['firstAdded'] :  null,
            'eod_description' => isset($profileArray['eod_description']) ? $profileArray['eod_description'] : '',
        ]);
        return Company::query()->where('cik', ltrim($profileArray['cik']))->first();
    }
}
