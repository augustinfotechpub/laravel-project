<?php

namespace App\Console\Commands;

use App\Services\FinancialModelingPrep;
use App\Services\EdgarFinancial;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BKSyncFinancialStatements extends Command
{
    protected $fmp, $edgar;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:financial-statements {--all} {--company-id= : Company id} {--from-company-id= : Start from company id} {--cik10digits= : cik10Digits only}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all existing companies financial statements.';

    protected $ticker, $period;
    protected $company_id, $cik;

    public $currentDate, $previous10Year;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FinancialModelingPrep $fmp)
    {
        parent::__construct();
        $this->fmp = $fmp;
        $this->edgar = new EdgarFinancial();
        $this->currentDate = date('Y');
        $this->previous10Year = date('Y', strtotime($this->currentDate. ' - 10 year'));
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Throwable
     */
    public function handle(): int
    {
        $this->info('');
        //\DB::disableQueryLog();
        $fromCompanyId = DB::table('settings')->where('key', 'from-company-id')->first(); //$this->option('from-company-id');
        $companyId = $this->option('company-id');
        $cik10digits = $this->option('cik10digits');
        if (!is_null($fromCompanyId) && $fromCompanyId->value != NULL && $fromCompanyId->value > 0) {
            
            $companies = DB::table('companies')->where('id', '>', $fromCompanyId->value)->limit(50)->get();
        } elseif (!is_null($companyId)) {
            
            $companies = DB::table('companies')->where('id', $companyId)->get();
        } elseif (!is_null($cik10digits)) {
            
            $companies = DB::table('companies')->whereRaw('LENGTH(cik) = 10')->limit(50)->get();
            $this->newLine();
            $this->info('10 digits cik number companies data syncing started.');
            $this->newLine();
        } else {
            
            $companies = DB::table('companies')->limit(50)->get();
            
        }
        
        $periods = ['FY' => 'Fiscal Year', 'quarter' => 'Quarter'];

        $this->info('|===============================================|');
        $this->info('|      Syncing Started Edgar financial statements.    |');
        $this->info('|===============================================|');
        $bar = $this->output->createProgressBar($companies->count());
        $bar->start();
        if (!empty($companies)) {
            foreach ($companies as $company) {
                try {
                    $this->company_id = $company->id;
                    DB::table('settings')->where('key', 'from-company-id')->update(array('value' => $this->company_id));

                    $tempFile = storage_path().'/sync-financial-stm.log';

                    file_put_contents($tempFile, '============ Company - '.$this->company_id.' ================'.PHP_EOL, FILE_APPEND);

                    $dbSymbols = DB::table('symbols')->where("company_cik", ltrim($company->cik, 0))->get();
                    $dbSymbolIs = json_decode($dbSymbols, true);
                    
                    file_put_contents($tempFile, '============ Symbol - '.print_r($dbSymbolIs, true).' ================'.PHP_EOL, FILE_APPEND);

                    if( !isset($dbSymbolIs[0]['symbol']) || empty($dbSymbolIs[0]['symbol'])  ){
                        continue;
                    }
                    $this->ticker = $dbSymbolIs[0]['symbol'];
                    
                    $this->info('Sync for '. $this->ticker . ' Company');

                    $this->cik = ltrim($company->cik, 0);

                    $this->period = 'Fiscal Year';

                    $this->storeIncomeStatement();

                    exit;
                        
                } catch (\Throwable $e) {
                    $this->error($e->getMessage(), $e->getLine());
                }
                $bar->advance();
            }
        }
        $bar->finish();
        $this->info('');
        $this->info('Companies Successfully Added');
        return 0;
    }

    protected function storeIncomeStatement()
    {
        
        $this->info($this->ticker . ' : Fetching Financial data from API '. Date('Y-m-d H:i:s'));
        $EdgarFinancial = new EdgarFinancial();
        $statements = $EdgarFinancial->getFinancialStatements($this->ticker, 120, $this->period);
       
        $balanceSheetNew = [];
        $balanceSheetOlder = [];

        $this->info($this->ticker . ' : Fetched Financial data from API '. Date('Y-m-d H:i:s'));
        if( isset($statements['Balance_Sheet']) ){

            uksort($statements['Balance_Sheet']['quarterly'], [$this, 'compare_date_keys'] );

            $this->info($this->ticker . ' : Inserting BS data '. Date('Y-m-d H:i:s'));
            $i = 0;

            $checkQuery = DB::table('eod_balance_sheets')->where('ticker', $this->ticker)->delete();

            foreach ($statements['Balance_Sheet']['quarterly'] as $date => $statement) {
                $i++;
                if( $this->check_in_range($this->currentDate, $this->previous10Year, $date) != 'true'){
                    continue;
                }

                $statement["company_id"] = $this->company_id;
                $statement["ticker"] = $this->ticker;
                $statement["period_type"] = "quarterly";
                $statement["period_date"] = $date;

                // Logical Columns Start
                $statement["grossPPE"] = ( (float)(@$statement['accumulatedDepreciation']) + (float)(@$statement['propertyPlantEquipment']) );
                $statement["dividendsPaid"] = @$statements['Cash_Flow']['quarterly'][$date]['dividendsPaid'];
                $statement["dividendShare"] = @$statements['Highlights']['DividendShare'];
                $statement["dividendYield"] = @$statements['Highlights']['DividendYield'];
                
                $statement["BookValue"] = ((float)@$statement['totalAssets'] - (float)@$statement['totalLiab']);
                // Logical Columns End

                try {

                    $statement['cik'] = $this->cik;

                    // if( !$checkQuery->get()->isEmpty() ){
                        // $this->info("Update ".$i);
                        // $checkQuery->update($statement);
                    // } else {
                        // $this->info("As New ".$i);
                        
                        $balanceSheetNew[] = $statement;
                        
                    // }
                       
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                
            }

            uksort($statements['Balance_Sheet']['yearly'], [$this, 'compare_date_keys'] );

            foreach ($statements['Balance_Sheet']['yearly'] as $date => $statement) {

                if( $this->check_in_range($this->currentDate, $this->previous10Year, $date) != 'true'){
                    continue;
                }

                $statement["company_id"] = $this->company_id;
                $statement["ticker"] = $this->ticker;
                $statement["period_type"] = "yearly";
                $statement["period_date"] = $date;

                // Logical Columns Start
                $statement["grossPPE"] = ( (float)(@$statement['accumulatedDepreciation']) + (float)(@$statement['propertyPlantEquipment']) );
                $statement["dividendsPaid"] = @$statements['Cash_Flow']['yearly'][$date]['dividendsPaid'];
                $statement["dividendShare"] = @$statements['Highlights']['DividendShare'];
                $statement["dividendYield"] = @$statements['Highlights']['DividendYield'];
                
                $statement["BookValue"] = ((float)@$statement['totalAssets'] - (float)@$statement['totalLiab']);

                // Logical Columns End

                try {

                    $statement['cik'] = $this->cik;

                    // $checkQuery = DB::table('eod_balance_sheets')->where('cik', $this->cik)->where('ticker', $this->ticker)->where('period_type', $statement['period_type'])->where('period_date', $statement['period_date']);


                    // if( !$checkQuery->get()->isEmpty() ){
                    //     $checkQuery->update($statement);
                    // } else {

                        $balanceSheetNew[] = $statement;
                    // }

                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                    
            }
            
            if( !empty($balanceSheetNew) ) {
                DB::table('eod_balance_sheets')->insert($balanceSheetNew);
            }
            
            $this->info($this->ticker . ' : Inserted BS data '. Date('Y-m-d H:i:s'));

        }

        if( isset($statements['Income_Statement']) ){
            $this->info($this->ticker . ' : Inserting IS data '. Date('Y-m-d H:i:s'));
            $this->storeIncomeStatementEdgar( $statements );
            $this->info($this->ticker . ' : Inserted IS data '. Date('Y-m-d H:i:s'));
        }

        if( isset($statements['Cash_Flow']) ){
            $this->info($this->ticker . ' : Inserting CF data '. Date('Y-m-d H:i:s'));
            $this->storeCashFlowEdgar( $statements );
            $this->info($this->ticker . ' : Inserted CF data '. Date('Y-m-d H:i:s'));
        }

    }

    protected function storeIncomeStatementEdgar($incomeData)
    {

        

        if( !empty($incomeData['Income_Statement']['quarterly']) ){

            uksort($incomeData['Income_Statement']['quarterly'], [$this, 'compare_date_keys'] );

            $incomeStatementNew = [];

            $oldDate = null;

            $checkQuery = DB::table('eod_income_statements')->where('ticker', $this->ticker)->delete();

            $tempFile = storage_path().'/is-dfl-dtl.log';

            foreach ($incomeData['Income_Statement']['quarterly'] as $date => $statement) {
                
                 file_put_contents($tempFile, '============ Date - '.$date.' ================'.PHP_EOL, FILE_APPEND);
                 file_put_contents($tempFile, '============ Old Date - '.$oldDate.' ================'.PHP_EOL, FILE_APPEND);
            
                if( $this->check_in_range($this->currentDate, $this->previous10Year, $date) != 'true'){
                    continue;
                }
                $statement["company_id"] = $this->company_id;
                $statement["ticker"] = $this->ticker;
                $statement["period_type"] = "quarterly";
                $statement["period_date"] = $date;

                if( (@$incomeData['Balance_Sheet']['quarterly'][$date]['commonStockSharesOutstanding'] !== null) && !empty(@$incomeData['Balance_Sheet']['quarterly'][$date]['commonStockSharesOutstanding']) ){

                    $statement["eps"] = $this->custombcdiv(@$statement['netIncomeApplicableToCommonShares'], $incomeData['Balance_Sheet']['quarterly'][$date]['commonStockSharesOutstanding']);

                } else {
                    $statement["eps"] = null;
                }

                $statement['dol'] = null;

                if( $oldDate !== null ){
                    
                     file_put_contents($tempFile, '==== Date operatingIncome  - '.$statement['operatingIncome'].' ================'.PHP_EOL, FILE_APPEND);
                     file_put_contents($tempFile, '==== Old Date operatingIncome  - '.$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'].' ================'.PHP_EOL, FILE_APPEND);
                    
                     file_put_contents($tempFile, '==== Date totalRevenue  - '.$statement['totalRevenue'].' ================'.PHP_EOL, FILE_APPEND);
                     file_put_contents($tempFile, '==== Old Date totalRevenue  - '.$incomeData['Income_Statement']['quarterly'][$oldDate]['totalRevenue'].' ================'.PHP_EOL, FILE_APPEND);
                    
                    if( @$statement['operatingIncome'] !== null && (float)@$statement['operatingIncome'] != 0 ){

                        if( @$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'] !== null && @$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'] != 0){


                            $oldOperatingIncome = @$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'];
                            $newOperatingIncome = @$statement['operatingIncome'];

                            $dataDifference = ((float)$oldOperatingIncome - (float)$newOperatingIncome);

                            $dataDifferencePercentage = $this->custombcdiv($dataDifference, $oldOperatingIncome, 6) * 100;

                            if( @$statement['totalRevenue'] !== null && (float)@$statement['totalRevenue'] != 0 ){


                                if( @$incomeData['Income_Statement']['quarterly'][$oldDate]['totalRevenue'] !== null && @$incomeData['Income_Statement']['quarterly'][$oldDate]['totalRevenue'] != 0 ){

                                    $oldTotalRevenue = @$incomeData['Income_Statement']['quarterly'][$oldDate]['totalRevenue'];
                                    $newTotalRevenue = @$statement['totalRevenue'];


                                    $dataDifferenceTR = ((float)$oldTotalRevenue - (float)$newTotalRevenue);

                                    $dataDifferencePercentageTR = $this->custombcdiv($dataDifferenceTR, $oldTotalRevenue, 6) * 100;

                                    

                                    if( (float)$dataDifferencePercentage != 0 && (float)$dataDifferencePercentageTR != 0 ){

                                    $statement['dol'] = $this->custombcdiv($dataDifferencePercentage, $dataDifferencePercentageTR, 6);
                                    file_put_contents($tempFile, '==== DOL  - '.$statement['dol'].' ================'.PHP_EOL, FILE_APPEND);
                                    }


                                }
                            }

                        }

                    }
                }

                $statement['dfl'] = null;
                $statement['dtl'] = null;

                if( $oldDate !== null ){
                    
                     file_put_contents($tempFile, '==== Date netIncomeFromContinuingOps  - '.$statement['netIncomeFromContinuingOps'].' ================'.PHP_EOL, FILE_APPEND);
                     file_put_contents($tempFile, '==== Old Date netIncomeFromContinuingOps  - '.$incomeData['Income_Statement']['quarterly'][$oldDate]['netIncomeFromContinuingOps'].' ================'.PHP_EOL, FILE_APPEND);
                    
                     file_put_contents($tempFile, '==== Date operatingIncome  - '.$statement['operatingIncome'].' ================'.PHP_EOL, FILE_APPEND);
                     file_put_contents($tempFile, '==== Old Date operatingIncome  - '.$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'].' ================'.PHP_EOL, FILE_APPEND);
                    
                    if( @$statement['netIncomeFromContinuingOps'] !== null && (float)@$statement['netIncomeFromContinuingOps'] != 0 ){

                        if( @$incomeData['Income_Statement']['quarterly'][$oldDate]['netIncomeFromContinuingOps'] !== null && @$incomeData['Income_Statement']['quarterly'][$oldDate]['netIncomeFromContinuingOps'] != 0){
                            $oldOperatingIncome = @$incomeData['Income_Statement']['quarterly'][$oldDate]['netIncomeFromContinuingOps'];
                            $newOperatingIncome = @$statement['netIncomeFromContinuingOps'];

                            $dataDifference = ((float)$oldOperatingIncome - (float)$newOperatingIncome);

                            
                            $dataDifferencePercentage = $this->custombcdiv($dataDifference, $oldOperatingIncome, 6) * 100;


                            if( @$statement['operatingIncome'] !== null && (float)@$statement['operatingIncome'] != 0 ){
                                if( @$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'] !== null && @$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'] != 0 ){

                                    $oldTotalRevenue = @$incomeData['Income_Statement']['quarterly'][$oldDate]['operatingIncome'];
                                    $newTotalRevenue = @$statement['operatingIncome'];

                                    $dataDifferenceTR = ((float)$oldTotalRevenue - (float)$newTotalRevenue);
                                    file_put_contents($tempFile, '============================'.PHP_EOL, FILE_APPEND);
                                    file_put_contents($tempFile, '==== oldTotalRevenue  - '.$oldTotalRevenue.' ================'.PHP_EOL, FILE_APPEND);
                                    file_put_contents($tempFile, '==== newTotalRevenue  - '.$newTotalRevenue.' ================'.PHP_EOL, FILE_APPEND);
                                    file_put_contents($tempFile, '==== dataDifferenceTR  - '.$dataDifferenceTR.' ================'.PHP_EOL, FILE_APPEND);
                                    
                                    $dataDifferencePercentageTR = $this->custombcdiv($dataDifferenceTR, $oldTotalRevenue, 6) * 100;
                                    
                                    file_put_contents($tempFile, '==== dataDifferencePercentage  - '.$dataDifferencePercentage.' ================'.PHP_EOL, FILE_APPEND);
                                    file_put_contents($tempFile, '==== dataDifferencePercentageTR  - '.$dataDifferencePercentageTR.' ================'.PHP_EOL, FILE_APPEND);
                                    $statement['dfl'] = $this->custombcdiv($dataDifferencePercentage, $dataDifferencePercentageTR, 6);

                                    $statement['dtl'] = $statement['dfl'] * $statement['dol'];

                                    file_put_contents($tempFile, '==== Date operatingIncome  - '.$statement['operatingIncome'].' ================'.PHP_EOL, FILE_APPEND);

                                }
                            }

                        }

                    }
                }
                // End of logical columns

                try {


                    $statement['cik'] = $this->cik;
                    // $checkQuery = DB::table('eod_income_statements')->where('cik', $this->cik)->where('ticker', $this->ticker)->where('period_type', $statement['period_type'])->where('period_date', $statement['period_date'])->first();
                    // file_put_contents($tempFile, '==== checkQuery  - '.print_r($checkQuery, true).' ================'.PHP_EOL, FILE_APPEND);
                    // if( !empty($checkQuery) ){
                    //     file_put_contents($tempFile, '==== Update  - '.print_r($statement, true).' ================'.PHP_EOL, FILE_APPEND);
                    //     //DB::table('eod_income_statements')->where('id', $checkQuery->id)->update($statement);
                    //     //$checkQuery->update($statement);
                    // } else {
                        //file_put_contents($tempFile, '==== Insert  - '.print_r($statement, true).' ================'.PHP_EOL, FILE_APPEND);
                        $incomeStatementNew[] = $statement;
                    //}

                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                $oldDate = $date;
            }
        }

        if( !empty($incomeData['Income_Statement']['yearly']) ){

            uksort($incomeData['Income_Statement']['yearly'], [$this, 'compare_date_keys'] );

            $oldDate = null;
            foreach ($incomeData['Income_Statement']['yearly'] as $date => $statement) {
                
                if( $this->check_in_range($this->currentDate, $this->previous10Year, $date) !='true' ){
                    continue;
                }


                $statement["company_id"] = $this->company_id;
                $statement["ticker"] = $this->ticker;
                $statement["period_type"] = "yearly";
                $statement["period_date"] = $date;

                if( (@$incomeData['Balance_Sheet']['yearly'][$date]['commonStockSharesOutstanding'] !== null) && !empty(@$incomeData['Balance_Sheet']['yearly'][$date]['commonStockSharesOutstanding']) ){

                    $statement["eps"] = $this->custombcdiv(@$statement['netIncomeApplicableToCommonShares'], @$incomeData['Balance_Sheet']['yearly'][$date]['commonStockSharesOutstanding']);

                } else {
                    $statement["eps"] = null;
                }
                
                $statement['dol'] = null;


                if( $oldDate !== null ){
                    if( @$statement['operatingIncome'] !== null && (float)@$statement['operatingIncome'] != 0 ){

                        if( @$incomeData['Income_Statement']['yearly'][$oldDate]['operatingIncome'] !== null && @$incomeData['Income_Statement']['yearly'][$oldDate]['operatingIncome'] != 0){
                            $oldOperatingIncome = @$incomeData['Income_Statement']['yearly'][$oldDate]['operatingIncome'];
                            $newOperatingIncome = @$statement['operatingIncome'];

                            $dataDifference = ((float)$oldOperatingIncome - (float)$newOperatingIncome);

                            
                            $dataDifferencePercentage = ( $this->custombcdiv($dataDifference, $oldOperatingIncome) * 100 );


                            if( @$statement['totalRevenue'] !== null && (float)@$statement['totalRevenue'] != 0 ){

                                if( @$incomeData['Income_Statement']['yearly'][$oldDate]['totalRevenue'] !== null && @$incomeData['Income_Statement']['yearly'][$oldDate]['totalRevenue'] != 0 ){

                                    $oldTotalRevenue = @$incomeData['Income_Statement']['yearly'][$oldDate]['totalRevenue'];
                                    $newTotalRevenue = @$statement['totalRevenue'];

                                    $dataDifferenceTR = ((float)$oldTotalRevenue - (float)$newTotalRevenue);

                                    
                                    $dataDifferencePercentageTR = ( $this->custombcdiv($dataDifferenceTR, $oldTotalRevenue) * 100 );

                                    
                                    $statement['dol'] = $this->custombcdiv($dataDifferencePercentage, $dataDifferencePercentageTR);
                                    
                                }
                            }

                        }

                    }
                }

                $statement['dfl'] = null;
                $statement['dtl'] = null;

                if( $oldDate !== null ){
                    if( @$statement['netIncomeFromContinuingOps'] !== null && (float)@$statement['netIncomeFromContinuingOps'] != 0 ){
                        
                        if( @$incomeData['Income_Statement']['yearly'][$oldDate]['netIncomeFromContinuingOps'] !== null && @$incomeData['Income_Statement']['yearly'][$oldDate]['netIncomeFromContinuingOps'] != 0){

                            $oldOperatingIncome = @$incomeData['Income_Statement']['yearly'][$oldDate]['netIncomeFromContinuingOps'];
                            $newOperatingIncome = @$statement['netIncomeFromContinuingOps'];

                            $dataDifference = ((float)$oldOperatingIncome - (float)$newOperatingIncome);

                            
                            $dataDifferencePercentage = ( $this->custombcdiv($dataDifference, $oldOperatingIncome) * 100);

                            if( @$statement['operatingIncome'] !== null && (float)@$statement['operatingIncome'] != 0 ){
                                if( @$incomeData['Income_Statement']['yearly'][$oldDate]['operatingIncome'] !== null && @$incomeData['Income_Statement']['yearly'][$oldDate]['operatingIncome'] != 0 ){

                                    $oldTotalRevenue = @$incomeData['Income_Statement']['yearly'][$oldDate]['operatingIncome'];
                                    $newTotalRevenue = @$statement['operatingIncome'];

                                    $dataDifferenceTR = ((float)$oldTotalRevenue - (float)$newTotalRevenue);

                                    $dataDifferencePercentageTR = ($dataDifferenceTR/$oldTotalRevenue) * 100;

                                    $dataDifferencePercentageTR = $this->custombcdiv($dataDifferenceTR, $oldTotalRevenue) * 100;

                                    
                                    $statement['dfl'] = $this->custombcdiv($dataDifferencePercentage, $dataDifferencePercentageTR);
                                   

                                    $statement['dtl'] = $statement['dfl'] * $statement['dol'];

                                }
                            }

                        }

                    }
                }

                // End of logical columns

                try {

                    $statement['cik'] = $this->cik;

                    // $checkQuery = DB::table('eod_income_statements')->where('cik', $this->cik)->where('ticker', $this->ticker)->where('period_type', $statement['period_type'])->where('period_date', $statement['period_date']);


                    // if( !$checkQuery->get()->isEmpty() ){
                    //     $checkQuery->update($statement);
                    // } else {
                        $incomeStatementNew[] = $statement;
                    // }

                } catch (Exception $e) {
                    echo $e->getMessage();
                }
                $oldDate = $date;
            }
        }
        
        if( !empty($incomeStatementNew) ){
            DB::table('eod_income_statements')->insert($incomeStatementNew);
        }

    }

    private function custombcdiv( $num1, $num2, $pointer = 6 ){

        $result = null;
        if( $num1 == null || $num1 == null ){
            return null;
        }

        // if( $num1 < 1 && $num1 > -1 ){
        //     return null;
        // }
        // if( $num2 < 1 && $num2 > -1 ){
        //     return null;
        // }

        if( $num2 == 0 || $num1 == 0 ){
            return null;
        }

        if( empty($num2) || empty($num1) ){
            return null;
        }

        if( is_null($num2) || is_null($num1) ){
            return null;
        }


        try {
            $result = (float)$num1/(float)$num2;
        } catch (Exception $e) {
            return null;
        }

        return $result;

    }

    private function check_in_range($startYear, $endYear, $year_from_user) {
        // Convert to timestamp
        
        $year_from_user = date('Y', strtotime($year_from_user));

        if( $year_from_user <= $startYear &&  $year_from_user >= $endYear ){
            return 'true';
        }

        return 'false';
    }

    protected function storeCashFlowEdgar($incomeData){

        $oldDate = null;
        $cashFlowNew = [];

        if( !empty($incomeData['Cash_Flow']['quarterly']) ){

            uksort($incomeData['Cash_Flow']['quarterly'], [$this, 'compare_date_keys'] );

            $checkQuery = DB::table('eod_cash_flows')->where('ticker', $this->ticker)->delete();

            foreach ($incomeData['Cash_Flow']['quarterly'] as $date => $statement) {
                if( $this->check_in_range($this->currentDate, $this->previous10Year, $date) != 'true'){
                    continue;
                }
                $statement["company_id"] = $this->company_id;
                $statement["ticker"] = $this->ticker;
                $statement["period_type"] = "quarterly";
                $statement["period_date"] = $date;
                $statement["propertyPlantAndEquipmentGross"] = null;

                // Start of logical columns
                $statement["netIncomeFromContinuingOps"] = @$incomeData['Income_Statement']['quarterly'][$date]['netIncomeFromContinuingOps'];
                $statement["depreciationAndAmortization"] = @$incomeData['Income_Statement']['quarterly'][$date]['depreciationAndAmortization'];

                $statement["totalCurrentAssets"] = null;
                $statement["totalCurrentLiabilities"] = null;
                $statement["accountsPayable"] = null;
                $statement["propertyPlantAndEquipmentGross"] = null;
                $statement["longTermDebtTotal"] = null;
                $statement["shortTermDebt"] = null;
                $statement["commonStockSharesOutstanding"] = null;

                if( $oldDate != null ){

                    $newTotalcurrentAssets = @$incomeData['Balance_Sheet']['quarterly'][$date]['totalCurrentAssets'];
                    $oldTotalcurrentAssets = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['totalCurrentAssets'];
                    $statement["totalCurrentAssets"] = ( (float)$oldTotalcurrentAssets - (float)$newTotalcurrentAssets );

                    $newtotalCurrentLiabilities = @$incomeData['Balance_Sheet']['quarterly'][$date]['totalCurrentLiabilities'];
                    $oldtotalCurrentLiabilities = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['totalCurrentLiabilities'];
                    $statement["totalCurrentLiabilities"] = ( (float)$oldtotalCurrentLiabilities - (float)$newtotalCurrentLiabilities );

                    $newAccountsPayable = @$incomeData['Balance_Sheet']['quarterly'][$date]['accountsPayable'];
                    $oldAccountsPayable = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['accountsPayable'];
                    $statement["accountsPayable"] = ( (float)$oldAccountsPayable - (float)$newAccountsPayable );
                    
                    $newPropertyPlantAndEquipmentGross = @$incomeData['Balance_Sheet']['quarterly'][$date]['propertyPlantAndEquipmentGross'];
                    $oldPropertyPlantAndEquipmentGross = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['propertyPlantAndEquipmentGross'];
                    $statement["propertyPlantAndEquipmentGross"] = ( (float)$oldPropertyPlantAndEquipmentGross - (float)$newPropertyPlantAndEquipmentGross );

                    $newLongTermDebtTotal = @$incomeData['Balance_Sheet']['quarterly'][$date]['longTermDebtTotal'];
                    $oldLongTermDebtTotal = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['longTermDebtTotal'];
                    $statement["longTermDebtTotal"] = ( (float)$oldLongTermDebtTotal - (float)$newLongTermDebtTotal );

                    $newShortTermDebt = @$incomeData['Balance_Sheet']['quarterly'][$date]['shortTermDebt'];
                    $oldShortTermDebt = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['shortTermDebt'];
                    $statement["shortTermDebt"] = ( (float)$oldShortTermDebt - (float)$newShortTermDebt );

                    $newCommonStockSharesOutstanding = @$incomeData['Balance_Sheet']['quarterly'][$date]['commonStockSharesOutstanding'];
                    $oldCommonStockSharesOutstanding = @$incomeData['Balance_Sheet']['quarterly'][$oldDate]['commonStockSharesOutstanding'];

                    $statement["commonStockSharesOutstanding"] = ( (float)$oldCommonStockSharesOutstanding - (float)$newCommonStockSharesOutstanding );

                }

                // End of logical columns

                try {
                    
                    $statement['cik'] = $this->cik;

                    // $checkQuery = DB::table('eod_cash_flows')->where('cik', $this->cik)->where('ticker', $this->ticker)->where('period_type', $statement['period_type'])->where('period_date', $statement['period_date']);


                    // if( !$checkQuery->get()->isEmpty() ){
                    //     $checkQuery->update($statement);
                    // } else {
                        $cashFlowNew[] = $statement;
                    // }
                    
                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                $oldDate = $date;
            }
        }

        $oldDate = null;

        if( !empty($incomeData['Cash_Flow']['yearly']) ){

            uksort($incomeData['Cash_Flow']['yearly'], [$this, 'compare_date_keys'] );
            
            foreach ($incomeData['Cash_Flow']['yearly'] as $date => $statement) {
                if( $this->check_in_range($this->currentDate, $this->previous10Year, $date) != 'true'){
                    continue;
                }
                $statement["company_id"] = $this->company_id;
                $statement["ticker"] = $this->ticker;
                $statement["period_type"] = "yearly";
                $statement["period_date"] = $date;

                // Start of logical columns
                $statement["netIncomeFromContinuingOps"] = @$incomeData['Income_Statement']['yearly'][$date]['netIncomeFromContinuingOps'];
                $statement["depreciationAndAmortization"] = @$incomeData['Income_Statement']['yearly'][$date]['depreciationAndAmortization'];

                $statement["totalCurrentAssets"] = null;
                $statement["totalCurrentLiabilities"] = null;
                $statement["accountsPayable"] = null;
                $statement["propertyPlantAndEquipmentGross"] = null;
                $statement["longTermDebtTotal"] = null;
                $statement["shortTermDebt"] = null;
                $statement["commonStockSharesOutstanding"] = null;

                if( $oldDate != null ){

                    $newTotalcurrentAssets = @$incomeData['Balance_Sheet']['yearly'][$date]['totalCurrentAssets'];
                    $oldTotalcurrentAssets = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['totalCurrentAssets'];
                    $statement["totalCurrentAssets"] = ( (float)$oldTotalcurrentAssets - (float)$newTotalcurrentAssets );

                    $newtotalCurrentLiabilities = @$incomeData['Balance_Sheet']['yearly'][$date]['totalCurrentLiabilities'];
                    $oldtotalCurrentLiabilities = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['totalCurrentLiabilities'];
                    $statement["totalCurrentLiabilities"] = ( (float)$oldtotalCurrentLiabilities - (float)$newtotalCurrentLiabilities );

                    $newAccountsPayable = @$incomeData['Balance_Sheet']['yearly'][$date]['accountsPayable'];
                    $oldAccountsPayable = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['accountsPayable'];
                    $statement["accountsPayable"] = ( (float)$oldAccountsPayable - (float)$newAccountsPayable );
                    
                    $newPropertyPlantAndEquipmentGross = @$incomeData['Balance_Sheet']['yearly'][$date]['propertyPlantAndEquipmentGross'];
                    $oldPropertyPlantAndEquipmentGross = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['propertyPlantAndEquipmentGross'];
                    $statement["propertyPlantAndEquipmentGross"] = ( (float)$oldPropertyPlantAndEquipmentGross - (float)$newPropertyPlantAndEquipmentGross );

                    $newLongTermDebtTotal = @$incomeData['Balance_Sheet']['yearly'][$date]['longTermDebtTotal'];
                    $oldLongTermDebtTotal = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['longTermDebtTotal'];
                    $statement["longTermDebtTotal"] = ( (float)$oldLongTermDebtTotal - (float)$newLongTermDebtTotal );

                    $newShortTermDebt = @$incomeData['Balance_Sheet']['yearly'][$date]['shortTermDebt'];
                    $oldShortTermDebt = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['shortTermDebt'];
                    $statement["shortTermDebt"] = ( (float)$oldShortTermDebt - (float)$newShortTermDebt );

                    $newCommonStockSharesOutstanding = @$incomeData['Balance_Sheet']['yearly'][$date]['commonStockSharesOutstanding'];
                    $oldCommonStockSharesOutstanding = @$incomeData['Balance_Sheet']['yearly'][$oldDate]['commonStockSharesOutstanding'];
                    $statement["commonStockSharesOutstanding"] = ( (float)$oldCommonStockSharesOutstanding - (float)$newCommonStockSharesOutstanding );

                }

                // End of logical columns
                try {

                    $statement['cik'] = $this->cik;

                    $checkQuery = DB::table('eod_cash_flows')->where('cik', $this->cik)->where('ticker', $this->ticker)->where('period_type', $statement['period_type'])->where('period_date', $statement['period_date']);


                    // if( !$checkQuery->get()->isEmpty() ){
                    //     $checkQuery->update($statement);
                    // } else {
                        $cashFlowNew[] = $statement;
                    // }

                } catch (Exception $e) {
                    echo $e->getMessage();
                }

                $oldDate = $date;
            }
        }
        if( !empty($cashFlowNew) ){

            $insert = DB::table('eod_cash_flows')->insert($cashFlowNew);   
        }
    }

    protected function compare_date_keys($dt1, $dt2) {
        return strtotime($dt1) - strtotime($dt2);
    }

}
