<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 01-12-2020
 * Time: 05:52 PM
 */

namespace App\Services;

use Illuminate\Http\Request;
use Mockery\Exception;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;

class Stripe
{
    protected $publishable_key;
    protected $secret_key;
    protected $stripe;

    public function __construct()
    {
        self::init();
    }

    protected function init()
    {
        $this->publishable_key = config('stripe.publishable_key');
        $this->secret_key = config('stripe.secret_key');
        if (!is_string($this->publishable_key)) {
            self::init();
        }
        if (!is_string($this->secret_key)) {
            self::init();
        }
        $this->stripe = new StripeClient($this->secret_key);
    }

    public function getAllProducts()
    {
        return $this->stripe->products->all();
    }

    public function createCustomer($customer)
    {
        return $this->stripe->customers->create([
//            'id' => $customer['id'],
            'email' => $customer['email'],
            'description' => $customer['description'] ?? null,
//            'payment_method' => $customer['payment_method'] ?? null,
//            'phone' => $customer['phone'] ?? null
        ]);
    }

    public function createCustomerSubscription($customer, $price)
    {
        return $this->stripe->subscriptions->create([
            'customer' => $customer->id,
            'items' => [
                'price' => $price
            ]
        ]);
    }

    public function createSubscriptionPlan(Request $request)
    {
        $stripe_product = self::createProduct($request);
        if (!is_null($stripe_product)) {

            $stripe_price = self::createPriceForPlan($request, $stripe_product);
        }

        return ["product" => $stripe_product, "price" => $stripe_price];
    }

    public function updateSubscriptionPlan(Request $request)
    {
        $stripe_product = self::updateProduct($request);

        if (!is_null($stripe_product)) {
            $stripe_price = self::updatePriceForPlan($request);

        }
        return ["product" => $stripe_product, "price" => $stripe_price];
    }

    protected function updateProduct(Request $request)
    {

        return $this->stripe->products->update(
            $request->get('product_key'),
            ['name' => $request->get('title')]);
    }

    protected function createProduct(Request $request)
    {
        return $this->stripe->products->create([
            'name' => $request->get('title'),
        ]);
    }

    public function createPriceForPlan(Request $request, $stripe_product)
    {

        return $this->stripe->prices->create([
            'unit_amount' => $request->get('price'),
            'currency' => 'usd',
            'recurring' => [
                'interval' => $request->interval_unit,
                'interval_count' => $request->interval_value,
            ],
            'product' => $stripe_product->id,
        ]);
    }

    public function updatePriceForPlan(Request $request)
    {

        return $this->stripe->prices->update(
            $request->get('price_key'), [
            'unit_amount' => $request->price_key,
            'currency' => 'usd',
            'recurring' => [
                'interval' => $request->interval_unit,
                'interval_count' => $request->interval_value,
            ],
            'product' => $request->product_key,
        ]);
    }

    public function makeSubscription($stripe_customer_id, $price_id): \Stripe\Subscription
    {
        return $this->stripe->subscriptions->create([
            'customer' => $stripe_customer_id,
            'items' => [[
                'price' => $price_id,
            ]]
        ]);
    }

    public function deleteCustomer()
    {
        // TODO: Implement deleteCustomer() method.
    }

    public function updateCustomer()
    {
        // TODO: Implement updateCustomer() method.
    }

    public function createPlan($plan)
    {
        try {
            $plan = \Stripe\Plan::create($plan);
        } catch (Exception $e) {
            dd($e);
        }
        return $plan;
    }

    public function makeCharge($requestData)
    {
        return $this->stripe->charges->create([
            "amount" => (int)$requestData['amount'],
            "currency" => $requestData['currency'] ?? "usd",

            "customer" => $requestData['stripeCustomerId'],
            'receipt_email' => $requestData['email'],
            "description" => $requestData['description'] ?? '---',
        ]);
    }

    public function subscriptionItems()
    {
        return $this->stripe->subscriptionItems->create([
            'subscription' => 'sub_IZLRjUHEIucQ7b',
            'price' => 'price_1Hx3QMKlA0HDGWBTOylVzBy7',
            'quantity' => 2,
        ]);
    }

    public function makePaymentIntent($requestData)
    {
        return $this->stripe->paymentIntents->create([
            'amount' => (int)$requestData['amount'],
            'currency' => 'usd',
            'payment_method_types' => ['card'],
        ]);
    }

    public function createSource($customer_id, $stripeToken)
    {
        return $this->stripe->customers->createSource(
            $customer_id,
            ['source' => $stripeToken]
        );
    }

    public function initPayout($amount, $receiver_account_id): \Stripe\Payout
    {
        return $this->stripe->payouts->create([
            'amount' => 1,
            'currency' => 'usd',
        ], [
            'stripe_account' => $receiver_account_id
        ]);
    }

    public function getPayoutDetails($payoutKey): \Stripe\Payout
    {
        return $this->stripe->payouts->retrieve(
            $payoutKey,
            []
        );
    }

    public function updatePayout($payoutKey, array $data): \Stripe\Payout
    {
        return $this->stripe->payouts->update(
            $payoutKey,
            $data
        );
    }

    public function getAllPayouts(): \Stripe\Collection
    {
        return $this->stripe->payouts->all([]);
    }

    public function cancelPayout($payoutKey): \Stripe\Payout
    {
        return $this->stripe->payouts->cancel($payoutKey);
    }

    public function reversePayout($payoutKey): \Stripe\Payout
    {
        return $this->stripe->payouts->reverse($payoutKey);
    }

    public function createAccount(array $data)
    {
        return $this->stripe->accounts->create([
            'type' => 'custom',
            'country' => 'US',
            'email' => $data['email_id'],
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
            ],
            'business_type' => 'individual',
            'individual' => [
                'dob' => [
                    'day' => $data['dob']['day'],
                    'month' => $data['dob']['month'],
                    'year' => $data['dob']['year'],
                ],
                'email' => $data['email_id'],
                'gender' => $data['gender'],
                'last_name' => $data['person']['last_name'] ?? null,
                'phone' => $data['person']['phone'] ?? null,
                'ssn_last_4' => $data['ssn_last_4'],

                "address" => [
                    "city" => $data['address']['city'], // "Bowie",
                    "country" => "US",
                    "line1" => $data['address']['line1'],// "9904 Nicol Ct W",
                    "line2" => $data['address']['line2'] ?? null,
                    "postal_code" => $data['address']['postal_code'], // "20721",
                    "state" => $data['address']['state'], // "MD"
                ]
            ],
            'external_account' => $data['stripe_bank_token']
        ]);
    }

    public function updateAccount($stripe_account_id)
    {
        return $this->stripe->accounts->update(
            $stripe_account_id
        );
    }

    public function createPerson($stripe_account_id, $first_name, $last_name)
    {
        return $this->stripe->accounts->createPerson(
            $stripe_account_id,
            [
                'first_name' => $first_name,
                'last_name' => $last_name
            ]
        );
    }

    public function makeTransfer($receiver_stripe_account_id)
    {
        return $this->stripe->transfers->create([
            'amount' => 400,
            'currency' => 'usd',
            'destination' => $receiver_stripe_account_id, // 'acct_1B4fYwKlA0HDGWBT',
            'transfer_group' => 'ORDER_95',
        ]);
    }

    public function createBankAccountToken($account_holder_name, $routing_number, $account_number, $account_holder_type = 'individual'): \Stripe\Token
    {
        return $this->stripe->tokens->create([
            'bank_account' => [
                'country' => 'US',
                'currency' => 'usd',
                'account_holder_name' => $account_holder_name, // 'Jenny Rosen',
                'account_holder_type' => $account_holder_type, // 'individual',
                'routing_number' => $routing_number, // '110000000',
                'account_number' => $account_number, // '000123456789',
            ],
        ]);
    }

    public function createBankAccount($customer_id, $account_holder_name, $routing_number, $account_number, $account_holder_type = 'individual')
    {
        $bank_token = self::createBankAccountToken($account_holder_name, $routing_number, $account_number, $account_holder_type)->id;
        return $this->stripe->customers->createSource(
            $customer_id,
            ['source' => $bank_token]
        );
    }

    public function updateBankAccount($customer_id, $account_holder_name, $routing_number, $account_number, $account_holder_type = 'individual')
    {
        $bank_account_id = self::createBankAccount($customer_id, $account_holder_name, $routing_number, $account_number, $account_holder_type)->id;
        return $this->stripe->customers->updateSource(
            $customer_id,
            $bank_account_id,
            []
        );
    }

    public function getCustomerDetails($stripe_customer_id)
    {
        return $this->stripe->customers->retrieve(
            $stripe_customer_id,
            []
        );
    }

    public function getCustomerBankAccounts($stripe_customer_id)
    {
        return $this->stripe->customers->allSources(
            $stripe_customer_id,
            ['object' => 'bank_account']
        );
    }


    public function deleteCustomerBankAccount($customer_id, $bank_id)
    {
        try {
            return $this->stripe->customers->deleteSource(
                $customer_id,
                $bank_id,
                []
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function allPlans()
    {
        $plans = $this->stripe->plans->all();
        foreach ($plans as $key => $plan) {

            if (!$plan->active) {
                unset($plans[$key]);
                continue;
            }
            $product = self::getProduct($plan->product);
            if (!$product->active) {
                unset($plans[$key]);
            }
        }
        return $plans;
    }

    public function getPlan($stripe_plan_id)
    {
        return $this->stripe->plans->retrieve($stripe_plan_id);
    }

    public function getProduct($product_id)
    {
        return $this->stripe->products->retrieve($product_id);
    }

    public function isPlanExists($stripe_plan_id): bool
    {
        try {
            $this->stripe->getPlan($stripe_plan_id);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
