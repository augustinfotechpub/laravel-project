<?php


namespace App\Services;


use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;

class EdgarFinancial
{
    protected $api_key, $uri;
    public $symbol, $limit, $period;

    public function __construct()
    {
        $this->api_key = config('services.edgar_financial.api_key');
    }

    public function getResponse($path, $method = 'get', $query = [], $apiVersion = 'v2')
    {
        $cashFlow = '';
        $incomeStatement = '';
        $balanceStatement = '';
        $returnResponse = [];

        try {
            $method = strtolower($method);
            $uri = config("services.edgar_financial.uri.eod_url");
            $token = config("services.edgar_financial.api_token");
            
            switch ($method) {
                case 'get':
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => $uri.$path.$query["symbol"].'?api_token='.$token."&filter=Highlights,Financials,Earnings",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 0,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'GET',
                    ));

                    $response = curl_exec($curl);

                    $response = (array) json_decode($response, true);

                    curl_close($curl);

                    if( isset($response['Financials'] ) and !empty($response['Financials']) )
                    {
                        // $cashFlow = $response['Financials']['Cash_Flow'];
                        // $incomeStatement = $response['Financials']['Income_Statement'];
                        // $balanceStatement = $response['Financials']['Balance_Sheet']; 

                        $returnResponse = $response['Financials'];
                        $returnResponse['Highlights'] = $response['Highlights'];
                        $returnResponse['History'] = $response['Earnings']['History'];
                    } else {
                        $returnResponse = [];   
                    }

                    break;
                case 'post':
                    $returnResponse = [];
                    break;
                default :
                    throw new Exception('Invalid http request Method. ' . $method);
            }

            if( isset($response->Error) ){
                throw new Exception( $response->Error->Message );
            }

            if(isset($response['Financials'])){
                // \Log::channel('quote_fetch')->info( json_encode($response) );
                return $returnResponse;
            } else {
                throw new Exception( "Can't fetch the Financials data" );
            }
            
            throw new Exception($response->serverError() . ', ' . $response->clientError());
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * {SYMBOL}?period=quarter&limit={API_KEY}
     */
    public function getFinancialStatements($symbol, $limit = 120, $period = 4)
    {
        // $period = strtolower($period);
        $path = 'api/fundamentals/';
        return $this->getResponse($path, 'get', ['symbol' => $symbol]);
    }

    public function getBalanceSheet($symbol, $limit = 10, $period = 'quarter')
    {
        $path = 'balance-sheet-statement/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        if (!is_null($period)) {
            $query['period'] = $period;
        }

        return $this->getResponse($path, 'get', $query);
    }

    public function getCashFlowStatement($symbol, $limit = 10, $period = 'quarter')
    {
        $path = 'cash-flow-statement/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        if (!is_null($period)) {
            $query['period'] = $period;
        }
        return $this->getResponse($path, 'get', $query);
    }

    public function XMLtoArray($url) {
        $fileContents= file_get_contents($url);
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $simpleXml = simplexml_load_string($fileContents);
        $array = json_encode($simpleXml);

        return $array;
    }

}
