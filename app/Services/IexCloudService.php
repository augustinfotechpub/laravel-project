<?php


namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Http;

class IexCloudService
{
    public function getResponse($endpoint, $method = 'get', $query = [], $apiVersion = 'stable')
    {
        try {
            $method = strtolower($method);
            $baseUrl = config("services.iex_cloud.base_url");
            $version = config("services.iex_cloud.version.$apiVersion");
            $url = $baseUrl . $version . $endpoint;
            $query['token'] = config("services.iex_cloud.keys.publishable");

            $response = match ($method) {
                'get' => Http::get($url, $query),
                'post' => Http::post($url, $query),
                default => throw new Exception('Invalid http request Method. ' . $method),
            };
            if ($response->successful()) {
                $response = $response->json();
                if (isset($ret['Error Message'])) {
                    throw new Exception($ret['Error Message']);
                }
                return $response;
            }
            throw new Exception($response->serverError() . ', ' . $response->clientError());
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
