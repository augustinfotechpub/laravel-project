<?php

namespace App\Services;


use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Exception;

class RapidAPI
{
    protected $client, $base_url, $request, $api_key, $api_host;

    public function __construct()
    {
        $this->api_host = env('X_RAPID_API_HOST');
        $this->api_key = env('X_RAPID_API_KEY');
    }

    protected function getResponse($path, array $query = [], $method = 'GET')
    {
        try {
            $query = http_build_query($query);
            $url = $path . $query;
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => "$url",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "$method",
                CURLOPT_HTTPHEADER => [
                    "x-rapidapi-host: $this->api_host",
                    "x-rapidapi-key: $this->api_key",
                    'Content-Type: application/json'
                ],
            ]);

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $response = json_decode($response);
            if ($err) {
                throw new Exception($err);
            }
            if (isset($response->message)) {
                throw new Exception($response->message);
            }
            return ['status' => 200, 'message' => 'Data Retrieved Successfully', 'data' => $response];
        } catch (Exception $exception) {
            return ['status' => 100, 'message' => $exception->getMessage(), 'data' => []];
        }
    }

    public function getCompanyProfile($symbol, $region = 'US')
    {
        $this->api_host = "apidojo-yahoo-finance-v1.p.rapidapi.com";
        $path = 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-profile?';
        $parameters = [
            'symbol' => $symbol,
            'region' => $region
        ];
        return self::getResponse($path, $parameters);
    }

    public function getChart($symbol, $interval = '5m', $range = '1d', $region = 'US')
    {
        $this->api_host = "apidojo-yahoo-finance-v1.p.rapidapi.com";
        $path = 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-chart?';
        $parameters = [
            'interval' => $interval,
            'symbol' => $symbol,
            'range' => $range,
            'region' => $region
        ];
        return self::getResponse($path, $parameters);
    }

    public function getStatistics($symbol, $region = 'US')
    {
        $this->api_host = "apidojo-yahoo-finance-v1.p.rapidapi.com";
        $path = 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-statistics?';
        $parameters = [
            'symbol' => $symbol,
            'region' => $region
        ];
        return self::getResponse($path, $parameters);
    }

    public function getStockHistory($symbol, $start_date = null, $end_date = null, $frequency = '1d'): array
    {
        if (is_null($start_date) || is_null($end_date)) {
            $start_date = Carbon::now()->subDays(1)->format('U');
            $end_date = Carbon::now()->format('U');
        }
        $this->api_host = "apidojo-yahoo-finance-v1.p.rapidapi.com";
        $path = 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-historical-data?';
        $parameters = [
            'period1' => $start_date,
            'period2' => $end_date,
            'symbol' => $symbol,
            'frequency' => $frequency,
            'filter' => 'history'
        ];
        return self::getResponse($path, $parameters);
    }

    public function getSummary()
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-summary?symbol=AMRN&region=US",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: apidojo-yahoo-finance-v1.p.rapidapi.com",
                "x-rapidapi-key: bb200440e6msh06c49c74aee3b4fp19bbadjsnc4e5fc2335b9"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception("cURL Error #:" . $err);
        } else {
            return json_decode($response);
        }
    }

    public function search($keywords)
    {

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://alpha-vantage.p.rapidapi.com/query?keywords=$keywords&function=SYMBOL_SEARCH&datatype=json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: alpha-vantage.p.rapidapi.com",
                "x-rapidapi-key: $this->api_key"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
        return json_decode($response);
    }

    public function getCompanyNewsList($symbol)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://yahoo-finance15.p.rapidapi.com/api/yahoo/ne/news/$symbol",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: yahoo-finance15.p.rapidapi.com",
                "x-rapidapi-key: $this->api_key"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new Exception("cURL Error #:" . $err);
        } else {
            return json_decode($response);
        }
    }

    public function getStockDetails($symbol)
    {
        $path = "https://yahoo-finance-low-latency.p.rapidapi.com/v11/finance/quoteSummary/$symbol?modules=defaultKeyStatistics%2CassetProfile";
        $this->api_host = "yahoo-finance-low-latency.p.rapidapi.com";
        return self::getResponse($path);
    }

    public function getQuote($symbol)
    {
        $path = "https://alpha-vantage.p.rapidapi.com/query?function=GLOBAL_QUOTE&symbol=" . $symbol . "&datatype=json";
        $this->api_host = "alpha-vantage.p.rapidapi.com";
        return self::getResponse($path);
    }

    public function getHolders($symbol)
    {
        $path = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-holders?symbol=" . $symbol . "&region=US";
        $this->api_host = "apidojo-yahoo-finance-v1.p.rapidapi.com";
        return $this->getResponse($path);
    }

    public function getIncomeStatement($symbol)
    {
        $path = "https://financial-statements.p.rapidapi.com/api/v1/resources/income-statement?";
        $parameters = [
            'ticker' => $symbol,
        ];
        $this->api_host = "financial-statements.p.rapidapi.com";
        return $this->getResponse($path, $parameters);
    }

    public function getBalanceSheet($symbol)
    {
        $path = "https://financial-statements.p.rapidapi.com/api/v1/resources/balance-sheet?";
        $parameters = [
            'ticker' => $symbol,
        ];
        $this->api_host = "financial-statements.p.rapidapi.com";
        return $this->getResponse($path, $parameters);
    }

    public function getCashFlowStatement($symbol)
    {
        $path = "https://financial-statements.p.rapidapi.com/api/v1/resources/cash-flow?";
        $parameters = [
            'ticker' => $symbol,
        ];
        $this->api_host = "financial-statements.p.rapidapi.com";
        return $this->getResponse($path, $parameters);
    }

    public function keyStats($symbol)
    {
        $path = "https://stock-analysis.p.rapidapi.com/api/v1/resources/key-stats?";
        $parameters = [
            'ticker' => $symbol,
        ];
        $this->api_host = "stock-analysis.p.rapidapi.com";
        return $this->getResponse($path, $parameters);
    }
}
