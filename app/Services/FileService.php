<?php


namespace App\Services;

use App\Interfaces\ImageInterface;
use Illuminate\Support\Str;

class FileService
{
    public function store($file, $uploadPath)
    {
        $uploadPath = storage_path($uploadPath);
        $extension = $file->getClientOriginalExtension();
        $fileName = Str::random(32) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function update($oldFile, $newFile, $uploadPath)
    {
        $uploadPath = storage_path($uploadPath);
        if (!empty($oldFile)) {
            $imagePath = $uploadPath . $oldFile;
            @unlink($imagePath);
        }
        $extension = $newFile->getClientOriginalExtension();
        $fileName = Str::random(32) . '.' . $extension;
        $newFile->move($uploadPath, $fileName);
        return $fileName;
    }

    public function remove($fileName, $uploadPath)
    {
        $uploadPath = storage_path($uploadPath);
        if (!empty($fileName)) {
            $imagePath = $uploadPath . $fileName;
            return @unlink($imagePath);
        }
        return false;
    }
}
