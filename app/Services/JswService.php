<?php


namespace App\Services;


use DOMDocument;
use Illuminate\Support\Facades\Http;
use Exception;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class JswService
{
    private const BASE_URL = "https://www.wsj.com/";
    private $client, $fmpService;

    public function __construct()
    {
        $this->client = new Client();
        $this->fmpService = new FinancialModelingPrep();
    }

    protected $insiderTrading;

    public function getOwnerships($symbol): array
    {
        $insiderTradings = $this->fmpService->getCompanyInsiderTrading($symbol, 1000);
        $this->insiderTrading = collect($insiderTradings)->unique("reportingName")->toArray();

        $endPoint = "market-data/quotes/$symbol/company-people";
        $url = self::BASE_URL . $endPoint;
        $crawler = $this->client->request('GET', $url);
        $directorsHtmlTable = $crawler->filter(".scrollBox>.cr_board_table");
        $peoples['directors'] = $directorsHtmlTable->filter("tr")
            ->each(function (Crawler $row, $i) {
                $column1 = $row->filter('td')->eq(0);
                $span = $column1->filter('span');
                $a = $span->eq(0)->filter('a');
                $name = $a->text();
                $profileLink = $a->attr('href');
                $biography = $this->getBiography($profileLink);
                $insiderInfo = $this->searchCikByName($name);
                $personInfo = [
                    'name' => $name,
                    'biography' => $biography,
                    'position' => $span->eq(1)->text(),
                    'type' => 'director',
                    'description' => $row->filter('td')->eq(1)->text(),
                ];
                return array_merge($insiderInfo, $personInfo);
            });

        $liNodes = $crawler->filter('.scrollBox>.cr_all_executives>li');

        $liNodes->reduce(function (Crawler $node, $i) {
            if ($i == 0)
                return true;
            $position = strtolower($node->filter('.cr_data_field>.data_lbl')->text());
            return $position != "independent director";
        });

        $peoples['officers'] = $liNodes->each(function (Crawler $crawler, $i) use ($liNodes) {
            $nameNode = $crawler->filter('.cr_data_field>.data_data>a');
            $position = $crawler->filter('.cr_data_field>.data_lbl')->text();
            $bioLink = $nameNode->attr('href');
            $name = $nameNode->text();
            $insiderInfo = $this->searchCikByName($name);
            if (strtolower($position) != "independent director") {
                $personInfo = [
                    'name' => $name,
                    'position' => $position,
                    'description' => null,
                    'type' => 'officer',
                    'biography' => $this->getBiography($bioLink),
                ];
                return array_merge($insiderInfo, $personInfo);
            }
        });
        return $peoples;
    }

    public function getEodOwnership($ticker)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://eodhistoricaldata.com/api/fundamentals/'.$ticker.'?api_token='.config('services.eod_key_parties.api_key'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response,true);
        return response( ['Holder' => @$response['Holders']['Institutions'], 'Officers' => @$response['General']['Officers'],'AnalystRatings'=> @$response['AnalystRatings']]);              
    }


    protected function getBiography($url): string
    {
        $crawler = $this->client->request('GET', $url);
        $biography = $crawler->filter('.cr_executive_description>p')->each(function ($node) {
            return $node->text();
        });
        return implode("<br/> ", $biography);
    }

    function getAbsoluteString($string): string
    {
        return str_replace('.', "", strtolower($string));
    }

    protected function searchCikByName($name)
    {
        $name = $this->getAbsoluteString($name);
        foreach ($this->insiderTrading as $item) {
            $wsjPersonName = strtolower($name);
            $wsjPersonNameWords = explode(" ", $wsjPersonName);
            $secPersonName = strtolower($item['reportingName']);
            $secPersonNameWords = explode(" ", $secPersonName);
            $intersectNameWords = array_intersect($wsjPersonNameWords, $secPersonNameWords);
            $intersectNameWordsCount = count($intersectNameWords);
            $wsjPersonNameWordsCount = count($wsjPersonNameWords);
            $secPersonNameWordsCount = count($secPersonNameWords);
            $threeWordsName = $wsjPersonNameWordsCount == 3 && $secPersonNameWordsCount == 3;
            if ($threeWordsName && $intersectNameWordsCount == 2)
                return $item;

            if ($intersectNameWordsCount == 2 && $wsjPersonNameWordsCount == 2 && $secPersonNameWordsCount == 2)
                return $item;

            if ($intersectNameWordsCount == 1 && $wsjPersonNameWordsCount == 1 && $secPersonNameWordsCount == 1)
                return $item;
        }
        return [];
    }
}
