<?php


namespace App\Services;

use DOMDocument;
use Goutte\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\IOFactory;
use SimpleXMLElement;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\DomCrawler\Crawler;

class SecEdgarService
{
    protected $endpoint = 'https://www.sec.gov/Archives/edgar/data/';
    protected $cik, $symbol, $fileService, $attempts = 0;

    public function __construct($symbol = null, $cik = null)
    {
        $this->cik = $cik;
        $this->symbol = $symbol;
        $this->fileService = new FileService();
    }

    /**
     * const URL = "storage/fillings/";
     * const PATH = "app/public/fillings/";
     * @param $url
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getFinancialReport($url): array
    {
        $fileContent = file_get_contents($url . 'Financial_Report.xlsx');
        $tmpFName = tempnam(sys_get_temp_dir(), "tmpxls");
        file_put_contents($tmpFName, $fileContent);
        $excelReader = IOFactory::createReaderForFile($tmpFName);
        $excelObj = $excelReader->load($tmpFName);
        $worksheets = $excelObj->getAllSheets();
        $reports = [];
        foreach ($worksheets as $index => $worksheet) {
            $reports[] = self::getReport($url, $index);
        }
        return $reports;
    }

    public function getReport($url, $index)
    {
        try {
            $report = file_get_contents($url . 'R' . ($index + 1) . '.htm');
            $strPos = strpos($report, '<body>');
            $report = substr($report, $strPos);
            $strPos = strpos($report, '</body>');
            $report = substr($report, 0, $strPos);
            return str_replace('<body>', '', $report);
        } catch (\Exception $e) {
            if ($this->attempts == 3)
                return false;
            $this->attempts++;
            self::getReport($url, $index);
        }
        return false;
    }

    public function getComprehensiveIncomeStatement($symbol)
    {
        $report = self::getForm10($symbol, 'year', 1);
        dd($report);
    }

    /*public function getFinancialReport($url): array
    {
        $fileContent = file_get_contents($url);
        $tmpFName = tempnam(sys_get_temp_dir(), "tmpxls");
        file_put_contents($tmpFName, $fileContent);
        $excelReader = IOFactory::createReaderForFile($tmpFName);
        $excelObj = $excelReader->load($tmpFName);
        $worksheets = $excelObj->getAllSheets();

        // dd(count($worksheets),$worksheets);
        $sheets = [];
        foreach ($worksheets as $index=> $worksheet) {
            $sheets[] = ['title'=>$worksheet->getTitle(),'data'=>$worksheet->toArray()];
        }
        return $sheets;
    }*/

    public function getArchiveUrl($final_link): string
    {
        $x = explode('/', $final_link);
        array_pop($x);
        return implode('/', $x) . '/';
    }

    public function getOwnerships($cik)
    {
        $fmp = new FinancialModelingPrep();
        $keys = ['name', 'cik', 'transactions_date', 'position'];

        $html = file_get_contents("http://www.sec.gov/cgi-bin/own-disp?action=getissuer&CIK=$cik");
        $client = new Crawler($html);
        $res = $client->filter("table")->eq(6);
        $htmlTable = $res->outerHtml();
        $DOM = new DOMDocument();
        $DOM->loadHTML($htmlTable);
        $tableRows = $DOM->getElementsByTagName('tr');
        $ownerships = [];
        foreach ($tableRows as $rowIndex => $tableRow) {
            if ($rowIndex == 0)
                continue;

            $columns = $tableRow->getElementsByTagName('td');
            foreach ($columns as $columnIndex => $column) {
                $ownerships[$rowIndex][$keys[$columnIndex]] = $column->textContent;
                if ($columnIndex == 3) {
                    $ownerships[$rowIndex]['type'] = str_contains($column->textContent, 'officer:') ? 'officer' : 'director';
                    if ($ownerships[$rowIndex]['type'] == 'officer') {
                        $ownerships[$rowIndex]['positions'] = explode(',', str_replace('officer:', '', $column->textContent));
                    } else {
                        $ownerships[$rowIndex]['positions'] = [];
                    }
                    $reportingCik = sprintf("%010d", (int)$ownerships[$rowIndex]['cik']);
                    $respJson = file_get_contents("https://financialmodelingprep.com/api/v4/insider-trading?reportingCik=$reportingCik&limit=100&apikey=" . config('services.financial_modeling_prep.api_key'));
                    $respArray = json_decode($respJson, true);
                    $respArrCollection = collect($respArray)->firstWhere('companyCik', $cik);
                    $ownerships[$rowIndex]['securitiesOwned'] = $respArrCollection['securitiesOwned'] ?? null;
                    $ownerships[$rowIndex]['transactionDate'] = $respArrCollection['transactionDate'] ?? null;
                }
            }
        }
        $ownerships = collect($ownerships)
            ->where('transactions_date', '>', now()->subYears(6)->format('Y-m-d'))
            ->groupBy('type')->map(function ($item) {
                if ($item[0]['type'] == 'officer') {
                    $item = $item->unique('position')->toArray();
                    $item = self::reOrderOfficersPositions($item);
                }
                return $item;
            });
        $ownerships['officers'] = $ownerships['officer'] ?? [];
        $ownerships['directors'] = $ownerships['director'] ?? [];
        unset($ownerships['officer'], $ownerships['director']);
        return $ownerships ?? null;
    }


    protected function reOrderOfficersPositions($items)
    {
        $items = array_values($items);
        $officers = [];
        foreach ($items as $key => $item) {
            $word = strtolower($item['position']);
            if (strpos($word, 'ceo') || strpos($word, 'chief executive officer')) {
                if (isset($officers[0])) {
                    continue;
                }
                $officers[0] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'coo') || strpos($word, 'chief operating officer')) {
                if (isset($officers[1])) {
                    continue;
                }
                $officers[1] = $item;
                unset($items[$key]);
                continue;
            }

            if (strpos($word, 'cio') || strpos($word, 'chief information officer')) {
                if (isset($officers[2])) {
                    continue;
                }
                $officers[2] = $item;
                unset($items[$key]);
                continue;
            }

            if (strpos($word, 'cfo') || strpos($word, 'chief financial officer')) {
                if (isset($officers[3])) {
                    continue;
                }
                $officers[3] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'svp') || strpos($word, 'senior vice president')) {
                if (isset($officers[4])) {
                    continue;
                }
                $officers[4] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'vp') || strpos($word, 'vice president')) {
                if (isset($officers[5])) {
                    continue;
                }
                $officers[5] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'svpp') || strpos($word, 'senior vice president of people')) {
                if (isset($officers[6])) {
                    continue;
                }
                $officers[6] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'gen\'l counsel') || strpos($word, 'general counsel')) {
                if (isset($officers[7])) {
                    continue;
                }
                $officers[7] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'chief technology officer') || strpos($word, 'cto')) {
                if (isset($officers[8])) {
                    continue;
                }
                $officers[8] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'chief information officer') || strpos($word, 'cio')) {
                if (isset($officers[9])) {
                    continue;
                }
                $officers[9] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'retail')) {
                if (isset($officers[10])) {
                    continue;
                }
                $officers[10] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'principal accounting officer') || strpos($word, 'pao')) {
                if (isset($officers[11])) {
                    continue;
                }
                $officers[11] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'secretary')) {
                if (isset($officers[12])) {
                    continue;
                }
                $officers[12] = $item;
                unset($items[$key]);
                continue;
            }
            if (strpos($word, 'senior vice president of worldwide marketing')) {
                if (isset($officers[13])) {
                    continue;
                }
                $officers[13] = $item;
                unset($items[$key]);
                continue;
            }
        }
        ksort($officers);
        return array_merge(array_values($officers), array_values($items));
    }


    public function getXSDUrl($url, $symbol)
    {
        $file = file_get_contents($url);
        $pos = strpos($file, '.xsd');
        return $url . strtolower($symbol) . '-' . substr($file, $pos - 8, 12);
    }

    public function httpRequest($url)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        // return the transfer as a string
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // $response contains the output string
        $response = curl_exec($curl);

        // close curl resource to free up system resources
        curl_close($curl);
        return $response;
    }

    public function get10KQReportsMetaLinks($directory)
    {
        $url = $directory['directory'] . 'MetaLinks.json';
        $metaLinksJsonObj = $this->httpRequest($url);
        return json_decode($metaLinksJsonObj, TRUE)['instance'][$directory['endpoint']]['report'];
    }

    public function get10KQReports($directory, $statementType, $metaLinksReportsArray = null): array
    {
        try {
            if (is_null($metaLinksReportsArray))
                $metaLinksReportsArray = $this->get10KQReportsMetaLinks($directory);
            foreach ($metaLinksReportsArray as $endpoint => $metaReport) {
                $sortName = strtolower($metaReport['shortName']);
                if (strpos($sortName, strtolower($statementType)) !== false) {
                    try {
                        $url = $directory['directory'] . $endpoint . '.htm';
                        Log::debug($url);

                        $rowHtml = $this->httpRequest($url);
                        $client = new Crawler($rowHtml);
                        $table = $client->filter('.report')->outerHtml();
                        $data[] = [
                            'title' => ucfirst($sortName),
                            'html' => $table,
                        ];
                    } catch (\Exception $e) {
                        \Log::error($e->getMessage());
                    }
                }
            }
            return $data ?? [];
        } catch (\Exception $e) {
            return $data ?? [];
        }
    }

    public function getReports($cik, array $searchStrings, $formType = '10-K'): array
    {

        try {
            $response = $this->getFormInfo($cik, $formType, true);
            $url = $response['directoryUrl'] . "FilingSummary.xml";
            $xmlString = $this->httpRequest($url);
            $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $reports = json_decode($json, TRUE);
            foreach ($reports["MyReports"]["Report"] as $report) {
                $sortName = strtolower($report['ShortName']);
                foreach ($searchStrings as $searchString) {
                    if (strpos($sortName, strtolower($searchString)) !== false) {
                        try {
                            $url = $response['directoryUrl'] . $report['HtmlFileName'];
                            $rowHtml = $this->httpRequest($url);
                            $client = new Crawler($rowHtml);
                            $table = $client->filter('.report')->outerHtml();
                            $data[] = [
                                'title' => ucfirst($sortName),
                                'html' => $table,
                            ];
                        } catch (\Exception $e) {
                            \Log::error($e->getMessage());
                        }
                    }
                }
            }
            return $data ?? [];
        } catch (\Exception $e) {
            return $data ?? [];
        }
    }

    function XML2Array(SimpleXMLElement $parent)
    {
        $array = array();
        foreach ($parent as $name => $element) {
            ($node = &$array[$name])
            && (1 === count($node) ? $node = array($node) : 1)
            && $node = &$node[];
            $node = $element->count() ? $this->XML2Array($element) : trim($element);
        }
        return $array;
    }

    function personEntityInfo($cik)
    {
        try {
            $html = file_get_contents("https://www.sec.gov/cgi-bin/own-disp?action=getowner&CIK=$cik");
            $strPos = strpos($html, "<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\">");
            $html = substr($html, $strPos);
            $strPos = strpos($html, "<br><br><b>Items 1") - 38;
            $htmlContent = substr($html, 0, $strPos);

            $DOM = new DOMDocument();
            $DOM->loadHTML($htmlContent);

            $Detail = $DOM->getElementsByTagName('td');

            $arrayData = [];
            //#Get row data/detail table without header name as key
            $i = 0;
            foreach ($Detail as $index => $sNodeDetail) {
                if ($index <= 3) {
                    continue;
                }
                if ($index % 4 == 0) {
                    $issuer = strtolower(trim($Detail->item($index + 0)->textContent));
                    $is_current = Str::contains($issuer, 'current');
                    $issuer = Str::after($issuer, 'name:');
                    $arrayData[$i]['issuer'] = Str::ucfirst($issuer);
                    $arrayData[$i]['cik'] = trim($Detail->item($index + 1)->textContent);
                    $arrayData[$i]['transaction_date'] = formatDate(trim($Detail->item($index + 2)->textContent));
                    $arrayData[$i]['ownership'] = ucfirst(trim($Detail->item($index + 3)->textContent));
                    $arrayData[$i]['is_current'] = $is_current;
                    $i++;
                }
            }
            return ['array' => $arrayData, 'html' => $htmlContent];
        } catch (\Exception $e) {
            return ['array' => [], 'html' => null];
        }
    }

    /**
     * barChartData = {
     * labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
     * datasets: [{
     * label: 'Dataset 1',
     * backgroundColor: window.chartColors.red,
     * data: [
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor()
     * ]
     * }, {
     * label: 'Dataset 2',
     * backgroundColor: window.chartColors.blue,
     * data: [
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor()
     * ]
     * }, {
     * label: 'Dataset 3',
     * backgroundColor: window.chartColors.green,
     * data: [
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor(),
     * randomScalingFactor()
     * ]
     * }]
     *
     * };
     * @param $cik
     * @return array
     */

    public function getAllFillingsList($cik)
    {
        $labels = [];
        $i = 0;
        while ($i <= 10000) {
            $res = self::getFillingsInstance($cik, $i);
            if (is_null($res)) {
                break;
            }
            foreach ($res['arrayData'] as $index => $item) {
                $date = formatDate($item['date'], 'Y-m-d');
                $value = $item['number_of_securities_transacted'];

                if (is_null($date)) {
                    continue;
                }
                if (in_array($date, $labels)) {
                    continue;
                }
                $labels[] = $date;

                if (strtolower($item['acquisition_or_disposition']) == 'a') {
                    $acquisitionData[] = (double)$value;
                    // $dispositionData[] = 0;
                } else {
                    $dispositionData[] = (double)$value;
                    // $acquisitionData[] = 0;
                }
            }
            $i = $i + 80;
        }
        return [
            'labels' => $labels ?? [],
            'datasets' => [
                [
                    "label" => 'Acquisition',
                    "backgroundColor" => '#141d2d', // getRGBColorString(),
                    "data" => $acquisitionData ?? []
                ],
                [
                    "label" => 'Disposition',
                    "backgroundColor" => 'rgba(54, 162, 235, 0.2)', // getRGBColorString(),
                    "data" => $dispositionData ?? []
                ]
            ]
        ];
    }

    protected
    function getFillingsInstance($cik, $start = 0)
    {
        $htmlDom = self::getHtmlDom($cik, $start);
        if (is_null($htmlDom)) {
            return null;
        }
        $DOM = $htmlDom['dom'];
        $htmlContent = $htmlDom['html'];
        if (is_null($DOM) || is_null($htmlContent)) {
            return null;
        }
        $Detail = $DOM->getElementsByTagName('td');
        $titles = $DOM->getElementsByTagName('th');
        $header_titles = [];

        foreach ($titles as $idx => $title) {
            if ($idx % 12 == 0) {
                for ($column = 0; $column <= 11; $column++) {
                    $header_titles[] = trim($titles->item($idx + $column)->textContent);
                }
            }
        }
        $arrayData = [];
        $i = 0;
        foreach ($Detail as $index => $sNodeDetail) {
            if ($index % 12 == 0) {
                $arrayData[$i]['acquisition_or_disposition'] = self::trimFillingDetailCols($Detail, $index, 0);
                $arrayData[$i]['date'] = self::trimFillingDetailCols($Detail, $index, 1);
                $arrayData[$i]['deemed_execution_date'] = self::trimFillingDetailCols($Detail, $index, 2);
                $arrayData[$i]['issuer'] = self::trimFillingDetailCols($Detail, $index, 3);
                $arrayData[$i]['form'] = self::trimFillingDetailCols($Detail, $index, 4);
                $arrayData[$i]['form_url'] = self::getAttributeValue($Detail, $index, 4);
                $arrayData[$i]['transaction_type'] = self::trimFillingDetailCols($Detail, $index, 5);
                $arrayData[$i]['direct_or_indirect_ownership'] = self::trimFillingDetailCols($Detail, $index, 6);
                $arrayData[$i]['number_of_securities_transacted'] = self::trimFillingDetailCols($Detail, $index, 7);
                $arrayData[$i]['number_of_securities_owned'] = self::trimFillingDetailCols($Detail, $index, 8);
                $arrayData[$i]['line_number'] = self::trimFillingDetailCols($Detail, $index, 9);
                $arrayData[$i]['issuer_cik'] = self::trimFillingDetailCols($Detail, $index, 10);
                $arrayData[$i]['security_name'] = self::trimFillingDetailCols($Detail, $index, 11);
                $i++;
            }
        }
        return compact('htmlContent', 'arrayData', 'header_titles');
    }

    public function getFillingsList($cik, $start = 0)
    {
        try {
            $res = self::getFillingsInstance($cik, $start);
            $arrayData = $res['arrayData'];
            $header_titles = $res['header_titles'];
            $htmlContent = $res['htmlContent'];

            $have_next = self::haveNext($cik, count($arrayData) + $start);
            $start = $have_next ? count($arrayData) + $start : 0;
            return [
                'header_titles' => $header_titles,
                'array' => $arrayData ?? [],
                'html' => $htmlContent,
                'have_next' => $have_next,
                'start' => $start
            ];
        } catch (\Exception $e) {
            return null;
        }
    }

    private
    function haveNext($cik, $start)
    {
        $htmlDom = self::getHtmlDom($cik, $start);
        $DOM = $htmlDom['dom'];
        $Detail = $DOM->getElementsByTagName('td');
        return ($Detail->count() > 0);
    }

    private
    function trimFillingDetailCols($Detail, $index, $column)
    {
        try {
            if ($column == 1 || $column == 2) {
                $date = trim($Detail->item($index + $column)->textContent);
                return formatDate($date);
            }
        } catch (\Exception $e) {
            return null;
        }
        return ucfirst(trim($Detail->item($index + $column)->textContent));
    }

    private
    function getAttributeValue($Detail, $index, $column)
    {
        try {
            $page_url = 'https://www.sec.gov' . $Detail->item($index + $column)->getElementsByTagName('a')->item(0)->getAttribute('href');
            $client = new Client();
            return 'https://www.sec.gov' . $client->request('GET', $page_url)->filter(".tableFile > tr > td > a")->attr('href');
        } catch (\Exception $e) {
            return null;
        }
    }

    private
    function getHtmlDom($cik, $start = 0)
    {
        try {
            $rawContent = file_get_contents("https://www.sec.gov/cgi-bin/own-disp?action=getowner&CIK=$cik&start=$start");
            $strPos = strpos($rawContent, "<table border=\"0\" cellspacing=\"0\" id=\"transaction-report\">");
            $rawContent = substr($rawContent, $strPos);
            $strPos = strpos($rawContent, "<form> <input");
            $html = substr($rawContent, 0, $strPos);
            if (empty($html)) {
                return null;
            }
            $DOM = new DOMDocument();
            $DOM->loadHTML($html);
            return ['dom' => $DOM, 'html' => $html];
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getFillings($cik, $limit = "", $type = "", $date = "", $owner = "")
    {
        try {
            $url = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=" . $cik . "&type=" . $type . "&dateb=" . $date . "&owner=" . $owner . "&start=0&count=" . $limit . "&output=atom";
            $xmlString = file_get_contents($url);
            $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            return json_decode($json, TRUE);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getFillingsByUrl($url)
    {
        $xmlString = file_get_contents($url);
        $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        return json_decode($json, TRUE);
    }

    public function getSegmentInfo($symbol, $period)
    {
        $form_type = ($period == 'quarter') ? '10-Q' : '10-K';
        $cik = session('profile')['cik'];
        $url = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=$cik&type=$form_type%25&dateb=&owner=exclude&start=0&count=10&output=atom";
        $xmlString = file_get_contents($url);
        $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        return json_decode($json, TRUE);
    }

    public function getForm10($symbol, $period, $limit = 10)
    {

        try {
            $form_type = ($period == 'quarter') ? '10-Q' : '10-K';
            $cik = session('profile')['cik'];
            $url = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=$cik&type=$form_type%25&dateb=&owner=exclude&start=0&count=$limit&output=atom";
            $xmlString = Http::get($url);
            $xmlString = $xmlString->body();
            $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $res = json_decode($json, TRUE);
            $roughUrl = $res['entry'][0]['content']['filing-href'] ?? null;
            if (is_null($roughUrl)) {
                return [];
            }
            $arr = explode('/', $roughUrl);
            $res['directory_url'] = str_replace(end($arr), '', $roughUrl);
            $res['company-info']['symbol'] = strtolower($symbol);
            $res['company-info']['period'] = $period;
            $res['xsd_url'] = self::getXSDUrl($res['directory_url'], $symbol);
            return $res;
        } catch (\Exception $e) {
            $this->attempts++;
            if ($this->attempts == 3)
                return [];
            self::getForm10($symbol, $period, $limit = 10);
        }
        return [];
    }

    /**
     * @param $query
     * @return array|string
     */
    public function searchProfile(string $query)
    {
        try {
            $response = Http::post('https://efts.sec.gov/LATEST/search-index', ['keysTyped' => $query]);
            $response = $response->json();
            $data = [];
            foreach ($response['hits']['hits'] as $index => $item) {
                /*if (!isset($item['_source']['tickers'])) {
                    unset($response['hits']['hits'][$index]);
                    continue;
                }*/
                if (isset($item['_source']['tickers'])) {
                    $garbage = '(' . $item['_source']['tickers'] . ')';
                    $name = trim(str_replace($garbage, '', $item['_source']['entity']));
                    foreach (explode(',', $item['_source']['tickers']) as $symbol) {
                        $data[] = array_merge($item, [
                            "symbol" => trim($symbol),
                            "cik" => $item['_id'],
                            "name" => $name ?? null,
                            "currency" => "USD",
                            "stockExchange" => null,
                            "exchangeShortName" => null,
                        ]);
                    }
                } else {
                    $data[] = array_merge($item, [
                        "symbol" => null,
                        "cik" => $item['_id'],
                        "name" => $item['_source']['entity'],
                        "currency" => "USD",
                        "stockExchange" => null,
                        "exchangeShortName" => null,
                    ]);
                }
            }
            return $data;
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getProfile($query)
    {
        return self::searchProfile($query)[0] ?? [];
    }

    public function getSicData()
    {
        $client = new Client();
        $request = $client->request('GET', 'https://www.sec.gov/info/edgar/siccodes.htm');
        $data = $request->filter('.sic > tbody > tr')->each(function (Crawler $node) {
            $item = $node->filter('td');
            if ($item->count() > 0) {
                return [
                    'sic' => $item->eq(0)->text(),
                    'office' => $item->eq(1)->text(),
                    'title' => $item->eq(2)->text()
                ];
            }
        });
        array_shift($data);
        return $data;
    }

    public function getFormInfo($cik, $formType, $onlyFirst = false): array
    {
        $cik = sprintf("%010d", $cik);
        $json = $this->httpRequest("https://data.sec.gov/submissions/CIK$cik.json");
        $array = json_decode($json);
        foreach ($array->filings->recent->form as $index => $form)
            if ($form == strtoupper($formType)) {
                $accessionNumber = $array->filings->recent->accessionNumber[$index];
                $directoryNumber = str_replace('-', '', $accessionNumber);
                $primaryDocName = $array->filings->recent->primaryDocument[$index];
                $item = [
                    'filingDate' => $array->filings->recent->accessionNumber[$index],
                    'accessionNumber' => $accessionNumber,
                    'directoryNumber' => $directoryNumber,
                    'reportDate' => $array->filings->recent->reportDate[$index],
                    'acceptanceDateTime' => $array->filings->recent->acceptanceDateTime[$index],
                    'act' => $array->filings->recent->act[$index],
                    'form' => $form,
                    'fileNumber' => $array->filings->recent->fileNumber[$index],
                    'filmNumber' => $array->filings->recent->filmNumber[$index],
                    'primaryDocument' => $primaryDocName,
                    'primaryDocDescription' => $array->filings->recent->primaryDocDescription[$index],
                    'directoryUrl' => "https://www.sec.gov/Archives/edgar/data/$cik/$directoryNumber/",
                    'primaryDocUrl' => "https://www.sec.gov/Archives/edgar/data/$cik/$directoryNumber/$primaryDocName"
                ];
                if ($onlyFirst)
                    return $item;
                $items[] = $item;
            }
        return $items ?? [];
    }
}
