<?php
/**
 * Created by PhpStorm.
 * User: ombharti
 * Date: 1/2/2018
 * Time: 5:03 PM
 */

namespace App\Services;

use App\Interfaces\EmailServiceInterface;
use App\Models\EmailTemplate;
use Illuminate\Mail\Mailer;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class EmailService implements EmailServiceInterface
{
    /**
     * @param $mail_params_array It will an array with basically including parameters
     *                           to(required), from(optional), from_names(optional), subject(optional), attachments(optional)
     * @param $template          Template name
     * @param $dynamic_data      Keys replacement value, it will refer template keys (dynamic variable)
     * @return mixed
     * @example call to this function
     *                           $emailServiceProvider->sendEmail(
     *                           array('to' => 'meetmalay@gmail.com', 'from' => 'meetmalay@gmail.com',
     *                           'from_name'=>'Spandooly', subject' => 'test message',
     *                           'attachments' => array('1.jpg', 'test.text'),
     *                           'headers' => array('X-MC-PreserveRecipients' => 'false')),
     *                           TEST_TEMPLATE_NAME,
     *                           array('NAME' => 'my name', 'EMAIL' => 'My Mail')
     *                           );
     */
    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 24;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail($mail_params_array, $template = null, $dynamic_data = [])
    {

        $to = $mail_params_array['to'];
        $result = null;
        if (is_array($to)) {
            foreach ($mail_params_array['to'] as $user_email) {
                $result = self::sendEmailToUser($user_email, $mail_params_array, $dynamic_data, $template);
            }
        } else {
            $result = self::sendEmailToUser($to, $mail_params_array, $dynamic_data, $template);
        }
        return $result;
    }


    public function sendMail($content_data, $to, $from, $from_name, $subject)
    {
        Mail::send('admin.emails.forgot-password-template', compact('content_data'), function ($message) use ($to, $from, $from_name, $subject) {
            $message->from($from, $from_name)
                ->to($to)
                ->subject($subject);

            if (isset($headers) && !empty($headers)) {
                $headers = $message->getHeaders();
                foreach ($headers as $key => $val) {
                    $headers->addTextHeader($key, $val);
                }
            }
            $headers = $message->getHeaders();
            $headers->addTextHeader('X-MC-PreserveRecipients', 'false');
        });
    }


    public function sendEmailToUser($to, $mail_params_array, $dynamic_data, $template = null)
    {
        $from = env('MAIL_FROM_ADDRESS');
        $from_name = env('APP_NAME');

        $template_variables = Config::get('emailvariables.' . $template);
        $replace_array = $key_array = [];
        if (!empty($template_variables)) {
            foreach ($template_variables as $key => $template_variable) {
                $key_array[] = "#" . $key . "#";
                $replace_array[] = (isset($dynamic_data[trim($key, '#')])) ? $dynamic_data[trim($key, '#')] : '';
            }
        }
        // Email Template content get and replace variable with user value
        $email_data = EmailTemplate::query()->where('action', $template)->first();

        $subject = isset($mail_params_array['subject']) ? $mail_params_array['subject'] : $email_data->subject;
        $subject = str_replace($key_array, $replace_array, $subject);
        $content = str_replace($key_array, $replace_array, $email_data->content);
        $content_data = ['email_content' => $content];
        try {
            Mail::send('emails.sample', $content_data, function ($message) use ($to, $from, $from_name, $subject) {
                $message->from($from, $from_name)
                    ->to($to)
                    ->subject($subject);

                if (isset($headers) && !empty($headers)) {
                    $headers = $message->getHeaders();
                    foreach ($headers as $key => $val) {
                        $headers->addTextHeader($key, $val);
                    }
                }
                $headers = $message->getHeaders();
                $headers->addTextHeader('X-MC-PreserveRecipients', 'false');
                $headers->addTextHeader('X-No-Track', \Illuminate\Support\Str::random(10));
            });
        } catch (\Exception $e) {

            return $e->getMessage();
        }
        return Mail::failures();
    }

}
