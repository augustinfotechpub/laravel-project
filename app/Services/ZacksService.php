<?php


namespace App\Services;


use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class ZacksService
{
    public $client, $dates = [], $rows = [], $keys = [];
    protected const BASE_URL = "https://nt3-s.zacks.com/";

    public function __construct()
    {
        $this->client = new Client();
    }

// https://nt3-s.zacks.com/fundamreports/Default.aspx?tbTicker=WFC&ddBasic=A&ddPeriod=10&ddFormType=0&ddReportFormat=S&ddUpdateType=R

    protected function getIntFormType($formType): int
    {
        switch ($formType) {
            case 'incomeStatement':
                return 0;
            case 'balanceSheet':
                return 1;
            case 'cashFlow':
                return 2;
        }
    }

    /**
     * @param string $symbol Company Ticker/Symbol
     * @param string $period Period Type one of Q,q,Quarter,quarter,quarterly,FY,fy,Annual,annual,annually,A
     * @param string $formType Form type could be 0:Income Statement, 1:Balance Sheet, 2: Cash Flow, 3:Segment info, 4: General, 5: Metrics
     */
    public function getFinancials(string $symbol, string $period, string $formType = 'incomeStatement', $limit = 10)
    {
        $this->rows = [];
        $this->dates = [];
        $params = [
            'tbTicker' => strtoupper($symbol),
            // 'ddBasic' => $this->getPeriod($period),
            'ddPeriod' => $limit,
            'ddFormType' => $this->getIntFormType($formType),
            'ddReportFormat' => 'S',
            'ddUpdateType' => 'R'
        ];
        // make a real request to an external site
        $client = new Client();
        $crawler = $client->request('GET', 'https://nt3-s.zacks.com/fundamreports/Default.aspx');

        $html = $crawler->outerHtml();
        $html = str_replace('onClick', 'fksdlfjkl', $html);
        $crawler = new Crawler($html, $crawler->getUri());
        $form = $crawler->selectButton('Update report')->form($params);
        // Disable validation for the whole form
        $form->disableValidation();
        // select an option
        // $form['ddBasic']->select('A');
        // submit that form
        $crawler = $client->submit($form);
        $htmlTable = $crawler->filter('.report')->eq(0);

        $htmlTable->filter('tr')->each(function (Crawler $tableRowNode, $tableRowNodeIndex) use ($formType) {
            if ($tableRowNodeIndex == 1)
                $tableRowNode->filter('th')->each(function ($tableHeaderNode, $j) {
                    if ($j > 0)
                        $this->dates[] = $tableHeaderNode->text();
                });
        });
        $searchMapArray = self::getSearchMapArray($formType);
        foreach ($this->dates as $dateIndex => $date) {
            $htmlTable->filter('tr')->each(function (Crawler $tableRowNode, $tableRowNodeIndex) use ($searchMapArray, $date, $dateIndex) {
                if ($tableRowNodeIndex > 1) {
                    /*return $tableRowNode->filter('td')->each(function (Crawler $node) {
                        return $node->text();
                    });*/
                    $tableDataNode = $tableRowNode->filter('td');
                    $tdTitleText = $tableDataNode->eq(0)->outerHtml();
                    foreach ($searchMapArray as $searchMapIndex => $searchMapArr) {
                        if (strpos($tdTitleText, $searchMapArr['title'])) {
                            try {
                                $entry = $this->filterEntry($tableDataNode->eq($dateIndex + 1)->text());
                                if (!is_null($entry))
                                    $this->rows[$date][$searchMapArr['column']] = $entry;
                            } catch (\Exception $e) {

                            }
                        }
                    }
                }
            });
        }
        return $this->rows;
    }

    protected function filterEntry($value)
    {
        try {
            if ($value == '--')
                return null;
            $nValue = str_replace(',', '', $value);
            $nValue = str_replace('(', '', $nValue);
            $nValue = str_replace(')', '', $nValue);
            return $nValue * 1000;
        } catch (\Exception $e) {
            dd($e->getMessage(), $value);
        }
    }

    protected function getSearchMapArray($formType)
    {
        return array_merge(self::MAP_STRINGS['bank']['statementsEntryQueries'][$formType],
            self::MAP_STRINGS['insurance']['statementsEntryQueries'][$formType]);
    }

    protected function getPeriod($period): string
    {
        $period = strtolower($period);
        switch ($period) {
            case 'q':
            case 'quarter':
            case 'quarterly':
                return 'Q';
            default :
                return 'A';
        }
    }

    protected const MAP_STRINGS = [
        'bank' => [
            'sics' => [
                'from' => 6021,
                'to' => 6036,
            ],
            'statementsEntryQueries' => [
                'incomeStatement' => [
                    ['column' => 'interestIncomeOnLoans', 'title' => 'Interest Income on Loans', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestIncomeOnInvestment', 'title' => 'Interest Income on Investment', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestIncomeOnSecurities', 'title' => 'Interest Income on Securities', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestIncomeOnDepositsInBanks', 'title' => 'Interest Income on Deposits in Banks', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'federalFundsSoldAndSecuritiesPurchasedUnderResaleAgreements', 'title' => 'Federal Funds Sold and Securities Purchased under Resale Agreements', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestIncomeFromOthers', 'title' => 'Interest Income from Others', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'interestOnDeposits', 'title' => 'Interest on Deposits', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestOnDebt', 'title' => 'Interest on Debt', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'federalFundsPurchasedAndSecuritiesSoldUnderResaleAgreements', 'title' => 'Federal Funds Purchased and Securities Sold under Resale Agreements', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestOnOtherBorrowedFunds', 'title' => 'Interest on other Borrowed funds', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'totalInterestExpense', 'title' => 'Total Interest Expense', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'provisionForLoanOrCreditLosses', 'title' => 'Provision For Loan/Credit Losses', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'netInterestIncomeAfterLoanLossProvision', 'title' => 'Net Interest Income After Loan Loss Provision', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'mortgageBankingActivities', 'title' => 'Mortgage Banking Activities', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'cardFees', 'title' => 'Card Fees', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'federalDepositInsurance', 'title' => 'Federal Deposit Insurance', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                ],
                'balanceSheet' => [
                    ['column' => 'cashAndCashEquivalentsAndDueFromBanks', 'title' => 'Cash and Cash Equivalents & Due from Banks', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'interestBearingDeposits', 'title' => 'Interest Bearing Deposits', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'moneyMarketInvestments', 'title' => 'Money Market Investments', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'shortTermInvestmentsBanks', 'title' => 'Short-Term Investments - Banks', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'federalFundsSold', 'title' => 'Federal Funds Sold', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'federalFundsSoldSecuritiesPurchasedUnderResaleAgreements', 'title' => 'Federal Funds Sold & Securities Purchased Under Resale Agreements', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'accruedInterestReceivable', 'title' => 'Accrued Interest Receivable', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'rightOfUseAssets - Current', 'title' => 'Right of Use Assets - Current', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'mortgageBackedSecurities', 'title' => 'Mortgage Backed Securities', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'municipalBondsAndGovernmentSecurities', 'title' => 'Municipal Bonds and Government Securities', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'investmentsInFedHomeLoanBank', 'title' => 'Investments in FedHomeLoanBank', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'investmentInRealEstate', 'title' => 'Investment in Real Estate', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'investmentOthers', 'title' => 'Investment - Others', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'loansHeldForSale', 'title' => 'Loans Held for Sale', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'grossLoansMade', 'title' => 'Gross Loans Made', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'loanLossAllowance', 'title' => 'Loan Loss Allowance', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'otherSubtractionsFromLoans', 'title' => 'Other Subtractions from Loans', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'netLoans', 'title' => 'Net Loans', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'grossPropertyPlantAndEquipment', 'title' => 'Gross Property, Plant & Equipment', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'TotalAccumulatedDepreciationAndDepletion', 'title' => 'Total Accumulated Depreciation & Depletion', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'netPropertyPlantAndEquipment', 'title' => 'Net Property, Plant & Equipment', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'federalHomeLoanBankDebt', 'title' => 'Federal Home Loan Bank Debt', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

                    ['column' => 'nonPerformingLoans', 'title' => 'Non-Performing Loans', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                ],
                'cashFlow' => [
                    ['column' => 'changeInInterestReceivable', 'title' => 'Change in Interest Receivable', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'changeInInterestPayable', 'title' => 'Change in Interest Payable', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                    ['column' => 'changeInLoans', 'title' => 'Change in Loans', 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
                ],
            ]
        ],
        'insurance' => [
            'sics' => [
                'from' => 6311,
                'to' => 6399
            ],
            'statementsEntryQueries' => [
                'incomeStatement' => [
                    ['column' => 'grossPremiumsWritten', 'title' => 'Gross Premiums Written', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'lessIncreaseInUnearnedPremiums', 'title' => 'Less Increase In Unearned Premiums', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'lessReinsuranceCeded', 'title' => 'Less Reinsurance Ceded', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

                    ['column' => 'policyRevenuesAndBenefits', 'title' => 'Policy Revenues and Benefits', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

                    ['column' => 'BenefitsToPolicyholdersAndClaims', 'title' => 'Benefits to Policyholders and Claims', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'lessReinsuranceRecoverable', 'title' => 'Less Reinsurance Recoverable', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

                    ['column' => 'interestCreditedToContractHoldersFunds', 'title' => 'Interest Credited to Contractholders Funds', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'policyAcquisitionOrUnderwritingCosts', 'title' => 'Policy Acquisition/Underwriting Costs', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'amortizationOfDeferredPolicyAcquisitionCosts', 'title' => 'Amortization of Deferred Policy Acquisition Costs', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'DividendsPaidToPolicyholders', 'title' => 'Dividends paid to Policyholders', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

                    ['column' => 'reinsuranceIncomeOrExpense', 'title' => 'Reinsurance Income or Expense', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                ],
                'balanceSheet' => [
                    ['column' => 'realEstateOwned', 'title' => 'Real Estate Owned', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'mortgageLoans', 'title' => 'Mortgage Loans', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'policyLoans', 'title' => 'Policy Loans', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'netPremiumsReceivables', 'title' => 'Net Premiums Receivables', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'netReinsuranceReceivables', 'title' => 'Net Reinsurance Receivables', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

                    ['column' => 'policyBenefitsLossesAndReserves', 'title' => 'Policy Benefits, Losses & Reserves', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'UnearnedPremiumReserveOrInsurancePayable', 'title' => 'Unearned Premium Reserve/Insurance Payable', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                    ['column' => 'reinsurancePayable', 'title' => 'Reinsurance Payable', 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
                ],
                'cashFlow' => [

                ],
            ]
        ]
    ];
}
