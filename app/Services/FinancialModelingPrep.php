<?php


namespace App\Services;
use App\Models\eod_cash_flow;
use App\Models\eod_balance_sheet;
use App\Models\eod_income_statement;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class FinancialModelingPrep
{
    protected $api_key, $uri, $uri_v4;
    public $symbol, $limit, $period,$currentYear="2020-12-31",$previousYear=null,$previousYear2=null,$previousYear3=null,$year;

    public function __construct()
    {
        $this->api_key = config('services.financial_modeling_prep.api_key');
    }

    public function getResponse($path, $method = 'get', $query = [], $apiVersion = 'v3')
    {
        // $queryBS=eod_balance_sheet::first()->toArray();
        // print_r($queryBS['dividendShare']);
        // die();
        //  $queryIS=' '->toArray();
        //  $queryCF=' '->toArray();

        //  $returnquote['marketcap']=$queryBS = totalCurrentAssets / totalCurrentLiabilities;


        try {
            $method = strtolower($method);
            $uri = config("services.financial_modeling_prep.uri.$apiVersion");
            $query['apikey'] = config("services.financial_modeling_prep.api_key");
            switch ($method) {
                case 'get':
                    $response = Http::get($uri . $path, $query);

                    break;
                case 'post':
                    $response = Http::post($uri . $path, $query);
                    break;
                default :
                    throw new Exception('Invalid http request Method. ' . $method);
            }
            if ($response->successful()) {
                $response = $response->json();
                if (isset($ret['Error Message'])) {
                    throw new Exception($ret['Error Message']);
                }

                \Log::channel('quote_fetch')->info( json_encode($response) );
                return $response;
            }
            throw new Exception($response->serverError() . ', ' . $response->clientError());
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getKeyMetrics($symbol, $limit = 40, $period = 'quarter')
    {
        /*$path = match(strtolower($period)) {
            'ttm' => 'key-metrics-ttm/',
            default => 'key-metrics/',
        };*/

        switch (strtolower($period)) {
            case "ttm":
                $path = "key-metrics-ttm/";
                break;
            default:
                $path = "key-metrics/";
        }
        $path = $path . $symbol;
        return $this->getResponse($path, 'get', ['limit' => $limit, 'period' => $period]);
    }

    public function getKeyMetricsTTM($symbol, $limit = 10)
    {
        $path = 'key-metrics-ttm/' . $symbol;
        return $this->getResponse($path, 'get', ['limit' => $limit]);
    }

    public function getFinancialRatios($symbol, $limit = 40, $period = 'quarter')
    {
        switch (strtolower($period)) {
            case ('ttm'):
                $path = 'ratios-ttm/';
                break;
            case 'FY':
                $path = 'ratios/';
                break;
            default :
                $path = 'ratios/';
        }
        $query['limit'] = $limit;
        if ($period == 'quarter') {
            $query['period'] = $period;
        }
        $path = $path . $symbol;
        return $this->getResponse($path, 'get', $query);
    }

    public function quote($symbol)
    {
        $path = 'quote/' . $symbol;
        return $this->getResponse($path);
    }

    public function getFinancialGrowth($symbol, $limit = 80, $period = 'quarter')
    {
        $path = 'financial-growth/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        if (!is_null($period)) {
            $query['period'] = $period;
        }
        return $this->getResponse($path, 'get', $query);
    }

    /**
     * https://financialmodelingprep.com/api/v3/income-statement/{SYMBOL}?period=quarter&limit={API_KEY}
     */
    public function getFinancialStatements($symbol, $limit = 120, $period = 'quarter')
    {
        $period = strtolower($period);
        $path = 'income-statement/' . strtoupper($symbol);
        return $this->getResponse($path, 'get', ['period' => $period, 'limit' => $limit]);
    }

    private function caclAllYears($year,$period){
        $this->previousYear2= date("Y-m-d", strtotime("-2 year", strtotime($year)));
        $this->previousYear3= date("Y-m-d", strtotime("-3 year", strtotime($year)));
        $this->previousYear= date("Y-m-d", strtotime("-1 year", strtotime($year)));

        $currnetQrt = ceil( date( 'n' ,strtotime($this->previousYear2) )  / 3);
        if($period == 'quarterly'){

        switch ($currnetQrt) {
            case '1.0':
                $this->previousYear = date("Y", strtotime($this->previousYear)) . "-03-31";
                $this->previousYear2 = date("Y", strtotime($this->previousYear2)) . "-03-31";
                $this->previousYear3 = date("Y", strtotime($this->previousYear3)) . "-03-31";
                break;
            case '2.0':
                $this->previousYear = date("Y", strtotime($this->previousYear)) . "-06-30";
                $this->previousYear2 = date("Y", strtotime($this->previousYear2)) . "-06-30";
                $this->previousYear3 = date("Y", strtotime($this->previousYear3)) . "-06-30";
                break;
            case '3.0':
                $this->previousYear = date("Y", strtotime($this->previousYear)) . "-09-30";
                $this->previousYear2 = date("Y", strtotime($this->previousYear2)) . "-09-30";
                $this->previousYear3 = date("Y", strtotime($this->previousYear3)) . "-09-30";
                break;
            case '4.0':
                $this->previousYear = date("Y", strtotime($this->previousYear)) . "-12-31";
                $this->previousYear2 = date("Y", strtotime($this->previousYear2)) . "-12-31";
                $this->previousYear3 = date("Y", strtotime($this->previousYear3)) . "-12-31";
                break;
            default:
                # code...
                break;
        }
    }
    else{
                $this->previousYear = date("Y", strtotime($this->previousYear)) . "-12-31";
                $this->previousYear2 = date("Y", strtotime($this->previousYear2)) . "-12-31";
                $this->previousYear3 = date("Y", strtotime($this->previousYear3)) . "-12-31";
    }
    }

    public function getEODFinancialStatements($cik=null, $limit = 120, $period = 'yearly',$year = null)
    {
        $cik=ltrim($cik,"0");

        if( is_null($year) ){
            $date = Carbon::now();
            $d = $date->toDateString();
            $d = date("Y-m-d", strtotime("-1 year", strtotime($d)));

            $this->currentYear = $d;
        } else {
            $this->currentYear = $year;
        }

        $this->caclAllYears($this->currentYear,$period);

        $returnArr=[];
        if(!is_null($year))
            {
                if($period == 'yearly'){
                    $date = \DateTime::createFromFormat("Y-m-d", $year);
                    $year1 = $year;
                    $previousYear1 =  \DateTime::createFromFormat("Y-m-d", $this->previousYear)->format("Y");


                 
                    $CF = DB::table('eod_cash_flows')->where('period_type' ,$period)->where('cik' ,$cik)->where('period_date' ,'like',$year1.'%')->get();
                    $CF = json_decode($CF, true);
                    
                    $IS = DB::table('eod_income_statements')->where('period_type', $period)->where('cik' ,$cik)->where('period_date', 'like',$year1.'%')->get();
                    $IS = json_decode($IS, true);
                    
                    $BS = DB::table('eod_balance_sheets')->where('period_type', $period)->where('cik' ,$cik)->where('period_date' , 'like',$year1.'%')->get();
                    $BS = json_decode($BS, true);
                    $BSP = DB::table('eod_balance_sheets')->where('period_type' ,$period)->where('cik' ,$cik)->where('period_date' , 'like',$previousYear1.'%')->get();
                    $BSP = json_decode($BSP, true);
    
                }
                else{

                    if( $year == '2020-12-31' ){
                        $CF = DB::table('eod_cash_flows')->where('period_type' ,$period)->where('cik' ,$cik)->orderBy('period_date', 'DESC')->get();
                        $CF = json_decode($CF, true);
                        
                        $IS = DB::table('eod_income_statements')->where('period_type', $period)->where('cik' ,$cik)->orderBy('period_date', 'DESC')->get();
                        $IS = json_decode($IS, true);
                        
                        $BS = DB::table('eod_balance_sheets')->where('period_type', $period)->where('cik' ,$cik)->orderBy('period_date', 'DESC')->get();
                        $BS = json_decode($BS, true);
                        $BSP = DB::table('eod_balance_sheets')->where('period_type' ,$period)->where('cik' ,$cik)->where('period_date' , $this->previousYear)->get();
                        $BSP = json_decode($BSP, true);   
                    } else {

                        $CF = DB::table('eod_cash_flows')->where('period_type' ,$period)->where('cik' ,$cik)->where('period_date' ,'like',$year.'%')->get();
                        $CF = json_decode($CF, true);
                        
                        $IS = DB::table('eod_income_statements')->where('period_type', $period)->where('cik' ,$cik)->where('period_date', 'like',$year.'%')->get();
                        $IS = json_decode($IS, true);
                        
                        $BS = DB::table('eod_balance_sheets')->where('period_type', $period)->where('cik' ,$cik)->where('period_date' , 'like',$year.'%')->get();
                        $BS = json_decode($BS, true);
                        $BSP = DB::table('eod_balance_sheets')->where('period_type' ,$period)->where('cik' ,$cik)->where('period_date' , $this->previousYear)->get();
                        $BSP = json_decode($BSP, true);   
                    }
                }
            }   

        $returnArr['IS']=@$IS[0];
        $returnArr['BS']=@$BS[0];
        $returnArr['CF']=@$CF[0];
        $returnArr['BSP']=@$BSP[0];


        return $returnArr;
    }

    public function get10kqDirectory($symbol, $period = 'FY'): array
    {
        $cik = $this->getCikBySymbol($symbol);
        try {
            $incomeStatement = $this->getFinancialStatements($symbol, 1, $period);
            $idx = explode('/', $incomeStatement[0]['finalLink']);
            return [
                'directory' => "https://www.sec.gov/Archives/edgar/data/" . $cik . "/" . $idx[7] . "/",
                'endpoint' => $idx[8]
            ];
        } catch (Exception $e) {
            return [];
        }
    }

    public function getStockScreener($sector, $industry, $limit = 100)
    {
        $path = 'stock-screener/';
        $query['limit'] = $limit;
        $query['country'] = 'US';
        $query['sector'] = $sector;
        $query['industry'] = $industry;
        return $this->getResponse($path, 'get', $query);
    }

    public function getFinancialStatementsGrowth($symbol, $limit = 10, $period = 'quarter')
    {
        $path = 'income-statement-growth/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        if (!is_null($period)) {
            $query['period'] = $period;
        }
        return $this->getResponse($path, 'get', $query);
    }

    public function getBalanceSheet($symbol, $limit = 10, $period = 'quarter')
    {
        $path = 'balance-sheet-statement/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        if (!is_null($period)) {
            $query['period'] = $period;
        }

        return $this->getResponse($path, 'get', $query);
    }

    public function getCashFlowStatement($symbol, $limit = 10, $period = 'quarter')
    {
        $path = 'cash-flow-statement/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        if (!is_null($period)) {
            $query['period'] = $period;
        }
        return $this->getResponse($path, 'get', $query);
    }

    public function getCompanyEnterpriseValue($symbol, $limit = 120, $period = 'quarter')
    {
        $path = 'enterprise-values/' . $symbol;
        $query = [];
        $query['limit'] = $limit;
        $query['period'] = $period;
        return $this->getResponse($path, 'get', $query);
    }

    public function getDailyHistoricalPriceFull($symbol)
    {
        $path = 'historical-price-full/' . $symbol;
        return $this->getResponse($path);
    }

    public function getDailyHistoricalStockSplit($symbol)
    {
        $path = 'historical-price-full/stock_split/' . $symbol;
        return $this->getResponse($path);
    }

    public function getFinancialStatementAsFullReported($symbol, $period = null)
    {
        $symbol = strtoupper($symbol);
        $path = 'financial-statement-full-as-reported/' . $symbol;
        return $this->getResponse($path, 'get', ['period' => $period]);
    }

    public function getEarningCallTranscript($symbol, $quarter = 2, $year = 2021)
    {
        $path = 'earning_call_transcript/' . $symbol;
        $query['quarter'] = $quarter;
        $query['year'] = $year;
//        $data = $this->>getResponse($path,'get',$query);

        return $this->getResponse($path, 'get', $query);
    }

    /**
     * Historical Chart API
     * @param $symbol
     * @param string $interval = 1min,5min,15min,30min,1hour,4hour
     * @return array|mixed
     * @throws Exception
     */
    public function stockHistoricalPrice($symbol, $interval = '5min')
    {
        $path = 'historical-chart/' . $interval . '/' . $symbol;
        return $this->getResponse($path);
    }

    /**
     * Historical Daily Prices with change and volume
     */
    public function getHistoricalDailyPrice($symbol)
    {
        $path = 'historical-price-full/' . $symbol;
        return $this->getResponse($path, ['serietype' => 'line']);
    }

    /**
     * Historical Daily Prices with change and volume interval
     * /api/v3/historical-price-full/AAPL?from=2018-03-12&to=2019-03-12
     */
    public function getHistoricalDailyPriceAndVolumeInterval($symbol, $from, $to)
    {
        $path = 'historical-price-full/' . $symbol;
        return $this->getResponse($path, ['from' => $from, 'to' => $to]);
    }

    /**
     * Historical Dividends
     * /api/v3/historical-price-full/stock_dividend/
     */
    public function getHistoricalDividends($symbol)
    {
        $path = 'stock_dividend/' . $symbol;
        return $this->getResponse($path);
    }

    /**
     * Get Company Outlook
     * https://financialmodelingprep.com/api/v4/company-outlook?symbol=AAPL&apikey=???
     * @param $symbol
     * @return array|mixed|string
     */
    public function getCompanyOutlook($symbol)
    {
        $path = 'company-outlook';
        return $this->getResponse($path, 'get', ['symbol' => $symbol], 'v4');
    }

    /**
     * Historical Daily Prices with change and volume Time series
     * /api/v3/historical-price-full/AAPL?timeseries=5
     * @param $symbol
     * @param int $timeSeries
     * @return array|mixed
     * @throws Exception
     */
    public function getHistoricalDailyPricesChangeAndVolumeTimeSeries($symbol, $timeSeries = 5)
    {
        $path = 'historical-price-full/' . $symbol;
        return $this->getResponse($path, ['timeseries' => $timeSeries]);
    }

    /**
     * Batch Request Stock Daily Historical price
     * /api/v3/historical-price-full/AAPL,GOOG,FB
     * @param array $symbol
     * @param int $timeSeries
     * @return array
     * @throws Exception
     */
    public function getBatchRequestStockDailyHistoricalPrice(array $symbol, $timeSeries = 5)
    {
        $path = 'historical-price-full/' . implode(',', $symbol);
        return $this->getResponse($path, ['timeseries' => $timeSeries]);
    }

    /**
     * Technical Indicators
     * Technical Indicators Daily
     * @param $symbol
     * @param int $period
     * Type: SMA - EMA - WMA - DEMA - TEMA - williams - RSI - ADX - standardDeviation
     * /api/v3/technical_indicator/daily/AAPL?period=10&type=ema
     * @param string $type
     * @return array|mixed|string
     * @throws Exception
     */
    public function getTechnicalIndicators($symbol, $period = 10, $type = 'ema')
    {
        $path = 'technical_indicator/daily/' . $symbol;
        return $this->getResponse($path, ['period' => $period, 'type' => $type]);
    }

    /**
     * IntraDay Indicators
     * /api/v3/technical_indicator/1min/
     * @param string $symbol
     * @param string $interval = 1min,5min,15min,30min,1hour,4hour
     * @param string $period
     * @param string $type = SMA - EMA - WMA - DEMA - TEMA - williams - RSI - ADX - standardDeviation
     * @return array|mixed|string
     */
    public function getIntraDayIndicators(string $symbol, string $interval, $period = '1min', $type = 'ema')
    {
        $path = 'technical_indicator/' . $interval . '/' . $symbol;
        return $this->getResponse($path, ['period' => $period, 'type' => $type]);
    }

    public function getProfile($symbol)
    {
        $path = 'profile/' . $symbol;
        return $this->getResponse($path);
    }

    public function getFillings($symbol, $limit = "", $type = "", $date = "", $owner = "")
    {
        $cik = self::getCikBySymbol($symbol);
        $url = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK=" . $cik . "&type=" . $type . "&dateb=" . $date . "&owner=" . $owner . "&start=0&count=" . $limit . "&output=atom";
        $xmlstring = file_get_contents($url);
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        if (isset($array['entry'])) {
            foreach ($array['entry'] as $content) {
                $fillings[] = $content['content'];
            }
        }
        return $fillings ?? [];
    }

    public function getEconomicCalender($from = null, $to = null)
    {
        if (is_null($from) || is_null($to)) {
            $from = Carbon::today()->subDays(30)->format('Y-m-d');
            $to = Carbon::today()->format('Y-m-d');
        } else {
            $from = Carbon::parse($from)->format('Y-m-d');
            $to = Carbon::parse($to)->format('Y-m-d');
        }
        $path = 'economic_calendar';
        return $this->getResponse($path, 'get', ['from' => $from, 'to' => $to]);
    }

    public function getStockDividendCalendar($from = null, $to = null)
    {
        if (is_null($from) || is_null($to)) {
            $from = Carbon::today()->subDays(10)->format('Y-m-d');
            $to = Carbon::today()->format('Y-m-d');
        } else {
            $from = Carbon::parse($from)->format('Y-m-d');
            $to = Carbon::parse($to)->format('Y-m-d');
        }
        $path = 'stock_dividend_calendar';
        return $this->getResponse($path, 'get', ['from' => $from, 'to' => $to]);
    }

    public function getStockSplitCalendar($from = null, $to = null)
    {
        if (is_null($from) || is_null($to)) {
            $from = Carbon::today()->subDays(30)->format('Y-m-d');
            $to = Carbon::today()->format('Y-m-d');
        } else {
            $from = Carbon::parse($from)->format('Y-m-d');
            $to = Carbon::parse($to)->format('Y-m-d');
        }
        $path = 'stock_split_calendar';
        return $this->getResponse($path, 'get', ['from' => $from, 'to' => $to]);
    }

    public function getIPOCalendar($from = null, $to = null)
    {
        if (is_null($from) || is_null($to)) {
            $from = Carbon::today()->subDays(30)->format('Y-m-d');
            $to = Carbon::today()->format('Y-m-d');
        } else {
            $from = Carbon::parse($from)->format('Y-m-d');
            $to = Carbon::parse($to)->format('Y-m-d');
        }
        $path = 'ipo_calendar';
        return $this->getResponse($path, 'get', ['from' => $from, 'to' => $to]);
    }

    public function getCompanyHistoricalEarningCalendar($symbol, $from = null, $to = null, $limit = 10)
    {
        if (is_null($from) || is_null($to)) {
            $from = Carbon::today()->subDays(30)->format('Y-m-d');
            $to = Carbon::today()->format('Y-m-d');
        } else {
            $from = Carbon::parse($from)->format('Y-m-d');
            $to = Carbon::parse($to)->format('Y-m-d');
        }
        $path = 'historical/earning_calendar/' . $symbol;
        return $this->getResponse($path, 'get', ['from' => $from, 'to' => $to, 'limit' => $limit]);
    }

    public function getEarningCalendar($from = null, $to = null)
    {
        $path = 'earning_calendar';
        if (is_null($from) || is_null($to)) {
            $from = Carbon::today()->subDays(5)->format('Y-m-d');
            $to = Carbon::today()->format('Y-m-d');
        } else {
            $from = Carbon::parse($from)->format('Y-m-d');
            $to = Carbon::parse($to)->format('Y-m-d');
        }
        return $this->getResponse($path, 'get', ['from' => $from, 'to' => $to]);
    }

    public function getCompanyEarnings($symbol, $limit = 50)
    {
        return $this->getResponse("historical/earning_calendar/$symbol", 'get', ['limit' => $limit]);
    }

    public function getUpcomingEarningDate($symbol)
    {
        try {
            $earnings = $this->getCompanyEarnings($symbol, 10);
            $dates = array_column($earnings, 'date');
            $date1 = Carbon::parse($dates[0]);
            $date2 = Carbon::parse($dates[1]);
            return now()->closest($date1, $date2)->toDateString();
        } catch (\Exception $e) {
            return '--';
        }
    }

    public function getKeyExecutives($symbol)
    {
        $secEdgarService = new SecEdgarService();
        $path = 'key-executives/' . $symbol;
        $rows = $this->getResponse($path);
        foreach ($rows as $index => $row) {
            $name = strtolower($row['name']);
            $name = str_replace('mr. ', '', $name);
            $name = str_replace('ms. ', '', $name);
            $name = str_replace('dr. ', '', $name);
            $person = $secEdgarService->getProfile($name);
            $rows[$index]['cik'] = $person['_id'] ?? null;
        }
        return $rows;
    }


    public function institutionalHolders($symbol)
    {
        $path = 'institutional-holder/' . $symbol;
        return $this->getResponse($path);
    }

    public function getPressRelease($symbol, $limit = 30)
    {
        $path = 'press-releases/' . $symbol;
        return $this->getResponse($path, 'get', ['limit' => $limit]);
    }

    public function getStockNews($symbol = null, $limit = 50)
    {
        if (is_null($symbol)) {
            $symbol = session('profile')['symbol'];
        }
        
        try {
            
            $res = Http::get('https://financialmodelingprep.com/api/v3/stock_news?tickers=' . strtoupper($symbol) . '&limit=50&apikey=' . $this->api_key);
            return $res->json();
        } catch (Exception $e) {
            return [];
        }

    }

    /**
     * Ticker Search
     * Search via ticker and company name:
     * Values for exchange parameter are: ETF | MUTUAL_FUND | COMMODITY | INDEX | CRYPTO | FOREX | TSX | AMEX | NASDAQ | NYSE | EURONEXT
     * /api/v3/search?query=AA&limit=10&exchange=NASDAQ
     * @param $query
     * @return array|mixed|string
     */
    public function search(string $query)
    {
        $path = 'search?';
        dd(Http::get('https://financialmodelingprep.com/api/v3/search?query=AA&limit=10&apikey=7c7f7727399cca3486e121267cfa6b0a')->json());
        return $this->getResponse($path, 'get', ['query' => $query, 'limit' => 10]);
    }

    public function getCikBySymbol($symbol)
    {
        $path = 'sec_filings/' . $symbol;
        return $this->getResponse($path, 'get', ['limit' => 1])[0]['cik'];
    }

    public function getCompanies()
    {
        try {
            $sp500 = self::getSP500Companies();
            $nasdaq100 = self::getNasdaq100Companies();
            return array_merge($sp500, $nasdaq100);
        } catch (Exception $e) {
            dd($sp500, $nasdaq100);
            return $e->getMessage();
        }
    }

    public function getSP500Companies()
    {
        $path = 'sp500_constituent/';
        return $this->getResponse($path,'get');
    }

    public function getNasdaq100Companies()
    {
        $path = 'nasdaq_constituent/';
        return $this->getResponse($path,'get');
    }

    public function getSymbolByCik($cik)
    {
        $file = file_get_contents("https://www.sec.gov/include/ticker.txt");
        $remove = "\n";
        $split = explode($remove, $file);
        $tab = "\t";
        foreach ($split as $string) {
            $row = explode($tab, $string);
            if ($row[1] == $cik) {
                return ['symbol' => $row[0], 'cik' => $row[1]];
            }
        }
        return null;
    }

    public function getChartData($symbol, $chartName, $index, $limit = 15, $period = 'quarter')
    {
        $requestData = explode(',', $chartName);
        $this->symbol = $symbol;
        $this->limit = $limit;
        $this->period = $period;
        return self::$chartName($index);
    }

    /**
     * @param $requestData = functionName,x_key,y_key
     * @return array
     */
    public function getTotalAssetsChartData($index)
    {
        $balanceSheet = self::getBalanceSheet($this->symbol, $this->limit, $this->period);
        $data = [];
        foreach ($balanceSheet as $item) {
            $data[] = ['time' => Carbon::Parse($item['date'])->format('U'), 'value' => $item[$index]];
        }
        return $data;
    }

    public function getBalanceSheetChart($index)
    {
        $balanceSheet = self::getBalanceSheet($this->symbol, $this->limit, $this->period);
        $data = [];
        foreach ($balanceSheet as $item) {
            $data[] = ['time' => $item['date'], 'value' => $item[$index]];
        }
        return $data;

    }

    public function getSicBySymbol($symbol)
    {

        $cik = self::getCikBySymbol($symbol);
        $url = "https://www.sec.gov/cgi-bin/browse-edgar?CIK=" . $cik . "&owner=exclude&output=atom";
        $xmlstring = file_get_contents($url);
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        return $array['company-info']['assigned-sic'];
    }

    public function getIndustryInformation($symbol, $limit = 10)
    {
        $sic = self::getSicBySymbol($symbol);
        $url = "https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&SIC=" . $sic . "&owner=exclude&count=" . $limit . "&output=atom";
        $xmlstring = file_get_contents($url);
        $xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        foreach ($array['entry'] as $index => $value) {
            $data[] = $array['entry'][$index]['content']['company-info'];
        }
        return $data;
    }

    /**
     * @Endpoint https://financialmodelingprep.com/api/v4/mapper-cik-name?apikey={API KEY}
     */
    public function getCikNames()
    {
        return $this->getResponse('mapper-cik-name', 'get', [], 'v4');
    }

    /**
     * @Endpoint https://financialmodelingprep.com/api/v4/insider-trading?reportingCik=0001663020&limit=100&apikey={API KEY}
     * @param $reportingCik
     * @param int $limit
     * @return array|mixed|string
     */
    public function getInsiderTrading($reportingCik, $limit = 100)
    {
        $reportingCik = sprintf("%010d", $reportingCik);
        return $this->getResponse('insider-trading', 'get', ['reportingCik' => $reportingCik, 'limit' => $limit], 'v4');
    }


    public function getCompanyInsiderTrading($symbolOrCik, $limit = 100)
    {
        if (is_int($symbolOrCik)) {
            $reportingCik = sprintf("%010d", $symbolOrCik);
            return $this->getResponse('insider-trading', 'get', ['companyCik' => $reportingCik, 'limit' => $limit], 'v4');
        } else {
            return $this->getResponse('insider-trading', 'get', ['symbol' => strtoupper($symbolOrCik), 'limit' => $limit], 'v4');
        }
    }


}
