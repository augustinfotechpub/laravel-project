<?php


namespace App\Services;


use DOMDocument;
use Goutte\Client;

class NasdaqService
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getInstitutionalHolders($symbol)
    {
        $url = "https://money.cnn.com/quote/shareholders/shareholders.html?symb=$symbol&subView=institutional";
        $response = $this->client->request('GET', $url);
        $res = $response->filter('.wsod_institutionalTop10');
        $htmlTable = $res->outerHtml();
        $dom = new DOMDocument();
        @$dom->loadHTML($htmlTable);
        $domThead = $dom->getElementsByTagName('th');
        $domTbody = $dom->getElementsByTagName('tbody');
        foreach ($domThead as $th) {
            $thead[] = trim($th->textContent);
        }

        $tbody = [];
        foreach ($domTbody[0]->getElementsByTagName('tr') as $tr_index => $tr) {
            foreach ($tr->getElementsByTagName('td') as $th) {
                $tbody[$tr_index][] = trim($th->textContent);
            }
        }
        return ['thead' => $thead ?? [], 'tbody' => $tbody];
    }
}
