<?php


namespace App\Services;


use App\Models\BalanceSheetStatement;
use App\Models\eod_balance_sheet;
use App\Models\CashFlowStatement;
use App\Models\eod_cash_flow;
use App\Models\ComprehensiveIncomeStatement;
use App\Models\IncomeStatement;
use App\Models\eod_income_statement;
use App\Models\ShareholdersEquityStatement;
use App\Models\DefinitionItem;

class FinanceService
{
    protected $symbol, $limit, $period, $profile;

    public function __construct($profile, $period = 'FY', $limit = 10)
    {
        $this->profile = $profile;
        $this->symbol = $profile['symbol'];
        $this->period = $period;
        $this->limit = $limit;
    }


    public function getIncomeStatement($appendTTM = true, $sortByDesc = false): array
    {
        $statement = [];
        $statement_rows = IncomeStatement::query()
            ->where('symbol', $this->symbol);
        if (in_array($this->period, ['Quarter', 'quarter'])) {
            $statement_rows->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4']);
        } else {
            $statement_rows->where('period', 'FY');
        }
        $statement_rows = $statement_rows->take($this->limit)->get();
        if ($sortByDesc) {
            $statement_rows = $statement_rows->sortDesc()->values()->all();
        }

        if ($appendTTM) {
            $statementTTM = IncomeStatement::query()
                ->where('symbol', $this->symbol)->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4'])
                ->take(4)->get();
            if ($sortByDesc)
                $statementTTM = $statementTTM->sortDesc()->values()->all();
        }

        foreach (IncomeStatement::TITLES as $titleIndex => $titleColumn) {
            if (isset($titleColumn['fromSic']) && isset($titleColumn['toSic'])) {
                if ($this->profile['sic'] >= $titleColumn['fromSic'] && $this->profile['sic'] <= $titleColumn['fromSic']) {
                    // Do Something if doesn't match.
                } else {
                    continue;
                }
            }
            foreach ($statement_rows as $rowIndex => $row) {
                if ($rowIndex == 0) {
                    $statement[$titleIndex][] = $titleColumn;
                    if ($appendTTM)
                        $statement[$titleIndex][] = $statementTTM->sum($titleColumn['column']);
                }
                $statement[$titleIndex][] = $row[$titleColumn['column']];
            }
        }
        $headerColumns = collect($statement_rows)->pluck('date')->toArray();
        return [
            'header' => 'Income Statement',
            'alert_title' => '_income_statement_alert',
            'sub_title' => "All amount in millions",
            'headerColumns' => $headerColumns,
            'rows' => $statement
        ];
    }

    public function getEodIncomeStatement($appendTTM = true, $sortByDesc = false,$showReverse=false): array
    {
        $statement = [];
        $statement_rows = eod_income_statement::orderBy('period_date', 'desc')
            ->where('ticker', $this->symbol);
        if (in_array($this->period, ['Quarter', 'quarter'])) {
            $statement_rows->whereIn('period_type', ['quarterly']);
        } else {
            $statement_rows->where('period_type', 'yearly');
        }
        $statement_rows = $statement_rows->take($this->limit)->get();
        if ($sortByDesc) {
            $statement_rows = $statement_rows->sortDesc()->values()->all();
        }

        $statement_rowss = eod_balance_sheet::orderBy('period_date', 'desc')
            ->where('ticker', $this->symbol);
        $statement_rowss->whereIn('period_type', ['quarterly']);
        $statement_rowss = $statement_rowss->take($this->limit)->get();
        if ($sortByDesc) {
            $statement_rowss = $statement_rowss->sortDesc()->values()->all();
        }

        $epsttm=$this->division(@$statement_rows[0]->netIncomeApplicableToCommonShares,(@$statement_rowss[0]->commonStockSharesOutstanding+@$statement_rowss[1]->commonStockSharesOutstanding+@$statement_rowss[2]->commonStockSharesOutstanding+@$statement_rowss[3]->commonStockSharesOutstanding)/4);
        if ($appendTTM) {
            $statementTTM = eod_income_statement::orderBy('period_date', 'desc')
                ->where('ticker', $this->symbol)->whereIn('period_type', ['quarterly'])
                ->take(4)->get();
            if ($sortByDesc)
                $statementTTM = $statementTTM->sortDesc()->values()->all();
        }

        $alertAllContent = [];
        $alertsTable=['income_statement_table_alert'];
        $alertsTableData= [];

        foreach ($alertsTable as $key => $value) {
            $definitionItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitionItemData) ){
                foreach ($definitionItemData as $ke => $val) {
                    $alertsTableData[$value][] = $val['content'];  
                } 
            }
        }
        foreach (eod_income_statement::TITLES as $titleIndex => $titleColumn) {
            /*if (isset($titleColumn['fromSic']) && isset($titleColumn['toSic'])) {
                if ($this->profile['sic'] >= $titleColumn['fromSic'] && $this->profile['sic'] <= $titleColumn['fromSic']) {
                    // Do Something if doesn't match.
                } else {
                    continue;
                }
            }*/

            $alertIdentifier = DefinitionItem::where('identifier', $titleColumn['column']."_".$titleColumn['alert_title'])->where('type', 'alert')->get();
            $alertIdentifier = (array)json_decode( json_encode($alertIdentifier) , true);

            $alertAllContent[$titleColumn['title']] = $alertIdentifier;

            foreach ($statement_rows as $rowIndex => $row) {

                if ($rowIndex == 0) {
                    @$statement[$titleIndex][] = $titleColumn;
                    if ($appendTTM)
                        @$statement[$titleIndex][] = $statementTTM->sum($titleColumn['column']);
                }
                @$statement[$titleIndex][] = $row[$titleColumn['column']];
            }
        }
        if($showReverse=='true')
        {
            foreach ($statement as $key => $value) {
                $statement[$key] = array_reverse($value);
                $b=array_pop($statement[$key]);
                // $c=array_pop($statement[$key]);
                array_unshift($statement[$key],$b);
            }
            $headerColumns = collect($statement_rows)->sortBy('date')->pluck('date')->toArray();
        }  
        else
        {
            $headerColumns = collect($statement_rows)->pluck('date')->toArray();
        }

        return [
            'header' => 'Income Statement',
            'alert_title' => '_income_statement_alert',
            'sub_title' => "In thousands, except per-share amounts",
            'alert_content' => $alertAllContent,
            'alert_table_header' => $alertsTableData,
            'headerColumns' => $headerColumns,
            'rows' => $statement,
            'epsttm' => $epsttm
        ];
    }

    public function getIncomeStatementFromEdgar()
    {
        return false;
    }

    public function getBalanceSheet($sortByDesc = false): array
    {
        $statement = [];
        $statement_rows = BalanceSheetStatement::query()
            ->where('symbol', $this->symbol);

        if (in_array($this->period, ['Quarter', 'quarter'])) {
            $statement_rows->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4']);
        } else {
            $statement_rows->where('period', 'FY');
        }
        $statement_rows = $statement_rows->take($this->limit)->get();

        if ($sortByDesc) {
            $statement_rows = $statement_rows->sortDesc()->values()->all();
        }

        foreach (BalanceSheetStatement::TITLES as $titleIndex => $titleColumn) {
            if (isset($titleColumn['fromSic']) && isset($titleColumn['toSic'])) {
                if ($this->profile['sic'] >= $titleColumn['fromSic'] && $this->profile['sic'] <= $titleColumn['fromSic']) {
                    // Do Something if doesn't match.
                } else {
                    continue;
                }
            }
            foreach ($statement_rows as $rowIndex => $row) {
                if ($rowIndex == 0) {
                    $statement[$titleIndex][] = $titleColumn;
                }
                $statement[$titleIndex][] = (float)$row[$titleColumn['column']];
            }
        }
        $headerColumns = collect($statement_rows)->pluck('date')->toArray();
        return [
            'header' => 'Balance Sheet Statement',
            'alert_title' => '_balance_statement_alert',
            'sub_title' => "All amount in millions",
            'headerColumns' => $headerColumns,
            'rows' => $statement
        ];
    }

    public function eodGetBalanceSheet($sortByDesc = false,$showReverse=false): array
    {
        $statement = [];
        $statement_rows = eod_balance_sheet::orderBy('period_date', 'desc')
            ->where('ticker', $this->symbol);
           
        if (in_array($this->period, ['Quarter', 'quarter'])) {
            $statement_rows->whereIn('period_type', ['quarterly']);
        } else {
            $statement_rows->where('period_type', 'yearly');
        }
        $statement_rows = $statement_rows->take($this->limit)->get();

        if ($sortByDesc) {
            $statement_rows = $statement_rows->sortDesc()->values()->all();
        }

        $alertAllContent = [];
        $alertsTable=['balance_sheet_table_alert'];
        $alertsTableData= [];

        foreach ($alertsTable as $key => $value) {
            $definitionItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitionItemData) ){
                foreach ($definitionItemData as $ke => $val) {
                    $alertsTableData[$value][] = $val['content'];  
                } 
            }
        }
        foreach (eod_balance_sheet::TITLES as $titleIndex => $titleColumn) {
            /*if (isset($titleColumn['fromSic']) && isset($titleColumn['toSic'])) {
                if ($this->profile['sic'] >= $titleColumn['fromSic'] && $this->profile['sic'] <= $titleColumn['fromSic']) {
                    // Do Something if doesn't match.
                } else {
                    continue;
                }
            }*/
            
            $alertIdentifier = DefinitionItem::where('identifier', $titleColumn['column']."_".$titleColumn['alert_title'])->where('type', 'alert')->get();
            $alertIdentifier = (array)json_decode( json_encode($alertIdentifier) , true);
            
            $alertAllContent[$titleColumn['title']] = $alertIdentifier;

            foreach ($statement_rows as $rowIndex => $row) {
                if ($rowIndex == 0) {
                    @$statement[$titleIndex][] = $titleColumn;
                }
                @$statement[$titleIndex][] = (float)$row[$titleColumn['column']];
            }
        }
        if($showReverse=='true')
        {
            foreach ($statement as $key => $value) {
                $statement[$key] = array_reverse($value);
                $b=array_pop($statement[$key]);
                array_unshift($statement[$key],$b);
            }
            $headerColumns = collect($statement_rows)->sortBy('date')->pluck('date')->toArray();
        }  
        else
        {
            $headerColumns = collect($statement_rows)->pluck('date')->toArray();
        }
        
        return [
            'header' => 'Balance Sheet',
            'alert_title' => '_balance_statement_alert',
            'sub_title' => "In thousands",
            'alert_content' => $alertAllContent,
            'alert_table_header' => $alertsTableData,
            'headerColumns' => $headerColumns,
            'rows' => $statement
        ];
    }

    public function getCashFlowStatement($appendTTM = true, $sortByDesc = false): array
    {
        $statement = [];
        $statement_rows = CashFlowStatement::query()
            ->where('symbol', $this->symbol);
        if (in_array($this->period, ['Quarter', 'quarter'])) {
            $statement_rows->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4']);
        } else {
            $statement_rows->where('period', 'FY');
        }
        $statement_rows = $statement_rows->take($this->limit)->get();

        if ($sortByDesc)
            $statement_rows = $statement_rows->sortDesc()->values()->all();

        if ($appendTTM) {
            $statementTTM = CashFlowStatement::query()
                ->where('symbol', $this->symbol)->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4'])
                ->take(4)->get();
            if ($sortByDesc)
                $statementTTM = $statementTTM->sortDesc()->values()->all();
        }

        foreach (CashFlowStatement::TITLES as $titleIndex => $titleColumn) {
            if (isset($titleColumn['fromSic']) && isset($titleColumn['toSic'])) {
                if ($this->profile['sic'] >= $titleColumn['fromSic'] && $this->profile['sic'] <= $titleColumn['fromSic']) {
                    // Do Something if doesn't match.
                } else {
                    continue;
                }
            }
            foreach ($statement_rows as $rowIndex => $row) {
                if ($rowIndex == 0) {
                    $statement[$titleIndex][] = $titleColumn;
                    if ($appendTTM)
                        $statement[$titleIndex][] = $statementTTM->sum($titleColumn['column']);
                }
                $statement[$titleIndex][] = (float)$row[$titleColumn['column']];
            }
        }
        $headerColumns = collect($statement_rows)->pluck('date')->toArray();
        return [
            'header' => 'Cash Flow Statement',
            'alert_title' => '_cash_flow_statement_alert',
            'sub_title' => "All amount in millions",
            'headerColumns' => $headerColumns,
            'rows' => $statement
        ];
    }

    public function eodGetCashFlowStatement($appendTTM = true, $sortByDesc = false,$showReverse=false): array
    {
        $statement = [];
        $statement_rows = eod_cash_flow::orderBy('period_date', 'desc')
            ->where('ticker', $this->symbol);
        if (in_array($this->period, ['Quarter', 'quarter'])) {
            $statement_rows->whereIn('period_type', ['quarterly']);
        } else {
            $statement_rows->where('period_type', 'yearly');
        }
        
        $statement_rows = $statement_rows->take($this->limit)->get();

        if ($sortByDesc)
            $statement_rows = $statement_rows->sortDesc()->values()->all();

        if ($appendTTM) {
            $statementTTM = eod_cash_flow::orderBy('period_date', 'desc')
                ->where('ticker', $this->symbol)->whereIn('period_type', ['quarterly'])
                ->take(4)->get();
            if ($sortByDesc)
                $statementTTM = $statementTTM->sortDesc()->values()->all();
        }

        $alertAllContent = [];
        $alertsTable=['cash_flow_statement_table_alert'];
        $alertsTableData= [];

        foreach ($alertsTable as $key => $value) {
            $definitionItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitionItemData) ){
                foreach ($definitionItemData as $ke => $val) {
                    $alertsTableData[$value][] = $val['content'];  
                } 
            }
        }
        foreach (eod_cash_flow::TITLES as $titleIndex => $titleColumn) {
            if (isset($titleColumn['fromSic']) && isset($titleColumn['toSic'])) {
                if ($this->profile['sic'] >= $titleColumn['fromSic'] && $this->profile['sic'] <= $titleColumn['fromSic']) {
                    // Do Something if doesn't match.
                } else {
                    continue;
                }
            }

            $alertIdentifier = DefinitionItem::where('identifier', $titleColumn['column']."_".$titleColumn['alert_title'])->where('type', 'alert')->get();
            $alertIdentifier = (array)json_decode( json_encode($alertIdentifier) , true);

            $alertAllContent[$titleColumn['title']] = $alertIdentifier;

            foreach ($statement_rows as $rowIndex => $row) {
                if ($rowIndex == 0) {
                    @$statement[$titleIndex][] = $titleColumn;
                    if ($appendTTM)
                        @$statement[$titleIndex][] = $statementTTM->sum($titleColumn['column']);
                }
                @$statement[$titleIndex][] = (float)@$row[$titleColumn['column']];
            }
        }
        if($showReverse=='true')
        {
            foreach ($statement as $key => $value) {
                $statement[$key] = array_reverse($value);
                $b=array_pop($statement[$key]);
                // $c=array_pop($statement[$key]);
                array_unshift($statement[$key],$b);
            }
            $headerColumns = collect($statement_rows)->sortBy('date')->pluck('date')->toArray();
        }  
        else
        {
            $headerColumns = collect($statement_rows)->pluck('date')->toArray();
        }
        return [
            'header' => 'Cash Flow Statement',
            'alert_title' => '_cash_flow_statement_alert',
            'sub_title' => "In thousands",
            'alert_content' => $alertAllContent,
            'alert_table_header' => $alertsTableData,
            'headerColumns' => $headerColumns,
            'rows' => $statement
        ];
    }

    public function getShareholdersEquityStatements(): array
    {
        return ShareholdersEquityStatement::query()
            ->where(['symbol' => $this->symbol, 'period' => $this->period])
            ->select('title', 'html')->get()->toArray();
    }

    public function getComprehensiveIncomeStatement(): array
    {
        return ComprehensiveIncomeStatement::query()
            ->where(['symbol' => $this->symbol, 'period' => $this->period])
            ->select('title', 'html')->get()->toArray();

    }

    public function division($numerator, $denominator) {
        // perform division operationSusta
        if($numerator == 0 || $denominator == 0){
            return '0';
        }else{
            return ((float)$numerator/ (float)$denominator);
        }
        
    }
}
