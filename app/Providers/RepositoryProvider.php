<?php

namespace App\Providers;

use App\Interfaces\Admin\AuditCommitteeMembersInterface;
use App\Interfaces\Admin\CompanyInterface;
use App\Interfaces\Admin\ContactRequestInterface;
use App\Interfaces\Admin\DefinitionInterface;
use App\Interfaces\Admin\DefinitionItemSicsInterface;
use App\Interfaces\Admin\DefinitionItemsInterface;
use App\Interfaces\Admin\DefinitionItemSymbolInterface;
use App\Interfaces\Admin\DefinitionItemTagsInterface;
use App\Interfaces\Admin\EmailTemplateInterface;
use App\Interfaces\Admin\FinancialStatementEntriesInterface;
use App\Interfaces\Admin\FinancialStatementInterface;
use App\Interfaces\Admin\KeyDefinitionMappingInterface;
use App\Interfaces\Admin\SiteConfigurationInterface;
use App\Interfaces\Admin\SubscriptionPlanInterface;
use App\Interfaces\EmailServiceInterface;
use App\Interfaces\Front\CustomerSubscriptionInterface;
use App\Interfaces\Front\UserInterface;
use App\Repositories\Admin\AuditCommitteeMembersRepository;
use App\Repositories\Admin\CompanyRepository;
use App\Repositories\Admin\ContactRequestRepository;
use App\Repositories\Admin\DefinitionItemSicsRepository;
use App\Repositories\Admin\DefinitionItemsRepository;
use App\Repositories\Admin\DefinitionItemSymbolRepository;
use App\Repositories\Admin\DefinitionItemTagsRepository;
use App\Repositories\Admin\DefinitionRepository;
use App\Repositories\Admin\EmailTemplateRepository;
use App\Repositories\Admin\FinancialStatementEntriesRepository;
use App\Repositories\Admin\FinancialStatementRepository;
use App\Repositories\Admin\KeyDefinitionMappingRepository;
use App\Repositories\Admin\SiteConfigurationRepository;
use App\Repositories\Admin\SubscriptionPlanRepository;
use App\Repositories\Front\CustomerSubscriptionRepository;
use App\Repositories\Front\UserRepository;
use App\Services\EmailService;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmailServiceInterface::class, function ($app) {
            return $app->make(EmailService::class);
        });

        // Front Interfaces and Repository
        $this->app->bind(\App\Interfaces\Front\SubscriptionPlanInterface::class, \App\Repositories\Front\SubscriptionPlanRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(CustomerSubscriptionInterface::class, CustomerSubscriptionRepository::class);


        // Admin Interfaces and Repository
        $this->app->bind(SiteConfigurationInterface::class, SiteConfigurationRepository::class);
        $this->app->bind(SubscriptionPlanInterface::class, SubscriptionPlanRepository::class);
        $this->app->bind(\App\Interfaces\Admin\UserInterface::class, \App\Repositories\Admin\UserRepository::class);
        $this->app->bind(\App\Interfaces\Admin\CustomerSubscriptionInterface::class, \App\Repositories\Admin\CustomerSubscriptionRepository::class);
        $this->app->bind(EmailTemplateInterface::class, EmailTemplateRepository::class);
        $this->app->bind(ContactRequestInterface::class, ContactRequestRepository::class);
        $this->app->bind(DefinitionInterface::class, DefinitionRepository::class);
        $this->app->bind(KeyDefinitionMappingInterface::class, KeyDefinitionMappingRepository::class);

        $this->app->bind(AuditCommitteeMembersInterface::class, AuditCommitteeMembersRepository::class);
        $this->app->bind(CompanyInterface::class, CompanyRepository::class);
        $this->app->bind(FinancialStatementInterface::class, FinancialStatementRepository::class);
        $this->app->bind(FinancialStatementEntriesInterface::class, FinancialStatementEntriesRepository::class);
        $this->app->bind(DefinitionItemsInterface::class, DefinitionItemsRepository::class);
        $this->app->bind(DefinitionItemSicsInterface::class, DefinitionItemSicsRepository::class);
        $this->app->bind(DefinitionItemSymbolInterface::class, DefinitionItemSymbolRepository::class);
        $this->app->bind(DefinitionItemTagsInterface::class, DefinitionItemTagsRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
