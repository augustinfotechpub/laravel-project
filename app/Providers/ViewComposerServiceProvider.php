<?php

namespace App\Providers;

use App\Http\ViewComposers\AdminViewComposer;
use App\Http\ViewComposers\FrontViewComposer;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{


    public function boot()
    {
        view()->composer(["admin.*"],AdminViewComposer::class);
        view()->composer(["front.*"],FrontViewComposer::class);
    }
}
