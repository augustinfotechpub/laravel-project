<?php

namespace App\Providers;

use App\Events\Admin\ForgotPasswordEmail;
use App\Events\Front\EmailVerification;
use App\Events\Front\WelcomeEmail;
use App\Http\Controllers\Admin\Auth\ForgotPasswordController;
use App\Listeners\Admin\ForgotPasswordEmailHandler;
use App\Listeners\Front\EmailVerificationHandler;
use App\Listeners\Front\WelcomeEmailHandler;
use App\Listeners\Front\MonthlysurveyEmailHandler;
use App\Listeners\Front\SubscriptionAlertEmailHandler;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Listeners\Front\CancelSubscriptionAlertNotificationEmailHandler;
use App\Listeners\Front\CancelSubscriptionAdminEmailHandler;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        //user
        \App\Events\Front\ForgotPasswordEmail::class=>[
            \App\Listeners\Front\ForgotPasswordEmailHandler::class,
        ],

        EmailVerification::class => [
            EmailVerificationHandler::class,
        ],

        ForgotPasswordEmail::class => [
            ForgotPasswordEmailHandler::class,

        ],

        WelcomeEmail::class => [
            WelcomeEmailHandler::class,

        ],
        \App\Events\Front\MonthlysurveyEmail::class => [
            MonthlysurveyEmailHandler::class,
        ],
        \App\Events\Front\SubscriptionAlertEmail::class => [
            \App\Listeners\Front\SubscriptionAlertEmailHandler::class,
        ],
        \App\Events\Front\CancelSubscriptionAlertNotificationEmail::class => [
            CancelSubscriptionAlertNotificationEmailHandler::class,
        ],
        \App\Events\Front\CancelSubscriptionAdminEmail::class => [
            CancelSubscriptionAdminEmailHandler::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
