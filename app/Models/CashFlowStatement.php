<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashFlowStatement extends Model
{
    use HasFactory;

    protected $table = 'cash_flow_statements';

    protected $fillable = [
        'id',
        'company_id',
        'date',
        'symbol',
        'reportedCurrency',
        'fillingDate',
        'acceptedDate',
        'period',
        'netIncome',
        'depreciationAndAmortization',
        'deferredIncomeTax',
        'stockBasedCompensation',
        'changeInWorkingCapital',
        'accountsReceivables',
        'inventory',
        'accountsPayables',
        'otherWorkingCapital',
        'otherNonCashItems',
        'netCashProvidedByOperatingActivities',
        'investmentsInPropertyPlantAndEquipment',
        'acquisitionsNet',
        'purchasesOfInvestments',
        'salesMaturitiesOfInvestments',
        'otherInvestingActivites',
        'netCashUsedForInvestingActivites',
        'debtRepayment',
        'commonStockIssued',
        'commonStockRepurchased',
        'dividendsPaid',
        'otherFinancingActivites',
        'netCashUsedProvidedByFinancingActivities',
        'effectOfForexChangesOnCash',
        'netChangeInCash',
        'cashAtEndOfPeriod',
        'cashAtBeginningOfPeriod',
        'operatingCashFlow',
        'capitalExpenditure',
        'freeCashFlow',
        'link',
        'finalLink',
        'created_at',
        'updated_at',
        'changeInInterestReceivable',
        'changeInInterestPayable',
        'changeInLoans',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Mutators
     */
    public function getFillingDateAttribute($value): string
    {
        return Carbon::parse($value)->format('Y/m/d');
    }

    protected $casts = [
        'date' => 'date:Y/m/d',
    ];

    public const TITLES = [
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Net income',
            'column' => 'netIncome',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Depreciation Amortization',
            'column' => 'depreciationAndAmortization',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Deferred Income Tax',
            'column' => 'deferredIncomeTax',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Stock based compensation',
            'column' => 'depreciationAndAmortization',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in working capital',
            'column' => 'changeInWorkingCapital',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Account Receivables',
            'column' => 'accountsReceivables',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Inventory',
            'column' => 'inventory',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Payable\'s Account',
            'column' => 'accountsPayables',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Other Working Capital',
            'column' => 'otherWorkingCapital',
            'indentation' => 0
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Other non-cash items',
            'column' => 'otherNonCashItems',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash Flow from Continuing investing Activities',
            'column' => 'netCashUsedForInvestingActivites',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Net PPE Purchase and Sale',
            'column' => 'investmentsInPropertyPlantAndEquipment',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Net Business Purchase and Sale',
            'column' => 'acquisitionsNet',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Purchase of Investments',
            'column' => 'purchasesOfInvestments',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Sale of Investments',
            'column' => 'purchasesOfInvestments',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash flow from Continuing financing Activities',
            'column' => 'netCashUsedProvidedByFinancingActivities',
            'indentation' => 0
        ],
                
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Repayment of Debt',
            'column' => 'debtRepayment',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Cash',
            'column' => 'netChangeInCash',
            'indentation' => 0
        ],
        
        
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Beginning Cash Position',
            'column' => 'cashAtBeginningOfPeriod',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'End Cash Position',
            'column' => 'cashAtEndOfPeriod',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Free Cash Flow',
            'column' => 'freeCashFlow',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash flow from operating activities',
            'column' => 'netCashProvidedByOperatingActivities',
            'indentation' => 4
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Capital Expenditure',
            'column' => 'capitalExpenditure',
            'indentation' => 4
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Issuance of Capital Stock',
            'column' => 'commonStockIssued',
            'indentation' => 0
        ],
    
        [
            'column' => 'changeInInterestReceivable',
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Interest Receivable',
            'indentation' => 0,
            'fromSic' => 6021,
            'toSic' => 6036,
            'sics' => [

            ],

        ],
        [
            'column' => 'changeInInterestPayable',
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Interest Payable',
            'indentation' => 0,
            'fromSic' => 6021,
            'toSic' => 6036,
            'sics' => [

            ],

        ],
        [
            'column' => 'changeInLoans',
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in Loans',
            'indentation' => 0,
            'fromSic' => 6021,
            'toSic' => 6036,
            'sics' => [

            ],
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Net Investments',
            'column' => 'purchasesOfInvestments',
            'indentation' => 0
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Other Financing Charges',
            'column' => 'otherFinancingActivites',
            'indentation' => 0
        ],


        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Repurchase of Capital Stock',
            'column' => 'commonStockRepurchased',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Deferred Tax',
            'column' => 'deferredIncomeTax',
            'indentation' => 0
        ],
        
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash Dividends Paid',
            'column' => 'dividendsPaid',
            'indentation' => 0
        ],
        


    ];
}
