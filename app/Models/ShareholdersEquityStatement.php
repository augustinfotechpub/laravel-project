<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShareholdersEquityStatement extends Model
{
    use HasFactory;

    protected $table = 'shareholders_equity_statements';

    protected $fillable = [
        'id',
        'company_id',
        'symbol',
        'period',
        'title',
        'html',
        'created_at',
        'updated_at'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
