<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sic extends Model
{
    use HasFactory, Search;

    protected $table = "sic";
    protected $fillable = ['id', 'sic_code', 'office', 'title', 'created_at', 'updated_at'];
    public const BANKING_INSURANCE_SICS = [601, 6011, 6019, 602, 6021, 6022, 6029, 603, 6035, 6036, 606, 6061, 6062, 608, 6081, 6082, 609, 6091, 6099];

    protected $searchable = [
        'sic_code',
        'office',
        'title',
    ];

    public function definitions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Definition', 'sic_key_definitions', 'sic_id', 'key_definition_id');

    }
}
