<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialStatement extends Model
{
    use HasFactory;

    public const PERIODS = ['FY' => 'Fiscal Year', 'quarter' => 'Quarter'];
    public const TYPES = ['income-statement' => 'Income Statement', 'balance-sheet' => 'Balance Sheet', 'cash-flow' => 'Cash Flow'];

    protected $table = 'financial_statements';
    protected $fillable = ['id', 'company_id', 'ticker', 'title', 'type', 'period', 'data', 'created_at', 'updated_at'];
    protected $casts = [
        'data' => 'array',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function company()
    {
        $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
