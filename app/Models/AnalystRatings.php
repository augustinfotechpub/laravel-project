<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalystRatings extends Model
{
    use HasFactory;

    protected $table = 'analystratings';
    protected $fillable = [
        'id',
        'company_cik',
        'company_ticker',
        'Rating',
        'TargetPrice',
        'StrongBuy',
        'Buy',
        'Hold',
        'Sell',
        'StrongSell',
        'created_at',
        'updated_at'
    ];
}
