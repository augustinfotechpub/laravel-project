<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class eod_balance_sheet extends Model
{
    use HasFactory;

    protected $table = 'eod_balance_sheets';
    protected $fillable = ['id', 'company_id', 'cik','ticker', 'period_type', 'period_date', 'date', 'filing_date', 'currency_symbol', 'totalAssets', 'intangibleAssets', 'earningAssets', 'otherCurrentAssets', 'totalLiab', 'totalStockholderEquity', 'deferredLongTermLiab', 'otherCurrentLiab', 'commonStock', 'retainedEarnings', 'otherLiab', 'goodWill', 'otherAssets', 'cash', 'totalCurrentLiabilities', 'netDebt', 'shortTermDebt', 'shortLongTermDebt', 'shortLongTermDebtTotal', 'otherStockholderEquity', 'propertyPlantEquipment', 'totalCurrentAssets', 'longTermInvestments', 'netTangibleAssets', 'shortTermInvestments', 'netReceivables', 'longTermDebt', 'inventory', 'accountsPayable', 'totalPermanentEquity', 'noncontrollingInterestInConsolidatedEntity', 'temporaryEquityRedeemableNoncontrollingInterests', 'accumulatedOtherComprehensiveIncome', 'additionalPaidInCapital', 'commonStockTotalEquity', 'preferredStockTotalEquity', 'retainedEarningsTotalEquity', 'treasuryStock', 'accumulatedAmortization', 'nonCurrrentAssetsOther', 'deferredLongTermAssetCharges', 'nonCurrentAssetsTotal', 'grossPPE', 'dividendsPaid', 'dividendShare', 'dividendYield', 'BookValue', 'capitalLeaseObligations', 'longTermDebtTotal', 'nonCurrentLiabilitiesOther', 'nonCurrentLiabilitiesTotal', 'negativeGoodwill', 'warrants', 'preferredStockRedeemable', 'capitalSurpluse', 'liabilitiesAndStockholdersEquity', 'cashAndShortTermInvestments', 'propertyPlantAndEquipmentGross', 'accumulatedDepreciation', 'netWorkingCapital', 'netInvestedCapital', 'commonStockSharesOutstanding', 'created_at', 'updated_at'];

    public const TITLES = [
	    ['column' => 'totalAssets', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Assets', 'indentation' => 0],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'totalCurrentAssets', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Current Assets', 'indentation' => 4],

	    ['column' => 'cash', 'alert_title' => 'balance_statement_alert', 'title' => 'Cash', 'indentation' => 8],

	    ['column' => 'cashAndShortTermInvestments', 'alert_title' => 'balance_statement_alert', 'title' => 'Cash and Cash Equivalents and Short-Term Investments', 'indentation' => 8],
	    ['column' => 'shortTermInvestments', 'alert_title' => 'balance_statement_alert', 'title' => 'Short-Term Investments', 'indentation' => 12],

	    ['column' => 'netReceivables', 'alert_title' => 'balance_statement_alert', 'title' => 'Receivables (net)', 'indentation' => 8],

	    ['column' => 'inventory', 'alert_title' => 'balance_statement_alert', 'title' => 'Inventory', 'indentation' => 8],

	    ['column' => 'otherCurrentAssets', 'alert_title' => 'balance_statement_alert', 'title' => 'Other Current Assets', 'indentation' => 8],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'nonCurrentAssetsTotal', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Non-Current Assets', 'indentation' => 4],

	     // Gross PPE (grandchild) (propertyPlantEquipment plus accumulatedDepreciation) (there’s a problem here because the net PPE amount is inserted for both gross and net PPE on the API - it’s incorrect - I can only see the data for apple so this order may change slightly once I see everything - this value needs to be PPE (net) plus accumulated depreciation)
	    ['column' => 'grossPPE', 'alert_title' => 'balance_statement_alert', 'title' => 'Gross PPE', 'indentation' => 8],
	    ['column' => 'accumulatedDepreciation', 'alert_title' => 'balance_statement_alert', 'title' => 'Accumulated Depreciation', 'indentation' => 12, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
	    ['column' => 'propertyPlantEquipment', 'alert_title' => 'balance_statement_alert', 'title' => 'PPE (net)', 'indentation' => 8],
	    ['column' => 'IntangibleAssets', 'alert_title' => 'balance_statement_alert', 'title' => 'Intangible Assets', 'indentation' => 8],
	    ['column' => 'accumulatedAmortization', 'alert_title' => 'balance_statement_alert', 'title' => 'Accumulated Amortization', 'indentation' => 12],
	    ['column' => 'goodwill', 'alert_title' => 'balance_statement_alert', 'title' => 'Goodwill', 'indentation' => 12],
	    
	    ['column' => 'longTermInvestments', 'alert_title' => 'balance_statement_alert', 'title' => 'Long-Term Investments', 'indentation' => 8],
	    ['column' => 'nonCurrrentAssetsOther', 'alert_title' => 'balance_statement_alert', 'title' => 'Other Non-Current Assets', 'indentation' => 8],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'otherAssets', 'alert_title' => 'balance_statement_alert', 'title' => 'Other Assets', 'indentation' => 8],

		['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'totalLiab', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Liabilities', 'indentation' => 0],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'totalCurrentLiabilities', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Current Liabilities', 'indentation' => 4],

	    ['column' => 'accountsPayable', 'alert_title' => 'balance_statement_alert', 'title' => 'Accounts Payable', 'indentation' => 8],

	    ['column' => 'shortTermDebt', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Short-Term Debt', 'indentation' => 8],

	    ['column' => 'otherCurrentLiab', 'alert_title' => 'balance_statement_alert', 'title' => 'Other Current Liabilities', 'indentation' => 8],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'nonCurrentLiabilitiesTotal', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Non-Current Liabilities', 'indentation' => 4],
	    ['column' => 'deferredLongTermLiab', 'alert_title' => 'balance_statement_alert', 'title' => 'Non-Current Deferred Liabilities', 'indentation' => 8],

	    ['column' => 'longTermDebtTotal', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Long-Term Debt', 'indentation' => 8],
	    ['column' => 'capitalLeaseObligations', 'alert_title' => 'balance_statement_alert', 'title' => 'Capital Lease Obligations', 'indentation' => 8],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'nonCurrentLiabilitiesOther', 'alert_title' => 'balance_statement_alert', 'title' => 'Other Non-Current Liabilities', 'indentation' => 8],
	    ['column' => 'negativeGoodwill', 'alert_title' => 'balance_statement_alert', 'title' => 'Negative Goodwill', 'indentation' => 8],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'totalStockholderEquity', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Stockholders’ Equity', 'indentation' => 0],
	    ['column' => 'commonStockTotalEquity', 'alert_title' => 'balance_statement_alert', 'title' => 'Common Stockholders’ Equity', 'indentation' => 4],
	    ['column' => 'commonStockSharesOutstanding', 'alert_title' => 'balance_statement_alert', 'title' => 'Common Shares Outstanding', 'indentation' => 8],
	    ['column' => 'preferredStockTotalEquity', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Preferred Stock', 'indentation' => 4],
	    ['column' => 'preferredStockRedeemable', 'alert_title' => 'balance_statement_alert', 'title' => 'Redeemable Preferred Stock', 'indentation' => 8],

	    ['column' => 'warrants', 'alert_title' => 'balance_statement_alert', 'title' => 'Warrants', 'indentation' => 4],
	    ['column' => 'additionalPaidInCapital', 'alert_title' => 'balance_statement_alert', 'title' => 'Additional Paid-In Capital', 'indentation' => 4],

	    ['column' => 'retainedEarnings', 'alert_title' => 'balance_statement_alert', 'title' => 'Retained Earnings', 'indentation' => 4],
	    ['column' => 'treasuryStock', 'alert_title' => 'balance_statement_alert', 'title' => 'Treasury Stock', 'indentation' => 4],

	    // Get it from cash flow
		['column' => 'dividendsPaid', 'alert_title' => 'balance_statement_alert', 'title' => 'Dividends Paid', 'indentation' => 4],

	    // Available on Highlights coumn from API
	    ['column' => 'dividendShare', 'alert_title' => 'balance_statement_alert', 'title' => 'Dividend Per Share', 'indentation' => 8],

	    // Available on Highlights coumn from API
	    ['column' => 'dividendYield', 'alert_title' => 'balance_statement_alert', 'title' => 'Dividend Yield', 'indentation' => 8],

	    ['column' => 'accumulatedOtherComprehensiveIncome', 'alert_title' => 'balance_statement_alert', 'title' => 'AOCI', 'indentation' => 4],
	    
	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

	    ['column' => 'liabilitiesAndStockholdersEquity', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Liabilities and Stockholders Equity', 'indentation' => 0],

	    ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],
	    
	    ['column' => 'shortLongTermDebtTotal', 'alert_title' => 'balance_statement_alert', 'title' => 'Total Debt', 'indentation' => 0],

	    ['column' => 'netDebt', 'alert_title' => 'balance_statement_alert', 'title' => 'Debt (net)', 'indentation' => 4],
	    ['column' => 'netWorkingCapital', 'alert_title' => 'balance_statement_alert', 'title' => 'Working Capital (net)', 'indentation' => 0],

	    ['column' => 'netTangibleAssets', 'alert_title' => 'balance_statement_alert', 'title' => 'Tangible Assets (net)', 'indentation' => 0],
	    
	    // Available on Highlights coumn from API
	    ['column' => 'BookValue', 'alert_title' => 'balance_statement_alert', 'title' => 'Book Value', 'indentation' => 0],
	    
	    
	];
}
