<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionPlan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'subscription_plans';

    protected $fillable = [
        'id',
        'product_key',
        'price_key',
        'title',
        'free_trial_period',
        'free_trial_unit',
        'interval_value',
        'interval_unit',
        'price',
        'currency',
        'other_data',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function customerSubscriptions()
    {
        return $this->hasMany(CustomerSubscription::class,'plan_id','id');
    }

}
