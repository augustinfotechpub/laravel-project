<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ownership extends Model
{
    use HasFactory;

    protected $table = 'ownerships';
    protected $fillable = [
        'id',
        'name',
        'issuerCik',
        'companyCik',
        'transactionDate',
        'position',
        'type',
        'description',
        'biography',
        'securitiesOwned',
        'gender',
        'pay',
        'yearBorn',
        'since',
        'created_at',
        'updated_at',
        'company_ticker'
    ];
}
