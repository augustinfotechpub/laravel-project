<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Admin extends Authenticatable
{
    use HasFactory,SoftDeletes;

    protected $table = 'admins';

    protected $fillable = ['id ', 'first_name', 'last_name', 'email', 'password', 'phone_number'];
    protected $hidden = ['password', 'remember_token',];

    protected $appends = ['full_name'];

    public function getFullNameAttribute(): string
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }
}
