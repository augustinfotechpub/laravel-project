<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditCommitteeMember extends Model
{
    use HasFactory;

    protected $table = 'audit_committee_members';
    protected $fillable = ['symbol', 'name', 'is_chairman'];
}
