<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CancelSubscription extends Model
{

    protected $table = 'cancel_subscription';

    protected $fillable = ['Userid', 'subscription_id', 'question_index', 'question', 'Message', 'created_at', 'modify_at'];

}