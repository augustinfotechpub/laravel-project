<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Definition extends Model
{
    use HasFactory;

    const TYPE = [
        'alert' => 'Alert',
        'definition' => 'Definition',
        'key_concept' => 'Key Concept',
        'industry_information' => 'Industry Information',
    ];

    protected $table = 'definitions';

    protected $primaryKey = 'id';

    protected $fillable = ['id ', 'title', 'identifier', 'created_at', 'updated_at',];

//    public function Sics()
//    {
//        return $this->belongsToMany('App\Models\Sic', 'sic_key_definitions', 'key_definition_id', 'sic_id');
//    }
    public static function orderBy(string $string, string $string1)
    {
    }

    public function items()
    {
       return $this->hasMany(DefinitionItem::class,'definition_id','id');
    }


}
