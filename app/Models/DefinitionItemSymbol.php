<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefinitionItemSymbol extends Model
{
    use HasFactory;
    protected $table = 'definition_item_symbols';
    protected $fillable = ['id','definition_id','definition_item_id','symbol','created_at','updated_at'];

    public function definition()
    {
        return $this->hasOne(Definition::class,'id','definition_id');
    }
}
