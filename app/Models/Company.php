<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Stripe\Balance;

class Company extends Model
{
    use HasFactory, Search;

    protected $table = 'companies';

    protected $fillable = [
        'id',
        'name',
        'cik',
        'entity_type',
        'sic',
        'ticker',
       /* 'exchanges',*/
        'sic_description',
        'ein',
        'description',
        'website',
        'investor_website',
        'category',
        'fiscal_year_end',
        'mailing_address_id',
        'business_address_id',
        'phone',
        'next_earning_date',
        'date_first_added',
        'founded_date',
        'created_at',
        'updated_at',
        'eod_description',
    ];

    protected $searchable = [
        'sic_code',
        'office',
        'title',
    ];
    protected $appends = [
        'tickers',
    ];

    /**
     * Mutators
     */
    public function getTickersAttribute(): array
    {
        return array_column($this->symbols->toArray(), 'symbol');
    }

    public function auditCommitteeMembers(): HasMany
    {
        return $this->hasMany(AuditCommitteeMember::class, 'symbol', 'symbol');
    }

    public function incomeStatements(): HasMany
    {
        return $this->hasMany(IncomeStatement::class);
    }

    public function balanceSheetStatements(): HasMany
    {
        return $this->hasMany(BalanceSheetStatement::class);
    }

    public function cashFlowStatements(): HasMany
    {
        return $this->hasMany(CashFlowStatement::class);
    }

    public function comprehensiveIncomeStatements(): HasMany
    {
        return $this->hasMany(ComprehensiveIncomeStatement::class);
    }

    public function shareholdersEquityStatement(): HasMany
    {
        return $this->hasMany(ShareholdersEquityStatement::class);
    }

    public function symbols(): HasMany
    {
        return $this->hasMany(Symbol::class);
    }

  
    protected $casts = [
        'tickers' => 'array',
        'exchanges' => 'array'
    ];
}
