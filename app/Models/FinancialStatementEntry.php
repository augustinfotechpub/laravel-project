<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialStatementEntry extends Model
{
    use HasFactory;

    protected $table = 'financial_statement_entries';
    protected $fillable = ['id', 'financial_statement_id', 'date', 'value', 'created_at', 'updated_at'];

    public function statement()
    {
        return $this->belongsTo(FinancialStatement::class, 'financial_statement_id', 'id');
    }
}
