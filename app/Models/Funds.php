<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Funds extends Model
{
    use HasFactory;

    protected $table = 'funds';
    protected $fillable = [
        'id',
        'company_cik',
        'company_ticker',
        'holder_name',
        'date',
        'totalShares',
        'totalAssets',
        'currentShares',
        'change',
        'change_p',
        'created_at',
        'updated_at'
    ];
}
