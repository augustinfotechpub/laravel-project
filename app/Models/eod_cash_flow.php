<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class eod_cash_flow extends Model
{
    use HasFactory;
    protected $table = 'eod_cash_flows';
    protected $fillable = ["id", "company_id", 'cik', "ticker", "period_type", "period_date", "date", "filing_date", "currency_symbol", "investments", "changeToLiabilities", "totalCashflowsFromInvestingActivities", "netBorrowings", "totalCashFromFinancingActivities",    "changeToOperatingActivities",  "netIncome",    "changeInCash", "beginPeriodCashFlow",  "endPeriodCashFlow",    "totalCashFromOperatingActivities", "netIncomeFromContinuingOps", "depreciationAndAmortization", "totalCurrentAssets", "totalCurrentLiabilities", "accountsPayable", "propertyPlantAndEquipmentGross", "longTermDebtTotal", "shortTermDebt", "commonStockSharesOutstanding",    "depreciation", "otherCashflowsFromInvestingActivities",    "dividendsPaid",    "changeToInventory",    "changeToAccountReceivables",   "salePurchaseOfStock",  "otherCashflowsFromFinancingActivities",    "changeToNetincome",    "capitalExpenditures",  "changeReceivables",    "cashFlowsOtherOperating",  "exchangeRateChanges",  "cashAndCashEquivalentsChanges",    "changeInWorkingCapital",   "otherNonCashItems",    "freeCashFlow", "created_at",   "updated_at"];

    public const TITLES = [


        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash Flows From Operating Activities',
            'column' => 'totalCashFromOperatingActivities',
            'indentation' => 0,
            'revenue' => 'self'
        ],

        // netIncomeFromContinuingOps From income tatement
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Net Income From Continuing Operations',
            'column' => 'netIncomeFromContinuingOps',
            'indentation' => 4,
            'revenue' => 'cfoa'
        ],

        // depreciationAndAmortization from Income statement
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Depreciation and Amortization',
            'column' => 'depreciationAndAmortization',
            'indentation' => 8,
            'revenue' => 'cfoa'
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Working Capital Changes',
            'column' => 'changeInWorkingCapital',
            'indentation' => 4,
            'revenue' => 'cfoa'
        ],

        // (totalCurrentAssets - balance sheet - the change from the previous period to the current period needs to be calculated here)
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Current Assets',
            'column' => 'totalCurrentAssets',
            'indentation' => 8,
            'revenue' => 'cfoa'
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Cash & Cash Equivalents',
            'column' => 'cashAndCashEquivalentsChanges',
            'indentation' => 12,
            'revenue' => 'cfoa'
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Accounts Receivable',
            'column' => 'changeToAccountReceivables',
            'indentation' => 12,
            'revenue' => 'cfoa'
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes In Inventory',
            'column' => 'changeToInventory',
            'indentation' => 12,
            'revenue' => 'cfoa'
        ],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        // totalCurrentLiabilities - balance sheet - the change from the previous period to the current period needs to be calculated here)
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes In Current Liabilities',
            'column' => 'totalCurrentLiabilities',
            'indentation' => 8
        ],
        
        // accountsPayable - balance sheet - the change from the previous period to the current period needs to be calculated here
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Accounts Payable',
            'column' => 'accountsPayable',
            'indentation' => 12
        ],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Other Operating Cash Flows',
            'column' => 'cashFlowsOtherOperating',
            'indentation' => 4
        ],
            
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change to Operating Activities',
            'column' => 'changeToOperatingActivities',
            'indentation' => 0
        ],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash Flows From Investing Activities',
            'column' => 'totalCashflowsFromInvestingActivities',
            'indentation' => 0,
            'revenue' => 'self'
        ],

        // propertyPlantAndEquipmentGross - balance sheet - the change from the previous period to the current period needs to be calculated here
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Change in PPE',
            'column' => 'propertyPlantAndEquipmentGross',
            'indentation' => 4,
            'revenue' => 'cfia'
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'CAPEX',
            'column' => 'capitalExpenditures',
            'indentation' => 4,
            'revenue' => 'cfia'
        ],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Other Cash Flows From Investing Activities',
            'column' => 'otherCashflowsFromInvestingActivities',
            'indentation' => 4,
            'revenue' => 'cfia'
        ],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Cash Flow From Financing Activities',
            'column' => 'totalCashFromFinancingActivities',
            'indentation' => 0,
            'revenue' => 'self'
        ],
        // longTermDebtTotal - balance sheet - the change from the previous period to the current period needs to be calculated here
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Long-Term Debt',
            'column' => 'longTermDebtTotal',
            'indentation' => 4,
            'revenue' => 'cffa'
        ],

        // shortTermDebt - balance sheet - the change from the previous period to the current period needs to be calculated here
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Short-Term Debt Issuance',
            'column' => 'shortTermDebt',
            'indentation' => 4,
            'revenue' => 'cffa'
        ],

        // commonStockSharesOutstanding - balance sheet - the change from the previous period to the current period needs to be calculated here
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Common Stock Issuance',
            'column' => 'commonStockSharesOutstanding',
            'indentation' => 4,
            'revenue' => 'cffa'
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Dividends Paid',
            'column' => 'dividendsPaid',
            'indentation' => 4,
            'revenue' => 'cffa'
        ],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Beginning of Period Cash Flow',
            'column' => 'beginPeriodCashFlow',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'End of Period Cash Flow',
            'column' => 'endPeriodCashFlow',
            'indentation' => 0
        ],
        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Changes in Cash',
            'column' => 'changeInCash',
            'indentation' => 4
        ],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        [
            'alert_title' => 'cash_flow_statement_alert',
            'title' => 'Free Cash Flow',
            'column' => 'freeCashFlow',
            'indentation' => 0
        ]

    ];
}
