<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IncomeStatement extends Model
{
    use HasFactory;


    protected $table = 'income_statements';

    protected $fillable = [
        'id',
        'company_id',
        'date',
        'symbol',
        'reportedCurrency',
        'fillingDate',
        'acceptedDate',
        'period',
        'revenue',
        'costOfRevenue',
        'grossProfit',
        'grossProfitRatio',
        'researchAndDevelopmentExpenses',
        'generalAndAdministrativeExpenses',
        'sellingAndMarketingExpenses',
        'otherExpenses',
        'operatingExpenses',
        'costAndExpenses',
        'interestExpense',
        'depreciationAndAmortization',
        'ebitda',
        'ebitdaratio',
        'operatingIncome',
        'operatingIncomeRatio',
        'totalOtherIncomeExpensesNet',
        'incomeBeforeTax',
        'incomeBeforeTaxRatio',
        'incomeTaxExpense',
        'netIncome',
        'netIncomeRatio',
        'eps',
        'epsdiluted',
        'weightedAverageShsOut',
        'weightedAverageShsOutDil',
        'link',
        'finalLink',
        'created_at',
        'updated_at',
        'interestIncomeOnLoans',
        'interestIncomeOnInvestment',
        'interestIncomeOnSecurities',
        'interestIncomeOnDepositsInBanks',
        'federalFundsSoldAndSecuritiesPurchasedUnderResaleAgreements',
        'interestIncomeFromOthers',
        'interestOnDeposits',
        'interestOnDebt',
        'federalFundsPurchasedAndSecuritiesSoldUnderResaleAgreements',
        'interestOnOtherBorrowedFunds',
        'totalInterestExpense',
        'provisionForLoanOrCreditLosses',
        'netInterestIncomeAfterLoanLossProvision',
        'mortgageBankingActivities',
        'cardFees',
        'federalDepositInsurance',
        'grossPremiumsWritten',
        'lessIncreaseInUnearnedPremiums',
        'lessReinsuranceCeded',
        'policyRevenuesAndBenefits',
        'BenefitsToPolicyholdersAndClaims',
        'lessReinsuranceRecoverable',
        'interestCreditedToContractHoldersFunds',
        'policyAcquisitionOrUnderwritingCosts',
        'amortizationOfDeferredPolicyAcquisitionCosts',
        'DividendsPaidToPolicyholders',
        'reinsuranceIncomeOrExpense',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }


    /**
     * Mutators
     */
    public function getFillingDateAttribute($value): string
    {
        return Carbon::parse($value)->format('Y/m/d');
    }

    /*protected $casts = [
        'date' => 'date:Y/m/d',
    ];*/

    public const TITLES = [
        ['column' => 'revenue', 'alert_title' => 'income_statement_alert', 'title' => 'Total Revenue', 'indentation' => 0],
        ['column' => 'costOfRevenue', 'alert_title' => 'income_statement_alert', 'title' => 'Cost of Revenue', 'indentation' => 4],

        ['column' => 'grossProfit', 'alert_title' => 'income_statement_alert', 'title' => 'Gross Profit', 'indentation' => 0],

        ['column' => 'operatingExpenses', 'alert_title' => 'income_statement_alert', 'title' => 'Operating Expense', 'indentation' => 0],
        ['column' => 'generalAndAdministrativeExpenses', 'alert_title' => 'income_statement_alert', 'title' => 'Selling General and Administrative', 'indentation' => 4],
        ['column' => 'researchAndDevelopmentExpenses', 'alert_title' => 'income_statement_alert', 'title' => 'Research & Development', 'indentation' => 4],
        ['column' => 'operatingIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Operating Income', 'indentation' => 0],
        ['column' => 'costAndExpenses', 'alert_title' => 'income_statement_alert', 'title' => 'Total Expenses', 'indentation' => 4],
        ['column' => 'otherExpenses', 'alert_title' => 'income_statement_alert', 'title' => 'Other Income Expense', 'indentation' => 4],
        ['column' => 'ebitda', 'alert_title' => 'income_statement_alert', 'title' => 'EBITDA', 'indentation' => 0],
        ['column' => 'interestExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Expense', 'indentation' => 4],
        ['column' => 'incomeBeforeTax', 'alert_title' => 'income_statement_alert', 'title' => 'Pretax Income', 'indentation' => 0],
        ['column' => 'incomeTaxExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Tax Provision', 'indentation' => 4],
        ['column' => 'netIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Net Income', 'indentation' => 0],
        ['column' => 'weightedAverageShsOut', 'alert_title' => 'income_statement_alert', 'title' => 'Basic Average Shares', 'indentation' => 4],

        ['column' => 'eps', 'alert_title' => 'income_statement_alert', 'title' => 'Basic EPS', 'indentation' => 0],
        ['column' => 'epsdiluted', 'alert_title' => 'income_statement_alert', 'title' => 'Diluted EPS', 'indentation' => 0],



        
        
        
        
        ['column' => 'weightedAverageShsOutDil', 'alert_title' => 'income_statement_alert', 'title' => 'Diluted Average Shares', 'indentation' => 0],
        // ['column' => 'totalInterestExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Total Interest Expense', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        // ['column' => 'interestExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Expense Non Operating', 'indentation' => 8],
        

        ['column' => 'reinsuranceIncomeOrExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Reinsurance Income or Expense', 'indentation' => 4, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        
        
        // ['column' => 'netIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Normalized EBITDA', 'indentation' => 0],
        /*['column' => 'netIncome', 'alert_title'=>'income_statement_alert,'title' => 'Diluted NI Available to Com Stockholders', 'indentation' => 0],*/


        // ['column' => 'operatingIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Total Operating Income as Reported', 'indentation' => 0],
        

        ['column' => 'interestIncomeOnLoans', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Income on Loans', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestIncomeOnInvestment', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Income on Investment', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestIncomeOnSecurities', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Income on Securities', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestIncomeOnDepositsInBanks', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Income on Deposits in Banks', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'federalFundsSoldAndSecuritiesPurchasedUnderResaleAgreements', 'alert_title' => 'income_statement_alert', 'title' => 'Federal Funds Sold and Securities Purchased under Resale Agreements', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestIncomeFromOthers', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Income from Others', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestCreditedToContractHoldersFunds', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Credited to Contractholders Funds', 'indentation' => 4, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'cardFees', 'alert_title' => 'income_statement_alert', 'title' => 'Card Fees', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestOnDeposits', 'alert_title' => 'income_statement_alert', 'title' => 'Interest on Deposits', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestOnDebt', 'alert_title' => 'income_statement_alert', 'title' => 'Interest on Debt', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'federalFundsPurchasedAndSecuritiesSoldUnderResaleAgreements', 'alert_title' => 'income_statement_alert', 'title' => 'Federal Funds Purchased and Securities Sold under Resale Agreements', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestOnOtherBorrowedFunds', 'alert_title' => 'income_statement_alert', 'title' => 'Interest on other Borrowed funds', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

        ['column' => 'provisionForLoanOrCreditLosses', 'alert_title' => 'income_statement_alert', 'title' => 'Provision For Loan/Credit Losses', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'mortgageBankingActivities', 'alert_title' => 'income_statement_alert', 'title' => 'Mortgage Banking Activities', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'federalDepositInsurance', 'alert_title' => 'income_statement_alert', 'title' => 'Federal Deposit Insurance', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'grossPremiumsWritten', 'alert_title' => 'income_statement_alert', 'title' => 'Gross Premiums Written', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'lessIncreaseInUnearnedPremiums', 'alert_title' => 'income_statement_alert', 'title' => 'Less Increase In Unearned Premiums', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'lessReinsuranceCeded', 'alert_title' => 'income_statement_alert', 'title' => 'Less Reinsurance Ceded', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

        ['column' => 'policyRevenuesAndBenefits', 'alert_title' => 'income_statement_alert', 'title' => 'Policy Revenues and Benefits', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'BenefitsToPolicyholdersAndClaims', 'alert_title' => 'income_statement_alert', 'title' => 'Benefits to Policyholders and Claims', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'lessReinsuranceRecoverable', 'alert_title' => 'income_statement_alert', 'title' => 'Less Reinsurance Recoverable', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

        ['column' => 'policyAcquisitionOrUnderwritingCosts', 'alert_title' => 'income_statement_alert', 'title' => 'Policy Acquisition/Underwriting Costs', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'amortizationOfDeferredPolicyAcquisitionCosts', 'alert_title' => 'income_statement_alert', 'title' => 'Amortization of Deferred Policy Acquisition Costs', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'DividendsPaidToPolicyholders', 'alert_title' => 'income_statement_alert', 'title' => 'Dividends paid to Policyholders', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

        ['column' => 'netInterestIncomeAfterLoanLossProvision', 'alert_title' => 'income_statement_alert', 'title' => 'Net Interest Income After Loan Loss Provision', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'costOfRevenue', 'alert_title' => 'income_statement_alert', 'title' => 'Reconciled Cost of Revenue', 'indentation' => 0],
        ['column' => 'depreciationAndAmortization', 'alert_title' => 'income_statement_alert', 'title' => 'Reconciled Depreciation', 'indentation' => 0]
    ];
}
