<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnershipInterest extends Model
{
    use HasFactory;

    protected $table = 'ownershipinterest';
    protected $fillable = [
        'id',
        'company_cik',
        'company_ticker',
        'holder_name',
        'holder_date',
        'totalShares',
        'totalAssets',
        'currentShares',
        'change',
        'change_p',
        'created_at',
        'updated_at'
    ];
}
