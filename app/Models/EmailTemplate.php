<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    use HasFactory;

    protected $table = 'email_templates';

    protected $fillable = ['id','title','subject','action','content','created_at','updated_at'];

    const EMAIL_ACTION = [
        "user_welcome_email" => "User Welcome Email",
        "user_forgot_password" => "User Forgot Password",
        "user_email_verification" => "User email verification",
        "admin_forgot_password" => "Admin Forgot Password",
        "user_monthly_survey" => "User Monthly Survey",
        "user_subscription_alert"=> "User Subscription Alert",
        "user_cancel_subscription_survey" => "Cancel Survey User notification",
        "admin_cancel_subscription_survey" => "Cancel Survey Admin notification"
    ];

}
