<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefinitionItemTag extends Model
{
    use HasFactory;
    protected $table = 'definition_item_tags';
    protected $fillable = ['id','definition_id','definition_item_id','keyword','created_at','updated_at'];
}
