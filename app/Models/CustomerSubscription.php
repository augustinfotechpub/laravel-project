<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerSubscription extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'customer_subscriptions';
    protected $fillable = [
        'id ', 'customer_id', 'plan_id', 'interval_value', 'interval_unit', 'start_date', 'end_date', 'trial_start', 'trial_end',
        'status', 'stripe_customer_id', 'stripe_plan_id', 'stripe_product_id', 'stripe_subscription_id',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $dates = ['start_date',
        'end_date',
        'trial_start',
        'trial_end'
    ];

    protected $appends = ['price'];

    public function getPriceAttribute()
    {
        // return $this->subscriptionPlan->price;
    }


    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function subscriptionPlan()
    {
        return $this->belongsTo(SubscriptionPlan::class, 'plan_id', 'id');

    }


}
