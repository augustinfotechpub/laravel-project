<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BalanceSheetStatement extends Model
{
    use HasFactory;


    protected $table = 'balance_sheet_statements';

    protected $fillable = [
        'id',
        'company_id',
        'date',
        'symbol',
        'reportedCurrency',
        'fillingDate',
        'acceptedDate',
        'period',
        'cashAndCashEquivalents',
        'shortTermInvestments',
        'cashAndShortTermInvestments',
        'netReceivables',
        'inventory',
        'otherCurrentAssets',
        'totalCurrentAssets',
        'propertyPlantEquipmentNet',
        'goodwill',
        'intangibleAssets',
        'goodwillAndIntangibleAssets',
        'longTermInvestments',
        'taxAssets',
        'otherNonCurrentAssets',
        'totalNonCurrentAssets',
        'otherAssets',
        'totalAssets',
        'accountPayables',
        'shortTermDebt',
        'taxPayables',
        'deferredRevenue',
        'otherCurrentLiabilities',
        'totalCurrentLiabilities',
        'longTermDebt',
        'deferredRevenueNonCurrent',
        'deferredTaxLiabilitiesNonCurrent',
        'otherNonCurrentLiabilities',
        'totalNonCurrentLiabilities',
        'otherLiabilities',
        'totalLiabilities',
        'commonStock',
        'retainedEarnings',
        'accumulatedOtherComprehensiveIncomeLoss',
        'othertotalStockholdersEquity',
        'totalStockholdersEquity',
        'totalLiabilitiesAndStockholdersEquity',
        'totalInvestments',
        'totalDebt',
        'netDebt',
        'created_at',
        'updated_at',
        'cashAndCashEquivalentsAndDueFromBanks',
        'interestBearingDeposits',
        'moneyMarketInvestments',
        'shortTermInvestmentsBanks',
        'federalFundsSold',
        'federalFundsSoldSecuritiesPurchasedUnderResaleAgreements',
        'accruedInterestReceivable',
        'rightOfUseAssets',
        'mortgageBackedSecurities',
        'municipalBondsAndGovernmentSecurities',
        'investmentsInFedHomeLoanBank',
        'investmentInRealEstate',
        'investmentOthers',
        'loansHeldForSale',
        'grossLoansMade',
        'loanLossAllowance',
        'otherSubtractionsFromLoans',
        'netLoans',
        'grossPropertyPlantAndEquipment',
        'TotalAccumulatedDepreciationAndDepletion',
        'netPropertyPlantAndEquipment',
        'federalHomeLoanBankDebt',
        'nonPerformingLoans',
        'realEstateOwned',
        'mortgageLoans',
        'policyLoans',
        'netPremiumsReceivables',
        'netReinsuranceReceivables',
        'rightOfUseAssetsCurrent',
        'policyBenefitsLossesAndReserves',
        'UnearnedPremiumReserveOrInsurancePayable',
        'reinsurancePayable',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Mutators
     */
    public function getFillingDateAttribute($value): string
    {
        return Carbon::parse($value)->format('Y/m/d');
    }

    public const TITLES = [
        ['column' => 'totalAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Total Assets', 'indentation' => 0],
        ['column' => 'totalCurrentAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Current Assets', 'indentation' => 4],

        ['column' => 'cashAndCashEquivalents', 'alert_title' => 'balance_sheet_alert', 'title' => 'Cash And Cash Equivalents', 'indentation' => 8],
        ['column' => 'shortTermInvestmentsBanks', 'alert_title' => 'balance_sheet_alert', 'title' => 'Short-Term Investments - Banks', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'cashAndCashEquivalentsAndDueFromBanks', 'alert_title' => 'balance_sheet_alert', 'title' => 'Cash and Cash Equivalents & Due from Banks', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'netLoans', 'alert_title' => 'balance_sheet_alert', 'title' => 'Net Loans', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'mortgageLoans', 'alert_title' => 'balance_sheet_alert', 'title' => 'Mortgage Loans', 'indentation' => 4, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'policyLoans', 'alert_title' => 'balance_sheet_alert', 'title' => 'Policy Loans', 'indentation' => 4, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'investmentOthers', 'alert_title' => 'balance_sheet_alert', 'title' => 'Investment - Others', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'interestBearingDeposits', 'alert_title' => 'balance_sheet_alert', 'title' => 'Interest Bearing Deposits', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

        ['column' => 'netReceivables', 'alert_title' => 'balance_sheet_alert', 'title' => 'Receivables', 'indentation' => 4],
        ['column' => 'inventory', 'alert_title' => 'balance_sheet_alert', 'title' => 'Inventory', 'indentation' => 4],
        ['column' => 'otherCurrentAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Other Current Assets', 'indentation' => 4],
        ['column' => 'otherNonCurrentAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Other Non Current Assets', 'indentation' => 4],
        ['column' => 'grossPropertyPlantAndEquipment', 'alert_title' => 'balance_sheet_alert', 'title' => 'Gross Property, Plant & Equipment', 'indentation' => 8, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'TotalAccumulatedDepreciationAndDepletion', 'alert_title' => 'balance_sheet_alert', 'title' => 'Total Accumulated Depreciation & Depletion', 'indentation' => 8, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'propertyPlantEquipmentNet', 'alert_title' => 'balance_sheet_alert', 'title' => 'Net PPE', 'indentation' => 8],
        ['column' => 'goodwillAndIntangibleAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Goodwill And Intangible Assets', 'indentation' => 4],
        ['column' => 'intangibleAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Intangible Assets', 'indentation' => 4],
        ['column' => 'longTermInvestments', 'alert_title' => 'balance_sheet_alert', 'title' => 'Available for Sale Securities', 'indentation' => 4],

        ['column' => 'totalNonCurrentAssets', 'alert_title' => 'balance_sheet_alert', 'title' => 'Total non-current Assets', 'indentation' => 4],
        ['column' => 'realEstateOwned', 'alert_title' => 'balance_sheet_alert', 'title' => 'Real Estate Owned', 'indentation' => 4, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

        ['column' => 'totalCurrentLiabilities', 'alert_title' => 'balance_sheet_alert', 'title' => 'Current Liabilities', 'indentation' => 8],
        ['column' => 'accountPayables', 'alert_title' => 'balance_sheet_alert', 'title' => 'Payables And Accrued Expenses', 'indentation' => 8],
        ['column' => 'deferredRevenue', 'alert_title' => 'balance_sheet_alert', 'title' => 'Current Deferred Liabilities', 'indentation' => 8],
        ['column' => 'totalNonCurrentLiabilities', 'alert_title' => 'balance_sheet_alert', 'title' => 'Total Non Current Liabilities Net Minority Interest', 'indentation' => 8],
        ['column' => 'totalLiabilities', 'alert_title' => 'balance_sheet_alert', 'title' => 'Total Liabilities Net Minority Interest', 'indentation' => 4],

        /*['column' => 'accountPayables', 'alert_title'=>'balance_sheet_alert','title' => 'Payables', 'indentation' => 6],
        ['column' => 'accountPayables', 'alert_title'=>'balance_sheet_alert','title' => 'Accounts Payables', 'indentation' => 8],*/
        ['column' => 'federalFundsSoldSecuritiesPurchasedUnderResaleAgreements', 'alert_title' => 'balance_sheet_alert', 'title' => 'Federal Funds Sold & Securities Purchased Under Resale Agreements', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'accruedInterestReceivable', 'alert_title' => 'balance_sheet_alert', 'title' => 'Accrued Interest Receivable', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

        ['column' => 'shortTermDebt', 'alert_title' => 'balance_sheet_alert', 'title' => 'Current Debt And Capital Lease Obligation', 'indentation' => 4],
        ['column' => 'otherCurrentLiabilities', 'alert_title' => 'balance_sheet_alert', 'title' => 'Other Current Liabilities', 'indentation' => 4],
        ['column' => 'rightOfUseAssetsCurrent', 'alert_title' => 'balance_sheet_alert', 'title' => 'Right of Use Assets - Current', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'mortgageBackedSecurities', 'alert_title' => 'balance_sheet_alert', 'title' => 'Mortgage Backed Securities', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'municipalBondsAndGovernmentSecurities', 'alert_title' => 'balance_sheet_alert', 'title' => 'Municipal Bonds and Government Securities', 'indentation' => 4, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],


        //////////////////
        ['column' => 'federalFundsSold', 'alert_title' => 'balance_sheet_alert', 'title' => 'Federal Funds Sold', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'longTermInvestments', 'alert_title' => 'balance_sheet_alert', 'title' => 'Investments And Advances', 'indentation' => 0],
        ['column' => 'longTermInvestments', 'alert_title' => 'balance_sheet_alert', 'title' => 'Investment in Financial Assets', 'indentation' => 0],
        ['column' => 'investmentsInFedHomeLoanBank', 'alert_title' => 'balance_sheet_alert', 'title' => 'Investments in FedHomeLoanBank', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'investmentInRealEstate', 'alert_title' => 'balance_sheet_alert', 'title' => 'Investment in Real Estate', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'moneyMarketInvestments', 'alert_title' => 'balance_sheet_alert', 'title' => 'Money Market Investments', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],

        ['column' => 'loansHeldForSale', 'alert_title' => 'balance_sheet_alert', 'title' => 'Loans Held for Sale', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'grossLoansMade', 'alert_title' => 'balance_sheet_alert', 'title' => 'Gross Loans Made', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'loanLossAllowance', 'alert_title' => 'balance_sheet_alert', 'title' => 'Loan Loss Allowance', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'otherSubtractionsFromLoans', 'alert_title' => 'balance_sheet_alert', 'title' => 'Other Subtractions from Loans', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],


        ['column' => 'netPremiumsReceivables', 'alert_title' => 'balance_sheet_alert', 'title' => 'Net Premiums Receivables', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'netReinsuranceReceivables', 'alert_title' => 'balance_sheet_alert', 'title' => 'Net Reinsurance Receivables', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

        ['column' => 'totalStockholdersEquity', 'alert_title' => 'balance_sheet_alert', 'title' => 'Common Stock Equity', 'indentation' => 4],
        ['column' => 'retainedEarnings', 'alert_title' => 'balance_sheet_alert', 'title' => 'Retained Earnings', 'indentation' => 4],
        ['column' => 'netPropertyPlantAndEquipment', 'alert_title' => 'balance_sheet_alert', 'title' => 'Net Property, Plant & Equipment', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'totalStockholdersEquity', 'alert_title' => 'balance_sheet_alert', 'title' => 'Tangible Book Value', 'indentation' => 4],

        ['column' => 'policyBenefitsLossesAndReserves', 'alert_title' => 'balance_sheet_alert', 'title' => 'Policy Benefits, Losses & Reserves', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'UnearnedPremiumReserveOrInsurancePayable', 'alert_title' => 'balance_sheet_alert', 'title' => 'Unearned Premium Reserve/Insurance Payable', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],
        ['column' => 'reinsurancePayable', 'alert_title' => 'balance_sheet_alert', 'title' => 'Reinsurance Payable', 'indentation' => 0, 'fromSic' => 6311, 'toSic' => 6399, 'sics' => []],

        ['column' => 'federalHomeLoanBankDebt', 'alert_title' => 'balance_sheet_alert', 'title' => 'Federal Home Loan Bank Debt', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'nonPerformingLoans', 'alert_title' => 'balance_sheet_alert', 'title' => 'Non-Performing Loans', 'indentation' => 0, 'fromSic' => 6021, 'toSic' => 6036, 'sics' => []],
        ['column' => 'totalDebt', 'alert_title' => 'balance_sheet_alert', 'title' => 'Total Debt', 'indentation' => 0],

        ['column' => 'netDebt', 'alert_title' => 'balance_sheet_alert', 'title' => 'Net Debt', 'indentation' => 0],

    ];
}
