<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefinitionItemTagMap extends Model
{
    use HasFactory;
    protected $table = 'definition_item_tag_maps';
    protected $fillable = array('id',
        'definition_item_tags_id',
        'definition_id',
        'definition_item_id',
        'cik',
        'ticker',
        'created_at',
        'updated_at');
}
