<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = 'addresses';
    protected $fillable = [
        'id',
        'company_id',
        'street1',
        'street2',
        'city',
        'zip_code',
        'state_or_country',
        'state_or_country_description',
        'created_at',
        'updated_at'
    ];
}
