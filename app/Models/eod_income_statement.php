<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class eod_income_statement extends Model
{
    use HasFactory;

    protected $table = 'eod_income_statements';
    protected $fillable = ['id', 'company_id', 'cik','ticker', 'period_type', 'period_date', 'date', "filing_date", "currency_symbol", "researchDevelopment", "effectOfAccountingCharges", "incomeBeforeTax", "minorityInterest", "netIncome", "sellingGeneralAdministrative", "sellingAndMarketingExpenses", "grossProfit", "reconciledDepreciation", "ebit", "ebitda", "depreciationAndAmortization", "nonOperatingIncomeNetOther", "operatingIncome", "otherOperatingExpenses", "interestExpense", "taxProvision", "interestIncome", "netInterestIncome", "extraordinaryItems", "nonRecurring", "otherItems", "incomeTaxExpense", "totalRevenue", "totalOperatingExpenses", "costOfRevenue", "totalOtherIncomeExpenseNet", "discontinuedOperations", "netIncomeFromContinuingOps", "netIncomeApplicableToCommonShares", "eps", "dol", "dfl", "dtl", "preferredStockAndOtherAdjustments", "created_at", "updated_at"];

    public const TITLES = [
    	['column' => 'totalRevenue', 'alert_title' => 'income_statement_alert', 'title' => 'Total Revenue', 'indentation' => 0],
    	['column' => 'costOfRevenue', 'alert_title' => 'income_statement_alert', 'title' => 'Cost of Revenue', 'indentation' => 4],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

    	['column' => 'grossProfit', 'alert_title' => 'income_statement_alert', 'title' => 'Gross Profit', 'indentation' => 0],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        ['column' => 'totalOperatingExpenses', 'alert_title' => 'income_statement_alert', 'title' => 'Total Operating Expenses', 'indentation' => 0],
        ['column' => 'sellingGeneralAdministrative', 'alert_title' => 'income_statement_alert', 'title' => 'Selling General and Administrative', 'indentation' => 4],
        ['column' => 'researchDevelopment', 'alert_title' => 'income_statement_alert', 'title' => 'Research & Development', 'indentation' => 4],
        ['column' => 'depreciationAndAmortization', 'alert_title' => 'income_statement_alert', 'title' => 'Depreciation and Amortization', 'indentation' => 4],
    	
        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

    	['column' => 'operatingIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Operating Income', 'indentation' => 0],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        ['column' => 'interestIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Income', 'indentation' => 4],
        ['column' => 'nonOperatingIncomeNetOther', 'alert_title' => 'income_statement_alert', 'title' => 'Other Non-Operating Income (Net)', 'indentation' => 4],

        ['column' => 'nonRecurring', 'alert_title' => 'income_statement_alert', 'title' => 'Non-Recurring Items', 'indentation' => 4],
        ['column' => 'totalOtherIncomeExpenseNet', 'alert_title' => 'income_statement_alert', 'title' => 'Total Other Income Expense (Net)', 'indentation' => 4],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        ['column' => 'ebitda', 'alert_title' => 'income_statement_alert', 'title' => 'EBITDA', 'indentation' => 0],
        ['column' => 'depreciationAndAmortization', 'alert_title' => 'income_statement_alert', 'title' => 'Depreciation and Amortization', 'indentation' => 4],
        ['column' => 'ebit', 'alert_title' => 'income_statement_alert', 'title' => 'EBIT', 'indentation' => 0],
    	['column' => 'interestExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Interest Expense', 'indentation' => 4],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        ['column' => 'incomeBeforeTax', 'alert_title' => 'income_statement_alert', 'title' => 'Income Before Tax', 'indentation' => 0],
        ['column' => 'incomeTaxExpense', 'alert_title' => 'income_statement_alert', 'title' => 'Income Tax Expense', 'indentation' => 4],

        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],

        ['column' => 'netIncome', 'alert_title' => 'income_statement_alert', 'title' => 'Net Income', 'indentation' => 0],
        ['column' => 'netIncomeFromContinuingOps', 'alert_title' => 'income_statement_alert', 'title' => 'Net Income From Continuing Operations', 'indentation' => 4],
        ['column' => 'preferredStockAndOtherAdjustments', 'alert_title' => 'income_statement_alert', 'title' => 'Preferred Stock and Other Adjustments', 'indentation' => 8],

        ['column' => 'netIncomeApplicableToCommonShares', 'alert_title' => 'income_statement_alert', 'title' => 'Net Income Applicable to Common Shareholders', 'indentation' => 4],
    	
        ['column' => '', 'alert_title' => '', 'title' => '', 'indentation' => 0],
        
    	['column' => 'eps', 'alert_title' => 'income_statement_alert', 'title' => 'EPS', 'indentation' => 0],

        ['column' => 'dol', 'alert_title' => 'income_statement_alert', 'title' => 'Degree of Operating Leverage', 'indentation' => 0],
        ['column' => 'dfl', 'alert_title' => 'income_statement_alert', 'title' => 'Degree of Financial Leverage', 'indentation' => 0],
        ['column' => 'dtl', 'alert_title' => 'income_statement_alert', 'title' => 'Degree of Total Leverage', 'indentation' => 0],

    ];
}