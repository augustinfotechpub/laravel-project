<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Survey extends Model
{

    protected $table = 'survey';

    protected $fillable = ['email', 'age', 'full_address', 'fav_feature', 'which_feature', 'why_not', 'for_living', 'not_fav_feature', 'which_feature_not', 'why_all', 'rating', 'pay_for_app', 'how_much', 'no_money', 'annually_amount', 'list_function_n', 'list_function_add', 'is_helpfull', 'how_much_helpful', 'not_helpfull', 'easy_to_use', 'why_easy', 'why_not_easy', 'how_many_exp', 'how_many_exp_in_stock', 'created_at', 'updated_at'];

}