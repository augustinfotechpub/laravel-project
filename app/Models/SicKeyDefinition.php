<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SicKeyDefinition extends Model
{
    use HasFactory;

    protected $table = "sic_key_definitions";
    protected $fillable = ['sic_id', 'key_definition_id'];
    public $timestamps = false;


    public function sic(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {

        return $this->belongsTo(Sic::class);
    }

    public function definition(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Definition::class);
    }
}

