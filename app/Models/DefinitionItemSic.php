<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefinitionItemSic extends Model
{
    use HasFactory;

    protected $table = 'definition_item_sics';
    protected $fillable = ['id', 'definition_id', 'definition_item_id', 'sic', 'created_at', 'updated_at'];


    public function definition()
    {
        return $this->hasOne(Definition::class,'id','definition_id');
    }
}
