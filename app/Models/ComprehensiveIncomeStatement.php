<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComprehensiveIncomeStatement extends Model
{
    use HasFactory;

    protected $table = 'comprehensive_income_statements';

    protected $fillable = [
        'id',
        'company_id',
        'symbol',
        'period',
        'title',
        'html',
        'created_at',
        'updated_at'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
