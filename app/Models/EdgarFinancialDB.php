<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EdgarFinancialDB extends Model
{
    use HasFactory;
    protected $table = 'edgar_financial';
    protected $fillable = ['id','company_id', 'taxonomyid', 'cik', 'companyname', 'entityid', 'primaryexchange', 'marketoperator', 'markettier', 'primarysymbol', 'siccode', 'sicdescription', 'usdconversionrate', 'restated', 'receiveddate', 'preliminary', 'periodlengthcode', 'periodlength', 'periodenddate', 'original', 'formtype', 'fiscalyear', 'fiscalquarter', 'dcn', 'currencycode', 'crosscalculated', 'audited', 'amended', 'changeincurrentassets', 'changeincurrentliabilities', 'changeininventories', 'dividendspaid', 'capitalexpenditures', 'cashfromfinancingactivities', 'cashfrominvestingactivities', 'cashfromoperatingactivities', 'cfdepreciationamortization', 'changeinaccountsreceivable', 'investmentchangesnet', 'netchangeincash', 'totaladjustments', 'ebit', 'costofrevenue', 'grossprofit', 'incomebeforetaxes', 'netincome', 'netincomeapplicabletocommon', 'researchdevelopmentexpense', 'totalrevenue', 'sellinggeneraladministrativeexpenses', 'commonstock', 'cashandcashequivalents', 'cashcashequivalentsandshortterminvestments', 'inventoriesnet', 'otherassets', 'othercurrentassets', 'othercurrentliabilities', 'otherliabilities', 'propertyplantequipmentnet', 'retainedearnings', 'totalassets', 'totalcurrentassets', 'totalcurrentliabilities', 'totalliabilities', 'totallongtermdebt', 'totalreceivablesnet', 'totalshorttermdebt', 'totalstockholdersequity'];
}
