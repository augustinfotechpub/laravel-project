<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MapperCikName extends Model
{
    use HasFactory, Search;

    protected $searchable = [
        'cik',
        'name',
    ];

    protected $table = 'mapper_cik_names';

    protected $fillable = ['id', 'cik', 'name', 'created_at', 'updated_at'];

    public static function search(string $search)
    {
        $columns = ["cik", "name"];
        // If the search is empty, return everything
        if (empty(trim($search))) {
            return static::select($columns)->get();
        } // If the search contains something, we perform the fuzzy search
        else {
            $fuzzySearch = implode("%", str_split($search)); // e.g. test -> t%e%s%t
            $fuzzySearch = "%$fuzzySearch%"; // test -> %t%e%s%t%s%
            return static::select($columns)->where("name", "like", $fuzzySearch)->get();
        }
    }
}
