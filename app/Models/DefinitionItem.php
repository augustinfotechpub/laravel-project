<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefinitionItem extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'definition_items';
    protected $fillable = ['id', 'definition_id', 'sub_title', 'type','identifier', 'content', 'created_at', 'updated_at'];


    public function sics()
    {
        return $this->hasMany(DefinitionItemSic::class, 'definition_item_id', 'id');
    }

    public function tags()
    {
        return $this->hasMany(DefinitionItemTag::class, 'definition_item_id', 'id');
    }

    public function symbols()
    {
        return $this->hasMany(DefinitionItemSymbol::class, 'definition_item_id', 'id');
    }
}

