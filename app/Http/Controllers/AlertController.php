<?php

namespace App\Http\Controllers;

use App\Models\Definition;
use App\Models\DefinitionItem;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    public function getAlert($identifier)
    {
        try {
            $alertData = DefinitionItem::query()->where(['type' => 'definition', 'identifier' => $identifier])->first();

            if (is_null($alertData))
                throw new \Exception('Alert Content not found.');

            $content = $alertData->content;
            return response()->json([
                'status' => 200,
                'message' => 'Alert Definitions',
                'data' => [
                    'id' => $identifier,
                    'content' => $content,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => ['identifier' => $identifier]]);
        }
    }
}
