<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 19-12-2020
 * Time: 12:34 AM
 */

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Services\FinancialModelingPrep;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\SiteConfiguration;
use Illuminate\Support\Facades\Auth;
use App\Services\SecEdgarService;
use App\Models\Company;
use App\Models\IncomeStatement;
use App\Models\eod_income_statement;
use Illuminate\Support\Carbon;
use App\Models\CashFlowStatement;
use App\Models\eod_cash_flow;
use App\Models\BalanceSheetStatement;
use App\Models\eod_balance_sheet;
use App\Models\Funds;
use App\Models\OwnershipInterest;
use App\Interfaces\Front\UserInterface;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Mail\SendMail;
use App\Models\User;


// use Carbon\Carbon;

class HomeController extends Controller
{
    protected $fmp;
    public $symbol,$profile,$summary,$institutionalShareholders;

    public function __construct(FinancialModelingPrep $financialModelingPrep,UserInterface $userRepository)
    {
        $this->fmp = $financialModelingPrep;
        $this->userRepository = $userRepository;
    }
     
     public function index()
    {   
         $user_auth = \auth()->user();
        if( !empty( $user_auth) )
        {
            if( $user_auth->is_intro ==  0 )
            {
                return redirect()->route('user.introduction')->with('success', 'Please read the introduction, and press continue.');
            }else if( $user_auth->is_disclosure ==  0 )
            {
                return redirect()->route('user.disclosure')->with('success', 'Please read the DISCLOSURE, and press continue.');
            }
        }
        return view('front.home.index');
    }

    public function landing()
    {
        return view('front.home.landing');
    }
    
    public function app(Request $request)
    {
        if (is_null(session('profile')))
            return redirect()->route('home');

        return view('front.layouts.innerpage');
    }

    public function chart($symbol)
    {
        $profile = $this->fmp->getProfile($symbol)[0];

        return view('front.appmain.chart', compact('symbol', 'profile'));
    }

    public function pdfTranscript($symbol)
    {
        $fmp = new FinancialModelingPrep();
        $res = $fmp->getEarningCallTranscript($symbol)[0];
        $data = [];
        $content = explode(': ', $res['content']);
        foreach ($content as $index => $item) {
            if ($index == 0) {
                continue;
            }
            $arr1 = explode('.', $content[$index - 1]);
            $speaker = end($arr1);
            if (str_contains(',', $speaker)) {
                $arr1 = explode(',', $speaker);
                $speaker = end($arr1);
            }
            if (strpos($speaker, "?") !== false) {
                $arr1 = explode('?', $speaker);
                $speaker = end($arr1);
            }
            if (strpos($speaker, "-") !== false) {
                $arr1 = explode('-', $speaker);
                $speaker = end($arr1);
            }
            $data[] = ['speaker' => $speaker, 'speech' => $item];
        }
        foreach ($data as $index => $item) {
            $speech = isset($data[$index + 1]['speaker']) ? str_replace($data[$index + 1]['speaker'], '', $item['speech']) : $item['speech'];
            $data[$index] = ['speaker' => trim($item['speaker']), 'speech' => $speech];
        }
        $res['content'] = $data;
        $pdf = PDF::loadView('front.pdf', compact('res'));
        return $pdf->stream('EarningTranscript.pdf');
    }

    public function showEarningTranscript($symbol, $quarter, $year)
    {

        $fmp = new FinancialModelingPrep();
        $res = $fmp->getEarningCallTranscript($symbol, $quarter, $year)[0];
        $data = [];
        $content = explode(': ', $res['content']);
        foreach ($content as $index => $item) {
            if ($index == 0) {
                continue;
            }
            $arr1 = explode('.', $content[$index - 1]);
            $speaker = end($arr1);
            if (str_contains(',', $speaker)) {
                $arr1 = explode(',', $speaker);
                $speaker = end($arr1);
            }
            if (strpos($speaker, "?") !== false) {
                $arr1 = explode('?', $speaker);
                $speaker = end($arr1);
            }
            if (strpos($speaker, "-") !== false) {
                $arr1 = explode('-', $speaker);
                $speaker = end($arr1);
            }
            $data[] = ['speaker' => $speaker, 'speech' => $item];
        }
        foreach ($data as $index => $item) {
            $speech = isset($data[$index + 1]['speaker']) ? str_replace($data[$index + 1]['speaker'], '', $item['speech']) : $item['speech'];
            $data[$index] = ['speaker' => trim($item['speaker']), 'speech' => $speech];
        }
        $res['content'] = $data;
        return view('front.view_earning_transcript', ['earningCallTranscript' => $res]);
    }

    public function stockDetails()
    {
        try {

            $currentDate = null;
            $fmp = new FinancialModelingPrep();
            $profile = session('profile');
            $quote =(array) $fmp->quote($profile['symbol'])[0];
           
            $currentDate = date("m/d/Y", strtotime( Carbon::now() ) );

            $profile = array_merge($quote, $profile);
            $view = view('livewire.partials.stock-details-template', compact(['profile', 'currentDate']))->render();
            return response()->json(['status' => 200, 'message' => 'Stock Details Fetched Successfully.', 'data' => compact('view')]);
            }

      
        catch (\Exception $exception) {
            return response()->json(['status' => 100, 'message' => $exception->getMessage(), 'data' => []]);
        }
    }
    
    
    public function searchCompany(Request $request){



        $query = $request->searchKey;
        if( empty($query) ){
            return [];
        }


        $bestMatches = DB::table("companies as com")
            ->join('symbols as sy', function ($join) {
                $join->on('sy.company_id', '=', 'com.id');
            })
            ->select("com.id", "com.cik", "com.name", "sy.symbol")
            ->orWhere("com.name", "like", "%$query%")
            ->orWhere("com.cik", "like", "%$query%")
            ->orWhere("sy.symbol", "like", "%$query%")
            ->orWhere("sy.symbol", "like", "%$query%")
            ->orWhere("com.description", "like", "%$query}%")
            ->orWhere("com.sic_description", "like", "%$query%")
            ->orWhere("com.eod_description", "like", "%$query%")
            ->orderby('com.name','asc')
            ->get()
            ->toArray();
            $company_results = [];
            foreach ($bestMatches as $bestMatchdata) {
                $company_results[] = [
                    'companyNameLong' => $bestMatchdata->name,
                    'ticker' => $bestMatchdata->symbol,
                    'cik' => $bestMatchdata->cik
                ];
                    
            }
        try {
            if( !empty($company_results))
            {
                $returnResponse = $company_results;
            }else {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.sec-api.io/full-text-search?token='.config('services.edgar_sec_api.api_key'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                "query": "'.$query.'"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                ),
                ));
                
                $response = curl_exec($curl);
                $response = json_decode($response);
                $returnResponse = [];
                if( isset($response->filings))
                {
                    $returnResponse = $response->filings;
                }else {
                    $returnResponse = [];
                }
            } 

            return response()->json($returnResponse);
        } catch (\Exception $e){
            return response()->json($returnResponse);;
        }  
    }

    protected function getUpcomingEarningsDate($symbol)
    {
        $date = now()->toDateString();
        $dates = IncomeStatement::query()->where('symbol', $symbol)
            ->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4'])
            ->orderByDesc('date')->pluck('date')->take(4)->toArray();
        foreach ($dates as $index => $date) {
            $dates[$index] = Carbon::parse($date)->addYear()->toDateString();
        }

        usort($dates, function ($a, $b) {
            return strtotime($a) - strtotime($b);
        });
        foreach ($dates as $count => $dateSingle) {
            if (strtotime($date) < strtotime($dateSingle)) {
                $nextDate = date('m/d/Y', strtotime($dateSingle));
                break;
            }
        }
        return $nextDate ?? null;
    }


    public function selectCompany(Request $request){

        $companyCik= urldecode($request->companyCik);
        $bestMatches = Company::where('cik', $companyCik)->get()->toArray();

        if( !isset( $bestMatches[0]['symbols'][0]['symbol'] ) || empty( $bestMatches[0]['symbols'][0]['symbol'] ) ){
            return redirect()->back()->with('error', 'Our database doesn’t contain information for this company');
        }

        $fmp = new FinancialModelingPrep();
        $profile = $fmp->getProfile(@$bestMatches[0]['symbols'][0]['symbol']);
        if(!isset($profile) || empty($profile))
        {
            return redirect()->back()->with('error', 'Our database doesn’t contain information for this company');
        }
        $this->profile = $profile[0];
        $this->profile['upcomingEarningsDate'] = $this->getUpcomingEarningsDate(@$bestMatches[0]['symbols'][0]['symbol']);
        $quote = (array) @$fmp->quote(@$bestMatches[0]['symbols'][0]['symbol'])[0];

        if( $quote == null || empty($quote) ){
            return redirect()->back()->with('error', 'Our database doesn’t contain information for this company');
        }

        $this->profile['sharesOutstanding'] = $quote['sharesOutstanding'];
        $company = Company::query()->whereIn('cik', [ltrim($this->profile['cik'], 0), $this->profile['cik']])->first();
        if (is_null($company)) {
            return redirect()->back()->with('error', 'Our database doesn’t contain information for this company');
            
        }
        $this->profile['sic'] = $company['sic'];
        session(['profile' => $this->profile]);
        return redirect(route('app_main'));
    }

    public function yearDatabs(Request $request)
    {
        $this->symbol = session('profile')['symbol'];
        $this->summary= eod_balance_sheet::where('ticker', $this->symbol)
        ->where('date','like',$request->year.'%')
        ->where('period_type', 'yearly')
        ->get();
        $summary=$this->summary->toJson();
        return view('livewire.financials.key-stats.b',compact('summary'));
    }

    public function yearDatais(Request $request)
    {
        $this->symbol = session('profile')['symbol'];
        $this->summary= eod_income_statement::where('ticker', $this->symbol)
        ->where('date','like',$request->year.'%')
        ->where('period_type', 'yearly')
        ->get();
        $summary=$this->summary->toJson();
        return view('livewire.financials.key-stats.i',compact('summary'));
    }

    public function yearDatacf(Request $request)
    {
        $this->symbol = session('profile')['symbol'];
        $this->summary= eod_cash_flow::where('ticker', $this->symbol)
        ->where('date','like',$request->year.'%')
        ->where('period_type', 'yearly')
        ->get();
        $summary=$this->summary->toJson();
        return view('livewire.financials.key-stats.c',compact('summary'));
    }

    public function holder(Request $request)
    {
        $profile = session('profile');
        $cik = $profile['cik'];

        if($request->t=='Funds')
        {
        $this->institutionalShareholders = Funds::query()->whereIn('company_cik', [$cik, ltrim($cik,0)])->get(); 
        $institutionalShareholders=$this->institutionalShareholders->toJson();
        return view('livewire.key-parties.k',compact('institutionalShareholders'));
        }
        else{
            $this->institutionalShareholders = OwnershipInterest::query()->whereIn('company_cik', [$cik, ltrim($cik,0)])->get(); 
        $institutionalShareholders=$this->institutionalShareholders->toJson();
        return view('livewire.key-parties.k',compact('institutionalShareholders'));
        }
    }

    public function checkbox($check)
    {

        if( $check != 1 ){
            return redirect()->back()->with('error', 'Operation failed');
        }

        $current_user = \auth()->user();
        $profile = $this->userRepository->getById($current_user->id);
        $profile->testing_period=$check;
        $profile->save();
        return redirect()->route('home');
    }
    
 }
