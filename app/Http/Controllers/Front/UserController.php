<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\StoreUserRequest;
use App\Http\Requests\Front\UpdateProfileRequest;
use App\Interfaces\Front\CustomerSubscriptionInterface;
use App\Interfaces\Front\SubscriptionPlanInterface;
use App\Interfaces\Front\UserInterface;
use App\Models\User;
use App\Services\Stripe;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\UriResolver;
use App\Models\SiteConfiguration;
use App\Models\Survey;
use App\Events\Front\EmailVerification;
use Illuminate\Support\Facades\Session;
use App\Events\Front\MonthlysurveyEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use App\Events\Front\SubscriptionAlertEmail;
use App\Models\CustomerSubscription;
use App\Models\CancelSubscription;
use App\Models\Admin;
use App\Events\Front\CancelSubscriptionAlertNotificationEmail;
use App\Events\Front\CancelSubscriptionAdminEmail;


class UserController extends Controller
{
    protected $subscriptionPlanRepository, $customerSubscriptionRepository, $userRepository, $stripService;

    public function __construct(SubscriptionPlanInterface $subscriptionPlanRepository,
                                UserInterface $userRepository, CustomerSubscriptionInterface $customerSubscriptionRepository, Stripe $stripRepository)
    {
        $this->subscriptionPlanRepository = $subscriptionPlanRepository;
        $this->stripService = $stripRepository;
        $this->userRepository = $userRepository;
        $this->customerSubscriptionRepository = $customerSubscriptionRepository;
    }

    public function register()
    {
        $plans = $this->subscriptionPlanRepository->getAll();
        if ($plans->isEmpty()) {
            return back()->with('warning', 'Sorry! There are no subscription plans added at the moment.');
        }
        return view('front.auth.registration');
    }
    
    


    public function store(StoreUserRequest $request)
    {
        $requestData = $request->all();
        $requestData['phone_number'] = cleanString($requestData['phone_number']);
        $requestData['password'] = isset($requestData['password']) ? bcrypt($requestData['password']) : null;
        $customToken = Str::random(12);
        $requestData['remember_token'] = $customToken;
        $requestData['email_verify_token'] = $customToken;
        $requestData['name'] = @$requestData['first_name'] . " " .$requestData['last_name'];
        $requestData['is_active'] = 0;

        $user = $this->userRepository->store($requestData);

        event(new EmailVerification($user));

        return redirect()->route('user.introduction')->with('success', 'Please verify yourself with the verification link sent to your email address, Please Read introduction and press continue')->with('user',$user);
        // print_r($user);
        //return view('front.home.introduction', compact('user'));
        // Auth::loginUsingId($user->id);
        // return redirect()->back()->with('success', 'Please verify yourself with the verification link sent to your email address, Read introduction and press continue')->with('user',);
        // return redirect()->route('user.subscription')->with('success', 'Your are successfully registered.');
    }

    public function welcome()
    {
        $welcomeVideo = SiteConfiguration::find(11);
        $welcomeText = SiteConfiguration::find(10);
        // $welcome_video = $welcomeVideo->value;
        // $welcome_text = $welcomeText->value;
        if( !isset($welcomeVideo->value) && empty($welcomeVideo->value) ){
            $welcome_video = '';
            } else {
            $welcome_video = $welcomeVideo->value;
            }
        if( !isset($welcomeText->value) && empty($welcomeText->value) ){
            $welcome_text = '';
            } else {
            $welcome_text = $welcomeText->value;
            }     
        
        return view('front.home.welcome', compact('welcome_text', 'welcome_video'));
    }

    public function emailVerify($token = null)
    {
        if ($token == null) {
            return redirect()->route('home');
        }
        Auth::logout();
        $user = User::where(['email_verify_token' => $token, 'status' => false])->first();

        if (is_null($user)) {
            return redirect()->route('user.login')->with('error', 'Your account is already verified, Try to login.');
        }
        if (Auth::loginUsingId($user->id)) {
            $this->userRepository->update(['status' => true, 'is_beta' => 1, 'is_active' => 1, 'email_verified_at' => date('Y-m-d H:i:s')], $user->id);
            return redirect()->route('user.profile')->with('success', 'Your Account Verified successfully.');
        }
        return redirect()->route('home')->with('error', 'Something Went wrong, Try to login.');
    }


    public function profile()
    {
        $current_user = \auth()->user();
        $profile = $this->userRepository->getById($current_user->id);
        $subscription = $this->customerSubscriptionRepository->getCustomerSubscription($current_user->id);
        $cancelQuestion = $this->cancelSurveyquestion();
        return view('front.profile.index', compact('profile', 'subscription','cancelQuestion'));
    }
    public function introduction(){
        $userauth = \auth()->user();
        if( $userauth && $userauth->is_intro != 0)
        {
            return redirect()->route('home')->with('success', 'User already submited Introduction form.');
        }
        $user = Session::get('user');
        if( !$userauth && !$user )
        {
            return redirect()->route('user.register')->with('success', 'Please Register.');
        }
        return view('front.home.introduction', compact('userauth'));
    }

   public function submitIntroduction(Request $request)
   {
       $get_user_id = $request->input('userid');
       $userauth = \auth()->user();   
       if($userauth)
       {
            $this->validate($request,['is_intro'=>'required',]);
            $user = User::where('id',$userauth->id)->update(['is_intro' => 1]);
            return redirect()->route('user.disclosure')->with('success', 'Please read the disclosure, then click the box at the bottom of the page');
       }
       if($get_user_id)
       {
            $user = $this->userRepository->getById($get_user_id);
            $this->validate($request,['is_intro'=>'required',]);
            if($user)
            {
                $check_update = User::where('id',$user->id)->update(['is_intro' => 1]);
                return redirect()->route('user.disclosure')->with('user',$user)->with('success', 'Please read the disclosure, then click the box at the bottom of the page');    ;
            }else{
                return redirect()->route('user.login')->with('success', 'Thank you for reading the initial information and agreeing to the legal disclosure');    
            }
       }
       
   }
  
    public function disclosure()
    {
        $userauth = \auth()->user();
        if( $userauth && $userauth->is_disclosure != 0)
        {
            return redirect()->route('home')->with('success', 'User already submited disclosure form.');            
        }
        $user = Session::get('user');
        if( !$userauth && !$user )
        {
            return redirect()->route('user.login')->with('success', 'Please login.');
        }
        return view('front.home.disclosure', compact('userauth'));
    }

    public function submitDisclosure(Request $request){
        $get_user_id = $request->input('userid');
        $userauth = \auth()->user();
        if($userauth)
        {
            $this->validate($request,['is_disclosure'=>'required']);
            $user = User::where('id',$userauth->id)->update(['is_disclosure' => 1]);
            return redirect()->route('home')->with('success', 'Please verify your account via the email account used to register');
        }

        if($get_user_id)
        {
            $this->validate($request,['is_disclosure'=>'required']);
            $user = User::where('id',$get_user_id)->update(['is_disclosure' => 1]);
            return redirect()->route('home')->with('success', 'Registration successful.');
        }
    }

    public function editProfile()
    {
        $profile = \auth()->user();
        return view('front.profile.edit', compact('profile'));
    }

    public function updateProfile(UpdateProfileRequest $request)
    {

        $this->userRepository->update($request->all(), $request->user_id);
        return redirect()->route('home')->with('success', 'Profile updated successfully.');
    }

    public function heatMap()
    {
        $client = new Client();
        $response = $client->request('GET', 'https://finviz.com/map.ashx?t=sec_all');
        // UriResolver::resolve('/fonts/lato-v17-latin-ext_latin-regular.woff2', '//finviz.com/'); // //finviz.com/

        $html = $response->html();

        $html = str_replace('<script src="/js/dfp.min.js"></script>', '', $html);
        $html = str_replace('<link rel="preload" href="/fonts/lato-v17-latin-ext_latin-regular.woff2" as="font" crossorigin>
            <link rel="preload" href="/fonts/lato-v17-latin-ext_latin-700.woff2" as="font" crossorigin>', '', $html);


        $html = str_replace('href="/', 'href="https://finviz.com/', $html);
        $html = str_replace('src="/', 'src="https://finviz.com/', $html);
        $html = str_replace("document.getElementById('map-script').appendChild(script);", '', $html);
        $html = str_replace("https://finviz.com/fonts/lato-v17-latin-ext_latin-700.woff2", '', $html);
        $html = str_replace("https://finviz.com/fonts/lato-v17-latin-ext_latin-regular.woff2", '', $html);
        $heatMap = $html;
        /*dd($heatMap);*/
        return view('front.appmain.partials.heatmap', compact('heatMap'));
    }
   
    public function submitSurvey(){
        return view('front.home.survey');
    }

    public function requestActivate($token = null){
        if( is_null($token) || empty($token) ){
            return redirect()->route('home');
        }

        $tokenMail = decryptMailEnc($token);

        $userData = User::where('email', $tokenMail);
        if( $userData->get()->isEmpty() ){
            return redirect()->route('home');
        }

        // $userDataFinal = $userData->get();
        $userData->update(['activate_my_account' => '1']);

        return redirect()->route('home')->with('success', 'Account activation request registered');
    }

    public function saveSurvey(Request $request){

        $data = ['email' => @$request->email, 'age' => @$request->age, 'full_address' => @$request->full_address, 'fav_feature' => @$request->fav_feature, 'which_feature' => @$request->which_feature, 'why_not' => @$request->why_not, 'for_living' => @$request->for_living , 'not_fav_feature' => @$request->not_fav_feature, 'which_feature_not' => @$request->which_feature_not, 'why_all' => @$request->why_all, 'rating' => @$request->rating, 'pay_for_app' => @$request->pay_for_app, 'how_much' => @$request->how_much, 'no_money' => @$request->no_money, 'annually_amount' => @$request->annually_amount, 'list_function_n' => @$request->list_function_n, 'list_function_add' => @$request->list_function_add, 'is_helpfull' => @$request->is_helpfull, 'how_much_helpful' => @$request->how_much_helpful, 'not_helpfull' => @$request->not_helpfull, 'easy_to_use' => @$request->easy_to_use, 'why_easy' => @$request->why_easy, 'why_not_easy' => @$request->why_not_easy, 'how_many_exp' => @$request->how_many_exp, 'how_many_exp_in_stock' => @$request->how_many_exp_in_stock];

        $Survey = Survey::insert($data);
            
        return redirect()->route('home')->with('success', 'Survey is complete');
    }

    public function monthly_survey(Request $request)
    {          
        $timeis=date('m-d-Y', time());
        $first_day_this_month = date('m-01-Y');
        
        if($timeis==$first_day_this_month){

            $get_users = $this->userRepository->getAllUsers();
            $get_users->each(function($user)
            {
                event(new MonthlysurveyEmail($user));
            });
            return "Mail triggerd";
        } 
        else{
            return "it is not 1st date of month.. try again later";
        }
    }

    public function subscriptionAlert(Request $request)
    {
        $startdate = date('Y-m-d 00:00:00',strtotime('+1 days'));
        $enddate = date('Y-m-d 23:59:59',strtotime('+1 days'));

        $custsubdata = CustomerSubscription::select('customer_id')
        ->where('end_date','>=',$startdate)
        ->where('end_date','<=',$enddate)
        ->get();
      
        if($custsubdata)
        {
            foreach($custsubdata as $cussubsingle)
            {
              
                $get_users = $this->userRepository->getById($cussubsingle->customer_id);
                
                event(new SubscriptionAlertEmail($get_users));
                print_r ($get_users->email.' email send successfully.<br>');
            
            }
        }
    }

    public function saveCancellationSurvey(Request $request){
        
        $user = \auth()->user(); //user details
        $surveyformdata = $request->all(); // form action value
        $other = $request->get('other');
        $questions = $request->get('questions');
        $cancelSurveyValues = [ 'Userid' => $user['id'],'subscription_id' =>$user['current_subscription_id'],
                                'user_name' =>$user['first_name'].' '.$user['last_name'],
                                'answer' => serialize($questions), 'Message' => $other
                            ];
        $CancelSubscription = CancelSubscription::insert($cancelSurveyValues);
        $admin = Admin::first();
        
        
        event(new CancelSubscriptionAlertNotificationEmail($user));
        event(new CancelSubscriptionAdminEmail($admin,$user));
        $changeStatus = CustomerSubscription::where('customer_id',$user->id)->update(['status' => 'cancel']);

        return redirect()->route('user.profile')->with('success', 'Your Subscription has been cancelled');
    }
    public function cancelSurveyquestion(){
        return array(
        '1'=>'What made you question if Finnfrastructure was a good fit for you?',
        '2'=>'When did you decide to cancel, and was there anything that prompted the decision?',
        '3'=>'What service are you using now?',
        '4'=>'Is there anything we could have done that would have prevented you from canceling?',
        '5'=>'Would you be open to signing up again if your main issue was no longer an issue?',
        '6'=>'Other',
        );
    }

}
