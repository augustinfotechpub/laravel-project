<?php

namespace App\Http\Controllers\Front;

use App\Events\Front\WelcomeEmail;
use App\Http\Controllers\Controller;
use App\Interfaces\Front\CustomerSubscriptionInterface;
use App\Interfaces\Front\SubscriptionPlanInterface;
use App\Interfaces\Front\UserInterface;
use App\Services\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\CustomerSubscription;
use App\Events\Front\SubscriptionAlertEmail;

class SubscriptionPlanController extends Controller
{
    protected $subscriptionPlanRepository, $stripService, $customerSubscriptionRepository, $userRepository;

    public function __construct(SubscriptionPlanInterface $subscriptionPlanRepository, Stripe $stripService, CustomerSubscriptionInterface $customerSubscriptionRepository, UserInterface $userRepository)
    {
        $this->subscriptionPlanRepository = $subscriptionPlanRepository;
        $this->stripService = $stripService;
        $this->customerSubscriptionRepository = $customerSubscriptionRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $plans = $this->subscriptionPlanRepository->getAll();
        if ($plans->isEmpty()) {
            return redirect()->route('home')->with('warning', 'Sorry! There are no subscription plans added at the moment.');
        }
        $current_plan = $this->subscriptionPlanRepository->getCurrentPlan($request->current_plan_id ?? null);
        return view('front.subscriptions.make_subscription', compact('plans', 'current_plan'));
    }

    public function makeSubscription(Request $request)
    {
        $requestData = $request->all();
        $user = auth()->user();
        $cus_id = 'cus_' . Str::random(16);
        $plan = $this->subscriptionPlanRepository->getById(($request->plan));

        // Create stripe customer
        $stripe_customer_res = $this->stripService->createCustomer([
            'id' => $cus_id,
            'name' => $request->first_name . ' ' . $request->last_name ?? null,
            'email' => $request->email_address,
            'phone' => $request->phone_number]);
        $this->stripService->makePaymentIntent(['amount' => $plan->price]);
        $this->stripService->createSource($stripe_customer_res->id, $requestData['stripeToken']);
        $stripe_subscription_res = $this->stripService->makeSubscription($stripe_customer_res->id, $plan->price_key);

        //store customer subscription
        $requestData['stripe_customer_id'] = $stripe_customer_res->id;
        $customer_subscription_res = $this->customerSubscriptionRepository->getCustomerSubscriptionRequest($stripe_subscription_res, $plan, $user->id);
        $requestData = array_merge($requestData, $customer_subscription_res);
        $customer_subscription = $this->customerSubscriptionRepository->store($requestData);

        //update user
        $requestData['active_subscription'] = true;
        $requestData['status'] = true;
        $requestData['current_subscription_id'] = $customer_subscription->id;
        $this->userRepository->update($requestData, $user->id);

        // Subscription detail send on user email
        $subscriptionData = ['plan_title' => $plan->title, 'price' => formatPrice($plan->price),];
        $subscriptionData['duration'] = formatDate($customer_subscription->start_date) . ' To ' . formatDate($customer_subscription->end_date);
        event(new WelcomeEmail(['user' => $user, 'subscription' => $subscriptionData]));

        return redirect()->route('user.profile')->with('success', 'Your subscription successfully started.');
    }

    public function changePlan(Request $request)
    {
        if ($request->ajax()) {
            $current_plan = $this->subscriptionPlanRepository->getCurrentPlan($request->plan_id);
            $html = view('front.subscriptions.partials.checkout-button', compact('current_plan'))->render();
            return response()->json(['html' => $html]);
        }
        return $request->all();

    }

    public function dailySubscriptionExpire(Request $request)
    {
        
        $startdate = date('Y-m-d 00:00:00');
        $enddate = date('Y-m-d 23:59:59');
       //get daily subscription query expired

        $custsubdata = CustomerSubscription::select('customer_id','end_date')
        ->where('end_date','>=',$startdate)
        ->where('end_date','<=',$enddate)
        ->get();
    
        if($custsubdata)
        {
        
            foreach($custsubdata as $cussubsingle)
            {
              // get customer data using by customer_id from user object
                $get_users = $this->userRepository->getById($cussubsingle->customer_id);
                $requestData['active_subscription'] = false;
                // active_subscription status update by using customer_id
                $this->userRepository->update($requestData, $cussubsingle->customer_id);
                $get_users->expired_date = date('Y-m-d',strtotime($cussubsingle->end_date));
                event(new SubscriptionAlertEmail($get_users));
                print_r ($get_users->email.' email send successfully.<br>');
            
            }
        }
    }
}
