<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Interfaces\Front\CustomerSubscriptionInterface;
use App\Interfaces\Front\UserInterface;
use App\Models\CustomerSubscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    protected $customerSubscriptionRepository, $userRepository;

    public function __construct(CustomerSubscriptionInterface $customerSubscriptionRepository, UserInterface $userRepository)
    {
        $this->customerSubscriptionRepository = $customerSubscriptionRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return view('front.auth.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|exists:users,email',
            'password' => 'required|string|min:6',
        ]);
        $user = User::where('email', $request->email)->first();
        // $currentDate = date("Y-m-d H:i:s");
        // $specifiedDate = date("Y-m-d H:i:s", strtotime(config("app.BetaExpiry")));
        // if the specified time is greater than the current time, do not let beta users to login
        // if($currentDate > $specifiedDate){
            if(isset($user->is_active) && $user->is_active != 1){
                return redirect()->back()->with('error', 'Please activate your account');
            }
        //  }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->route('home');
        }
        return redirect()->back()->with('error', 'Oops! You have entered invalid credentials');

    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        return redirect()->route('user.login');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * 1. If new user then got to make subscription with socialite request
     * 2. If 1 false && active subsciption == false
     * 3. make subscription store user data.
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        if (is_null($user->email)) {
            return redirect()->route('home')->with('error', 'Something went wrong, try with different account.');
        }
        $existingUser = User::where('email', $user->email)->first();

        if (is_null($existingUser)) {
// Go to register page with socialite provided info
            $user_arr = explode(" ", $user->name);
            $first_name = $user_arr[0] ?? null;
            $last_name = end($user_arr);
            $createUser = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $user->email,
                'provider' => $provider,
                'provider_id' => $user->id,
                'password' => encrypt('123456')
            ]);
            Auth::loginUsingId($createUser->id);
            return redirect()->route('user.subscription');

        } else if (!is_null($existingUser) && $existingUser->active_subscription == 0) {
            Auth::loginUsingId($existingUser->id);
            return redirect()->route('user.subscription', $existingUser->id);
        }
        Auth::loginUsingId($existingUser->id);
        return redirect()->route('user.profile');

    }
}
