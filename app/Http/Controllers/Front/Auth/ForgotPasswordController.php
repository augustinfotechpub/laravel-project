<?php

namespace App\Http\Controllers\Front\Auth;

use App\Events\Front\ForgotPasswordEmail;
use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    public function showForgotPasswordForm()
    {
        return view('front.auth.forgot_password');
    }

    public function sendPasswordResetLink(Request $request)
    {
        $requestData = $request->all();

        $token = Str::random(15);
        $user = User::where('email', $requestData['email'])->first();
        if (isset($user)) {
            $reset_data = array('email' => $requestData['email'], 'token' => $token, 'created_at' => date('Y-m-d'));
            $isEmailExist = PasswordReset::where('email', '=', $requestData['email'])->exists();
            if ($isEmailExist) {
                PasswordReset::where('email', '=', $requestData['email'])->update($reset_data);
            } else {
                PasswordReset::create($reset_data);
            }
            $reset_data['name'] = $user['first_name'] . ' ' . (isset($user['last_name']) ? $user['last_name'] : '');
            $reset_data['reset_link'] = route('user.reset_password.create', $token);
            event(new ForgotPasswordEmail($reset_data));

            return redirect()->back()->with('success', 'Email sent successfully');

        } else {
            return redirect()->back()->with('error', 'Email address not exist');
        }

    }

    public function resetPasswordForm($token)
    {

        $passwordReset = PasswordReset::where('token', $token)->first();
        if (is_null($passwordReset)) {
            return redirect()->route('home')->with('error','Sorry, You have already use this link.');
        }
        $user = User::where('email', $passwordReset->email)->firstOrFail();
        return view('front.auth.reset_password', compact('token', 'user'));

    }

    public function passwordReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|exists:password_resets,token',
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = User::where('email', $request->email)->firstOrFail();
        PasswordReset::where('email', $user->email)->delete();
        $user->update(['password' => bcrypt($request->password)]);
        auth()->loginUsingId($user->id);
        return redirect()->route('home')->with('success', 'Password reset successfully.');


    }
}
