<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreEmailTemplateRequest;
use App\Http\Requests\Admin\UpdateEmailTemplateRequest;
use App\Interfaces\Admin\EmailTemplateInterface;
use Illuminate\Http\Request;

class EmailTemplateController extends Controller
{
    protected $emailTemplateRepository;

    public function __construct(EmailTemplateInterface $emailTemplateRepository)
    {
        $this->emailTemplateRepository = $emailTemplateRepository;
    }

    public function index()
    {
        $email_templates = $this->emailTemplateRepository->getAll();
        return view('admin.email_templates.index', compact('email_templates'));
    }

    public function create()
    {
        return view('admin.email_templates.create');

    }

    public function store(StoreEmailTemplateRequest $request)
    {

        $this->emailTemplateRepository->store($request);
        return redirect()->route('admin.email_templates.list')->with('success', 'Data add successfully.');
    }

    public function edit($id)
    {
        $email_template = $this->emailTemplateRepository->getById($id);
        return view('admin.email_templates.edit', compact('email_template'));

    }

    public function update(UpdateEmailTemplateRequest $request)
    {
        $this->emailTemplateRepository->update($request, $request->id);
        return redirect()->route('admin.email_templates.list')->with('success', 'Data updated successfully');

    }

    public function delete($id)
    {

        $this->emailTemplateRepository->delete($id);
        return redirect()->back()->with('success', 'Data deleted successfully.');


    }
}
