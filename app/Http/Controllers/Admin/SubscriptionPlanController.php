<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreSubscriptionPlanRequest;
use App\Interfaces\Admin\SubscriptionPlanInterface;
use Illuminate\Http\Request;

class SubscriptionPlanController extends Controller
{
    protected $subscriptionPlanRepository;

    public function __construct(SubscriptionPlanInterface $subscriptionPlanRepository)
    {
        $this->subscriptionPlanRepository = $subscriptionPlanRepository;
    }

    public function index()
    {
        $subscription_plans = $this->subscriptionPlanRepository->getAll();
        return view('admin.subscription_plans.index',compact('subscription_plans'));
    }

    public function create()
    {
        return view('admin.subscription_plans.create');
    }

    public function store(StoreSubscriptionPlanRequest $request)
    {

        $response = $this->subscriptionPlanRepository->store($request);
        if ($response['status'] == true) {
            return redirect()->route('admin.subscriptions_plan.list')->with('success', $response['message']);
        } else {
            return redirect()->route('admin.subscription_plan.create')->with("error", $response['message']);
        }
    }

    public function edit($id)
    {
        $subscription_plan = $this->subscriptionPlanRepository->getById($id);
        return view('admin.subscription_plans.edit',compact('subscription_plan'));
    }

    public function update(Request $request)
    {

        $this->subscriptionPlanRepository->update($request,$request->id);
        return redirect()->route('admin.subscriptions_plan.list')->with('success','Data updated successfully.');
    }

    public function show($id)
    {
        $subscription_plan = $this->subscriptionPlanRepository->getById($id);
        return view('admin.subscription_plans.show',compact('subscription_plan'));
    }

    public function delete($id)
    {
        $this->subscriptionPlanRepository->delete($id);
        return redirect()->route('admin.subscriptions_plan.list')->with('success','Data deleted successfully.');
    }
}
