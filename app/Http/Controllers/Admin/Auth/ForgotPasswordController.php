<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Events\Admin\ForgotPasswordEmail;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    public function __construct()
    {

    }

    public function showForgotPasswordForm()
    {
        return view('admin.auth.forgot-password');
    }

    public function sendPasswordResetLink(Request $request)
    {
        $requestData = $request->all();

        $token = $random = Str::random(15);
        $admin = Admin::where('email', $requestData['email'])->first();
        if (isset($admin)) {
            $reset_data = array('email' => $requestData['email'], 'token' => $token, 'created_at' => date('Y-m-d'));
            $isEmailExist = PasswordReset::where('email', '=', $requestData['email'])->exists();
            if ($isEmailExist) {
                PasswordReset::where('email', '=', $requestData['email'])->update($reset_data);
            } else {
                PasswordReset::create($reset_data);
            }
            $reset_data['name'] = $admin['first_name'] . ' ' . (isset($admin['last_name']) ? $admin['last_name'] : '');
            $reset_data['reset_link'] = route('admin.password_reset.create', $token);
            event(new ForgotPasswordEmail($reset_data));

            return redirect()->back()->with('success', 'Email send successfully');

        } else {
            return redirect()->back()->with('error', 'Email address not exist');
        }

    }

    public function resetPasswordForm($token)
    {

        $email = PasswordReset::where('token', $token)->firstOrFail()->email;
        $admin = Admin::where('email', $email)->firstOrFail();
        return view('admin.auth.reset-password', compact('token', 'admin'));

    }

    public function passwordReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|exists:password_resets,token',
            'email' => 'required|email|exists:admins,email',
            'password' => 'required|confirmed|min:6',
        ]);

        $admin = Admin::where('email', $request->email)->firstOrFail();
        PasswordReset::where('email', $admin->email)->delete();
        $admin->update(['password' => bcrypt($request->password)]);
        auth()->guard('admin')->loginUsingId($admin->id);
        $appName = config('app.name');
        return redirect()->route('admin.dashboard')->with('success', "Welcome to $appName.");


    }
}
