<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\DefinitionInterface;
use App\Interfaces\Admin\KeyDefinitionMappingInterface;
use App\Models\Sic;
use App\Models\SicKeyDefinition;
use Illuminate\Http\Request;

class KeyDefinitionMappingController extends Controller
{
    protected $keyDefinitionMappingRepository, $definitionRepository;

    public function __construct(KeyDefinitionMappingInterface $keyDefinitionMappingRepository, DefinitionInterface $definitionRepository)
    {
        $this->keyDefinitionMappingRepository = $keyDefinitionMappingRepository;
        $this->definitionRepository = $definitionRepository;
    }

    public function index(Request $request)
    {

        if ($request->ajax()) {
            return $this->keyDefinitionMappingRepository->getAllAjax();
        }
        return view('admin.key_alert_mapping.index');
    }

    public function create()
    {
        $sics = $this->keyDefinitionMappingRepository->getAll();
        $definitions = $this->definitionRepository->getAll();
        return view('admin.key_alert_mapping.create', compact('sics', 'definitions'));
    }

    public function store(Request $request)
    {

        if (is_null($request->definitions)) {
            return redirect()->back()->with('error', 'Please select at list one key definition.');
        }
        $this->keyDefinitionMappingRepository->store($request);
        return redirect()->route('admin.key_alert_mapping.list')->with('success', 'Data inserted successfully.');
    }

    public function show($id)
    {
        $sic = $this->keyDefinitionMappingRepository->getById($id);
        return view('admin.key_alert_mapping.show', compact('sic'));
    }

    public function edit($id)
    {
        $sics = $this->keyDefinitionMappingRepository->getAll();
        $definitions = $this->definitionRepository->getAll();
        $sic = $this->keyDefinitionMappingRepository->getById($id);
        return view('admin.key_alert_mapping.edit', compact('sics', 'definitions', 'sic'));
    }

    public function update(Request $request, $id)
    {
        if (is_null($request->key_definition)) {
            return redirect()->back()->with('error', 'Please select at list one key definition.');
        }
        $this->keyDefinitionMappingRepository->update($request, $id);
        return redirect()->route('admin.key_alert_mapping.list')->with('success', 'Data updated successfully.');

    }

    public function delete($id)
    {
        $this->keyDefinitionMappingRepository->delete($id);
        return redirect()->back()->with('success', 'Data deleted successfully.');

    }
}
