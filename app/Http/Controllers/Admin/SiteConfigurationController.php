<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\SiteConfigurationInterface;
use App\Models\SiteConfiguration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SiteConfigurationController extends Controller
{
    protected $siteConfigurationRepository;

    public function __construct(SiteConfigurationInterface $siteConfigurationRepository)
    {
        $this->siteConfigurationRepository = $siteConfigurationRepository;
    }

    public function index()
    {
        $configuration_data = $this->siteConfigurationRepository->getAll();
        return view('admin.configuration.index', compact('configuration_data'));
    }

    public function update(Request $request)
    {   
        $fileSaveLink = '';
        if (isset( $request->file("configuration")["WELCOME_VIDEO_FILE"] )) {
            
            $file = $request->file("configuration")["WELCOME_VIDEO_FILE"];

            $validator = Validator::make($request->all(), [
                'configuration.WELCOME_VIDEO_FILE' => 'max:350000',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }

            $filename = str_replace( ' ', '_', time().'_'.$file->getClientOriginalName() );
            // File upload location
            $location = 'storage/uploads/';
            // Upload file
            $file->move($location,$filename);

            $fileSaveLink = $location.$filename;
        }

        $this->siteConfigurationRepository->update($request);

        if( !empty($fileSaveLink) ){
            $customSave = SiteConfiguration::find(11);
            $customSave->value = $fileSaveLink;
            $customSave->save();
        }

        return redirect()->route('admin.configurations')->with('success', "Site Configuration Data Updated Sucessfully");

    }

    public function remove_video(){
        $customSave = SiteConfiguration::find(11);
        $customSave->value = '';
        $customSave->save();
    }
}
