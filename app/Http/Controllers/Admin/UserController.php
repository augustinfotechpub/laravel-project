<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\UserInterface;
use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRepository;
use \Cache;
use Artisan;
use Illuminate\Support\Facades\DB;
use App\Mail\MarkdownMail;
use Mail;
use App\Models\Newsletter;
use App\Models\CancelSubscription;
use Yajra\DataTables\DataTables;



class UserController extends Controller
{
    protected $userRepository, $MAIL_SUBJECT = 'Site Live';

    public function __construct(UserInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->userRepository->getAllAjax();
        }
        return view('admin.users.index');
    }

    public function syncIndex(){
        return view('admin.users.sync');
    }

    private function getIdIfexists($symbol =''){

        if( empty($symbol) ){
            return false;
        }

        $data = DB::table('symbols')->where('symbol', $symbol)->get()->toArray();
        if( !isset($data[0]) || empty($data) ){
            return false;
        }

        return $data;
    }

    private function getIdIfexistsAlert($alertId = ''){
        if( empty($alertId) ){
            return false;
        }

        $data = DB::table('definitions')->where('id', $alertId)->get()->toArray();
        if( !isset($data[0]) || empty($data) ){
            return false;
        }

        return $data;
    }

    public function syncSubmit(Request $request)
    {
        $allInput = $request->input();
        $symbolData= [];

        switch ($allInput['syncType']) {
            case 'keyword':
                # code...
                $alertData = $this->getIdIfexistsAlert($allInput['alert']);
                
                if( !$alertData ){
                    return redirect()->back()->with('error','No Alert found for this Id');
                }

                Artisan::call('sync:cik-keywords --alert-id='.$alertData[0]->id);
                return redirect()->back()->with('success','Company financial statements are synced');
                break;
            
            case 'company':

                $symbolData = $this->getIdIfexists($allInput['symbol']);
                
                if( !$symbolData ){
                    return redirect()->back()->with('error','No company found for this symbol');
                }

                Artisan::call('sync:financial-statements --company-id='.$symbolData[0]->company_id);
                return redirect()->back()->with('success','Company financial statements are synced');
                break;
            case 'keyparties':

                $symbolData = $this->getIdIfexists($allInput['symbol']);
                
                if( !$symbolData ){
                    return redirect()->back()->with('error','No company found for this symbol');
                }

                Artisan::call('sync:ownerships --company-id='.$symbolData[0]->company_id);
                return redirect()->back()->with('success','Company Key Parties are synced');
                break;

            default:
                # code...
                break;
        }

    }

    public function show($id)
    {
        $user = $this->userRepository->getById($id);
        return view('admin.users.show', compact('user'));
    }


    public function destroy($id)
    {
        $this->userRepository->delete($id);
        return redirect()->back()->with('success','Data deleted successfully.');
    }

    public function showchat()
    {

        $conversationId = 1;
        $data['conversationId'] = !empty($conversationId) ? $conversationId : 0;

        $data['users'] = User::toBase()
        ->limit(50)
        ->orderBy('name')
        ->select(['name', 'id'])
        ->pluck('name', 'id')
        ->except(getLoggedInUserIdCustom());
        // ->where('id', $id);

        Cache::put('show_user', getLoggedInUserIdCustom());

        $customUserData = User::find(getLoggedInUserIdCustom());

        /** @var UserRepository $userRepository */
        $userRepository = app(UserRepository::class);
        /** @var BlockUserRepository $blockUserRepository */
        $myContactIds = $userRepository->myContactIdsForUser(getLoggedInUserIdCustom());

        $data['enableGroupSetting'] = false;
        $data['membersCanAddGroup'] = false;
        $data['myContactIds'] = $myContactIds;
        $data['blockUserIds'] = [];
        $data['blockedByMeUserIds'] = [];
        $data['CustomAuth'] = $customUserData;

        return view('admin.users.showchat', $data);
 
    }

    public function newsLetter(){
        return view('admin.users.newsletter');   
    }

    public function newsLetterEmail(Request $request)
    {
        $email = $request->input();
        $data = [];
        $data['USER_NAME'] = 'User';
        Mail::to($email['symbol'])->send(new MarkdownMail('emails.email_test', 'Email Test | Finnfrastructure', $data));
        return redirect()->back()->with('success','Test mail has been sent.');
        
    }

    public function newsLetterEmailAll()
    {
        $where = ['status'=>'0'];
        // $userData =  Newsletter::where($where)->get()->toArray();
        $userData =  DB::table('newsletters')->where($where)->get();
        $userData = json_decode($userData, true);
        $data = [];
        if( !empty($userData) ){
            foreach ($userData as $key => $value) {
                $data['USER_NAME'] = $value['name'];
                Mail::to($value['email'])->send(new MarkdownMail('emails.email_test', $this->MAIL_SUBJECT.' | Fnnfrastructure', $data));
                // Newsletter::where('email', $value['email'])->update(['status' => 1]); 
                DB::table('newsletters')->where('email', $value['email'])->update(['status' => 1]);
            }

            return redirect()->back()->with('success','Mail has been sent to all users.');
        }

        return redirect()->back()->with('success','No users pending to send mail');

    }

    public function newsletterEmailUpdate(Request $request)
    {
        $email = $request->input();
        $email = json_decode($email['email']);
        $checkJson=$this->isJson($email);
        if($checkJson==true)
        {
            $email= json_decode( json_encode($email), true);
            DB::table('newsletters')->insertOrIgnore($email);
            return redirect()->back()->withSuccess('Great! Successfully store users in newsletter list');
        }
        else{
            return redirect()->back()->with('error','Please Enter Json Format Data');
        }
        
    }

    public function isJson($email) {
        return json_last_error() === JSON_ERROR_NONE;
     }

    public function cancelSubscriptionFeedback(Request $request){

        if ($request->ajax()) {
            $cancel_subscription = CancelSubscription::all();
            return DataTables::of($cancel_subscription)
            ->addColumn('answer', function ($data) {
                $html = '';
                $get_question = $this->cancelSurveyquestion();
                $get_data = $data->answer;
                if(!empty($get_data))
                {
                    $get_answer = unserialize($get_data);
                    return array('question' => $get_question, 'answer'=> $get_answer);
                    if(is_array($get_answer) && !empty($get_answer))
                    {
                        foreach($get_answer as $index => $ans)
                        {
                            $html .="<b>".$get_question[$index+1]."</b>\n";
                            $html .="<p>$ans</p>";
                            $html .="<br>";
                        }
                    }
                    return $html        ;
                }

                return view('backend.includes.action_column', compact('module_name', 'data'));
            })
            ->make(true);
        }
        return view('admin.users.cancel_subscription');   
    }
    public function cancelSurveyquestion(){
        return array(
        '1'=>'What made you question if Finnfrastructure was a good fit for you?',
        '2'=>'When did you decide to cancel, and was there anything that prompted the decision?',
        '3'=>'What service are you using now?',
        '4'=>'Is there anything we could have done that would have prevented you from canceling?',
        '5'=>'Would you be open to signing up again if your main issue was no longer an issue?',
        '6'=>'Other',
        );
    }


}
