<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\AuditCommitteeMembersInterface;
use App\Interfaces\Admin\CompanyInterface;
use App\Models\Company;
use App\Services\FinancialModelingPrep;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public $auditCommitteeMembersRepository, $companyRepository, $fmp;

    public function __construct(CompanyInterface $companyRepository, AuditCommitteeMembersInterface $auditCommitteeMembersRepository, FinancialModelingPrep $fmp)
    {
        $this->fmp = $fmp;
        $this->companyRepository = $companyRepository;
        $this->auditCommitteeMembersRepository = $auditCommitteeMembersRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->companyRepository->getAllAjax($request);
        }
        return view('admin.companies.index');
    }

    public function destroy($company_id)
    {
        $this->companyRepository->delete($company_id);
        return redirect()->back()->with('success', 'Company Removed successfully');
    }

    public function show($id)
    {
        $company = $this->companyRepository->getById($id);
        return view('admin.companies.show', compact('company'));
    }

    public function create()
    {
        return view('admin.companies.create');
    }

    public function edit($id)
    {
        $company = $this->companyRepository->getById($id);
        return view('admin.companies.edit', compact('company'));
    }

    public function update($id, Request $request)
    {
        $this->companyRepository->update($id, $request);
        return redirect()->route('admin.companies.index')->with('success', 'Data updated successfully.');
    }
}
