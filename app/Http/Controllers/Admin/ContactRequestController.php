<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\ContactRequestInterface;
use Illuminate\Http\Request;

class ContactRequestController extends Controller
{
    protected $contactRequestRepository;

    public function __construct(ContactRequestInterface $contactRequestRepository)
    {
        $this->contactRequestRepository = $contactRequestRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->contactRequestRepository->getAllAjax();
        }
        return view('admin.contact_requests.index');
    }


    public function show($id)
    {
        $contact_request = $this->contactRequestRepository->getById($id);
        return view('admin.contact_requests.show',compact('contact_request'));
    }


    public function destroy($id)
    {
        $this->contactRequestRepository->delete($id);
        return redirect()->back()->with('success','Data deleted successfully.');
    }
}
