<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\CustomerSubscriptionInterface;
use App\Models\CustomerSubscription;
use Illuminate\Http\Request;

class CustomerSubscriptionController extends Controller
{
    protected $customerSubscriptionRepository;

    public function __construct(CustomerSubscriptionInterface $customerSubscriptionRepository)
    {
        $this->customerSubscriptionRepository = $customerSubscriptionRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->customerSubscriptionRepository->getAllAjax();
        }
        return view('admin.customer_subscriptions.index');

    }

    public function show($id)
    {
        $customer_subscription = $this->customerSubscriptionRepository->getById($id);
        return view('admin.customer_subscriptions.show',compact('customer_subscription'));

    }

    public function delete($id)
    {
        $this->customerSubscriptionRepository->delete($id);
        return redirect()->back()->with('success','Data deleted successfully.');
    }
}
