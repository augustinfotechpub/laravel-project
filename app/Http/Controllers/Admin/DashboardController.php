<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\Admin\CustomerSubscriptionInterface;
use App\Interfaces\Admin\UserInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $userRepository, $customerSubscriptionRepository;

    public function __construct(UserInterface $userRepository, CustomerSubscriptionInterface $customerSubscriptionRepository)
    {
        $this->userRepository = $userRepository;
        $this->customerSubscriptionRepository = $customerSubscriptionRepository;

    }

    public function index()
    {

        $users = $this->userRepository->getLatestUsers(10);
        $active_users = $this->userRepository->getActivateUsers()->count();
        $total_subscriptions_amount = $this->customerSubscriptionRepository->getTotalAmount();
        $current_month_total = $this->customerSubscriptionRepository->getThisMonthTotalAmount();
        $current_month_expired = $this->customerSubscriptionRepository->getThisMonthExpired()->count();

        return view('admin.dashboard.index', compact('users', 'active_users','total_subscriptions_amount','current_month_total','current_month_expired'));
    }
}
