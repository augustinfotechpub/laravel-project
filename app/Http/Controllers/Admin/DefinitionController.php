<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DefinitionRequest;
use App\Interfaces\Admin\DefinitionInterface;
use App\Interfaces\Admin\DefinitionItemSicsInterface;
use App\Interfaces\Admin\DefinitionItemsInterface;
use App\Interfaces\Admin\DefinitionItemSymbolInterface;
use App\Interfaces\Admin\DefinitionItemTagsInterface;
use App\Models\Company;
use App\Models\Definition;
use App\Models\DefinitionItem;
use App\Models\DefinitionItemSic;
use App\Models\DefinitionItemSymbol;
use App\Models\DefinitionItemTag;
use App\Models\DefinitionItemTagMap;
use App\Models\Sic;
use App\Models\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Imports\XlsxImport;
use Yajra\DataTables\DataTables;

class DefinitionController extends Controller
{
    protected $definitionRepository, $definitionItemsRepository, $definitionItemSicsRepository, $definitionItemTagsRepository, $definitionItemSymbolRepository;

    public function __construct(DefinitionInterface $definitionRepository, DefinitionItemsInterface $definitionItemsRepository, DefinitionItemSicsInterface $definitionItemSicsRepository, DefinitionItemSymbolInterface $definitionItemSymbolRepository, DefinitionItemTagsInterface $definitionItemTagsRepository)
    {
        $this->definitionRepository = $definitionRepository;
        $this->definitionItemsRepository = $definitionItemsRepository;
        $this->definitionItemSicsRepository = $definitionItemSicsRepository;
        $this->definitionItemSymbolRepository = $definitionItemSymbolRepository;
        $this->definitionItemTagsRepository = $definitionItemTagsRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->definitionRepository->getAllAjax();
        }
        return view('admin.alerts.index');
    }

    public function survey(Request $request)
    {
        if ($request->ajax()) {
            $definitions = Survey::query()->orderBy('id','desc')->get();
            
        return DataTables::of($definitions)
        ->editColumn('id', function ($definitions) {
            return ucfirst($definitions->id);
        })
        ->addColumn('action', function ($definition) {
            return view('admin.alerts.partials.action-survey', compact('definition'));
        })
        ->rawColumns(['action'])
        ->make(true);

        }
        return view('admin.users.show-survey');
    }

    public function importAlerts(Request $request){
        if ($request->ajax()) {
            return false;
        }
        
        $validator = Validator::make($request->all(), [
            'deleteAll' => 'nullable',
            'file' => 'required|mimes:xlsx,xls'
        ]);


        if( $request->input('deleteAll') == 'on' ){
            Definition::truncate();
            DefinitionItem::truncate();
            DefinitionItemSic::truncate();
            DefinitionItemSymbol::truncate();
            DefinitionItemTag::truncate();
            DefinitionItemTagMap::truncate();
        }

        if ($validator->fails()) {
            return redirect()->route('admin.alerts.list')->withErrors($validator);
        }

        $path1 = $request->file('file')->store('temp');
        $path = storage_path('app').'/'.$path1;
        // $path = $request->file('file')->getRealPath();

        $data = \Excel::import(new XlsxImport, $path);
        if(!$data){
            return redirect()->back()->with('error', 'File can not be imported, wrong formatting');            
        }

        return redirect()->back()->with('success', 'File has been imported into Alerts');

    }

    public function create()
    {
        $types = Definition::TYPE;
        $alertIdentifiers = config('alertDefinitionIdentifiers');
        return view('admin.alerts.create', compact('types', 'alertIdentifiers'));
    }

    public function store(DefinitionRequest $request)
    {
        $definition = $this->definitionRepository->store($request);
        $definition_item_id = $this->definitionItemsRepository->store($definition->id, $request);
        $this->definitionItemSicsRepository->store($definition->id, $definition_item_id, $request);
        $this->definitionItemSymbolRepository->store($definition->id, $definition_item_id, $request);
        $this->definitionItemTagsRepository->store($definition->id, $definition_item_id, $request);
        return redirect()->route('admin.alerts.list')->with('success', 'Data inserted successfully.');
    }

    public function edit($id)
    {
        $types = Definition::TYPE;
        $alertIdentifiers = config('alertDefinitionIdentifiers');
        $definition = $this->definitionRepository->getById($id);
        return view('admin.alerts.edit', compact('types', 'alertIdentifiers', 'definition'));
    }

    public function update(DefinitionRequest $request, $id)
    {

        $this->definitionRepository->update($request, $id);
        $definition_item_id = $this->definitionItemsRepository->update($request, $id);
        $this->definitionItemTagsRepository->update($request, $id, $definition_item_id);
        $this->definitionItemSicsRepository->update($request, $id, $definition_item_id);
        $this->definitionItemSymbolRepository->update($request, $id, $definition_item_id);

        return redirect()->route('admin.alerts.list')->with('success', 'Data updated successfully.');
    }

    public function show($id)
    {
        $definition = $this->definitionRepository->getById($id);
        return view('admin.alerts.show', compact('definition'));
    }

    public function surveyShow($id)
    {   
        $definition = Survey::where('id', $id)->get();
        return view('admin.users.show-survey-details', compact('definition'));
    }

    public function delete($id)
    {
        $this->definitionRepository->delete($id);
        $this->definitionItemsRepository->delete($id);
        $this->definitionItemTagsRepository->delete($id);
        $this->definitionItemSicsRepository->delete($id);
        $this->definitionItemSymbolRepository->delete($id);
        return redirect()->back()->with('success', 'Data deleted successfully.');
    }

    public function deleteTag($id)
    {
        DefinitionItemTag::destroy($id);
        return redirect()->back();
    }

    public function deleteSic($id)
    {
        DefinitionItemSic::destroy($id);
        return redirect()->back();
    }

    public function deleteSymbol($id)
    {
        DefinitionItemSymbol::destroy($id);
        return redirect()->back();
    }
}
