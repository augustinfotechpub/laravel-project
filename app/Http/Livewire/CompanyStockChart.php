<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use Carbon\Carbon;
use Livewire\Component;

class CompanyStockChart extends Component
{
    protected $rapidApiService;
    public $symbol, $statistics,
        $start_date, $end_date,
        $initComponent,
        $is_active = '5min',
        $attempts = 0, $symbols,
        $chartData, $frequency;

    public function mount($symbol)
    {
        $this->symbols[] = $this->symbol;
        $this->initComponent = false;
        $this->symbol = $symbol;
        $this->start_date = Carbon::now()->subDays(30)->format('U');
        $this->end_date = Carbon::now()->format('U');
        $this->getComparisonSearchResults();
    }

    public function render()
    {
        return view('livewire.company-stock-chart');
    }

    public function addSymbol()
    {
        if (!is_null($this->newSymbol)) {
            $this->symbols[] = $this->newSymbol;
            // self::getStatistics($this->newSymbol);
            $this->newSymbol = null;
        }
    }

    public function removeSymbol($comparison_symbol)
    {

    }

    public function getStatistics()
    {
        $fmp = new FinancialModelingPrep();
        if ($this->is_active == ('daily')) {
            $rowData = $fmp->getDailyHistoricalPriceFull($this->symbol)['historical'];
        } else {
            $rowData = $fmp->stockHistoricalPrice($this->symbol, $this->is_active);
        }
        $data = [];
        foreach ($rowData as $row) {
            $data[] = [
                'time' => Carbon::parse($row['date'])->toArray()['timestamp'],
                'value' => $row['close']];
        }
        $this->chartData = array_reverse($data);
        $this->dispatchBrowserEvent('render-fundamental-chart', ['symbol' => $this->symbols, 'dataset' => $this->chartData]);
    }

    public function changeFrequency($frequency)
    {
        $this->frequency = $frequency;
    }

    public function changePeriod($is_active)
    {
        $this->is_active = $is_active;
        self::getStatistics();
    }

    public function initComponent()
    {
        self::getStatistics();
        $this->initComponent = true;
    }
}
