<?php

namespace App\Http\Livewire;

use App\Services\SecEdgarService;
use Goutte\Client;
use Illuminate\Support\Collection;
use Livewire\Component;

class FillingDetails extends Component
{
    protected $listeners = ['getDetails' => 'getFillingDetails'];
    public $cik, $symbol, $initComponent = false, $transactions = [], $name, $form;

    public function render()
    {
        return view('livewire.filling-details');
    }

    public function getDetail($start = 0)
    {
        $sec = new SecEdgarService();
        $res = $sec->getFillingsList($this->cik, $start);
        $collection = [];
        if (is_null($res)) {
            request()->session()->flash('error', "Details not found for : $this->cik");
            // return redirect()->route("app.key-parties", ['symbol' => $this->symbol]);
        } else {
            $collection = collect($res['array'])->groupBy(['date', 'issuer'])->map(function ($q) {
                $q->details = $q[array_keys($q->toArray())[0]];
                $a = $q->details->where('acquisition_or_disposition', 'A')->sum('number_of_securities_owned');
                $d = $q->details->where('acquisition_or_disposition', 'D')->sum('number_of_securities_owned');
                $q->issuer = $q->details[0]['issuer'];
                $q->date = formatDate($q->details[0]['date']);
                $q->form = $q->details[0]['form'];
                $q->noso = abs($d - $a);
                $q->form_url = $q->details[0]['form_url'];
                return $q;
            });
        }
        $this->transactions = [
            'have_next' => $res['have_next'],
            'start' => $res['start'],
            'header_titles' => $res['header_titles'],
             'data' => $collection
        ];
    }

    public function initComponent()
    {
        self::getDetail();
        $this->initComponent = true;
    }

    public function getNext($start)
    {
        self::getDetail($start);
    }

    public function getFillingDetails($cik, $name, $symbol)
    {
        $this->initComponent = false;
        $this->cik = $cik;
        $this->symbol = $symbol;
        $this->name = $name;
        $this->initComponent();
    }

    public function getForm($url)
    {
        $client = new Client();
        $html = $client->request('GET', $url)->filter('body')->html();
        $this->form = $html;
    }
}
