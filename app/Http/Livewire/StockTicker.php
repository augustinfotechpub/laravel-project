<?php

namespace App\Http\Livewire;

use Livewire\Component;

class StockTicker extends Component
{
    public function render()
    {
        return view('livewire.stock-ticker');
    }
}
