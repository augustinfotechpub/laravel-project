<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CompanyRegularMarketPrice extends Component
{
    public function render()
    {
        return view('livewire.company-regular-market-price');
    }
}
