<?php

namespace App\Http\Livewire\Admin;

use App\Models\Definition;
use Illuminate\Support\Str;
use Livewire\Component;

class AlertDefinitions extends Component
{
    protected $listeners = ['removeAlertDefinitionItem' => 'removeItm'];
    public $items = [], $i = 1;

    public function render()
    {
        $this->addItem();
        return view('livewire.admin.alert-definitions');
    }

    public function addItem($i = 1)
    {
        $key = Str::random();
        $this->items[$i] = $key;
        $this->i++;
        $this->dispatchBrowserEvent('text-editor');

    }

    public function removeItem($key)
    {
        unset($this->items[$key]);
    }

    public function addTags()
    {

    }

    public function removeTags()
    {

    }

    public function addSic()
    {

    }

    public function removeSic()
    {

    }

    public function addSymbol()
    {

    }

    public function removeSymbol()
    {

    }
}
