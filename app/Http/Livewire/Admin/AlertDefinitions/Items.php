<?php

namespace App\Http\Livewire\Admin\AlertDefinitions;

use App\Models\Definition;
use Illuminate\Support\Str;
use Livewire\Component;

class Items extends Component
{
    public $types, $key, $inputs, $sicI = 1, $cikI = 1, $symbolI = 1, $tagI = 1;



    public function render()
    {
        $this->types = Definition::TYPE;
        $this->addInput('sic');
        $this->addInput('symbol');
        $this->addInput('tag');
        return view('livewire.admin.alert-definitions.items');
    }

    public function removedd()
    {

        $this->emit('removeAlertDefinitionItem', $this->key);
    }

    public function addInput($type, $i = 1)
    {

        $key = Str::random();
        switch ($type) {
            case 'sic':
                $this->inputs['sic'][$i] = $key;
                $this->sicI++;
                break;
            case 'cik':
                $this->inputs[$type][$i] = $key;
                $this->cikI++;
                break;
            case 'symbol':
                $this->inputs[$type][$i] = $key;
                $this->symbolI++;
                break;
            case 'tag':
                $this->inputs[$type][$i] = $key;
                $this->tagI++;
                break;
        }

    }

    public function removeInput($type, $index)
    {
        unset($this->inputs[$type][$index]);
    }
}
