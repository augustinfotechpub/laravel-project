<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Goutte\Client;
use Livewire\Component;

class SecFillings extends Component
{
    public $symbol, $cik, $fillings = [], $date, $type, $owner = "include", $loadContent = false, $modalData, $showECT = true, $earningCallTranscript, $loadEarningCallTranscript = true;

    public function mount()
    {
        if (session('sec-filing-cik')) {
            $this->cik = session('sec-filing-cik');
            session()->forget('sec-filing-cik');
            $this->showECT = false;
        } else {
            $this->symbol = session('profile')['symbol'];
            $fmp = new FinancialModelingPrep();
            $this->cik = $fmp->getCikBySymbol($this->symbol);
        }
    }

    public function render()
    {
        return view('livewire.sec-fillings');
    }

    public function getFillings()
    {
        $secEdgar = new SecEdgarService();
        if (!is_null($this->date)) {
            $this->date = formatDate($this->date, 'Ymd');
        }
        $this->fillings = $secEdgar->getFillings($this->cik, 100, $this->type, $this->date, $this->owner);
        if (empty($this->fillings)) {
            self::getFillings();
        }
        $this->loadContent = true;
    }

    public function viewDocument($url)
    {
        try {
            $client = new Client();
            $url = 'https://www.sec.gov' . $client->request('GET', $url)->filter(".tableFile > tr > td > a")->attr('href');
            $html = file_get_contents($url);
            $html = str_replace('<a ', '<a target="_blank" ', $html);
            $this->modalData = $html;
        } catch (\Exception $e) {
            $this->modalData = null;
        }
    }

    public function getDetails($final_link)
    {
        $symbol = $this->symbol;
        return redirect()->route('sec-filling-details', compact('final_link', 'symbol'));
    }

    public function newPage($url)
    {
        $secEdgar = new SecEdgarService();
        $this->fillings = $secEdgar->getFillingsByUrl($url);
    }

    public function getEarningCallTranscript()
    {
        try {
            $fmp = new FinancialModelingPrep();
            $year_limit = 5;
            $year = Carbon::now()->format('Y');
            $data = [];
            for ($y = 1; $y <= $year_limit; $y++) {
                $year = (int)$year;
                $quarter_no = 4;
                for ($q = $quarter_no; $q >= 1; $q--) {
                    $res = $fmp->getEarningCallTranscript($this->symbol, $q, $year);
                    if (!empty($res)) {
                        $data[] = $fmp->getEarningCallTranscript($this->symbol, $q, $year)[0];
                    }
                }
                $year--;
            }
            $this->earningCallTranscript = $data;
            $this->loadEarningCallTranscript = false;
        } catch (\Exception $e) {
            $this->earningCallTranscript = [];
            $this->loadEarningCallTranscript = false;
        }
    }
}
