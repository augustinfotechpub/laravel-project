<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Index extends Component
{
    protected $listeners = ['changeContentComponent' => 'changeContentComponent'];

    public $content_component, $symbol;

    public function mount()
    {
        $this->symbol = session('profile')['symbol'];
        $this->fill(['content_component' => 'app-main']);
    }

    public function render()
    {
        return view('livewire.index');
    }

    public function changeContentComponent($data)
    {
        $this->content_component = $data['content_component'];
    }
}
