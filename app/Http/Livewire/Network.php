<?php

namespace App\Http\Livewire;

use Goutte\Client;
use Livewire\Component;

class Network extends Component
{
    public $heatMap;

    public function render()
    {
        self::getSP500HeatMap();
        return view('livewire.network');
    }

    public function getSP500HeatMap()
    {
        $client = new Client();
        $response = $client->request('GET', 'https://finviz.com/map.ashx?t=sec_all');
//        dd($response->html());
         $this->heatMap = $response->html();
    }
}
