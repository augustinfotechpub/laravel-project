<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\IncomeStatement;
use App\Services\FinancialModelingPrep;
use App\Services\RapidAPI;
use App\Services\SecEdgarService;
use Carbon\Carbon;
use Livewire\Component;

class SearchBar extends Component
{
    public $query, $bestMatches, $highlightIndex, $content_component, $profile;

    public function mount()
    {
        $this->resetEngine();
    }

    public function render()
    {
        return view('livewire.search-bar');
    }

    public function resetEngine()
    {
        $this->query = '';
        $this->bestMatches = [];
        $this->highlightIndex = 0;
    }

    public function incrementHighlight()
    {
        if ($this->highlightIndex === count($this->bestMatches) - 1) {
            $this->highlightIndex = 0;
            return;
        }
        $this->highlightIndex++;
    }

    public function decrementHighlight()
    {
        if ($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->bestMatches) - 1;
            return;
        }
        $this->highlightIndex--;
    }

    public function selectCompany($index = null)
    {
        if (!is_null($index)) {
            $company = $this->bestMatches[$index] ?? 0;
        } else {
            $company = $this->bestMatches[$this->highlightIndex] ?? 0;
        }
        if (is_null($company['symbol'])) {
            return;
        }
        $fmp = new FinancialModelingPrep();
        $profile = $fmp->getProfile($company['symbol']);
        $this->profile = $profile[0];
        $this->profile['upcomingEarningsDate'] = $this->getUpcomingEarningsDate($company['symbol']);
        $quote = (array) $fmp->quote($company['symbol'])[0];
        $this->profile['sharesOutstanding'] = $quote['sharesOutstanding'];
        $company = Company::query()->whereIn('cik', [ltrim($this->profile['cik'], 0), $this->profile['cik']])->first();
        if (is_null($company)) {
            $this->emit('alert', 'error', 'Company records are unavailable in our database.');
            return;
        }
        $this->profile['sic'] = $company['sic'];
        session(['profile' => $this->profile]);
        $this->redirect(route('app_main'));
    }

    protected function getUpcomingEarningsDate($symbol)
    {
        $date = now()->toDateString();
        $dates = IncomeStatement::query()->where('symbol', $symbol)
            ->whereIn('period', ['Q1', 'Q2', 'Q3', 'Q4'])
            ->orderByDesc('date')->pluck('date')->take(4)->toArray();
        foreach ($dates as $index => $date) {
            $dates[$index] = Carbon::parse($date)->addYear()->toDateString();
        }

        usort($dates, function ($a, $b) {
            return strtotime($a) - strtotime($b);
        });
        foreach ($dates as $count => $dateSingle) {
            if (strtotime($date) < strtotime($dateSingle)) {
                $nextDate = date('Y/m/d', strtotime($dateSingle));
                break;
            }
        }
        return $nextDate ?? null;
    }


    public function updatedQuery()
    {
        $query = $this->query;
        $bestMatches = Company::query()->where('name', 'LIKE', "%{$query}%")
            ->orWhereHas('symbols', function ($q) use ($query) {
                $q->where('symbol', 'LIKE', "%{$query}%");
            })
            ->orWhere('cik', 'LIKE', "%{$query}%")->orderByRaw("CASE
                WHEN name LIKE '{$query}' THEN 1
                WHEN name LIKE '{$query}%' THEN 2
                WHEN name LIKE '%{$query}' THEN 4
                ELSE 3
            END")
            ->take(10)->get()->toArray();
        $results = [];
        foreach ($bestMatches as $bestMatch) {
            foreach ($bestMatch['symbols'] as $symbol) {
                $bestMatch['symbol'] = $symbol;
                $results[] = [
                    'name' => $bestMatch['name'],
                    'symbol' => $symbol['symbol'],
                    'cik' => $bestMatch['cik']
                ];
            }
        }
        $this->bestMatches = $results;
        /*$secEdgarService = new SecEdgarService();
        $this->bestMatches = $secEdgarService->searchProfile($this->query) ?? [];*/
    }

    public function getSicFillings($cik)
    {
        session(['sec-filing-cik' => $cik]);
        $this->emit('changeContentComponent', ['content_component' => 'sec-fillings']);
    }
}
