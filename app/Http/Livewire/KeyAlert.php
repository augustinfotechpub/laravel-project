<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\Definition;
use Illuminate\Support\Str;
use Livewire\Component;

class KeyAlert extends Component
{
    public $alert, $alertUniqueKey, $alertExists = false;

    public function mount()
    {
        $this->alertUniqueKey = filterUniqueKey($this->alertUniqueKey);
    }

    public function render()
    {
        return view('livewire.key-alert');
    }

    public function getAlert()
    {
        $this->alert = Definition::query()->where(['type' => 'alert'])->first();
        if (!is_null($this->alert))
            $this->alertExists = true;
    }
}
