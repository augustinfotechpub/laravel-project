<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\Definition;
use App\Models\DefinitionItemSic;
use App\Models\DefinitionItemSymbol;
use App\Models\Sic;
use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class StockSummary extends Component
{
    public $symbol, $initStockSummary = false, $profile;
    public $industryInformations, $keyConcepts, $showKeyConcept = false, $showIndustryInfo = false;

    public function mount()
    {
        $this->profile = session('profile');
        $this->symbol = $this->profile['symbol'];
    }

    public function render()
    {
        return view('livewire.stock-summary');
    }

    public function initStockSummary()
    {
        $fmp = new FinancialModelingPrep();
        $profile = session('profile');
        $quote = (array) $fmp->quote($this->symbol)[0];
        $this->profile = array_merge($quote, $profile);
//        $this->dispatchBrowserEvent('initTooltip');
        $this->getKeyConcepts();
        $this->getIndustryInformation();
        $this->initStockSummary = true;
    }

    public function getKeyConcepts()
    {
        $this->keyConcepts = $this->getDefinitionsKeyConcepts('key_concept');
        $this->showKeyConcept = true;
    }

    public function getIndustryInformation()
    {
        $this->industryInformations = $this->getDefinitions('industry_information');
        $this->showIndustryInfo = true;
    }


    public function getDefinitions($type)
    {
        $arrayReturn = [];
        $sicNo = $this->profile['sic'];
        $symbol = $this->profile['symbol'];
        $cik = ltrim( $this->profile['cik'], '0' );
        // $sic = DefinitionItemSic::where('sic', $sic)->get();
        $finalSic = [];
        $finalArrayReturn = [];
        $finalArrayMerge = [];
        $first = [];
        $sic = [];

        $first = DB::table('definitions')
        ->join("definition_items", function($join){
            $join->on("definitions.id", "=", "definition_items.definition_id");
        })->join("definition_item_sics", function($join){
            $join->on("definition_item_sics.definition_item_id", "=", "definition_items.id");
        })->select("definition_item_sics.sic", "definitions.id", "definitions.title", "definition_items.id as item_id", "definition_items.type", "definition_items.content", "definition_items.sub_title")
        ->where('sic', $sicNo)->where('definition_items.type', 'industry_information')->get()->toArray();

        $second = DB::table('definitions')
        ->join("definition_items", function($join){
            $join->on("definition_items.definition_id", "=", "definitions.id");
        })->join("definition_item_tag_maps", function($join){
            $join->on("definition_item_tag_maps.definition_item_id", "=", "definition_items.id");
        })->select("definitions.id", "definition_items.sub_title", "definitions.title", "definition_items.content", "definition_items.id as item_id")
        ->where('cik', $cik)->where('definition_items.type', 'industry_information')->get()->toArray();

        // $second = DB::table('definition_item_tag_maps')
        // ->join("definition_items", function($join){
        //     $join->on("definition_items.id", "=", "definition_item_tag_maps.definition_item_tags_id");
        // })->join("definitions", function($join){
        //     $join->on("definition_items.definition_id", "=", "definitions.id");
        // })->select("definitions.id", "definition_items.sub_title", "definitions.title", "definition_items.content", "definition_items.id as item_id")
        // ->where('cik', $cik)->where('definition_items.type', 'industry_information')->get()->toArray();

        $sic = DB::table('definitions')
        ->join("definition_items", function($join){
            $join->on("definitions.id", "=", "definition_items.definition_id");
        })->join("definition_item_symbols", function($join){
            $join->on("definition_item_symbols.definition_item_id", "=", "definition_items.id");
        })->select("definitions.id", "definitions.title", "definition_items.id as item_id", "definition_items.type", "definition_items.content", "definition_items.sub_title", "definition_item_symbols.symbol")
        ->where('symbol', $symbol)->where('definition_items.type', 'industry_information')->get()->toArray();

        $finalArrayMerge = array_merge($sic, $first, $second);

        if( !empty($finalArrayMerge) ){
            foreach ($finalArrayMerge as $key => $value) {
                $finalArrayReturn[$value->id]['title'] = $value->title;
                $finalArrayReturn[$value->id]['sub_title'][$value->item_id] = [ 'sub_title' => $value->sub_title, 'content' => $value->content, 'item_id' => $value->item_id ];
            }
        }        

        return $finalArrayReturn;
    }

    public function getDefinitionsKeyConcepts($type)
    {
        $arrayReturn = [];
        $sicNo = $this->profile['sic'];
        $symbol = $this->profile['symbol'];
        $cik = ltrim( $this->profile['cik'], '0' );
        // $sic = DefinitionItemSic::where('sic', $sic)->get();
        $finalSic = [];
        $finalArrayReturn = [];
        $finalArrayMerge = [];
        $first = [];
        $sic = [];

        $first = DB::table('definitions')
        ->join("definition_items", function($join){
            $join->on("definitions.id", "=", "definition_items.definition_id");
        })->join("definition_item_sics", function($join){
            $join->on("definition_item_sics.definition_item_id", "=", "definition_items.id");
        })->select("definition_item_sics.sic", "definitions.id", "definitions.title", "definition_items.id as item_id", "definition_items.type", "definition_items.content", "definition_items.sub_title")
        ->where('sic', $sicNo)->where('definition_items.type', 'key_concept')->get()->toArray();


        $second = DB::table('definition_item_tag_maps')
        ->join("definition_items", function($join){
            $join->on("definition_items.id", "=", "definition_item_tag_maps.definition_item_id");
        })->join("definitions", function($join){
            $join->on("definition_items.definition_id", "=", "definitions.id");
        })->select("definitions.id", "definition_items.sub_title", "definitions.title", "definition_items.content", "definition_items.id as item_id")
        ->where('cik', $cik)->where('definition_items.type', 'key_concept')->get()->toArray();

        
        $sic = DB::table('definitions')
        ->join("definition_items", function($join){
            $join->on("definitions.id", "=", "definition_items.definition_id");
        })->join("definition_item_symbols", function($join){
            $join->on("definition_item_symbols.definition_item_id", "=", "definition_items.id");
        })->select("definitions.id", "definitions.title", "definition_items.id as item_id", "definition_items.type", "definition_items.content", "definition_items.sub_title", "definition_item_symbols.symbol")
        ->where('symbol', $symbol)->where('definition_items.type', 'key_concept')->get()->toArray();

        $finalArrayMerge = array_merge($sic, $first, $second);
        if( !empty($finalArrayMerge) ){
            foreach ($finalArrayMerge as $key => $value) {
                $finalArrayReturn[$value->id]['title'] = $value->title;
                $finalArrayReturn[$value->id]['sub_title'][$value->item_id] = [ 'sub_title' => $value->sub_title, 'content' => $value->content, 'item_id' => $value->item_id ];
            }
        }        
        return $finalArrayReturn;

    }
}