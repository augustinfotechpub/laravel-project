<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use App\Services\RapidAPI;
use Livewire\Component;
use App\Models\DefinitionItem;

class CompanyNewsList extends Component
{
    public $newsList,$alertContent;

    public function mount()
    {
        self::getNews();
        $DefinitionItem = new DefinitionItem();
        $alertData = $DefinitionItem->query()->where(['type' => 'alert', 'identifier' => "news_feed_alert"])->get()->toArray();
        $this->alertContent = @$alertData;
    }

    public function render()
    {
        return view('livewire.company-news-list');
    }

    public function getNews()
    {
        $fmp = new FinancialModelingPrep();
        $this->newsList = $fmp->getStockNews();
    }
}
