<?php

namespace App\Http\Livewire\KeyParties;

use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Livewire\Component;

class PersonEntityDetails extends Component
{
    protected $listeners = ['getDetails' => 'getDetails'];

    public $symbol, $cik, $personHoldingsChart, $peInfo = [], $officer = null,
        $isPersonHoldingsChartLoading = false, $person, $showPersonHoldingsChart = false,
        $loadPersonHoldingsChart = true;

    public function render()
    {
        return view('livewire.key-parties.person-entity-details');
    }

    public function personHoldingsChart()
    {
        $data = [];
        $labels = [];
        $colors = [];
        $fmp = new FinancialModelingPrep();
        $stock_details = $fmp->getKeyExecutives($this->symbol);
        foreach ($stock_details as $stock_detail) {
            $labels[] = str_replace('\'', '', $stock_detail['name']);
            $data[] = $stock_detail['pay'];
            $colors[] = getHaxColor();
        }
        $this->personHoldingsChart = ['data' => $data, 'labels' => $labels, 'colors' => $colors];
        $this->dispatchBrowserEvent('personHoldingsChart', [
            'dataset' => [
                'data' => $data,
                'backgroundColor' => $colors
            ],
            'labels' => $labels
        ]);
        $this->loadPersonHoldingsChart = false;
        $this->showPersonHoldingsChart = true;
    }

    /**
     * Get Ownership Details
     * @param $cik
     * @param $name
     * @param $symbol
     */
    public function getDetails($cik, $name, $symbol)
    {
        $sec = new SecEdgarService();
        $this->cik = $cik;
        $this->symbol = $symbol;
        $this->officer = ['cik' => $cik, 'name' => $name];
        $this->peInfo = $sec->personEntityInfo($this->officer['cik'])['array'];
        self::personHoldingsChart();
    }
}
