<?php

namespace App\Http\Livewire\KeyParties;

use App\Http\Livewire\CustomComponent;
use App\Models\AnalystRatings;
use App\Models\Ownership;
use App\Models\OwnershipInterest;
use App\Repositories\Admin\CompanyRepository;
use App\Services\FinancialModelingPrep;
use App\Services\NasdaqService;
use App\Services\SecEdgarService;
use Goutte\Client;
use Illuminate\Support\Str;
use Livewire\Component;
use App\Models\DefinitionItem;

class KeyParties extends Component
{
    protected $listeners = ['getDetails' => 'showPersonEntityDetails'];
    public $collection, $personHoldingsData = [];

    public $symbol, $cik, $name, $initKeyParties = false, $data = [], $executive_management,
        $board_of_directors, $executives, $directors, $holders, $initGetDetails = false,
        $stock_details, $peInfo = [], $personHoldingsChart, $ownershipInterest = [],
        $officer = null, $person, $form, $transactions, $activeCik, $pressReleases = [],
        $currentOfficer = null, $showPersonTransactionsChart = false,
        $loadPressReleases = true, $institutionalShareholders, $showPersonHoldingsChart = false, $company, $activePersonName,$analystratings,$alertsData = [],$alertsDataComp = ['executive_management_alert','analyst_ratings_alert','ownership_interest_alert'];


    public function mount()
    {
        $this->symbol = session('profile')['symbol'];
        foreach ($this->alertsDataComp as $key => $value) {
            $definitionItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitionItemData) ){
                foreach ($definitionItemData as $ke => $val) {
                    $this->alertsData[$value][] = $val['content'];  
                } 
            }
        }
    }

    public function render()
    {
        return view('livewire.key-parties.key-parties');
    }

    public function keyParties()
    {
        $profile = session('profile');
        $cik = $profile['cik'];

        // print_r(ltrim($cik,0));
        // die;

        $this->executives = Ownership::query()->whereIn('companyCik', [$cik, ltrim($cik,0)])->where('type', 'officer')->get();
        $this->analystratings = AnalystRatings::query()->whereIn('company_cik', [$cik, ltrim($cik,0)])->get();
        $this->institutionalShareholders = OwnershipInterest::query()->whereIn('company_cik', [$cik, ltrim($cik,0)])->get(); ;

    }

    public function showPersonEntityDetails($cik, $name)
    {
        $this->cik = $cik;
        $this->name = $name;
    }

    public function getCompanyOfficers()
    {
        $executive_management = [];
        $board_of_directors = [];
        foreach ($this->stock_details as $officer) {
            if (Str::contains($officer['title'], 'Director')) {
                $board_of_directors[] = $officer;
            } else {
                $executive_management[] = $officer;
            }
        }
        $this->executive_management = $executive_management;
        $this->board_of_directors = $board_of_directors;
    }

    public function getAuditCommitteeMembers()
    {
        $companyRepository = new CompanyRepository();
        $this->company = $companyRepository->getBySymbol($this->symbol);
    }

    public function getHolders()
    {
        $data = [];
        $labels = [];
        $colors = [];
        foreach ($this->stock_details as $stock_detail) {
            $labels[] = str_replace('\'', '', $stock_detail['name']);
            $data[] = $stock_detail['pay'];
            $colors[] = getHaxColor();
        }
        $this->dispatchBrowserEvent('renderChart2', ['data' => $data, 'labels' => $labels, 'colors' => $colors]);
    }

    public function initKeyParties()
    {
        $this->keyParties();
        // $this->getOwnerships();
        $nasdaq = new NasdaqService();
        // $this->institutionalShareholders = $nasdaq->getInstitutionalHolders($this->symbol);
        $this->initKeyParties = true;
    }

    public function getOwnerships()
    {
        $secService = new SecEdgarService();
        $fmp = new FinancialModelingPrep();
        $cik = $fmp->getCikBySymbol($this->symbol);
        $owners = $secService->getOwnerships($cik);

        $this->getDirectors($owners);
        $this->getOfficers($owners);
    }

    public function getDirectors($owners)
    {
        $directors = [];
        foreach ($owners as $owner) {
            if ($owner['position'] == 'director') {
                $directors[] = $owner;
            }
        }
        return $directors;
    }

    public function getOfficers($owners)
    {
        $officers = [];
        foreach ($owners as $owner) {
            if (str_contains($owner['position'], 'officer')) {
                $officers[] = $owner;
            }
        }
        $this->officers = $this->filterRoles($officers);
    }

    private function filterRoles($officers)
    {
        $terms = [
            ['ceo', 'chief executive officer'],
            ['coo', 'chief operating officer'],
            ['cfo', 'chief marketing officer'],
            ['cio', 'chief financial officer', 'chief finance officer'],
            ['cmo', 'chief information officer'],
            ['cto', 'chief technology officer'],
            ['cco', 'chief communications officer', 'chief communication officer'],
        ];
        $array1 = [];
        foreach ($terms as $term) {
            $flag = false;
            foreach ($officers as $officer_key => $officer) {
                foreach ($term as $possibility)
                    if (strpos(strtolower($officer['position']), $possibility) !== false) {
                        $array1[] = $officer;
                        unset($officers[$officer_key]);
                        $flag = true;
                    }
                if ($flag) {
                    break;
                }
            }
            if ($flag) {
                continue;
            }
        }
        return array_merge($array1, $officers);
    }

    public function getPersonEarningStatistics($cik)
    {
        $sec = new SecEdgarService();
        $res = $sec->getAllFillingsList($cik);
        if (is_null($res)) {
            request()->session()->flash('error', "Details not found for : $cik");
            return redirect()->route("app.key-parties", ['symbol' => $this->symbol]);
        }
        /*dd($res);*/
        $this->dispatchBrowserEvent('getPersonEarningStatistics', ['data' => $res ?? [], 'symbol' => $this->symbol]);
    }

    /**
     * Get Ownership Details
     * @param $cik
     * @param $name
     * @param $symbol
     */
    public function getDetails($id = null)
    {
        try {
            if (!isset($this->executives[0])) {
                throw new \Exception('Person not exists!');
            }

            // For Default will be first executive.
            if (is_null($id))
                $id = $this->executives[0]->id;

            $this->currentOfficer = Ownership::query()->where('id', $id)->first();
            $this->reset('transactions', 'showPersonTransactionsChart');
            if (!is_null($this->currentOfficer->issuerCik)) {
                /*$sec = new SecEdgarService();
                $this->peInfo = $sec->personEntityInfo($this->currentOfficer->issuerCik)['array'];
                $this->showPersonHoldingsChart = true;*/
                // $this->getTransactions();
            }
        } catch (\Exception $e) {
        }
        $this->initGetDetails = true;
    }

    public function getTransactions()
    {
        try {
            $fmp = new FinancialModelingPrep();
            $response = $fmp->getInsiderTrading($this->currentOfficer->issuerCik, 1000);
            if (empty($response))
                throw new \Exception("Details not found for : " . $this->currentOfficer->issuerCik);

            $data = [];
            foreach ($response as $res) {
                if ($res['companyCik'] == session('profile')['cik']) {
                    $data[] = $res;
                }
            }

            $transactions = collect($data)->where('companyCik', session('profile')['cik'])->unique('transactionDate')->toArray();
            $this->showPersonTransactionsChart = true;
            $chartData = [
                'name' => $this->currentOfficer->name,
                'data' => array_values(array_column($transactions, 'securitiesOwned')),
                'labels' => array_values(array_column($transactions, 'transactionDate'))
            ];
            $this->dispatchBrowserEvent('personTransactionsChart', $chartData);
        } catch (\Exception $e) {

        }
    }


    public function personHoldingsChart($requestData)
    {
        $shareHoldings = ($requestData['securitiesOwned'] / session('profile')['sharesOutstanding']) * 100;
        $total = 100 - $shareHoldings;
        $data = [$shareHoldings, $total];
        $labels = ['Shareholdings', 'Total'];
        $colors = [];
        foreach ($requestData as $item) {
            $colors[] = getHaxColor();
        }
        $this->personHoldingsChart = ['data' => $data, 'labels' => $labels, 'colors' => $colors];
        $this->dispatchBrowserEvent('personHoldingsChart', [
            'dataset' => [
                'data' => $data,
                'backgroundColor' => $colors
            ],
            'labels' => $labels
        ]);
        $this->showPersonHoldingsChart = true;
    }

    public function getForm($url)
    {
        $client = new Client();
        $html = $client->request('GET', $url)->filter('body')->html();
        $this->form = $html;
        $this->dispatchBrowserEvent('updateFormADBGColor');
    }

    public function getPressRelease()
    {
        $fmp = new FinancialModelingPrep();
        $this->pressReleases = $fmp->getPressRelease($this->symbol);
        $this->loadPressReleases = false;
    }
}
