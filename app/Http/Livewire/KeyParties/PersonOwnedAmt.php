<?php

namespace App\Http\Livewire\KeyParties;

use App\Services\FinancialModelingPrep;
use App\Services\NasdaqService;
use Livewire\Component;

class PersonOwnedAmt extends Component
{
    public $symbol, $profile, $stock_details, $personOwnedAmtChart, $loading = true;

    public function mount()
    {
        $this->profile = session('profile');
        $this->symbol = $this->profile['symbol'];
    }

    public function render()
    {
        return view('livewire.key-parties.person-owned-amt');
    }

    public function getHolders()
    {
        $nasdaq = new NasdaqService();
        $res = $nasdaq->getInstitutionalHolders($this->profile['symbol']);
        $data = [];
        $labels = array_column($res['tbody'], 0);
        $colors = [];
        foreach (array_column($res['tbody'], 1) as $item) {
            $data[] = str_replace('%', '', $item);
            $colors[] = getHaxColor();
        }
        // $this->personOwnedAmtChart = ['data' => $data, 'labels' => $labels, 'colors' => $colors];
        $this->loading = false;
        // $this->dispatchBrowserEvent('personOwnedAmtChart', ['data' => $data, 'labels' => $labels, 'colors' => $colors]);
        $this->dispatchBrowserEvent('personOwnedAmtChart', [
            'dataset' => [
                'data' => $data,
                'backgroundColor' => $colors
            ],
            'labels' => $labels
        ]);
    }
}
