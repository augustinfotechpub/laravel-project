<?php

namespace App\Http\Livewire\KeyParties;

use Asantibanez\LivewireCharts\Models\ColumnChartModel;
use Asantibanez\LivewireCharts\Models\LineChartModel;
use Asantibanez\LivewireCharts\Models\PieChartModel;
use Livewire\Component;

class PersonTransactionsChart extends Component
{
    protected $listeners = [
        'personTransactionsChart' => 'render',
    ];

    public function render($transactions = null)
    {
        $personTransactionsChartModel = null;
        $personTransactionsChartModel = new ColumnChartModel();
        $personTransactionsChartModel->setTitle('Expenses by Type');
        if (!is_null($transactions)) {
            foreach ($transactions as $transaction) {
                $personTransactionsChartModel->addColumn($transaction['transactionDate'], $transaction['securitiesOwned'],getRGBColorString());
            }
        }
        return view('livewire.key-parties.person-transactions-chart')
            ->with(['personTransactionsChartModel' => $personTransactionsChartModel]);
    }
}
