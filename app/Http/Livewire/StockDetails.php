<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use Livewire\Component;

class StockDetails extends Component
{

    public $symbol, $initStockSummary = false, $profile, $industryInformation;

    public function mount()
    {
        $this->profile = session('profile');
        $this->symbol = $this->profile['symbol'];
    }

    public function render()
    {
        return view('livewire.stock-details');
    }

    public function initStockSummary()
    {
        $this->initStockSummary = true;
        $fmp = new FinancialModelingPrep();
        $profile = session('profile');
        $quote = $fmp->quote($this->symbol)[0];
        $this->profile = array_merge($quote, $profile);
        $this->dispatchBrowserEvent('initBSTooltip');
    }
}
