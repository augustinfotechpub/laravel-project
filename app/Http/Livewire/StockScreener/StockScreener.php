<?php

namespace App\Http\Livewire\StockScreener;

use Livewire\Component;

class StockScreener extends Component
{
    public function render()
    {
        return view('livewire.stock-screener.stock-screener');
    }
}
