<?php

namespace App\Http\Livewire\Partials;

use Livewire\Component;
use App\Models\DefinitionItem;

class Navbar extends Component
{
    public $content_component, $alertsDataNav = [], $alertsDataNavSec = ['market_alert', 'home_alert', 'key_parties_alert', 'financials_alert', 'ratios_alert', 'charting_technical_analysis_alert', 'regulatory_filing_alert', 'network_alert', 'screening_alert'];

    /**
     * @ComponentsName = 'app-main',
     * 'calender',
     * 'charting',
     * 'company-news-list',
     * 'company-regular-market-price',
     * 'company-stock-chart',
     * 'filling-details',
     * 'financial-key-stats',
     * 'financial-models',
     * 'financials',
     * 'key-parties',
     * 'person-entity-details',
     * 'person-owned-amt',
     * 'ratios',
     * 'search-bar',
     * 'sec-fillings',
     * 'stock-summary'
     *
     */

    public function mount()
    {
            
        foreach ($this->alertsDataNavSec as $key => $value) {
            $definitioItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitioItemData) ){

                foreach ($definitioItemData as $ke => $val) {
                    $this->alertsDataNav[$value][] = $val['content'];   
                }
                
            }
        }
        $this->fill(['content_component' => 'app-main']);
    }

    public function render()
    {
        return view('livewire.partials.navbar');
    }

    public function changeContentComponent($component)
    {
        $this->content_component = $component;
        $this->emit('changeContentComponent', ['content_component' => $this->content_component]);
    }
}
