<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use Carbon\Carbon;
use Livewire\Component;

class   FinancialModels extends Component
{
    protected $fmp, $income_statement, $balance_sheet;
    public $symbol, $currentModel = 'dcf', $models, $initDCF = false;

    public function mount(FinancialModelingPrep $fmp, $symbol)
    {
        $this->symbol = $symbol;
        $this->fmp = new $fmp;
    }

    public function render()
    {
        return view('livewire.financial-models');
    }

    protected function initFinanceModels()
    {
        $this->fmp = new FinancialModelingPrep();
        // $incomeStatement = $this->fmp->getFinancialStatements($this->symbol);
        self::getDCFModel();
    }

    public function initDCF()
    {
        $this->fmp = new FinancialModelingPrep();
        self::getDCFModel();
        $this->initDCF = true;
    }

    public function changeModel($model)
    {
        $this->currentModel = $model;
    }

    public function getDCFModel()
    {
        $this->balance_sheet = $this->fmp->getBalanceSheet($this->symbol, 10, 'FY');
        $this->income_statement = $this->fmp->getFinancialStatements($this->symbol, 10, 'FY');

        $this->models['dcf']['operating_data'] = self::getOperatingData();
        $this->models['dcf']['balance_sheet_data'] = self::getBalanceSheetData();
        // $this->models['dcf']['weighted_avg_cost_of_capital'] = self::getWeightedAverageCostOfCapital();
        $this->models['dcf']['free_cash_flow_buildup'] = self::getFreeCashFlowBuildup();
        $this->models['dcf']['intrinsic_value'] = self::getIntrinsicValue();
    }

    protected function getBalanceSheetData()
    {
        $cash_flow_statement = $this->fmp->getCashFlowStatement($this->symbol, 10, 'FY');
        $years = self::getYears($this->balance_sheet);
        $years = array_merge(['year'], $years);
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->balance_sheet, 'cashAndShortTermInvestments', 'Total Cash')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($cash_flow_statement, 'accountsReceivables', 'Accounts Receivables')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->balance_sheet, 'inventory', 'Inventories')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($cash_flow_statement, 'accountsPayables', 'Accounts Payable')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($cash_flow_statement, 'capitalExpenditure', 'Capital Expenditure')];
        return [
            'title' => 'Balance Sheet Data',
            'sub_title' => 'All amount in millions',
            'table' => [
                'thead' => $years,
                'tbody_rows' => $rows
            ]
        ];
    }

    protected function getOperatingData()
    {
        $years = self::getYears($this->balance_sheet);
        $years = array_merge(['year'], $years);
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'revenue', 'Revenue')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'ebitda', 'EBITDA')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'ebitdaratio', 'EBITDA Ratio %', true)];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'depreciationAndAmortization', 'Depreciation')];
        return [
            'title' => 'Operating Data',
            'sub_title' => 'All amount in millions',
            'table' => [
                'thead' => $years,
                'tbody_rows' => $rows
            ]
        ];
    }

    protected function getWeightedAverageCostOfCapital()
    {
        $profile = $this->fmp->getProfile($this->symbol);
        $quote = $this->fmp->quote($this->symbol);
        $years = [];
        $rows[] = $this->filterData($profile, 'price', 'Share price');
        $rows[] = $this->filterData($profile, 'beta', 'Beta');
        $rows[] = $this->filterData($quote, 'sharesOutstanding', 'Diluted Shares Outstanding');
        $rows[] = ['Cost of Debt', '--'];
        $rows[] = ['Tax Rate', '--'];
        $rows[] = ['After-tax Cost of Debt', '--'];
        $rows[] = ['Risk-Free Rate', '--'];
        $rows[] = ['Market Risk Premium', '--'];
        $rows[] = ['Cost of Equity', '--'];
        $rows[] = ['Total Debt', '--'];
        $rows[] = ['Total Equity', '--'];
        $rows[] = ['Total Capital', '--'];
        $rows[] = ['Debt Weighting', '--'];
        $rows[] = ['Equity Weighting', '--'];
        $rows[] = ['Wacc', '--'];
        return [
            'title' => 'Weighted Average Cost Of Capital',
            'sub_title' => 'All amount in millions',
            'table' => [
                'thead' => $years,
                'tbody_rows' => $rows
            ]
        ];
    }

    protected function getYears($items)
    {
        $years = [];
        foreach ($items as $item) {
            $years[] = formatDate($item['date'], 'Y');
        }
        return $years;
    }

    protected function filterData($obj, $key, $title, $percentage = false)
    {
        $data = [];
        foreach ($obj as $index => $item) {
            if ($index == 0) {
                $data[] = $title;
            }
            if (!$percentage) {
                $data[] = self::amount($item[$key]);
            } else {
                $data[] = number_format($item[$key], '2') . ' %';
            }
        }
        return $data;
    }

    public function getDCFModelLevered()
    {

    }

    public function getFreeCashFlowBuildup()
    {
        $cash_flow_statement = $this->fmp->getCashFlowStatement($this->symbol, 10, 'FY');
        $years = self::getYears($cash_flow_statement);
        $years = array_merge(['year'], $years);
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'revenue', 'Revenue')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'ebitda', 'EBITDA')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'ebitdaratio', 'EBITDA Ratio %',true)];
        $rows[] = ['styles' => 'border-bottom: 2px solid #1a1b1d;', 'classes' => '', 'columns' => ['Tax Rate', '--', '--', '--', '--', '--', '--', '--', '--', '--', '--']];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($this->income_statement, 'depreciationAndAmortization', 'Depreciation')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($cash_flow_statement, 'accountsReceivables', 'Accounts Receivables')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($cash_flow_statement, 'accountsPayables', 'Accounts Payable')];
        $rows[] = ['styles' => 'border-bottom: 2px solid #1a1b1d;', 'classes' => '', 'columns' => $this->filterData($cash_flow_statement, 'capitalExpenditure', 'Capital Expenditure')];
        return [
            'title' => 'Build Up Free Cash',
            'sub_title' => 'All amount in millions',
            'table' => [
                'thead' => $years,
                'tbody_rows' => $rows
            ]
        ];
    }

    public function getIntrinsicValue()
    {
        $cash_flow_statement = $this->fmp->getCashFlowStatement($this->symbol, 10, 'FY');
        $intrinsic_value = $this->fmp->getCompanyEnterpriseValue($this->symbol, 10, 'FY');
        $balance_sheet = $this->fmp->getBalanceSheet($this->symbol, 10, 'FY');
        $quote = $this->fmp->quote($this->symbol);

        $years = self::getYears($cash_flow_statement);
        $years = array_merge(['year'], $years);
        $title = 'Intrinsic Value';
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($intrinsic_value, 'enterpriseValue', 'Enterprise Value')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($balance_sheet, 'netDebt', 'Net Debt')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($balance_sheet, 'totalStockholdersEquity', 'Equity Value')];
        $rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($quote, 'sharesOutstanding', 'Shares Outstanding')];
        /*$rows[] = ['styles' => '', 'classes' => '', 'columns' => $this->filterData($balance_sheet, 'sharesOutstanding', 'Equity Value Per Share')];*/
        return [
            'title' => 'Intrinsic Value',
            'sub_title' => 'All amount in millions',
            'table' => [
                'thead' => $years,
                'tbody_rows' => $rows
            ]
        ];
    }

    public function getTerminalValue()
    {

    }

    public function getWeightedAverageCoastCapital()
    {

    }

    protected function getModelItem($title, $keyName, $statement)
    {
        $row = [];
        foreach ($statement as $key => $statementItem) {
            if ($key == 0) {
                $row[] = $title;
            }
            $row[] = self::amount($statementItem[$keyName]);
        }
        return $row;
    }

    public function amount($amount)
    {

        return "<span style='color:" . ($amount < 0 ? 'red' : 'green') . "'>" . currencyInMillion($amount) . "</span>";
    }
}
