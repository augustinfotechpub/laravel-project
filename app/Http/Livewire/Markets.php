<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Markets extends Component
{
    public function render()
    {
        return view('livewire.markets');
    }
}
