<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use App\Services\RapidAPI;
use Carbon\Carbon;
use Livewire\Component;
use App\Models\eod_balance_sheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Models\DefinitionItem;

class Ratios extends Component
{
    public $period='yearly';
    public $initRatios, $symbol, $ratios = [], $initSection = false;
    public $quote, $financialRatiosQ, $profile,$financialRatiosY, $cashFlowStatement, $incomeStatement, $companyOutlook, $keyMetricsTTM, $keyMetricsQ, $balanceStatement,$currentyear,$year='2020',$allYear;
    public $alertsData = [],$alertsDataComp = ['interest_coverage_ratios_alert','fixed_charge_coverage_ratios_alert','inventory_turnover_ratios_alert','days_of_inventory_on_hand_doh_ratios_alert','receivables_turnover_ratios_alert','days_of_sales_outstanding_dso_ratios_alert','payables_turnover_ratios_alert','days_payable_outstanding_dpo_ratios_alert','working_capital_turnover_ratios_alert','fixed_asset_turnover_ratios_alert','total_asset_turnover_ratios_alert','cash_flow_to_revenue_ratios_alert','cash_return_on_assets_ratios_alert','cash_return_on_equity_ratios_alert','cash_to_operating_income_ratios_alert','cash_flow_per_share_ratios_alert','debt_payment_ratios_alert','dividend_payment_ratios_alert','investing_and_financing_ratios_alert','debt_coverage_ratios_alert','performance_interest_coverage_ratios_alert','reinvestment_ratios_alert','ebit_interest_coverage_ratios_alert','ebitda_interest_coverage_ratios_alert','return_on_capital_ratios_alert','free_operating_cash_flow_to_debt_ratios_alert','discretionary_cash_flow_to_debt_ratios_alert','cash_flow_net_to_capex_ratios_alert','gross_profit_margin_ratios_alert','operating_margin_ratios_alert','pre_tax_margin_ratios_alert','net_income_profit_margin_ratios_alert','operating_roa_ratios_alert','roa_ratios_alert','return_on_total_capital_ratios_alert','return_on_total_equity_ratios_alert','return_on_common_equity_ratios_alert','debt_to_asset_ratio_ratios_alert','debt_to_capital_ratio_ratios_alert','debt_to_equity_ratios_alert','financial_leverage_ratios_alert','p/e_ratios_alert','p/cf_ratios_alert','p/s_ratios_alert','p/bv_ratios_alert','ebitda/share_ratios_alert','dividend_per_share_ratios_alert','dividend_payout_ratio_ratios_alert','dividend_retention_ratio_ratios_alert','sustainable_growth_rate_ratios_alert','current_ratio_ratios_alert','quick_ratio_ratios_alert','cash_ratio_ratios_alert','defensive_interval_ratio_ratios_alert','cash_conversion_cycle_ratios_alert','coverage_ratios_alert','activity_ratios_alert','performance_ratios_alert','credit_ratios_alert','return_on_sale_ratios_alert','return_on_investment_ratios_alert','solvency_ratios_alert','valuation_ratios_alert','price_per_share_ratios_alert','dividend_payout_ratios_alert','liquidity_ratios_alert'];

    public function mount()
    {
        $this->profile = session('profile');
        $this->symbol = $this->profile['symbol'];
        $this->fill([
            'initRatios' => false,
            'ratios' => []
        ]);
        foreach ($this->alertsDataComp as $key => $value) {
            $definitionItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitionItemData) ){
                foreach ($definitionItemData as $ke => $val) {
                    $this->alertsData[$value][] = $val['content'];  
                } 
            }
        }
    }

    public function render()
    {
        if($this->period=='yearly')
        {
        $this->allYear = eod_balance_sheet::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $this->symbol)
        ->where('period_type','yearly')
        ->orderBy('dates','desc')
        ->limit(10)
        ->pluck('dates')->unique()->toArray();
        }
        else
        {
        $this->allYear= eod_balance_sheet::selectRaw('date')
        ->where('ticker', $this->symbol)
        ->where('period_type','quarterly')
        ->orderBy('date','desc')
        ->limit(40)
        ->pluck('date')->unique()->toArray();
        }
        return view('livewire.ratios');

    }

    public function initRatios()
    {
        $this->initRatios = true;
        $fmp = new FinancialModelingPrep();
        $profile = session('profile');
        $quote = $fmp->quote($this->symbol)[0];
        $this->profile = array_merge($quote, $profile);
    }

    public function changeRatioPeriod($period)
    {
        $this->period = $period;
        if($this->period=='yearly')
        {
        $current_year = eod_balance_sheet::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $this->symbol)
        ->where('period_type','yearly')
        ->orderBy('dates','desc')
        ->limit(1)
        ->pluck('dates')->unique();
        }
        else
        {
        $current_year= eod_balance_sheet::selectRaw('date')
        ->where('ticker', $this->symbol)
        ->where('period_type','quarterly')
        ->orderBy('date','desc')
        ->limit(1)
        ->pluck('date')->unique();
        }
        $this->year=$current_year[0];
        self::getData();
    }

    public function change($period,$year)
    {
        $this->year=$year;
        $this->period = $period;
        self::getData();
    }
    // public function sumData($statement, $key_name)
    // {
        // $sum = 0;
        // foreach ($statement as $key => $value) {
        //     $sum += !is_null($value[$key_name]) ? $value[$key_name] : 0;
        // }
        // return $sum;
        
    // }

    public function getData()
    {
        $fmp = new FinancialModelingPrep();
        $this->quote = $fmp->quote($this->symbol)[0];
        // $this->incomeStatement = $fmp->getFinancialStatements($this->symbol, 4, 'quarter');
        // $this->cashFlowStatement = $fmp->getCashFlowStatement($this->symbol, 4, 'quarter');
        $this->companyOutlook = array(); //$fmp->getCompanyOutlook($this->symbol);
        $this->keyMetricsTTM = array(); //$fmp->getKeyMetrics($this->symbol, 1, 'ttm')[0];
        $this->keyMetricsQ = array(); //$fmp->getKeyMetrics($this->symbol, 4, 'quarterly')[0];
        // $this->balanceStatement = $fmp->getBalanceSheet($this->symbol, 1, 'quarter')[0];
        $this->financialRatiosQ = $fmp->getEODFinancialStatements($this->profile['cik'], 4, $this->period,$this->year);
        // $this->financialRatiosY = $fmp->getFinancialRatios($this->symbol, 5, 'FY');
    

        $this->ratios['Valuation Ratios'] = self::getValuationRatios();
        $this->ratios['Coverage Ratios'] = self::getCoverageRatios();
        $this->ratios['Activity Ratios'] = self::getActivityRatios();
        $this->ratios['Performance Ratios'] = self::getPerformanceRatios();
        $this->ratios['Credit Ratios'] = self::getCreditRatios();
        $this->ratios['Return on Sales (Revenue) Ratios'] = self::getBalanceSummary();
        $this->ratios['Return on Investment Ratios'] = self::getCashFlowSummary();
        $this->ratios['Solvency Ratios'] = self::getSolvencyRatios();
        $this->ratios['Dividend-Related Ratios'] = self::getDividendRelatedRatios();
        $this->ratios['Price Per Share Ratios'] = self::getPricePerShareRatios();
        $this->ratios['Liquidity Ratios'] = self::getLiquidityRatios();
        $this->initSection = true;



    }

    public function getCreditRatios()
    {
        $table['heading'] = 'Credit Ratios';
        $table['rows'] = [
            [
                'title' => 'EBIT Interest Coverage',
                'identifier' => 'ebit_interest_coverage_ratios_alert',
                'value' => abs(number_format((float)$this->division3((float)@$this->financialRatiosQ['IS']['ebit'] , ((float)@$this->financialRatiosQ['IS']['interestExpense'])),2,'.','')),
            ], [
                'title' => 'EBITDA Interest Coverage',
                'identifier' => 'ebitda_interest_coverage_ratios_alert',
                'value' => abs(number_format((float)$this->division3((float)@$this->financialRatiosQ['IS']['ebitda'], ((float)@$this->financialRatiosQ['IS']['interestExpense'])),2,'.','')),
            ], [
                'title' => 'Return On Capital',
                'identifier' => 'return_on_capital_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['IS']['ebit'] ), (((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity'])+((float)@$this->financialRatiosQ['BSP']['shortLongTermDebtTotal'])+((float)@$this->financialRatiosQ['BS']['totalStockholderEquity']+(float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal']))/2),2,'.',''),   
            ], [
                'title' => 'Free Operating Cash Flow to Debt',
                'identifier' => 'free_operating_cash_flow_to_debt_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] - (float)@$this->financialRatiosQ['CF']['capitalExpenditures'] ), (((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal']) + ((float)@$this->financialRatiosQ['BSP']['shortLongTermDebtTotal']) )/2 ),2,'.',''),
            ], [
                'title' => 'Discretionary Cash Flow to Debt',
                'identifier' => 'discretionary_cash_flow_to_debt_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] - abs((float)@$this->financialRatiosQ['CF']['dividendsPaid']) - (float)@$this->financialRatiosQ['CF']['capitalExpenditures']) , ( ((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal']) + ((float)@$this->financialRatiosQ['BSP']['shortLongTermDebtTotal']) )/2),2,'.',''),
            ], [
                'title' => 'Cash Flow (net) to CAPEX',
                'identifier' => 'cash_flow_net_to_capex_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] - abs((float)@$this->financialRatiosQ['CF']['dividendsPaid'] )), ((float)@$this->financialRatiosQ['CF']['capitalExpenditures'])),2,'.',''),
            ],
        ];
        return $table;

    }

    public function getBalanceSummary()
    {
        $table['heading'] = 'Return on Sales (Revenue) Ratios';
        $table['rows'] = [
            [
                'title' => 'Gross Profit Margin',
                'identifier' => 'gross_profit_margin_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['IS']['grossProfit'] ), ((float)@$this->financialRatiosQ['IS']['totalRevenue'])),2,'.',''),
            ], [
                'title' => 'Operating Margin',
                'identifier' => 'operating_margin_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['IS']['operatingIncome'] ), ((float)@$this->financialRatiosQ['IS']['totalRevenue'])),2,'.',''),
            ], [
                'title' => 'Pre-Tax Margin',
                'identifier' => 'pre_tax_margin_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['IS']['incomeBeforeTax']), ((float)@$this->financialRatiosQ['IS']['totalRevenue'])),2,'.',''),
            ], [
                'title' => 'Net Income (Profit) Margin',
                'identifier' => 'net_income_profit_margin_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['IS']['netIncome']) , ((float)@$this->financialRatiosQ['IS']['totalRevenue'])),2,'.',''),
            ]
        ];
        return $table;
    }

    public function getCashFlowSummary()
    {
        if($this->period == 'yearly'){
        $table['heading'] = 'Return on Investment Ratios';
        $table['rows'] = [
            [
                'title' => 'Operating ROA',
                'identifier' => 'operating_roa_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['operatingIncome']) , ((float)@$this->financialRatiosQ['BSP']['totalAssets']+(float)@$this->financialRatiosQ['BS']['totalAssets'])/2),2,'.',''),
            ], [
                'title' => 'ROA',
                'identifier' => 'roa_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['netIncome']) , ((float)@$this->financialRatiosQ['BSP']['totalAssets']+(float)@$this->financialRatiosQ['BS']['totalAssets'])/2),2,'.',''),
            ],[
                'title' => 'Return on Total Capital',
                'identifier' => 'return_on_total_capital_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['ebit']) , (((float)@$this->financialRatiosQ['BS']['netDebt']) + ((float)@$this->financialRatiosQ['BSP']['netDebt']) +((float)@$this->financialRatiosQ['BS']['totalStockholderEquity']) + ((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity']))/2 ),2,'.',''),
            ],[
                'title' => 'Return on Total Equity',
                'identifier' => 'return_on_total_equity_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['netIncome']) , ((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity']+(float)@$this->financialRatiosQ['BS']['totalStockholderEquity'])/2),2,'.',''),
            ],[
                'title' => 'Return on Common Equity',
                'identifier' => 'return_on_common_equity_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['netIncome'] - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'])*0.06), ((float)@$this->financialRatiosQ['BSP']['commonStockTotalEquity']+(float)@$this->financialRatiosQ['BS']['commonStockTotalEquity'])/2),2,'.',''),

            ]
        ];
        return $table;
    }
        else{
            $table['heading'] = 'Return on Investment Ratios';
            $table['rows'] = [
            [
                'title' => 'Operating ROA',
                'identifier' => 'operating_roa_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['operatingIncome']) , ((float)@$this->financialRatiosQ['BSP']['totalAssets']+(float)@$this->financialRatiosQ['BS']['totalAssets'])/2),2,'.',''),
            ], [
                'title' => 'ROA',
                'identifier' => 'roa_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['netIncome']) , ((float)@$this->financialRatiosQ['BSP']['totalAssets']+(float)@$this->financialRatiosQ['BS']['totalAssets'])/2),2,'.',''),
            ],[
                'title' => 'Return on Total Capital',
                'identifier' => 'return_on_total_capital_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['ebit']) , (((float)@$this->financialRatiosQ['BS']['netDebt']) + ((float)@$this->financialRatiosQ['BSP']['netDebt']) +((float)@$this->financialRatiosQ['BS']['totalStockholderEquity']) + ((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity']))/2 ),2,'.',''),
            ],[
                'title' => 'Return on Total Equity',
                'identifier' => 'return_on_total_equity_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['netIncome']) , ((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity']+(float)@$this->financialRatiosQ['BS']['totalStockholderEquity'])/2),2,'.',''),
            ],[
                'title' => 'Return on Common Equity',
                'identifier' => 'return_on_common_equity_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['IS']['netIncome'] - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'])*0.015), ((float)@$this->financialRatiosQ['BSP']['commonStockTotalEquity']+(float)@$this->financialRatiosQ['BS']['commonStockTotalEquity'])/2),2,'.',''),

            ]
        ];
        return $table;
        }

    }

    public function getPerformanceRatios()
    {
        if($this->period == 'yearly'){
        $table['heading'] = 'Performance Ratios';
        $table['rows'] = [
            [
                'title' => 'Cash Flow to Revenue',
                'identifier' => 'cash_flow_to_revenue_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , ((float)@$this->financialRatiosQ['IS']['totalRevenue'])),2,'.',''),
            ], [
                'title' => 'Cash Return on Assets',
                'identifier' => 'cash_return_on_assets_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , (((float)@$this->financialRatiosQ['BSP']['totalAssets'])+((float)@$this->financialRatiosQ['BS']['totalAssets']))/2),2,'.',''),
            ],[
                'title' => 'Cash Return on Equity',
                'identifier' => 'cash_return_on_equity_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , (((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity']+(float)@$this->financialRatiosQ['BS']['totalStockholderEquity'])/2)),2,'.',''),
            ],[
                'title' => 'Cash to Operating Income',
                'identifier' => 'cash_to_operating_income_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ), ((float)@$this->financialRatiosQ['IS']['operatingIncome'])),2,'.',''),
            ],[
                'title' => 'Cash Flow Per Share',
                'identifier' => 'cash_flow_per_share_ratios_alert',
                'value' => number_format((float)$this-> division3((((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ) - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'])*0.06), (((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']) + ((float)@$this->financialRatiosQ['BSP']['commonStockSharesOutstanding']))/2 ),2,'.',''),
            ],[
                'title' => 'Debt Payment',
                'identifier' => 'debt_payment_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , ((float)@$this->financialRatiosQ['BS']['longTermDebtTotal'])*0.045),2,'.',''),
            ],[
                'title' => 'Dividend Payment',
                'identifier' => 'dividend_payment_ratios_alert',
                'value' => number_format((float)$this-> division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid']))),2,'.',''),
            ],[
                'title' => 'Investing and Financing',
                'identifier' => 'investing_and_financing_ratios_alert',
                'value' => number_format((float)$this-> division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , (((float)@$this->financialRatiosQ['CF']['totalCashflowsFromInvestingActivities'])+(float)@$this->financialRatiosQ['CF']['totalCashFromFinancingActivities'])),2,'.',''),
            ],[
                'title' => 'Debt Coverage',
                'identifier' => 'debt_coverage_ratios_alert',
                'value' => number_format((float)$this-> division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ), (((float)@$this->financialRatiosQ['BSP']['shortLongTermDebtTotal']) + ((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal'])/2 ) ),2,'.',''),
            ],[
                'title' => 'Performance Interest Coverage',
                'identifier' => 'performance_interest_coverage_ratios_alert',
                'value' => abs(number_format((float)$this-> division3((((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) + ((float)@$this->financialRatiosQ['IS']['interestExpense'])+(float)@$this->financialRatiosQ['IS']['incomeTaxExpense']),((float)@$this->financialRatiosQ['IS']['interestExpense'])),2,'.','')),
            ],[
                'title' => 'Reinvestment',
                'identifier' => 'reinvestment_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ), ((float)@$this->financialRatiosQ['CF']['capitalExpenditures'])),2,'.',''),
            ],
        ];
        return $table;
    }
        else{
            $table['heading'] = 'Performance Ratios';
        $table['rows'] = [
            [
                'title' => 'Cash Flow to Revenue',
                'identifier' => 'cash_flow_to_revenue_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , ((float)@$this->financialRatiosQ['IS']['totalRevenue'])),2,'.',''),
            ], [
                'title' => 'Cash Return on Assets',
                'identifier' => 'cash_return_on_assets_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , (((float)@$this->financialRatiosQ['BSP']['totalAssets'])+((float)@$this->financialRatiosQ['BS']['totalAssets']))/2),2,'.',''),
            ],[
                'title' => 'Cash Return on Equity',
                'identifier' => 'cash_return_on_equity_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , ((float)@$this->financialRatiosQ['BSP']['totalStockholderEquity']+(float)@$this->financialRatiosQ['BS']['totalStockholderEquity'])/2),2,'.',''),
            ],[
                'title' => 'Cash to Operating Income',
                'identifier' => 'cash_to_operating_income_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ), ((float)@$this->financialRatiosQ['IS']['operatingIncome'])),2,'.',''),
            ],[
                'title' => 'Cash Flow Per Share',
                'identifier' => 'cash_flow_per_share_ratios_alert',
                'value' => number_format((float)$this-> division3((((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ) - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'])*0.06), (((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']) + ((float)@$this->financialRatiosQ['BSP']['commonStockSharesOutstanding']))/2 ),2,'.',''),
            ],[
                'title' => 'Debt Payment',
                'identifier' => 'debt_payment_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , ((float)@$this->financialRatiosQ['BS']['longTermDebtTotal'])*0.01125),2,'.',''),
            ],[
                'title' => 'Dividend Payment',
                'identifier' => 'dividend_payment_ratios_alert',
                'value' => number_format((float)$this-> division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid']))),2,'.',''),
            ],[
                'title' => 'Investing and Financing',
                'identifier' => 'investing_and_financing_ratios_alert',
                'value' => number_format((float)$this-> division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) , (((float)@$this->financialRatiosQ['CF']['totalCashflowsFromInvestingActivities'])+(float)@$this->financialRatiosQ['CF']['totalCashFromFinancingActivities'])),2,'.',''),
            ],[
                'title' => 'Debt Coverage',
                'identifier' => 'debt_coverage_ratios_alert',
                'value' => number_format((float)$this-> division2(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ), (((float)@$this->financialRatiosQ['BSP']['shortLongTermDebtTotal']) + ((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal'])/2 ) ),2,'.',''),
            ],[
                'title' => 'Performance Interest Coverage',
                'identifier' => 'performance_interest_coverage_ratios_alert',
                'value' => abs(number_format((float)$this-> division3((((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities']) + ((float)@$this->financialRatiosQ['IS']['interestExpense'])+(float)@$this->financialRatiosQ['IS']['incomeTaxExpense']),((float)@$this->financialRatiosQ['IS']['interestExpense'])),2,'.','')),
            ],[
                'title' => 'Reinvestment',
                'identifier' => 'reinvestment_ratios_alert',
                'value' => number_format((float)$this-> division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ), ((float)@$this->financialRatiosQ['CF']['capitalExpenditures'])),2,'.',''),
            ],
        ];
        return $table;

        }
    }

    public function getActivityRatios()
    {
        if($this->period == 'yearly'){
        $table['heading'] = 'Activity Ratios';
        $table['rows'] = [
            [
                'title' => 'Inventory Turnover',
                'identifier' => 'inventory_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['inventory']+((float)@$this->financialRatiosQ['BS']['inventory']))/2)),2,'.',''),

            ], [
                'title' => 'Days of Inventory on Hand (DOH)',
                'identifier' => 'days_of_inventory_on_hand_doh_ratios_alert',
                'value' => number_format((float)($this->division3(365,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['inventory']+(float)@$this->financialRatiosQ['BS']['inventory'])/2)))),2,'.',''),
                ],
            [
                'title' => 'Receivables Turnover',
                'identifier' => 'receivables_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), (((float)@$this->financialRatiosQ['BSP']['netReceivables']+(float)@$this->financialRatiosQ['BS']['netReceivables'])/2))),2,'.',''),
            ],
            [
                'title' => 'Days of Sales Outstanding (DSO)',
                'identifier' => 'days_of_sales_outstanding_dso_ratios_alert',
                'value' => number_format((float)($this->division3(365,($this->division1(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ),(((float)@$this->financialRatiosQ['BSP']['netReceivables']+(float)@$this->financialRatiosQ['BS']['netReceivables'])/2))))),2,'.',''),
            ],
            [
                'title' => 'Payables Turnover',
                'identifier' => 'payables_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['accountsPayable']+(float)@$this->financialRatiosQ['BS']['accountsPayable'])/2)),2,'.',''),
            ],
            [
                'title' => 'Days Payable Outstanding (DPO)',
                'identifier' => 'days_payable_outstanding_dpo_ratios_alert',
                'value' => number_format((float)($this->division3(365,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue']),((float)@$this->financialRatiosQ['BSP']['accountsPayable']+(float)@$this->financialRatiosQ['BS']['accountsPayable'])/2)))),2,'.',''),
            ],
            [
                'title' => 'Working Capital Turnover',
                'identifier' => 'working_capital_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['netWorkingCapital']+(float)@$this->financialRatiosQ['BS']['netWorkingCapital'])/2)),2,'.',''),
            ],
            [
                'title' => 'Fixed Asset Turnover',
                'identifier' => 'fixed_asset_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['propertyPlantEquipment']+(float)@$this->financialRatiosQ['BS']['propertyPlantEquipment'])/2)),2,'.',''),
            ],
            [
                'title' => 'Total Asset Turnover',
                'identifier' => 'total_asset_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['totalAssets']+(float)@$this->financialRatiosQ['BS']['totalAssets'])/2)),2,'.',''),
            ],
        ];
        return $table;
    }
        else{
            $table['heading'] = 'Activity Ratios';
            $table['rows'] = [
            [
                'title' => 'Inventory Turnover',
                'identifier' => 'inventory_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['inventory']+((float)@$this->financialRatiosQ['BS']['inventory']))/2)),2,'.',''),

            ], [
                'title' => 'Days of Inventory on Hand (DOH)',
                'identifier' => 'days_of_inventory_on_hand_doh_ratios_alert',
                'value' => number_format((float)($this->division3(90,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['inventory']+(float)@$this->financialRatiosQ['BS']['inventory'])/2)))),2,'.',''),
                ],
            [
                'title' => 'Receivables Turnover',
                'identifier' => 'receivables_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['netReceivables']+(float)@$this->financialRatiosQ['BS']['netReceivables'])/2)),2,'.',''),
            ],
            [
                'title' => 'Days of Sales Outstanding (DSO)',
                'identifier' => 'days_of_sales_outstanding_dso_ratios_alert',
                'value' => number_format((float)($this->division3(90,($this->division1(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ),((float)@$this->financialRatiosQ['BSP']['netReceivables']+(float)@$this->financialRatiosQ['BS']['netReceivables'])/2)))),2,'.',''),
            ],
            [
                'title' => 'Payables Turnover',
                'identifier' => 'payables_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['accountsPayable']+(float)@$this->financialRatiosQ['BS']['accountsPayable'])/2)),2,'.',''),
            ],
            [
                'title' => 'Days Payable Outstanding (DPO)',
                'identifier' => 'days_payable_outstanding_dpo_ratios_alert',
                'value' => number_format((float)($this->division3(90,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue']),(((float)@$this->financialRatiosQ['BSP']['accountsPayable'])+(float)@$this->financialRatiosQ['BS']['accountsPayable'])/2)))),2,'.',''),
            ],
            [
                'title' => 'Working Capital Turnover',
                'identifier' => 'working_capital_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['netWorkingCapital']+(float)@$this->financialRatiosQ['BS']['netWorkingCapital'])/2)),2,'.',''),
            ],
            [
                'title' => 'Fixed Asset Turnover',
                'identifier' => 'fixed_asset_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['propertyPlantEquipment']+(float)@$this->financialRatiosQ['BS']['propertyPlantEquipment'])/2)),2,'.',''),
            ],
            [
                'title' => 'Total Asset Turnover',
                'identifier' => 'total_asset_turnover_ratios_alert',
                'value' => number_format((float)($this->division3(((float)@$this->financialRatiosQ['IS']['totalRevenue'] ), ((float)@$this->financialRatiosQ['BSP']['totalAssets']+(float)@$this->financialRatiosQ['BS']['totalAssets'])/2)),2,'.',''),
            ],
        ];
        return $table;
        }

    }

    public function getCoverageRatios()
    {
        $table['heading'] = 'Coverage Ratios';
        $table['rows'] = [
            [
                'title' => 'Interest Coverage',
                'identifier' => 'interest_coverage_ratios_alert',
                'value' => abs(number_format((float)($this-> division3(((float)@$this->financialRatiosQ['IS']['ebit'] ), ((float)@$this->financialRatiosQ['IS']['interestExpense']))),2,'.','')),
            ], [
                'title' => 'Fixed Charge Coverage',
                'identifier' => 'fixed_charge_coverage_ratios_alert',
                 'value' => number_format((float)($this-> division3((((float)@$this->financialRatiosQ['IS']['ebit']) +(float)@$this->financialRatiosQ['BS']['capitalLeaseObligations']) ,((float)@$this->financialRatiosQ['IS']['interestExpense']+(float)@$this->financialRatiosQ['BS']['capitalLeaseObligations']))),2,'.',''),
            ],
        ];
        return $table;
    }

    public function getDividendRelatedRatios()
    {
        if($this->period == 'yearly'){
        $table['heading'] = 'Dividend-Related Ratios';
        $table['rows'] = [
            [
                'title' => 'Dividend Payout Ratio',
                'identifier' => 'dividend_payout_ratio_ratios_alert',
                'value' => abs(number_format((float)$this->division3(abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid'] )), ((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'])),2,'.','')),
            ],
            [
                'title' => 'Dividend Retention Rate',
                'identifier' => 'dividend_retention_ratio_ratios_alert',
                'value' => number_format((float)$this->division3($this->substract((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'] ,abs((float)@$this->financialRatiosQ['CF']['dividendsPaid'] )), ((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'])),2,'.',''),
            ],
            [
                'title' => 'Sustainable Growth Rate',
                'identifier' => 'sustainable_growth_rate_ratios_alert',
                'value' => number_format((float)$this->division3($this->division3($this->substract(((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'] ),abs((float)@$this->financialRatiosQ['CF']['dividendsPaid'] )), ((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'])),($this->division1(((float)@$this->financialRatiosQ['IS']['netIncome'] - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'])*0.06), ((((float)@$this->financialRatiosQ['BSP']['commonStockTotalEquity'])+((float)@$this->financialRatiosQ['BS']['commonStockTotalEquity']))/2)))),2,'.',''),
            ]
        ];
        return $table;
    }
        else{
            $table['heading'] = 'Dividend-Related Ratios';
            $table['rows'] = [
            [
                'title' => 'Dividend Payout Ratio',
                'identifier' => 'dividend_payout_ratio_ratios_alert',
                'value' => number_format((float)$this->division3(abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid'] )), ((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'])),2,'.',''),
            ],
            [
                'title' => 'Dividend Retention Rate',
                'identifier' => 'dividend_retention_ratio_ratios_alert',
                'value' => number_format((float)$this->division3($this->substract(((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'] ),abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid'] ))), ((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'])),2,'.',''),
            ],
            [
                'title' => 'Sustainable Growth Rate',
                'identifier' => 'sustainable_growth_rate_ratios_alert',
                'value' => number_format((float)$this->division3($this->division1($this->substract(((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'] ),abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid'] ))), ((float)@$this->financialRatiosQ['IS']['netIncomeApplicableToCommonShares'])),$this->division1(((float)@$this->financialRatiosQ['IS']['netIncome'] - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'])*0.015), (((float)@$this->financialRatiosQ['BSP']['commonStockTotalEquity'])+((float)@$this->financialRatiosQ['BS']['commonStockTotalEquity']))/2)),2,'.',''),
            ]
        ];
        return $table;
        }
    }

    public function getSolvencyRatios()
    {
        $table['heading'] = 'Solvency Ratios';
        $table['rows'] = [
            [
                'title' => 'Debt-to-Asset Ratio',
                'identifier' => 'debt_to_asset_ratio_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal'] ), ((float)@$this->financialRatiosQ['BS']['totalAssets'])),2,'.',''),

            ], [
                'title' => 'Debt-to-Capital Ratio',
                'identifier' => 'debt_to_capital_ratio_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal'] ), (((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal'])+((float)@$this->financialRatiosQ['BS']['totalStockholderEquity']))),2,'.',''),
            ], [
                'title' => 'Debt-to-Equity',
                'identifier' => 'debt_to_equity_ratios_alert',
                'value' => number_format((float)$this->division2(((float)@$this->financialRatiosQ['BS']['shortLongTermDebtTotal'] ), ((float)@$this->financialRatiosQ['BS']['totalStockholderEquity'])),2,'.',''),
                
            ], [
                'title' => 'Financial Leverage',
                'identifier' => 'financial_leverage_ratios_alert',
                'value' => number_format((float)$this->division2( ((float)@$this->financialRatiosQ['BS']['totalAssets']),(float)@$this->financialRatiosQ['BS']['totalStockholderEquity']),2,'.',''),
            ],
        ];
        return $table;
    }

    public function getValuationRatios()
    {
        $table['heading'] = 'Valuation Ratios';
        $table['rows'] = [
            [
                'title' => 'P/E',
                'identifier' => 'p/e_ratios_alert',
                'value' => number_format((float)$this->division3($this->profile['price'],((float)@$this->financialRatiosQ['IS']['eps'])),2,'.',''),
            ], [
                'title' => 'P/CF',
                'identifier' => 'p/cf_ratios_alert',
                'value' => number_format((float)$this->division3($this->profile['price'],($this->division1((((float)@$this->financialRatiosQ['CF']['freeCashFlow'])+((float)@$this->financialRatiosQ['CF']['capitalExpenditures'])),((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding'])))),2,'.',''),
            ], [
                'title' => 'P/S',
                'identifier' => 'p/s_ratios_alert',
                'value' => number_format((float)$this->division3($this->profile['price'],$this->division1(((float)@$this->financialRatiosQ['IS']['totalRevenue']),((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']))),2,'.',''),
            ], [
                'title' => 'P/BV',
                'identifier' => 'p/bv_ratios_alert',
                'value' => number_format((float)$this->division3($this->profile['price'],$this->division1((((float)@$this->financialRatiosQ['BS']['totalAssets'])-((float)@$this->financialRatiosQ['BS']['totalLiab'])),((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']))),2,'.',''),
            ],
        ];
        return $table;
    }

    public function getPricePerShareRatios()
    {
        if($this->period == 'yearly'){
        $table['heading'] = 'Price Per Share Ratios';
        $table['rows'] = [
            [
                'title' => 'Cash Flow Per Share',
                'identifier' => 'cash_flow_per_share_ratios_alert',
                'value' => abs(number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ) - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'] *0.06), (((float)@$this->financialRatiosQ['BSP']['commonStockSharesOutstanding'])+((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']))/2),2,'.','')),

            ],
            [
                'title' => 'EBITDA / Share',
                'identifier' => 'ebitda/share_ratios_alert',
                'value' => abs(number_format((float)$this->division3(((float)@$this->financialRatiosQ['IS']['ebitda'] ), (((float)@$this->financialRatiosQ['BSP']['commonStockSharesOutstanding'])+((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']))/2),2,'.','')),

            ],
            [
                'title' => 'Dividend Per Share',
                'identifier' => 'dividend_per_share_ratios_alert',
                'value' => abs(number_format((float)$this->division3(abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid'])) , ((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding'])),2,'.','')),

            ]
        ];
        return $table;
    }
    else{
        $table['heading'] = 'Price Per Share Ratios';
        $table['rows'] = [
            [
                'title' => 'Cash Flow Per Share',
                'identifier' => 'cash_flow_per_share_ratios_alert',
                'value' => abs(number_format((float)$this->division3(((float)@$this->financialRatiosQ['CF']['totalCashFromOperatingActivities'] ) - ((float)@$this->financialRatiosQ['BS']['preferredStockTotalEquity'] *0.015), (((float)@$this->financialRatiosQ['BSP']['commonStockSharesOutstanding'])+((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']))/2),2,'.','')),

            ],
            [
                'title' => 'EBITDA / Share',
                'identifier' => 'ebitda/share_ratios_alert',
                'value' => abs(number_format((float)$this->division3(((float)@$this->financialRatiosQ['IS']['ebitda'] ), (((float)@$this->financialRatiosQ['BSP']['commonStockSharesOutstanding'])+((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding']))/2),2,'.','')),

            ],
            [
                'title' => 'Dividend Per Share',
                'identifier' => 'dividend_per_share_ratios_alert',
                'value' => abs(number_format((float)$this->division3(abs(((float)@$this->financialRatiosQ['CF']['dividendsPaid'])) , ((float)@$this->financialRatiosQ['BS']['commonStockSharesOutstanding'])),2,'.','')),

            ]
            ];
            return $table;

            }
    }

    public function getLiquidityRatios()
    {
        if($this->period == 'yearly'){
        $table['heading'] = 'Liquidity Ratios';
        $table['rows'] = [ 
            [
                'title' => 'Current Ratio',
                'identifier' => 'current_ratio_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['BS']['totalCurrentAssets']) , ((float)@$this->financialRatiosQ['BS']['totalCurrentLiabilities'])),2,'.',''),    
            ],
            [
                'title' => 'Quick Ratio',
                'identifier' => 'quick_ratio_ratios_alert',
                'value' => number_format((float)$this->division3((((float)@$this->financialRatiosQ['BS']['cashAndShortTermInvestments'])+((float)@$this->financialRatiosQ['BS']['netReceivables'])) , ((float)@$this->financialRatiosQ['BS']['totalCurrentLiabilities'])),2,'.',''),
            ],
            [
                'title' => 'Cash Ratio',
                'identifier' => 'cash_ratio_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['BS']['cashAndShortTermInvestments']) , ((float)@$this->financialRatiosQ['BS']['totalCurrentLiabilities'])),2,'.',''),
            ],
            [
                'title' => 'Defensive Interval Ratio',
                'identifier' => 'defensive_interval_ratio_ratios_alert',
                'value' => number_format((float)$this->division3((((float)@$this->financialRatiosQ['BS']['cashAndShortTermInvestments'])+((float)@$this->financialRatiosQ['BS']['netReceivables'])),(((float)@$this->financialRatiosQ['IS']['totalOperatingExpenses'])/365)),2,'.',''),
            ],
            [
                'title' => 'Cash Conversion Cycle',
                'identifier' => 'cash_conversion_cycle_ratios_alert',
                'value' => number_format((float)(($this->division3(365,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue'] ), (((float)@$this->financialRatiosQ['BSP']['inventory']+(float)@$this->financialRatiosQ['BS']['inventory'])/2))))) + ($this->division3( 365,($this->division1(((float)@$this->financialRatiosQ['IS']['totalRevenue']),((float)@$this->financialRatiosQ['BSP']['netReceivables']+(float)@$this->financialRatiosQ['BS']['netReceivables'])/2)))) - ($this-> division3(365,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue']),(((float)@$this->financialRatiosQ['BSP']['accountsPayable']+(float)@$this->financialRatiosQ['BS']['accountsPayable'])/2)))))),2,'.',''),
            ]
        ];
        return($table);
    }
        else{
            $table['heading'] = 'Liquidity Ratios';
            $table['rows'] = [ 
            [
                'title' => 'Current Ratio',
                'identifier' => 'current_ratio_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['BS']['totalCurrentAssets']) , ((float)@$this->financialRatiosQ['BS']['totalCurrentLiabilities'])),2,'.',''),    
            ],
            [
                'title' => 'Quick Ratio',
                'identifier' => 'quick_ratio_ratios_alert',
                'value' => number_format((float)$this->division3((((float)@$this->financialRatiosQ['BS']['cashAndShortTermInvestments'])+((float)@$this->financialRatiosQ['BS']['netReceivables'])) , ((float)@$this->financialRatiosQ['BS']['totalCurrentLiabilities'])),2,'.',''),
            ],
            [
                'title' => 'Cash Ratio',
                'identifier' => 'cash_ratio_ratios_alert',
                'value' => number_format((float)$this->division3(((float)@$this->financialRatiosQ['BS']['cashAndShortTermInvestments']) , ((float)@$this->financialRatiosQ['BS']['totalCurrentLiabilities'])),2,'.',''),
            ],
            [
                'title' => 'Defensive Interval Ratio',
                'identifier' => 'defensive_interval_ratio_ratios_alert',
                'value' => number_format((float)$this->division3((((float)@$this->financialRatiosQ['BS']['cashAndShortTermInvestments'])+((float)@$this->financialRatiosQ['BS']['netReceivables'])),(((float)@$this->financialRatiosQ['IS']['totalOperatingExpenses'])/90)),2,'.',''),
            ],
            [
                'title' => 'Cash Conversion Cycle',
                'identifier' => 'cash_conversion_cycle_ratios_alert',
                'value' => number_format((float)(($this->division3(90,($this->division1((float)@$this->financialRatiosQ['IS']['costOfRevenue'],((float)@$this->financialRatiosQ['BSP']['inventory']+(float)@$this->financialRatiosQ['BS']['inventory']/2))))) + ($this->division1(90,($this->division1(((float)@$this->financialRatiosQ['IS']['totalRevenue']),((float)@$this->financialRatiosQ['BSP']['netReceivables']+(float)@$this->financialRatiosQ['BS']['netReceivables']/2))))) - ($this-> division1(90,($this->division1(((float)@$this->financialRatiosQ['IS']['costOfRevenue']),((float)@$this->financialRatiosQ['BSP']['accountsPayable']+(float)@$this->financialRatiosQ['BS']['accountsPayable'])/2))))),2,'.',''),
            ]
        ];
        return($table);
        }
    }

   
    public function division3($numerator, $denominator) {
        // perform division operationSusta
        if($numerator == 0 || $denominator == 0){
            return '0';
        }
        if($numerator == 0.00 || $denominator == 0.00 ){
            return '0';
        }
        else{
            return ((float)$numerator/ (float)$denominator);
        }
        
    }
    
    public function division2($numerator, $denominator) {
        // perform division operation
        if($denominator == 0 || $numerator == 0){
            return '0';
        }
        if($denominator == 0.00 || $numerator == 0.00 ){
            return '0';
        }
        else{
            return ((float)$numerator/(float)$denominator);
        }
        
    }    

    public function division1($numerator, $denominator) {
        // perform division operation
        if($denominator == 0 || $numerator == 0){
            return '0' ;
        }
        if($denominator == 0.00 || $numerator == 0.00 ){
            return '0';
        }
        else{
            return ((float)$numerator/ (float)$denominator);
        }
        
    }

    public function substract($minus1, $minus2) {
        // perform substract operation
        if($minus2 == 0 || $minus1 == 0){
            return '0' ;
        }
        if($minus2 == 0.00 || $minus1 == 0.00 ){
            return '0';
        }
        else{
            return ((float)$minus1 - (float)$minus2);
        }
        
    }



}
