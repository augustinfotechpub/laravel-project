<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use Livewire\Component;

class ActiveIndustryChart extends Component
{
    public $relatedIndustries, $activeRelatedIndustry, $company, $symbol, $isLoading = true;

    public function mount()
    {
        $this->symbol = session('profile');
    }

    public function render()
    {
        return view('livewire.active-industry-chart');
    }

    public function initComponent()
    {
        $this->company = session('profile');
        self::getRelatedIndustries();
        $this->isLoading = false;
    }

    public function getRelatedIndustries()
    {
        $fmp = new FinancialModelingPrep();
        $this->relatedIndustries = $fmp->getStockScreener($this->company['sector'], $this->company['industry'], 100);
        $this->activeRelatedIndustry = $this->relatedIndustries[0]['symbol'];
        self::getChart($this->activeRelatedIndustry);
    }

    public function getChart($symbol)
    {
        $fmp = new FinancialModelingPrep();
        $incomeStatements = array_reverse($fmp->getFinancialStatements($symbol, 100, 'FY'));
        $cashFlows = array_reverse($fmp->getCashFlowStatement($symbol, 100, 'FY'));
        $dataRevenue = [];
        $dataNetIncome = [];
        $dataFreeCashFlow = [];

        foreach ($incomeStatements as $key => $item) {
            $data['labels'][] = $incomeStatements[$key]['date'];
            $dataRevenue[] = $incomeStatements[$key]['revenue'] ?? (0.0);
            $dataNetIncome[] = $incomeStatements[$key]['netIncome'] ?? (0.0);
            $dataFreeCashFlow[] = $cashFlows[$key]['freeCashFlow'] ?? (0.0);
        }
        $data['datasets'][] = ['data' => $dataRevenue, 'label' => 'Revenue', 'borderColor' => getHaxColor()];
        $data['datasets'][] = ['data' => $dataNetIncome, 'label' => 'Net Income', 'borderColor' => getHaxColor()];
        $data['datasets'][] = ['data' => $dataFreeCashFlow, 'label' => 'Free Cash Flow', 'borderColor' => getHaxColor()];
        $this->dispatchBrowserEvent('activeIndustryChart', $data);
        $this->activeRelatedIndustry = $symbol;
    }
}
