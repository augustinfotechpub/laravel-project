<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use Carbon\Carbon;
use Livewire\Component;

class Calender extends Component
{
    public $symbol, $calendars = [], $calendarsSetting, $initSection = false, $industryInformations, $period = "today", $fromDate, $toDate;

    public function mount()
    {
        $this->calendarsSetting = [
            'initEconomicCalender' => false,
            'initStockDividendCalendar' => false,
            'initStockSplitCalendar' => false,
            'initGetIPOCalendar' => false,
            'initCompanyHistoricalEarningCalendar' => false,
            'initEarningCalendar' => false,
            'initIndustryInformation' => false,
        ];
        $this->fromDate = now()->subDays(5)->format('Y-m-d');
        $this->toDate = now()->format('Y-m-d');
    }

    public function render()
    {
        return view('livewire.calender');
    }

    public function getData()
    {
        $this->initSection = true;
    }

    public function getEconomicCalendar()
    {
        $fmp = new FinancialModelingPrep();
        $this->calendars['economicCalender'] = $fmp->getEconomicCalender();
        $this->calendarsSetting['initEconomicCalender'] = true;
    }

    public function getStockDividendCalendar()
    {
        $fmp = new FinancialModelingPrep();
        $this->calendars['stockDividendCalendar'] = $fmp->getStockDividendCalendar();
        $this->calendarsSetting['initStockDividendCalendar'] = true;

    }

    public function getStockSplitCalendar()
    {
        $fmp = new FinancialModelingPrep();
        $this->calendars['stockSplitCalendar'] = $fmp->getStockSplitCalendar();
        $this->calendarsSetting['initStockSplitCalendar'] = true;

    }

    public function getIPOCalendar()
    {
        $fmp = new FinancialModelingPrep();
        $this->calendars['getIPOCalendar'] = $fmp->getIPOCalendar();
        $this->calendarsSetting['initGetIPOCalendar'] = true;
    }

    public function getCompanyHistoricalEarningCalendar()
    {
        $fmp = new FinancialModelingPrep();
        $this->calendars['companyHistoricalEarningCalendar'] = $fmp->getCompanyHistoricalEarningCalendar($this->symbol);
        $this->calendarsSetting['initCompanyHistoricalEarningCalendar'] = true;

    }

    public function getEarningCalendar()
    {
        $fmp = new FinancialModelingPrep();
        $earnings = $fmp->getEarningCalendar($this->fromDate, $this->toDate);
        $this->calendars['earningCalendar'] = $earnings;
        $this->calendarsSetting['initEarningCalendar'] = true;

    }

    public function getIndustryInformation()
    {/**/

        $fmp = new FinancialModelingPrep();
        $profile = session('profile');
        $this->industryInformations = $fmp->getStockScreener($profile['sector'], $profile['industry']);
        $this->calendarsSetting['initIndustryInformation'] = true;
    }

    public function changePeriod($period)
    {
        $this->period = $period;
        self::getData();
    }


}
