<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Livewire\Component;
use App\Models\DefinitionItem;

use App\Models\Setting;
use App\Models\User;
use App\Repositories\BlockUserRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class AppMain extends Component
{
    public $symbol, $region, $holders, $initCompanyProfile = false, $attempt = 0, $loadSection1 = false, $initAdvancedChart = false,
        $key_executives,$profile, $alertContent, $definitionContent, $conversationId, $users, $enableGroupSetting, $membersCanAddGroup, $myContactIds, $blockUserIds, $blockedByMeUserIds, $notification_sound, $showNetwork = false,$alertContent1, $definitionContent1;

    public function mount(Request $request)
    {   
        
        if (Auth::check()) {
            $this->showNetwork = true;
        }


        $DefinitionItem = new DefinitionItem();

        $alertData = $DefinitionItem->query()->where(['type' => 'alert', 'identifier' => "profile_alert"])->get()->toArray();
        $definitionData = $DefinitionItem->query()->where(['type' => 'definition', 'identifier' => "profile_alert"])->first();

        $alertData1 = $DefinitionItem->query()->where(['type' => 'alert', 'identifier' => "network_alert"])->get()->toArray();
        $definitionData1 = $DefinitionItem->query()->where(['type' => 'definition', 'identifier' => "network_alert"])->first();

        $this->alertContent = @$alertData;
        $this->definitionContent = @$definitionData->content;
        $this->alertContent1 = @$alertData1;
        $this->definitionContent1 = @$definitionData1->content;
        $this->profile = session('profile');
        $this->symbol = $this->profile['symbol'];
    }

    public function render()
    {
        return view('livewire.app-main');
    }

    public function getAdvancedChart()
    {
        $this->dispatchBrowserEvent('initTradingViewChart', ['profile' => $this->profile]);
        $this->initAdvancedChart = true;
    }

    public function initCompanyProfile()
    {
        $fmp = new FinancialModelingPrep();
        $this->key_executives = $fmp->getKeyExecutives($this->symbol);
        $this->dispatchBrowserEvent('initTradingViewChart', ['profile' => $this->profile]);
        $this->initCompanyProfile = true;
        $this->loadSection1 = true;
    }

    public function getOwners()
    {
        $this->dispatchBrowserEvent('profile', 'get Owners task started');
        $profile = $this->profile;
        $secService = new SecEdgarService();
        $owners = $secService->getOwnerships($this->profile['cik']);
        $profile['ownerships'] = $owners;
        session(['profile' => $profile]);
        $this->dispatchBrowserEvent('profile', $this->profile);
    }
}
