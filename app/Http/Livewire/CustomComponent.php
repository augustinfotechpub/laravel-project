<?php


namespace App\Http\Livewire;


use App\Models\Definition;
use Livewire\Component;

abstract class CustomComponent extends Component
{
    public $tooltips = [];
    /**
     * 101 = loading
     * 102 = error
     * 102 = not found
     * 104 = exception
     * 200 = success
     */
    public $loadStatus = 101;

    public function alertTooltip($identifier)
    {

        $definition = Definition::query()->whereHas('items',function($q){
            return $q->where('type','=','alert');
        })->where('identifier',$identifier)->first();

        $this->dispatchBrowserEvent('alertTooltip', [
            'id' => $identifier,
            'content' => $definition->items[0]->content??'Data Not Found'
        ]);
    }
}
