<?php

namespace App\Http\Livewire;

use App\Services\FinancialModelingPrep;
use Illuminate\Support\Str;
use Livewire\Component;

class Charting extends Component
{
    public $symbol,$containerFluid = true, $initCharting = false, $statements, $charts = [], $chartsData = [], $period = 'quarter', $modalChartData = [];

    public function render()
    {
        return view('livewire.charting');
    }

    public function initCharting()
    {
        /*$this->initCharting = true;
        self::getDividendsAndShares();
        self::getEstimates();
        self::getIncomeStatement();
        self::getBalanceSheet();
        self::getCashFlowStatement();
        self::getCurrentValuation();
        self::getLiquidityAndSolvency();
        self::getEmployeeCountMetrics();
        self::getAdvancedMetrics();*/
        $this->dispatchBrowserEvent('chartingChartEvent', $this->symbol);
    }

    public function getStatementItem($title, $keyName, $balanceSheet, $chart = false, $numberFormat = '$')
    {
        $rows = [];
        $chartKey = Str::random();
        $chartData = [];
        foreach ($balanceSheet as $key => $balanceSheetItem) {
            if ($key == 0) {
                $rows[] = $title;
                if ($chart) {
                    $rows[] = $chart ? "<button class='btn switcher switcher-item-for-charting-chart btn-sm px-1 py-0 text-info' data-chart-id='" . $chartKey . "' data-bs-toggle=\"modal\" data-bs-target=\"#chartModal\"><i class='fa fa-chart-line'></i></button>" : null;
                }
            }
            if ($numberFormat === '%') {
                $rows[] = number_format($balanceSheetItem[$keyName] ? $balanceSheetItem[$keyName] : '0.0', 2) . ' %';
            } elseif ($numberFormat === 'number') {
                $rows[] = number_format($balanceSheetItem[$keyName] ? $balanceSheetItem[$keyName] : '0.0', 2);
            } else {
                $rows[] = currencyInMillion($balanceSheetItem[$keyName] ? $balanceSheetItem[$keyName] : '0.0');
            }

            if ($chart) {
                $chartData[] = [
                    'time' => $balanceSheetItem['date'],
                    'value' => $balanceSheetItem[$keyName] ? $balanceSheetItem[$keyName] : '0.0'
                ];
            }
        }
        if ($chart) {
            $this->chartsData[$chartKey] = array_reverse($chartData);
        }
        return $rows;
    }

    public function getDividendsAndShares()
    {
        $fmp = new FinancialModelingPrep();
        $companyQuote = $fmp->quote($this->symbol);
        $historicalPriceData = $fmp->getDailyHistoricalPriceFull($this->symbol);
        $keyMetrics = $fmp->getKeyMetrics($this->symbol, 1, 'ttm');
        $historicalStockSplit = $fmp->getDailyHistoricalStockSplit($this->symbol);

        $this->charts['dividendsAndShares'] = [
            'Shares Outstanding (million)' => currencyInMillion($companyQuote[0]['sharesOutstanding']),
            'Dividend' => currencyFormat($historicalPriceData['historical'][0]['dividend'] ?? null),
            'Dividend Yield' => number_format($keyMetrics[0]['dividendYieldPercentageTTM'], 2) . '%',
            'Latest Dividend Pay Date' => formatDate($historicalPriceData['historical'][0]['date']),
            'Last Split Factor' => $historicalStockSplit['historical'][0]['numerator'],
            'Last Split Date' => formatDate($historicalStockSplit['historical'][0]['date']),
        ];
    }

    public function getEstimates()
    {
        $fmp = new FinancialModelingPrep();
        $financialStatements = $fmp->getFinancialStatements($this->symbol, 15, $this->period);
        $companyQuote = $fmp->quote($this->symbol);
        $keyMetrics = $fmp->getKeyMetrics($this->symbol, 1, 'ttm');
        if ($this->period === 'quarter' || $this->period === 'FY') {
            $dates = array_column($financialStatements, 'date');
            $years = [];
            foreach ($dates as $date) {
                $years[] = formatDate($date);
            }
            $table['header'] = true;
            $table['period'] = $this->period;
            $table['dates'] = $years;
            $table['items'][] = self::getStatementItem('Revenue Estimates', 'costOfRevenue', $financialStatements);
            $this->charts['estimates'] = $table;
        } else {
            $table['header'] = false;
            $table['period'] = $this->period;
            $table['items'][] = ['PE Ratio (TTM)', currencyFormat($keyMetrics[0]['peRatioTTM'])];
            $table['items'][] = ['EPS Estimates for Current Quarter', $companyQuote[0]['eps']];
            $this->charts['estimates'] = $table;
        }
    }

    public function getIncomeStatement()
    {
        $fmp = new FinancialModelingPrep();
        $financialStatements = $fmp->getFinancialStatements($this->symbol, 15, $this->period);

        $dates = array_column($financialStatements, 'date');
        $years = [];
        foreach ($dates as $date) {
            $years[] = formatDate($date);
        }

        $table['header'] = "Income Statement";
        $table['period'] = $this->period;
        $table['dates'] = $years;
        $table['thead_prepend'] = array_merge(['Title', 'Chart'], $table['dates']);
        $table['items'][] = self::getStatementItem('Revenue', 'revenue', $financialStatements, true);
        $table['items'][] = self::getStatementItem('EPS Diluted', 'epsdiluted', $financialStatements, true, 'number');
        $table['items'][] = self::getStatementItem('Net Income', 'netIncome', $financialStatements, true);
        $table['items'][] = self::getStatementItem('EBITDA', 'ebitda', $financialStatements, true);
        $this->charts['incomeStatement'] = $table;
    }

    public function getBalanceSheet()
    {
        $fmp = new FinancialModelingPrep();
        $balanceSheet = $fmp->getBalanceSheet($this->symbol, 15, $this->period);

        $dates = array_column($balanceSheet, 'date');
        $years = [];
        foreach ($dates as $index => $date) {
            $years[] = formatDate($date);
        }
        $table['header'] = "Balance Sheet";
        $table['period'] = $this->period;
        $table['dates'] = $years;
        $table['thead_prepend'] = array_merge(['Title', 'Chart'], $table['dates']);

        $table['items'][] = self::getStatementItem('Total Assets', 'totalCurrentAssets', $balanceSheet, true);
        $table['items'][] = self::getStatementItem('Cash and Short Term Investments', 'totalInvestments', $balanceSheet, true);
        $table['items'][] = self::getStatementItem('Total Liabilities', 'totalCurrentLiabilities', $balanceSheet, true);
        $table['items'][] = self::getStatementItem('Total Long Term Debt', 'longTermDebt', $balanceSheet, true);
        $table['items'][] = self::getStatementItem('Shareholders Equity', 'totalStockholdersEquity', $balanceSheet, true);
        $this->charts['balanceSheet'] = $table;
    }

    public function getCashFlowStatement()
    {
        $fmp = new FinancialModelingPrep();
        $cashFlowQStatements = $fmp->getCashFlowStatement($this->symbol, 15, $this->period);
        $dates = array_column($cashFlowQStatements, 'date');
        $years = [];
        foreach ($dates as $date) {
            $years[] = formatDate($date);
        }
        $table['header'] = "Cash Flow Statement";
        $table['period'] = $this->period;
        $table['dates'] = $years;
        $table['thead_prepend'] = array_merge(['Title', 'Chart'], $table['dates']);


        $table['items'][] = self::getStatementItem('Cash from Financing', 'netCashUsedProvidedByFinancingActivities', $cashFlowQStatements, true);
        $table['items'][] = self::getStatementItem('Cash from Investing', 'netCashUsedForInvestingActivites', $cashFlowQStatements, true);
        $table['items'][] = self::getStatementItem('Cash from Operations', 'netCashProvidedByOperatingActivities', $cashFlowQStatements, true);
        $table['items'][] = self::getStatementItem('Capital Expenditures', 'capitalExpenditure', $cashFlowQStatements, true);
        $this->charts['cashFlowStatement'] = $table;
    }

    public function getCurrentValuation()
    {
        $fmp = new FinancialModelingPrep();
        $CompanyEnterpriseValue = $fmp->getCompanyEnterpriseValue($this->symbol, 15, $this->period);
        $keyMetrics = $fmp->getKeyMetrics($this->symbol, 15, $this->period);
        $financialRatios = $fmp->getFinancialRatios($this->symbol, 15, $this->period);
        $financialStatements = $fmp->getFinancialStatements($this->symbol, 15, $this->period);

        if ($this->period === 'quarter' || $this->period === 'FY') {
            $dates = array_column($CompanyEnterpriseValue, 'date');
            $years = [];
            foreach ($dates as $date) {
                $years[] = formatDate($date);
            }
            $table['header'] = true;
            $table['period'] = $this->period;
            $table['dates'] = $years;
            $table['thead_prepend'] = array_merge(['Title', 'Chart'], $table['dates']);


            $table['items'][] = self::getStatementItem('Market Cap', 'marketCapitalization', $CompanyEnterpriseValue, true);
            $table['items'][] = self::getStatementItem('Enterprise Value', 'enterpriseValue', $CompanyEnterpriseValue, true);
            $table['items'][] = self::getStatementItem('PE Ratio', 'peRatio', $keyMetrics, true, 'number');
            $table['items'][] = self::getStatementItem('Current Ratio', 'currentRatio', $financialRatios, true, 'number');
            $table['items'][] = self::getStatementItem('PS Ratio', 'priceToSalesRatio', $financialRatios, true, 'number');
            $table['items'][] = self::getStatementItem('Price Book Value', 'priceBookValueRatio', $financialRatios, true, 'number');
            $table['items'][] = self::getStatementItem('Earning Yield', 'earningsYield', $keyMetrics, true, '%');
            $table['items'][] = self::getStatementItem('EBITDA', 'ebitda', $financialStatements, true,);
            $table['items'][] = self::getStatementItem('EBITDA Ratio', 'ebitdaratio', $financialStatements, true, '%');
            $this->charts['currentValuation'] = $table;
        } else {
            $table['header'] = false;
            $table['period'] = $this->period;
            $table['items'][] = ['PE Ratio (TTM)', currencyFormat($keyMetrics[0]['peRatioTTM'])];
            $table['items'][] = ['PEG Ratio (TTM)', currencyFormat($financialRatios[0]['pegRatioTTM'])];
            $table['items'][] = ['Earning Yield (TTM)', currencyFormat($keyMetrics[0]['earningsYieldTTM'])];
            $table['items'][] = ['PS Ratio (TTM)', currencyFormat($financialRatios[0]['priceToSalesRatioTTM'])];
            $table['items'][] = ['Price Book Value (TTM)', currencyFormat($financialRatios[0]['priceBookValueRatioTTM'])];
            $this->charts['currentValuation'] = $table;
        }

    }

    public function getLiquidityAndSolvency()
    {
        $fmp = new FinancialModelingPrep();
        $financialRatios = $fmp->getFinancialRatios($this->symbol, 15, $this->period);
        $keyMetrics = $fmp->getKeyMetrics($this->symbol, 15, $this->period);
        $cashFlowStatements = $fmp->getCashFlowStatement($this->symbol, 15, $this->period);

        if ($this->period === 'quarter' || $this->period === 'FY') {
            $dates = array_column($financialRatios, 'date');
            $years = [];
            foreach ($dates as $date) {
                $years[] = formatDate($date);
            }
            $table['header'] = true;
            $table['period'] = $this->period;
            $table['dates'] = $years;
            $table['thead_prepend'] = array_merge(['Title', 'Chart'], $table['dates']);

            $table['items'][] = self::getStatementItem('Current Ratio', 'currentRatio', $financialRatios, true, 'number');
            $table['items'][] = self::getStatementItem('Debt to Equity', 'debtToEquity', $keyMetrics, true, 'number');
            $table['items'][] = self::getStatementItem('Free Cash Flow', 'freeCashFlow', $cashFlowStatements, true);
            $this->charts['liquidityAndSolvency'] = $table;
        } else {
            $table['header'] = false;
            $table['period'] = $this->period;
            $table['items'][] = ['Current Ratio (TTM)', currencyFormat($financialRatios[0]['currentRatioTTM'])];
            $table['items'][] = ['Debt to Equity (TTM)', currencyFormat($keyMetrics[0]['debtToEquityTTM'])];
            $this->charts['liquidityAndSolvency'] = $table;

        }
    }

    public function getEmployeeCountMetrics()
    {
        $fmp = new FinancialModelingPrep();
        $profile = $fmp->getProfile($this->symbol);

        $this->charts['employeeCountMetrics'] = [
            'Total Employees (Annual)' => number_format($profile[0]['fullTimeEmployees']),
        ];
    }

    public function getAdvancedMetrics()
    {
        $fmp = new FinancialModelingPrep();
        $companyQuote = $fmp->quote($this->symbol);
        $this->charts['advancedMetrics'] = [
            'Total Employees (Annual)' => number_format($companyQuote[0]['marketCap']),
        ];
    }

    public function getChart($chartName)
    {
        $data = explode(',', $chartName);
        $chartName = $data[0];
        $index = $data[1];
        $fmp = new FinancialModelingPrep();
        $chartData = $fmp->getChartData($this->symbol, $chartName, $index, 15, $this->period);
        $this->modalChartData = $chartData;
        $this->dispatchBrowserEvent('modalChart', [
            'name=' => 'Total Assets Chart Statistics',
            'symbol' => $this->symbol,
            'chartData' => $chartData]);
    }

    public function changePeriod($period)
    {
        $this->period = $period;
        self::initCharting();
    }
}
