<?php

namespace App\Http\Livewire\Financials;

use App\Services\FinancialModelingPrep;
use App\Services\SecEdgarService;
use Illuminate\Support\Facades\Log;
use JetBrains\PhpStorm\NoReturn;
use Livewire\Component;
use Symfony\Component\DomCrawler\Crawler;

class KeyStats extends Component
{
    protected $fmp, $secEdgarService, $directory;

    public $reports = [], $period, $symbol, $status = 'false', $profile;

    public function mount()
    {
        $this->profile = session('profile');
        $this->period = 'FY';
        $this->symbol = $this->profile['symbol'];
    }

    public function render()
    {
        return view('livewire.financials.key-stats');
    }

    public function getReportsx()
    {
        $this->fmp = new FinancialModelingPrep();
        $this->secEdgarService = new SecEdgarService();
        $this->directory = $this->fmp->get10kqDirectory($this->symbol, $this->period);
        if (count($this->directory) == 0) {
            $this->status = 'empty';
            return;
        }
        $metaLinksReportsArray = $this->secEdgarService->get10KQReportsMetaLinks($this->directory);
        $searchStrings = ['segment', 'geographic', 'product', 'services'];
        collect($metaLinksReportsArray)->filter(function ($item, $key) use ($searchStrings) {
            $shortName = strtolower($item['shortName']);
            foreach ($searchStrings as $string) {
                if (strpos($shortName, $string) !== false) {
                    $url = $this->directory['directory'] . $key . '.htm';
                    $rowHtml = $this->secEdgarService->httpRequest($url);
                    $client = new Crawler($rowHtml);
                    $this->reports[] = [
                        'title' => $item['shortName'],
                        'html' => $client->filter('.report')->outerHtml()
                    ];
                }
            }
        });
        $this->reports = collect($this->reports)->unique();
        $this->status = 'true';
    }

    public function getReports()
    {
        $this->secEdgarService = new SecEdgarService();
        $searchStrings = ['segment', 'geographic', 'product', 'services','Management’s Discussion and Analysis'];
        $formType = $this->period == 'quarter' ? '10-Q' : '10-K';
        $this->reports = $this->secEdgarService->getReports($this->profile['cik'], $searchStrings, $formType);
        if (empty($this->reports)) {
            $this->reports = $this->secEdgarService->getReports($this->profile['cik'], $searchStrings, '20-F');
            if(empty($this->reports))
                $this->reports = $this->secEdgarService->getReports($this->profile['cik'], $searchStrings, '40-F');
        }
        if (empty($this->reports)) {
            $this->status = 'empty';
            return;
        }
        $this->reports = collect($this->reports)->unique();
        $this->status = 'true';
    }

    public function changePeriod($period)
    {
        $this->period = $period;
        $this->getReports();
    }
}
