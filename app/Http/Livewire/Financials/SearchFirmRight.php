<?php

namespace App\Http\Livewire\Financials;

use App\Repositories\Admin\CompanyRepository;
use Livewire\Component;

class SearchFirmRight extends Component
{
    public $query, $bestMatches, $highlightIndex, $content_component, $profile;

    public function mount()
    {
        $this->profile = session('profile');
        $this->resetEngine();
    }

    public function render()
    {
        return view('livewire.financials.search-firm-right');
    }


    public function resetEngine()
    {
        $this->query = '';
        $this->bestMatches = [];
        $this->highlightIndex = 0;
    }

    public function incrementHighlight()
    {
        if ($this->highlightIndex === count($this->bestMatches) - 1) {
            $this->highlightIndex = 0;
            return;
        }
        $this->highlightIndex++;
    }

    public function decrementHighlight()
    {
        if ($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->bestMatches) - 1;
            return;
        }
        $this->highlightIndex--;
    }

    public function addCompany($companyCik,$ticker)
    {     
        $this->emit('addCompanyForCompare',$companyCik,$ticker);
    }

    public function updatedQuery()
    {
        $companyRepository = new CompanyRepository();
        $this->bestMatches = $companyRepository->searchCompanyright($this->profile, $this->query);
        
    }

    public function getSicFillings($cik)
    {
        session(['sec-filing-cik' => $cik]);
        $this->emit('changeContentComponent', ['content_component' => 'sec-fillings']);
    }
}
