<?php

namespace App\Http\Livewire\Financials;

use App\Http\Livewire\CustomComponent;
use App\Services\FinanceService;
use Livewire\Component;

class Financials extends Component
{

    public $symbol, $initFinancials = false, $statements, $statement, $limit = 10,
        $currentStatement = 'income-statement', $final_link, $period = 'quarter',
        $comparableStatement, $comparableCompanies, $profile, $loadContent = false, $alertContent = [],$showReverse= "false",$epsttm;

    public function mount()
    {
        $this->profile = session('profile');
        $this->symbol = $this->profile['symbol'];
    }

    public function render()
    {
        return view('livewire.financials.financials');
    }

    public function initFinancials()
    {   
        $financeService = new FinanceService($this->profile, $this->period, $this->limit);
        $this->statement = $financeService->getEodIncomeStatement();
        $this->epsttm = $this->statement['epsttm'];
        $this->initFinancials = true;
    }

    public function changeStatement($statement, $period = 'FY')
    {
        $this->period = $period;
        $financeService = new FinanceService($this->profile, $this->period, $this->limit);
        $this->currentStatement = $statement;
        $this->reset('statement');
        $this->emit('changeStatement', $statement);
        switch ($statement) {
            case 'comprehensive-income':
                $this->statement = $financeService->getComprehensiveIncomeStatement();
                break;
            case 'shareholders-equity':
                $this->statement = $financeService->getShareholdersEquityStatements();
                break;
            case 'income-statement':
                // $this->statement = $financeService->getIncomeStatement();
                // $this->alertContent = $financeService->getFinAlertsContent();
                $this->statement = $financeService->getEodIncomeStatement(true,false,$this->showReverse);
                $this->epsttm = $this->statement['epsttm'];
                // $this->statement = $financeService->getIncomeStatementFromEdgar();
                break;
            case 'balance-sheet':
                // $this->statement = $financeService->getBalanceSheet();
                $this->statement = $financeService->eodGetBalanceSheet(false,$this->showReverse);
                break;
            case 'cash-flow':
                // $this->statement = $financeService->getCashFlowStatement();
                $this->statement = $financeService->eodGetCashFlowStatement(true,false,$this->showReverse);
                break;
            case 'standard-size-compare':
                $this->emit('addCompanyForCompare', $this->profile['cik'], $this->profile['symbol'], $statement,'horizontal');
                break;
            case 'common-size-compare':
                // Inter firm
                $this->currentStatement = 'common-size-compare';                
                $this->emit('addCompanyForCompare', $this->profile['cik'], $this->profile['symbol'], 'common-size-compare','vertical');
                break;
            case 'common-size-compare-vertical':
                // Vertical
                $this->currentStatement = 'common-size-compare-vertical';
                $this->emit('addCompanyForCompare', $this->profile['cik'], $this->profile['symbol'], 'common-size-compare','ver');
                break;
            case 'common-size-compare-horizontal':
                // Intra firm
                $this->currentStatement = 'common-size-compare-horizontal';
                $this->emit('addCompanyForCompare', $this->profile['cik'], $this->profile['symbol'], 'common-size-compare', 'horizontal');
                break;
        }
        $this->loadContent = false;
    }

    public function changeView($reverse,$currentS,$per)
    {
        $this->showReverse = $reverse;
        $this->changeStatement($currentS,$per);       
    }
}
