<?php

namespace App\Http\Livewire\Financials\KeyStats;

use App\Http\Livewire\CustomComponent;
use App\Models\CashFlowStatement;
use App\Models\eod_cash_flow;
use App\Models\IncomeStatement;
use App\Services\FinancialModelingPrep;
use App\Traits\FinancialsTrait;
use Livewire\Component;

class CF extends Component
{
    use FinancialsTrait;

    public $chartData = [], $summary, $symbol, $period = 'FY',$initSummary=false,$loadStatus, $year;

    public function mount()
    {
        $this->symbol = session('profile')['symbol'];
    }

    public function render()
    {
        return view('livewire.financials.key-stats.c-f');
    }

    protected function getTimeSeries($array, $index = 'date'): array
    {
        return array_column($array, $index);
    }

    public function initSummary($period = 'yearly')
    {
        try {
            $this->getSummary($period);
            $this->year = eod_cash_flow::selectRaw('substr(date,1,4) as dates')
            ->where('ticker', $this->symbol)
            ->orderBy('dates','desc')
            ->pluck('dates')->unique()->toArray();
            $this->loadStatus = 200;
        } catch (\Exception $e) {
            $this->loadStatus = 104;
        }
    }

    public function getSummary($period = 'yearly')
    {
        $this->period = $period;
        $this->summary = eod_cash_flow::query()->where('ticker', $this->symbol);
        if (in_array($period, ['quarter'])) {
            $this->summary->whereIn('period_type', ['quarterly']);
        } else {
            $this->summary->where('period_type', 'yearly');
        }
        $this->summary = $this->summary->get()->toArray();
        $time = $this->getTimeSeries($this->summary, 'date');
        $ignorableIndexes = [0, 1, 2, 3, 4, 5, 6, 7,38,39,40,41];
        $dataSerieses = $this->getDataSerieses($this->summary, $ignorableIndexes);

        $intervals = $this->getIntervals($this->summary[0], $ignorableIndexes);
        $this->chartData['cf'] = [
            'time' => $time,
            'dataSerieses' => $dataSerieses,
            'intervals' => $intervals
        ];
    }

    public function getChartData($chart)
    {
        $this->dispatchBrowserEvent('cashFlowChart', ['data' => $this->chartData['cf']]);
    }

    public function changePeriod($chart, $period)
    {
        $this->getSummary($period);
    }
}
