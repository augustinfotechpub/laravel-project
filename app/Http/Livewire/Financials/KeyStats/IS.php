<?php

namespace App\Http\Livewire\Financials\KeyStats;

use App\Models\IncomeStatement;
use App\Models\eod_income_statement;
use App\Traits\FinancialsTrait;
use Livewire\Component;

class IS extends Component
{
    use FinancialsTrait;

    public $initSummary = false, $chartData = [], $summary, $period = 'FY', $symbol, $year;

    public function mount()
    {
        $this->symbol = session('profile')['symbol'];
    }

    public function render()
    {
        return view('livewire.financials.key-stats.i-s');
    }

    public function initSummary($period = 'yearly')
    {
        $this->getSummary($period);
        $this->year = eod_income_statement::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $this->symbol)
        ->where('period_type', 'yearly')
        ->orderBy('dates','desc')
        ->pluck('dates')->unique()->toArray();
        $this->initSummary = true;
    }

    public function getSummary($period = 'yearly')
    {
        $this->period = $period;
        $this->summary = eod_income_statement::query()->where('ticker', $this->symbol);
        if (in_array($period, ['quarter'])) {
            $this->summary->whereIn('period_type', ['quarterly']);
        } else {
            $this->summary->where('period_type', 'yearly');
        }
        $this->summary = $this->summary->get()->toArray();

        $time = $this->getTimeSeries($this->summary, 'date');
        $ignorableIndexes = [0, 1, 2, 3, 4, 5, 6, 7, 34, 35, 36, 37];
        $dataSerieses = $this->getDataSerieses($this->summary, $ignorableIndexes);
        $intervals = isset($this->summary[0]) ? $this->getIntervals($this->summary[0], $ignorableIndexes) : [];
        $this->chartData['isChart'] = [
            'time' => $time,
            'dataSerieses' => $dataSerieses,
            'intervals' => $intervals
        ];
    }

    public function changePeriod($chart, $period)
    {
        $this->getSummary($period);
    }

    public function getChartData($chart)
    {
        $this->dispatchBrowserEvent('incomeStatementChart', ['data' => $this->chartData['isChart']]);
    }
}
