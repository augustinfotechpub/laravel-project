<?php

namespace App\Http\Livewire\Financials\KeyStats;

use App\Models\BalanceSheetStatement;
use App\Models\eod_balance_sheet;
use App\Models\IncomeStatement;
use App\Services\FinancialModelingPrep;
use App\Traits\FinancialsTrait;
use Livewire\Component;
use function PHPUnit\Framework\isEmpty;

class BS extends Component
{
    use FinancialsTrait;

    public $chartData = [], $summary, $initSummary = false, $period = 'FY', $symbol, $year;

    public function mount()
    {
        $this->symbol = session('profile')['symbol'];
    }

    public function render()
    {
        return view('livewire.financials.key-stats.b-s');
    }

    public function initSummary($period = 'yearly')
    {
        $this->getSummary($period);
        $this->year = eod_balance_sheet::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $this->symbol)
        ->orderBy('dates','desc')
        ->pluck('dates')->unique()->toArray();
        $this->initSummary = true;
    }

    public function getSummary($period = 'yearly')
    {
        $this->period = $period;
        $this->summary = eod_balance_sheet::query()->where('ticker', $this->symbol);
        if (in_array($period, ['quarter'])) {
            $this->summary->whereIn('period_type', ['quarterly']);
        } else {
            $this->summary->where('period_type', 'yearly');
        }
        $this->summary = $this->summary->get()->toArray();
        if (empty($this->summary))
            return;
        $time = $this->getTimeSeries($this->summary, 'date');
        $ignorableIndexes = [0, 1, 2, 3, 4, 5, 6, 7, 47, 48, 49, 50];
        $dataSerieses = $this->getDataSerieses($this->summary, $ignorableIndexes);
        $intervals = $this->getIntervals($this->summary[0], $ignorableIndexes);
        $this->chartData['bs'] = [
            'time' => $time,
            'dataSerieses' => $dataSerieses,
            'intervals' => $intervals
        ];
    }

    public function getChartData($chart)
    {
        $this->dispatchBrowserEvent('balanceSheetChart', ['data' => $this->chartData['bs']]);
    }

    public function changePeriod($chart, $period)
    {
        $this->getSummary($period);
    }
}
