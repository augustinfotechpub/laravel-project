<?php

namespace App\Http\Livewire\Financials\Comparables;

use App\Models\BalanceSheetStatement;
use App\Models\CashFlowStatement;
use App\Models\FinancialStatement;
use App\Models\IncomeStatement;
use App\Services\FinanceService;
use App\Services\FinancialModelingPrep;
use Livewire\Component;
use App\Models\eod_balance_sheet;
use App\Models\eod_cash_flow;
use App\Models\eod_income_statement;
use App\Models\DefinitionItem;

use function PHPUnit\Framework\isNull;

class Comparables extends Component
{
    protected $listeners = ['addCompanyForCompare' => 'addCompanyForCompare', 'changeStatement' => 'changeStatement'];

    public $comparableStatementRows, $period = 'FY', $currentType = "horizontal", $currentStatementName = 'balance-sheet',
        $incomeStatementList = null, $comparableCompanies, $headers, $symbols=[], $compareType = 'common', $profile = null,
        $horizontalStatement = null, $limit = 10,$year='2020',$allYearBS=[],$allYearIS=[],$allYearCF=[],$header,$alertsData = [],$alertsDataComp = ['standard_comparable_alert','vertical_comparable_alert','interfirm_comparable_alert','intrafirm_comparable_alert'],$alert_content = [];

    public function mount()
    {
        $this->profile = session('profile');
        foreach ($this->alertsDataComp as $key => $value) {
            $definitionItemData = DefinitionItem::select('content')->where('identifier', $value)->where('type', 'alert')->get()->toArray();
            if( !empty($definitionItemData) ){
                foreach ($definitionItemData as $ke => $val) {
                    $this->alertsData[$value][] = $val['content'];  
                } 
            }
        }
    }

    public function render()
    {
        return view('livewire.financials.comparables.comparables');
    }

    public function changeStatement($statement)
    {
        $this->compareType = $statement;
    }


    public function changeType($type = 'horizontal')
    {
        $this->currentType = $type;
        $this->addCompanyForCompare(null,null,null,$type);
    }

    public function addCompanyForCompare($cik = null, $symbol = null, $compareType = null,$forceStatement='vertical')
    {

        // echo $compareType;
        // echo " ". $symbol;

        if(!is_null($symbol))
        {
            $fetchsymbol=$symbol;

        }
        else{

            $fetchsymbol = array_key_first($this->symbols);
        }

        if( !empty( $this->symbols) && $compareType == 'common-size-compare' ){
            $this->symbols = [array_key_first($this->symbols) => $this->symbols[$fetchsymbol] ];
        }

        if($this->period=='FY')
        {
        $this->allYearBS = eod_balance_sheet::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $fetchsymbol)
        ->where('period_type','yearly')
        ->orderBy('dates','desc')
        ->limit(10)
        ->pluck('dates')->unique()->toArray();

        $this->allYearCF = eod_cash_flow::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $fetchsymbol)
        ->where('period_type','yearly')
        ->orderBy('dates','desc')
        ->limit(10)
        ->pluck('dates')->unique()->toArray();

        $this->allYearIS = eod_income_statement::selectRaw('substr(date,1,4) as dates')
        ->where('ticker', $fetchsymbol)
        ->where('period_type','yearly')
        ->orderBy('dates','desc')
        ->limit(10)
        ->pluck('dates')->unique()->toArray();
        }
        else{

        $this->allYearBS = eod_balance_sheet::selectRaw('date')
        ->where('ticker', $fetchsymbol)
        ->where('period_type','quarterly')
        ->orderBy('date','desc')
        ->limit(46)
        ->pluck('date')->unique()->toArray();

        $this->allYearCF = eod_cash_flow::selectRaw('date')
        ->where('ticker', $fetchsymbol)
        ->where('period_type','quarterly')
        ->orderBy('date','desc')
        ->limit(46)
        ->pluck('date')->unique()->toArray();

        $this->allYearIS = eod_income_statement::selectRaw('date')
        ->where('ticker', $fetchsymbol)
        ->where('period_type','quarterly')
        ->orderBy('date','desc')
        ->limit(46)
        ->pluck('date')->unique()->toArray();

        }
       
        /*if($this->currentType=='vertical')
        {
            $forceStatement='vertical';
        }*/
        $this->currentType = $forceStatement;
        $this->reset('horizontalStatement');
        if (!is_null($compareType)) {
            switch ($compareType) {
                case "standard-size-compare":
                    $this->compareType = "standard";
                    break;
                case "common-size-compare":
                    $this->compareType = "common";
                    break;
            }
        }
        if ($this->currentType == 'horizontal' && $this->compareType == 'common') {
            $financeService = new FinanceService($this->profile, $this->period, $this->limit);
            switch ($this->currentStatementName) {
                case 'income-statement':
                    $this->horizontalStatement = $financeService->getEodIncomeStatement(false, true);
                    break;
                case 'balance-sheet':
                    $this->horizontalStatement = $financeService->eodGetBalanceSheet(true);
                    break;
                case 'cash-flow':
                    $this->incomeStatementList = $financeService->getEodIncomeStatement(false, true);
                    $this->horizontalStatement = $financeService->eodGetCashFlowStatement(false, true);
                    break;
            }
            return;
        }

        if ($cik != null && $symbol != null)
            $this->symbols[$symbol] = $cik;

        if (is_null($this->symbols) || count($this->symbols) == 0) {
            $this->symbols[$this->profile['symbol']] = $this->profile['cik'];
        }

        switch ($this->currentStatementName) {
            
            case 'balance-sheet':
                $this->getBalanceSheetStatement($this->year);
                break;
            case 'income-statement':
                $this->getIncomeStatement($this->year);
                break;
            case 'cash-flow':
                $this->getCashFlowStatement($this->year);
        }
    }

    public function changeCurrentStatementName($statement,$forceStatement='horizontal')
    {
        $this->currentType=$forceStatement;
        $this->currentStatementName = $statement;

        if( $forceStatement == 'ver' ){
            $this->addCompanyForCompare(null,null,'common-size-compare',$forceStatement);    
        } else {
            $this->addCompanyForCompare(null,null,null,$forceStatement);
        }

        
    }

    public function changePeriod($statement, $period,$forceStatement='horizontal')
    {
        $current_year = date('Y')-1;
        $this->currentType=$forceStatement;
        $this->compareType = $statement;
        $this->period = $period;
        $this->year=$current_year;
        $this->addCompanyForCompare(null,null,null,$forceStatement);
    }

    public function change($statement, $period,$year)
    {
        $this->year=$year;
        $this->compareType = $statement;
        $this->period = $period;
        $this->addCompanyForCompare(null,null,null,$this->currentType);
    }

    public function getBalanceSheetStatement($year='2020')
    {
        $records = [];
        $recordsHeader = [];
        $headerColumns = [];
        $alertAllContent = [];
        foreach ($this->symbols as $symbol => $key) {
            $recordsHeader = [];
            if(!is_null($year))
            {
            $record = eod_balance_sheet::where('ticker', $symbol)->where('period_date','like',$year.'%');
            }
            else
            {
            $record = eod_balance_sheet::where('ticker', $symbol);
            }

            if (in_array($this->period, ['Quarter', 'quarter'])) {
                $record->where('period_type', 'quarterly');
            } else {
                $record->where('period_type', 'yearly');
            }
            $record = $record->orderBy('period_date', 'desc')->get()->toArray();
            
            @$records[] = $record[0];
            @$recordsHeader[] = $record[0];
            
            $headerColumns[$symbol] = collect($recordsHeader)->sortBy('date')->pluck('date')->toArray();
        }
        $this->reset('comparableStatementRows');
        $statementRows = [];

        foreach (eod_balance_sheet::TITLES as $titleKeyIndex => $titleKeyValue) {
            $alertIdentifier = DefinitionItem::where('identifier', $titleKeyValue['column']."_".$titleKeyValue['alert_title'])->where('type', 'alert')->get();
            $alertIdentifier = (array)json_decode( json_encode($alertIdentifier) , true);
            
            $alertAllContent[$titleKeyValue['title']] = $alertIdentifier;

            foreach ($records as $recordKey => $record) {
                    if ($recordKey == 0)
                        $statementRows[$titleKeyIndex][] = $titleKeyValue;
                    if ($this->compareType == 'standard') {
                        $statementRows[$titleKeyIndex][] = @$record[$titleKeyValue['column']];
                    } else {
                        if(is_null(@$record['totalAssets']))
                        {
                            @$statementRows[$titleKeyIndex][] = 0;
                        }
                        else{
                        @$statementRows[$titleKeyIndex][] = (@$record[$titleKeyValue['column']] / @$record['totalAssets']) * 100;
                    }
                }
            }
        }
        $this->comparableStatementRows = $statementRows;
        $this->header=$headerColumns;
        $this->alert_content = $alertAllContent;
    }

    public function getIncomeStatement($year=null)
    {
        $records = [];
        $recordsHeader = [];
        $headerColumns = [];
        $alertAllContent = [];
        foreach ($this->symbols as $symbol => $key) {
            $recordsHeader = [];
            if(!is_null($year))
            {
            $record = eod_income_statement::where('ticker', $symbol)->where('period_date','like',$year.'%');
            }
            else
            {
                $record = eod_income_statement::where('ticker', $symbol);
            }
           

            if (in_array($this->period, ['Quarter', 'quarter'])) {
                $record->where('period_type','quarterly');
            } else {
                $record->where('period_type', 'yearly');
            }
            $record = $record->orderBy('period_date', 'desc')->get()->toArray();
            $records[]=@$record[0];
            @$recordsHeader[] = $record[0];
            $headerColumns[$symbol] = collect($recordsHeader)->sortBy('date')->pluck('date')->toArray();
        }
        $this->reset('comparableStatementRows');
        $statementRows = [];
        foreach (eod_income_statement::TITLES as $titleKeyIndex => $titleKeyValue) {

            $alertIdentifier = DefinitionItem::where('identifier', $titleKeyValue['column']."_".$titleKeyValue['alert_title'])->where('type', 'alert')->get();
            $alertIdentifier = (array)json_decode( json_encode($alertIdentifier) , true);
            
            $alertAllContent[$titleKeyValue['title']] = $alertIdentifier;

            foreach ($records as $recordKey => $record) {
                if ($recordKey == 0)
                    $statementRows[$titleKeyIndex][] = $titleKeyValue;
                // Standard or Common
                if(is_null(@$record['totalRevenue']))
                {
                    @$statementRows[$titleKeyIndex][] = ($this->compareType == 'standard') ? @$record[$titleKeyValue['column']] : 0;
                }
                else{
                @$statementRows[$titleKeyIndex][] = ($this->compareType == 'standard') ? @$record[$titleKeyValue['column']] : (@$record[$titleKeyValue['column']] / @$record['totalRevenue']) * 100;
                }
            }
        }
        $this->comparableStatementRows = $statementRows;
        $this->header=$headerColumns;
        $this->alert_content = $alertAllContent;
    }

    public function getCashFlowStatement($year=null)
    {
        $records = [];
        $recordsHeader = [];
        $incomeStatements = [];
        $headerColumns = [];
        $alertAllContent = [];
        foreach ($this->symbols as $symbol => $key) {
            $recordsHeader = [];
            if(!is_null($year))
            {
                $incomeStatement = eod_income_statement::where('ticker', $symbol)->where('period_date','like',$year.'%');
                $record = eod_cash_flow::where('ticker', $symbol)->where('period_date','like',$year.'%');        
            }
            else
            {
                $incomeStatement = eod_income_statement::where('ticker', $symbol);
                $record = eod_cash_flow::where('ticker', $symbol);           
            }
            
            if (in_array($this->period, ['Quarter', 'quarter'])) {
                $incomeStatement->whereIn('period_type', ['quarterly']);
                $record->where('period_type','quarterly');
            } else {
                $incomeStatement->where('period_type', 'yearly');
                $record->where('period_type', 'yearly');
            }
            $incomeStatement = $incomeStatement->orderBy('period_date', 'desc')->get()->toArray();

            $incomeStatement = @$incomeStatement[0];

            // if (is_null($incomeStatement)) {
            //     /*$this->dispatchBrowserEvent('alert',
            //         ['type' => "error", 'message' => "fasdf"]);*/
            //     unset($this->symbols[$symbol]);
            //     return;
            // }

            $incomeStatements[] = $incomeStatement;

            $record = $record->orderBy('period_date', 'desc')->get()->toArray();

            $records[]=@$record[0];
            @$recordsHeader[] = $record[0];
            $headerColumns[$symbol] = collect($recordsHeader)->sortBy('date')->pluck('date')->toArray();

        }
        $this->reset('comparableStatementRows');

        $statementRows = [];
        $cfoa = $cfia = $cffa = 0;
        foreach (eod_cash_flow::TITLES as $titleKeyIndex => $titleKeyValue) {
            $alertIdentifier = DefinitionItem::where('identifier', $titleKeyValue['column']."_".$titleKeyValue['alert_title'])->where('type', 'alert')->get();
            $alertIdentifier = (array)json_decode( json_encode($alertIdentifier) , true);
            
            $alertAllContent[$titleKeyValue['title']] = $alertIdentifier;
            foreach ($records as $recordKey => $record) {
                if (isset($record[$titleKeyValue['column']])) {
                    $record[$titleKeyValue['column']] = str_replace('-','',$record[$titleKeyValue['column']]);
                }
                $revenue = @$incomeStatements[$recordKey]['totalRevenue'];
                if ($recordKey == 0) {
                    $statementRows[$titleKeyIndex][] = $titleKeyValue;
                }
                if ($this->compareType == 'standard') {
                    $statementRows[$titleKeyIndex][] = @$record[$titleKeyValue['column']];
                } else {
                    if (isset($titleKeyValue['revenue'])) {
                        switch ($titleKeyValue['revenue']) {
                            case 'self':
                                $statementRows[$titleKeyIndex][] = 100;
                                if ($titleKeyValue['column'] == 'totalCashFromOperatingActivities') {
                                    $cfoa = $record[$titleKeyValue['column']];
                                } else if ($titleKeyValue['column'] == 'totalCashflowsFromInvestingActivities') {
                                    $cfia = $record[$titleKeyValue['column']];
                                } else if ($titleKeyValue['column'] == 'totalCashFromFinancingActivities') {
                                    $cffa = $record[$titleKeyValue['column']];
                                }
                                continue 2;
                                break;
                            case 'cfoa':
                                $revenue = $cfoa;
                                break;
                            case 'cfia':
                                $revenue = $cfia;
                                break;
                            case 'cffa':
                                $revenue = $cffa;
                                break;
                            default:
                                $revenue = @$incomeStatements[$recordKey]['totalRevenue'];
                                break;
                        }
                    }
                    if(is_null($revenue))
                    {
                        $statementRows[$titleKeyIndex][] = 0;
                    }
                    else
                    {
                        $statementRows[$titleKeyIndex][] = (@$record[$titleKeyValue['column']] / @$revenue) * 100;
                    }
                }
            }
        }
        $this->comparableStatementRows = $statementRows;
        $this->header=$headerColumns;
        $this->alert_content = $alertAllContent;
    }

    public function removeFromCompare($symbol)
    {
        $symbols = $this->symbols;
        unset($symbols[$symbol]);
        $this->symbols = $symbols;
        $this->getBalanceSheetStatement();
    }
}
