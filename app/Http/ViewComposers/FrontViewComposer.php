<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 19-04-2018
 * Time: 01:09 AM
 */

namespace App\Http\ViewComposers;

use App\Models\Definition;
use App\Models\SiteConfiguration;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;

class FrontViewComposer
{
    public function compose(View $view)
    {
        $current_user = auth()->user();
        $configuration = SiteConfiguration::pluck('value', 'identifier');

        $view->with([
            'current_user' => $current_user,
            'configuration' => $configuration,
        ]);
    }
}
