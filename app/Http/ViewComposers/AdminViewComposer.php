<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 19-04-2018
 * Time: 01:09 AM
 */

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class AdminViewComposer
{


    public function compose(View $view)
    {
        $current_user = auth()->guard('admin')->user();
        if (!auth()->guard('admin')->check()) {
            return redirect()->route('admin.login');
        }


        $view->with([
            'current_user' => $current_user,
        ]);
    }
}
