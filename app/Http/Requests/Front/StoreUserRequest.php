<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string|max:30|min:3',
            'last_name'=>'required|string|max:30|min:3',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:6',
            'phone_number'=>'required|numeric|unique:users,phone_number|min:8',
            'address_line1'=>'required',
            'address_line2'=>'nullable',
            'city'=>'required|max:25|min:2',
            'state'=>'required|max:25|min:2',
            'country'=>'required|max:25|min:2',
            'zip_code'=>'required|regex:/[a-zA-Z0-9\s]+/|max:10',
        ];
    }
}
