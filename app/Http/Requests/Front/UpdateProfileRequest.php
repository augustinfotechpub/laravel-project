<?php

namespace App\Http\Requests\Front;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {

        return [
            'first_name'=>'required|string|max:30|min:3',
            'last_name'=>'required|string|max:30|min:3',
            'phone_number'=>'required|numeric|unique:users,phone_number|min:8',
            'zip_code'=>'required|regex:/[a-zA-Z0-9\s]+/|max:10',
            'city'=>'required|max:25|min:2',
            'state'=>'required|max:25|min:2',
            'country'=>'required|max:25|min:2',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $user = User::where(['phone_number' => cleanString($this->phone_number)])->where('id', '!=', $this->user_id)->first();
            if (!is_null($user)) {
                $validator->errors()->add('phone_number', 'This phone number is already exists.');
            }
        });
    }
}
