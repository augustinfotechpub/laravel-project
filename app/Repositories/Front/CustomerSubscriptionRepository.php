<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Front;

use App\Interfaces\Front\CustomerSubscriptionInterface;
use App\Models\CustomerSubscription;
use App\Models\SubscriptionPlan;
use App\Models\User;
use Cassandra\Custom;
use Illuminate\Support\Carbon;
use Stripe\Stripe;
use Stripe\Subscription;
use Illuminate\Support\Facades\DB;

class CustomerSubscriptionRepository implements CustomerSubscriptionInterface
{
    public function store($requestData)
    {

        return CustomerSubscription::create($requestData);
    }

    public function getCustomerSubscription($user_id)
    {
        return DB::table('customer_subscriptions')
            ->join('subscription_plans', 'subscription_plans.id', '=', 'customer_subscriptions.plan_id')
            ->where('customer_subscriptions.customer_id' ,'=', $user_id)
            ->whereIn('customer_subscriptions.status', ['active','cancel'])
            ->get();
   
        
        //return CustomerSubscription::firstWhere(['customer_id' => $user_id, 'status' => 'active']);

    }

    public function makeSubscriptionSocialiteSignup($requestData)
    {

    }

    public function getCustomerSubscriptionRequest(Subscription $stripe_subscription_res, SubscriptionPlan $plan, $user_id)
    {
        //store customer subscription
        $date = date('Y-m-d', $stripe_subscription_res->start_date);
        $subscription_end_date = Carbon::parse($date)->add($plan->interval_value, $plan->interval_unit)->toDateString();
        $requestData['customer_id'] = $user_id;
        $requestData['plan_id'] = $plan->id;
        $requestData['interval_value'] = $stripe_subscription_res->plan->interval_count;
        $requestData['interval_unit'] = $stripe_subscription_res->plan->interval;
        $requestData['start_date'] = $stripe_subscription_res->start_date;
        $requestData['end_date'] = $subscription_end_date;
        $requestData['trial_start'] = $stripe_subscription_res->trial_start;
        $requestData['trial_end'] = $stripe_subscription_res->trial_end;
        $requestData['status'] = $stripe_subscription_res->status;
        $requestData['stripe_plan_id'] = $stripe_subscription_res->plan->id;
        $requestData['stripe_product_id'] = $stripe_subscription_res->plan->product;
        $requestData['stripe_subscription_id'] = $stripe_subscription_res->id;
        return $requestData;
    }

    public function checkSubscription($id)
    {

        $customerPlan = CustomerSubscription::firstWhere('customer_id', $id);
        if ($customerPlan->end_date < Carbon::now()) {

            CustomerSubscription::where('customer_id',$id)->update(['status' => null]);
            return false;
        } else {
            return true;
        }
    }

}
