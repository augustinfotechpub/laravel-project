<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Front;

use App\Interfaces\Front\UserInterface;
use App\Models\User;
use Illuminate\Support\Str;

class UserRepository implements UserInterface
{
    public function store($requestData)
    {
        return User::create($requestData);
    }

    public function update($requestData, $id)
    {
        if (isset($requestData['phone_number'])) {
            $requestData['phone_number'] = cleanString($requestData['phone_number']);
        }
        $user = User::findOrFail($id);
        return $user->update($requestData);

    }

    public function getById($id)
    {
        return User::findOrFail($id);
    }
    
    public function getAllUsers()
    {
        return User::all();
    }
}
