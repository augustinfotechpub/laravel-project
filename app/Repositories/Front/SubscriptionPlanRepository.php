<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Front;

use App\Interfaces\Front\SubscriptionPlanInterface;
use App\Models\SubscriptionPlan;

class SubscriptionPlanRepository implements SubscriptionPlanInterface
{

    public function getAll()
    {
        return SubscriptionPlan::all();
    }

    public function getById($id)
    {
        return SubscriptionPlan::findOrFail($id);
    }

    public function getCurrentPlan($current_plan_id)
    {
        $plan = SubscriptionPlan::find($current_plan_id);
        if (is_null($current_plan_id) || is_null($plan)) {
            $plan = SubscriptionPlan::first();
        }
        return $plan;
    }



}
