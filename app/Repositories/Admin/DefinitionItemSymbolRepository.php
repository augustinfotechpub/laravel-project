<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\DefinitionItemSymbolInterface;
use App\Models\DefinitionItemSymbol;


class DefinitionItemSymbolRepository implements DefinitionItemSymbolInterface
{
    public function store($definition_id, $definition_item_id, $request)
    {
        $requestData = $request->all();
        if (isset($requestData['symbols'])) {
            foreach ($definition_item_id as $index => $id) {
                foreach ($requestData['symbols'][$index] as $symbol) {
                    if (!is_null($symbol)) {
                        DefinitionItemSymbol::create([
                            'definition_id' => $definition_id,
                            'definition_item_id' => $id,
                            'symbol' => $symbol,
                        ]);
                    }
                }
            }
        }
        return true;

    }

    public function update($request, $definition_id, $definition_item_id)
    {
        $this->delete($definition_id);
        $this->store($definition_id, $definition_item_id, $request);
        return true;
    }

    public function delete($id)
    {
        return DefinitionItemSymbol::where('definition_id', $id)->delete();
    }
}
