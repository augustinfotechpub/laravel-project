<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\DefinitionItemSicsInterface;
use App\Interfaces\Admin\DefinitionItemTagsInterface;
use App\Models\DefinitionItemSic;
use App\Models\DefinitionItemTag;
use App\Models\DefinitionItemTagMap;

class DefinitionItemTagsRepository implements DefinitionItemTagsInterface
{
    public function store($definition_id, $definition_item_id, $request)
    {
        $requestData = $request->all();
        
        foreach ($definition_item_id as $index => $id) {
            foreach ($requestData['tags'][$index] as $tag) {
                if (!is_null($tag)) {
                    DefinitionItemTag::create([
                        'definition_id' => $definition_id,
                        'definition_item_id' => $id,
                        'keyword' => $tag,
                    ]);
                }
            }
        }
        return true;

    }

    public function update($request, $definition_id, $definition_item_id)
    {
        $this->delete($definition_id);
        $this->deleteTagMap($definition_id);
        $this->store($definition_id, $definition_item_id, $request);
        return true;
    }

    public function deleteTagMap($id)
    {
        return DefinitionItemTagMap::where('definition_id', $id)->delete();
    }

    public function delete($id)
    {
        return DefinitionItemTag::where('definition_id', $id)->delete();
    }


}
