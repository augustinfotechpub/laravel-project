<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;

use App\Interfaces\Admin\SubscriptionPlanInterface;
use App\Models\SubscriptionPlan;
use App\Services\Stripe;
use Illuminate\Http\Request;

class SubscriptionPlanRepository implements SubscriptionPlanInterface
{
    protected $stripeService;

    public function __construct(Stripe $stripeService)
    {

        $this->stripeService = $stripeService;
    }

    public function getAll()
    {
        return SubscriptionPlan::all();
    }

    public function getById($id)
    {
        return SubscriptionPlan::find($id);
    }

    public function store(Request $request)
    {
        $requestData = $request->all();

        try {
            // Create New Subscription Plan in Stripe
            $stripePlanResponse = $this->stripeService->createSubscriptionPlan($request);
            // Store the New Subscription Plan in the database

            SubscriptionPlan::create(
                [
                    'title' => $requestData['title'],
                    'product_key' => $stripePlanResponse['price']->product,
                    'price_key' => $stripePlanResponse['price']->id,
                    'free_trial_period' => $requestData['free_trial_period'],
                    'free_trial_unit' => $requestData['free_trial_unit'],
                    'currency' => 'USD',
                    'interval_value' => $requestData['interval_value'],
                    'interval_unit' => $requestData['interval_unit'],
                    'price' => $requestData['price']
                ]
            );
            return ['status' => true, "message" => "Subscription Plan created successfully.", "response" => $stripePlanResponse];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function update(Request $request,$id)
    {
        $requestData = $request->all();
        $stripePlanResponse = $this->stripeService->updateSubscriptionPlan($request);
        $requestData['price_key'] = $stripePlanResponse['price']->id;
        $requestData['product_key'] = $stripePlanResponse['price']->product;
        return  SubscriptionPlan::findOrFail($id)->update($requestData);
    }

    public function delete($id)
    {
        return SubscriptionPlan::destroy($id);
    }

}
