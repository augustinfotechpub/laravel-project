<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;

use App\Interfaces\Admin\SiteConfigurationInterface;
use App\Models\SiteConfiguration;
use Illuminate\Http\Request;

class SiteConfigurationRepository implements SiteConfigurationInterface
{
    public function getAll()
    {
        return SiteConfiguration::all();
    }

    public function update(Request $request)
    {
        foreach ($request->get('configuration') as $configuration_key => $configuration_value) {
            SiteConfiguration::where(['identifier' => $configuration_key])->update(['value' => $configuration_value]);

        }
    }
}
