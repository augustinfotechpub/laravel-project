<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\KeyDefinitionMappingInterface;
use App\Models\Sic;
use App\Models\SicKeyDefinition;
use Yajra\DataTables\DataTables;

class KeyDefinitionMappingRepository implements KeyDefinitionMappingInterface
{

    public function getAllAjax()
    {
        $sic = Sic::with('definitions')->get();
        return DataTables::of($sic)
            ->editColumn('title', function ($sic) {
                return ucfirst($sic->title);
            })
            ->addColumn('mapping_definition', function ($sic) {
                return view('admin.key_alert_mapping.partials.mapping_definition', compact('sic'));
            })
            ->addColumn('action', function ($sic) {
                return view('admin.key_alert_mapping.partials.action', compact('sic'));
            })
            ->rawColumns(['mapping_definition', 'action',])
            ->make(true);

    }

    public function getAll()
    {
        return Sic::all();
    }

    public function store($request)
    {

        foreach ($request->definitions as $sic => $definitions) {
            foreach ($definitions as $definition) {
                if (!SicKeyDefinition::query()->where(['sic_id' => $sic, 'key_definition_id' => $definition])->exists() && !is_null($definition)) {
                    SicKeyDefinition::query()->create(['sic_id' => $sic, 'key_definition_id' => $definition]);
                }
            }
        }
        return true;
    }

    public function delete($id)
    {
        return SicKeyDefinition::where('sic_id', $id)->delete();
    }

    public function getById($id)
    {
        return Sic::findOrFail($id);
    }

    public function update($request, $id)
    {
        SicKeyDefinition::where('sic_id', $id)->delete();
        foreach ($request->key_definition as $ley_definition) {
            if (!SicKeyDefinition::query()->where(['sic_id' => $id, 'key_definition_id' => $ley_definition])->exists() && !is_null($ley_definition)) {
                SicKeyDefinition::query()->create(['sic_id' => $id, 'key_definition_id' => $ley_definition]);
            }
        }
    }

}
