<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\DefinitionItemSicsInterface;
use App\Models\DefinitionItemSic;


class DefinitionItemSicsRepository implements DefinitionItemSicsInterface
{
    public function store($definition_id, $definition_item_id, $request)
    {
        $requestData = $request->all();

        if (isset($requestData['sic_start']) && isset($requestData['sic_end'])) {
            foreach ($definition_item_id as $index => $id) {

                if( !isset($requestData['sic_start'][$index]) || empty($requestData['sic_start'][$index]) ){
                    continue;
                }

                foreach ((array)$requestData['sic_start'][$index] as $sic_start_key => $sic_start ) {

                    $start = isset($requestData['sic_start'][$index][$sic_start_key]) ? $requestData['sic_start'][$index][$sic_start_key] : null;
                    $end = isset($requestData['sic_end'][$index][$sic_start_key]) ? $requestData['sic_end'][$index][$sic_start_key] : null;

                    if (!is_null($start) && !is_null($end)) {
                        for ($i = $start; $i <= $end; $i++) {
                            DefinitionItemSic::create([
                                'definition_id' => $definition_id,
                                'definition_item_id' => $id,
                                'sic' => $i,
                            ]);
                        }
                    }
                }

            }
        }
        if (isset($requestData['sics'])) {
            foreach ($definition_item_id as $index => $id) {
                foreach ($requestData['sics'][$index] as $sic) {
                    if (!is_null($sic)) {
                        DefinitionItemSic::create([
                            'definition_id' => $definition_id,
                            'definition_item_id' => $id,
                            'sic' => $sic,
                        ]);
                    }
                }
            }
        }
        return true;
    }

    public function update($request, $definition_id, $definition_item_id)
    {
        $this->delete($definition_id);
        $this->store($definition_id, $definition_item_id, $request);
        return true;
    }

    public function delete($id)
    {

        return DefinitionItemSic::where('definition_id', $id)->delete();
    }


}
