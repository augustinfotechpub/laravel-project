<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\CompanyInterface;
use App\Models\AuditCommitteeMember;
use App\Models\Company;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class CompanyRepository implements CompanyInterface
{

    public function getAllAjax(Request $request)
    {
        $rows = Company::query()->get();
        return DataTables::of($rows)
            ->editColumn('auditCommitteeMembers', function ($data) {
                return view('admin.companies.partials.audit_committee_members', compact('data'));
            })
            ->addColumn('action', function ($data) {
                return view('admin.companies.partials.action', compact('data'));
            })
            ->rawColumns(['auditCommitteeMembers', 'action'])
            ->make(true);
    }

    public function delete($id)
    {
        try {
            $company = Company::query()->where('id', $id)->firstOrFail();
            $company->auditCommitteeMembers->delete();
            $company->delete();
        } catch (\Exception $e) {

        }
    }

    public function getById($id)
    {
        return Company::query()->where('id', $id)->firstOrFail();
    }

    public function getBySymbol($symbol)
    {
        return Company::query()->where('symbol', $symbol)->first();
    }

    public function update($id, Request $request)
    {
        $company = self::getById($id);
        !$company->auditCommitteeMembers->isEmpty() ? AuditCommitteeMember::query()->where('symbol', $company->symbol)->delete() : null;
        foreach ($request->members_name as $key => $name) {
            if (is_null($name)) {
                continue;
            }
            AuditCommitteeMember::query()->create([
                'symbol' => $company->symbol,
                'name' => $name,
                'is_chairman' => (isset($request->is_chairman) && $request->is_chairman == $key)
            ]);
        }
    }

    public function getAll()
    {
        return Company::query()->where('entity_type', 'operating')->get();
    }

    public function searchCompany($profile, $keyword)
    {
        $query = $keyword;
        /*$bestMatches = Company::query()->where('name', 'LIKE', "%{$query}%")
            ->orWhereHas('symbols', function ($q) use ($query) {
                $q->where('symbol', 'LIKE', "%{$query}%");
            })
            ->orWhere('cik', 'LIKE', "%{$query}%")
            ->take(5)->get()->toArray();*/

            // $bestMatches = DB::table("companies as c")
            //     ->join("symbols as s", function($join){
            //             $join->on("c.id", "=", "s.company_id");
            //         })
            //     ->select("c.id", "c.cik", "c.name", "s.symbol")
            //     ->orWhere("c.name", "like", "%{$query}%")
            //     // ->orWhere("c.cik", "like", "%{$query}%")
            //     ->orWhere("s.symbol", "like", "%{$query}%")
            //     ->get();
            
            $bestMatches = DB::table("companies as com")
                ->join('symbols as sy', function ($join) {
                    $join->on('sy.company_id', '=', 'com.id');
                })
                ->select("com.id", "com.cik", "com.name", "sy.symbol")
                ->orWhere("com.name", "like", "%$query%")
                ->orWhere("com.cik", "like", "%$query%")
                ->orWhere("sy.symbol", "like", "%$query%")
                ->orWhere("com.description", "like", "%$query}%")
                ->orWhere("com.sic_description", "like", "%$query%")
                ->orWhere("com.eod_description", "like", "%$query%")
                ->orderby('com.name','asc')
                ->limit(50)
                ->get()
                ->toArray();

        $bestMatches = (array) json_decode( json_encode( $bestMatches ), true );
        // $bestMatches = DB::table("companies as c")
		// 		->join("symbols as s", function($join){
        // 				$join->on("c.id", "=", "s.company_id");
		// 			})
		// 		->select("c.id", "c.cik", "c.name", "s.symbol")
		// 		->orWhere("c.name", "like", "%{$query}%")
		// 		->orWhere("c.cik", "like", "%{$query}%")
		// 		->orWhere("s.symbol", "like", "%{$query}%")
		// 		->get();
        // $bestMatches = (array) json_decode( json_encode( $bestMatches ), true );	    
        return $bestMatches;
    }

    public function searchcompanyright($profile, $keyword)
    {
            $sic=$profile["sic"];
            $s=$profile["symbol"];
            $bestMatches = DB::table("companies as c")
            ->join("symbols as s", function($join){
                    $join->on("c.id", "=", "s.company_id");
                })
            ->select("c.id", "c.cik", "c.name", "s.symbol","c.sic")
            ->orWhere("c.sic", "like", $sic)
            ->where('s.symbol', '!=' , $s)
            ->get(); 

        $bestMatches = (array) json_decode( json_encode( $bestMatches ), true ); 
        return $bestMatches;
    }

    public function getCompanyByCik($cik)
    {
        $cik = ltrim($cik, 0);
        return Company::query()->where('cik', $cik)->first();
    }

    public function getCompaniesListAfterId($id)
    {
        return Company::query()->where('id', '>=', $id)->get();
    }
}
