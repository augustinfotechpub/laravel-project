<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\UserInterface;
use App\Models\User;
use Yajra\DataTables\DataTables;

class UserRepository implements UserInterface
{
    public function getAllAjax()
    {
        $users = User::latest()->get();

        return DataTables::of($users)
            ->addColumn('name', function ($users) {
                return $users->full_name;
            })
            ->addColumn('activate_my_account', function ($users) {
                if( $users->activate_my_account == '1' ){
                    return 'Yes';
                } else {
                    return 'No';
                }
            })
            ->addColumn('cancel_subscription', function ($users) {
                if( $users->cancel_subscription == '1' ){
                    return 'Yes';
                } else {
                    return 'No';
                }
            })
            ->addColumn('action', function ($users) {
                return view('admin.users.partials.action', compact('users'));
            })
            ->rawColumns(['name', 'action',])
            ->make(true);
    }

    public function getById($id)
    {
        return User::findOrFail($id);
    }

    public function delete($id)
    {
        return User::findOrFail($id)->delete();
    }

    public function getLatestUsers($limit)
    {
       return  User::orderBy('id', 'desc')->take($limit)->get();
    }

    public function getActivateUsers()
    {
        return  User::where(['status'=>true,'active_subscription'=>true])->get();

    }

}
