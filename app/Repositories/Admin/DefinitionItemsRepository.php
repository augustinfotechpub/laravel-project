<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\DefinitionItemsInterface;
use App\Models\DefinitionItem;


class DefinitionItemsRepository implements DefinitionItemsInterface
{
    public function store($definition_id, $request)
    {
        $requestData = $request->all();
        $definition_item_id = [];


        if (isset($requestData['content']) ) {
            
            foreach ($requestData['content'] as $index => $item) {
                if (!is_null($requestData['content'][$index])) {
                    $definitionItem = DefinitionItem::create([
                        'definition_id' => $definition_id,
                        'sub_title' => $requestData['sub_title'][$index],
                        'type' => $requestData['type'][$index],
                        'identifier' => $requestData['identifier'][$index],
                        'content' => $requestData['content'][$index],
                    ]);
                    $definition_item_id[$index] = $definitionItem->id;
                }
            }
        }
        
        return $definition_item_id;
    }

    public function update($request, $definition_id)
    {
        $requestData = $request->all();
        $this->delete($definition_id);
        return $this->store($definition_id, $request);
    }

    public function delete($id)
    {
        return DefinitionItem::where('definition_id', $id)->delete();
    }
}
