<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\FinancialStatementInterface;
use App\Models\FinancialStatement;

class FinancialStatementRepository implements FinancialStatementInterface
{
    public function getPeriods()
    {
        return FinancialStatement::PERIODS;
    }

    public function storeStatement(array $data)
    {
        FinancialStatement::query()->updateOrCreate([
            'company_id' => $data['company_id'],
            'ticker' => $data['ticker'],
            'type' => $data['type'],
            'period' => $data['period']
        ],
            $data,
        );
    }

    public function getStatement($symbol, $type, $period = 'FY')
    {
        $period = strtolower($period);
        if (in_array($period, ['year', 'annual', 'fy']))
            $period = 'FY';
        if (in_array($period, ['quarter', 'quarterly']))
            $period = 'quarter';
        return FinancialStatement::query()->where(['ticker' => strtoupper($symbol), 'period' => $period, 'type' => $type])->first();
    }

    public function getLatestStatement($companyId, $period, $type)
    {
        $period = strtolower($period);
        if (in_array($period, ['quarter', 'quarterly']))
            $period = 'quarter';
        if (in_array($period, ['annual', 'annually', 'fy']))
            $period = 'FY';

        return FinancialStatement::query()
            ->where([
                'company_id' => $companyId,
                'period' => $period,
                'type' => $type
            ])->first();
    }
}
