<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\CustomerSubscriptionInterface;
use App\Models\CustomerSubscription;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;

class CustomerSubscriptionRepository implements CustomerSubscriptionInterface
{
    public function getAllAjax()
    {
        $customer_subscriptions = CustomerSubscription::with('customer', 'subscriptionPlan')->latest()->get();

        return DataTables::of($customer_subscriptions)
            ->addColumn('plan_title', function ($customer_subscriptions) {
                return $customer_subscriptions->subscriptionPlan->title;
            })
            ->addColumn('customer_name', function ($customer_subscriptions) {
                if(isset($customer_subscriptions->customer) && isset($customer_subscriptions->customer->full_name))
                {
                    return $customer_subscriptions->customer->full_name;
                }
                return '';
            })
            ->addColumn('price', function ($customer_subscriptions) {
                return formatPrice($customer_subscriptions->subscriptionPlan->price);
            })
            ->editColumn('start_date', function ($customer_subscriptions) {
                return formatDate($customer_subscriptions->start_date);
            })
            ->editColumn('end_date', function ($customer_subscriptions) {
                return formatDate($customer_subscriptions->end_date);
            })
            ->editColumn('status', function ($customer_subscriptions) {
                if ($customer_subscriptions->status == 'active') {
                    return ucfirst($customer_subscriptions->status);
                } else {
                    return 'Deactivate';
               }
            })
            ->addColumn('action', function ($customer_subscriptions) {
                return view('admin.customer_subscriptions.partials.action', compact('customer_subscriptions'));
            })
            ->rawColumns(['plan_title', 'customer_name', 'price', 'action',])
            ->make(true);
    }

    public function getById($id)
    {
        return CustomerSubscription::findOrFail($id);
    }

    public function delete($id)
    {
        return CustomerSubscription::findOrFail($id)->delete();
    }

    public function getTotalAmount()
    {
        return CustomerSubscription::where(['status' => 'active'])->get()->sum('price');
    }

    public function getThisMonthTotalAmount()
    {
        return CustomerSubscription::whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)->get()->sum('price');
    }

    public function getThisMonthExpired()
    {
        return CustomerSubscription::whereYear('end_date', Carbon::now()->year)
            ->whereMonth('end_date', Carbon::now()->month)->get();

    }
}
