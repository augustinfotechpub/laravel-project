<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\FinancialStatementEntriesInterface;
use App\Models\FinancialStatementEntry;

class FinancialStatementEntriesRepository implements FinancialStatementEntriesInterface
{

    public function getByStatementId($id,$limit = 10)
    {
        return FinancialStatementEntry::query()->where('financial_statement_id', $id)->take($limit)->get();
    }

    public function store(array $data)
    {
        // TODO: Implement store() method.
    }
}
