<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\ContactRequestInterface;
use App\Models\Contact;
use Yajra\DataTables\DataTables;

class ContactRequestRepository implements ContactRequestInterface
{
    public function getAllAjax()
    {
        $contacts = Contact::latest()->get();

        return DataTables::of($contacts)
            ->editColumn('name', function ($contacts) {
                return ucfirst($contacts->name);
            })
            ->addColumn('action', function ($contacts) {
                return view('admin.contact_requests.partials.action', compact('contacts'));
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getById($id)
    {
        return Contact::findOrFail($id);
    }

    public function delete($id)
    {
        return Contact::findOrFail($id)->delete();
    }


}
