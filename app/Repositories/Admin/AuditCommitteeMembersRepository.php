<?php


namespace App\Repositories\Admin;


use App\Interfaces\Admin\AuditCommitteeMembersInterface;
use App\Models\Company;
use Yajra\DataTables\Facades\DataTables;

class AuditCommitteeMembersRepository implements AuditCommitteeMembersInterface
{
    public function getAllAjax()
    {
        return null;
    }
}
