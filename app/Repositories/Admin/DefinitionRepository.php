<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\DefinitionInterface;
use App\Models\Definition;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class DefinitionRepository implements DefinitionInterface
{
    public function getAllAjax()
    {

        $definitions = Definition::query()->orderBy('id','desc')->get();

        return DataTables::of($definitions)
            ->editColumn('title', function ($definitions) {
                return ucfirst($definitions->title);
            })
            ->addColumn('action', function ($definition) {
                return view('admin.alerts.partials.action', compact('definition'));
            })
            ->rawColumns(['action'])
            ->make(true);

    }

    public function getAll()
    {
        return Definition::query()->get();
    }

    public function getById($id)
    {
        $definition = Definition::findOrFail($id);
        return $definition;
    }

    public function store(Request $request)
    {
        $request->except('identifier');
        $requestData = $request->all();
        return Definition::query()->create([
            'title' => $requestData['title']
        ]);
    }

    public function update(Request $request, $id)
    {
        $definition = Definition::query()->findOrFail($id);
        $requestData = $request->all();
        return $definition->update([
            'title'=>$requestData['title'],
            'identifier'=>$requestData['identifier'],
        ]);
    }

    public function delete($id)
    {
        return Definition::query()->findOrFail($id)->delete();
    }

}
