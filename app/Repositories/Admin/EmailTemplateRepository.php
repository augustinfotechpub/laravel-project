<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 04-12-2020
 * Time: 04:49 PM
 */

namespace App\Repositories\Admin;


use App\Interfaces\Admin\EmailTemplateInterface;
use App\Models\EmailTemplate;
use Illuminate\Http\Request;

class EmailTemplateRepository implements EmailTemplateInterface
{
    public function getAll()
    {
        return EmailTemplate::all();
    }

    public function getById($id)
    {
        return EmailTemplate::findOrFail($id);
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        return EmailTemplate::create($requestData);
    }

    public function update(Request $request, $id)
    {
        $requestData = $request->all();
        return EmailTemplate::findOrFail($id)->update($requestData);
    }

    public function delete($id)
    {
        return EmailTemplate::findOrFail($id)->delete();
    }

}
