<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('companies');
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('cik');
            $table->string('entity_type')->nullable();
            $table->string('sic')->nullable();
            $table->json('tickers')->nullable();
            $table->json('exchanges')->nullable();
            $table->text('sic_description')->nullable();
            $table->string('ein')->nullable();
            $table->string('description')->nullable();
            $table->string('website')->nullable();
            $table->string('investor_website')->nullable();
            $table->string('category')->nullable();
            $table->string('fiscal_year_end')->nullable();
            $table->integer('mailing_address_id')->nullable();
            $table->integer('business_address_id')->nullable();
            $table->string('phone')->nullable();
            $table->date('date_first_added')->nullable();
            $table->date('founded_date')->nullable();
            $table->timestamps();
        });

        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string('street1');
            $table->string('street2')->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('type')->nullable();
            $table->string('state_or_country')->nullable();
            $table->string('state_or_country_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
        Schema::dropIfExists('addresses');
    }
}
