<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('symbols', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->bigInteger('exchange_id');
            $table->string('symbol');
            $table->timestamps();
        });

        Schema::create('exchanges', function (Blueprint $table) {
            $table->id();
            $table->string('short_name')->nullable();
            $table->string('full_name')->nullable();
            $table->timestamps();
        });

        Schema::create('income_statements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->string('date')->nullable();
            $table->string('symbol')->nullable();
            $table->string('reportedCurrency')->nullable();
            $table->string('fillingDate')->nullable();
            $table->string('acceptedDate')->nullable();
            $table->string('period')->nullable();
            $table->string('revenue')->nullable();
            $table->string('costOfRevenue')->nullable();
            $table->string('grossProfit')->nullable();
            $table->string('grossProfitRatio')->nullable();
            $table->string('researchAndDevelopmentExpenses')->nullable();
            $table->string('generalAndAdministrativeExpenses')->nullable();
            $table->string('sellingAndMarketingExpenses')->nullable();
            $table->string('otherExpenses')->nullable();
            $table->string('operatingExpenses')->nullable();
            $table->string('costAndExpenses')->nullable();
            $table->string('interestExpense')->nullable();
            $table->string('depreciationAndAmortization')->nullable();
            $table->string('ebitda')->nullable();
            $table->string('ebitdaratio')->nullable();
            $table->string('operatingIncome')->nullable();
            $table->string('operatingIncomeRatio')->nullable();
            $table->string('totalOtherIncomeExpensesNet')->nullable();
            $table->string('incomeBeforeTax')->nullable();
            $table->string('incomeBeforeTaxRatio')->nullable();
            $table->string('incomeTaxExpense')->nullable();
            $table->string('netIncome')->nullable();
            $table->string('netIncomeRatio')->nullable();
            $table->string('eps')->nullable();
            $table->string('epsdiluted')->nullable();
            $table->string('weightedAverageShsOut')->nullable();
            $table->string('weightedAverageShsOutDil')->nullable();
            $table->string('link')->nullable();
            $table->string('finalLink')->nullable();
            $table->timestamps();
        });

        Schema::create('balance_sheet_statements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id')->nullable();
            $table->string('date')->nullable();
            $table->string('symbol')->nullable();
            $table->string('reportedCurrency')->nullable();
            $table->string('fillingDate')->nullable();
            $table->string('acceptedDate')->nullable();
            $table->string('period')->nullable();
            $table->string('cashAndCashEquivalents')->nullable();
            $table->string('shortTermInvestments')->nullable();
            $table->string('cashAndShortTermInvestments')->nullable();
            $table->string('netReceivables')->nullable();
            $table->string('inventory')->nullable();
            $table->string('otherCurrentAssets')->nullable();
            $table->string('totalCurrentAssets')->nullable();
            $table->string('propertyPlantEquipmentNet')->nullable();
            $table->string('goodwill')->nullable();
            $table->string('intangibleAssets')->nullable();
            $table->string('goodwillAndIntangibleAssets')->nullable();
            $table->string('longTermInvestments')->nullable();
            $table->string('taxAssets')->nullable();
            $table->string('otherNonCurrentAssets')->nullable();
            $table->string('totalNonCurrentAssets')->nullable();
            $table->string('otherAssets')->nullable();
            $table->string('totalAssets')->nullable();
            $table->string('accountPayables')->nullable();
            $table->string('shortTermDebt')->nullable();
            $table->string('taxPayables')->nullable();
            $table->string('deferredRevenue')->nullable();
            $table->string('otherCurrentLiabilities')->nullable();
            $table->string('totalCurrentLiabilities')->nullable();
            $table->string('longTermDebt')->nullable();
            $table->string('deferredRevenueNonCurrent')->nullable();
            $table->string('deferredTaxLiabilitiesNonCurrent')->nullable();
            $table->string('otherNonCurrentLiabilities')->nullable();
            $table->string('totalNonCurrentLiabilities')->nullable();
            $table->string('otherLiabilities')->nullable();
            $table->string('totalLiabilities')->nullable();
            $table->string('commonStock')->nullable();
            $table->string('retainedEarnings')->nullable();
            $table->string('accumulatedOtherComprehensiveIncomeLoss')->nullable();
            $table->string('othertotalStockholdersEquity')->nullable();
            $table->string('totalStockholdersEquity')->nullable();
            $table->string('totalLiabilitiesAndStockholdersEquity')->nullable();
            $table->string('totalInvestments')->nullable();
            $table->string('totalDebt')->nullable();
            $table->string('netDebt')->nullable();
            $table->string('link')->nullable();
            $table->string('finalLink')->nullable();
            $table->timestamps();
        });

        Schema::create('cash_flow_statements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->string('date')->nullable();
            $table->string('symbol')->nullable();
            $table->string('reportedCurrency')->nullable();
            $table->string('fillingDate')->nullable();
            $table->string('acceptedDate')->nullable();
            $table->string('period')->nullable();
            $table->string('netIncome')->nullable();
            $table->string('depreciationAndAmortization')->nullable();
            $table->string('deferredIncomeTax')->nullable();
            $table->string('stockBasedCompensation')->nullable();
            $table->string('changeInWorkingCapital')->nullable();
            $table->string('accountsReceivables')->nullable();
            $table->string('inventory')->nullable();
            $table->string('accountsPayables')->nullable();
            $table->string('otherWorkingCapital')->nullable();
            $table->string('otherNonCashItems')->nullable();
            $table->string('netCashProvidedByOperatingActivities')->nullable();
            $table->string('investmentsInPropertyPlantAndEquipment')->nullable();
            $table->string('acquisitionsNet')->nullable();
            $table->string('purchasesOfInvestments')->nullable();
            $table->string('salesMaturitiesOfInvestments')->nullable();
            $table->string('otherInvestingActivites')->nullable();
            $table->string('netCashUsedForInvestingActivites')->nullable();
            $table->string('debtRepayment')->nullable();
            $table->string('commonStockIssued')->nullable();
            $table->string('commonStockRepurchased')->nullable();
            $table->string('dividendsPaid')->nullable();
            $table->string('otherFinancingActivites')->nullable();
            $table->string('netCashUsedProvidedByFinancingActivities')->nullable();
            $table->string('effectOfForexChangesOnCash')->nullable();
            $table->string('netChangeInCash')->nullable();
            $table->string('cashAtEndOfPeriod')->nullable();
            $table->string('cashAtBeginningOfPeriod')->nullable();
            $table->string('operatingCashFlow')->nullable();
            $table->string('capitalExpenditure')->nullable();
            $table->string('freeCashFlow')->nullable();
            $table->string('link')->nullable();
            $table->string('finalLink')->nullable();
            $table->timestamps();
        });

        Schema::create('comprehensive_income_statements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->string('symbol')->nullable();
            $table->string('period')->nullable(); // Quarter,FY,TTM
            $table->string('title')->nullable();
            $table->longText('html')->nullable();  // Content will be in HTML Format
            $table->timestamps();
        });

        Schema::create('shareholders_equity_statements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->string('symbol')->nullable();
            $table->string('period')->nullable(); // Quarter,FY,TTM
            $table->string('title')->nullable();
            $table->longText('html')->nullable(); // Content will be in HTML Format
            $table->timestamps();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('tickers', 'exchanges');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('symbols');
        Schema::dropIfExists('exchanges');
        Schema::dropIfExists('income_statements');
        Schema::dropIfExists('balance_sheet_statements');
        Schema::dropIfExists('cash_flow_statements');
        Schema::dropIfExists('comprehensive_income_statements');
        Schema::dropIfExists('shareholders_equity_statements');
        Schema::table('companies', function (Blueprint $table) {
            $table->json('tickers')->nullable();
            $table->json('exchanges')->nullable();
        });
    }
}
