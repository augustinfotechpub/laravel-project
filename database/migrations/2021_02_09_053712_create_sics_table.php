<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sic', function (Blueprint $table) {
            $table->id();
            $table->string('sic_code')->nullable();
            $table->string('office')->nullable();
            $table->string('title')->nullable();
            $table->timestamps();
        });

        Schema::create('sic_key_definitions', function (Blueprint $table) {
            $table->integer('sic_id')->nullable();
            $table->integer('key_definition_id')->nullable();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sics');
        Schema::dropIfExists('sic_key_definitions');
    }
}
