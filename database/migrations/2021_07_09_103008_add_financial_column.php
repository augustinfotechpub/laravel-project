<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinancialColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eod_balance_sheets', function (Blueprint $table) {
            $table->string('grossPPE')->nullable()->after('nonCurrentAssetsTotal');
            $table->text('dividendsPaid')->nullable()->after('grossPPE');
            $table->text('dividendShare')->nullable()->after('dividendsPaid');
            $table->text('dividendYield')->nullable()->after('dividendShare');
            $table->text('BookValue')->nullable()->after('dividendYield');
        });

        Schema::table('eod_cash_flows', function (Blueprint $table) {
            $table->string('netIncomeFromContinuingOps')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('depreciationAndAmortization')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('totalCurrentAssets')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('totalCurrentLiabilities')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('accountsPayable')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('propertyPlantAndEquipmentGross')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('longTermDebtTotal')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('shortTermDebt')->nullable()->after('totalCashFromOperatingActivities');
            $table->string('commonStockSharesOutstanding')->nullable()->after('totalCashFromOperatingActivities');
        });

        Schema::table('eod_income_statements', function (Blueprint $table) {
            $table->string('eps')->nullable()->after('netIncomeApplicableToCommonShares');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
