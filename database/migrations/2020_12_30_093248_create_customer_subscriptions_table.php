<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->integer('plan_id');
            $table->string('interval_value')->nullable();
            $table->string('interval_unit')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamp('trial_start')->nullable();
            $table->timestamp('trial_end')->nullable();
            $table->string('status')->nullable(); // Active, expired, payment_pending

            $table->string('stripe_customer_id');
            $table->string('stripe_plan_id')->nullable();
            $table->string('stripe_product_id')->nullable();
            $table->string('stripe_subscription_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string("first_name")->nullable()->after('id');
            $table->string("last_name")->nullable()->after('first_name');
            $table->string("image")->nullable();
            $table->string("phone_number")->unique()->nullable();
            $table->string("address_line1")->nullable();
            $table->string("address_line2")->nullable();
            $table->string("city")->nullable();
            $table->string("state")->nullable();
            $table->string("country")->nullable();
            $table->string("zip_code")->nullable();
            $table->boolean('status')->default(false);
            $table->string('stripe_customer_id')->nullable();
            $table->integer('current_subscription_id')->nullable();
            $table->boolean("active_subscription")->default(0);
            $table->softDeletes()->after('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_subscriptions');

        Schema::table('users', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn(
                'first_name','last_name','image','phone_number','address_line1','address_line2',
                'city','state','country','zip_code','status','stripe_customer_id','current_subscription_id',
                'active_subscription','deleted_at');

        });
    }
}
