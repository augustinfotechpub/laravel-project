<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEdgarFinancialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edgar_financial', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->bigInteger('taxonomyid')->nullable();
            $table->text('cik')->nullable();
            $table->text('companyname')->nullable();
            $table->text('entityid')->nullable();
            $table->text('primaryexchange')->nullable();
            $table->text('marketoperator')->nullable();
            $table->text('markettier')->nullable();
            $table->text('primarysymbol')->nullable();
            $table->text('siccode')->nullable();
            $table->text('sicdescription')->nullable();
            $table->text('usdconversionrate')->nullable();
            $table->text('restated')->nullable();
            $table->text('receiveddate')->nullable();
            $table->text('preliminary')->nullable();
            $table->text('periodlengthcode')->nullable();
            $table->text('periodlength')->nullable();
            $table->text('periodenddate')->nullable();
            $table->text('original')->nullable();
            $table->text('formtype')->nullable();
            $table->text('fiscalyear')->nullable();
            $table->text('fiscalquarter')->nullable();
            $table->text('dcn')->nullable();
            $table->text('currencycode')->nullable();
            $table->text('crosscalculated')->nullable();
            $table->text('audited')->nullable();
            $table->text('amended')->nullable();
            $table->text('changeincurrentassets')->nullable();
            $table->text('changeincurrentliabilities')->nullable();
            $table->text('changeininventories')->nullable();
            $table->text('effectofexchangerateoncash')->nullable();
            $table->text('capitalexpenditures')->nullable();
            $table->text('cashfromfinancingactivities')->nullable();
            $table->text('cashfrominvestingactivities')->nullable();
            $table->text('cashfromoperatingactivities')->nullable();
            $table->text('cfdepreciationamortization')->nullable();
            $table->text('changeinaccountsreceivable')->nullable();
            $table->text('investmentchangesnet')->nullable();
            $table->text('netchangeincash')->nullable();
            $table->text('totaladjustments')->nullable();
            $table->text('ebit')->nullable();
            $table->text('costofrevenue')->nullable();
            $table->text('equityearnings')->nullable();
            $table->text('grossprofit')->nullable();
            $table->text('incomebeforetaxes')->nullable();
            $table->text('interestexpense')->nullable();
            $table->text('netincome')->nullable();
            $table->text('netincomeapplicabletocommon')->nullable();
            $table->text('totalrevenue')->nullable();
            $table->text('sellinggeneraladministrativeexpenses')->nullable();
            $table->text('commonstock')->nullable();
            $table->text('cashandcashequivalents')->nullable();
            $table->text('cashcashequivalentsandshortterminvestments')->nullable();
            $table->text('goodwill')->nullable();
            $table->text('inventoriesnet')->nullable();
            $table->text('otherassets')->nullable();
            $table->text('otherliabilities')->nullable();
            $table->text('preferredstock')->nullable();
            $table->text('propertyplantequipmentnet')->nullable();
            $table->text('retainedearnings')->nullable();
            $table->text('totalassets')->nullable();


            $table->text('totalcurrentassets')->nullable();
            $table->text('totalcurrentliabilities')->nullable();
            $table->text('totalliabilities')->nullable();
            $table->text('totallongtermdebt')->nullable();
            $table->text('totalreceivablesnet')->nullable();
            $table->text('totalstockholdersequity')->nullable();
            $table->text('treasurystock')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edgar_financial');
    }
}
