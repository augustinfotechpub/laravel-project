<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plans', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->string("free_trial_period")->nullable();
            $table->string("free_trial_unit")->nullable();
            $table->string("interval_value")->nullable();
            $table->string("interval_unit")->nullable();
            $table->decimal('price', 8, 2)->default(0.0);
            $table->string("currency")->default("USD");
            $table->longText("other_data")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plans');
    }
}
