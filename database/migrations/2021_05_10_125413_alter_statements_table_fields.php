<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterStatementsTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('income_statements', function (Blueprint $table) {
            $table->date('date')->nullable()->change();
        });
        Schema::table('balance_sheet_statements', function (Blueprint $table) {
            $table->date('date')->nullable()->change();
        });
        Schema::table('cash_flow_statements', function (Blueprint $table) {
            $table->date('date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('income_statements', function (Blueprint $table) {
            $table->string('date')->nullable()->change();
        });
        Schema::table('balance_sheet_statements', function (Blueprint $table) {
            $table->string('date')->nullable()->change();
        });
        Schema::table('cash_flow_statements', function (Blueprint $table) {
            $table->string('date')->nullable()->change();
        });
    }
}
