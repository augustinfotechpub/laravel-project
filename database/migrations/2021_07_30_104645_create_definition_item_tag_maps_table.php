<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefinitionItemTagMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('definition_item_tag_maps', function (Blueprint $table) {
            $table->id();
            $table->integer('definition_item_tags_id')->nullable();
            $table->integer('definition_id')->nullable();
            $table->integer('definition_item_id')->nullable();
            $table->integer('cik')->nullable();
            $table->string('ticker')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });

        Schema::table('definition_item_tags', function (Blueprint $table) {
            $table->integer('is_synced')->nullable()->after('keyword');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('definition_item_tag_maps');
    }
}
