<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyCikforFinancialEOD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eod_balance_sheets', function (Blueprint $table) {
            $table->bigInteger('cik')->nullable()->after('company_id');
        });
        Schema::table('eod_cash_flows', function (Blueprint $table) {
            $table->bigInteger('cik')->nullable()->after('company_id');
        });
        Schema::table('eod_income_statements', function (Blueprint $table) {
            $table->bigInteger('cik')->nullable()->after('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
    }
}
