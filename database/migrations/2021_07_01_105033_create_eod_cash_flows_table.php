<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEodCashFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eod_cash_flows', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->string('ticker')->nullable();
            $table->string('period_type')->nullable();
            $table->string('period_date')->nullable();

            $table->string('date')->nullable();
            $table->string('filing_date')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('investments')->nullable();
            $table->string('changeToLiabilities')->nullable();
            $table->string('totalCashflowsFromInvestingActivities')->nullable();
            $table->string('netBorrowings')->nullable();
            $table->string('totalCashFromFinancingActivities')->nullable();
            $table->string('changeToOperatingActivities')->nullable();
            $table->string('netIncome')->nullable();
            $table->string('changeInCash')->nullable();
            $table->string('beginPeriodCashFlow')->nullable();
            $table->string('endPeriodCashFlow')->nullable();
            $table->string('totalCashFromOperatingActivities')->nullable();
            $table->string('depreciation')->nullable();
            $table->string('otherCashflowsFromInvestingActivities')->nullable();
            $table->string('dividendsPaid')->nullable();
            $table->string('changeToInventory')->nullable();
            $table->string('changeToAccountReceivables')->nullable();
            $table->string('salePurchaseOfStock')->nullable();
            $table->string('otherCashflowsFromFinancingActivities')->nullable();
            $table->string('changeToNetincome')->nullable();
            $table->string('capitalExpenditures')->nullable();
            $table->string('changeReceivables')->nullable();
            $table->string('cashFlowsOtherOperating')->nullable();
            $table->string('exchangeRateChanges')->nullable();
            $table->string('cashAndCashEquivalentsChanges')->nullable();
            $table->string('changeInWorkingCapital')->nullable();
            $table->string('otherNonCashItems')->nullable();
            $table->string('freeCashFlow')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eod_cash_flows');
    }
}
