<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalystratingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analystratings', function (Blueprint $table) {
            $table->id();
            $table->string('company_cik');
            $table->string('company_ticker');
            $table->string('Rating')->nullable();
            $table->string('TargetPrice')->nullable();
            $table->string('StrongBuy')->nullable();
            $table->string('Buy')->nullable();
            $table->string('Hold')->nullable();
            $table->string('Sell')->nullable();
            $table->string('StrongSell')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analystratings');
    }
}
