<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterStatementsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('income_statements', function (Blueprint $table) {
            $table->string('interestIncomeOnLoans')->nullable();
            $table->string('interestIncomeOnInvestment')->nullable();
            $table->string('interestIncomeOnSecurities')->nullable();
            $table->string('interestIncomeOnDepositsInBanks')->nullable();
            $table->string('federalFundsSoldAndSecuritiesPurchasedUnderResaleAgreements')->nullable();
            $table->string('interestIncomeFromOthers')->nullable();
            $table->string('interestOnDeposits')->nullable();
            $table->string('interestOnDebt')->nullable();
            $table->string('federalFundsPurchasedAndSecuritiesSoldUnderResaleAgreements')->nullable();
            $table->string('interestOnOtherBorrowedFunds')->nullable();
            $table->string('totalInterestExpense')->nullable();
            $table->string('provisionForLoanOrCreditLosses')->nullable();
            $table->string('netInterestIncomeAfterLoanLossProvision')->nullable();
            $table->string('mortgageBankingActivities')->nullable();
            $table->string('cardFees')->nullable();
            $table->string('federalDepositInsurance')->nullable();
            $table->string('grossPremiumsWritten')->nullable();
            $table->string('lessIncreaseInUnearnedPremiums')->nullable();
            $table->string('lessReinsuranceCeded')->nullable();
            $table->string('policyRevenuesAndBenefits')->nullable();
            $table->string('BenefitsToPolicyholdersAndClaims')->nullable();
            $table->string('lessReinsuranceRecoverable')->nullable();
            $table->string('interestCreditedToContractHoldersFunds')->nullable();
            $table->string('policyAcquisitionOrUnderwritingCosts')->nullable();
            $table->string('amortizationOfDeferredPolicyAcquisitionCosts')->nullable();
            $table->string('DividendsPaidToPolicyholders')->nullable();
            $table->string('reinsuranceIncomeOrExpense')->nullable();
        });

        Schema::table('balance_sheet_statements', function (Blueprint $table) {
            $table->text('cashAndCashEquivalentsAndDueFromBanks')->nullable();
            $table->text('interestBearingDeposits')->nullable();
            $table->text('moneyMarketInvestments')->nullable();
            $table->text('shortTermInvestmentsBanks')->nullable();
            $table->text('federalFundsSold')->nullable();
            $table->text('federalFundsSoldSecuritiesPurchasedUnderResaleAgreements')->nullable();
            $table->text('accruedInterestReceivable')->nullable();
            $table->text('rightOfUseAssets')->nullable();
            $table->text('mortgageBackedSecurities')->nullable();
            $table->text('municipalBondsAndGovernmentSecurities')->nullable();
            $table->text('investmentsInFedHomeLoanBank')->nullable();
            $table->text('investmentInRealEstate')->nullable();
            $table->text('investmentOthers')->nullable();
            $table->text('loansHeldForSale')->nullable();
            $table->text('grossLoansMade')->nullable();
            $table->text('loanLossAllowance')->nullable();
            $table->text('otherSubtractionsFromLoans')->nullable();
            $table->text('netLoans')->nullable();
            $table->text('grossPropertyPlantAndEquipment')->nullable();
            $table->text('TotalAccumulatedDepreciationAndDepletion')->nullable();
            $table->text('netPropertyPlantAndEquipment')->nullable();
            $table->text('federalHomeLoanBankDebt')->nullable();
            $table->text('nonPerformingLoans')->nullable();
            $table->text('realEstateOwned')->nullable();
            $table->text('mortgageLoans')->nullable();
            $table->text('policyLoans')->nullable();
            $table->text('netPremiumsReceivables')->nullable();
            $table->text('netReinsuranceReceivables')->nullable();
            $table->text('rightOfUseAssetsCurrent')->nullable();
            $table->text('policyBenefitsLossesAndReserves')->nullable();
            $table->text('UnearnedPremiumReserveOrInsurancePayable')->nullable();
            $table->text('reinsurancePayable')->nullable();
        });

        Schema::table('cash_flow_statements', function (Blueprint $table) {
            $table->text('changeInInterestReceivable')->nullable();
            $table->text('changeInInterestPayable')->nullable();
            $table->text('changeInLoans')->nullable();
        });
        Schema::dropIfExists('financial_statements');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('income_statements', function (Blueprint $table) {
            $table->dropColumn('interestIncomeOnLoans', 'interestIncomeOnInvestment', 'interestIncomeOnSecurities', 'interestIncomeOnDepositsInBanks', 'federalFundsSoldAndSecuritiesPurchasedUnderResaleAgreements', 'interestIncomeFromOthers', 'interestOnDeposits', 'interestOnDebt', 'federalFundsPurchasedAndSecuritiesSoldUnderResaleAgreements', 'interestOnOtherBorrowedFunds', 'totalInterestExpense', 'provisionForLoanOrCreditLosses', 'netInterestIncomeAfterLoanLossProvision', 'mortgageBankingActivities', 'cardFees', 'federalDepositInsurance', 'grossPremiumsWritten', 'lessIncreaseInUnearnedPremiums', 'lessReinsuranceCeded', 'policyRevenuesAndBenefits', 'BenefitsToPolicyholdersAndClaims', 'lessReinsuranceRecoverable', 'interestCreditedToContractHoldersFunds', 'policyAcquisitionOrUnderwritingCosts', 'amortizationOfDeferredPolicyAcquisitionCosts', 'DividendsPaidToPolicyholders', 'reinsuranceIncomeOrExpense');
        });
        Schema::table('balance_sheet_statements', function (Blueprint $table) {
            $table->dropColumn('cashAndCashEquivalentsAndDueFromBanks', 'interestBearingDeposits', 'moneyMarketInvestments', 'shortTermInvestmentsBanks', 'federalFundsSold', 'federalFundsSoldSecuritiesPurchasedUnderResaleAgreements', 'accruedInterestReceivable', 'rightOfUseAssets', 'mortgageBackedSecurities', 'municipalBondsAndGovernmentSecurities', 'investmentsInFedHomeLoanBank', 'investmentInRealEstate', 'investmentOthers', 'loansHeldForSale', 'grossLoansMade', 'loanLossAllowance', 'otherSubtractionsFromLoans', 'netLoans', 'TotalAccumulatedDepreciationAndDepletion', 'netPropertyPlantAndEquipment', 'federalHomeLoanBankDebt', 'nonPerformingLoans', 'realEstateOwned', 'mortgageLoans', 'policyLoans', 'netPremiumsReceivables', 'netReinsuranceReceivables', 'rightOfUseAssetsCurrent', 'grossPropertyPlantAndEquipment', 'policyBenefitsLossesAndReserves', 'UnearnedPremiumReserveOrInsurancePayable', 'reinsurancePayable');
        });
        Schema::table('cash_flow_statements', function (Blueprint $table) {
            $table->dropColumn('changeInInterestReceivable', 'changeInInterestPayable', 'changeInLoans');
        });
    }
}
