<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEodBalanceSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eod_balance_sheets', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->string('ticker')->nullable();
            $table->string('period_type')->nullable();
            $table->string('period_date')->nullable();
            $table->string('date')->nullable();
            $table->string('filing_date')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('totalAssets')->nullable();
            $table->string('intangibleAssets')->nullable();
            $table->string('earningAssets')->nullable();
            $table->string('otherCurrentAssets')->nullable();
            $table->string('totalLiab')->nullable();
            $table->string('totalStockholderEquity')->nullable();
            $table->string('deferredLongTermLiab')->nullable();
            $table->string('otherCurrentLiab')->nullable();
            $table->string('commonStock')->nullable();
            $table->string('retainedEarnings')->nullable();
            $table->string('otherLiab')->nullable();
            $table->string('goodWill')->nullable();
            $table->string('otherAssets')->nullable();
            $table->string('cash')->nullable();
            $table->string('totalCurrentLiabilities')->nullable();
            $table->string('netDebt')->nullable();
            $table->string('shortTermDebt')->nullable();
            $table->string('shortLongTermDebt')->nullable();
            $table->string('shortLongTermDebtTotal')->nullable();
            $table->string('otherStockholderEquity')->nullable();
            $table->string('propertyPlantEquipment')->nullable();
            $table->string('totalCurrentAssets')->nullable();
            $table->string('longTermInvestments')->nullable();
            $table->string('netTangibleAssets')->nullable();
            $table->string('shortTermInvestments')->nullable();
            $table->string('netReceivables')->nullable();
            $table->string('longTermDebt')->nullable();
            $table->string('inventory')->nullable();
            $table->string('accountsPayable')->nullable();
            $table->string('totalPermanentEquity')->nullable();
            $table->string('noncontrollingInterestInConsolidatedEntity')->nullable();
            $table->string('temporaryEquityRedeemableNoncontrollingInterests')->nullable();
            $table->string('accumulatedOtherComprehensiveIncome')->nullable();
            $table->string('additionalPaidInCapital')->nullable();
            $table->string('commonStockTotalEquity')->nullable();
            $table->string('preferredStockTotalEquity')->nullable();
            $table->string('retainedEarningsTotalEquity')->nullable();
            $table->string('treasuryStock')->nullable();
            $table->string('accumulatedAmortization')->nullable();
            $table->string('nonCurrrentAssetsOther')->nullable();
            $table->string('deferredLongTermAssetCharges')->nullable();
            $table->string('nonCurrentAssetsTotal')->nullable();
            $table->string('capitalLeaseObligations')->nullable();
            $table->string('longTermDebtTotal')->nullable();
            $table->string('nonCurrentLiabilitiesOther')->nullable();
            $table->string('nonCurrentLiabilitiesTotal')->nullable();
            $table->string('negativeGoodwill')->nullable();
            $table->string('warrants')->nullable();
            $table->string('preferredStockRedeemable')->nullable();
            $table->string('capitalSurpluse')->nullable();
            $table->string('liabilitiesAndStockholdersEquity')->nullable();
            $table->string('cashAndShortTermInvestments')->nullable();
            $table->string('propertyPlantAndEquipmentGross')->nullable();
            $table->string('accumulatedDepreciation')->nullable();
            $table->string('netWorkingCapital')->nullable();
            $table->string('netInvestedCapital')->nullable();
            $table->string('commonStockSharesOutstanding')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eod_balance_sheets');
    }
}
