<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEodIncomeStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eod_income_statements', function (Blueprint $table) {
            $table->id();

            $table->integer('company_id')->nullable();
            $table->string('ticker')->nullable();
            $table->string('period_type')->nullable();
            $table->string('period_date')->nullable();

            $table->string('date')->nullable();
            $table->string('filing_date')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('researchDevelopment')->nullable();
            $table->string('effectOfAccountingCharges')->nullable();
            $table->string('incomeBeforeTax')->nullable();
            $table->string('minorityInterest')->nullable();
            $table->string('netIncome')->nullable();
            $table->string('sellingGeneralAdministrative')->nullable();
            $table->string('sellingAndMarketingExpenses')->nullable();
            $table->string('grossProfit')->nullable();
            $table->string('reconciledDepreciation')->nullable();
            $table->string('ebit')->nullable();
            $table->string('ebitda')->nullable();
            $table->string('depreciationAndAmortization')->nullable();
            $table->string('nonOperatingIncomeNetOther')->nullable();
            $table->string('operatingIncome')->nullable();
            $table->string('otherOperatingExpenses')->nullable();
            $table->string('interestExpense')->nullable();
            $table->string('taxProvision')->nullable();
            $table->string('interestIncome')->nullable();
            $table->string('netInterestIncome')->nullable();
            $table->string('extraordinaryItems')->nullable();
            $table->string('nonRecurring')->nullable();
            $table->string('otherItems')->nullable();
            $table->string('incomeTaxExpense')->nullable();
            $table->string('totalRevenue')->nullable();
            $table->string('totalOperatingExpenses')->nullable();
            $table->string('costOfRevenue')->nullable();
            $table->string('totalOtherIncomeExpenseNet')->nullable();
            $table->string('discontinuedOperations')->nullable();
            $table->string('netIncomeFromContinuingOps')->nullable();
            $table->string('netIncomeApplicableToCommonShares')->nullable();
            $table->string('preferredStockAndOtherAdjustments')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eod_income_statements');
    }
}
