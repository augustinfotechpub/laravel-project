<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllDefinitionRelatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('definitions', function (Blueprint $table) {
            $table->dropColumn('key','type','definition','tags','sics','symbols');
            $table->string('identifier')->after('title')->nullable();
        });

        Schema::create('definition_items', function (Blueprint $table) {
            $table->id();
            $table->string('sub_title')->nullable();
            $table->string('type')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();

        });

        Schema::create('definition_item_sics', function (Blueprint $table) {
            $table->id();
            $table->integer('definition_id')->nullable();
            $table->integer('definition_item_id')->nullable();
            $table->string('sic')->nullable();
            $table->timestamps();


        });

        Schema::create('definition_item_symbols', function (Blueprint $table) {
            $table->id();
            $table->integer('definition_id')->nullable();
            $table->integer('definition_item_id')->nullable();
            $table->string('symbol')->nullable();
            $table->timestamps();


        });

        Schema::create('definition_item_tags', function (Blueprint $table) {
            $table->id();
            $table->integer('definition_id')->nullable();
            $table->integer('definition_item_id')->nullable();
            $table->string('keyword')->nullable();
            $table->timestamps();


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('definitions', function (Blueprint $table) {
            $table->dropColumn('identifier');
            $table->string('key')->nullable();
            $table->string('type')->nullable();
            $table->text('definition')->nullable();
            $table->text('tags')->nullable();
            $table->text('sics')->nullable();
            $table->text('symbols')->nullable();
        });

        Schema::dropIfExists('definition_items');
        Schema::dropIfExists('definition_item_sics');
        Schema::dropIfExists('definition_item_symbols');
        Schema::dropIfExists('definition_item_tags');
    }
}
