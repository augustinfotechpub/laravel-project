<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * "symbol" : "ABT",
     * "name" : "Abbott Laboratories",
     * "sector" : "Health Care",
     * "subSector" : "Health Care Equipment",
     * "headQuarter" : "North Chicago, Illinois",
     * "dateFirstAdded" : "1964-03-31",
     * "cik" : "0000001800",
     * "founded" : "1888"
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('symbol');
            $table->string('cik');
            $table->string('name');
            $table->string('sector');
            $table->string('subSector');
            $table->string('headQuarter');
            $table->string('dateFirstAdded')->nullable();
            $table->string('founded')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp500_companies');
    }
}
