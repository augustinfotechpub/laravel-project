<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function (Blueprint $table) {
            $table->id();

            $table->string('age')->nullable();


            $table->string('full_address')->nullable();
            $table->string('fav_feature')->nullable();
            $table->string('which_feature')->nullable();
            $table->string('why_not')->nullable();
            $table->string('for_living')->nullable();
            $table->string('not_fav_feature')->nullable();
            $table->string('which_feature_not')->nullable();
            $table->string('why_all')->nullable();
            $table->string('rating')->nullable();
            $table->string('pay_for_app')->nullable();
            $table->string('how_much')->nullable();

            $table->string('no_money')->nullable();
            $table->string('annually_amount')->nullable();
            $table->string('list_function_n')->nullable();
            $table->string('list_function_add')->nullable();

            $table->string('is_helpfull')->nullable();
            $table->string('how_much_helpful')->nullable();
            $table->string('not_helpfull')->nullable();
            $table->string('easy_to_use')->nullable();

            $table->string('why_easy')->nullable();
            $table->string('why_not_easy')->nullable();
            $table->string('how_many_exp')->nullable();
            $table->string('how_many_exp_in_stock')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey');
    }
}
