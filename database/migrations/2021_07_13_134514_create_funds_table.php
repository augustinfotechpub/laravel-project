<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funds', function (Blueprint $table) {
            $table->id();
            $table->string('company_cik');
            $table->string('company_ticker');
            $table->string('holder_name')->nullable();
            $table->string('date')->nullable();
            $table->string('totalShares')->nullable();
            $table->string('totalAssets')->nullable();
            $table->string('currentShares')->nullable();
            $table->string('change')->nullable();
            $table->string('change_p')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funds');
    }
}
