<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnershipinterestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ownershipinterest', function (Blueprint $table) {
            $table->id();
            $table->string('company_cik');
            $table->string('company_ticker');
            $table->string('holder_name')->nullable();
            $table->string('holder_date')->nullable();
            $table->string('totalShares')->nullable();
            $table->string('totalAssets')->nullable();
            $table->string('currentShares')->nullable();
            $table->string('change')->nullable();
            $table->string('change_p')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ownershipinterest');
    }
}
