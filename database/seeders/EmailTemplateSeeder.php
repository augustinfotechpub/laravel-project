<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_templates')->truncate();

        DB::table('email_templates')->insert([
            'title' => 'User Forgot Password',
            'subject' => 'Forgot Password',
            'content' => view('emails.forgot_password'),
            'action' => 'user_forgot_password',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('email_templates')->insert([
            'title' => 'Admin Forgot Password',
            'subject' => 'Forgot Password',
            'content' => view('emails.forgot_password'),
            'action' => 'admin_forgot_password',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('email_templates')->insert([
            'title' => 'User Email Verification	',
            'subject' => 'Email Verification',
            'content' => view('emails.email_verification'),
            'action' => 'user_email_verification',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
