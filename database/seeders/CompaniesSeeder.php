<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Services\FinancialModelingPrep;
use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    protected $fmp;

    public function __construct(FinancialModelingPrep $fmp)
    {
        $this->fmp = $fmp;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::query()->truncate();
        $companies = $this->fmp->getCompanies();
        foreach ($companies as $company) {
            try {
                Company::query()->updateOrCreate(['symbol' => $company['symbol']], $company);
            } catch (\Exception $e) {

            }
        }
    }
}
