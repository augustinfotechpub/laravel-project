<?php

namespace Database\Seeders;

use App\Services\SecEdgarService;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SICSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sic = DB::table('sic');
        $sic->truncate();
        $sec = new SecEdgarService();
        $sic = $sec->getSicData();
        foreach ($sic as $item) {
            DB::table('sic')->insert([
                'sic_code' => $item['sic'],
                'office' => strtolower($item['office']),
                'title' => strtolower($item['title']),
            ]);
        }
    }
}
