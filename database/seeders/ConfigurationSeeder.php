<?php

namespace Database\Seeders;

use App\Models\SiteConfiguration;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admins = [
            [
                'id' => 1,
                'first_name' => 'Malay',
                'last_name' => 'Mehta',
                'email' => 'malay@urvam.com',
                'password' => bcrypt('urvaa1205'),
                'phone_number' => "9897959685",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        $site_configurations = [

            [
                'title' => 'Business Email Address',
                'identifier' => 'BUSINESS_EMAIL_ADDRESS',
                'value' => 'info@finnfrastructure.com',
            ],

            [
                'title' => 'Business Phone Number',
                'identifier' => 'BUSINESS_PHONE_NUMBER',
                'value' => '907-323-6002',
            ],

            [
                'title' => 'Address',
                'identifier' => 'ADDRESS',
                'value' => '4797  Blackwell Street',
            ],

            [
                'title' => 'City',
                'identifier' => 'CITY',
                'value' => 'Dry Creek',
            ],
            [
                'title' => 'State',
                'identifier' => 'STATE',
                'value' => 'Alaska',
            ],
            [
                'title' => 'Postal Code',
                'identifier' => 'POSTAL_CODE',
                'value' => '99737',
            ],
            [
                'title' => 'Country',
                'identifier' => 'COUNTRY',
                'value' => 'USA',
            ],
            [
                'title' => 'Welcome Text',
                'identifier' => 'WELCOME_TEXT',
                'value' => '',
            ],
            [
                'title' => 'Welcome video file',
                'identifier' => 'WELCOME_VIDEO_FILE',
                'value' => '',
            ],
//            [
//                'title' => 'Latitude',
//                'identifier' => 'LATITUDE',
//                'value' => 27.994402,
//            ],
//            [
//                'title' => 'Longitude',
//                'identifier' => 'LONGITUDE',
//                'value' => -81.760254,
//            ],
        ];
        SiteConfiguration::query()->truncate();
        Schema::disableForeignKeyConstraints();
        // Inserting Data into the Admins Tables
        $admins_table = DB::table('admins');
        $admins_table->truncate();
        $admins_table->insert($admins);

        // Inserting Data into the Configuration Tables
        $site_configuration_table = DB::table('site_configurations');
        $site_configuration_table->truncate();
        $site_configuration_table->insert($site_configurations);
        Schema::enableForeignKeyConstraints();
    }
}
