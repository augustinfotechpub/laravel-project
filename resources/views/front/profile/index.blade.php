@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Profile</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->
        <section id="contact" class="contact">
            <div class="container-xl" data-aos="fade-up">
                <div class="section-title">
                    <h2>Profile</h2>
                </div>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home"
                           role="tab"
                           aria-controls="home" aria-selected="true">Profile Info</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab"
                           aria-controls="profile" aria-selected="false">Subscription Info</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    @include('front.profile.partials.profile-info')
                    @include('front.profile.partials.subscription-info')
                </div>
            </div>
        </section><!-- End Contact Section -->
        @include('front.profile.partials.cancelation-survey')

    </main><!-- End #main -->

@endsection


