@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('user.profile') }}">Profile</a></li>
                    <li>Edit</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->
        <section id="contact" class="contact">
            <div class="container" data-aos="fade-up">

                <div class="section-title">
                    <h2>Update Profile</h2>
                </div>
                    <div class="card alert-primary">
                        <div class="card-header card-header-bg text-light">
                            <h4 class="mb-0">Edit Profile</h4>
                        </div>
                        <div class="card-body pb-0">
                            <form class="row" action="{{ route('user.profile.update') }}" method="post">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $profile->id }}">
                                <input type="hidden" name="email" value="{{ $profile->email }}">
                                   <div class="row g-2">
                                       <div class="col-md-6">
                                           <label for="first_name" class="form-label"><b>First Name</b></label>
                                           <input type="text" class="form-control" id="first_name" name="first_name"
                                                  value="{{ $profile->first_name }}">
                                       </div>
                                       <div class="col-md-6">
                                           <label for="last_name" class="form-label"><b>Last Name</b></label>
                                           <input type="text" class="form-control" id="last_name" name="last_name"
                                                  value="{{ $profile->last_name }}">
                                       </div>
                                   </div>
                                   <div class="row g-2">
                                       <div class="col-md-6">
                                           <label for="email" class="form-label"><b>Email</b></label>
                                           <input type="email" class="form-control" id="email" name="email"
                                                  value="{{ $profile->email }}" disabled>
                                       </div>
                                       <div class="col-md-6">
                                           <label for="phone_number" class="form-label"><b>Phone Number</b></label>
                                           <input type="text" class="form-control" id="phone_number" name="phone_number"
                                                  value="{{ $profile->phone_number }}">
                                       </div>
                                   </div>
                                   <div class="row g-2">
                                       <div class="col-md-6">
                                           <label for="address_line1" class="form-label"><b>Address Line 1</b></label>
                                           <input type="text" class="form-control" id="address_line1" name="address_line1"
                                                  value="{{ $profile->address_line1 }}">
                                       </div>
                                       <div class="col-md-6">
                                           <label for="address_line2" class="form-label"><b>Address Line 2</b></label>
                                           <input type="text" class="form-control" id="address_line2" name="address_line2"
                                                  value="{{ $profile->address_line2 }}">
                                       </div>
                                   </div>
                                   <div class="row g-2">
                                       <div class="col-md-3">
                                           <label for="city" class="form-label"><b>City</b></label>
                                           <input type="text" class="form-control" id="city" name="city"
                                                  value="{{ $profile->city }}">
                                       </div>
                                       <div class="col-md-3">
                                           <label for="state" class="form-label"><b>State</b></label>
                                           <input type="text" class="form-control" id="state" name="state"
                                                  value="{{ $profile->state }}">
                                       </div>
                                       <div class="col-md-3">
                                           <label for="country" class="form-label"><b>Country</b></label>
                                           <input type="text" class="form-control" id="country" name="country"
                                                  value="{{ $profile->country }}">
                                       </div>
                                       <div class="col-md-3">
                                           <label for="zip_code" class="form-label"><b>Zip Code</b></label>
                                           <input type="text" class="form-control" id="zip_code" name="zip_code"
                                                  value="{{ $profile->zip_code }}">
                                       </div>
                                   </div>

                                   <div class="col-12 card-footer text-end mt-3">
                                       <button type="submit" class="btn btn-primary btn-sm py-2 px-4">
                                           <i class="ri-refresh-line mr-2"></i> Update
                                       </button>
                                   </div>
                            </form>
                        </div>
                    </div>
            </div>
        </section><!-- End Contact Section -->
    </main><!-- End #main -->

@endsection
@section('scripts')
    <script>
        $("#phone_number").mask("(999) 999-9999", {autoclear: true});
    </script>
@endsection


