<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Plan Name</th>
            <th scope="col">Start Date</th>
            <th scope="col">End Date</th>
            <th scope="col">Price</th>
            <th scope="col">Status</th>
            <th scope="col">Cancel</th>

        </tr>
        </thead>
        <tbody>
        @if (isset($subscription))
            @foreach ($subscription as $item)
            <tr>
                <td>{{ $item->title }}</td>
                <td>{{ formatDate($item->start_date) }}</td>
                <td>{{ formatDate($item->end_date) }}</td>
                <td>{{ formatPrice($item->price) }}</td>
                <td>{{ ucfirst($item->status )}}</td>
                @if (isset($item->status) && $item->status == 'active')
                    <td><button class="btn btn-sm btn-primary" type="button" id="cancel-survey-btn">Cancel subscription </button></td>
                @else
                    <td></td>
                @endif
            </tr>    
            @endforeach
            
        @endif
        </tbody>
    </table>    
</div>
