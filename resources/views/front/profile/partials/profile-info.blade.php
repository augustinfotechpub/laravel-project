<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div class="card alert-primary">
        <div class="card-body pb-0">
            <dl class="row">
                <dt class="col-sm-3">Name</dt>
                <dd class="col-sm-9">
                    {{$profile->full_name}}
                </dd>

                <dt class="col-sm-3">Email</dt>
                <dd class="col-sm-9">
                    {{$profile->email}}
                </dd>

                <dt class="col-sm-3">Phone Number</dt>
                <dd class="col-sm-9">
                    {{$profile->phone_number}}
                </dd>

                <dt class="col-sm-3">Address</dt>
                <dd class="col-sm-9">
                    {{$profile->address_line1.', '.$profile->address_line2}}
                </dd>

                <dt class="col-sm-3">City</dt>
                <dd class="col-sm-9">
                    {{$profile->city}}
                </dd>

                <dt class="col-sm-3">State</dt>
                <dd class="col-sm-9">
                    {{$profile->state}}
                </dd>

                <dt class="col-sm-3">Country</dt>
                <dd class="col-sm-9">
                    {{$profile->country}}
                </dd>

                <dt class="col-sm-3">Zip Code</dt>
                <dd class="col-sm-9">
                    {{$profile->zip_code}}
                </dd>
            </dl>
        </div>
        <div class="card-footer text-end">
            <a href="{{ route('user.profile.edit') }}" class="btn btn-primary py-2 px-4 btn-sm">
                <i class="ri-edit-2-line me-2"></i>Edit Profile
            </a>
        </div>
    </div>
</div>




