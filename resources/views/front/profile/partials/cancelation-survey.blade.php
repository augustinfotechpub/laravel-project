    
<!-- The Modal -->
<div id="cancelSurveyModal" class="modal" role="dialog" aria-hidden="true" tabindex="-1">
    
  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Cancellation Survey Feedback</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <form action="/cancellation-survey" class="cancellation_survey_form" method="post">
      <div class="modal-body">
        {{ csrf_field() }}
        <div class="row g-2">
          <div class="col-md-12">

            @foreach ($cancelQuestion as $questionkey => $question)
              <label for="question_{{ $questionkey }}"><b> {{ $question }}</b></label><br>
              <input type="text" class="form-control" name="questions[]" id="question_{{ $questionkey }}" required="true">
              
            @endforeach

          </div>
          <div class="col-md-12 other_option">
            <label for="Message"><b></b></label><br>
	          <input type="text" id="other" name="other" class="form-control" placeholder="Anything else that you want to share.." />
          </div>
        </div>
        <br/>
          <input type="submit" class="btn btn-lg btn-primary" name="submit" value="Cancel Account" />
        <br/><br/>
      </div>
    </form>
  </div>
</div>