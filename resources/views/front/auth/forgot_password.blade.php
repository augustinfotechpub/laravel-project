@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Forgot Password</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->

        <section class="inner-page pt-0">
            <div class="container">
                <section id="contact" class="contact">
                    <div class="container" data-aos="fade-up">

                        <div class="section-title">
                            <h2>Forgot Password</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 mt-5 mt-lg-0 mx-auto">
                                <form action="{{ route('user.forgot_password.send') }}" method="post" role="form"
                                      class="php-email-form">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="Email address" value="{{old('email')}}" autofocus>
                                            @error('email')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-info text-white submit">Send Password Reset Link</button>
                                            <div class="mt-3">
                                                <a href="{{ route('user.login') }}">Back to login?</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </section>
    </main><!-- End #main -->
@endsection
