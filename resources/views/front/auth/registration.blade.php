@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Registration</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->
        <section class="inner-page py-0">
            <div class="container">
                <section id="contact" class="contact">
                    <div class="container" data-aos="fade-up">
                        <div class="section-title">
                            <h2>Registration</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 mx-auto mt-5 mt-lg-0">
                                <form action="{{ route('user.register.store') }}" method="post" role="form"
                                      class="php-email-form">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="first_name">First Name</label>
                                            <input type="text" name="first_name" class="form-control" id="first_name"
                                                   placeholder="First name" autofocus value="{{old('first_name')}}">
                                            @error('first_name')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" name="last_name" class="form-control" id="last_name"
                                                   placeholder="Last name" value="{{old('last_name')}}">
                                            @error('last_name')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="Email address" value="{{old('email')}}">
                                            @error('email')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                   placeholder="Password">
                                            @error('password')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="phone_number">Phone Number</label>
                                            <input type="text" class="form-control" name="phone_number"
                                                   id="phone_number"
                                                   placeholder="Phone number" value="{{old('phone_number')}}">
                                            @error('phone_number')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="address_line1">Address Line 1</label>
                                            <input type="text" class="form-control" name="address_line1"
                                                   id="address_line1" value="{{old('address_line1')}}">
                                            @error('address_line1')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="address_line2">Address Line 2</label>
                                            <input type="text" class="form-control" name="address_line2"
                                                   id="address_line2" value="{{old('address_line2')}}">
                                            @error('address_line2')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" name="city" id="city"
                                                   placeholder="City" value="{{old('city')}}">
                                            @error('city')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" name="state" id="state"
                                                   placeholder="State" value="{{old('state')}}">
                                            @error('state')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="country">Country</label>
                                            <input type="text" class="form-control" name="country" id="country"
                                                   placeholder="Country" value="{{old('country')}}">
                                            @error('country')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="zip_code">Zip Code</label>
                                            <input type="text" class="form-control" name="zip_code" id="zip_code"
                                                   placeholder="Zip Code" value="{{old('zip_code')}}">
                                            @error('zip_code')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <input type="submit" class="btn btn-primary text-white fw-bold px-5" value="Sign Up" style="border-radius:30px;background-color:#47b2e4;border-color:#47b2e4;"/>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </section><!-- End Contact Section -->
            </div>
        </section>
    </main><!-- End #main -->
@endsection
