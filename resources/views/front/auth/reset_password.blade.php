@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Reset Password</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->

        <section class="inner-page pt-0">
            <div class="container">
                <section id="contact" class="contact">
                    <div class="container" data-aos="fade-up">

                        <div class="section-title">
                            <h2>Reset Password</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 offset-4 mt-3 mt-lg-0 d-flex align-items-stretch">
                                <form action="{{ route('user.reset_password.update') }}" method="post" role="form"
                                      class="php-email-form">
                                    @csrf
                                    <input type="hidden" name="token" value="{{$token}}">
                                    <input type="hidden" name="email" value="{{$user->email}}">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <input type="text" value="{{$user->email}}" disabled class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                   placeholder="Password" autofocus>
                                            @error('password')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation"
                                                   placeholder="Confirm password">
                                            @error('password_confirmation')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-5">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn btn-info text-white submit">Reset Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </section>
    </main><!-- End #main -->
@endsection
