@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Login</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->

        <section class="inner-page pt-0">
            <div class="container">
                <section id="contact" class="contact">
                    <div class="container" data-aos="fade-up">

                        <div class="section-title">
                            <h2>Sign in</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 mt-5 mt-lg-0 mx-auto">
                                <form action="{{ route('user.authenticate') }}" method="post" role="form"
                                      class="php-email-form">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="Email address" value="{{old('email')}}" autofocus>
                                            @error('email')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                   placeholder="Password">
                                            @error('password')
                                            <small class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="px-5 py-2">Login</button>
                                        </div>
                                        <div class="col-md-12 text-center mt-3">
                                            <a href="{{ route('user.forgot_password.create') }}">forgot password?</a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </section>
    </main><!-- End #main -->
@endsection
