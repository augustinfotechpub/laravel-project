<nav class="navbar navbar-expand-xl navbar-light alert-primary innerpage_menu fixed-top">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="innerpageMenu">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a href="{{ route('app_main',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app_main'?'active':'inactive' }}">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('app.key-parties',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.key-parties'?'active':'inactive' }}">Key
                        Parties</a></li>
                <li class="nav-item">
                    <a href="{{ route('app.financial-statements',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.financial-statements'?'active':'inactive' }}">
                        Financial Snapshot
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('app.charting',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.charting'?'active':'inactive' }}">
                        Charting / Technical Analysis
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('app.ratios',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.ratios'?'active':'inactive' }}">
                        Ratios
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('app.financial-model',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.financial-model'?'active':'inactive' }}">
                        Financial Models
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('app.sec-fillings',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.sec-fillings'?'active':'inactive' }}">
                        Regulatory Fillings
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('app.calendar',['symbol'=>$symbol]) }}"
                       class="nav-link {{ currentRouteName() == 'app.calendar'?'active':'inactive' }}">
                        Calendar
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a href="{{ route('user.register') }}" class="nav-link">
                        My Account
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
