<div class="tradingview-widget-container">
    <div id="tradingview_3c9bc"></div>
    {{--<div class="tradingview-widget-copyright">
        <a href="https://in.tradingview.com/symbols/NASDAQ-{{getProfileSymbol()}}/"
           rel="noopener" target="_blank"><span class="blue-text">{{getProfileSymbol()}} Chart</span></a>
    --}}</div>
<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
<script type="text/javascript">
    new TradingView.widget(
        {
            "width": 'auto',
            "height": 'auto',
            "symbol": "{{ getProfileSymbol() }}",
            "timezone": "Etc/UTC",
            "theme": "light",
            "style": "1",
            "locale": "in",
            "toolbar_bg": "#f1f3f6",
            "enable_publishing": false,
            "withdateranges": true,
            "range": "YTD",
            "hide_side_toolbar": false,
            "allow_symbol_change": true,
            "details": true,
            "hotlist": true,
            "calendar": true,
            "container_id": "tradingview_3c9bc"
        }
    );
</script>
</div>
