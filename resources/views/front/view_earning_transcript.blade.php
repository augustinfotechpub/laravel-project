<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Finnfrastructure - Stock Analysis Application</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <!-- Favicons -->
    <link href="{{ asset('front/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('front/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @include('front.layouts.partials.css')
</head>
<body>
@php
    $profile =  session('profile');
@endphp
<div class="container">
    <div class="row mt-2">
        <div class="col-md-12">
            @if(isset($earningCallTranscript['content']))
                <div class="card bg-light mb-3">
                    <div class="card-header" style="background-color: #283a5a !important;color: #b9c7dc;"><b>{{ $profile['companyName'].' ('.$profile['symbol'].') CEO '.$profile['ceo'].' On Q'.$earningCallTranscript['quarter'].' '.$earningCallTranscript['year'].' Results - Earnings Call Transcript' }}</b></div>
                    <div class="card-body" style="background-color: #e6eaf1;">
                        <div class="row">
                            <div class="col-md-4">
                                <p class="card-title"><b>Company: </b>{{ $profile['companyName'] }}</p>
                                <p class="card-title"><b>CEO: </b>{{ $profile['ceo'] }}</p>
                                <p class="card-title"><b>Price: </b>{{ formatPrice($profile['price']) }}</p>
                                <p class="card-title"><b>Website: </b><a href="{{ $profile['website'] }}" target="_blank" class="text-dark">{{ $profile['website'] }}</a> </p>
                            </div>
                            <div class="col-md-4">
                                <p class="card-title"><b>Symbol: </b>{{ $profile['symbol'] }}</p>
                                <p class="card-title"><b>Quarter: </b>{{ $earningCallTranscript['quarter'] }}</p>
                                <p class="card-title"><b>Year: </b>{{ $earningCallTranscript['year'] }}</p>
                                <p class="card-title"><b>Date: </b>{{ formatDate($earningCallTranscript['date']) }}</p>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="card" style="max-height: 100%">
                    <div class="card-header" style="background-color: #283a5a !important;color: #b9c7dc;"><b>Q{{ $earningCallTranscript['quarter'] }}: {{ \Carbon\Carbon::parse($earningCallTranscript['date'])->format('Y-m-d') }} Earnings Summary</b></div>
                    @foreach($earningCallTranscript['content'] as $content)
                        <div class="card-body" style="background-color: #e6eaf1;">
                            <h5 class="card-title">{{ $content['speaker'] }}:</h5>
                            <p class="card-text transcript-heading">{{ $content['speech'] }}</p>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>

@stack('scripts')
</body>
</html>
