    <script
        src="https://checkout.stripe.com/checkout.js"
        class="stripe-button submit-button"
        data-key="{{ env('STRIPE_PUBLISHABLE_KEY') }}"
        data-amount="{{ $current_plan['price']*100 }}"
        data-name="{{ ucfirst($current_plan->title) }}"
        data-description="{{ $current_plan->title }} | $ {{ $current_plan->price }}"
        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
        data-locale="auto"
        data-currency="usd">
    </script>
