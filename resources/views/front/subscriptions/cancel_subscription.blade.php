@extends('front.layouts.master')
@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="subscription text-left">
                <h5>Subscription</h5>
            </div>
            <form action="/user/subscription/page/submit" class="page" method="post">
                @csrf
                <input type="submit" id="btn" class="btn btn-lg btn-primary" name="submit" value="Cancel subscription"/>
                
            </form>
        </div>
    </div> 
</div>

@endsection