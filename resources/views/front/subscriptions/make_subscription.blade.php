@extends('front.layouts.master')
@section('styles')
    <style>
        .fixed-top {
            background-color: rgba(40, 58, 90, 0.9);
        }
    </style>
@endsection
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Make Subscription</li>
                </ol>
            </div>
        </section><!-- End Breadcrumbs -->
        <div>

        </div>
        <section class="inner-page py-0">
            <div class="container">
                <section id="contact" class="contact">
                    <div class="container" data-aos="fade-up">

                        <div class="section-title">
                            <h2>Choose your subscription</h2>
                        </div>

                        <div class="row">
                            <div class="col-lg-10 offset-1 mt-5 mt-lg-0 d-flex align-items-stretch">
                                <form action="{{ route('user.subscription.store') }}" method="post" role="form"
                                      class="php-email-form">
                                    @csrf
                                    <div class="row">
                                        @foreach($plans as $plan)
                                            <label for="plan-{{ $plan->id }}" class="col-md-3 price-box">
                                                <div class="card alert-primary" id="sub-{{ $plan->id }}">
                                                    <div class="card-body">
                                                        <div class="row mx-auto g-0 align-items-center">
                                                            <div class="col-1">
                                                                <input type="radio" id="plan-{{ $plan->id }}"
                                                                       name="plan"
                                                                       value="{{ $plan->id }}" {{ $loop->first?'checked':null }}>
                                                            </div>
                                                            <div class="col-10 m-auto">
                                                                <h5 class="card-title">{{ $plan->title }}</h5>
                                                            </div>
                                                        </div>
                                                        <p class="card-text">
                                                            <span>Duration: {{ $plan->free_trial_period.' '.$plan->free_trial_unit}}</span>
                                                        </p>
                                                        <h5 class="card-text">{{ formatPrice($plan->price) }}</h5>
                                                    </div>
                                                </div>
                                            </label>
                                        @endforeach
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-12 text-center" id="checkout-button">
                                            @include('front.subscriptions.partials.checkout-button',['current_plan'=>$current_plan])
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </section><!-- End Contact Section -->
            </div>
        </section>
    </main><!-- End #main -->
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#phone_number").mask("(999) 999-9999", {autoclear: true});
            $(document).on('click', '.price-box', function () {
                updatePlan($(this).find('input').val());
            });
        });
        function updatePlan(plan_id) {
            var url = '{{ route('user.subscription.chang_plan') }}';
            $.ajax({
                type: "get",
                url: url,
                data: {'plan_id': plan_id},
                success: function (data) {
                    console.log('success');
                    $('#checkout-button').html(data.html);
                    $('.price-box').find('.card').removeClass('alert-success');
                    $('.price-box').find('.card').addClass('alert-primary');
                    $("#sub-" + plan_id).removeClass('alert-primary');
                    $("#sub-" + plan_id).addClass('alert-success');
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
    </script>
@endsection
