<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Finnfrastructure - Stock Analysis Application</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('front/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('front/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @include('front.layouts.partials.css')
    @yield('styles')
    <livewire:styles/>
    @stack('styles')
</head>
<body>

@include('front.layouts.partials.header')
@include('front.layouts.partials.flash_messages')
@yield('content')
<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
{{--<div id="preloader"></div>--}}
@include('front.layouts.partials.footer')
@include('front.layouts.partials.js')

</body>
</html>
