<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} | Stock Analysis Application</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('front/assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('front/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @include('front.layouts.partials.css')
    @yield('css')
    <livewire:styles/>
    @stack('styles')
</head>
<body>
@include('front.layouts.partials.innerpage_header')
@include('front.layouts.partials.flash_messages')
{{--@yield('content')--}}
<livewire:index/>
@include('front.layouts.partials.footer')
<a href="javascript:void(0);" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
{{--<div id="preloader"></div>--}}

@include('front.layouts.partials.js')
</body>
</html>
