<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="container-fluid footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright {{ date("Y") }} - <strong><span>Finnfrastructure</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            
        </div>
    </div>
</footer><!-- End Footer -->
<div id="loader-x" class="d-none">
    <div className="d-flex justify-content-center align-items-center mt-5">
        <img src="{{ asset('front/img/spinner.svg') }}" alt="Loading Component">
    </div>
</div>
