<div class="row">
    <div class="col-md-12">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block mt-2">
                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block mt-2">
                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">×</button>
                @if ($errors->any())
                    {{ implode('', $errors->all('<div>:message</div>')) }}
                @endif
                <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block mt-2">
                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif


        @if ($message = Session::get('info'))
            <div class="alert alert-info alert-block mt-2">
                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('flash_message'))
            <div class="alert alert-success alert-block mt-2">
                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif 
    </div>
</div>
