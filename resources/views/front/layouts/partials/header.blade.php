<!--======= Header =======-->
<header id="header" class="sticky-top">
    <div class="container-lg d-flex align-items-center">

        <a href="{{ route('home') }}">
            <h4 class="logo me-auto text-white">Finnfrastructure</h4>
            <small class="logo-sub-txt text-white">Knowledge Infrastructure for Finance and Markets</small>
        </a>

        <nav class="nav-menu d-none d-lg-block ms-auto">
            <ul>
                <!-- <li class="active"><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('home',['section_id'=>'#about']) }}">About</a></li>
                <li><a href="{{ route('home',['section_id'=>'#services']) }}">Services</a></li>
                <li><a href="{{ route('home',['section_id'=>'#contact']) }}">Contact</a></li> -->
                <li class="d-lg-none account-mobile-Menu">
                    <h6 class="account-mobile-Menu-link" data-bs-target="#accountMenu" data-bs-toggle="collapse" aria-expanded="false">
                        @if (auth()->check())
                            {{auth()->user()->full_name}}
                        @else
                            Get Started
                        @endif
                        <i class="ri-arrow-down-s-line"></i>
                    </h6>
                    <div class="collapse" id="accountMenu">
                        @if (auth()->check())
                            <div><a class="text-dark small" href="{{route('user.profile')}}">My Profile</a></div>
                            <div><a class="text-dark small" href="{{route('user.profile.edit')}}">Edit Profile</a></div>
                            <div><a class="text-dark small" href="{{ route('user.logout') }}">Logout</a></div>
                        @else
                            <div><a class="text-dark small" href="{{ route('user.login') }}">Login</a></div>
                            <div><a class="text-dark small" href="{{ route('user.register') }}">Register</a></div>
                        @endif
                    </div>
                </li>
            </ul>
        </nav><!-- .nav-menu -->
        <div class="dropdown d-none d-lg-inline-block">
            <a href="" class="get-started-btn scrollto" id="dropdownMenuButton2" data-bs-toggle="dropdown"
               aria-expanded="false">
                @if (auth()->check())
                    {{auth()->user()->full_name}}
                @else
                    Get Started
                @endif</a>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                @if (auth()->check())
                    <li><a class="dropdown-item" href="{{route('user.profile')}}">My Profile</a></li>
                    <li><a class="dropdown-item" href="{{route('user.profile.edit')}}">Edit Profile</a></li>
                    <li><a class="dropdown-item" href="{{ route('user.logout') }}">Logout</a></li>
                    <li><a class="dropdown-item" href="https://player.vimeo.com/external/181445574.hd.mp4?s=d24f32d879305e931468d55e4d7ce6efb5a95c39&amp;profile_id=119">Videos</a></li>
                   <!-- <li><a class="dropdown-item" href="{{ route('user.logout') }}">Videos</a></li>-->
                @else
                    <li><a class="dropdown-item" href="{{ route('user.login') }}">Login</a></li>
                    <li><a class="dropdown-item" href="{{ route('user.register') }}">Register</a></li>
                @endif


            </ul>
        </div>
    </div>
</header>
<!--======= End Header =======-->
