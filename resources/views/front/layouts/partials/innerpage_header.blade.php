<!-- ======= Header ======= -->
<header id="header" class="sticky-top">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-4 col-8 pe-0">
                <a href="{{ route('home') }}" class="d-table">
                    <h4 class="logo me-auto text-white">Finnfrastructure</h4>
                    <small class="logo-sub-txt text-white">Knowledge Infrastructure for Finance and Markets</small>
                </a>
            </div>
            <div class="col-lg-3 col-3 order-lg-3 text-end">
                @if(auth()->check())
                    @if(auth()->user()->is_beta == "1" && getExpiryStatus() )
                       <div>
                            <span style="color: #fd9595;">Beta Expires in:</span> 
                            <span style="color: #fd9595;" id="expiryDate"></span>
                        </div>
                       <script type="text/javascript">
                            $(document).ready(function() {
                                countDownExpiry('{{ config("app.BetaExpiry")}}', '{{ getCurrentTime() }}');
                            });
                         </script>
                    @endif
                @endif
                <span class="d-none d-md-inline-block">
                    <a href="" class="get-started-btn scrollto" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                    @if (auth()->check())
                        {{auth()->user()->full_name}}
                    @else
                        Get Started
                    @endif</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                        @if (auth()->check())
                            <li><a class="dropdown-item" href="{{route('user.profile')}}">My Profile</a></li>
                            <li><a class="dropdown-item" href="{{route('user.profile.edit')}}">Edit Profile</a></li>
                            <li><a class="dropdown-item" href="{{ route('user.logout') }}">Logout</a></li>
                            <li><a class="dropdown-item" href="https://player.vimeo.com/external/181445574.hd.mp4?s=d24f32d879305e931468d55e4d7ce6efb5a95c39&amp;profile_id=119">Videos</a></li>

                        @else
                            <li><a class="dropdown-item" href="{{ route('user.login') }}">Login</a></li>
                            <li><a class="dropdown-item" href="{{ route('user.register') }}">Register</a></li>
                        @endif
                    </ul>
                </span>  
                <button class="btn d-xl-none" id="menuButton" type="button" data-bs-toggle="collapse" data-bs-target="#innerpageMenu"
                        aria-expanded="false" style="outline: none;box-shadow:none;">
                    <i class="ri-menu-line"></i>
                    <i class="ri-close-line"></i>
                </button>
            </div>
            <div class="col-lg-5 mt-sm-4 mt-1 mt-lg-0 innerpage-search">
                <livewire:search-bar/>
            </div>
        </div>
    </div>
</header><!-- End Header -->
