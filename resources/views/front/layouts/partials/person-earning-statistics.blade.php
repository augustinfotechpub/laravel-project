<!-- Modal -->
<div wire:ignore.self class="modal fade" id="getPersonEarningStatistics" tabindex="-1"
     aria-labelledby="getPersonEarningStatistics" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="getPersonEarningStatistics">Person Earning Statistics</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                {{--<div id="getPersonEarningStatisticsChart"></div>--}}
                <canvas id="getPersonEarningStatisticsChart" height="350" width="750"></canvas>
                <div class="d-flex justify-content-center pec d-none">
                    <div class="spinner-border" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
