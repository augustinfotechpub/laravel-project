<!-- Vendor JS Files -->
<script src="//code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
<script src="{{ asset('front/assets/vendor/bootstrap/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('front/assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<script src="//cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>

<script src="{{ asset('front/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('front/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
{{--<script src="{{ asset('front/assets/vendor/php-email-form/validate.js') }}"></script>--}}
<script src="{{ asset('front/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('front/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('front/assets/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('front/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('front/assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('front/lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

{{-- Chart Js Resource --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script src="//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script src="//unpkg.com/lightweight-charts/dist/lightweight-charts.standalone.production.js"></script>
<script type="text/javascript" src="//s3.tradingview.com/tv.js"></script>

{{-- Heicharts Js--}}
<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>
<script src="//code.highcharts.com/modules/export-data.js"></script>
<script src="//code.highcharts.com/modules/accessibility.js"></script>

<script src="{{ asset('plugins/tiny-tour-master/dist/tour.min.js') }}"></script>

<script src="//unpkg.com/lightweight-charts@3.2.0/dist/lightweight-charts.standalone.production.js"></script>

@yield('scripts')
@stack('scripts')
<livewire:scripts/>
<script src="{{ asset('js/app.js') }}"></script>
<!-- Alpine.js -->
<script src="//cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<!-- Template Main JS File -->
<script src="{{ asset('front/assets/js/main.js') }}"></script>
<script>
    var $app_url = "{{ url('/') }}";
</script>

<!-- Development -->
<script src="//unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
<script src="//unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>

<script src="{{ asset('plugins/edgar/show.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('front/js/custom.js') }}"></script>

<script>
    var profile = @json(session('profile'));
</script>
<script src="{{ asset('front/js/afterStack.js') }}"></script>
