<div class="row">
    <div class="col-md-12">
        <div class="card scrollable-card-body mh-400 shadow">
            @foreach($res['content'] as $content)
                <div class="card-body">
                    <h5 class="card-title">{{ $content['speaker'] }}:</h5>
                    <p class="card-text transcript-heading">{{ $content['speech'] }}</p>
                </div>
            @endforeach
        </div>
    </div>
</div>
