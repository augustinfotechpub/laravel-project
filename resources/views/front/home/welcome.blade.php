@extends('front.layouts.master')
@section('content')
<style>
    .customSize p{
        font-size: 1.25rem !important;
    }
</style>
        <main id="main">            
                <section id="cliens" class="" style="overflow:visible;">
                    <div class="container">
                        <div class="row customSize text-center">
                            
                                <center><b>DO NOT SKIP THIS PAGE - READ IT</b></center><br>

<p>In 1936, John Maynard Keynes, a well-known British economist, used the concept of a beauty contest to explain the fluctuations in the stock market.  The contest was a newspaper contest, whereby entrants were asked to select the six prettiest faces from 100 pictures.  The participants who selected the most popular faces were eligible to receive a prize.  In this contest, Keynes indicated that the naive strategy would be to select the faces which you believe to be the most attractive based on your own perception of beauty.  A more sophisticated entrant would think about what the majority perception of beauty is currently, and make a decision on the faces to select based on inference from the knowledge of public perceptions.  This process is logical, because, after all, the goal is to win the prize. The primary question here is how does one ascertain or determine beauty when the concept has not been clearly defined.</p>
<br /><br />

<p>This platform has been designed to help you ascertain beauty, in a general sense.  The goal is to organize your thoughts around value-creating concepts that are commonly used by businesses that issue stock in public markets.  Once you’re able to anchor your thoughts to value-creating concepts, you’ll be able to apply these concepts across sectors and industries in order to understand what drives the markets in which these companies operate.  It’s difficult to understand how markets function before understanding the thought processes, tools, and techniques of market participants.</p>
<br /><br />
<p>Recent research has indicated that the best-performing 4% of publicly traded companies explain the net gain for the entire U.S. stock market since 1926.  This means that ascertaining beauty has enormous potential with respect to potential market returns.  Please also remember that ascertaining beauty is a process.  If you have only recently become interested in learning more about value creation in markets, then the process will be difficult at the beginning.  You will certainly know more about these value-creating principles at the end of this testing period than you did before it.  You will also more than likely have more questions at the conclusion of the beta testing period than you did at the beginning, and this is completely normal. As in any endeavor, learning is a process.</p>
<br /><br />
<p>Registration for the beta test will remain open until the test concludes.  If you know of someone that has an interest in learning more about value creation in public markets,  please have them create an account in order to ensure that all functions of the application are working properly.  Also, please make a list of the defects that you find within this application.  You will have an opportunity to list them when the testing period concludes.</p>
<br /><br />
<p>At this stage of development, there are some limitations with respect to data presentation.  This problem will be easily fixed shortly after the non-beta version is released.  Also, the concepts presented are not exclusive to the companies to which they are attached.  The concepts can be applied to various companies across various points in their development cycles.</p>
<br /><br />
<p>Please don’t attempt to access the application via a mobile device, as the experience on mobile devices will more than likely be severely diminished.</p>
<br /><br />
<p style="text-align: left;">Lastly, we propose an agreement.  In exchange for access to this platform, you agree to diligently complete the survey at the conclusion of the testing period.  Please check the box below if you agree to these terms. Please indicate this wish by clicking the link below.</p><br>
<br /><br />
<p>Thank you for your participation, and we look forward to reviewing your comments at the conclusion of the beta testing period.</p><br/><br/>

<p><b>Jamall Broussard</b></p>
                            
                        </div>
                    </div>
                </section>

            @if( $welcome_video != "")
                <section class="" style="overflow:visible;padding-top: 0;">
                    <div class="container">
                        <div class="row text-center">
                            
                            <video width="200" controls>
                                <source src="{{env('APP_URL')}}/{{$welcome_video}}" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                </section>
            @endif

            <section id="cliens" class="" style="overflow:visible;padding-top: 5px;">
                <div class="container">
                    <div class="row customSize text-center">
                        <a href="{{ route('checkbox.submit', '1') }}">I agree to diligently complete the survey at the conclusion the testing period</a>
                    </div>
                </div>
            </section>
        </main><!-- End #main -->
@endsection