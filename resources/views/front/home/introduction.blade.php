@extends('front.layouts.master')
@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if($user = Session::get('user') )
@endif
<main id="main">
    <section id="contact" class="contact">
        <div class="container aos-init aos-animate" data-aos="fade-up">
            <div class="section-title">
                <h2>WELCOME</h2>
            </div>
        <div class="card alert-primary">
            <div class="card-header card-header-bg text-light">
                <h4 class="mb-0">Why choose FINNFRASTRUCTURE ?</h4>
            </div>
            <div class="card-body pb-0">
                <form action="/user/introduction/submit" class="introduction" method="post">
                    {{ csrf_field() }}
                    <div class="row g-2">
                        <div class="col-md-12">
                            <p><b>Welcome to Finnfrastructure.  We are an educational technology company.  Our goal is to facilitate and accelerate your understanding of how companies create value in publicly traded equity (stock) markets.  After spending time on our platform, you will enhance your understanding of value-creating concepts.  This will allow you to develop confidence in your analytical abilities as you look to participate in the capital markets. </b></p> 
                            <p><b>
                                Our slogan is knowledge infrastructure for finance and markets.  Knowledge is relevant because it is your only defense against turbulent markets.  Infrastructure is important because it allows the flow of information which allows you to learn and grow in your journey as an investor.
                            </b></p>
                            <p><b>When money is made in an investment, the investor either knew what they were doing or got lucky.  Sometimes, the outcome is a result of a combination of the two, but at this stage, it’s better to focus on outcomes that are a bit more tangible.  It’s possible to invest ten thousand dollars and make four million in three years, pay your taxes, and move to Latin America or Southeast Asia.  This certainly happens, although much less frequently than people believe.  While it’s possible, the more likely option is that you will have to develop some repeatable process that will enable you to build wealth over time in order to achieve your goals.  This is Finnfrastructure’s purpose.  The goal is to help you anchor your thoughts around value-creating concepts, which facilitates learning and understanding.  The sooner you realize that this is the path with a higher degree of sustainability, the better off you will be.</b></p> 
                            <p><b> technology does the heavy lifting on your behalf, but you will have to apply the concepts that are presented in order to understand that particular company’s value-creating process.   After reviewing and thinking about the concepts, you’ll have a greater understanding of the business, how and why the concepts apply, and begin to develop opinions regarding management’s strategies and the execution of these strategies.  Learning isn’t an overnight process, but it also doesn’t have to be an Odyssey.</b></p>
                            <p><b>Happy Learning </b></p> 
                        </div>
                    </div>
                    
                    <div class="row g-2">
                        <div class="col-md-12 how_many_exp_in_stock">
                                <input type="checkbox" id="check" name="is_intro" value="1" onclick="enable()">
                                <label for="accept"><b>Thank you for reading the initial information and agreeing to the legal disclosure</b></label><br>
                                    <br>
                                    <input type="submit" id="btn" class="btn btn-lg btn-primary" name="submit" value="Continue" disabled="true" />
                                    @if(isset($user) && $user->id )
                                        <input type="hidden" name="userid" value="{{$user->id}}" />
                                    @endif
                                    <br><br>
                            </div>
                    </div> 
                </form>
            </div>
        </div>
        </div>
    </section>
    </main>
<script>
    function enable(){
        var check=document.getElementById("check");
        var btn=document.getElementById("btn");
        if(check.checked)
        {
            btn.removeAttribute("disabled");
        }
        else{
            btn.disabled="true";
        }
        }
    </script>  
@endsection
