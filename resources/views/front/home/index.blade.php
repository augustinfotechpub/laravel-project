@extends('front.layouts.master')
@section('content')
    <main id="main">
        <!-- ======= Cliens Section ======= -->
        <section id="cliens" class="cliens section-bg" style="overflow:visible;">
            <div class="container">
                <div class="row my-5">
                    <div class="col-lg-6 col-md-10 mx-auto livewire-search"{{-- @if(auth()->check())--}} id="search" {{--@endif--}}>
                        <livewire:search-bar/>
                    </div>
                </div>
            </div>
        </section><!-- End Clients Section -->
        

    </main><!-- End #main -->
@endsection