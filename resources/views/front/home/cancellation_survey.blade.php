
@extends('front.layouts.master')
@section('content')

<style>
.survey_form .row.g-2{
	margin-bottom: 20px;
}
.survey_form p{
	margin-bottom: 0;
}
.survey_form label {
    font-weight: bold;
}
</style>

<main id="main">
	<section id="contact" class="contact">
		<div class="container aos-init aos-animate" data-aos="fade-up">
			<div class="section-title">
				<h2>Cancellation Survey</h2>
			</div>
            <div class="card alert-primary">
                <div class="card-header card-header-bg text-light">
                    <h4 class="mb-0">Cancellation Survey</h4>
                </div>
                <div class="card-body pb-0">
                    <form action="/cancellation-survey" class="cancellation_survey_form" method="post">
                        {{ csrf_field() }}
                        <div class="row g-2">
                            <div class="col-md-12">
                                <input type="radio" class="good_fit" id="good_fit" name="action" value="YES">
                                <label for="What_made_you_ques_if_Finnfrastructure_was_good_fit_for_you"><b> What made you question if Finnfrastructure was a good fit for you?</b></label><br>
                                <input type="radio" class="decide_cancel" id="decide_cancel" name="action" value="YES">
                                <label for="When_did_you_decide_to_cancel"><b> When did you decide to cancel, and was there anything that prompted the decision?</b></label><br>
                                <input type="radio" class="service_used" id="service_used" name="action" value="YES">
                                <label for="What_service_are_you_using_now"><b> What service are you using now?</b></label><br>
                                <input type="radio" class="prevent_cancel" id="prevent_cancel" name="action" value="YES">
                                <label for="There_anything_we_could_have_done"><b> There anything we could have done that would have prevented you from canceling?</b></label><br>
                                <input type="radio" class="sign_again" id="sign_again" name="action" value="YES">
                                <label for="You_be_open_to_signing_up_again_if_your"><b> You be open to signing up again if your main issue was no longer an issue?</b></label><br>
                                <input type="radio" class="other" id="other" name="action" value="YES">
                                <label for="other"><b> Other</b></label><br>
                            </div>
                            <div class="col-md-12 other_option">
                                <label for="Message"><b></b></label><br>
	                   		    <input type="text" id="other" name="other" class="form-control" placeholder="Anything else that you want to share.." />
                            </div>
                        </div>
                        <br/>
                        <input type="submit" class="btn btn-lg btn-primary" name="submit" value="Cancel Account" />
                        <br/><br/>
                    </form>
                </div>
            </div>
		</div>
	</section>
</main>
@endsection