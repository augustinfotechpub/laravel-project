@extends('front.layouts.master')
@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style>
    .disclosure .row.g-2{
        margin-bottom: 20px;
    }
    .disclosure p{
        font-style: initial;
        margin-bottom: 0;
    }
    .disclosure label {
        font-weight: bold;
    }
    </style>
    @if($user = Session::get('user') )
    @endif
    <main id="main">
        <section id="contact" class="contact">
            <div class="container aos-init aos-animate" data-aos="fade-up">
                <div class="section-title">
                    <h2>Legal Disclosure</h2>
                </div>
            <div class="card alert-primary">
                
                <div class="card-body pb-0">
                    <form action="/user/disclosure/submit" class="disclosure" method="post">

                        <p><b>Finnfrastructure is an educational technology platform that enables interested parties to learn how value is created in publicly traded equity markets.  The information viewed here may not be correct and should be double-checked via additional sources, particularly company filings, to verify its accuracy.  This information alone should not be relied upon to make investment decisions. </b></p>
                       <br> <p><b>
                            Finnfrastructure is a new company, but data accuracy is a challenge across the financial education and financial data industry.  Professional investors pay upwards of two thousand dollars per month for financial data, and it’s still not one hundred percent accurate.  Finnfrastructure is no exception.  
                        </b></p><br>
                        <br/>
                        <p><b>The data, particularly the key concepts and industry information, are themselves new concepts in the industry.  The application of these concepts to individual-level stock analysis involves extrapolation, and the concepts may not be applied correctly based on technological constraints, especially at this stage of the company’s development.  View the data with caution as you would with any third-party service.</p></b>
                        <p><b><br>
                            Thank you 
                        </b> </p>
                        <br/>
                        {{ csrf_field() }}
                        <br>
            
                        <input type="checkbox" id="check" name="is_disclosure" value="1"  onclick="enable()">
                        <label for="accept"><b>Thank you for reading the initial information and agreeing to the legal disclosure</b></label><br>
                           <br />
                           <input type="submit" id="btn" class="btn btn-lg btn-primary" name="submit" value="Continue" disabled />
                           @if(isset($user) && $user->id )
                                <input type="hidden" name="userid" value="{{$user->id}}" />
                            @endif
                           <br/><br/>
                    </form>
                </div>
            </div>
            </div>
        </section>
    </main>
    <script>
        function enable(){
            var check=document.getElementById("check");
            var btn=document.getElementById("btn");
            if(check.checked)
            {
                btn.removeAttribute("disabled");
            }
            else{
                btn.disabled="true";
            }
            }
     </script>  
@endsection