@extends('front.layouts.master')
@section('content')

<style>
.which_feature, .why_not_feat, .why_not, .which_feature_not, .how_much, .no_money, .was_helpfull, .not_helpfull, .why_easy, .why_not_easy{
	display: none;
}	
.survey_form .row.g-2{
	margin-bottom: 20px;
}
.survey_form p{
	margin-bottom: 0;
}
.survey_form label {
    font-weight: bold;
}
</style>

<main id="main">
	<section id="contact" class="contact">
		<div class="container aos-init aos-animate" data-aos="fade-up">
			<div class="section-title">
				<h2>Submit Survey</h2>
			</div>
		<div class="card alert-primary">
			<div class="card-header card-header-bg text-light">
				<h4 class="mb-0">Survey</h4>
			</div>
			<div class="card-body pb-0">
				<form action="/survey" class="survey_form" method="post">
			    	{{ csrf_field() }}
			    	<div class="row g-2">
                       <div class="col-md-12">
                           <label for="email" class="form-label"><b>Email</b></label>
                           <input type="text" class="form-control" required id="email" name="email" value="">
                       </div>
                   </div>
			    	<div class="row g-2">
                       <div class="col-md-12">
                           <label for="age" class="form-label"><b>How old are you?</b></label>
                           <input type="text" class="form-control" required id="age" name="age" value="">
                       </div>
                   </div>
                   	<div class="row g-2">
	                   <div class="col-md-12">
	                       <label for="full_address" class="form-label"><b>In which city, state/province/department/parish/territory/division/prefecture/region, and country do you live?</b></label>
	                       <input type="text" class="form-control" required id="full_address" name="full_address" value="">
	                   </div>
	               </div>

	               <div class="row g-2">
	                   <div class="col-md-12">
	                   		<b><p>Was there a certain feature(s) of this app that was most useful for you?</p></b>
	                    	<input type="radio" class="fav_feature_c" id="fav_feature" name="fav_feature" value="YES">
							<label for="fav_feature">YES</label><br>
							<input type="radio" class="fav_feature_c" id="fav_feature_n" name="fav_feature" value="NO">
							<label for="fav_feature_n">NO</label><br>
	                   </div>
	                   <div class="col-md-12 which_feature">
	                   		<label for="which_feature">Which feature(s)</label>
	                   		<input type="text" id="which_feature" name="which_feature" class="form-control" />
	                   </div>
	                   <div class="col-md-12 why_not">
	                   		<label for="why_not">Why not</label>
	                   		<input type="text" id="why_not" name="why_not" class="form-control" />
	                   </div>
	               </div>

	               <div class="row g-2">
	                   <div class="col-md-12">
	                       <label for="for_living" class="form-label"><b>What do you do for a living?</b></label>
	                       <input type="text" class="form-control" required id="for_living" name="for_living" value="">
	                   </div>
	               </div>
	               
		               <div class="row g-2">
		                   <div class="col-md-12">
		                   		<b><p>Was there a certain feature(s) of this app that you would like to see removed?</p></b>
		                    	<input type="radio" class="not_fav_feature_c" id="not_fav_feature" name="not_fav_feature" value="YES">
								<label for="not_fav_feature">YES</label><br>
								<input type="radio" class="not_fav_feature_c" id="not_fav_feature_n" name="not_fav_feature" value="NO">
								<label for="not_fav_feature_n">NO</label><br>
		                   </div>
		                   <div class="col-md-12 which_feature_not">
		                   		<label for="which_feature_not">Which feature(s)</label>
		                   		<input type="text" id="which_feature_not" name="which_feature_not" class="form-control" />
		                   </div>
		                   <div class="col-md-12 why_not_feat">
		                   		<label for="why_all">Why were all of the features useful?</label>
		                   		<input type="text" id="why_all" name="why_all" class="form-control" />
		                   </div>
		               </div>

		               <div class="row g-2">
		                    <div class="col-md-12">
		                   		<b><p>How likely are you to recommend Finnfrastructure to someone?</p></b>

		                   		<input type="radio" class="rating" id="rating_1" name="rating" value="1">
								<label for="rating_1">1</label>

								<input type="radio" class="not_fav_feature_c" id="rating_2" name="rating" value="2">
								<label for="rating_2">2</label>
								<input type="radio" class="not_fav_feature_c" id="rating_3" name="rating" value="3">
								<label for="rating_3">3</label>
								<input type="radio" class="not_fav_feature_c" id="rating_4" name="rating" value="4">
								<label for="rating_4">4</label>
								<input type="radio" class="not_fav_feature_c" id="rating_5" name="rating" value="5">
								<label for="rating_5">5</label>
								<input type="radio" class="not_fav_feature_c" id="rating_6" name="rating" value="6">
								<label for="rating_6">6</label>
								<input type="radio" class="not_fav_feature_c" id="rating_7" name="rating" value="7">
								<label for="rating_7">7</label>
								<input type="radio" class="not_fav_feature_c" id="rating_8" name="rating" value="8">
								<label for="rating_8">8</label>
								<input type="radio" class="not_fav_feature_c" id="rating_9" name="rating" value="9">
								<label for="rating_9">9</label>
								<input type="radio" class="not_fav_feature_c" id="rating_10" name="rating" value="10">
								<label for="rating_10">10</label>

		                   	</div>
		                </div>
		               	
		               	<div class="row g-2">
		                   <div class="col-md-12">
		                   		<b><p>Would you pay to use this app in its current form?</p></b>
		                    	<input type="radio" class="pay_for_app" id="pay_for_app" name="pay_for_app" value="YES">
								<label for="pay_for_app">YES</label><br>
								<input type="radio" class="pay_for_app" id="pay_for_app_s" name="pay_for_app" value="NO">
								<label for="pay_for_app_s">NO</label><br>
		                   </div>
		                   <div class="col-md-12 how_much">
		                   		<label for="how_much">How much would you pay, on a monthly basis, to use all of the features?</label>
		                   		<input type="text" id="how_much" name="how_much" class="form-control" />
		                   </div>
		                   <div class="col-md-12 no_money">
		                   		<label for="no_money">Why not?</label>
		                   		<input type="text" id="no_money" name="no_money" class="form-control" />
		                   </div>
		               </div>


						<div class="row g-2">
							<div class="col-md-12">
								<label for="annually_amount" class="form-label"><b>Approximately how much money do you make annually?</b></label>
								<input type="text" required class="form-control" id="annually_amount" name="annually_amount" value="">
							</div>
						</div>

						<div class="row g-2">
							<div class="col-md-12">
								<label for="list_function_n" class="form-label"><b>Please list the functions of the app that did not work properly or satisfy your expectations.</b></label>
								<input type="text" required class="form-control" id="list_function_n" name="list_function_n" value="">
							</div>
						</div>

						<div class="row g-2">
							<div class="col-md-12">
								<label for="list_function_add" class="form-label"><b>What features would you add to the app?</b></label>
								<input type="text" required class="form-control" id="list_function_add" name="list_function_add" value="">
							</div>
						</div>

						<div class="row g-2">
		                   <div class="col-md-12">
		                   		<b><p>Were the concepts presented in the app helpful?</p></b>
		                    	<input type="radio" class="is_helpfull" id="is_helpfull" name="is_helpfull" value="YES">
								<label for="is_helpfull">YES</label><br>
								<input type="radio" class="is_helpfull" id="is_helpfull_s" name="is_helpfull" value="NO">
								<label for="is_helpfull_s">NO</label><br>
		                   </div>
		                   <div class="col-md-12 was_helpfull">
		                   		<label for="how_much_helpful">Which concepts were the most helpful?</label>
		                   		<input type="text" id="how_much_helpful" name="how_much_helpful" class="form-control" />
		                   </div>
		                   <div class="col-md-12 not_helpfull">
		                   		<label for="not_helpfull">What does the app lack that prohibited you from learning?</label>
		                   		<input type="text" id="not_helpfull" name="not_helpfull" class="form-control" />
		                   </div>
		               </div>

		               <div class="row g-2">
		                   <div class="col-md-12">
		                   		<b><p>Was the app easy to use?</p></b>
		                    	<input type="radio" class="easy_to_use" id="easy_to_use" name="easy_to_use" value="YES">
								<label for="easy_to_use">YES</label><br>
								<input type="radio" class="easy_to_use" id="easy_to_use_s" name="easy_to_use" value="NO">
								<label for="easy_to_use_s">NO</label><br>
		                   </div>
		                   <div class="col-md-12 why_easy">
		                   		<label for="why_easy">Why?</label>
		                   		<input type="text" id="why_easy" name="why_easy" class="form-control" />
		                   </div>
		                   <div class="col-md-12 why_not_easy">
		                   		<label for="why_not_easy">Why not?</label>
		                   		<input type="text" id="why_not_easy" name="why_not_easy" class="form-control" />
		                   </div>
		               </div>

		                <div class="row g-2">
			               <div class="col-md-12 how_many_exp">
		                   		<label for="how_many_exp"><b><p>How many years of experience do you have investing in the stock market and/or researching stocks?</p></b></label>
		                   		<input type="text" required id="how_many_exp" name="how_many_exp" class="form-control" />
		                   </div>
		                </div>

		                <div class="row g-2">
		                   <div class="col-md-12 how_many_exp_in_stock">
		                   		<label for="how_many_exp_in_stock"><b><p>How much experience do you have with stock market investing and/or data platforms?  If you have experience with these services, which one(s) have you utilized?</p></b></label>
		                   		<input type="text" required id="how_many_exp_in_stock" name="how_many_exp_in_stock" class="form-control" />
		                   </div>
		               </div>
	                   <br />
	                   <input type="submit" class="btn btn-lg btn-primary" name="submit" value="Submit" />
	                   <br/><br/>

			    </form>
			</div>
		</div>
		</div>
	</section>
</main>

<script>
	(function($){
		$(".fav_feature_c").on("change", function(e){
			
			if( $(this).val() == 'YES' ){
				$(".which_feature").show();
				$(".why_not").hide();
			} else if($(this).val() == 'NO'){
				$(".which_feature").hide();
				$(".why_not").show();
			}
		});

		$(".not_fav_feature_c").on("change", function(e){
			
			if( $(this).val() == 'YES' ){
				$(".which_feature_not").show();
				$(".why_not_feat").hide();
			} else if($(this).val() == 'NO'){
				$(".which_feature_not").hide();
				$(".why_not_feat").show();
			}
		});

		$(".pay_for_app").on("change", function(e){
			
			if( $(this).val() == 'YES' ){
				$(".how_much").show();
				$(".no_money").hide();
			} else if($(this).val() == 'NO'){
				$(".how_much").hide();
				$(".no_money").show();
			}
		});

		$(".is_helpfull").on("change", function(e){
			
			if( $(this).val() == 'YES' ){
				$(".was_helpfull").show();
				$(".not_helpfull").hide();
			} else if($(this).val() == 'NO'){
				$(".was_helpfull").hide();
				$(".not_helpfull").show();
			}
		});
		
		$(".easy_to_use").on("change", function(e){
			
			if( $(this).val() == 'YES' ){
				$(".why_easy").show();
				$(".why_not_easy").hide();
			} else if($(this).val() == 'NO'){
				$(".why_easy").hide();
				$(".why_not_easy").show();
			}
		});
		
	})(jQuery);
</script>

@endsection