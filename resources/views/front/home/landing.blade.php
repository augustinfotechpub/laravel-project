@extends('front.layouts.master')
@section('content')

<section class="herosec">
    <div id="hero" class="container">
        <div class="texture"></div>
      <video loop muted autoplay preload="auto">
            <source src="https://player.vimeo.com/external/181445574.hd.mp4?s=d24f32d879305e931468d55e4d7ce6efb5a95c39&amp;profile_id=119" type="video/mp4">
            Your browser does not support the video tag.  
      </video>

      
      <div class="caption">
        <h1 class="display-4">Lorem ipsum dolor</h1>
        <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit.</p>
      </div>
      
    </div>
</section>


<style>
body{
    overflow: hidden;
}
::-webkit-scrollbar{
    display:none;
}

.herosec{
  height:calc(100vh - 135px);
}
#hero {
  height:100%;
  width:100%;
  background:red;
  display:flex;
  align-items:center;
  justify-content:left;
}


#hero::after {
width:100%;
height:100%;
content: '';
position:absolute;
z-index:10;
top:0;
left:0;
background:rgba(0,0,0,0.5)
}


#hero video {
width:100%;
height:100%;
position:absolute;
top:0;
left:0;
z-index:5;
object-fit:cover;
font-family:'object-fit: cover';
}


#hero .texture {
width:100%;
height:100%;
position:absolute;
top:0;
left:0;
z-index:15;
background:url();
}


#hero .caption {
position:relative;
z-index:20;
text-align:left;
color:#ffffff;
}


#hero .caption h1 {
text-transform: uppercase;
font-size:2em;
font-family: "Raleway";
margin-bottom:0.5rem;
}

#hero .caption h3 {
font-weight:400;
font-size:1.5rem;
margin:0;
font-family: "Raleway";
}


@media screen and (min-width:768px)
{
#hero .caption h1 {
  font-size:2.5rem;
}

#hero .caption h3 {
  font-size:1.75rem;
}
}


@media screen and (min-width:992px)
{
#hero .caption h1 {
  font-size:3rem;
}

#hero .caption h3 {
  font-size:2rem;
}
}


@media screen and (min-width:1200px)
{
#hero .caption h1 {
  font-size:4rem;
}

#hero .caption h3 {
  font-size:2rem;
}
}



#header {
    transition: all 0.5s;
    z-index: 997;
    padding: 15px 0;
    background-color:rgba(40,58,90,0.3);
}



#footer {
    font-size: 14px;
    background-color:rgba(55,81,126,0.3);
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    z-index: 500;
    margin-top: auto;
}
</style>


<script>
    objectFitVideos();
</script>


<script>
    var objectFitVideos=function(t){"use strict";function e(t){for(var e=getComputedStyle(t).fontFamily,o=null,i={};null!==(o=l.exec(e));)i[o[1]]=o[2];return i["object-position"]?n(i):i}function o(t){var o=-1;t?"length"in t||(t=[t]):t=document.querySelectorAll("video");for(;t[++o];){var n=e(t[o]);(n["object-fit"]||n["object-position"])&&(n["object-fit"]=n["object-fit"]||"fill",i(t[o],n))}}function i(t,e){function o(){var o=t.videoWidth,n=t.videoHeight,d=o/n,a=r.clientWidth,c=r.clientHeight,p=a/c,l=0,s=0;i.marginLeft=i.marginTop=0,(d<p?"contain"===e["object-fit"]:"cover"===e["object-fit"])?(l=c*d,s=a/d,i.width=Math.round(l)+"px",i.height=c+"px","left"===e["object-position-x"]?i.marginLeft=0:"right"===e["object-position-x"]?i.marginLeft=Math.round(a-l)+"px":i.marginLeft=Math.round((a-l)/2)+"px"):(s=a/d,i.width=a+"px",i.height=Math.round(s)+"px","top"===e["object-position-y"]?i.marginTop=0:"bottom"===e["object-position-y"]?i.marginTop=Math.round(c-s)+"px":i.marginTop=Math.round((c-s)/2)+"px"),t.autoplay&&t.play()}if("fill"!==e["object-fit"]){var i=t.style,n=window.getComputedStyle(t),r=document.createElement("object-fit");r.appendChild(t.parentNode.replaceChild(r,t));var d={height:"100%",width:"100%",boxSizing:"content-box",display:"inline-block",overflow:"hidden"};"backgroundColor backgroundImage borderColor borderStyle borderWidth bottom fontSize lineHeight left opacity margin position right top visibility".replace(/\w+/g,function(t){d[t]=n[t]});for(var a in d)r.style[a]=d[a];i.border=i.margin=i.padding=0,i.display="block",i.opacity=1,t.addEventListener("loadedmetadata",o),window.addEventListener("optimizedResize",o),t.readyState>=1&&(t.removeEventListener("loadedmetadata",o),o())}}function n(t){return~t["object-position"].indexOf("left")?t["object-position-x"]="left":~t["object-position"].indexOf("right")?t["object-position-x"]="right":t["object-position-x"]="center",~t["object-position"].indexOf("top")?t["object-position-y"]="top":~t["object-position"].indexOf("bottom")?t["object-position-y"]="bottom":t["object-position-y"]="center",t}function r(t,e,o){o=o||window;var i=!1,n=null;try{n=new CustomEvent(e)}catch(t){n=document.createEvent("Event"),n.initEvent(e,!0,!0)}var r=function(){i||(i=!0,requestAnimationFrame(function(){o.dispatchEvent(n),i=!1}))};o.addEventListener(t,r)}var d=navigator.userAgent.indexOf("Edge/")>=0,a=new Image,c="object-fit"in a.style&&!d,p="object-position"in a.style&&!d,l=/(object-fit|object-position)\s*:\s*([-\w\s%]+)/g;c&&p||(o(t),r("resize","optimizedResize"))};"undefined"!=typeof module&&"undefined"!=typeof module.exports&&(module.exports=objectFitVideos);
</script>

@endsection