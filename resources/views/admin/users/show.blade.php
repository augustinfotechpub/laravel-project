@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.list') }}">Users</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>User Details</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right" href="{{ route('admin.users.list') }}"><i class="fa fa-arrow-left"> </i> Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Name</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->full_name }}
                                    </dd>

                                    <dt class="col-sm-3">Email</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->email }}
                                    </dd>

                                    <dt class="col-sm-3">Phone Number</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->phone_number }}
                                    </dd>

                                    <dt class="col-sm-3">Address</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->address_line1.', '.$user->address_line2 }}
                                    </dd>

                                    <dt class="col-sm-3">City</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->city }}
                                    </dd>

                                    <dt class="col-sm-3">State</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->state }}
                                    </dd>

                                    <dt class="col-sm-3">Country</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->country }}
                                    </dd>

                                    <dt class="col-sm-3">Zip Code</dt>
                                    <dd class="col-sm-9">
                                        {{ $user->zip_code }}
                                    </dd>

                                    <dt class="col-sm-3">Status</dt>
                                    <dd class="col-sm-9">
                                        {{ ($user->status)?'Active':'Inactive' }}
                                    </dd>


                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
