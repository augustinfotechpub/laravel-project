<div class="btn-group float-right">
	<a href="{{route('admin.user.showchat')}}?id={{$users->id}}" target="_blank" class="btn btn-primary py-1 px-2"><i
        class="fa fa-comment"></i></a>

    <a href="{{route('admin.user.show',$users->id)}}" class="btn btn-info py-1 px-2"><i
            class="fa fa-eye"></i></a>

    <button onclick="confirmPureDelete('{{ route('admin.user.delete',$users->id) }}')"
            class="btn btn-danger py-1 px-2">
        <i class="fa fa-trash"></i>
    </button>

</div>
