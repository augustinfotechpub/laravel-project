@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Users</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Users</h3></div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">Name</th>
                                        <th class="wd-15p sorting_asc">Email</th>
                                        <th class="wd-15p sorting_asc">Phone Number</th>
                                        <th class="wd-15p sorting_asc">Activate My Account</th>
                                        <th class="wd-15p sorting_asc">Subscription Cancelled</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
          //  $("#datatable").css("width", "100%");

            pageNU=0;
            pageLU=10;
            var setPageN = 0, setPageL = 10;
            if( localStorage.getItem('pageLU') != null ){
                setPageL = localStorage.getItem('pageLU');
            }

            if( localStorage.getItem('pageNU') != null ){
                setPageN = localStorage.getItem('pageNU');
            }

            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                pageLength: parseInt(setPageL),
                displayStart: ( parseInt(setPageL) * parseInt(setPageN) ),
                ajax: "{{ route('admin.users.list') }}",
                "drawCallback": function(settings, json) {
                    jQuery.each( jQuery(".row td:nth-child(3)"), function(i, v){
                        var text = jQuery(v).text();
                        jQuery(v).text( text.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3') );
                    } );
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'activate_my_account', name: 'activate_my_account'},
                    {data: 'cancel_subscription', name: 'cancel_subscription'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#datatable').on( 'length.dt', function ( e, settings, len) {
                pageLU = len;
                console.log( table.page.info() );
                localStorage.setItem('pageLU', pageLU);

                var info = table.page.info();
                pageNU = info.page;
                localStorage.setItem('pageNU', pageNU);
            });

            $('#datatable').on( 'page.dt', function (e, settings, len) {
                var info = table.page.info();
                pageNU = info.page;
                localStorage.setItem('pageNU', pageNU);
            }); 
        });
    </script>

@endsection
