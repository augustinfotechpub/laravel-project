@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Cancel Survey Feedback</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Cancel Survey Feedback</h3></div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">ID</th>
                                        <th class="wd-15p sorting_asc">User Name</th>
                                        <th class="wd-15p sorting_asc">Subscription ID</th>
                                        <th class="wd-15p sorting_asc">Answer</th>
                                        <th class="wd-15p sorting_asc">Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {

            pageNU=0;
            pageLU=10;
            var setPageN = 0, setPageL = 10;
            if( localStorage.getItem('pageLU') != null ){
                setPageL = localStorage.getItem('pageLU');
            }

            if( localStorage.getItem('pageNU') != null ){
                setPageN = localStorage.getItem('pageNU');
            }

            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                pageLength: parseInt(setPageL),
                displayStart: ( parseInt(setPageL) * parseInt(setPageN) ),
                ajax: '{{ route('admin.cancel-survey-feedback') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'user_name', name: 'user_name'},
                    {data: 'subscription_id', name: 'subscription_id'},
                    {data: 'answer', name: 'answer', render : function(data,type, row){
                        var html = '';
                        jQuery.each(data.answer, function(index,val){
                            console.log(data.question[index+1]);
                            html += "<p><b>"+data.question[index+1]+"</b></p>";
                            html += "<p>"+val+"</p>";
                            html += "<hr>";
                        })
                        return html;
                    }},
                    {data: 'Message', name: 'Message'},
                ]
            });

            $('#datatable').on( 'length.dt', function ( e, settings, len) {
                pageLU = len;
                console.log( table.page.info() );
                localStorage.setItem('pageLU', pageLU);

                var info = table.page.info();
                pageNU = info.page;
                localStorage.setItem('pageNU', pageNU);
            });

            $('#datatable').on( 'page.dt', function (e, settings, len) {
                var info = table.page.info();
                pageNU = info.page;
                localStorage.setItem('pageNU', pageNU);
            }); 
        });
    </script>

@endsection
