@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.list') }}">Users</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>User Details</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right" href="{{ route('admin.users.list') }}"><i class="fa fa-arrow-left"> </i> Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <table class="table az-table-reference">
                                        <tbody>
                                    @foreach($definition as $item)
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $item->id }}</td>  
                                    </tr>
                                    <tr>
                                        <th>AGE</th>
                                        <td>{{ $item->age }}</td>  
                                    </tr>
                                    <tr>
                                        <th>FULL ADDRESS</th>
                                        <td>{{ $item->full_address }}</td>  
                                    </tr>
                                    <tr>
                                        <th>FAV FEATURE</th>
                                        <td>{{ $item->fav_feature }}</td>  
                                    </tr>
                                    <tr>
                                        <th>WHICH FEATURE</th>
                                        <td>{{ $item->which_feature }}</td>  
                                    </tr>
                                    <tr>
                                        <th>WHY NOT</th>
                                        <td>{{ $item->why_not }}</td>  
                                    </tr>
                                    <tr>
                                        <th>FOR LIVING</th>
                                        <td>{{ $item->for_living }}</td>  
                                    </tr>
                                    <tr>
                                        <th>NOT FAV FEATURE</th>
                                        <td>{{ $item->not_fav_feature }}</td>  
                                    </tr>
                                    <tr>
                                        <th>WHICH FEATURE NOT</th>
                                        <td>{{ $item->which_feature_not }}</td>  
                                    </tr>
                                    <tr>
                                        <th>WHY ALL</th>
                                        <td>{{ $item->why_all }}</td>  
                                    </tr>
                                    <tr>
                                        <th>RATING</th>
                                        <td>{{ $item->rating }}</td>  
                                    </tr>
                                    <tr>
                                        <th>PAY FOR APP</th>
                                        <td>{{ $item->pay_for_app }}</td>  
                                    </tr>
                                    <tr>
                                        <th>HOW MUCH</th>
                                        <td>{{ $item->how_much }}</td>  
                                    </tr>
                                    <tr>
                                        <th>NO MONEY</th>
                                        <td>{{ $item->no_money }}</td>  
                                    </tr>
                                    <tr>
                                        <th>ANNUALLY AMOUNT</th>
                                        <td>{{ $item->annually_amount }}</td>  
                                    </tr>
                                    <tr>
                                        <th>LIST FUNCTION N</th>
                                        <td>{{ $item->list_function_n }}</td>  
                                    </tr>
                                    <tr>
                                        <th>LIST FUNCTION ADD</th>
                                        <td>{{ $item->list_function_add }}</td>  
                                    </tr>
                                    <tr>
                                        <th>IS HELPFULL</th>
                                        <td>{{ $item->is_helpfull }}</td>  
                                    </tr>
                                    <tr>
                                        <th>HOW MUCH HELPFULL</th>
                                        <td>{{ $item->how_much_helpful }}</td>  
                                    </tr>
                                    <tr>
                                        <th>NOT HELPFULL</th>
                                        <td>{{ $item->not_helpfull }}</td>  
                                    </tr>
                                    <tr>
                                        <th>EASY TO USE</th>
                                        <td>{{ $item->easy_to_use }}</td>  
                                    </tr>
                                    <tr>
                                        <th>WHY EASY</th>
                                        <td>{{ $item->why_easy }}</td>  
                                    </tr>
                                    <tr>
                                        <th>WHY NOT EASY</th>
                                        <td>{{ $item->why_not_easy }}</td>  
                                    </tr>
                                    <tr>
                                        <th>HOW MUCH EXP</th>
                                        <td>{{ $item->how_many_exp }}</td>  
                                    </tr>
                                    <tr>
                                        <th>HOW MANY EXP IN STOCK</th>
                                        <td>{{ $item->how_many_exp_in_stock }}</td>  
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    </table>
                                    
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
