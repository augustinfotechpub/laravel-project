@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Alerts</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            {{ Form::open(array('url' => 'admin/sync')) }}
	                            
                            <div class="input-group input-group-sm">
                                <label class="form-label w-100">Type</label>
                                <select name="syncType" class="form-control form-control-sm syncType">
                                    <option value="company" selected>Company Sync</option>
                                    <option value="keyword">Keyword Sync</option>
                                    <option value="keyparties">Key Parties</option>
                                </select>
                            </div>
                        	<br />
                            <div class="input-group input-group-sm symbol">
                                <label class="form-label w-100">Symbols</label>
                                <input type="text" class="form-control form-control-sm tag" name="symbol" placeholder="AMZN">
                            </div>

                            <div class="input-group input-group-sm alertSync">
                                <label class="form-label w-100">Alert Id</label>
                                <input type="text" class="form-control form-control-sm tag" name="alert" placeholder="213">
                            </div>

                                <br />

                                {!! Form::submit('Submit!') !!}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
	<script type="text/javascript">
		jQuery(".alertSync").hide();
		jQuery(".symbol").show();
		jQuery(document).on("change", ".syncType", function(){

			var syncType = jQuery(this).val();
			if( syncType == 'company' ){
				jQuery(".alertSync").hide();
				jQuery(".symbol").show();
			} else if( syncType == 'keyparties' ){
                jQuery(".alertSync").hide();
                jQuery(".symbol").show();
            }else {
				jQuery(".alertSync").show();
				jQuery(".symbol").hide();
			}

			console.log( jQuery(this).val() );
		});
	</script>
	
@endsection