@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Alerts</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">Id</th>
                                        <th class="wd-15p sorting_asc">Email</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            $("#datatable").css("width", "100%");
            pageNS=0;
            pageLS=10;
            var setpageNS = 0, setpageLS = 10;
            if( localStorage.getItem('pageLS') != null ){
                setpageLS = localStorage.getItem('pageLS');
            }

            if( localStorage.getItem('pageNS') != null ){
                setpageNS = localStorage.getItem('pageNS');
            }

            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                pageLength: parseInt(setpageLS),
                displayStart: ( parseInt(setpageLS) * parseInt(setpageNS) ),
                ajax: "{{ route('admin.survey.list') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'email', name: 'email'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#datatable').on( 'length.dt', function ( e, settings, len) {
                pageLS=len;
                console.log( table.page.info() );
                localStorage.setItem('pageLS', pageLS);

                var info = table.page.info();
                pageNS=info.page;
                localStorage.setItem('pageNS', pageNS);
            });

            $('#datatable').on( 'page.dt', function (e, settings, len) {
                var info = table.page.info();
                pageNS=info.page;
                localStorage.setItem('pageNS', pageNS);
            });  
        });
    </script>
@endsection
