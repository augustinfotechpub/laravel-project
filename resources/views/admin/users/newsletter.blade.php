@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Alerts</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            {{ Form::open(array('url' => 'admin/newsletterEmail')) }}
	                            
                        	<br />
                            <div class="input-group input-group-sm symbol">
                                <label class="form-label w-100">Email:</label>
                                <input type="text" class="form-control form-control-sm tag" name="symbol" placeholder="Enter Email">
                            </div>

                                <br />

                                {!! Form::submit('Send Test Mail!') !!}
                            {{ Form::close() }}
                        </div>
                        <hr/>
                        <div class="col-md-12">
                            {{ Form::open(array('url' => 'admin/newsletterEmailAll')) }}

                        	<br />
                                {!! Form::submit('Send to All Users!') !!}
                            {{ Form::close() }}
                        </div>
                        <hr/>
                        <div class="col-md-12">
                            {{ Form::open(array('url' => 'admin/newsletterEmailUpdate')) }}

                            <label class="form-label w-100">Register User for Newsletter (JSON FORMAT REQUIRED):</label>
                                <textarea class="form-control form-control-sm tag" rows="20" name="email" placeholder="Enter Json Email data to Store in DB"></textarea>
                                <br />
                                {!! Form::submit('Save to Db!') !!}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
