@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.alerts.list') }}">Alerts</a></li>
                <li class="breadcrumb-item">Create</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="card">
                    <div class="card-header alert-primary">
                        <div class="card-header-row">
                            <div class="row">
                                <div class="col-md-6"><h3>Create Alert</h3></div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.alerts.list') }}" title="Back"
                                       class="btn btn-warning btn-sm font-weight-bold float-right">
                                        <i class="fa fa-arrow-left mr-1"></i>Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.alert.store') }}"
                              class="form-horizontal">
                            @csrf
                            @include('admin.alerts.form', ['formMode' => 'Create'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.alerts.partials.add-item')
@endsection
@section('style')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function () {

            $('.identifierAction').hide();
            $('#identifier').on('change', function () {
                $('.identifierAction').hide();
                $('.action-'+$(this).val()).show('d-none');
            });

            var item = $('.template').clone().last();
            item.removeClass('d-none');
            item.appendTo('.definitionItems');
            item.find('.identifier').select2();
            item.find('.content').summernote({
                height: 150
            });
            // item.find('.type').select2();

            var counter = 1
            $(document).on('click', '.addTemplate', function () {
                var item = item = $('.template').clone().last();
                item.removeClass('d-none');
                item.find('.identifier').attr('name', 'identifier[' + counter + ']');
                item.find('.subTitle').attr('name', 'sub_title[' + counter + ']');
                item.find('.type').attr('name', 'type[' + counter + ']');
                item.find('.sicStart').attr('name', 'sic_start[' + counter + ']');
                item.find('.sicEnd').attr('name', 'sic_end[' + counter + ']');
                item.find('.tag').attr('name', 'tags[' + counter + '][]');
                item.find('.sic').attr('name', 'sics[' + counter + '][]');
                item.find('.symbol').attr('name', 'symbols[' + counter + '][]');
                item.find('.content').attr('name', 'content[' + counter + ']');
                item.appendTo('.definitionItems');
                item.find('.content').summernote({
                    height: 150
                });
                item.find('.identifier').select2();
                counter++;
            })
            $(document).on('click', '.removeTemplate', function () {
                var len = $('.template').length;
                if (len > 2) {
                    $(this).closest('.template').remove();
                }
            });


            $(document).on('click', '.addTag', function () {
                var item = $(this).parent('div').clone();
                item.find('.tag').val('');
                item.find('.addTag').addClass('d-none');
                item.find('.removeTag').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowTag');
                item.appendTo(row);
            });

            $(document).on('click', '.addInBetween', function () {
                var item = $(this).parent('div').clone();
                item.find('input').val('');
                item.find('.addInBetween').addClass('d-none');
                item.find('.removeInBetween').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowInBetween');
                item.appendTo(row);
            });

            $(document).on('click', '.removeTag', function () {
                $(this).parent('div').remove();
            });

            $(document).on('click', '.removeInBetween', function () {
                $(this).parent('div').remove();
            });

            $(document).on('click', '.addSic', function () {
                var item = $(this).parent('div').clone();
                item.find('.sic').val('');
                item.find('.addSic').addClass('d-none');
                item.find('.removeSic').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowSic');
                item.appendTo(row);
            });
            $(document).on('click', '.removeSic', function () {
                $(this).parent('div').remove();
            });

            $(document).on('click', '.addSymbol', function () {
                var item = $(this).parent('div').clone();
                item.find('.symbol').val('');
                item.find('.addSymbol').addClass('d-none');
                item.find('.removeSymbol').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowSymbol');
                item.appendTo(row);
            });
            $(document).on('click', '.removeSymbol', function () {
                $(this).parent('div').remove();
            });

        });
    </script>
@endsection
