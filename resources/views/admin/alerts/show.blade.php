@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.alerts.list') }}">Key Definitions</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Alert Detail</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right"
                                           href="{{ route('admin.alerts.list') }}"><i class="fa fa-arrow-left"> </i>
                                            Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Title</dt>
                                    <dd class="col-sm-9">
                                        <b> {{ ucfirst($definition->title) }} </b>
                                    </dd>
                                    <dt class="col-sm-3">Identifier</dt>
                                    <dd class="col-sm-9">
                                        <b> {{ $definition->identifier }} </b>
                                    </dd>
                                </dl>

                                <table class="table az-table-reference">
                                    <thead>
                                    <tr>
                                        <th>TYPE</th>
                                        <th>SUB TITLE</th>
                                        <th>KEYWORD</th>
                                        <th>SYMBOL</th>
                                        <th>SIC</th>
                                        <th>CONTENT</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($definition->items as $item)
                                        <tr>
                                            <td>
                                                @if( isset(\App\Models\Definition::TYPE[$item->type]))
                                                {{ \App\Models\Definition::TYPE[$item->type] }}
                                                @endif
                                                </td>
                                            <td>{{ $item->sub_title }}</td>
                                            <td>
                                                @foreach($item->tags as $itemTag)
                                                    <div class="badge rounded-pill bg-dark py-2 px-2 mb-1 text-white">
                                                        {{ $itemTag->keyword }}
                                                    </div>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach($item->symbols as $itemSymbol)
                                                    <div class="badge rounded-pill bg-dark py-2 px-2 mb-1 text-white">
                                                        {{ $itemSymbol->symbol }}
                                                    </div>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach($item->sics as $itemSic)
                                                    <div class="badge rounded-pill bg-dark py-2 px-2 mb-1 text-white">
                                                        {{ $itemSic->sic }}
                                                    </div>
                                                @endforeach
                                            </td>
                                            <td>{!! $item->content !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
