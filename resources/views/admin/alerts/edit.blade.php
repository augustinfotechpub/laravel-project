@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.alerts.list') }}">Alerts</a></li>
                <li class="breadcrumb-item">Edit</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="card">
                    <div class="card-header alert-primary">
                        <div class="card-header-row">
                            <div class="row">
                                <div class="col-md-6"><h3>Edit Alert</h3></div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.alerts.list') }}" title="Back"
                                       class="btn btn-warning btn-sm font-weight-bold float-right">
                                        <i class="fa fa-arrow-left mr-1"></i>Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.alert.update',$definition->id) }}"
                              class="form-horizontal">
                            {{ csrf_field() }}
                            @include('admin.alerts.form', ['formMode' => 'Edit'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.alerts.partials.add-item')
@endsection
@section('style')
    <link href="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function () {

            // $('.identifierAction').hide();
            // $('#identifier').on('change', function () {
            //     $('.identifierAction').hide();
            //     $('.action-'+$(this).val()).show('d-none');
            // });
            $('.contentEdit').summernote({
                height: 150
            });
            $('.identifierEdit').each(function (){
                $(this).select2();
            });



            var counter = 0;
            $(document).on('click', '.addTemplate', function () {
                counter = $('.template').length - 1;
                var item = item = $('.template').clone().last();
                item.removeClass('d-none');
                item.find('.identifier').attr('name', 'identifier[' + counter + ']');
                item.find('.subTitle').attr('name', 'sub_title[' + counter + ']');
                item.find('.type').attr('name', 'type[' + counter + ']');
                item.find('.sicStart').attr('name', 'sic_start[' + counter + ']');
                item.find('.sicEnd').attr('name', 'sic_end[' + counter + ']');
                item.find('.tag').attr('name', 'tags[' + counter + '][]');
                item.find('.sic').attr('name', 'sics[' + counter + '][]');
                item.find('.symbol').attr('name', 'symbols[' + counter + '][]');
                item.find('.content').attr('name', 'content[' + counter + ']');
                item.appendTo('.definitionItems');
                item.find('.content').summernote({
                    height: 150
                });
                item.find('.identifier').select2();
                counter++;
            })
            $(document).on('click', '.removeTemplate', function () {
                $(this).closest('.template').remove();
            });

            $(document).on('click', '.addTag', function () {
                var item = $(this).parent('div').clone();
                item.find('.tag').val('');
                item.find('.addTag').addClass('d-none');
                item.find('.removeTag').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowTag');
                item.appendTo(row);
            });
            $(document).on('click', '.removeTag', function () {
                $(this).parent('div').remove();
            });

            $(document).on('click', '.addInBetween', function () {
                var item = $(this).parent('div').clone();
                item.find('input').val('');
                item.find('.addInBetween').addClass('d-none');
                item.find('.removeInBetween').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowInBetween');
                item.appendTo(row);
            });

            $(document).on('click', '.removeInBetween', function () {
                $(this).parent('div').remove();
            });

            $(document).on('click', '.addSic', function () {
                var item = $(this).parent('div').clone();
                item.find('.sic').val('');
                item.find('.addSic').addClass('d-none');
                item.find('.removeSic').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowSic');
                item.appendTo(row);
            });
            $(document).on('click', '.removeSic', function () {
                $(this).parent('div').remove();
            });

            $(document).on('click', '.addSymbol', function () {
                var item = $(this).parent('div').clone();
                item.find('.symbol').val('');
                item.find('.addSymbol').addClass('d-none');
                item.find('.removeSymbol').removeClass('d-none');
                var row = $(this).parent('div').parent('div').parent('div').find('.rowSymbol');
                item.appendTo(row);
            });
            $(document).on('click', '.removeSymbol', function () {
                $(this).parent('div').remove();
            });


            $(document).on('click', '#tag', function () {
                var dataId = $(this).data('id');
                var url = $app_url + '/admin/alert/tag/delete/' + dataId;

                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "JSON",
                    data: {},
                    success: function () {
                    }
                });
                $(this).closest('.badge').remove();
            });

            $(document).on('click', '#sic', function () {
                var dataId = $(this).data('id');
                var url = $app_url + '/admin/alert/sic/delete/' + dataId;

                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "JSON",
                    data: {},
                    success: function () {
                    }
                });
                $(this).closest('.badge').remove();
            });

            $(document).on('click', '#symbol', function () {
                var dataId = $(this).data('id');
                var url = $app_url + '/admin/alert/symbol/delete/' + dataId;

                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: "JSON",
                    data: {},
                    success: function () {
                    }
                });
                $(this).closest('.badge').remove();
            });

        });
    </script>
@endsection

