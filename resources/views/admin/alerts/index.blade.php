@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Alerts</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-5"><h3>Alerts</h3></div>
                                    <div class="col-md-4">
                                        {{ Form::open(array('url' => 'admin/alerts/import', 'files' => true)) }}
                                            <label>
                                                {{ Form::checkbox('deleteAll',null,null, array('id'=>'delete-data')) }}
                                                 Remove all data    
                                            </label>
                                            
                                            {!! Form::file('file', ['id' => 'inputGroupMotivation', 'class' => 'file form-control form-control-sm']) !!}
                                            
                                            {!! Form::submit('Go!') !!}
                                        {{ Form::close() }}
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ route('admin.alert.create') }}"
                                           class="float-right btn btn-inline btn-xs btn-success">Create New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">Title</th>
                                        <th class="wd-15p sorting_asc">Identifier</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            $("#datatable").css("width", "100%");
            pageN=0;
            pageL=10;
            var setPageN = 0, setPageL = 10;
            if( localStorage.getItem('pageL') != null ){
                setPageL = localStorage.getItem('pageL');
            }

            if( localStorage.getItem('pageN') != null ){
                setPageN = localStorage.getItem('pageN');
            }

            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                pageLength: parseInt(setPageL),
                displayStart: ( parseInt(setPageL) * parseInt(setPageN) ),
                ajax: "{{ route('admin.alerts.list') }}",
                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'identifier', name: 'identifier'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#datatable').on( 'length.dt', function ( e, settings, len) {
                pageL=len;
                console.log( table.page.info() );
                localStorage.setItem('pageL', pageL);

                var info = table.page.info();
                pageN=info.page;
                localStorage.setItem('pageN', pageN);
            });

            $('#datatable').on( 'page.dt', function (e, settings, len) {
                var info = table.page.info();
                pageN=info.page;
                localStorage.setItem('pageN', pageN);
            });  
        });
    </script>
@endsection
