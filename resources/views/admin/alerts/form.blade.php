<div class="row row-sm mg-b-20">
    <div class="col-lg-4">
        <div class="input-group">
            <label class="control-label w-100">Title <small>*</small></label>
            <input type="text" class="form-control w-100" name="title" placeholder="Title"
                   value="{{ isset($definition) ? $definition->title : old('title') }}" autofocus>
        </div>
    </div>
    <div class="col-md-8">
        <a class="border-0 bg-success float-right mt-3 text-white px-2 py-1 addTemplate">
            <i class="fa fa-plus"></i>
            Add More
        </a>
    </div>
</div>

@if(isset($definition))
    @foreach($definition->items as $index=>$item)
        <div class="col-md-12 border-top mt-2 bg-gray-100 template">
            <div class="row">
                <div class="col-lg-6">
                    <div class="row row-sm mg-b-20">
                        <div class="col-md-12 mt-2">
                            <div class="input-group input-group-sm">
                                <label class="control-label w-100">Identifier</label>
                                <select name="identifier[{{$index}}]" id="identifier" class="form-control form-control-sm identifierEdit w-100">
                                    <option>Select Identifier</option>
                                    @foreach($alertIdentifiers as $alertIdentifier)
                                        <option value="{{ $alertIdentifier['identifier'] }}"
                                                @if(isset($definition) && ($item->identifier ==  $alertIdentifier['identifier'])) selected @endif>{{ $alertIdentifier['title'] }}</option>
                                    @endforeach
                                </select>
                               {{-- @foreach($alertIdentifiers as $alertIdentifier)
                                    <div class="action-{{$alertIdentifier['identifier']}} identifierAction mt-1">
                                        <b class="w-100">{{ $alertIdentifier['info'] }}</b>
                                    </div>
                                @endforeach--}}
                            </div>
                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="input-group input-group-sm">
                                <label for="type" class="form-label w-100">Sub Title</label>
                                <input type="text" class="form-control form-control-sm subTitle"
                                       name="sub_title[{{$index}}]" value="{{ $item->sub_title }}"
                                       placeholder="Sub Title">
                            </div>
                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="input-group input-group-sm">
                                <label class="form-label w-100">Type</label>
                                <select name="type[{{$index}}]" class="form-control form-control-sm type">
                                    @foreach($types as $key=>$type)
                                        <option
                                            value="{{ $key }}" {{ $key==$item->type?'selected':'' }}>{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 mt-2">
                            <label for="tags" class="form-label w-100">Tags</label>
                            <div class="input-group input-group-sm my-1">
                                <input type="text" class="form-control form-control-sm tag" name="tags[{{ $index }}][]">
                                <button class="border-0 bg-success text-white addTag" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="border-0 bg-danger text-white removeTag d-none" type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="rowTag"></div>
                            @foreach($item->tags as $tagItem)
                                <div class="badge rounded-pill bg-dark py-2 px-2 mb-1 text-white">
                                    {{ $tagItem->keyword }}
                                    <i class="fa fa-trash ml-1" id="tag" data-id="{{ $tagItem->id }}"
                                       style="cursor: pointer;"></i>
                                    <input type="hidden" name="tags[{{ $index }}][]" value="{{ $tagItem->keyword }}">
                                </div>
                            @endforeach
                        </div>

                        <div class="col-md-12 mt-2">
                            <label for="sics" class="w-100">SIC's Code Between</label>
                            <div class="input-group input-group-sm mb-3">
                                <input type="number" name="sic_start[0][]" class="form-control form-control-sm sicStart"
                                       placeholder="SIC Start">
                                <span class="input-group-text" id="basic-addon1">-</span>
                                <input type="number" name="sic_end[0][]" class="form-control form-control-sm sicEnd"
                                       placeholder="SIC End">
                                <button class="border-0 bg-success text-white addInBetween" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="border-0 bg-danger text-white removeInBetween d-none" type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="rowInBetween">
                            </div>
                        </div>
                
                        <div class="col-md-12 mt-2">
                            <label for="sics" class="w-100">SIC's Code</label>
                            <div class="input-group input-group-sm my-1">
                                <input type="number" class="form-control form-control-sm sic" name="sics[{{$index}}][]">
                                <button class="border-0 bg-success text-white addSic" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="border-0 bg-danger text-white removeSic d-none" type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="rowSic"></div>
                            @foreach($item->sics as $sicItem)
                                <span class="badge rounded-pill bg-dark py-2 px-2 mb-1 text-white">
                                {{ $sicItem->sic }}
                                <i class="fa fa-trash ml-1" id="sic" data-id="{{ $sicItem->id }}"
                                   style="cursor: pointer;"></i>
                                <input type="hidden" name="sics[{{ $index }}][]" value="{{ $sicItem->sic }}">
                                </span>
                            @endforeach
                        </div>
                        <div class="col-md-12 mt-2">
                            <label for="symbols" class="w-100">Symbols</label>
                            <div class="input-group input-group-sm my-1">
                                <input type="text" class="form-control form-control-sm symbol"
                                       name="symbols[{{$index}}][]">
                                <button class="border-0 bg-success text-white addSymbol" type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="border-0 bg-danger text-white removeSymbol d-none" type="button">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="rowSymbol"></div>
                            @foreach($item->symbols as $symbolItem)
                                <span class="badge rounded-pill bg-dark py-2 px-2 mb-1 text-white">
                                {{ $symbolItem->symbol }}
                                <i class="fa fa-trash ml-1" id="symbol" data-id="{{ $symbolItem->id }}"
                                   style="cursor: pointer;"></i>
                                <input type="hidden" name="symbols[{{ $index }}][]" value="{{ $symbolItem->symbol }}">
                                </span>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="col-lg-12 mt-2">
                        <a class="border-0 bg-danger float-right text-white mb-1 px-2 py-1 removeTemplate"
                           type="button">
                            <i class="fa fa-trash"></i>
                            Remove Item
                        </a>
                    </div>
                    <div class="col-lg-12 mt-5">
                        <div class="input-group input-group-sm">
                            <label class="control-label w-100">Definition <small>*</small></label>
                            <textarea class="form-control form-control-sm contentEdit" rows="10"
                                      name="content[{{$index}}]" required="">{!! $item->content !!}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="definitionItems">
</div>
<div class="form-group mb-0 mt-3 text-right">
    <button class="btn btn-primary px-5 text-white">{{ $formMode }}</button>
</div>

