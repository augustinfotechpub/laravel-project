<div class="btn-group float-right">

    <a href="{{route('admin.alert.show',$definition->id)}}" class="btn btn-info py-1 px-2"><i
            class="fa fa-eye"></i></a>
    <a href="{{route('admin.alert.edit',$definition->id)}}" class="btn btn-primary py-1 px-2"><i
            class="fa fa-edit"></i></a>

        <button onclick="confirmPureDelete('{{ route('admin.alert.delete',$definition->id) }}')"
                class="btn btn-danger py-1 px-2">
            <i class="fa fa-trash"></i>
        </button>

</div>
