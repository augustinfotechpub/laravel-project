<div class="col-md-12 border-top mt-2 bg-gray-100 template d-none">
    <div class="row">
        <div class="col-lg-6">
            <div class="row row-sm mg-b-20">
                <div class="col-md-12 mt-2">
                    <div class="input-group input-group-sm">
                        <label class="control-label w-100">Identifier</label>
                        <select name="identifier[0]" id="identifier" class="form-control form-control-sm identifier w-100">
                            <option>Select Identifier</option>
                            @foreach($alertIdentifiers as $item)
                                <option value="{{ $item['identifier'] }}"
                                        @if(isset($definition) && ($definition->identifier ==  $item['identifier'])) selected @endif>{{ $item['title'] }}</option>
                            @endforeach
                        </select>
{{--                        @foreach($alertIdentifiers as $item)--}}
{{--                            <div class="action-{{$item['identifier']}} identifierAction mt-1">--}}
{{--                                <b class="w-100">{{ $item['info'] }}</b>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="input-group input-group-sm">
                        <label for="type" class="form-label">Sub Title</label>
                        <input type="text" class="form-control form-control-sm w-100 subTitle" name="sub_title[0]"
                               placeholder="Sub Title">
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="input-group input-group-sm">
                        <label class="form-label w-100">Type</label>
                        <select name="type[0]" class="form-control form-control-sm type">
                            @foreach($types as $key=>$type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="tags" class="form-label w-100">Tags</label>
                    <div class="input-group input-group-sm my-1">
                        <input type="text" class="form-control form-control-sm tag" name="tags[0][]">
                        <button class="border-0 bg-success text-white addTag" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                        <button class="border-0 bg-danger text-white removeTag d-none" type="button">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                    <div class="rowTag">
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="sics" class="w-100">SIC's Code Between</label>
                    <div class="input-group input-group-sm mb-3">
                        <input type="number" name="sic_start[0][]" class="form-control form-control-sm sicStart"
                               placeholder="SIC Start">
                        <span class="input-group-text" id="basic-addon1">-</span>
                        <input type="number" name="sic_end[0][]" class="form-control form-control-sm sicEnd"
                               placeholder="SIC End">
                        <button class="border-0 bg-success text-white addInBetween" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                        <button class="border-0 bg-danger text-white removeInBetween d-none" type="button">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                    <div class="rowInBetween">
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="sics">SIC's Code</label>
                    <div class="input-group input-group-sm my-1">
                        <input type="number" class="form-control form-control-sm sic" name="sics[0][]">
                        <button class="border-0 bg-success text-white addSic" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                        <button class="border-0 bg-danger text-white removeSic d-none" type="button">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                    <div class="rowSic">

                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="symbols" class="w-100">Symbols</label>
                    <div class="input-group input-group-sm my-1">
                        <input type="text" class="form-control form-control-sm symbol" name="symbols[0][]">
                        <button class="border-0 bg-success text-white addSymbol" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                        <button class="border-0 bg-danger text-white removeSymbol d-none" type="button">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                    <div class="rowSymbol">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="col-lg-12 mt-2">
                <a class="border-0 bg-danger float-right text-white mb-1 px-2 py-1 removeTemplate" type="button">
                    <i class="fa fa-trash"></i> Remove Item
                </a>
            </div>
            <div class="col-lg-12 mt-5">
                <div class="input-group input-group-sm">
                    <label class="control-label w-100">Definition <small>*</small></label>
                    <textarea class="form-control form-control-sm content" rows="10" name="content[0]"
                              required>{{--{!! isset($item) ? $item->content: ''!!}--}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
