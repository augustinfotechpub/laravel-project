@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.contact_requests.list') }}">Contact RequestS</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Contact Request Details</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right" href="{{ route('admin.contact_requests.list') }}"><i class="fa fa-arrow-left"> </i> Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Name</dt>
                                    <dd class="col-sm-9">
                                        {{ $contact_request->name }}
                                    </dd>

                                    <dt class="col-sm-3">Email Address</dt>
                                    <dd class="col-sm-9">
                                        {{ $contact_request->email }}
                                    </dd>

                                    <dt class="col-sm-3">Subject</dt>
                                    <dd class="col-sm-9">
                                        {{ $contact_request->subject }}
                                    </dd>

                                    <dt class="col-sm-3">Message</dt>
                                    <dd class="col-sm-9">
                                        {!! $contact_request->message !!}
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
