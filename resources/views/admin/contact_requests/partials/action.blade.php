<div class="btn-group float-right">

    <a href="{{route('admin.contact_request.show',$contacts->id)}}" class="btn btn-info py-1 px-2"><i
            class="fa fa-eye"></i></a>

    <button onclick="confirmPureDelete('{{ route('admin.contact_request.delete',$contacts->id) }}')"
            class="btn btn-danger py-1 px-2">
        <i class="fa fa-trash"></i>
    </button>

</div>
