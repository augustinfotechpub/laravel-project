@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Companies</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>Add/Update Members for symbol # {{ $company->symbol }}</h4>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a href="{{ route('admin.companies.index') }}" class="btn btn-warning">
                                            <i class="fa fa-arrow-left"></i>
                                            Back</a>
                                        <a href="{{ route('admin.companies.show',$company->id) }}"
                                           class="btn btn-info">View</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('admin.companies.update',$company->id) }}" method="post">
                                    @method('PATCH')
                                    @csrf
                                    <div class="row mb-2">
                                        <div class="col-md-4">
                                            <label>Member Name</label>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Is Chairman</label>
                                        </div>
                                        <div class="col-md-4">
                                            <a class="btn btn-success btn-sm text-white" id="add">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                    <div id="members">
                                        @foreach($company->auditCommitteeMembers as $member)
                                            @php($randString = \Illuminate\Support\Str::random())
                                            <div class="row pb-2">
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control members_name"
                                                           name="members_name[{{ $randString }}]" value="{{ $member->name }}"
                                                           placeholder="Member Name">
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="radio" class="is_chairman" name="is_chairman"
                                                           {{ $member->is_chairman?'checked':null }}
                                                           value="{{ $randString }}">
                                                </div>
                                                <div class="col-md-4">
                                                    <a class="btn btn-danger btn-sm remove text-white">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </div>
                                                <div class="col-md-2"></div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 text-center">
                                            <button class="btn btn-success">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none" id="member-template">
        <div class="row pb-2">
            <div class="col-md-4">
                <input type="text" class="form-control members_name" name="members_name[]" placeholder="Member Name">
            </div>
            <div class="col-md-2">
                <input type="radio" class="is_chairman" name="is_chairman" value="1">
            </div>
            <div class="col-md-4">
                <a class="btn btn-danger btn-sm remove text-white">
                    <i class="fa fa-minus"></i>
                </a>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var memberTemplate = $('#member-template');
        var members = $('#members');
        $(document).ready(function () {
            addMember();
            $(document).on('click', '#add', function () {
                addMember();
            });
            $(document).on('click', '.remove', function () {
                $(this).closest('.row').remove();
            });
        });

        function addMember() {
            var tempId = makeid(10);
            var row = memberTemplate.find('.row').clone();
            row.find('.members_name').attr('name', 'members_name[' + tempId + ']');
            row.find('.is_chairman').attr('value', tempId);
            members.append(row);
        }

        function makeid(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

    </script>


@endsection
