<ul>
    @foreach($data->auditCommitteeMembers as $member)
        <li>
            {{ $member->name }}{{ $member->is_chairman?'<small>(Chairman)</small>':null }}
        </li>
    @endforeach
</ul>
