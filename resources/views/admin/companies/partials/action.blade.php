<div class="btn-group float-right">
    <a href="{{route('admin.companies.show',$data->id)}}" class="btn btn-info action-button"><i
            class="fa fa-eye"></i></a>
    <a href="{{route('admin.companies.edit',$data->id)}}" class="btn btn-success action-button"><i
            class="fa fa-edit"></i></a>
    <button onclick="confirmPureDelete('{{ route('admin.companies.destroy',$data->id) }}')"
            class="delete btn btn-danger action-button">
        <i class="fa fa-trash"></i>
    </button>
</div>
