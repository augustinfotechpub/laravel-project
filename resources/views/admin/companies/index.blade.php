@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Companies</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Companies</h3></div>
                                    {{--<div class="col-md-6 text-right">
                                        <a href="{{ route('admin.companies.create') }}" class="btn btn-primary">Create</a>
                                    </div>--}}
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive dataTable no-footer">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">Symbol</th>
                                        <th class="wd-15p sorting_asc">CIK</th>
                                        <th class="wd-15p sorting_asc">Name</th>
                                        <th class="wd-15p sorting_asc">Sector</th>
                                        <th class="wd-15p sorting_asc">Head Quarter</th>
                                        <th class="wd-15p sorting_asc">Founded</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: false,
                pageLength: 30,
                ajax: "{{ route('admin.companies.index') }}",
                columns: [
                    {data: 'symbol', name: 'symbol'},
                    {data: 'cik', name: 'cik'},
                    {data: 'name', name: 'name'},
                    {data: 'sector', name: 'sector'},
                    {data: 'headQuarter', name: 'headQuarter'},
                    {data: 'founded', name: 'founded'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>

@endsection
