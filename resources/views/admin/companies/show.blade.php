@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.companies.index') }}">Companies</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Company Details</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right"
                                           href="{{ route('admin.companies.index') }}">
                                            <i class="fa fa-arrow-left"> </i>
                                            Back
                                        </a>
                                        <a class="btn btn-success float-right"
                                           href="{{ route('admin.companies.edit',$company->id) }}">
                                            <i class="fa fa-edit"> </i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Id</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->id }}
                                    </dd>

                                    <dt class="col-sm-3">Symbol</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->symbol }}
                                    </dd>

                                    <dt class="col-sm-3">CIK</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->cik }}
                                    </dd>

                                    <dt class="col-sm-3">Name</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->name }}
                                    </dd>

                                    <dt class="col-sm-3">Sector</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->sector }}
                                    </dd>

                                    <dt class="col-sm-3">Sub Sector</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->subSector }}
                                    </dd>

                                    <dt class="col-sm-3">Headquarter</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->headQuarter }}
                                    </dd>

                                    <dt class="col-sm-3">Date first added</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->dateFirstAdded }}
                                    </dd>
                                    <dt class="col-sm-3">Founded in</dt>
                                    <dd class="col-sm-9">
                                        {{ $company->founded }}
                                    </dd>
                                    <dt class="col-sm-3">Created at</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($company->created_at) }}
                                    </dd>
                                    <dt class="col-sm-3">Last updated at</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($company->updated_at) }}
                                    </dd>
                                </dl>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Audit Committee Members</h4>
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Is Chairman</th>
                                            </tr>
                                            </thead>
                                            <body>
                                            @foreach($company->auditCommitteeMembers as $member)
                                                <tr>
                                                    <td>{{ ucfirst($member->name) }}</td>
                                                    <td>{{ $member->is_chairman?'Yes':null }}</td>
                                                </tr>
                                            @endforeach
                                            </body>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
