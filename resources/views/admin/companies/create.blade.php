@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Companies</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Add Members</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('admin.companies.store') }}">
                                    <div class="row">
                                        <div class="col-md-6">Member Name</div>
                                        <div class="col-md-2">Is Chairman</div>
                                        <div class="col-md-4">
                                            <a class="btn btn-success btn-sm" id="add">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="members">

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none" id="member-template">
        <div class="row">
            <div class="col-md-6">
                <label>
                    <input type="text" class="form-control" placeholder="Member Name">
                </label>
            </div>
            <div class="col-md-2">
                <label>
                    <input type="checkbox" name="is_chairman">
                </label>
            </div>
            <div class="col-md-4">
                <a class="btn btn-danger btn-sm remove">
                    <i class="fa fa-minus"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        var memberTemplate = $('#member-template');
        var members = $('#members');
        $(document).ready(function () {
            $(document).on('click', '#add', function () {
                addMember();
            });
            $(document).on('click', '.remove', function () {
                $(this).closest('.row').remove();
            });
        });

        function addMember() {
            var row = memberTemplate.find('.row').clone();
            members.append(row);
        }
    </script>
@endsection
