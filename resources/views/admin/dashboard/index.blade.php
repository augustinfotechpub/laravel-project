@extends('admin.layouts.master')
@section('content')
    <div class="az-content az-content-dashboard-eight">
        <div class="container d-block">
            <h2 class="az-content-title tx-24 mg-b-5">Hi, welcome back!</h2>
            <p class="mg-b-20 mg-lg-b-25">Your executive or sass dashboard template.</p>

            <div class="row row-sm mg-b-20">

                <div class="col-lg-8">
                    <div class="row row-xs row-sm--sm">
                        <div class="col-sm-6 col-md-3">
                            <div class="card card-dashboard-seventeen">
                                <div class="card-body">
                                    <h6 class="card-title">Users
                                        <p> <span class="op-7">All Active Users</span></p></h6>
                                    <div>
                                        <h4>{{ $active_users }}</h4>
                                        <span>Total : {{$active_users}}</span>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <div id="flotChart1" class="flot-chart"></div>
                                </div><!-- chart-wrapper -->
                            </div>
                        </div><!-- col -->
                        <div class="col-sm-6 col-md-3 mg-t-20 mg-sm-t-0">
                            <div class="card card-dashboard-seventeen">
                                <div class="card-body">
                                    <h6 class="card-title">Subscriptions
                                        <p> <span class="op-7">All subscription total amount</span></p></h6>
                                    <div>
                                        <h4>{{ formatPrice($total_subscriptions_amount) }}</h4>
                                        <span>Total: {{ formatPrice($total_subscriptions_amount) }}</span>
                                    </div>
                                </div><!-- card-body -->
                                <div class="chart-wrapper">
                                    <div id="flotChart2" class="flot-chart"></div>
                                </div><!-- chart-wrapper -->
                            </div><!-- card -->
                        </div><!-- col -->
                        <div class="col-sm-6 col-md-3 mg-t-20 mg-md-t-0">
                            <div class="card card-dashboard-seventeen bg-primary-dark tx-white">
                                <div class="card-body">
                                    <h6 class="card-title">Subscriptions
                                        <p> <span class="op-7">This Month Total Amount</span></p></h6>
                                    <div>
                                        <h4 class="text-white">{{formatPrice($current_month_total)}}</h4>
                                        <span class="op-7">Total: {{formatPrice($current_month_total)}}</span>
                                    </div>
                                </div><!-- card-body -->
                                <div class="chart-wrapper">
                                    <div id="flotChart3" class="flot-chart"></div>
                                </div><!-- chart-wrapper -->
                            </div><!-- card -->
                        </div><!-- col -->
                        <div class="col-sm-6 col-md-3 mg-t-20 mg-md-t-0">
                            <div class="card card-dashboard-seventeen bg-primary tx-white">
                                <div class="card-body">
                                    <h6 class="card-title">Subscriptions
                                        <p> <span class="op-7">Expired Subscription This Month</span></p></h6>

                                    <div>
                                        <h4 class="text-white">{{$current_month_expired}}</h4>
                                        <span class="op-7">Total: {{$current_month_expired}}</span>
                                    </div>
                                </div><!-- card-body -->
                                <div class="chart-wrapper">
                                    <div id="flotChart4" class="flot-chart"></div>
                                </div><!-- chart-wrapper -->
                            </div>
                        </div><!-- col -->
                        <div class="col-12 mg-t-20">
                            <div class="card">
                                <div class="card-header alert-primary">
                                    <h5 class="mb-1">New Registered</h5>
                                    <p class="mb-0 text-muted">Recent Signup Advisor.</p>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped mg-b-0">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Reg. Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $user)
                                               <tr>
                                                   <td>{{$user->full_name}}</td>
                                                   <td>{{$user->email}}</td>
                                                   <td>{{ formatDate($user->created_at) }}</td>
                                                   <td>
                                                       <a href="{{route('admin.user.show',$user->id)}}" class="edit btn btn-info action-button"><i
                                                               class="fa fa-eye"></i></a>
                                                   </td>
                                               </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div><!-- bd -->
                                </div>
                            </div>
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- col -->


{{--                <div class="col-lg-4 mg-t-20 mg-lg-t-0">--}}
{{--                    <div class="card card-dashboard-eighteen">--}}
{{--                        <h6 class="card-title mg-b-5">Finance Monitoring</h6>--}}
{{--                        <p class="tx-gray-500 mg-b-0">July 01,2018 - September 30,2018</p>--}}
{{--                        <div class="card-body row row-xs">--}}
{{--                            <div class="col-6">--}}
{{--                                <h6 class="dot-primary"><span>$</span>387,098</h6>--}}
{{--                                <label>Accounts Receivable</label>--}}
{{--                            </div><!-- col -->--}}
{{--                            <div class="col-6">--}}
{{--                                <h6 class="dot-purple"><span>$</span>657,213</h6>--}}
{{--                                <label>Accounts Payable</label>--}}
{{--                            </div><!-- col -->--}}
{{--                            <div class="col-6 mg-t-30">--}}
{{--                                <h6 class="dot-teal"><span>$</span>332,891</h6>--}}
{{--                                <label>Monthly Burn</label>--}}
{{--                            </div><!-- col -->--}}
{{--                            <div class="col-6 mg-t-30">--}}
{{--                                <h6 class="dot-dark-blue"><span>$</span>78,005</h6>--}}
{{--                                <label>Net Monthly Burn</label>--}}
{{--                            </div><!-- col -->--}}
{{--                        </div><!-- card-body -->--}}
{{--                        <h6 class="card-title mg-b-10">Monthly Trends</h6>--}}
{{--                        <div class="chartjs-wrapper"><canvas id="chartBar5"></canvas></div>--}}
{{--                    </div><!-- card -->--}}
{{--                </div><!-- col -->--}}


            </div><!-- row -->
        </div><!-- container -->
    </div><!-- az-content -->
@endsection
