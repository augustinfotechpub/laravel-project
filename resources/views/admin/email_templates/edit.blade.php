@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.email_templates.list') }}">Email Templates</a></li>
                <li class="breadcrumb-item">Edit</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="card">
                    <div class="card-header alert-primary">
                        <div class="card-header-row">
                            <div class="row">
                                <div class="col-md-6"><h3>Edit Email Template</h3></div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.email_templates.list') }}" title="Back"
                                       class="btn btn-warning btn-sm font-weight-bold float-right">
                                        <i class="fa fa-arrow-left mr-1"></i>Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.email_template.update') }}"
                              class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$email_template->id}}">
                            @include('admin.email_templates.form', ['formMode' => 'Edit'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
