@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Email Templates</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="card">
                    <div class="card-header alert-primary">
                        <div class="card-header-row">
                            <div class="row">
                                <div class="col-md-6"><h3>Email Templates</h3></div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.email_template.create') }}"
                                       class="float-right btn btn-inline btn-xs btn-success">Create New</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped mg-b-0">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Subject</th>
                                    <th>Action</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($email_templates as $email_template)
                                    <tr>
                                        <td>{{ $email_template->title }}</td>
                                        <td>{{ $email_template->subject }}</td>
                                        <td>{{ $email_template->action }}</td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <a href="{{route('admin.email_template.edit',$email_template->id)}}" class="btn btn-primary py-1 px-2"><i
                                                        class="fa fa-edit"></i></a>
                                                <button onclick="confirmPureDelete('{{ route('admin.email_template.delete',$email_template->id) }}')"
                                                        class="btn btn-danger py-1 px-2">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
