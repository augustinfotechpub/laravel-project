<style>
    .email-action {
        display: table-row;
    }

    .select2-container--open .select2-dropdown--below {
        z-index: 1000;
    }
</style>
@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection
<div class="row row-sm mg-b-20">
    <div class="col-lg-6">
        <div class="row row-sm mg-b-20">
            <div class="col-lg-12">
                <label class="control-label">Title</label>
                {!! Form::text('title', isset($email_template) ? $email_template->title : '', ['class' => 'form-control','autofocus', 'placeholder' => 'Title',
                            'data-validation' => 'required', 'data-validation-error-msg' => 'Title is required'] ) !!}
                {!! $errors->first('title', '<p class="validate">:message</p>') !!}
            </div>
            <div class="col-lg-12">
                <label class="control-label">Subject</label>
                {!! Form::text('subject', isset($email_template) ? $email_template->subject : '', ['class' => 'form-control', 'placeholder' => 'Subject',
                            'data-validation' => 'required', 'data-validation-error-msg' => 'Subject is required'] ) !!}
                {!! $errors->first('subject', '<p class="validate">:message</p>') !!}
            </div>
            <div class="col-lg-12">
                <label class="control-label">Action</label>
                {!! Form::select('action', \App\Models\EmailTemplate::EMAIL_ACTION,
                                isset($email_template) ? $email_template->action : '',['class'=>'form-control select2','id'=>'action',
                                'placeholder' => 'Action','data-validation' => 'required', 'data-validation-error-msg' => 'Action is required']) !!}
                {!! $errors->first('action', '<p class="validate">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="row row-sm mg-b-20">
            <div class="col-lg-12">
                <label class="control-label">Key Value Pairs</label>
                <table class="table table-bordered">
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    @foreach(config('emailvariables') as $action_key=> $action)
                        @foreach($action as $key=>$value)
                            <tr class="action-{{ $action_key }} email-action"
                                style="display: {{ (isset($email_template)&& $email_template->action == $action_key)?'table-row':'none' }};">
                                <td>{{ $key }}</td>
                                <td>{{ $value }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row row-sm mg-b-20">
    <div class="col-lg-12 ">
        <label class="control-label">Template Content</label>
        <textarea class="form-control wysiwyg_editor" rows="5"
                  name="content">{!! isset($email_template) ? $email_template['content'] : '' !!}</textarea>
        {!! $errors->first('content', '<p class="validate">:message</p>') !!}

    </div>
</div>
<div class="form-group mb-0 text-right">
    <button type="submit" class="btn btn-primary px-5">{{ $formMode }}</button>
</div>
@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
    <script>
        $(".wysiwyg_editor").summernote();
        $(document).ready(function () {
            $('#action').on('change', function () {
                console.log('js');
                $('.email-action').hide();
                $('.action-' + $(this).val()).show();
            });
        });
    </script>
@endsection
