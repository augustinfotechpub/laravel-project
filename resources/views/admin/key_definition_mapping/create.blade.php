@extends('admin.layouts.master')
@section('style')
    <style>
        .select2-container {
            width: 100% !important;
            padding: 0;
        }
    </style>
@endsection
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.key_alert_mapping.list') }}">Key Definitions
                        Mapping</a></li>
                <li class="breadcrumb-item">Create</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="card">
                    <div class="card-header alert-primary">
                        <div class="card-header-row">
                            <div class="row">
                                <div class="col-md-6"><h3>Create Key Definition Mapping</h3></div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.key_alert_mapping.list') }}" title="Back"
                                       class="btn btn-warning btn-sm font-weight-bold float-right">
                                        <i class="fa fa-arrow-left mr-1"></i>Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.key_alert_mapping.store') }}"
                              class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-rows">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <a class="btn btn-success btn-sm float-end text-white" id="add"
                                           onclick="addRow();">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Select SIC</label>
                                    </div>
                                    <div class="col-md-7">
                                        <label>Select Key definitions</label>
                                    </div>
                                    <div class="col-md-1 text-right"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.key_alert_mapping.partials.addRow')
@endsection
@section('script')
    <script type="text/javascript">
        addRow();
        $(document).ready(function () {
            $(document).on('click', '.remove', function () {
                $(this).closest('.row').remove();
            });

            $(".sic").closest('.row').find('.definitions').attr('name', 'definitions[' + $('.sic').val() + '][]');
            $(document).on('change', '.sic', function () {
                $(this).closest('.row').find('.definitions').attr('name', 'definitions[' + $(this).val() + '][]');
            });
        });

        function addRow() {
            $(".sic").closest('.row').find('.definitions').attr('name', 'definitions[' + $('.sic').val() + '][]');
            var row = $('#row-template').find('.row').clone();
            row.find('select').select2();
            $(".form-rows").append(row);
        }
    </script>
@endsection

