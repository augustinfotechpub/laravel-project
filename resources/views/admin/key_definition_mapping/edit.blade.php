@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.key_alert_mapping.list') }}">SIC Key
                        Definition Mapping</a></li>
                <li class="breadcrumb-item">Edit</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="card">
                    <div class="card-header alert-primary">
                        <div class="card-header-row">
                            <div class="row">
                                <div class="col-md-6"><h3>Edit SIC Key Definition Mapping</h3></div>
                                <div class="col-md-6">
                                    <a href="{{ route('admin.key_alert_mapping.list') }}" title="Back"
                                       class="btn btn-warning btn-sm font-weight-bold float-right">
                                        <i class="fa fa-arrow-left mr-1"></i>Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.key_alert_mapping.update',$sic->id) }}"
                              class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-5">
                                    <label>Select SIC</label>
                                    <select class="form-control select2" name="sic">
                                        @foreach($sics as $item)
                                            <option value="{{ $item->id }}" @if($item->id===$sic->id) selected @endif>
                                                SIC : {{ $item->sic_code }}
                                                - {{ ucfirst( $item->title) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-7">
                                    <label>Select Key Definition</label>
                                    <select class="form-control select2" multiple name="key_definition[]">
                                        @foreach($definitions as $definition)
                                            <option value="{{ $definition->id }}"
                                                    @foreach ($sic->definitions as $key_definition)
                                                    @if($definition->id===$key_definition->id)
                                                    selected
                                                    @endif
                                                    @endforeach
                                            >{{ ucfirst( $definition->title) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <button class="btn btn-primary float-right">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

