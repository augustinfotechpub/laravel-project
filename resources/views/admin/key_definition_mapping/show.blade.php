@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.key_alert_mapping.list') }}">SIC Key
                        Definitions Mapping</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>SIC Key Definitions Mapping Details</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right"
                                           href="{{ route('admin.key_alert_mapping.list') }}"><i
                                                class="fa fa-arrow-left"> </i> Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">SIC Code</dt>
                                    <dd class="col-sm-9">
                                        {{ $sic->sic_code }}
                                    </dd>
                                    <dt class="col-sm-3">Title</dt>
                                    <dd class="col-sm-9">
                                        {{ ucfirst($sic->title) }}
                                    </dd>

                                    <dt class="col-sm-3">SIC Key definition mapping list</dt>
                                    <dd class="col-sm-9">
                                        @if(isset($sic->definitions[0]))
                                            @foreach($sic->definitions as $definition)
                                                <a href="{{ route('admin.alert.show',$definition->id) }}"
                                                   class="btn btn-secondary btn-sm mt-1">{{ ucfirst($definition->title) }}</a>
                                            @endforeach
                                        @else
                                            <span class="text-danger">Key Definitions not mapped</span>
                                        @endif
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
