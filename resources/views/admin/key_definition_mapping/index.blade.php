@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">SIC Key Definitions Mapping</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>SIC Key Definitions Mapping</h3></div>
                                    <div class="col-md-6">
                                        <a href="{{ route('admin.key_alert_mapping.create') }}"
                                           class="float-right btn btn-inline btn-xs btn-success">Create New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">SIC Code</th>
                                        <th class="wd-15p sorting_asc">Title</th>
                                        <th class="wd-15p sorting_asc">Mapping Definition</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
            //  $("#datatable").css("width", "100%");
            var table = $('#datatable').removeAttr('width').DataTable({
                fixedColumns: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.key_alert_mapping.list') }}",
                columns: [
                    {data: 'sic_code', name: 'sic_code'},
                    {data: 'title', name: 'title'},
                    {data: 'mapping_definition', name: 'mapping_definition'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>

@endsection


