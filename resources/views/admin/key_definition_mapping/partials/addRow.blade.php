<div class="d-none mt-4" id="row-template">
    <div class="row pb-2">
        <div class="col-md-4">
            <select class="form-control sic">
                @foreach($sics as $item)
                    <option value="{{ $item->id }}">SIC : {{ $item->sic_code }} - {{ ucfirst( $item->title) }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-5">
            <select class="form-control definitions" multiple>
                @foreach($definitions as $definition)
                    <option value="{{ $definition->id }}">{{ ucfirst( $definition->title) }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 text-right">
            <a class="btn btn-danger btn-sm remove text-white">
                <i class="fa fa-minus"></i>
            </a>
        </div>
    </div>
</div>
