<div class="btn-group float-right">
    {{-- show bottun   --}}
    <a href="{{ route('admin.key_alert_mapping.show',$sic->id) }}"
       class="btn btn-info py-1 px-2"><i
            class="fa fa-eye"></i></a>
    {{-- edit bottun   --}}
    <a href="{{ route('admin.key_alert_mapping.edit',$sic->id) }}" class="btn btn-success py-1 px-2">
        <i class="fa fa-edit"></i></a>
    {{-- delte bottun   --}}
    <button onclick="confirmPureDelete('{{ route('admin.key_alert_mapping.delete',$sic->id) }}')"
            class="btn btn-danger py-1 px-2">
        <i class="fa fa-trash"></i>
    </button>

</div>
