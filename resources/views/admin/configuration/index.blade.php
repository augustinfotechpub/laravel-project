@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative mt-1">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Site Configuration</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Site Configuration</h3></div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('admin.configuration.update') }}" enctype="multipart/form-data">
                                    @csrf
                                    @foreach($configuration_data as $configuration)

                                        @if($configuration->identifier == 'WELCOME_VIDEO_FILE')
                                            <div class="row mt-2">
                                                <div class="col-md-6">
                                                    <label for="">{{ $configuration->title }}</label>
                                                </div>
                                                <div class="col-md-6">
                                                    @if( !empty($configuration->value))
                                                        <div class="video_container">
                                                            <video width="200" controls>
                                                                <source src="{{URL::to('/') }}/{{$configuration->value}}" type="video/mp4" />
                                                            </video>
                                                            <span class="remove_video">X</span>
                                                        </div>
                                                        <br />
                                                    @endif

                                                    <input type="file" class="video_field" name="configuration[{{ $configuration->identifier }}]"
                                                           value="{{ $configuration->value }}" class="form-control" />
                                                </div>
                                            </div>
                                        @else
                                            <div class="row mt-2">
                                                <div class="col-md-6">
                                                    <label for="">{{ $configuration->title }}</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" name="configuration[{{ $configuration->identifier }}]"
                                                           value="{{ $configuration->value }}" class="form-control">
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                    
                                    <div class="row mt-5">
                                        <div class="col-md-12">
                                            <input type="submit" name="submit" value="Update Configuration Details"
                                                   class="btn btn-success float-right">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
