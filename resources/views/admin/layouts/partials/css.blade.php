<!-- vendor css -->
<link href="{{ asset('back-theme/lib/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
<link href="{{ asset('back-theme/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('back-theme/lib/typicons.font/typicons.css') }}" rel="stylesheet">
<link href="{{ asset('back-theme/lib/select2/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('back-theme/lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
<link href="{{ asset('back-theme/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}">


<!-- azia CSS -->
<link rel="stylesheet" href="{{ asset('back-theme/css/azia.css') }}">
<link rel="stylesheet" href="{{ asset('back-theme/css/custom.css') }}">

