<script src="{{ asset('back-theme/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/ionicons/ionicons.js') }}"></script>
<script src="{{ asset('back-theme/lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" type="text/javascript"></script>

<script src="{{ asset('back-theme/js/azia.js') }}"></script>
<script src="{{ asset('back-theme/js/custom.js') }}"></script>
<script type="text/javascript">

    var $app_url = "{{ url('/') }}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.select2').select2();

    function confirmPureDelete(delete_url) {
        bootbox.confirm({
            message: "Are you sure you want to delete ?",
            buttons: {
                confirm: {
                    label: "Yes",
                    className: 'btn-success'
                },
                cancel: {
                    label: "No",
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                console.log(result);
                if (result === true) {
                    var bootBoxDelete = $('#bootBoxDelete');
                    console.log(bootBoxDelete.html());
                    bootBoxDelete.attr('action', delete_url);
                    bootBoxDelete.submit();
                }
            }
        });
    }

    $(".select2").select2();
</script>

