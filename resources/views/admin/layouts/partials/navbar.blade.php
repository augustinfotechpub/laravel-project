@php($currentRoute = \Illuminate\Support\Facades\Route::currentRouteName())
<div class="az-navbar az-navbar-two az-navbar-dashboard-eight">
    <div class="container">
        <div><a href="index.html" class="az-logo">az<span>i</span>a</a></div>
        <div class="az-navbar-search">
            <input type="search" class="form-control" placeholder="Search for schedules and events...">
            <button class="btn"><i class="fas fa-search "></i></button>
        </div><!-- az-navbar-search -->
        <ul class="nav">
            <li class="nav-label">Main Menu</li>
            <li class="nav-item @if($currentRoute =='admin.dashboard') active @endif">
                <a href="{{route('admin.dashboard')}}" class="nav-link"><i class="far fa-chart-bar fa-2x pe-1"></i>
                    Dashboard</a>
            </li><!-- nav-item -->
            <li class="nav-item @if($currentRoute =='admin.users.list') active @endif ">
                <a href="" class="nav-link with-sub"><i class="fa fa-users fa-2x pe-1"></i>User</a>
                <nav class="nav-sub">
                    <a href="{{ route('admin.users.list') }}" class="nav-sub-link">All Users</a>
                </nav>
            </li><!-- nav-item -->
            <li class="nav-item
                @if(($currentRoute =='admin.subscriptions_plan.list') ||
                    ($currentRoute =='admin.subscriptions.list')) active @endif">
                <a href="" class="nav-link with-sub"><i class="fas fa-gift fa-2x pe-1"></i>Subscription</a>
                <nav class="nav-sub">
                    <a href="{{ route('admin.subscriptions_plan.list') }}" class="nav-sub-link">Subscription Plans</a>
                    <a href="{{ route('admin.subscriptions.list') }}" class="nav-sub-link">Subscriptions</a>
                </nav>
            </li><!-- nav-item -->

            <li class="nav-item
                @if(($currentRoute =='admin.contact_requests.list')) active @endif">
                <a href="" class="nav-link with-sub"><i class="fa fa-handshake fa-2x pe-1"></i>Contact</a>
                <nav class="nav-sub">
                    <a href="{{ route('admin.contact_requests.list') }}" class="nav-sub-link">All Contact Requests</a>
                </nav>
            </li><!-- nav-item -->
            <li class="nav-item
                    @if($currentRoute =='admin.configurations' || $currentRoute =='admin.email_templates.list' || $currentRoute =='admin.alerts.list' || $currentRoute =='admin.key_alert_mapping.list') active @endif">
                <a href="" class="nav-link with-sub"><i class="fa fa-cog fa-fw pe-1"></i>System</a>
                <nav class="nav-sub">
                    <a href="{{ route('admin.configurations') }}" class="nav-sub-link">Site Configurations</a>
                    <a href="{{ route('admin.email_templates.list') }}" class="nav-sub-link">Email Templates</a>
                    <a href="{{ route('admin.alerts.list') }}" class="nav-sub-link">Alerts</a>
                    {{--<a href="{{ route('admin.key_alert_mapping.list') }}" class="nav-sub-link">SIC Key Definition Mapping</a>--}}
                    <a href="{{ route('admin.sync.index') }}" class="nav-sub-link">Sync</a>
                    <a href="{{ route('admin.companies.index') }}" class="nav-sub-link">Companies</a>
                    <a href="{{ route('admin.survey.list') }}" class="nav-sub-link">Survey</a>
                    <a href="{{ route('admin.cancel-survey-feedback') }}" class="nav-sub-link">Cancel Survey Feedback</a>
                    <a href="{{ route('admin.newsletter') }}" class="nav-sub-link">News Letter</a>
                </nav>
            </li><!-- nav-item -->
        </ul><!-- nav -->
    </div><!-- container -->
</div><!-- az-navbar -->
