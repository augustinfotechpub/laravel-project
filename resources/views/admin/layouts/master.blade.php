<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="{{ config('app.name') }}">
    <meta name="author" content="{{ config('app.name') }}">

    <title>{{ config('app.name') }}</title>
    @include('admin.layouts.partials.css')
    @yield('style')
    <livewire:styles/>
    @stack('styles')
</head>
<body class="az-body bg-white">

@include('admin.layouts.partials.header')
@include('admin.layouts.partials.navbar')
@yield('content')
<form id="bootBoxDelete" method="post">
    @csrf
    @method('DELETE')
    <button type="submit" style="display: none">
        <i class="fa fa-trash"></i>
    </button>
</form>
@include('admin.layouts.partials.footer')
@include('admin.layouts.partials.js')
@stack('scripts')
<livewire:scripts/>
@yield('script')
</body>
</html>
