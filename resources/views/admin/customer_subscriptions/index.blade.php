@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Subscriptions</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Subscriptions</h3></div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">Plan Title</th>
                                        <th class="wd-15p sorting_asc">Customer Name</th>
                                        <th class="wd-15p sorting_asc">Start Date</th>
                                        <th class="wd-15p sorting_asc">End Date</th>
                                        <th class="wd-15p sorting_asc">Price</th>
                                        <th class="wd-15p sorting_asc">Status</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function () {
              $("#datatable").css("width", "100%");
            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('admin.subscriptions.list') }}",
                columns: [
                    {data: 'plan_title', name: 'plan_title'},
                    {data: 'customer_name', name: 'customer_name'},
                    {data: 'start_date', name: 'start_date'},
                    {data: 'end_date', name: 'end_date'},
                    {data: 'price', name: 'price'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>

@endsection
