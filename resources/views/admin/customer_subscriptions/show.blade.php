@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.subscriptions.list') }}">Subscriptions</a></li>
                <li class="breadcrumb-item">Detail</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Subscription Detail</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right"
                                           href="{{ route('admin.subscriptions.list') }}"><i
                                                class="fa fa-arrow-left"> </i> Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Plan</dt>
                                    <dd class="col-sm-9">
                                        {{ $customer_subscription->subscriptionPlan->title }}
                                    </dd>

                                    <dt class="col-sm-3">Customer Name</dt>
                                    <dd class="col-sm-9">
                                        {{ $customer_subscription->customer->full_name }}
                                    </dd>

                                    <dt class="col-sm-3">Plan Price</dt>
                                    <dd class="col-sm-9">
                                        {{ formatPrice($customer_subscription->subscriptionPlan->price) }}
                                    </dd>

                                    <dt class="col-sm-3">Plan Duration<small> (Interval)</small></dt>
                                    <dd class="col-sm-9">
                                        {{ $customer_subscription->interval_value.' '.ucfirst($customer_subscription->interval_unit) }}
                                    </dd>

                                    <dt class="col-sm-3">Subscription Status</dt>
                                    <dd class="col-sm-9">
                                        {{ ucfirst($customer_subscription->status) }}
                                    </dd>

                                    <dt class="col-sm-3">Subscription Start Date</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($customer_subscription->start_date) }}
                                    </dd>

                                    <dt class="col-sm-3">Subscription End Date</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($customer_subscription->end_date) }}
                                    </dd>

                                    <dt class="col-sm-3">Subscription Trial Start Date</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($customer_subscription->trial_start)??'--' }}
                                    </dd>

                                    <dt class="col-sm-3">Subscription Trial End Date</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($customer_subscription->trial_end)??'--' }}
                                    </dd>


                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
