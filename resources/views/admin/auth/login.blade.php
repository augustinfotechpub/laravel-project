<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="{{ config('app.name') }}">
    <meta name="author" content="{{ config('app.name') }}">

    <title>{{env('APP_NAME')}} | Login</title>

    <!-- vendor css -->
    <link href="{{ asset('back-theme/lib/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('back-theme/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('back-theme/lib/typicons.font/typicons.css') }}" rel="stylesheet">

    <!-- azia CSS -->
    <link rel="stylesheet" href="{{ asset('back-theme/css/azia.css') }}">
    <link rel="stylesheet" href="{{ asset('back-theme/css/login.css') }}">

</head>
<body class="az-body">
<div class="az-signin-wrapper">
    <div class="az-card-signin h-auto">
        <h1 class="az-logo text-center">Login</h1>
        <div class="az-signin-header">
            <h4 class="text-center">{{env('APP_NAME')}}</h4>
            @include('admin.layouts.partials.flash_messages')
            <form method="post" action="{{route('admin.login.authenticate')}}">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="text" class="form-control" placeholder="Enter your email" value="{{old('email')}}" autofocus>
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Password</label>
                    <input name="password" type="password" class="form-control" placeholder="Enter your password"
                           value="">
                </div><!-- form-group -->
                <button class="btn btn-az-light rounded-5 btn-block font-weight-bold">Sign In</button>
            </form>
        </div><!-- az-signin-header -->
        <div class="az-signin-footer">
            <p class="text-center"><a href="{{ route('admin.forgot_password.create') }}">Forgot password?</a></p>
        </div><!-- az-signin-footer -->
        <span class="magic-ball magic-ball3"></span>
    </div><!-- az-card-signin -->

    <span class="magic-ball magic-ball1"></span>
    <span class="magic-ball magic-ball2"></span>
</div><!-- az-signin-wrapper -->

<script src="{{ asset('back-theme/lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('back-theme/lib/ionicons/ionicons.js') }}"></script>

<script src="{{ asset('back-theme/js/azia.js') }}"></script>
<script>
    $(function () {
        'use strict'
    });
</script>
</body>
</html>
