@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Subscriptions Plan</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Subscription Plans</h3></div>
                                    <div class="col-md-6">
                                        {{--<a href="{{ route('subscription_plans.sync') }}"
                                           class="float-right btn btn-inline btn-xs btn-info">Sync Plans</a>--}}
                                        <a href="{{ route('admin.subscription_plan.create') }}"
                                           class="float-right btn btn-inline btn-xs btn-success">Create New</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatable2" class="display responsive nowrap dataTable no-footer dtr-inline"
                                       role="grid"
                                       aria-describedby="datatable2_info" style="width: 1142px;">
                                    <thead>
                                    <tr role="row">
                                        <th class="wd-15p sorting_asc">Plan Title</th>
                                        <th class="wd-15p sorting_asc">Price</th>
                                        <th class="wd-15p sorting_asc">Interval</th>
                                        <th class="wd-15p sorting_asc">Free Trial Period</th>
                                        <th class="wd-15p sorting_asc">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscription_plans as $subscription_plan)
                                        <tr role="row" class="odd">
                                            <td tabindex="0" class="sorting_1">{{ $subscription_plan->title }}<br>
                                                <small>Stripe Product:
                                                    <span>{{ $subscription_plan->product_key }}</span></small></td>
                                            <td>{{ formatPrice($subscription_plan->price) }}
                                                <br/>
                                                <small>Price Key:
                                                    <span>{{ $subscription_plan->price_key }}</span></small></td>
                                            <td>{{ $subscription_plan->interval_value . " " . $subscription_plan->interval_unit }}</td>
                                            <td>{{ $subscription_plan->free_trial_period . " " . $subscription_plan->free_trial_unit }}</td>
                                            <td>
                                                <a href="{{route('admin.subscription_plan.show',$subscription_plan->id)}}" class="btn btn-info py-1 px-2"><i
                                                        class="fa fa-eye"></i></a>
{{--                                                <a href="{{route('admin.subscription_plan.edit',$subscription_plan->id)}}" class="edit btn btn-success action-button"><i--}}
{{--                                                        class="fa fa-edit"></i></a>--}}
                                                    <button onclick="confirmPureDelete('{{ route('admin.subscription_plan.delete',$subscription_plan->id) }}')"
                                                            class="btn btn-danger py-1 px-2">
                                                        <i class="fa fa-trash"></i>
                                                    </button>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
