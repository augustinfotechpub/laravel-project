@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.subscriptions_plan.list') }}">Subscriptions Plan</a></li>
                <li class="breadcrumb-item">Details</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Subscription Details</h3></div>
                                    <div class="col-md-6">
                                        <a class="btn btn-warning float-right" href="{{ route('admin.subscriptions_plan.list') }}"><i class="fa fa-arrow-left"> </i> Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-3">Plan</dt>
                                    <dd class="col-sm-9">
                                        {{ $subscription_plan->title }}
                                    </dd>

                                    <dt class="col-sm-3">Plan Price</dt>
                                    <dd class="col-sm-9">
                                        {{ formatPrice($subscription_plan->price) }}
                                    </dd>

                                    <dt class="col-sm-3">Plan Duration<small> (Interval)</small></dt>
                                    <dd class="col-sm-9">
                                        {{ $subscription_plan->interval_value.' '.ucfirst($subscription_plan->interval_unit) }}
                                    </dd>

                                    <dt class="col-sm-3">Free Trial Period</dt>
                                    <dd class="col-sm-9">
                                        {{ $subscription_plan->free_trial_period.' '.ucfirst($subscription_plan->free_trial_unit) }}
                                    </dd>

                                    <dt class="col-sm-3">Created Date</dt>
                                    <dd class="col-sm-9">
                                       {{ formatDate($subscription_plan->created_at) }}
                                    </dd>

                                    <dt class="col-sm-3">Updated Date</dt>
                                    <dd class="col-sm-9">
                                        {{ formatDate($subscription_plan->updated_at) }}
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
