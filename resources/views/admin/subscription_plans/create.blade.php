@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.subscriptions_plan.list') }}">Subscriptions Plan</a></li>
                <li class="breadcrumb-item">Create New Subscription Plan</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Create New Subscription Plan</h3></div>
                                    <div class="col-md-6">
                                        <a href="{{ route('admin.subscriptions_plan.list') }}" class="float-right btn btn-warning mb-0">
                                            <i class="fa fa-arrow-left"> </i> Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('admin.subscription_plan.store') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Plan Title</label>
                                                <input type="text" name="title" class="form-control" value="{{old('title')}}" autofocus>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Price</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"
                                                           name="price" value="{{old('price')}}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">.00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Interval</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="interval_value" placeholder="" value="{{old('interval_value')}}">
                                                    <div class="input-group-prepend">
                                                        <select name="interval_unit" id="" class="form-control bg-light">
                                                            <option value="day">Day(s)</option>
                                                            <option value="week">Week(s)</option>
                                                            <option value="month">Month(s)</option>
                                                        </select>
                                                    </div><!-- input-group-prepend -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Free Trial Details</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="free_trial_period" placeholder="" value="{{old('free_trial_period')}}">
                                                    <div class="input-group-prepend">
                                                        <select name="free_trial_unit" id="" class="form-control bg-light">
                                                            <option value="day">Day(s)</option>
                                                            <option value="week">Week(s)</option>
                                                            <option value="month">Month(s)</option>
                                                        </select>
                                                    </div><!-- input-group-prepend -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" name="submit" value="Save Plan Details" class="btn btn-sm btn-success">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
