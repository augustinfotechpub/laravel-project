@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container px-md-0">
            <ul class="breadcrumb az-content-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.subscriptions_plan.list') }}">Subscriptions Plan</a></li>
                <li class="breadcrumb-item">Edit Subscription plan</li>
            </ul>
        </div>
    </div>
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                @include('admin.layouts.partials.flash_messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header alert-primary">
                                <div class="row">
                                    <div class="col-md-6"><h3>Edit Subscription Plan</h3></div>
                                    <div class="col-md-6">
                                        <a href="{{ route('admin.subscriptions_plan.list') }}"
                                           class="float-right btn btn-warning mb-0">
                                            <i class="fa fa-arrow-left"> </i> Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" action="{{ route('admin.subscription_plan.update') }}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $subscription_plan->id }}">
                                    <input type="hidden" name="product_key" value="{{ $subscription_plan->product_key }}">
                                    <input type="hidden" name="price_key" value="{{ $subscription_plan->price_key }}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Plan Title</label>
                                                <input type="text" name="title" value="{{ $subscription_plan->title }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Price</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="text" class="form-control"
                                                           aria-label="Amount (to the nearest dollar)"
                                                           value="{{ $subscription_plan->price }}"
                                                           name="price">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">.00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Interval</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="interval_value"
                                                           value="{{ $subscription_plan->interval_value }}"
                                                           placeholder="">
                                                    <div class="input-group-prepend">
                                                        <select name="interval_unit" id=""
                                                                class="form-control bg-light">
                                                            <option
                                                                value="day" {{ $subscription_plan->interval_unit == 'day'?'selected':null }}>
                                                                Day(s)
                                                            </option>
                                                            <option
                                                                value="week" {{ $subscription_plan->interval_unit == 'week'?'selected':null }}>
                                                                Week(s)
                                                            </option>
                                                            <option
                                                                value="month" {{ $subscription_plan->interval_unit == 'month'?'selected':null }}>
                                                                Month(s)
                                                            </option>
                                                        </select>
                                                    </div><!-- input-group-prepend -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Free Trial Details</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="free_trial_period"
                                                           placeholder="" value="{{ $subscription_plan->free_trial_period }}">
                                                    <div class="input-group-prepend">
                                                        <select name="free_trial_unit" id=""
                                                                class="form-control bg-light">
                                                            <option
                                                                value="day" {{ $subscription_plan->free_trial_unit == 'day'?'selected':null }}>
                                                                Day(s)
                                                            </option>
                                                            <option
                                                                value="week" {{ $subscription_plan->free_trial_unit == 'week'?'selected':null }}>
                                                                Week(s)
                                                            </option>
                                                            <option
                                                                value="month" {{ $subscription_plan->free_trial_unit == 'month'?'selected':null }}>
                                                                Month(s)
                                                            </option>
                                                        </select>
                                                    </div><!-- input-group-prepend -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" name="submit" value="Save Plan Details"
                                                   class="btn btn-sm btn-success">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
