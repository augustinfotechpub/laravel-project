<div xmlns:wire="http://www.w3.org/1999/xhtml">
    <div class="row mt-4">
        <div class="col-lg-8 order-lg-2 mb-4 mb-lg-0">
            @include('livewire.widgets.home-screen-chart')
        </div>
        <div class="col-lg-4">
            <div class="card shadow border-0">
                <div class="card-header">
                    
                        <h5 class="tippy card-title" id="{{ getAlertId('profile_alert') }}" style="display: inline-block;"> {{ $profile['companyName'] }} Profile</h5>
                        @if (!(empty(@$alertContent)))
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertContent) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach(@$alertContent as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item['content'] !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item['content'] !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$alertContent[0]['content'] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                    
                </div>
                <div class="card-body scrollable-card-body" style="height: 350px">
                    <div class="w-full">Sector: <strong>{{ $profile['sector'] }}</strong></div>
                    <div class="w-full">Industry: <strong>{{ $profile['industry'] }}</strong></div>
                    <div class="w-full">Employees: <strong>{{ $profile['fullTimeEmployees'] }}</strong></div>
                    <p class="w-full mt-3 text-justify">
                        {{ $profile['description'] }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-md-6 mb-6 mb-md-0">
            <div class="" style="height: 583px; margin-top:-7px;">
                @include('livewire.widgets.stock-summary')
            </div>
        </div>
        <div class="col-md-6 mb-6 mb-md-0">
            
        </div>
    </div>

    
    {{--<livewire:active-industry-chart/>--}}
</div>
