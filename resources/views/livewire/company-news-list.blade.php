<div class="row">
    <div class="col-md-12">
        <div class="card shadow">
            <div class="card-header card-header-bg border-0 text-light">
                <h4 class="tippy mb-0" id="{{ getAlertId('news_feed_alert') }}">News Feed</h4>
                @if (!(empty(@$alertContent)))
                    <i class="bx bx-info-circle alertSlider has-tooltip">
                        @if( count(@$alertContent) > 1 )
                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                <div id="alertCarousel3" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        @foreach(@$alertContent as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item['content'] !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item['content'] !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarousel3" data-slide="prev">
                                    <i class="bx bx-skip-previous"></i>
                                    <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarousel3" data-slide="next">
                                    <i class="bx bx-skip-next"></i>
                                    <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        @else
                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                {!! @$alertContent[0]['content'] !!}
                            </div>
                        @endif
                    </i>
                @endif
            </div>
            <div class="list-group card-body scrollable-card-body p-0 rounded-0 border-start-0 border-end-0"
                 style="text-align: justify;margin: 15px 0 15px 0;">
                @forelse($newsList as $news)
                    <a href="{{ $news['url'] }}" target="_blank" class="list-group-item list-group-item-action rounded-0 border-start-0 border-end-0">
                        <img src="{{ $news['image'] }}" class="img-thumbnail border-0 p-0 mb-3">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{ $news['title'] }}</h5>
                        </div>
                        <p class="mb-1">{{ \Illuminate\Support\Str::limit($news['text'],100,'.') }}<span
                                class="text-info">read more.</span></p>
                            <small class="text-muted">{{ \Carbon\Carbon::parse($news['publishedDate'])->diffForHumans() }}</small>
                    </a>
                @empty

                @endforelse
            </div>
        </div>
    </div>
</div>
