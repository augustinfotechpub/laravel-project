<div>
    <style>
        .iframe-resp {
            height: inherit;
            width: -webkit-fill-available;
        }
    </style>
    <div class="row mt-4">
        {{--<div class="col-md-6">
            <div class="table__heading">
                <h4>Economic Calendar</h4>
            </div>
            <div class="table-responsive scrollable-card-body mb-5 mh-400 shadow">
                <table class="table table-sort table-hover scroll-table">
                    <tr>
                        <th>Event</th>
                        <th>Date</th>
                        <th>Country</th>
                        <th>Actual</th>
                        <th>Previous</th>
                        <th>Change</th>
                        <th>Percentage Change</th>
                        <th>Estimate</th>
                    </tr>
                    @foreach($calendars['economicCalender'] as $event)
                        <tr>
                            <td>{{ $event['event']??'--' }}</td>
                            <td>{{ formatDate($event['date']) }}</td>
                            <td>{{ $event['country']??'--' }}</td>
                            <td>{{ $event['actual']??'--' }}</td>
                            <td>{{ $event['previous']??'--' }}</td>
                            <td>{{ $event['change']??'--' }}</td>
                            <td>{{ number_format($event['changePercentage'],2)??'--' }}</td>
                            <td>{{ $event['estimate']??'--' }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>--}}
        <div class="col-md-12">
            <h3 class="font-semibold text-3xl text-center">Economic Calendar</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('calendar.economic') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>


        <div class="col-md-12">
            <div class="table__heading">
                <h4>Earnings Calendar</h4>
                <div>
                    <label for="fromDate">From Date</label>
                    <input type="date" id="fromDate" class="form-control-sm text-black" wire:model="fromDate"/>

                    <label for="toDate">To Date</label>
                    <input type="date" id="toDate" class="form-control-sm text-black" wire:model="toDate"/>
                    <button class="btn btn-sm btn-primary" wire:click="getEarningCalendar" wire:loading.attr="disabled">Submit</button>
                </div>
            </div>
            <div class="table-responsive scrollable-card-body mh-400 shadow" wire:init="getEarningCalendar">
                @if($this->calendarsSetting['initEarningCalendar'])
                    <table class="table table-sort table-hover scroll-table p-3">
                        <tr>
                            <th>SYMBOL</th>
                            <th>EPS ESTIMATED</th>
                            <th>EPS REPORTED</th>
                            <th>REVENUE FORECAST</th>
                            <th>REVENUE</th>
                            <th>DATE</th>
                        </tr>
                        @foreach(array_reverse($calendars['earningCalendar']) as $event)
                            <tr>
                                <td>{{ $event['symbol']??'--' }}</td>
                                <td>{{ $event['epsEstimated']??'--' }}</td>
                                <td>{{ $event['eps']??'--' }}</td>
                                <td>{{ $event['revenueEstimated']??'--' }}</td>
                                <td>{{ $event['revenue']??'--' }}</td>
                                <td>{{ $event['date'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    @include('front.layouts.partials.loader_b')
                @endif
            </div>
        </div>
        @if($this->calendarsSetting['initEarningCalendar'])
            <div class="col-md-12" wire:init="getStockDividendCalendar">
                <div class="table__heading">
                    <h4>Dividend Calendar</h4>
                </div>
                <div class="table-responsive scrollable-card-body mb-5 mh-400 shadow">
                    @if($this->calendarsSetting['initStockDividendCalendar'])
                        <table class="table table-sort table-hover scroll-table p-3">
                            <tr>
                                <th>Declaration Date</th>
                                <th>Record Date</th>
                                <th>Date of Issue</th>
                                <th>Ex-Dividend Date</th>
                                <th>Adj Dividend</th>
                                <th>Symbol</th>
                                <th>Date</th>
                            </tr>
                            @foreach($calendars['stockDividendCalendar'] as $event)
                                <tr>
                                    <td>{{ formatDate($event['declarationDate']) }}</td>
                                    <td>{{ formatDate($event['recordDate']) }}</td>
                                    <td>{{ formatDate($event['paymentDate']) }}</td>
                                    <td>{{ $event['label']??'--' }}</td>
                                    <td>{{ $event['adjDividend']??'--' }}</td>
                                    <td>{{ $event['symbol']??'--' }}</td>
                                    <td>{{ formatDate($event['date']) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
            </div>
        @endif
        @if($this->calendarsSetting['initStockDividendCalendar'])
            <div class="col-md-12">
                <div class="table__heading">
                    <h4>Stock Split Calendar</h4>
                </div>
                <div class="table-responsive scrollable-card-body mb-5 mh-400 shadow" wire:init="getStockSplitCalendar">
                    @if($this->calendarsSetting['initStockSplitCalendar'])
                        <table class="table table-sort table-hover scroll-table p-3">
                            <tr>
                                <th>Label</th>
                                <th>Symbol</th>
                                <th>Numerator</th>
                                <th>Denominator</th>
                                <th>Date</th>
                            </tr>
                            @foreach($calendars['stockSplitCalendar'] as $event)
                                <tr>
                                    <td>{{ $event['label']??'--' }}</td>
                                    <td>{{ $event['symbol']??'--' }}</td>
                                    <td>{{ $event['numerator']??'--' }}</td>
                                    <td>{{ $event['denominator']??'--' }}</td>
                                    <td>{{ formatDate($event['date']) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
            </div>
        @endif
        @if($this->calendarsSetting['initStockSplitCalendar'])
            <div class="col-md-12">
                <div class="table__heading">
                    <h4>IPO Calendar</h4>
                </div>
                <div class="table-responsive scrollable-card-body mb-5 mh-400 shadow" wire:init="getIPOCalendar">
                    @if($this->calendarsSetting['initGetIPOCalendar'])
                        <table class="table table-sort table-hover scroll-table p-3">
                            <tr>
                                <th>Company</th>
                                <th>Symbol</th>
                                <th>Exchange</th>
                                <th>Actions</th>
                                <th>Shares</th>
                                <th>PriceRange</th>
                                <th>MarketCap</th>
                                <th>Date</th>
                            </tr>
                            @foreach($calendars['getIPOCalendar'] as $event)
                                <tr>
                                    <td>{{ $event['company']??'--' }}</td>
                                    <td>{{ $event['symbol']??'--' }}</td>
                                    <td>{{ $event['exchange']??'--' }}</td>
                                    <td>{{ $event['actions']??'--' }}</td>
                                    <td>{{ number_format($event['shares']) }}</td>
                                    <td>{{ $event['priceRange']??'--' }}</td>
                                    <td>{{ currencyFormat($event['marketCap']) }}</td>
                                    <td>{{ formatDate($event['date']) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
            </div>
        @endif
        @if($this->calendarsSetting['initGetIPOCalendar'])
            <div class="col-md-12">
                <div class="table__heading">
                    <h4>Company Historical Earning Calendar</h4>
                </div>
                <div class="table-responsive scrollable-card-body mb-5 mh-400 shadow"
                     wire:init="getCompanyHistoricalEarningCalendar">
                    @if($this->calendarsSetting['initCompanyHistoricalEarningCalendar'])
                        <table class="table table-sort table-hover scroll-table p-3">
                            <tr>
                                <th>Symbol</th>
                                <th>Eps</th>
                                <th>EpsEstimated</th>
                                <th>Date</th>
                            </tr>
                            @foreach($calendars['companyHistoricalEarningCalendar'] as $event)
                                <tr>
                                    <td>{{ $event['symbol']??'--' }}</td>
                                    <td>{{ $event['eps']??'--' }}</td>
                                    <td>{{ $event['epsEstimated']??'--' }}</td>
                                    <td>{{ formatDate($event['date']) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
            </div>
        @endif
        @if($this->calendarsSetting['initCompanyHistoricalEarningCalendar'])
            <div class="col-md-12">
                <div class="table__heading">
                    <h4>Industry Information</h4>
                </div>
                <div class="table-responsive scrollable-card-body mh-400 shadow" wire:init="getIndustryInformation">
                    @if($this->calendarsSetting['initIndustryInformation'])
                        <table class="table table-sort table-hover scroll-table p-3">
                            <tr>
                                <th>Company Name</th>
                                <th>Symbol</th>
                                <th>Industry</th>
                                <th>Price</th>
                            </tr>
                            @foreach($industryInformations as $index=>$industry)
                                <tr>
                                    <td><a href="#" data-bs-toggle="modal"
                                           data-bs-target="#industry-{{$index}}"> {{ $industry['companyName'] }} </a>
                                    </td>
                                    <td>{{ $industry['symbol'] }}</td>
                                    <td>{{ $industry['industry'] }}</td>
                                    <td>{{ currencyFormat($industry['price']) }}</td>
                                </tr>
                            @endforeach
                        </table>
                        @foreach($industryInformations as $index=>$industry)
                            <div class="modal fade" id="industry-{{$index}}" tabindex="-1"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"
                                                id="exampleModalLabel">Industry Information -
                                                ({{ $industry['companyName'] }})</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-hover table-sort scroll-table mb-0">
                                                <tbody>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Symbol</td>
                                                    <td class="small text-start">{{ $industry['symbol'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Company Name</td>
                                                    <td class="small text-start">{{ $industry['companyName'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Market Cap
                                                        <small>(in million)</small>
                                                    </td>
                                                    <td class="small text-start">{{ currencyInMillion($industry['marketCap']) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Sector</td>
                                                    <td class="small text-start">{{ $industry['sector'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Industry</td>
                                                    <td class="small text-start">{{ $industry['industry'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Beta</td>
                                                    <td class="small text-start">{{ number_format($industry['beta'],2) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Price</td>
                                                    <td class="small text-start">{{ currencyFormat($industry['price']) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Last Annual Dividend</td>
                                                    <td class="small text-start">{{ currencyFormat($industry['lastAnnualDividend']) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Volume
                                                        <small>(in million)</small>
                                                    </td>
                                                    <td class="small text-start">{{ currencyInMillion($industry['volume']) }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Exchange</td>
                                                    <td class="small text-start">{{ $industry['exchange'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Exchange Short Name</td>
                                                    <td class="small text-start">{{ $industry['exchangeShortName'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-bolder small ps-3">Country</td>
                                                    <td class="small text-start">{{ $industry['country'] }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
            </div>
        @endif
    </div>
    <div class="row mt-4">
        <div wire:loading>
            @include('front.layouts.partials.loader_b')
        </div>
    </div>
</div>



