@if(in_array($currentStatement,['income-statement','balance-sheet','cash-flow']))

    @if(count($statement) > 0)
        <div class="row mx-0" xmlns:wire="http://www.w3.org/1999/xhtml">
            <div class="table__heading">
                <div class="col-md-4">
                    @if($currentStatement=='cash-flow')
                        <h4><span class="tippy mx-auto px-0" id="{{ getAlertId('cash_flow_statement_table_alert') }}">{{ $statement['header'] }}</span>     
                        @if( !empty(@$this->statement['alert_table_header']['cash_flow_statement_table_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$this->statement['alert_table_header']['cash_flow_statement_table_alert']) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel_{{$currentStatement}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach(@$this->statement['alert_table_header']['cash_flow_statement_table_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel_{{$currentStatement}}" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel_{{$currentStatement}}" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$this->statement['alert_table_header']['cash_flow_statement_table_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                        </h4>
                        <small>{{ $statement['sub_title']??null }}</small>
                    @elseif($currentStatement=='income-statement')   
                        <h4><span class="tippy mx-auto px-0" id="{{ getAlertId('income_statement_table_alert') }}">{{ $statement['header'] }}</span>
                        @if( !empty(@$this->statement['alert_table_header']['income_statement_table_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if(count(@$this->statement['alert_table_header']['income_statement_table_alert']) > 1)
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel_{{$currentStatement}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">

                                                @foreach(@$this->statement['alert_table_header']['income_statement_table_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel_{{$currentStatement}}" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel_{{$currentStatement}}" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$this->statement['alert_table_header']['income_statement_table_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                        </h4>
                        <small>{{ $statement['sub_title']??null }}</small>
                    @else
                        <h4><span class="tippy mx-auto px-0" id="{{ getAlertId('balance_sheet_table_alert') }}">{{ $statement['header'] }}</span>
                        @if( !empty(@$this->statement['alert_table_header']['balance_sheet_table_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$this->statement['alert_table_header']['balance_sheet_table_alert']) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel_{{$currentStatement}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">

                                                @foreach(@$this->statement['alert_table_header']['balance_sheet_table_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel_{{$currentStatement}}" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel_{{$currentStatement}}" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$this->statement['alert_table_header']['balance_sheet_table_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                        </h4>
                        <small>{{ $statement['sub_title']??null }}</small>        
                    @endif    
                </div>
                <div class="col-md-4 text-left">
                    <div class="dropdown">
                        <div class="dropdown-toggle item text-uppercase text-sm font-black" type="button"
                             id="dropdownMenuButton1" data-bs-toggle="dropdown"
                             aria-expanded="false">
                            View 
                            @if($showReverse == 'false' || $showReverse == false)
                                <small style="text-transform: capitalize">Accounting View</small> 
                            @else
                                <small style="text-transform: capitalize">Standard View</small>  
                            @endif       
                        </div>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li>
                                <a class="dropdown-item" href="javascript:void(0)"
                                    wire:click="changeView('true','{{ $currentStatement }}','{{ $period }}')">STANDARD</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="javascript:void(0)"
                                    wire:click="changeView('false','{{ $currentStatement }}','{{ $period }}')">ACCOUNTING</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="btn-group float-end">
                        <button class="btn btn-{{ $period == 'quarter'?"primary":"secondary"}} btn-sm"
                                wire:click="changeStatement('{{ $currentStatement }}','quarter')" {{--wire:loading.remove="getStatements"--}}>
                            Quarterly
                        </button>
                        <button class="btn btn-{{ $period == 'FY'?"primary":"secondary"}} btn-sm"
                                wire:click="changeStatement('{{ $currentStatement }}','FY')">
                            Annual
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 financial">
                <div class="table-responsive scrollable-card-body shadow" style="height: auto;">
                    <table class="top-scrollbars table table-hover table-sort scroll-table mb-0 p-2 ttm-hold">
                        <thead>
                        <tr class="border-0 border-bottom-0 statement-tr">
                            @forelse($statement['headerColumns'] as $headerColumn)
                                @if($loop->first)
                                    <th>Breakdown</th>
                                    
                                    @if(in_array($currentStatement,['income-statement','cash-flow']) && ($showReverse == 'false'))
                                        <th>TTM</th>
                                    @endif
                                @endif
                                <th>{{ formatDate($headerColumn, 'n/d/Y') }}</th>

                                @if($loop->last)
                                    @if(in_array($currentStatement,['income-statement','cash-flow']) && $showReverse == 'true' )
                                        <th>TTM</th>
                                    @endif
                                @endif

                            @empty
                            <h3 class="rotateempty">NO RECORD FOUND</h3>    
                            @endforelse
                        </tr>
                        </thead>
                        <tbody>
                        @include('livewire.financials.partials.financials_table_body')
                    </table>
                </div>
            </div>
        </div>
    @else
        <h3 class="text-center">Statements Not found!</h3>
    @endif
@endif
