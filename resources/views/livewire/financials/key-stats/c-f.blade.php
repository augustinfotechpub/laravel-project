<div class="row" wire:init="initSummary" xmlns:wire="http://www.w3.org/1999/xhtml">
    @if($loadStatus == 200)
        <div class="col-md-12">
            <div class="section-header"  style="display:inline-flex;">
                <div>
                    <h4 class="mb-0">Cash Flow</h4>
                    <p class="mb-0">All amount in millions</p>
                </div>
                <div style="margin-left:30px; margin-top:3px;">
                    @if(isset($year))
                    <select id="yearcf" style="color: red;">
                        <option value="" selected disabled>Select Year</option>
                        @foreach($year as $year)
                        <option value="{{$year}}"> {{$year}}</option>
                        @endforeach    
                    </select> 
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6" id="cfdatatable">
            <div class="table-responsive scrollable-card-body mb-3 shadow pb-3"
                 style="height:360px;">
                <table class="table table-hover table-sort scroll-table mb-0 p-2">
                    <thead>
                    <tr class="border-0 border-bottom-0">
                        <th>Key</th>
                        <th>Value</th>
                        <th class="text-end">Chart</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($summary[0] as $key=>$value)
                        @if(in_array($loop->index ,[1,2,36,37]))
                            @continue
                        @endif
                        @if ( in_array($key,['depreciationAndAmortization','changeInWorkingCapital','capitalExpenditure','netCashProvidedByOperatingActivities','otherInvestingActivites','otherFinancingActivites','operatingCashFlow']))
                            <tr>
                                <td class="tippy" id="{{ getAlertId($key,'_key_stats_CS_alert') }}">{{ ucfirst(strUpperToSpace($key)) }}</td>
                                <td>{{ currencyInMillion($value) }}</td>
                                <td class="text-end">
                                    @if(!in_array($loop->index,[0, 1, 2, 3, 4, 5, 6, 7,38,39,40,41]))
                                        <button
                                            class="btn switcher switcher-item-for-cf btn-sm px-1 py-0 text-info {{ $loop->first?'highlighted ':null }}"
                                            data-key="{{ $key }}">
                                            <i class="fa fa-chart-line"></i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row mb-2 shadow pb-4 me-0">
                <div class="col-md-12 py-1" style="background-color: #bcd9f1; padding: 2px 10px;">
                    <div class="float-right">
                        <div class="btn-group">
                            <button
                                class="btn btn-{{ $period == 'quarter'?'primary':'secondary' }} btn-sm custom-btn-sm"
                                wire:click="changePeriod('cf','quarter')" wire:loading.remove="changePeriod">
                                Quarterly
                            </button>
                            <button
                                class="btn btn-{{ $period == 'quarter'?'secondary':'primary' }} btn-sm custom-btn-sm"
                                wire:click="changePeriod('cf','year')" wire:loading.remove="changePeriod">Yearly
                            </button>

                        </div>
                        <button class="btn btn-secondary btn-sm custom-btn-sm" wire:loading
                                wire:target="changePeriod">
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </button>
                    </div>
                </div>
                <div class="col-md-12" id="cash-flow-chart" style="position: relative;"
                     wire:init="getChartData('cf')">
                </div>
            </div>
        </div>
    @elseif($loadStatus == 101)
        <div class="col-md-12">
            @include('front.layouts.partials.loader_b')
        </div>
    @endif
</div>

<script>
    $(document).on("change","body #yearcf",function()
    {
    var year = $(this).val(); 
    console.log(year);
    if(year){
     $.ajax({
       type:"GET",
       data:"year="+year,
       url:"{{url('yeardatacf')}}",
       success:function(data)
       {
           $('#cfdatatable').html("");
           $('#cfdatatable').html(data);
       }
           });
           }
    });
</script>