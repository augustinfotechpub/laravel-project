@php
    $summary=json_decode($summary,true)
@endphp        
        <div class="table-responsive scrollable-card-body mb-3 shadow pb-3"
        style="height:360px;">
        <table class="table table-hover table-sort scroll-table mb-0 p-2">
            <thead>
            <tr class="border-0 border-bottom-0">
                <th>Key</th>
                <th>Value</th>
                <th class="text-end">Chart</th>
            </tr>
            </thead>
            <tbody>
            @foreach($summary[0] as $key=>$value)
                @if(in_array($loop->index ,[1,2,36,37]))
                    @continue
                @endif
                @if ( in_array($key,['depreciationAndAmortization','changeInWorkingCapital','capitalExpenditure','netCashProvidedByOperatingActivities','otherInvestingActivites','otherFinancingActivites','operatingCashFlow']))
                    <tr>
                        <td class="tippy" id="{{ getAlertId($key,'_key_stats_CS_alert') }}">{{ ucfirst(strUpperToSpace($key)) }}</td>
                        <td>{{ currencyInMillion($value) }}</td>
                        <td class="text-end">
                            @if(!in_array($loop->index,[0, 1, 2, 3, 4, 5, 6, 7,38,39,40,41]))
                                <button
                                    class="btn switcher switcher-item-for-cf btn-sm px-1 py-0 text-info {{ $loop->first?'highlighted ':null }}"
                                    data-key="{{ $key }}">
                                    <i class="fa fa-chart-line"></i>
                                </button>
                            @endif
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
        </div>
