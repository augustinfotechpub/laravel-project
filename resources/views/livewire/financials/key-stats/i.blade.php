@php
    $summary=json_decode($summary,true)
@endphp
           <div class="table-responsive scrollable-card-body mb-3 shadow pb-3"
                    style="height:360px;">
                    @if(isset($summary[0]))
                        <table class="table table-hover table-sort scroll-table mb-0 p-2">
                            <thead>
                            <tr class="border-0 border-bottom-0">
                                <th>Key</th>
                                <th>Value</th>
                                <th class="text-end">Chart</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($summary[0] as $key=>$value)
                                @if (in_array($key,['revenue','costOfRevenue','grossProfit','interestExpense','operatingExpenses','operatingIncome','netIncome']))
                                    <tr>
                                        <td class="tippy" id="{{ getAlertId($key,'_key_stats_IS_alert') }}">{{ ucfirst(strUpperToSpace($key)) }}</td>
                                        <td>{{ currencyInMillion($value) }}</td>
                                        <td class="text-end">
                                            @if(!in_array($loop->index,[0, 1, 2, 3, 4, 5,6,7, 34, 35,36,37]))
                                                <button
                                                    class="btn switcher switcher-item-for-is btn-sm px-1 py-0 text-info {{ $loop->first?'active ':null }}"
                                                    data-key="{{ $key }}">
                                                    <i class="fa fa-chart-line"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @empty
                                <tr>
                                    <td colspan="3">
                                        <h3>No records found.</h3>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    @else
                        <h3>Income statement not found.</h3>
                    @endif
            </div>

        