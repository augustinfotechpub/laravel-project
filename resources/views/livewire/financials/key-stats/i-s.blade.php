<div class="row mt-3" wire:init="initSummary">
    <div class="col-md-12">
        <div class="section-header"  style="display:inline-flex;">
            <div>
                <h4 class="mb-0">Income Statement</h4>
                <p class="mb-0">All amount in millions</p>
            </div>
            <div style="margin-left:30px; margin-top:3px;">   
                @if(isset($year))
                <select id="yearis" style="color: red;">
                    <option value="" selected disabled>Select Year</option>
                    @foreach($year as $year)
                    <option value="{{$year}}"> {{$year}}</option>
                    @endforeach    
                </select> 
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6" id="isdatatable">
                <div class="table-responsive scrollable-card-body mb-3 shadow pb-3"
                     style="height:360px;">
                    @if($initSummary)
                        @if(isset($summary[0]))
                            <table class="table table-hover table-sort scroll-table mb-0 p-2">
                                <thead>
                                <tr class="border-0 border-bottom-0">
                                    <th>Key</th>
                                    <th>Value</th>
                                    <th class="text-end">Chart</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($summary[0] as $key=>$value)
                                    @if (in_array($key,['revenue','costOfRevenue','grossProfit','interestExpense','operatingExpenses','operatingIncome','netIncome']))
                                        <tr>
                                            <td class="tippy" id="{{ getAlertId($key,'_key_stats_IS_alert') }}">{{ ucfirst(strUpperToSpace($key)) }}</td>
                                            <td>{{ currencyInMillion($value) }}</td>
                                            <td class="text-end">
                                                @if(!in_array($loop->index,[0, 1, 2, 3, 4, 5,6,7, 34, 35,36,37]))
                                                    <button
                                                        class="btn switcher switcher-item-for-is btn-sm px-1 py-0 text-info {{ $loop->first?'active ':null }}"
                                                        data-key="{{ $key }}">
                                                        <i class="fa fa-chart-line"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="3">
                                            <h3>No records found.</h3>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        @else
                            <h3>Income statement not found.</h3>
                        @endif
                    @else
                        @include('front.layouts.partials.loader')
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="row mb-2 shadow pb-4 me-0">
                    <div class="col-md-12 py-1" style="background-color: #bcd9f1; padding: 2px 10px;">
                        <div class="float-right">
                            <div class="btn-group">
                                <button
                                    class="btn btn-{{ $period == 'quarter'?'primary':'secondary' }} btn-sm custom-btn-sm"
                                    wire:click="changePeriod('is','quarter')" wire:loading.remove="changePeriod">
                                    Quarterly
                                </button>

                                <button
                                    class="btn btn-{{ $period == 'quarter'?'secondary':'primary' }} btn-sm custom-btn-sm"
                                    wire:click="changePeriod('is','FY')" wire:loading.remove="changePeriod">Yearly
                                </button>
                            </div>
                            <button class="btn btn-secondary btn-sm custom-btn-sm" wire:loading
                                    wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                Loading...
                            </button>
                        </div>
                    </div>
                    @if($initSummary)
                        <div class="col-md-12" id="income-statement-chart" style="position: relative;"
                             wire:init="getChartData('is')">
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).on("change","body #yearis",function()
    {
     var year = $(this).val(); 
     if(year){
     $.ajax({
       type:"GET",
       data:"year="+year,
       url:"{{url('yeardatais')}}",
       success:function(data)
       {
           $('#isdatatable').html("");
           $('#isdatatable').html(data);
       }
           });
           }
   });
</script>