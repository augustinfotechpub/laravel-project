<div class="row" wire:init="initFinancials">
    @if($initFinancials)
        @include('livewire.financials.partials.navbar')
        <div class="col-md-12" {{--wire:loading.remove="changeStatement"--}}>
            @include('livewire.financials.partials.financials-summary')
            @include('livewire.financials.income-balance-cashflow')
            {{-- Comprehensive Income, Shareholders Equity --}}
            @include('livewire.financials.comprehensive-or-shareholders')
            {{-- Comparables --}}
            @if(in_array($currentStatement,['standard-size-compare','common-size-compare', 'common-size-compare-vertical', 'common-size-compare-horizontal']))
                <livewire:financials.comparables.comparables/>
            @endif
        </div>
        <div class="col-md-12" wire:loading>
            @include('front.layouts.partials.loader_b')
        </div>
    @else
        @include('front.layouts.partials.loader_b')
    @endif
</div>
