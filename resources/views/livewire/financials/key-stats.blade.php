<div class="col-md-12" wire:init="getReports">
    @if($status == 'false')
        @include('front.layouts.partials.loader_b')
    @elseif($status == 'true')
        <div class="section-header">
            <button class="btn btn-{{ $period=='quarter'?'primary':'secondary' }} btn-sm custom-btn-sm"
                    wire:click="changePeriod('quarter')" wire:loading.remove="changePeriod">
                Quarterly
            </button>
            <button class="btn btn-{{ $period=='FY'?'primary':'secondary' }} btn-sm custom-btn-sm"
                    wire:click="changePeriod('FY')" wire:loading.remove="changePeriod">
                Annual
            </button>
            <button class="btn btn-sm btn-primary" type="button" disabled wire:loading="changePeriod">
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>
        </div>
        <div class="col-md-12 mt-3">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        @foreach($reports as $report)
                            <a class="list-group-item list-group-item-action cursor-pointer tabLinks"
                               onclick="openContent(event,'tabContentId{{ $loop->iteration }}')">
                                {{ $report['title'] }}
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6">
                    @foreach($reports as $report)
                        <div class="tabContent" id="tabContentId{{ $loop->iteration }}"
                             style="display:{{ $loop->iteration == 1?'block':"none" }}">{!! $report['html'] !!}</div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
</div>
