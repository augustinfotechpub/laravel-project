<tbody>
    <?php
    /*echo "<pre>";
    print_r($this->statement);
    echo "</pre>";*/
    ?>
@foreach($this->statement['rows'] as $key=> $columns)
    <tr>
        @foreach($columns as $column)
            <td class="financials-td @if($loop->iteration == 1) @endif"
                id="{{ $loop->first?getAlertId($column['column'],$this->statement['alert_title']):'' }}">
                @if($loop->iteration == 1)
                    @php($title = str_repeat('&nbsp; ',$column['indentation']).$column['title'])
                    @php($titleOnly = $column['title'])
                    <span class="tippy" id="{{ getAlertId($column['column'],$this->statement['alert_title']) }}">{!! $title !!}</span>
                    @if( $title != '' )
                        @if( @$this->statement['alert_content'][$column['title']][0]['content'] != '')
                            @if($key >= 10)
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip bottom-table">
                            @else
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip ">
                            @endif
                                @if(count(@$this->statement['alert_content'][$column['title']]) > 1)
                                    <div class="tooltip-content">
                                        <div id="alertCarouselfinancial_{{$column['column']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">

                                                @foreach(@$this->statement['alert_content'][$column['title']] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item['content'] !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item['content'] !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarouselfinancial_{{$column['column']}}" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarouselfinancial_{{$column['column']}}" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! @$this->statement['alert_content'][$column['title']][0]['content'] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                        @if(($title=='Degree of Operating Leverage') || ($title=='Degree of Financial Leverage') || ($title=='Degree of Total Leverage'))
                        <i class="bx bx-info-circle financialSlider ratioSlider has-tooltip bottom-table">
                            <div class="tooltip-content">
                              @switch($title)
                                @case('Degree of Operating Leverage')
                                % change in operatingIncome (between previous period and current period) / % change in totalRevenue (between previous period and current period)
                                  @break
                                @case('Degree of Financial Leverage')
                                % change in netIncomeFromContinuingOps (between previous period and current period) / % change in operatingIncome (between previous period and current period)
                                @break

                                @case('Degree of Total Leverage')
                                DOL X DFL
                                @break
                                @default
                              @endswitch
                            </div>
                        </i>
                        @endif
                    @endif
                @else
                    @if( !empty($title) )

                        @if(!in_array( $titleOnly, ["EPS", "Degree of Operating Leverage", "Degree of Financial Leverage", "Degree of Total Leverage"]))
                            {!! amount($column) !!}
                        @else
                            <!-- EPS -->
                            @if(in_array( $titleOnly, ["Degree of Operating Leverage", "Degree of Financial Leverage", "Degree of Total Leverage"])) 
                                @php($column = str_replace('-','',$column)) 
                            @endif
                            @if($showReverse=='false')
                                @if($loop->index != 1)
                                    {!! formatAmount($column, true) !!}      
                                @else
                                    @if(in_array( $titleOnly, ["Degree of Operating Leverage", "Degree of Financial Leverage", "Degree of Total Leverage"]))
                                        --
                                    @else
                                        {!! formatAmount($epsttm) !!}    
                                    @endif    
                                @endif     
                            @else   
                                @if($loop->index != 11)
                                    {!! formatAmount($column) !!}      
                                @else
                                    @if(in_array( $titleOnly, ["Degree of Operating Leverage", "Degree of Financial Leverage", "Degree of Total Leverage"]))
                                        {!! formatAmount($column, true) !!} 
                                    @else
                                        {!! formatAmount($epsttm) !!}    
                                    @endif
                                        
                                @endif   
                            @endif        
                        @endif 
                    @endif
                @endif
            </td>
        @endforeach
    </tr>
    {{--@if(in_array($columns[0]['column'],KEYS_FOR_NEXT_BLANK_ROW[$currentStatement]))
        <tr>
            {!! str_repeat('<td class="financials-td" style="background-color: aliceblue;"><span>&nbsp;</span></td>',count($columns)) !!}
        </tr>
    @endif--}}
@endforeach
</tbody>
