<tbody>
@foreach($comparableStatementRows as $key=> $columns)
    <tr>
        @foreach($columns as $column)
            <td class="{{ $loop->iteration == 1?'tippy':'' }} financials-td"
                id="{{ $loop->iteration ==1? getAlertId($column['column'],'_vertical_'.$column['alert_title']):'' }}">
                @if($loop->iteration == 1)
                    @php ($titleOnly = $column['title'])
                    <span class="tippy" id="{{ getAlertId($column['column'].'_',$column['alert_title']) }}">{!! $column['title'] !!}</span>
                    @if( isset($alert_content) && isset($alert_content[$column['title']]) && count($alert_content[$column['title']]) > 1)
                        @if($column['title'] != '')
                            @if($key >= 10)
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip bottom-table">
                            @else
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip ">
                            @endif
                        @endif
                        <div class="tooltip-content">
                            <div id="alertCarouselfinancial_{{$column['column']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">

                                    @foreach($alert_content[$column['title']] as $item)

                                        @if($loop->first)
                                            <div class="item active">
                                                {!! $item['content'] !!}
                                            </div>
                                        @else
                                            <div class="item">
                                                {!! $item['content'] !!}
                                            </div>
                                        @endif
                                        
                                    @endforeach
                                    
                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#alertCarouselfinancial_{{$column['column']}}" data-slide="prev">
                                <i class="bx bx-skip-previous"></i>
                                <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#alertCarouselfinancial_{{$column['column']}}" data-slide="next">
                                <i class="bx bx-skip-next"></i>
                                <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    @else
                        @if(isset($alert_content[$column['title']][0]))
                            @if($column['title'] != '')
                                @if($key >= 10)
                                <i class="bx bx-info-circle financialSlider alertSlider has-tooltip bottom-table">
                                @else
                                <i class="bx bx-info-circle financialSlider alertSlider has-tooltip ">
                                @endif
                            @endif
                            <div class="tooltip-content" style="padding: 15px;">    
                            
                                {!! $alert_content[$column['title']][0]['content'] !!}                            
                            </div>
                        @endif
                    @endif
                @else
                    @if($compareType =="standard")
                        @if(in_array( $titleOnly, ["EPS", "Degree of Operating Leverage", "Degree of Financial Leverage", "Degree of Total Leverage"]))
                            @php($column = str_replace('-','',$column))
                        @endif
                        @if( $columns[0]['title'] != '' )
                            {!! amount($column) !!}
                        @endif
                    @elseif($compareType == "common")
                        @if( $columns[0]['title'] != '' )
                            @if(percentage($column)!=0)
                                {!! percentage($column) !!}  
                            @else
                                --      
                            @endif      
                        @endif
                    @else
                        ---
                    @endif
                @endif
            </td>
        @endforeach
    </tr>
    {{--@if(in_array($columns[0]['column'],KEYS_FOR_NEXT_BLANK_ROW[$currentStatementName]))
        <tr>
            {!! str_repeat('<td class="financials-td" style="background-color: aliceblue;"><span>&nbsp;</span></td>',count($columns)) !!}
        </tr>
    @endif--}}
@endforeach
</tbody>
