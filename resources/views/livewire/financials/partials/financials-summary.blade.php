@if($currentStatement == 'key-stats')
    <livewire:financials.key-stats.i-s/>
    <livewire:financials.key-stats.b-s/>
    <livewire:financials.key-stats.c-f/>
    <livewire:financials.key-stats/>
@endif
