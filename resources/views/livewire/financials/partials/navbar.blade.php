<div class="col-md-12 border sm:py-2 md:py-3">
    <div class="options">
        {{--<div class="item text-uppercase text-sm"font-black >View</h5>--}}
        <div class="tippy item text-uppercase text-sm font-black {{ $currentStatement == 'income-statement'?'active':null }}"
             id="{{ getAlertId('income_statement_sub_navbar_alert') }}"
            wire:click="changeStatement('income-statement')">Income Statement / Statement of Operations</div>
        <div class="tippy item text-uppercase text-sm font-black {{ $currentStatement == 'balance-sheet'?'active':null }}"
             id="{{ getAlertId('balance_sheet_sub_navbar_alert') }}"
            wire:click="changeStatement('balance-sheet')">Balance Sheet / Statement of Financial Position</div>
        <div class="tippy item text-uppercase text-sm font-black {{ $currentStatement == 'cash-flow'?'active':null }}"
             id="{{ getAlertId('cash_flow_statement_sub_navbar_alert') }}"
             wire:click="changeStatement('cash-flow')">Cash Flow Statement / Statement of Cash Flows</div>
        <div class="dropdown">
            <div class="tippy dropdown-toggle item text-uppercase text-sm font-black"  type="button" id="comparablesDropdown" data-toggle="dropdown"
                aria-expanded="false">
                Comparables
            </div>
            <ul class="dropdown-menu" aria-labelledby="comparablesDropdown">
                <li>
                    <a class="dropdown-item text-lg {{ $currentStatement == 'standard-size-compare'?'active':null }}"
                       href="javascript:void(0)"
                       wire:click="changeStatement('standard-size-compare')">
                        STANDARD
                    </a>
                </li>
                <li>
                    <div class="tippy dropdown-toggle item text-lg"  type="button" id="comparablesDropdown1" data-bs-toggle="dropdown"
                       aria-expanded="false">
                       COMMON-SIZE
                   </div>
                    <ul class="dropdown-submenu" aria-labelledby="comparablesDropdown1">
                        <li>
                            <a class="dropdown-item text-md {{ $currentStatement == 'common-size-compare-vertical'?'active':null }}"
                                    href="javascript:void(0)"
                                    wire:click="changeStatement('common-size-compare-vertical')">
                                        VERTICAL
                            </a>
                        </li>
                        <li>
                            <div class="tippy dropdown-toggle item text-md"  type="button" id="comparablesDropdown2" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                HORIZONTAL
                            </div>
                            <ul class="dropdown-submenu" aria-labelledby="comparablesDropdown2">
                                <li>
                                    <a class="dropdown-item  text-sm {{ $currentStatement == 'common-size-compare'?'active':null }}"
                                    href="javascript:void(0)"
                                    wire:click="changeStatement('common-size-compare')">
                                        INTER-FIRM
                                    </a>
                                </li>
                                <li>    
                                    <a class="dropdown-item  text-sm {{ $currentStatement == 'common-size-compare-horizontal'?'active':null }}"
                                    href="javascript:void(0)"
                                    wire:click="changeStatement('common-size-compare-horizontal')">
                                        INTRA-FIRM
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
