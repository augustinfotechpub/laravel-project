@if(in_array($currentStatement,['comprehensive-income','shareholders-equity']))
    <div class="col-md-12">
        <div class="section-header">
            <div class="btn-group">
                <button class="btn btn-{{ $period=='quarter'?'primary':'secondary' }} btn-sm custom-btn-sm"
                        wire:click="changeStatement('{{ $currentStatement }}','quarter')"
                        wire:loading.remove="changeStatement">
                    Quarterly
                </button>
                <button class="btn btn-{{ $period=='FY'?'primary':'secondary' }} btn-sm custom-btn-sm"
                        wire:click="changeStatement('{{ $currentStatement }}','FY')"
                        wire:loading.remove="changeStatement">
                    Annual
                </button>
            </div>
            <button class="btn btn-sm btn-primary" type="button" disabled wire:loading="changeStatement">
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>
        </div>
    </div>
    <div wire:ignore.self class="col-md-12 mt-3">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    @foreach($statement as $report)
                        <a class="list-group-item list-group-item-action cursor-pointer tabLinks"
                           onclick="openContent(event,'tabContentId{{ $loop->iteration }}')">
                            {{ $report['title'] }}
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6">
                @foreach($statement as $report)
                    <div class="tabContent" id="tabContentId{{ $loop->iteration }}"
                         style="display:{{ $loop->iteration == 1?'block':"none" }}">{!! $report['html'] !!}</div>
                @endforeach
            </div>
        </div>
    </div>
@endif
