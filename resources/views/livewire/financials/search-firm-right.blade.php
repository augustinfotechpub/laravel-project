<div class="relatives company-search">
    {{----}}
    <div class="input-group">
        <span class="input-group-text" id="basic-addon1" style="background: aliceblue;border-right: 0px;">
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16" viewBox="0 0 15 16">
                <path fill="#4A90E2" fill-rule="evenodd"
                      d="M11.624 10.038l2.25 2.68a1 1 0 0 1-.123 1.41l-.904.758a1 1 0 0 1-1.409-.123L9.145 12.03c-.5.229-1.037.399-1.605.499C4.032 13.147.698 10.87.094 7.444-.51 4.018 1.844.74 5.352.12c3.508-.619 6.842 1.658 7.446 5.084a6.214 6.214 0 0 1-1.174 4.834zm-4.565-.24c1.964-.345 3.282-2.182 2.944-4.1-.338-1.92-2.205-3.194-4.17-2.848-1.964.346-3.282 2.183-2.944 4.102.338 1.919 2.205 3.193 4.17 2.847z"/>
            </svg>
        </span>
        <input type="text"
               id="search-input"
               class="form-control"
               placeholder="Comps Suggestions - Press Any Key"
               wire:model="query"
               wire:keydown.escape="resetEngine"
               autocomplete="off"
               style="border-left: 0; background: aliceblue;"/>
    </div>

    <div class="company-searches-container">
        <div wire:loading class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg list-group-item">
            Searching...
        </div>

        @if(!empty($query))
            <div class="fixed top-0 right-0 bottom-0 left-0" wire:click="resetEngine">Reset</div>
            <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg cursor-pointer"
                 id="search-hint-group">
                 @foreach($bestMatches as $i => $bestMatch)
                 {{-- @foreach($bestMatch['tickers'] as $ticker) --}}
                     <a href="javascript:void(0)"
                        class="search-hint list-group-item {{ $loop->first && $i==0 ? 'active' : null }} cursor-pointer"
                        wire:click="addCompany('{{ $bestMatch['cik'] }}','{{ $bestMatch['symbol'] }}')">
                         {{ $bestMatch['name'] }}
                         <small>
                             ({{ $bestMatch['symbol'] }})
                         </small>
                     </a>
                 {{-- @endforeach --}}
             @endforeach
            </div>
            @empty($bestMatches)
                <div class="list-group">
                    <div class="list-group-item">No results! &#128527;</div>
                </div>
            @endempty
        @endif
    </div>
    @push('scripts')
        <script type="text/javascript">
            $(document).mouseup(function (e) {
                var container = $("#search-hint-group");
                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });
        </script>
    @endpush
</div>
