<div class="row mx-0 mt-2" xmlns:wire="http://www.w3.org/1999/xhtml" xmlns:livewire>
    <div class="row">
    @if($currentType == 'vertical')
        <div class="col-md-6">
            <livewire:financials.search-firm/>
        </div>
        <div class="col-md-6">
            <livewire:financials.search-firm-right/>
        </div>
    @endif
    </div>
    <div class="row">
        @if($currentType == 'vertical')
        <div class="col-md-{{ $currentType == 'vertical'?8:12 }} text-center" style="margin-top:20px; margin-left:15%;">
        @else
        <div class="col-md-{{ $currentType == 'vertical'?8:12 }} text-center">
        @endif    
            <button class="btn btn-sm btn-primary" type="button" disabled wire:loading="changeCurrentStatementName"
                    wire:target="changeCurrentStatementName">
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>

            <div class="btn-group" wire:loading.remove="changeCurrentStatementName">
                <button class="btn btn-sm btn-{{ $currentStatementName == 'balance-sheet'?'primary':'secondary' }}"
                        wire:click="changeCurrentStatementName('balance-sheet','{{$currentType}}')">
                    Balance Sheet / Statement of Financial Position
                </button>
                <button class="btn btn-sm btn-{{ $currentStatementName == 'income-statement'?'primary':'secondary' }}"
                        wire:click="changeCurrentStatementName('income-statement','{{$currentType}}')">
                    Income Statement / Statement of Operations
                </button>
                <button class="btn btn-sm btn-{{ $currentStatementName == 'cash-flow'?'primary':'secondary' }}"
                        wire:click="changeCurrentStatementName('cash-flow','{{$currentType}}')">
                    Cash Flow Statement / Statement of Cash Flows
                </button>
            </div>

            <div class="btn-group float-end" wire:loading="changeType" wire:target="changeType">
                <button class="btn btn-sm btn-primary" type="button" disabled>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-12 min-h-screen">
        <div class="row mx-0" xmlns:wire="http://www.w3.org/1999/xhtml">
            <div class="table__heading">
                <div class="col-md-8">
                    @if(isset($horizontalStatement['rows']) || !is_null($comparableStatementRows))
                        @if($currentType == 'vertical')
                            <h4 class="tippy mx-auto px-0" id="{{ getAlertId('interfirm_comparable_alert') }}">HORIZONTAL INTER-FIRM COMMON-SIZE ANALYSIS
                                @if( !empty($alertsData['interfirm_comparable_alert']) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData['interfirm_comparable_alert']) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel1" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData['interfirm_comparable_alert'] as $item)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel1" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel1" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData['interfirm_comparable_alert'][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                @endif    
                            </h4>
                        @elseif($currentType == 'horizontal')
                            <h4 class="tippy mx-auto px-0" id="{{ getAlertId('intrafirm_comparable_alert') }}">HORIZONTAL INTRA-FIRM COMMON-SIZE ANALYSIS
                                @if( !empty($alertsData['intrafirm_comparable_alert']) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData['intrafirm_comparable_alert']) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel2" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData['intrafirm_comparable_alert'] as $item)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel2" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel2" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData['intrafirm_comparable_alert'][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                @endif    
                            </h4>
                        @else
                            <h4 class="tippy mx-auto px-0" id="{{ getAlertId('vertical_comparable_alert') }}">VERTICAL COMMON-SIZE ANALYSIS
                                @if( !empty($alertsData['vertical_comparable_alert']) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData['vertical_comparable_alert']) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel3" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData['vertical_comparable_alert'] as $item)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel3" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel3" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData['vertical_comparable_alert'][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                @endif    
                            </h4>
                        @endif
                    @endif    
                </div>
                <div class="col-md-4">
                    <div class="btn-group float-end" wire:loading="changePeriod" wire:target="changePeriod">
                        <button class="btn btn-sm btn-primary" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </button>
                    </div>
                    <div class="btn-group float-end" wire:loading.remove="changePeriod">
                        <button class="btn btn-{{ $period == 'quarter'?"primary":"secondary"}} btn-sm"
                                wire:click="changePeriod('{{ $compareType }}','quarter','{{$currentType}}')">
                            Quarterly
                        </button>
                        <button class="btn btn-{{ $period == 'FY'?"primary":"secondary"}} btn-sm"
                                wire:click="changePeriod('{{ $compareType }}','FY','{{$currentType}}')">
                            Annual
                        </button>
                    </div>
                    @if($currentType == 'ver' || $currentType == 'vertical')
                        <div class="dropdown dropdownInBlock">
                            <div class="tippy dropdown-toggle item text-uppercase text-sm font-black"  type="button" id="comparablesDropdown" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                {{$year}}
                            </div>
                            <ul class="dropdown-menu scrollable-menu" role="menu" style="height: auto;max-height: 200px; overflow-x: hidden;" aria-labelledby="comparablesDropdown">
                                @if($currentStatementName == 'balance-sheet')
                                    @foreach($allYearBS as $year)
                                        @if($period=='FY')
                                            <li>
                                                <a class="dropdown-item" wire:click="change('{{ $compareType }}','FY','{{$year}}')" >
                                                    {{$year}}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a class="dropdown-item" wire:click="change('{{ $compareType }}','quarter','{{$year}}')" >
                                                    {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                                </a>
                                            </li>
                                        @endif        
                                    @endforeach
                                @endif    
                                @if($currentStatementName == 'cash-flow')
                                    @foreach($allYearCF as $year)
                                        @if($period=='FY')
                                            <li>
                                                <a class="dropdown-item" wire:click="change('{{ $compareType }}','FY','{{$year}}')" >
                                                    {{$year}}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a class="dropdown-item" wire:click="change('{{ $compareType }}','quarter','{{$year}}')" >
                                                    {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                @endif    
                                @if($currentStatementName == 'income-statement')
                                    @foreach($allYearIS as $year)
                                        @if($period=='FY')
                                            <li>
                                                <a class="dropdown-item" wire:click="change('{{ $compareType }}','FY','{{$year}}')" >
                                                    {{$year}}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a class="dropdown-item" wire:click="change('{{ $compareType }}','quarter','{{$year}}')" >
                                                    {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                @endif    
                            </ul>
                        </div>
                    @endif    
                </div>
            </div>
        </div>
        @if(in_array($currentStatementName ,['balance-sheet','income-statement','cash-flow']))
            <div class="row">
                <div class="col-md-12">
                    @if($currentType == 'vertical' || $currentType=='ver')
                        @if(!is_null($comparableStatementRows))
                            <div class="table-responsive scrollable-card-body mb-3 shadow-sm h-auto">
                                <table class="table table-hover table-sort scroll-table mb-0 p-2 ttm-hold">
                                    <thead>
                                    <tr class="border-0 border-bottom-0 statement-tr">
                                        <th><strong>Breakdown</strong></th>
                                        @foreach($symbols as $symbol=>$cik)
                                            <th wire:key="{{ $loop->index }}">
                                                {{ $symbol }} <br> {{ formatDate(@$this->header[$symbol][0], 'n/d/Y') }}
                                            </th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @include('livewire.financials.partials.comparableCommonRows')
                                </table>
                            </div>
                        @endif
                    @elseif($currentType == 'horizontal' && isset($horizontalStatement['rows']))
                        <div class="table-responsive scrollable-card-body mb-3 shadow-sm" style="height: auto;">
                            <table class="table table-hover table-sort scroll-table mb-0 p-2 ttm-hold">
                                <thead>
                                <tr class="border-0 border-bottom-0 statement-tr">
                                    @foreach($horizontalStatement['headerColumns'] as $headerColumn)
                                        @if($loop->first)
                                            <th>Breakdown</th>
                                        @endif
                                        <th>{{ formatDate($headerColumn, 'n/d/Y') }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @php $cfoa = $cfia = $cffa = 0; @endphp                                    
                                @foreach($horizontalStatement['rows'] as $key => $columns)
                                    <tr>
                                        @foreach($columns as $columnKey => $column)
                                            <td class="financials-td @if($loop->first) tippy @endif" @if($loop->first) id="{{ getAlertId($column['column'],'_horizontal_'.$column['alert_title']) }}" @endif>
                                                @php 
                                                    $revenue_key = 0;
                                                    if (is_array($column) && isset($column['revenue'])) {
                                                        switch ($column['revenue']) {
                                                            case 'self':
                                                                $revenue_key = $key;
                                                                $self_column = true;
                                                                break;
                                                            case 'cfoa':
                                                            case 'cfia':
                                                            case 'cffa':
                                                                $revenue_key = $key;
                                                                break;
                                                            default:
                                                                $revenue_key = 0;
                                                                break;
                                                        }
                                                    }
                                                    $revenue = $horizontalStatement['rows'][$revenue_key][$columnKey];
                                                @endphp
                                                @if($loop->first)
                                                    @php 
                                                        $title = str_repeat('&nbsp; ',$column['indentation']).$column['title'];
                                                        if (isset($column['revenue']) && $column['revenue'] == 'self') {
                                                            $self_column = true;
                                                        } else {
                                                            $self_column = false;
                                                        }
                                                    @endphp
                                                    <span class="tippy {{ $column['indentation'] }}" id="{{ getAlertId($column['column'].'_',$column['alert_title']) }}">{!! $title !!}</span>
                                                    @if( isset($horizontalStatement['alert_content']) && isset($horizontalStatement['alert_content'][$column['title']]) && count($horizontalStatement['alert_content'][$column['title']]) > 1)
                                                        @if($column['title'] != '')
                                                            @if($key >= 10)
                                                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip bottom-table">
                                                            @else
                                                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip ">
                                                            @endif
                                                        @endif
                                                        <div class="tooltip-content">
                                                            <div id="alertCarouselfinancial_{{$column['column']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                                
                                                                <!-- Wrapper for slides -->
                                                                <div class="carousel-inner">

                                                                    @foreach($horizontalStatement['alert_content'][$column['title']] as $item)

                                                                        @if($loop->first)
                                                                            <div class="item active">
                                                                                {!! $item['content'] !!}
                                                                            </div>
                                                                        @else
                                                                            <div class="item">
                                                                                {!! $item['content'] !!}
                                                                            </div>
                                                                        @endif
                                                                        
                                                                    @endforeach
                                                                    
                                                                </div>

                                                                <!-- Left and right controls -->
                                                                <a class="left carousel-control" href="#alertCarouselfinancial_{{$column['column']}}" data-slide="prev">
                                                                <i class="bx bx-skip-previous"></i>
                                                                <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="right carousel-control" href="#alertCarouselfinancial_{{$column['column']}}" data-slide="next">
                                                                <i class="bx bx-skip-next"></i>
                                                                <span class="sr-only">Next</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @else
                                                        @if(isset($horizontalStatement['alert_content'][$column['title']][0]))
                                                            @if($column['title'] != '')
                                                                @if($key >= 10)
                                                                <i class="bx bx-info-circle financialSlider alertSlider has-tooltip bottom-table">
                                                                @else
                                                                <i class="bx bx-info-circle financialSlider alertSlider has-tooltip ">
                                                                @endif
                                                            @endif
                                                            <div class="tooltip-content" style="padding: 15px;">    
                                                            
                                                                {!! $horizontalStatement['alert_content'][$column['title']][0]['content'] !!}                            
                                                            </div>
                                                        @endif
                                                    @endif
                                                @else
                                                    @php 
                                                        $column_percentage = 0;
                                                        if ($currentStatementName == 'cash-flow') {
                                                            $column = str_replace('-','',$column);
                                                        }
                                                        if ($revenue != 0) {
                                                            $column_percentage = percentage(($column/$revenue)*100);
                                                        }
                                                    @endphp
                                                    @switch($currentStatementName)
                                                        @case('income-statement')
                                                            @if( $title != '' )
                                                                
                                                                @if($column_percentage != 0)

                                                                    @if( in_array($title, ['EPS']) )
                                                                        --
                                                                    @else
                                                                        {{ $column_percentage }}                           
                                                                    @endif                     
                                                                @else
                                                                    --
                                                                @endif        
                                                            @endif
                                                            @break
                                                        @case('balance-sheet')
                                                            @if( $title != '' )
                                                                @if($column_percentage != 0)
                                                                    {{ $column_percentage }}                                                
                                                                @else
                                                                    --
                                                                @endif    
                                                            @endif
                                                            @break
                                                        @case('cash-flow')
                                                            @if( $title != '' )
                                                                @if($self_column === true)
                                                                    {{ percentage(100) }}
                                                                @else
                                                                    @if($column_percentage != 0)
                                                                        {{ $column_percentage }}     
                                                                    @else
                                                                        --
                                                                    @endif
                                                                @endif
                                                            @endif
                                                            @break
                                                    @endswitch
                                                    
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                    {{--@if(in_array($columns[0]['column'],KEYS_FOR_NEXT_BLANK_ROW[$currentStatementName]))
                                        <tr>
                                            {!! str_repeat('<td class="financials-td" style="background-color: aliceblue;"><span>&nbsp;</span></td>',count($columns)) !!}
                                        </tr>
                                    @endif--}}
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        {{-- <h3>No data found</h3> --}}
                    @endif
                </div>
            </div>
        @endif
    </div>
</div>
