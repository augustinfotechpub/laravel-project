<div class="row mx-0 mt-2" xmlns:wire="http://www.w3.org/1999/xhtml" xmlns:livewire>
    @switch($compareType)
        @case('common')
        @include('livewire.financials.comparables.common-size')
        @break
        @case('standard')
        @include('livewire.financials.comparables.standard-size')
    @endswitch
</div>
