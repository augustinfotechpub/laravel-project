<div class="row mx-0 mt-2" xmlns:wire="http://www.w3.org/1999/xhtml" xmlns:livewire>
    <div class="row">
        <div class="col-md-6">
            <livewire:financials.search-firm/>
        </div>
        <div class="col-md-6">
            <livewire:financials.search-firm-right/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 text-center" style="margin-top:20px; margin-left:15%;">
            <button class="btn btn-sm btn-primary" type="button" disabled wire:loading="changeCurrentStatementName"
                    wire:target="changeCurrentStatementName">
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Loading...
            </button>

            <div class="btn-group" wire:loading.remove="changeCurrentStatementName">
                <button class="btn btn-sm btn-{{ $currentStatementName == 'balance-sheet'?'primary':'secondary' }}"
                        wire:click="changeCurrentStatementName('balance-sheet','vertical')">
                    Balance Sheet / Statement of Financial Position
                </button>
                <button class="btn btn-sm btn-{{ $currentStatementName == 'income-statement'?'primary':'secondary' }}"
                        wire:click="changeCurrentStatementName('income-statement','vertical')">
                    Income Statement / Statement of Operations
                </button>
                <button class="btn btn-sm btn-{{ $currentStatementName == 'cash-flow'?'primary':'secondary' }}"
                        wire:click="changeCurrentStatementName('cash-flow','vertical')">
                    Cash Flow Statement / Statement of Cash Flows
                </button>
            </div>
        </div>
    </div>
    <div class="col-md-12 min-h-screen">
        <div class="row mx-0" xmlns:wire="http://www.w3.org/1999/xhtml">
            <div class="table__heading">
                <div class="col-md-8">
                    <h4 class="tippy mx-auto px-0" id="{{ getAlertId('standard_comparable_alert') }}">STANDARD COMPARABLES
                        @if( !empty($alertsData['standard_comparable_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsData['standard_comparable_alert']) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach(@$alertsData['standard_comparable_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$alertsData['standard_comparable_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif    
                    </h4>
                    <div class="dropdown dropdownInBlock">
                        <div class="tippy dropdown-toggle item text-uppercase text-sm font-black"  type="button" id="comparablesDropdown" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            {{$year}}
                        </div>
                        <ul class="dropdown-menu scrollable-menu" role="menu" style="height: auto;max-height: 200px; overflow-x: hidden;" aria-labelledby="comparablesDropdown">
                            @if($currentStatementName == 'balance-sheet')
                                @foreach($allYearBS as $year)
                                    @if($period=='FY')
                                        <li>
                                            <a class="dropdown-item" wire:click="change('{{ $compareType }}','FY','{{$year}}')" >
                                                {{$year}}
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a class="dropdown-item" wire:click="change('{{ $compareType }}','quarter','{{$year}}')" >
                                                {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                            </a>
                                        </li>
                                    @endif        
                                @endforeach
                            @endif    
                            @if($currentStatementName == 'cash-flow')
                                @foreach($allYearCF as $year)
                                    @if($period=='FY')
                                        <li>
                                            <a class="dropdown-item" wire:click="change('{{ $compareType }}','FY','{{$year}}')" >
                                                {{$year}}
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a class="dropdown-item" wire:click="change('{{ $compareType }}','quarter','{{$year}}')" >
                                                {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif    
                            @if($currentStatementName == 'income-statement')
                                @foreach($allYearIS as $year)
                                    @if($period=='FY')
                                        <li>
                                            <a class="dropdown-item" wire:click="change('{{ $compareType }}','FY','{{$year}}')" >
                                                {{$year}}
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a class="dropdown-item" wire:click="change('{{ $compareType }}','quarter','{{$year}}')" >
                                                {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif    
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="btn-group float-end" wire:loading="changePeriod" wire:target="changePeriod">
                        <button class="btn btn-sm btn-primary" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </button>
                    </div>
                    <div class="btn-group float-end" wire:loading.remove="changePeriod">
                        <button class="btn btn-{{ $period == 'quarter'?"primary":"secondary"}} btn-sm"
                                wire:click="changePeriod('{{ $compareType }}','quarter')">
                            Quarterly
                        </button>
                        <button class="btn btn-{{ $period == 'FY'?"primary":"secondary"}} btn-sm"
                                wire:click="changePeriod('{{ $compareType }}','FY')">
                            Annual
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(in_array($currentStatementName ,['balance-sheet','income-statement','cash-flow']))
            <div class="row">
                <div class="col-md-12">

                    @if(!is_null($comparableStatementRows))
                        <div class="table-responsive scrollable-card-body mb-3 shadow h-auto">
                            <table class="table table-hover table-sort scroll-table mb-0 p-2 ttm-hold">
                                <thead>
                                <tr class="border-0 border-bottom-0 statement-tr">
                                    <th>Breakdown</th>
                                    @foreach($symbols as $symbol=>$cik)
                                        <th wire:key="{{ $loop->index }}">
                                            {{ $symbol }} <br> {{ formatDate(@$header[$symbol][0], 'n/d/Y') }}
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @include('livewire.financials.partials.comparableCommonRows')
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </div>
</div>
