<div class="row mt-4">
    <div class="" style="height: 600px;">
        <iframe src="{{ route('stock-screener-widget') }}" frameborder="0" class="object-contain iframe-resp"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
</div>
