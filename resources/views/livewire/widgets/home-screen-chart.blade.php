<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div id="tradingview_34970" style="height:400px;"></div>
    <script type="text/javascript" src="//s3.tradingview.com/tv.js"></script>
    <script type="text/javascript">
        new TradingView.widget(
            {
                "autosize": true,
                "symbol": "{{ session('profile')['exchangeShortName'] }}" + ':' + "{{ session('profile')['symbol'] }}",
                "interval": "D",
                "timezone": "Etc/UTC",
                "theme": "light",
                "style": "3",
                "locale": "en",
                "toolbar_bg": "#f1f3f6",
                "enable_publishing": false,
                "withdateranges": true,
                "dateRange": "60M",
                "allow_symbol_change": true,
                "container_id": "tradingview_34970"
            }
        );
    </script>
</div>
<!-- TradingView Widget END -->
