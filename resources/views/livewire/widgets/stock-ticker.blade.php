<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
</div>
<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
    {
        "symbols"
    :
        [
            {
                "description": "Apple Inc. (AAPL)",
                "proName": "NASDAQ:AAPL"
            },
            {
                "description": "Facebook Inc. (FB)",
                "proName": "NASDAQ:FB"
            },
            {
                "description": "Amazon (AMZN)",
                "proName": "NASDAQ:AMZN"
            },
            {
                "description": "Netflix (NFLX)",
                "proName": "NASDAQ:NFLX"
            },
            {
                "description": "Alphabet Inc. (GOOG)",
                "proName": "NASDAQ:GOOG"
            },
            {
                "description": "Tesla Inc (TSLA)",
                "proName": "NASDAQ:TSLA"
            },
            {
                "description": "Microsoft Corp(MSFT)",
                "proName": "NASDAQ:MSFT"
            },
            {
                "description": "JPMORGAN CHASE & CO(JPM)",
                "proName": "JPM"
            },
            {
                "description": "JOHNSON & JOHNSON(JNJ)",
                "proName": "JNJ"
            },
            {
                "description": "VISA INC.(V)",
                "proName": "V"
            },
            {
                "description": "WELLS FARGO & COMPANY/MN(WFC)",
                "proName": "WFC"
            }
        ],
            "showSymbolLogo"
    :
        true,
            "colorTheme"
    :
        "light",
            "isTransparent"
    :
        true,
            "largeChartUrl"
    :
        "",
            "displayMode"
    :
        "adaptive",
            "locale"
    :
        "en"
    }
</script>
</div>
<!-- TradingView Widget END -->
<script>
    {{--document.querySelector('.tv-header__link').remove();--}}
</script>
