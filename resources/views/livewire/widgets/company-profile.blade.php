<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-symbol-profile.js" async>
        {
            "symbol": "{{ getProfileSymbol() }}",
            "width": "100%",
            "height": "100%",
            "colorTheme": "light",
            "isTransparent": false,
            "locale": "en",
            "largeChartUrl": "{{ route('widget.company-profile') }}"
        }
    </script>
</div>
<!-- TradingView  Widget END -->
