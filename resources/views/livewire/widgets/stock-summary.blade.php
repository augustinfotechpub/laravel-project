{{--
<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="//s3.tradingview.com/external-embedding/embed-widget-financials.js" async>
        {
            "symbol": "{{ getProfileSymbol() }}",
            "colorTheme": "light",
            "isTransparent": false,
            "largeChartUrl": "{{ route('widget.stock-summary') }}",
            "displayMode": "regular",
            "width": "100%",
            "height": "100%",
            "locale": "en"
        }
    </script>
</div>
<!-- TradingView Widget END -->
--}}

<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container shadow" style="margin-top: 7px;">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-financials.js" async>
        {
            "symbol": "{{ getProfileSymbol() }}",
            "colorTheme": "light",
            "isTransparent": false,
            "largeChartUrl": "{{ route('widget.stock-summary') }}",
            "displayMode": "regular",
            "width": "100%",
            "height": "100%",
            "locale": "en"
        }
    </script>
</div>
<!-- TradingView Widget END -->
