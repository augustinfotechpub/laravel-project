<div>
    {{-- Brief intro part --}}
    <div class="row">
        <div class="col-md-12 border py-3">
            <div class="options">
                <h5>Show:</h5>
                <h5 class="item {{ $currentModel == 'dcf'?'active':null }}"
                    wire:click="changeModel('dcf')">DCF Model</h5>
                <h5 class="item {{ $currentModel == 'dcf-model-levered'?'active':null }}"
                    wire:click="changeModel('dcf-model-levered')">DCF Model Levered</h5>
                {{--<h5 class="item {{ $currentModel == 'free-cash-flow-build-up'?'active':null }}"
                    wire:click="changeModel('free-cash-flow-build-up')">Free Cash Flow Build up</h5>
                <h5 class="item {{ $currentModel == 'terminal-value'?'active':null }}"
                    wire:click="changeModel('terminal-value')">Terminal Value</h5>
                <h5 class="item {{ $currentModel == 'intrinsic-value'?'active':null }}"
                    wire:click="changeModel('intrinsic-value')">Intrinsic Value </h5>
                <h5 class="item {{ $currentModel == 'weight-average-coast-capital'?'active':null }}"
                    wire:click="changeModel('weight-average-coast-capital')">Weighted Average Cost of Capital</h5>--}}
            </div>
        </div>
        <div class="col-md-12">
            @switch($currentModel)
                @case('dcf')
                <div class="row" wire:init="initDCF">
                    @if($initDCF)
                        <div class="col-md-12">
                            <div class="table__heading p-3 text-white rounded"
                                 style="background-color: #283a5a!important;">
                                <h5>Discounted Cash Flow (DCF) Analysis Un Levered</h5>
                                <p class="mt-3">{{ $table['sub_title']??null }}</p>
                            </div>
                        </div>
                        @foreach($models['dcf'] as $model)
                            <div class="col-md-12">
                                <div class="table__heading">
                                    <h4>{{ $model['title'] }}</h4>
                                    <span>{{ $model['sub_title']??null }}</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive scrollable-card-body mb-3 mh-400 shadow pb-3">
                                    <table class="table table-hover table-sort mb-0 p-2 scroll-table">
                                        <thead>
                                        <tr class="border-0 border-bottom-0">
                                            @foreach($model['table']['thead'] as $column)
                                                <th>{{ $column }}</th>
                                            @endforeach
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($model['table']['tbody_rows'] as $row)
                                            <tr class="{{ $row['classes']??null }}" style="{{ $row['styles']??null }}">
                                                @foreach($row['columns'] as $column)
                                                    @if($loop->first)
                                                        <td><strong>{{ $column }}</strong></td>
                                                    @else
                                                        <td>{!! $column !!}</td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
                @break
                @case('dcf-model-levered')
                <div class="row">
                    <div class="col-md-12">
                        <div class="table__heading">
                            <h3>Discounted Cash Flow (DCF) Analysis Levered</h3>
                            {{--<p class="mt-3">{{ $table['sub_title']??null }}</p>--}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive scrollable-card-body mb-3 mh-400 shadow pb-3">
                            <table class="table table-hover table-sort mb-0 p-2 scroll-table">
                                <thead>
                                <tr class="border-0 border-bottom-0">
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ 'name' }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @break
                @case('free-cash-flow-build-up')
                <div class="row">
                    <div class="col-md-12">
                        <div class="table__heading">
                            <h3>Free Cash Flow Build Up</h3>
                            {{--<p class="mt-3">{{ $table['sub_title']??null }}</p>--}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive scrollable-card-body mb-3 mh-400 shadow pb-3">
                            <table class="table table-hover table-sort mb-0 p-2 scroll-table">
                                <thead>
                                <tr class="border-0 border-bottom-0">
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ 'name' }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @break
                @case('terminal-value')
                <div class="row">
                    <div class="col-md-12">
                        <div class="table__heading">
                            <h3>Terminal Value</h3>
                            {{--<p class="mt-3">{{ $table['sub_title']??null }}</p>--}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive scrollable-card-body mb-3 mh-400 shadow pb-3">
                            <table class="table table-hover table-sort mb-0 p-2 scroll-table">
                                <thead>
                                <tr class="border-0 border-bottom-0">
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ 'name' }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @break
                @case('intrinsic-value')
                <div class="row">
                    <div class="col-md-12">
                        <div class="table__heading">
                            <h3>Intrinsic Value</h3>
                            {{--<p class="mt-3">{{ $table['sub_title']??null }}</p>--}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive scrollable-card-body mb-3 mh-400 shadow pb-3">
                            <table class="table table-hover table-sort mb-0 p-2 scroll-table">
                                <thead>
                                <tr class="border-0 border-bottom-0">
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ 'name' }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @break
                @case('weight-average-coast-capital')
                <div class="row">
                    <div class="col-md-12">
                        <div class="table__heading">
                            <h3>Weighted Average Cost Of Capital</h3>
                            {{--<p class="mt-3">{{ $table['sub_title']??null }}</p>--}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-sort mb-0 p-2">
                                <thead>
                                <tr class="border-0 border-bottom-0">
                                    <th>Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ 'name' }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endswitch
        </div>
    </div>
</div>
