<div class="col-md-4 col-sm-12 stock-media-col border-start border-end px-0" wire:init="initStockSummary"
     {{--wire:poll.15000ms--}}>
    @if($initStockSummary)
        <div class="media stock-summary-media pt-2">
            <img src="{{ $profile['image'] }}" width="50" alt="{{ session('profile')['companyName'] }}"
                 class="img-circle img-rounded">
            <div class="media-body" style="flex:initial;">
                <h3>{{ $profile['companyName'] }}</h3>
                <small>{{ $profile['symbol']."   -  ".$profile['exchangeShortName'] }}</small>
            </div>
        </div>
        <div class="price-view">
            <h4 class="mb-0 mt-2">
                ${{ number_format(((float)($profile['price'])),2,'.',',') }}
                <sup class="text-warning has-tooltip cursor-pointer"
                ><span class="tooltip-content w-36">Quotes are delayed by 15 min</span> D</sup>
                <small class=" {{ ($profile['changes']<0)?'text-danger':'text-success' }}">
                    {{ number_format(((float)($profile['changes'])),2,'.',',') }}
                    ({{ number_format(((float)($profile['changes']/$profile['price'])*100),2,'.',',') }}
                    %)
                </small>
            </h4>
        </div>
        
        <div class="stock-summary-status py-2">
            <h6 class="mb-0">
                {{ currencyFormat($profile['eps']) }}
                <small class="fw-normal">EPS</small>
            </h6>
            <h6 class="mb-0">
                {{ currencyInMillion($profile['marketCap']) }}
                <small class="fw-normal">MARKET CAP (in millions)</small>
            </h6>
            <h6 class="mb-0">
                {{ number_format(((float)($profile['pe'])),2,'.',',') }}
                <small class="fw-normal">P/E RATIO</small>
            </h6>
        </div>
    @else
        @include('front.layouts.partials.loader_b')
    @endif
</div>
