<div class="col-md-12 border-top mt-2 bg-gray-100">
    <div class="row">
        <div class="col-lg-6">
            <div class="row row-sm mg-b-20">
                <div class="col-md-12 mt-2">
                    <label for="type" class="form-label">Type</label>
                    <select name="type[]" class="form-control select2">
                        @foreach($types as $key=>$type)
                            <option value="{{ $key }}"
                                    @if (isset($definition) && $definition->type == $key) selected @endif>{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="tags" class="form-label">Tags</label>
                    @foreach($inputs['tag'] as $index=> $input)
                        <div class="input-group my-1">
                            <input type="text" class="form-control" name="tags[{{$index}}][]">
                            <div class="input-group-btn">
                                @if($loop->first)
                                    <a class="btn btn-success text-white" type="button"
                                       wire:click="addInput('tag',{{ $tagI }})">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @else
                                    <a class="btn btn-danger text-white" type="button"
                                       wire:click="removeInput('tag',{{ $index }})">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-12 mt-2">
                    <label for="sics">SIC's</label>
                    @foreach($inputs['sic'] as $index=> $input)
                        <div class="input-group my-1">
                            <input type="text" class="form-control" name="sic[{{$index}}][]">
                            <div class="input-group-btn">
                                @if($loop->first)
                                    <a class="btn btn-success text-white" type="button"
                                       wire:click="addInput('sic',{{ $sicI }})">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @else
                                    <a class="btn btn-danger text-white" type="button"
                                       wire:click="removeInput('sic',{{ $index }})">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-12 mt-2">
                    <label for="symbols">Symbols</label>
                    @foreach($inputs['symbol'] as $index=> $input)
                        <div class="input-group my-1">
                            <input type="text" class="form-control" name="symbol[{{ $index }}][]">
                            <div class="input-group-btn">
                                @if($loop->first)
                                    <a class="btn btn-success text-white" type="button"
                                       wire:click="addInput('symbol',{{ $symbolI }})">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @else
                                    <a class="btn btn-danger text-white" type="button"
                                       wire:click="removeInput('symbol',{{ $index }})">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="col-lg-12 mt-2">
                <a class="btn btn-danger float-right text-white mb-1" wire:click="remove" type="button">
                    <i class="fa fa-trash"></i> Remove Item
                </a>
            </div>
            <div class="col-lg-12 mt-2">
                <label class="control-label" for="definition">Definition <small>*</small></label>
                <textarea class="form-control wysiwyg_editor" rows="10"
                          name="content">{!! isset($definition) ? $definition->definition : '' !!}</textarea>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('.wysiwyg_editor').summernote({
                    height: 150
                });
            });
            window.addEventListener('text-editor', event => {
                $('.wysiwyg_editor').summernote({
                    height: 150
                });
            })
        </script>
    @endpush
</div>
