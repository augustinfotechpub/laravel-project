<div class="row" wire:init="initComponent">
    <div class="col-md-12">
        <div class="table__heading">
            <h4>Related Industries S-Curve Chart</h4>
        </div>
    </div>
    @if(!$isLoading)
        <div class="col-md-4">
            <div class="table-responsive scrollable-card-body mb-3 shadow pb-3"
                 style="height:auto;max-height:400px;">
                <table class="table table-hover table-sort scroll-table mb-0 p-2">
                    <thead>
                    <tr class="border-0 border-bottom-0">
                        <th>Company Name</th>
                        <th class="text-end">Chart</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($relatedIndustries as $industry)
                        <tr>
                            <td>
                                {{ $industry['companyName'] }}
                            </td>
                            <td class="text-end">
                                <button
                                    class="btn btn-sm px-1 py-0 {{ $activeRelatedIndustry == $industry['symbol']?'text-white btn-info':'text-info btn-link' }}"
                                    wire:click="getChart('{{ $industry['symbol'] }}')">
                                    <i class="fa fa-chart-line"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-8">
            <div id="activeIndustryChartDiv" class="row shadow" wire:loading.remove="getChart" style="height: 400px;">
                <canvas id="activeIndustryChart" style="height: 400px;"></canvas>
            </div>
            <div class="row" wire:loading wire:target="getChart">
                @include('front.layouts.partials.loader_b')
            </div>
        </div>
    @endif
</div>
