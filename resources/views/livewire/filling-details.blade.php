<div class="mx-2 my-5">
    @include('livewire.partials.filling-details')
    <div wire:ignore.self class="modal fade" id="fillingDetailModal" tabindex="-1"
         aria-labelledby="fillingDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="fillingDetailModalLabel">Form 4 Details</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div wire:loading.remove="getForm" id="formHtmlContent">
                        {!! $form !!}
                    </div>
                    <div wire:loading wire:target="getForm">
                        @include('front.layouts.partials.loader_b')
                    </div>
                </div>
                <div class="modal-footer pe-5 me-5">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--alert when scrolling over AR when AR financing disclosed 'Under ASU 2016-15, the total amount of recognized cash
    flow does not change, but the classification of those cash flows may change significantly - this might lead to
    undesirable effects on commonly used valuation metrics, such as free cash flow - the distinction generally lies with
    recourse or non-recourse financing (legal rights to demand or obtain payment)'--}}
</div>

