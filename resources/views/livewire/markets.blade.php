<div>
    <div class="row mt-2">
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('indices_alert') }}">Indices</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.indices') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('commodities_alert') }}">Commodities</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.commodities') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('forex_alert') }}">Forex</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.forex') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('bonds_alert') }}">Bonds</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.bonds') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('futures_alert') }}">Futures</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.futures') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('currencies_alert') }}">Currencies</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.currencies') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('stocks_alert') }}">Stocks</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.stocks') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>

        <div class="col-md-4">
            <h3 class="tippy font-semibold text-3xl text-center" id="{{ getAlertId('cryptocurrencies_alert') }}">Cryptocurrencies</h3>
            <div class="" style="height: 700px;">
                <iframe src="{{ route('markets.cryptocurrencies') }}" frameborder="0" class="object-contain iframe-resp"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
