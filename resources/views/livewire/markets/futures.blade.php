<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
        {
            "colorTheme": "light",
            "dateRange": "12M",
            "showChart": true,
            "locale": "en",
            "largeChartUrl": "{{ route('markets.futures') }}",
            "isTransparent": false,
            "showSymbolLogo": true,
            "width": "100%",
            "height": "660",
            "plotLineColorGrowing": "rgba(33, 150, 243, 1)",
            "plotLineColorFalling": "rgba(33, 150, 243, 1)",
            "gridLineColor": "rgba(240, 243, 250, 1)",
            "scaleFontColor": "rgba(120, 123, 134, 1)",
            "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
            "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
            "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
            "tabs": [
            {
                "title": "Energy",
                "symbols": [
                    {
                        "s": "NYMEX:CL1!",
                        "d": "Crude Oil"
                    },
                    {
                        "s": "NYSE:NGS",
                        "d": "Natural Gas"
                    },
                    {
                        "s": "NYMEX:BB1!",
                        "d": "Brent Oil"
                    },
                    {
                        "s": "NYMEX:RB1!",
                        "d": "Gasoline "
                    },
                    {
                        "s": "NYMEX:HO1!",
                        "d": "Heating Oil"
                    },
                    {
                        "s": "CBOT:EH1!",
                        "d": "Ethanol"
                    }
                ]
            },
            {
                "title": "Metals",
                "symbols": []
            },
            {
                "title": "Agricultural",
                "symbols": []
            },
            {
                "title": "Currencies",
                "symbols": []
            },
            {
                "title": "Indices",
                "symbols": [],
                "originalTitle": "Indices"
            },
            {
                "title": "Interest Rates",
                "symbols": []
            }
        ]
        }
    </script>
</div>
<!-- TradingView Widget END -->
