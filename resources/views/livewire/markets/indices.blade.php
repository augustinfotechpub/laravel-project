<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
        {
            "colorTheme": "light",
            "dateRange": "12M",
            "showChart": true,
            "locale": "en",
            "largeChartUrl": "{{ route('markets.indices') }}",
            "isTransparent": false,
            "showSymbolLogo": true,
            "width": "100%",
            "height": "660",
            "plotLineColorGrowing": "rgba(33, 150, 243, 1)",
            "plotLineColorFalling": "rgba(33, 150, 243, 1)",
            "gridLineColor": "rgba(240, 243, 250, 1)",
            "scaleFontColor": "rgba(120, 123, 134, 1)",
            "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
            "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
            "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
            "tabs": [
            {
                "title": "Major",
                "symbols": [
                    {
                        "s": "XETR:DAX",
                        "d": "DAX INDEX"
                    },
                    {
                        "s": "TSX:TSX",
                        "d": "S&P / TSX COMPOSITE INDEX"
                    },
                    {
                        "s": "ASX:XJO",
                        "d": "S&P / ASX 200 INDEX"
                    },
                    {
                        "s": "TVC:SPX",
                        "d": "S&P 500 INDEX"
                    },
                    {
                        "s": "NASDAQ:IXIC",
                        "d": "NASDAQ COMPOSITE INDEX"
                    },
                    {
                        "s": "FOREXCOM:DJI",
                        "d": "DOW JONES INDUSTRIAL AVERAGE INDEX"
                    },
                    {
                        "s": "TVC:VIX",
                        "d": "VOLATILITY S&P 500 INDEX"
                    },
                    {
                        "s": "TSX:TSX",
                        "d": "S&P/TSX COMPOSITE INDEX"
                    },
                    {
                        "s": "KRX:KOSPI",
                        "d": "KOREA COMPOSITE STOCK PRICE INDEX"
                    }
                ]
            },
            {
                "title": "US",
                "symbols": [
                    {
                        "s": "TVC:SPX",
                        "d": "S&P 500 INDEX"
                    },
                    {
                        "s": "SPCFD:SVX",
                        "d": "SVX 500 VALUE INDEX"
                    },
                    {
                        "s": "SPCFD:MID",
                        "d": "S&P 400 INDEX"
                    },
                    {
                        "s": "CBOE:OEX",
                        "d": "S&P 100 INDEX"
                    },
                    {
                        "s": "SPCFD:SPGSCI",
                        "d": "S&P GOLDMAN SACHS COMMODITY INDEX"
                    },
                    {
                        "s": "DJCFD:DJI",
                        "d": "DOE JONES INDUSTRIAL AVERAGE INDEX"
                    },
                    {
                        "s": "NASDAQ:IXIC",
                        "d": "NASDAQ COMPOSIT INDEX"
                    },
                    {
                        "s": "NASDAQ:NDX",
                        "d": "NASDAQ 100 INDEX"
                    },
                    {
                        "s": "RUSSELL:RUA",
                        "d": "RUSSELL 3000 INDEX"
                    },
                    {
                        "s": "RUSSELL:RUT",
                        "d": "RUSSEL 2000 INDEX"
                    },
                    {
                        "s": "RUSSELL:RUI",
                        "d": "RUSSELL 1000 INDEX"
                    },
                    {
                        "s": "NYSE:NYA",
                        "d": "NYSE COMPOSITE INDEX"
                    },
                    {
                        "s": "NYSE:XMI",
                        "d": "NYSE ARCA MAJOR MARKET INDEX"
                    },
                    {
                        "s": "NYSE:XAX",
                        "d": "AMEX COMPOSITE INDEX"
                    },
                    {
                        "s": "NASDAQ:OSX",
                        "d": "PHLX OIL SERVICE SECTOR INDEX"
                    },
                    {
                        "s": "DJCFD:DJCIKC",
                        "d": "DOW JONES COMMODITY INDEX COFFEE"
                    },
                    {
                        "s": "DJCFD:DJA",
                        "d": "DOW JONES COMPOSITE AVERAGE INDEX"
                    }
                ]
            },
            {
                "title": "Currency",
                "symbols": [
                    {
                        "s": "INDEX:DXY",
                        "d": "U.S. DOLLAR CURRENCY INDEX"
                    },
                    {
                        "s": "TVC:EXY",
                        "d": "EURO CURRENCY INDEX"
                    },
                    {
                        "s": "TVC:BXY",
                        "d": "BRITISH PUUND CURRENCY INDEX"
                    },
                    {
                        "s": "TVC:SXY",
                        "d": "SWISS FRANC CURRENCY INDEX"
                    },
                    {
                        "s": "FOREXCOM:EURJPY",
                        "d": "JAPANESE YEN CURRENCY INDEX"
                    },
                    {
                        "s": "TVC:CXY",
                        "d": "CANADIAN DOLLAR CURRENCY INDEX"
                    },
                    {
                        "s": "TVC:AXY",
                        "d": "AUSTRALIAN DOLLAR CURRENCY INDEX"
                    },
                    {
                        "s": "TVC:ZXY",
                        "d": "NEW ZEALAND DOLLAR CURRENCY INDEX"
                    }
                ]
            }
        ]
        }
    </script>
</div>
<!-- TradingView Widget END -->
