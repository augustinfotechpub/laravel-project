<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
        {
            "colorTheme": "light",
            "dateRange": "12M",
            "showChart": true,
            "locale": "en",
            "largeChartUrl": "{{ route('markets.currencies') }}",
            "isTransparent": false,
            "showSymbolLogo": true,
            "width": "100%",
            "height": "660",
            "plotLineColorGrowing": "rgba(33, 150, 243, 1)",
            "plotLineColorFalling": "rgba(33, 150, 243, 1)",
            "gridLineColor": "rgba(240, 243, 250, 1)",
            "scaleFontColor": "rgba(120, 123, 134, 1)",
            "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
            "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
            "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
            "tabs": [
            {
                "title": "Major",
                "symbols": [
                    {
                        "s": "FX:USDJPY",
                        "d": "U.S. DOLLAR / JAPANESE YEN"
                    },
                    {
                        "s": "FX:GBPUSD",
                        "d": "BRITISH POUND / U.S. DOLLAR"
                    },
                    {
                        "s": "FX:AUDUSD",
                        "d": "AUSTRALIAN DOLLAR / U.S. DOLLAR"
                    },
                    {
                        "s": "FX:USDCAD",
                        "d": "U.S. DOLLAR / CANADIAN DOLLAR"
                    },
                    {
                        "s": "FX:USDCHF",
                        "d": "U.S. DOLLAR / SWISS FRANC"
                    },
                    {
                        "s": "FX:NZDUSD",
                        "d": "NEW ZEALAND DOLLAR / U.S. DOLLAR"
                    },
                    {
                        "s": "FX:EURUSD",
                        "d": "EURO / U.S. DOLLAR"
                    }
                ],
                "originalTitle": "Indices"
            },
            {
                "title": "Minor",
                "symbols": [
                    {
                        "s": "FX:EURGBP",
                        "d": "EURO / BRITISH POUND"
                    },
                    {
                        "s": "FX:EURJPY",
                        "d": "EURO / JAPANESE YEN"
                    },
                    {
                        "s": "FX:GBPJPY",
                        "d": "BRITISH POUND / JAPANESE YEN"
                    },
                    {
                        "s": "FX:CADJPY",
                        "d": "CANADIAN DOLLAR / JAPANESE YEN"
                    },
                    {
                        "s": "FX:GBPCAD",
                        "d": "BRITISH POUND / CANADIAN DOLLAR"
                    },
                    {
                        "s": "FX:EURCAD",
                        "d": "EURO / CANADIAN DOLLAR"
                    },
                    {
                        "s": "FX:EURAUD",
                        "d": "EURO / AUSTRALIAN DOLLAR"
                    },
                    {
                        "s": "FX:EURCHF",
                        "d": "EURO / SWISS FRANC"
                    },
                    {
                        "s": "FX:EURNZD",
                        "d": "EURO / NEW ZEALAND DOLLAR"
                    },
                    {
                        "s": "FX_IDC:GBPEUR",
                        "d": "BRITISH POUND / EURO"
                    }
                ],
                "originalTitle": "Commodities"
            },
            {
                "title": "Exotic",
                "symbols": [
                    {
                        "s": "FX_IDC:USDBRL",
                        "d": "U.S. DOLLAR / BRAZILIAN REAL"
                    },
                    {
                        "s": "FX:USDCNH",
                        "d": "U.S. DOLLAR / OFFSHORE CHINESE YUAN"
                    },
                    {
                        "s": "FX_IDC:USDCZK",
                        "d": "U.S. DOLLAR / CZECH KORUNA"
                    },
                    {
                        "s": "FX_IDC:USDDKK",
                        "d": "U.S. DOLLAR / DANISH KRONE"
                    },
                    {
                        "s": "FX_IDC:USDHKD",
                        "d": "U.S. DOLLAR / HONG KONG DOLLAR"
                    },
                    {
                        "s": "FX_IDC:USDHUF",
                        "d": "U.S. DOLLAR / HUNGARIAN FORINT"
                    },
                    {
                        "s": "FX_IDC:USDINR",
                        "d": "U.S. DOLLAR / INDIAN RUPEE"
                    },
                    {
                        "s": "FX:USDMXN",
                        "d": "U.S. DOLLAR / MEXICAN PESO"
                    },
                    {
                        "s": "FX:USDNOK",
                        "d": "U.S. DOLLAR / NORWEGIAN KRONE"
                    },
                    {
                        "s": "FX_IDC:USDPLN",
                        "d": "U.S. DOLLAR / POLISH ZLOTY"
                    },
                    {
                        "s": "FX_IDC:USDRON",
                        "d": "U.S. DOLLAR / ROMANIAN LEU"
                    },
                    {
                        "s": "FOREXCOM:USDRUB",
                        "d": "U.S. DOLLAR / RUSSIAN RUBLE"
                    },
                    {
                        "s": "FX_IDC:USDSAR",
                        "d": "U.S. DOLLAR / SAUDI ARABIAN RIYAL"
                    },
                    {
                        "s": "FX:USDSEK",
                        "d": "U.S. DOLLAR / SWEDISH KRONA"
                    },
                    {
                        "s": "OANDA:USDSGD",
                        "d": "U.S. DOLLAR / SINGAPORE DOLLAR"
                    }
                ],
                "originalTitle": "Bonds"
            }
        ]
        }
    </script>
</div>
<!-- TradingView Widget END -->
