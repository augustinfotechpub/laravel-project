<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
        {
            "colorTheme": "light",
            "dateRange": "12M",
            "showChart": true,
            "locale": "en",
            "largeChartUrl": "{{ route('markets.bonds') }}",
            "isTransparent": false,
            "showSymbolLogo": true,
            "width": "100%",
            "height": "660",
            "plotLineColorGrowing": "rgba(33, 150, 243, 1)",
            "plotLineColorFalling": "rgba(33, 150, 243, 1)",
            "gridLineColor": "rgba(240, 243, 250, 1)",
            "scaleFontColor": "rgba(120, 123, 134, 1)",
            "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
            "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
            "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
            "tabs": [
            {
                "title": "Bonds",
                "symbols": [
                    {
                        "s": "CME:GE1!",
                        "d": "Eurodollar"
                    },
                    {
                        "s": "CBOT:ZB1!",
                        "d": "T-Bond"
                    },
                    {
                        "s": "CBOT:UB1!",
                        "d": "Ultra T-Bond"
                    },
                    {
                        "s": "EUREX:FGBL1!",
                        "d": "Euro Bund"
                    },
                    {
                        "s": "EUREX:FBTP1!",
                        "d": "Euro BTP"
                    },
                    {
                        "s": "EUREX:FGBM1!",
                        "d": "Euro BOBL"
                    }
                ],
                "originalTitle": "Bonds"
            }
        ]
        }
    </script>
</div>
<!-- TradingView Widget END -->
