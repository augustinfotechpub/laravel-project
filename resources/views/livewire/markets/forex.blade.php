<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
        {
            "colorTheme": "light",
            "dateRange": "12M",
            "showChart": true,
            "locale": "en",
            "largeChartUrl": "{{ route('markets.forex') }}",
            "isTransparent": false,
            "showSymbolLogo": true,
            "width": "100%",
            "height": "660",
            "plotLineColorGrowing": "rgba(33, 150, 243, 1)",
            "plotLineColorFalling": "rgba(33, 150, 243, 1)",
            "gridLineColor": "rgba(240, 243, 250, 1)",
            "scaleFontColor": "rgba(120, 123, 134, 1)",
            "belowLineFillColorGrowing": "rgba(33, 150, 243, 0.12)",
            "belowLineFillColorFalling": "rgba(33, 150, 243, 0.12)",
            "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
            "tabs": [
            {
                "title": "Forex",
                "symbols": [
                    {
                        "s": "FX:EURUSD"
                    },
                    {
                        "s": "FX:GBPUSD"
                    },
                    {
                        "s": "FX:USDJPY"
                    },
                    {
                        "s": "FX:USDCHF"
                    },
                    {
                        "s": "FX:AUDUSD"
                    },
                    {
                        "s": "FX:USDCAD"
                    }
                ],
                "originalTitle": "Forex"
            }
        ]
        }
    </script>
</div>
<!-- TradingView Widget END -->
