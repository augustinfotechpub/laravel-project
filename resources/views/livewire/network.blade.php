<div class="container-fluid">
    <h3 class="text-center m-3">Network Messages</h3>
    @auth
        <iframe src="{{ route('conversations') }}" style="width: 100%; height: 500px"></iframe>
     @else
        <div class="network-container">
            <h3>Please logIn or Sign Up to chat with other users.</h3>
        </div>
    @endauth    
</div>
