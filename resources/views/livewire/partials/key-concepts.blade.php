<?php 
if(isset($keyConcepts) && is_array($keyConcepts) && !empty($keyConcepts))
{
    $arraycolumn = array_column($keyConcepts,'title');
    array_multisort($arraycolumn, SORT_ASC, $keyConcepts);
}
?>
@if(!empty($keyConcepts))
    @foreach($keyConcepts as $keyConcept)
        <span class='has-tooltip'>
            <div class='tooltip-content'>
                @if(count($keyConcept['sub_title']) >1)
                    <ul class="nav nav-tabs myTab" role="tablist">
                        @foreach($keyConcept['sub_title'] as $item)
                            <li class="nav-item" role="presentation">
                                <button class="nav-link @if($loop->first) active @endif" id="home{{ $item['item_id'] }}"
                                        data-bs-toggle="tab" data-bs-target="#tab{{$item['item_id']}}" type="button" role="tab"
                                        aria-controls="home" aria-selected="true">
                                    {{ ucfirst($item['sub_title']) }}
                                </button>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content myTabContent">
                        @foreach($keyConcept['sub_title'] as $item)
                            <div class="tab-pane mt-1 fade show @if($loop->first) active @endif" id="tab{{ $item['item_id'] }}"
                                 role="tabpanel" aria-labelledby="home{{ $item['item_id'] }}">
                                    {!!  $item['content']  !!}
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="tab-content myTabContent">
                        @foreach($keyConcept['sub_title'] as $item)
                            <div class="tab-pane mt-1 fade show @if($loop->first) active @endif" id="tab{{ $item['item_id'] }}"
                                 role="tabpanel" aria-labelledby="home{{ $item['item_id'] }}">
                                    {!!  $item['content']  !!}
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
          <span class="badge bg-danger p-1">{{ $keyConcept['title'] }}</span>
        </span>
    @endforeach
@endif
