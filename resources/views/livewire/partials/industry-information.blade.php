<?php 
if(isset($industryInformations) && is_array($industryInformations) && !empty($industryInformations))
{
    $arraycolumn = array_column($industryInformations,'title');
    array_multisort($arraycolumn, SORT_ASC, $industryInformations);
}
?>
@if(!empty($industryInformations))
    @foreach($industryInformations as $industryInfo)
        <span class='has-tooltip'>
            <div class='tooltip-content'>
                @if(count($industryInfo['sub_title']) >1)
                    <ul class="nav nav-tabs myTab" role="tablist">
                        @php $myTabcontent = ''; @endphp
                        @foreach($industryInfo['sub_title'] as $item)
                            <li class="nav-item" role="presentation">
                                <button class="nav-link @if($loop->first) active @endif" id="home{{ $item['item_id'] }}"
                                        data-bs-toggle="tab" data-bs-target="#tab{{$item['item_id']}}" type="button" role="tab"
                                        aria-controls="home" aria-selected="true">
                                    {{ ucfirst($item['sub_title']) }}
                                </button>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content myTabContent">
                        @foreach($industryInfo['sub_title'] as $item)
                            <div class="tab-pane mt-1 fade show @if($loop->first) active @endif" id="tab{{ $item['item_id'] }}"
                                 role="tabpanel" aria-labelledby="home{{ $item['item_id'] }}">
                                    {!!  $item['content']  !!}
                            </div>
                        @endforeach
                    </div>
                @else
                    @foreach($industryInfo['sub_title'] as $item)
                        <div class="tab-pane mt-1 fade show @if($loop->first) active @endif" id="tab{{ $item['item_id'] }}"
                             role="tabpanel" aria-labelledby="home{{ $item['item_id'] }}">
                                {!!  $item['content']  !!}
                        </div>
                    @endforeach
                @endif
            </div>
          <span class="badge bg-primary p-1">{{ $industryInfo['title'] }}</span>
        </span>
    @endforeach
@endif
