<nav class="navbar navbar-expand-xl navbar-light alert-primary innerpage_menu fixed-top">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="innerpageMenu">
            <ul class="navbar-nav mx-auto text-truncate">
                <li class="nav-item">
                    <a href="javascript:void(0);" wire:click="changeContentComponent('markets')"
                       class="nav-link px-2 my-1 py-0 {{ $content_component == 'markets'?'active':'inactive' }}">
                        Markets
                    </a>

                    @if( !empty($alertsDataNav['market_alert']) )
                        <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsDataNav['market_alert']) > 1 )
                                <div class="tooltip-content">
                                
                                    <div id="alertCarouselmarket" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                        
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">

                                            @foreach($alertsDataNav['market_alert'] as $item)
                                                  
                                                @if($loop->first)
                                                    <div class="item active">
                                                        {!! $item !!}
                                                    </div>
                                                @else
                                                    <div class="item">
                                                        {!! $item !!}
                                                    </div>
                                                @endif
                                                
                                            @endforeach
                                            
                                        </div>

                                        <!-- Left and right controls -->
                                        <a class="left carousel-control" href="#alertCarouselmarket" data-slide="prev">
                                          <i class="bx bx-skip-previous"></i>
                                          <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#alertCarouselmarket" data-slide="next">
                                          <i class="bx bx-skip-next"></i>
                                          <span class="sr-only">Next</span>
                                        </a>
                                    </div>

                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['market_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                </li>
                <li class="nav-item">
                    <a href="{{ route('app_main') }}"
                       class="tippy nav-link px-2 my-1 py-0 {{ $content_component == 'app-main'?'active':'inactive' }}" id="{{ getAlertId('home_alert') }}">
                        Home
                    </a>
                    @if( !empty($alertsDataNav['home_alert']) )
                        <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                            @if( count(@$alertsDataNav['home_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselhome" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false" data-interval="1000">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['home_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselhome" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselhome" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['home_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0);" wire:click="changeContentComponent('key-parties')"
                       class="tippy nav-link px-2 my-1 py-0 {{ $content_component == 'key-parties'?'active':'inactive' }}" id="{{ getAlertId('key_parties_alert') }}">
                        Key Parties
                    </a>
                    @if( !empty($alertsDataNav['key_parties_alert']) )
                        <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                            @if( count(@$alertsDataNav['key_parties_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselparties" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['key_parties_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselparties" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselparties" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['key_parties_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif

                </li>
                <li class="nav-item">
                    <a href="javascript:void(0);" wire:click="changeContentComponent('financials')"
                       class="tippy nav-link px-2 my-1 py-0 {{ $content_component == 'financials'?'active':'inactive' }}" id="{{ getAlertId('financials_alert') }}">
                        Financials
                    </a>
                    @if( !empty($alertsDataNav['financials_alert']) )
                        <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                            @if( count(@$alertsDataNav['financials_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselfinancial" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['financials_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselfinancial" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselfinancial" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['financials_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0);" wire:click="changeContentComponent('ratios')"
                       class="nav-link px-2 my-1 py-0 {{ $content_component == 'ratios'?'active':'inactive' }}">
                        Ratios
                    </a>
                                @if( !empty($alertsDataNav['ratios_alert']) )
                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                            @if( count(@$alertsDataNav['ratios_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselratio" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['ratios_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselratio" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselratio" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['ratios_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                            @endif
                </li>
                <li class="nav-item">
                    <a href="{{ route('full_chart') }}" target="_blank"
                       class="tippy nav-link px-2 my-1 py-0 charting_tech_anal {{ $content_component == 'charting'?'active':'inactive' }}" id="{{ getAlertId('charting_technical_analysis_alert') }}">
                        Charting / Technical Analysis
                    </a>
                    @if( !empty($alertsDataNav['charting_technical_analysis_alert']) )
                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsDataNav['charting_technical_analysis_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselcharting" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['charting_technical_analysis_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselcharting" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselcharting" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['charting_technical_analysis_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                </li>
    
                <li class="nav-item">
                    <a href="javascript:void(0);" wire:click="changeContentComponent('network')"
                       class="nav-link px-2 my-1 py-0 {{ $content_component == 'network'?'active':'inactive' }}">
                        Network
                    </a>
                    @if( !empty($alertsDataNav['network_alert']) )
                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsDataNav['network_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselNetwork" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['network_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselNetwork" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselNetwork" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['network_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                </li>
                <li class="nav-item">
                    <a href="{{ route('stock-screener-widget') }}" target="_blank"
                       {{--wire:click="changeContentComponent('charting')"--}}
                       class="nav-link px-2 my-1 py-0 {{ $content_component == 'charting'?'active':'inactive' }}">
                       Screening
                    </a>
                    @if( !empty($alertsDataNav['screening_alert']) )
                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsDataNav['screening_alert']) > 1 )
                                <div class="tooltip-content">
                                <div id="alertCarouselScreening" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                    
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">

                                        @foreach($alertsDataNav['screening_alert'] as $item)

                                            @if($loop->first)
                                                <div class="item active">
                                                    {!! $item !!}
                                                </div>
                                            @else
                                                <div class="item">
                                                    {!! $item !!}
                                                </div>
                                            @endif
                                            
                                        @endforeach
                                        
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#alertCarouselScreening" data-slide="prev">
                                      <i class="bx bx-skip-previous"></i>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#alertCarouselScreening" data-slide="next">
                                      <i class="bx bx-skip-next"></i>
                                      <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                </div>
                                @else
                                    <div class="tooltip-content" style="padding: 15px;">    
                                        {!! $alertsDataNav['screening_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif

                </li>
                <li class="nav-item">
                    <a href="javascript:void(0);" wire:click="changeContentComponent('library')"
                       class="nav-link px-2 my-1 py-0 {{ $content_component == 'library'?'active':'inactive' }}">
                        Library
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a href="javascript:void(0);" class="nav-link">
                        My Account
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

