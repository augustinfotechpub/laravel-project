@if(isset($items['rows']))
    @foreach($items['rows'] as $item_first)
        <tr>
            @foreach($item_first['columns'] as $item1)
                <td>{!! $item1 !!}</td>
            @endforeach
        </tr>
    @endforeach
    @if(isset($item_first['rows']))
        @foreach($item_first['rows'] as $item_second)
            <tr>
                @foreach($item_second['columns'] as $item2)
                    <td>{!! $item2 !!}</td>
                @endforeach
            </tr>
        @endforeach
        @if(isset($item_second['rows']))
            @foreach($item_second['rows'] as $item_third)
                <tr>
                    @foreach($item_third['columns'] as $item3)
                        <td>{!! $item3 !!}</td>
                    @endforeach
                </tr>
            @endforeach
            @if(isset($item_third['rows']))
                @foreach($item_third['rows'] as $item_forth)
                    <tr>
                        @foreach($item_forth['columns'] as $item4)
                            <td>{!! $item4 !!}</td>
                        @endforeach
                    </tr>
                @endforeach
                @if(isset($item_forth['rows']))
                    @foreach($item_forth['rows'] as $item_five)
                        <tr>
                            @foreach($item_five['columns'] as $item5)
                                <td>{!! $item5 !!}</td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
            @endif
        @endif
    @endif
@endif
