<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
        {
            "symbols": [
            {
                "proName": "FOREXCOM:SPXUSD",
                "title": "S&P 500"
            },
            {
                "proName": "FOREXCOM:NSXUSD",
                "title": "Nasdaq 100"
            },
            {
                "proName": "FX_IDC:EURUSD",
                "title": "EUR/USD"
            },
            {
                "proName": "BITSTAMP:BTCUSD",
                "title": "BTC/USD"
            },
            {
                "proName": "BITSTAMP:ETHUSD",
                "title": "ETH/USD"
            },
            {
                "description": "FB",
                "proName": "NASDAQ:FB"
            },
            {
                "description": "AMZN",
                "proName": "NASDAQ:AMZN"
            },
            {
                "description": "AAPL",
                "proName": "NASDAQ:AAPL"
            },
            {
                "description": "NETFLIX",
                "proName": "NASDAQ:NFLX"
            },
            {
                "description": "ALPHABET INC",
                "proName": "NASDAQ:GOOG"
            },
            {
                "description": "Microsoft Corp(MSFT)",
                "proName": "NASDAQ:MSFT"
            },
            {
                "description": "JPMORGAN CHASE & CO(JPM)",
                "proName": "NASDAQ:JPM"
            },
            {
                "description": "JOHNSON & JOHNSON(JNJ)",
                "proName": "NASDAQ:JNJ"
            },
            {
                "description": "VISA INC.(V)",
                "proName": "NASDAQ:V"
            },
            {
                "description": "WELLS FARGO & COMPANY/MN(WFC)",
                "proName": "NASDAQ:WFC"
            }
        ],
            "showSymbolLogo": true,
            "colorTheme": "light",
            "isTransparent": true,
            "largeChartUrl": "{{ route('ticker-tap') }}",
            "displayMode": "adaptive",
            "locale": "en"
        }
    </script>
</div>
<!-- TradingView Widget END -->
