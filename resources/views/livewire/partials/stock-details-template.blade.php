    <div class="media stock-summary-media pt-2">
        <tr>
        <img src="{{ $profile['image'] }}" width="50" alt="{{ session('profile')['companyName'] }}"
             class="img-circle img-rounded">
        </tr>
        <div class="media-body" style="flex:initial;">
            <h3>{{ $profile['companyName'] }}</h3>
            <small>{{ $profile['symbol']."   -  ".$profile['exchangeShortName'] }}</small>
        </div>
    </div>
    <div class="price-view">
        <h4 class="mb-0 mt-2">
            ${{ number_format(((float)($profile['price'])),2,'.',',') }}
            <sup class="text-warning" id="delayed-tooltip">D</sup>
            <small class=" {{ ($profile['changes']<0)?'text-danger':'text-success' }}">
                {{ number_format(((float)($profile['changes'])),2,'.',',') }}
                ({{ number_format(((float)($profile['changes']/$profile['price'])*100),2,'.',',') }}
                %)
            </small>
        </h4>
    </div>
    <hr class="my-1">
    <div class="stock-summary-status py-2">
    </div>
