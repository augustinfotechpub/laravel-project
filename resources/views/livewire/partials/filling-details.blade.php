<div class="row px-2 pt-4">
    @if(!empty($transactions))
        <div class="col-md-12">
            <div class="table__heading">
                <h4>Sales history for CIK: {{ $cik }}</h4>
            </div>
        </div>
        <div class="col-md-12">
            <div class="table-responsive shadow">
                <table class="table table-hover table-sort mb-0 p-2">
                    <thead>
                    <tr class="border-0 border-bottom-0">
                        <th>Issuer</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Form</th>
                        <th>Owned</th>
                        <th>Transacted</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($transactions as $transaction)
                        @php($color = $transaction["acquistionOrDisposition"] == 'A'?'bg-green-200':'bg-red-200')
                        <tr class="{{ $color }}">
                            <td>{{ $transaction["symbol"]??'--' }}</td>
                            <td class="{{ $color }}">{{ $transaction["transactionDate"]??'--' }}</td>
                            <td class="{{ $color }}">{{ $transaction['acquistionOrDisposition'] == 'A'?'Purchase':'Sale' }}</td>
                            <td class="{{ $color }}">
                                <a class="btn btn-link p-0" data-bs-toggle="modal"
                                   data-bs-target="#fillingDetailModal"
                                   wire:click="getForm('{{ $transaction['link']??'--' }}')">
                                    {{ $transaction['formType']??'--' }}
                                </a>
                            </td>
                            <td class="{{ $color }}">{{ $transaction["securitiesOwned"]??'--' }}</td>
                            <td class="{{ $color }}">{{ $transaction["securitiesTransacted"]??'--' }}</td>
                        </tr>
                    @empty

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    @endif
</div>
