<div wire:init="getAlert" data-alertKey="{{ $alertUniqueKey }}">
    {{--@if($alertExists)
        <div class='has-tooltip'>
            <div class='tooltip-content'>
                {!! $alert->definition !!}
            </div>
            {{ $alert->title }}
        </div>
    @endif--}}
    <div class='has-tooltip'>
        <div class='tooltip-content'>
            Above the line costs are costs related to actors, music, and photography.
        </div>
    </div>
</div>
