<div>
    @php
        $profile =  session('profile');
    @endphp
    <div class="row mt-4 table__heading mx-auto" wire:init="getFillings">
        <div class="col-md-4">
            <h4 class="d-flex flex-row bd-highlight"> Regulatory Filings</h4>
        </div>
        <div class="col-md-8">
            <form wire:submit.prevent="getFillings" class="row g-3 float-end">
                <div class="col-auto">
                    <label for="fillingType" class="form-label"><small>Filing Type:</small></label>
                    {{--<input type="text" class="form-control form-control-sm" wire:model.lazy="type" id="fillingType"
                           placeholder="Filing type">--}}
                    <select id="fillingType" class="form-control form-control-sm" wire:model.lazy="type">
                        <option selected value="">All</option>
                        @foreach(config('filingformtypes') as $formType)
                            <option>{{ $formType }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-auto">
                    <label for="date" class="form-label"><small>Prior to Date:</small></label>
                    <input type="date" class="form-control form-control-sm" wire:model.lazy="date" id="date">
                </div>
                <div class="col-auto">
                    <label class="form-check-label" for="inlineRadio1"><small>Ownership?</small></label><br>
                    <div class="form-check form-check-inline" style="padding-top: 13px;">
                        <input class="form-check-input" type="radio" name="owner" id="include"
                               value="include" wire:model.lazy="owner" checked>
                        <label class="form-check-label" for="include"><small>Include</small></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="owner" id="exclude"
                               value="exclude" wire:model.lazy="owner">
                        <label class="form-check-label" for="exclude"><small>Exclude</small></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="owner" id="only"
                               value="only" wire:model.lazy="owner">
                        <label class="form-check-label" for="only"><small>Only</small></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-label">&nbsp;</label>
                        <button id="{{ \Illuminate\Support\Str::random() }}" type="button"
                                wire:loading.remove="getFillings"
                                class="btn btn-light btn-sm fw-bold" wire:click="getFillings"
                                wire:loading.attr="disabled">
                            Search
                        </button>
                        <button class="btn btn-light btn-sm fw-bold" type="button" disabled wire:loading
                                wire:target="getFillings">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Loading...
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if($loadContent)
        <div class="row">
            <div class="col-md-12">
                <div wire:loading.remove="getFillings" class="table-responsive scrollable-card-body mh-400 shadow-sm">
                    @if(isset($fillings['entry']))
                        <table class="table table-sort table-hover scroll-table p-3">
                            <thead>
                            <tr>
                                <th width="200">Type</th>
                                <th>Description</th>
                                <th width="200">Filing Date</th>
                                <th width="200">Document</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($fillings['entry'] as $entry)
                                <tr>
                                    <td>{{ $entry['content']['filing-type'] }}</td>
                                    <td class="small">{{ $entry['content']['form-name'] }}
                                        <br>Acc-no: {{ $entry['content']['accession-number'] }}
                                        &nbsp;({{ $entry['content']['act']??null }} Act)&nbsp;
                                        Size: {{ $entry['content']['size'] }}</td>
                                    <td>{{ formatDate($entry['content']['filing-date']) }}</td>
                                    <td>
                                        <a class="btn btn-link" target="_blank"
                                           href="{{ $entry['content']['filing-href'] }}">
                                            <i class="fa fa-file-alt"></i>
                                        </a>
                                        {{--@if(in_array($entry['content']['filing-type'],['424B2','FWP','4','3','DEFA14A','DEF 14A','S-8 POS']))
                                            <button class="btn btn-link"
                                                    wire:click="viewDocument('{{ $entry['content']['filing-href'] }}')"
                                                    data-bs-toggle="modal" data-bs-target="#documentModal">
                                                <i class="fa fa-file-alt"></i>
                                            </button>
                                        @endif--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <h4 class="text-center">No Data found</h4>
                    @endif
                </div>
                <div wire:target="getFillings" wire:loading>
                    @include('front.layouts.partials.loader_b')
                </div>
            </div>
            <div class="col-md-12 py-3 text-end pe-3">
                @if($fillings)
                    @foreach($fillings['link'] as $link)
                        @if(in_array($link['@attributes']['rel'],['prev','next']))
                            <button id="{{ \Illuminate\Support\Str::random() }}"
                                    wire:loading.remove="newPage"
                                    class="btn btn-sm btn-info text-white {{ $loop->index == 1?'active':null }}"
                                    wire:click="newPage('{{ $link['@attributes']['href'] }}')"
                                    wire:loading.attr="disabled">
                                {{ ucfirst($link['@attributes']['rel']) }}
                            </button>
                        @endif
                    @endforeach
                @endif
                <button class="btn btn-sm btn-info text-white" type="button" disabled wire:loading
                        wire:target="newPage">
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-12">
                @include('front.layouts.partials.loader_b')
            </div>
        </div>
    @endif
    @if($showECT)
        <div class="row shadow-sm">
            <div class="col-md-12">
                <div class="row mt-4 table__heading mx-auto" wire:init="getEarningCallTranscript">
                    <div class="col-md-6">
                        <h4 class="d-flex flex-row bd-highlight">Earnings Call Transcripts</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-2">
                        @if (!$loadEarningCallTranscript)
                            @foreach($earningCallTranscript as $ect)
                                <div class="badge bg-light m-2">
                                    <a target="_blank"
                                       href="{{ route('earning_call_transcript',['symbol'=>$symbol,'quarter'=>$ect['quarter'],'year'=>$ect['year']]) }}"
                                       class="list-group-item list-group-item-action">
                                        {{ ' On Q'.$ect['quarter'].' '.$ect['year'] }}
                                    </a>
                                </div>
                            @endforeach
                    </div>
                    @else
                        @include('front.layouts.partials.loader_b')
                    @endif
                </div>
            </div>
        </div>
@endif

<!-- Modal -->
    <div wire:ignore.self class="modal fade" id="documentModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="documentModalLabel">Form Details</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div wire:loading.remove="viewDocument" id="fillingDetailModal">
                        {!! $modalData !!}
                    </div>
                    <div wire:target="viewDocument" wire:loading class="col-md-12">
                        @include('front.layouts.partials.loader_b')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





