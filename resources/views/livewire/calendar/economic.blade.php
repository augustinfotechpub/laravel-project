<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"/>
<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div class="tradingview-widget-container__widget"></div>
    <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-events.js" async>
        {
            "colorTheme": "light",
            "isTransparent": false,
            "width": "100%",
            "height": "660",
            "locale": "en",
            "importanceFilter": "-1,0,1",
            "currencyFilter": "USD,ARS,BRL,COP,CLP,PEN,MXN,CAD,AUD,CNY,GBP,EUR,DEM,HKD,INR,ESP,PTE,ITL,FRF,GRD,JPY,KRW,SGD,MYR,SAR,AED"
        }
    </script>
</div>
<!-- TradingView Widget END -->
