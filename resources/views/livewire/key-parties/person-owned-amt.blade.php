<div class="card w-full border-0 shadow">
    <div class="card-header card-header-bg text-light border-bottom-0">
        <h5 class="tippy mb-0" id="{{ getAlertId('percentage_ownership_alert') }}">Percentage Ownership</h5>
    </div>
    <div class="card-body" wire:init="getHolders">
        @if($loading)
            @include('front.layouts.partials.loader')
        @else
            <canvas id="personOwnedAmtChart"></canvas>
        @endif
    </div>
</div>
