<div wire:init="initKeyParties" xmlns:wire="http://www.w3.org/1999/xhtml" xmlns:livewire="">
    @if($initKeyParties)
        <div class="row mt-4">
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="card shadow border-0">
                    <div class="card-header card-header-bg text-light border-bottom-0">
                        <h5 class="tippy mb-0" id="{{ getAlertId('executive_management_alert') }}">EXECUTIVE MANAGEMENT</h5>
                        @if( !empty(@$alertsData['executive_management_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsData['executive_management_alert']) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach(@$alertsData['executive_management_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$alertsData['executive_management_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                    </div>
                    <div class="card-body scrollable-card-body p-0">
                        <div class="list-group">
                            @forelse($executives as $officer)
                                <div
                                    class="list-group-item list-group-item-action  border-end-0 border-start-0 rounded-0 small px-2 py-1">
                                    <div class="d-flex w-100 justify-content-between">
                                            <span class="mb-1">
                                                <strong>
                                                    {{ strtoupper($officer->name) }}<br/>
                                                    <small>({{ $officer->position }})</small>
                                                </strong>
                                            </span>
                                        <small class="ic-tippy" data-tippy-content="Securities Owned by Date {{ $officer->transactionDate?formatDate($officer->transactionDate):'' }}">

                                            <i class="fa fa-currency"></i>
                                            {{-- $officer->securitiesOwned --}}
                                        </small>
                                    </div>
                                    {{-- <div class="text-end text-info text-decoration-underline small">
                                        <a
                                            @if($initKeyParties)
                                            href="#peInfo"
                                            wire:click="getDetails('{{ $officer->id }}')"
                                            wire:key="{{ $loop->index }}" id="getDetail"
                                            @endif>
                                            View More
                                        </a>
                                    </div> --}}
                                </div>
                            @empty
                            <h3>No Record Found</h3>    
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-3">
                <div class="card shadow border-0">
                    <div class="card-header card-header-bg text-light border-bottom-0">
                        <h5 class="tippy mb-0" id="{{ getAlertId('analyst_ratings_alert') }}">ANALYST RATINGS</h5>
                        @if( !empty(@$alertsData['analyst_ratings_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsData['analyst_ratings_alert']) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel1" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach(@$alertsData['analyst_ratings_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel1" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel1" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$alertsData['analyst_ratings_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                    </div>
                    <div class="card-body scrollable-card-body p-0">
                        <table class="table table-stripe">
                            @forelse($analystratings as $tr)
                                <tr>
                                    <td>Rating</td>
                                    <td>{{  strtoupper($tr->Rating) }}</td>
                                </tr>
                                <tr>
                                    <td>Target Price</td>
                                    <td>{{ strtoupper($tr->TargetPrice) }}</td>
                                </tr>
                                <tr>
                                    <td>Strong Buy</td>
                                    <td>{{ strtoupper($tr->StrongBuy) }}</td>
                                </tr>
                                <tr>
                                    <td>Buy</td>
                                    <td>{{ strtoupper($tr->Buy) }}</td>
                                </tr>
                                <tr>
                                    <td>Hold</td>
                                    <td>{{ strtoupper($tr->Hold) }}</td>
                                </tr>
                                <tr>
                                    <td>Sell</td>
                                    <td>{{ strtoupper($tr->Sell) }}</td>
                                </tr>
                                <tr>
                                    <td>StrongSell</td>
                                    <td>{{ strtoupper($tr->StrongSell) }}</td>
                                </tr>
                            @empty
                            <h3>No Record Found</h3>    
                            @endforelse    
                        </table>
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-3 col-md-6 mb-3">
                <div class="card shadow border-0">
                    <div class="card-header card-header-bg text-light border-bottom-0">
                        <h5 class="mb-0">AUDIT COMMITTEE</h5>
                    </div>
                    <div class="card-body scrollable-card-body px-0" wire:init="getAuditCommitteeMembers">
                        @if(isset($company->auditCommitteeMembers))
                            <table class="table table-stripe">
                                @foreach($company->auditCommitteeMembers as $member)
                                    <tr>
                                        <td>{{ ucfirst($member->name) }} {!! $member->is_chairman?'<small>Chairman</small>':null !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>--}}
            <div class="col-lg-6 col-md-6 mb-3" >
                <div class="card shadow border-0">
                    <div class="card-header card-header-bg text-light border-bottom-0" style="display:inline-flex;">
                        <h5 class="tippy mb-0" id="{{ getAlertId('ownership_interest_alert') }}">OWNERSHIP INTEREST</h5>
                        @if( !empty(@$alertsData['ownership_interest_alert']) )
                            <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                @if( count(@$alertsData['ownership_interest_alert']) > 1 )
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                        <div id="alertCarousel2" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                            
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach(@$alertsData['ownership_interest_alert'] as $item)

                                                    @if($loop->first)
                                                        <div class="item active">
                                                            {!! $item !!}
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            {!! $item !!}
                                                        </div>
                                                    @endif
                                                    
                                                @endforeach
                                                
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#alertCarousel2" data-slide="prev">
                                            <i class="bx bx-skip-previous"></i>
                                            <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#alertCarousel2" data-slide="next">
                                            <i class="bx bx-skip-next"></i>
                                            <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                @else
                                    <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                        {!! @$alertsData['ownership_interest_alert'][0] !!}
                                    </div>
                                @endif
                            </i>
                        @endif
                        <select id="holder" style="color:black; margin-left:20px;">
                            <option value="Institutions">Institutions</option> 
                            <option value="Funds">Funds</option>     
                        </select> 
                    </div>
                    <div id="holdertable" class="card-body scrollable-card-body p-0">
                        <table class="table table-stripe">
                                <tr>
                                    <th>NAME</th>
                                    <th>Total <br> Shares</th>
                                    <th>Total <br> Assests</th>
                                    <th>Current <br> Shares</th>
                                    <th style="vertical-align: top;">Change</th>
                                    <th style="text-align: center;">% <br> Change</th>
                                </tr>
                            @forelse($institutionalShareholders as $tr)
                                <tr>
                                    <td>{{ ($tr->holder_name) }}</td>
                                    <td>{!! decimal($tr->totalShares) !!}M</td>
                                    <td>{!! decimal($tr->totalAssets) !!}M</td>
                                    <td>{!! number_format($tr->currentShares) !!}M</td>
                                    <td>{!! number_format($tr->change) !!}M</td>
                                    <td>{!! decimal($tr->change_p) !!}</td>
                                </tr>
                            @empty
                            <h3>No Record Found</h3>    
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" wire:loading wire:target="getDetails">
                @include('front.layouts.partials.loader_b')
            </div>
        </div>
        <div class="row mt-1" wire:init="getDetails" wire:loading.remove="getDetails">
            @if($initGetDetails)
                <div class="col-lg-6 col-md-6 mb-6">
                    <div class="row">
                        {{-- @include('livewire.key-parties.person-entity-details') --}}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 mb-6">
                    <div class="row">
                        <div class="col-12">
                            @if($initKeyParties)
                                {{-- <livewire:key-parties.person-owned-amt/> --}}
                            @endif
                        </div>
                        {{--<div class="col-12 d-none" id="chsw">
                            <div class="row mt-4">
                                <div class="col-md-12">
                                    --}}{{-- Specific Person Chart will be here--}}{{--
                                    <div class="table__heading mt-0">
                                        <h5>Current Holding Shares weightings</h5>
                                    </div>
                                    <canvas id="personHoldingsChart" class="shadow"></canvas>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            @else
                <div class="row mt-4">
                    @include('front.layouts.partials.loader_b')
                </div>
            @endif
        </div>
    @else
        <div class="row mt-4">
            @include('front.layouts.partials.loader_b')
        </div>
    @endif
</div>

<script>
    $(document).on("change","body #holder",function()
    {
    var t = $(this).val(); 
      $.ajax({
        type:"GET",
        data:"t="+t,
        url:"{{url('funds')}}",
        success:function(data)
        {
            $('#holdertable').html("");
            $('#holdertable').html(data);
        }
            });
    });
</script>