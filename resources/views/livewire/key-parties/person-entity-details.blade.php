@if(!is_null($currentOfficer))
    <div id="peInfo">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-full border-0 shadow">
                    <div class="card-header card-header-bg text-light border-bottom-0">
                        <h5 class="mb-0">About {{ $currentOfficer->name }}</h5>
                    </div>
                    <div class="card-body text-justify">
                        {!! $currentOfficer->biography??null !!}
                    </div>
                </div>
                {{--<div class="card w-full border-0 shadow">
                    <div class="card-header card-header-bg text-light border-bottom-0">
                        <h5 class="mb-0">Insider Roles</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-sort mb-0 p-2" id="pe-details">
                            @foreach($peInfo as $info)
                                @if($loop->first)
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Transaction Date</th>
                                        <th>Position</th>
                                    </tr>
                                    </thead>
                                @endif
                                <tr>
                                    <td>
                                        <strong>{{ ucfirst($info['issuer']) }}</strong>
                                        <small>{{ $info['is_current']?'Current':null }}</small>
                                    </td>
                                    <td>{{ $info['transaction_date'] }}</td>
                                    <td>{{ $info['ownership'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>--}}
                {{--@if(count($personHoldingsData) > 0)
                    <div class="row mt-1">
                        <div class="col-md-12">
                            <div class="table__heading mt-0">
                                <h5>
                                    Holdings {{ $activePersonName??null }}
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive shadow">
                                <table class="table table-hover table-sort mb-0 p-2" id="pe-details">
                                    @foreach($personHoldingsData as $personHolding)
                                        @if($loop->first)
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>Equities Date</th>
                                                <th>Shares</th>
                                                <th>Valuations</th>
                                            </tr>
                                            </thead>
                                        @endif
                                        <tr>
                                            <td><strong>{{ $personHolding['issuer'] }}</strong></td>
                                            <td>{{ $personHolding['date'] }}</td>
                                            <td>{{ $personHolding['noso'] }}</td>
                                            <td>
                                                ${{ number_format($personHolding['noso']*session('profile')['price'],2) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endif--}}
            </div>

{{--            @if($showPersonTransactionsChart)--}}
{{--                <div class="col-md-6 shadow px-0">--}}
{{--                    --}}{{--<livewire:key-parties.person-transactions-chart/>--}}
{{--                    <div class="card-header card-header-bg text-light border-bottom-0">--}}
{{--                        <h5 class="mb-0">SALES HISTORY</h5>--}}
{{--                    </div>--}}
{{--                    <div id="personTransactionsChart"></div>--}}
{{--                    --}}{{--@include('livewire.filling-details',['form'=>$form,'transactions'=>$transactions])--}}
{{--                </div>--}}
{{--            @endif--}}
        </div>
    </div>
@endif
