@php
     $institutionalShareholders=json_decode($institutionalShareholders)
@endphp
<div class="card-body scrollable-card-body p-0">
    <table class="table table-stripe">
        
            <tr>
                <th>NAME</th>
                <th>Total <br> Shares</th>
                <th>Total <br> Assests</th>
                <th>Current <br> Shares</th>
                <th style="vertical-align: top;">Change</th>
                <th style="text-align: center;">% <br> Change</th>
            </tr>
        
        
        @forelse($institutionalShareholders as $tr)
            <tr>
                {{-- @foreach($tr as $td)
                    <td>{{ $loop->first?strtoupper($td):$td }}</td>
                @endforeach --}}
                <td>{{ ($tr->holder_name) }}</td>
                <td>{!! decimal($tr->totalShares) !!}M</td>
                <td>{!! decimal($tr->totalAssets) !!}M</td>
                <td>{!! number_format($tr->currentShares) !!}M</td>
                <td>{!! number_format($tr->change) !!}M</td>
                <td>{!! decimal($tr->change_p) !!}</td>
            </tr>
        @empty
        <h3>No Record Found</h3>    
        @endforelse
    </table>
</div>