<div>
    @if(!is_null($personTransactionsChartModel))
        <div class="card-header card-header-bg text-light border-bottom-0">
            <h5 class="mb-0">SALES HISTORY</h5>
        </div>
        <livewire:livewire-column-chart :column-chart-model="$personTransactionsChartModel"/>
    @endif
</div>
