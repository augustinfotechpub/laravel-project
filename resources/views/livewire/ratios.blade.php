<div wire:init="getData">
    <div class="container ratioTab">
      @if($initSection)
        <div class="row mt-5">
            <div class="col-md-6">
              <div class="col-md-12">
                  <div class="dropdown dropdownInBlock">
                      <div class="tippy dropdown-toggle item text-uppercase text-sm font-black"  type="button" id="comparablesDropdown" data-toggle="dropdown"
                          aria-expanded="false">
                          {{$year}}
                      </div>
                      <ul class="dropdown-menu scrollable-menu" role="menu" style="height: auto;max-height: 200px; overflow-x: hidden;" aria-labelledby="comparablesDropdown">
                          @foreach($allYear as $year)
                              @if($this->period=='yearly')
                              <li>
                                  <a class="dropdown-item" wire:click="change('yearly','{{$year}}')" >
                                      {{$year}}
                                  </a>
                              </li>
                              @else
                                  <li>
                                      <a class="dropdown-item" wire:click="change('quarterly','{{$year}}')" >
                                          {{ \Carbon\Carbon::parse($year)->format('m/d/Y') }}
                                      </a>
                                  </li> 
                              @endif        
                          @endforeach
                      </ul>
                  </div>  
                  <div class="col-md-4" style="float: right;">
                      <div class="btn-group float-end" wire:loading="changeRatioPeriod" wire:target="changeRatioPeriod">
                          <button class="btn btn-sm btn-primary" type="button" disabled>
                              <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                              Loading...
                          </button>
                      </div>
                      <div class="btn-group float-end" wire:loading.remove="changeRatioPeriod">
                          <button class="btn btn-{{ $period == 'quarterly'?"primary":"secondary"}} btn-sm"
                                  wire:click="changeRatioPeriod('quarterly')">
                              Quarterly
                          </button>
                          <button class="btn btn-{{ $period == 'yearly'?"primary":"secondary"}} btn-sm"
                                  wire:click="changeRatioPeriod('yearly')">
                              Annual
                          </button>
                      </div>
                  </div>  
              </div>
              @if($ratios['Coverage Ratios'])
                  <div class="col-md-12 mt-2">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('coverage_ratios_alert') }}">{{ $ratios['Coverage Ratios']['heading'] }}</span>
                                 @if( !empty(@$alertsData['coverage_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['coverage_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">      
                                              <div id="alertCarousel_coverage_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['coverage_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
                                              
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_coverage_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_coverage_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['coverage_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif 
                              </h6></td>
                          </tr>
                          @foreach($ratios['Coverage Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                      @if( !empty(@$alertsData[$item['identifier']]) )
                                      <i class="bx bx-info-circle financialSlider alertSlider has-tooltip" >
                                          @if( count(@$alertsData[$item['identifier']]) > 1 )
                                              <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                  <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                      
                                                      <!-- Wrapper for slides -->
                                                      <div class="carousel-inner">
                                                          @foreach(@$alertsData[$item['identifier']] as $item1)
          
                                                              @if($loop->first)
                                                                  <div class="item active">
                                                                      {!! $item1 !!}
                                                                  </div>
                                                              @else
                                                                  <div class="item">
                                                                      {!! $item1 !!}
                                                                  </div>
                                                              @endif
                                                              
                                                          @endforeach
                                                          
                                                      </div>
                                                  </div>
                                                      <!-- Left and right controls -->
                                                      <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                      <i class="bx bx-skip-previous"></i>
                                                      <span class="sr-only">Previous</span>
                                                      </a>
                                                      <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                      <i class="bx bx-skip-next"></i>
                                                      <span class="sr-only">Next</span>
                                                      </a>
                                                  </div>
                                              </div>
                                          @else
                                              <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                  {!! @$alertsData[$item['identifier']][0] !!}
                                              </div>
                                          @endif
                                      </i>
                                    @endif
                                    <i class="bx bx-info-circle ratioSlider has-tooltip">
                                      <div class="tooltip-content">
                                        @switch($item['title'])
                                          @case('Interest Coverage')
                                              ebit / interestExpense
                                            @break
  
                                          @case('Fixed Charge Coverage')
                                              (ebit + capitalLeaseObligations) / ( interestExpense + capitalLeaseObligations)
                                            @break
  
                                          @default
                                        @endswitch
                                      </div>
                                    </i>
  
                                  </td>
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Activity Ratios'])
                  <div class="col-md-12">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('activity_ratios_alert') }}">{{ $ratios['Activity Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['activity_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['activity_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_activity_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['activity_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_activity_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_activity_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['activity_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Activity Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                  <i  class="bx bx-info-circle ratioSlider has-tooltip" >
                                    <div class="tooltip-content" >
                                      @switch($item['title'])
                                        @case('Inventory Turnover')
                                        costOfRevenue / (inventory (in previous period) + inventory (in current period) / 2)
                                          @break
  
                                        @case('Days of Inventory on Hand (DOH)')
                                        365 (for yearly calculation) or 90 (for quarterly calculation) / inventory turnover (the output of the entire equation above)
                                          @break
  
                                        @case('Receivables Turnover')
                                        totalRevenue / (netReceivables (in previous period) + netReceivables (in current period) / 2)
                                        @break
  
                                        @case('Days of Sales Outstanding (DSO)')
                                        365 (for yearly calculation) or 90 (for quarterly calculation) / receivables turnover (the output of the entire equation above)
                                          @break
  
                                        @case('Payables Turnover')
                                        costOfRevenue / (accountsPayable (in previous period) + accountsPayable (in current period) / 2)
                                          @break
  
                                        @case('Days Payable Outstanding (DPO)')
                                        365 (for yearly calculation) or 90 (for quarterly calculation / payables turnover (the output of the entire equation above)
                                          @break
  
                                        @case('Working Capital Turnover')
                                        totalRevenue / (netWorkingCapital (in previous period) + netWorkingCapital (in current period) / 2)
                                          @break
  
                                        @case('Fixed Asset Turnover')
                                        totalRevenue / (propertyPlantEquipment (in previous period) + propertyPlantEquipment (in current period) / 2)
                                          @break
  
                                        @case('Total Asset Turnover')
                                        totalRevenue / (totalAssets (in previous period) + totalAssets (in current period) / 2)
                                          @break
                                        
                                        @default
                                      @endswitch
                                    </div>
                                  </i>
  
                                  </td>
  
                              <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Performance Ratios'])
                  <div class="col-md-12 mt-2">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('performance_ratios_alert') }}">{{ $ratios['Performance Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['performance_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['performance_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_performance_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['performance_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_performance_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_performance_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['performance_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Performance Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif 
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                      <div class="tooltip-content">
                                        @switch($item['title'])
                                          @case('Cash Flow to Revenue')
                                          totalCashFromOperatingActivities / totalRevenue
                                            @break
  
                                          @case('Cash Return on Assets')
                                          totalCashFromOperatingActivities / (totalAssets (in previous period) + totalAssets (in current period) / 2)
                                            @break
  
                                          @case('Cash Return on Equity')
                                          totalCashFromOperatingActivities / (totalStockholderEquity (in previous period) + totalStockholderEquity (in current period) / 2)
                                          @break
  
                                          @case('Cash to Operating Income')
                                          totalCashFromOperatingActivities / operatingIncome
                                            @break
  
                                          @case('Cash Flow Per Share')
                                          totalCashFromOperatingActivities - (preferredStockTotalEquity (if present) (times .015 for quarterly calculation) or (times .06 for annual)) / (commonStockSharesOutstanding (in previous period) + commonstocksharesoutstanding (in current period) / 2)
                                            @break
  
                                          @case('Debt Payment')
                                          totalCashFromOperatingActivities / (longTermDebtTotal (times .01125 for quarterly calculation) or (times .045 for annual))
                                            @break
  
                                          @case('Dividend Payment')
                                            totalCashFromOperatingActivities / dividendsPaid
                                            @break
  
                                          @case('Investing and Financing')
                                            totalCashFromOperatingActivities / (totalCashflowsFromInvestingActivities + totalCashFromFinancingActivities)
                                            @break
  
                                          @case('Debt Coverage')
                                            totalCashFromOperatingActivities / (shortLongTermDebtTotal (in previous period) + shortLongTermDebtTotal (in current period) / 2)
                                            @break
  
                                          @case('Performance Interest Coverage')
                                            (totalCashFromOperatingActivities + interestExpense + incomeTaxExpense) / interestExpense
                                            @break
  
                                          @case('Reinvestment')
                                            totalCashFromOperatingActivities / capitalExpenditures
                                            @break
  
                                          @default
                                        @endswitch
                                      </div>
                                    </i>
  
                                  </td>
  
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Credit Ratios'])
                  <div class="col-md-12 mt-2">
                      <table class="table table-sort table-hover full-width-table mb-0">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('credit_ratios_alert') }}">{{ $ratios['Credit Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['credit_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['credit_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_credit_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['credit_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_credit_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_credit_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['credit_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Credit Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                  <div class="tooltip-content">
                                    @switch($item['title'])
                                      @case('EBIT Interest Coverage')
                                          ebit / interestExpense
                                        @break
  
                                      @case('EBITDA Interest Coverage')
                                          ebitda / interestExpense
                                        @break
  
                                      @case('Return On Capital')
                                          ebit / (totalStockholderEquity (in previous period) + shortLongTermDebtTotal (in previous period)) + (totalStockholderEquity (in current period) + shortLongTermDebtTotal (in current period) / 2)
                                        @break
  
                                      @case('Free Operating Cash Flow to Debt')
                                          (totalCashFromOperatingActivities - capitalExpenditures) / (shortLongTermDebtTotal (in previous period) + shortLongTermDebtTotal (in current period) / 2)
                                        @break
  
                                      @case('Discretionary Cash Flow to Debt')
                                          (totalCashFromOperatingActivities - capitalExpenditures - dividendsPaid) / (shortLongTermDebtTotal (in previous period) + shortLongTermDebtTotal (in current period) / 2)
                                        @break
  
                                      @case('Cash Flow (net) to CAPEX')
                                          (totalCashFromOperatingActivities - dividendsPaid) / capitalExpenditures
                                        @break
  
                                      @default
                                    @endswitch
                                  </div>
                                </i>
  
                              </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Return on Sales (Revenue) Ratios'])
                  <div class="col-md-12 mt-2">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('return_on_sale_ratios_alert') }}">{{ $ratios['Return on Sales (Revenue) Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['return_on_sale_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['return_on_sale_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_return_on_sale_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['return_on_sale_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_return_on_sale_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_return_on_sale_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['return_on_sale_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Return on Sales (Revenue) Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                  <div class="tooltip-content">
                                    @switch($item['title'])
                                      @case('Gross Profit Margin')
                                      grossProfit / totalRevenue
                                        @break
  
                                      @case('Operating Margin')
                                      operatingIncome / totalRevenue
                                        @break
  
                                      @case('Pre-Tax Margin')
                                      incomeBeforeTax / totalRevenue
                                      @break
                                      
                                      @case('Net Income (Profit) Margin')
                                      netIncome / totalRevenue 
                                      @break
                                      @default
                                    @endswitch
                                  </div>
                                </i>
  
                              </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Return on Investment Ratios'])
                  <div class="col-md-12 mt-2 mb-2">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4 bottom-table">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('return_on_investment_ratios_alert') }}">{{ $ratios['Return on Investment Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['return_on_investment_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['return_on_investment_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_return_on_investment_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['return_on_investment_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_return_on_investment_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_return_on_investment_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['return_on_investment_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Return on Investment Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif                                
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                  <div class="tooltip-content">
                                    @switch($item['title'])
                                      @case('Operating ROA')
                                      operatingIncome / (totalAssets (in previous period) + totalAssets (in current period) / 2)
                                        @break
  
                                      @case('ROA')
                                      netIncome / (totalAssets (in previous period) + totalAssets (in current period) / 2)
                                        @break
  
                                      @case('Return on Total Capital')
                                      ebit / (netDebt (in previous period) + netDebt (in current period) + totalStockholderEquity (in previous period) + totalStockholderEquity(in current period) / 2)
                                        @break
  
                                      @case('Return on Total Equity')
                                        netIncome / (totalStockholderEquity (in previous period) + totalStockholderEquity (in current period) / 2)
                                        @break
  
                                      @case('Return on Common Equity')
                                        netIncome - (preferredStockTotalEquity (if present) (times .015 for quarterly calculation) or (times .06 for annual)) /  (commonStockTotalEquity (in previous period) + commonStockTotalEquity (in current period) / 2)
                                        @break
  
                                      @default
                                    @endswitch
                                  </div>
                                </i>
  
                              </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
          </div>
          <div class="col-md-6">
              @if($ratios['Solvency Ratios'])
                  <div class="col-md-12">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('solvency_ratios_alert') }}">{{ $ratios['Solvency Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['solvency_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['solvency_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_solvency_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['solvency_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_solvency_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_solvency_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['solvency_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Solvency Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                     <i class="bx bx-info-circle ratioSlider has-tooltip">
                                      <div class="tooltip-content">
                                        @switch($item['title'])
                                          @case('Debt-to-Asset Ratio')
                                          shortLongTermDebtTotal / totalAssets
                                            @break
  
                                          @case('Debt-to-Capital Ratio')
                                          shortLongTermDebtTotal / (shortLongTermDebtTotal + totalStockholderEquity)
                                            @break
  
                                          @case('Debt-to-Equity')
                                          shortLongTermDebtTotal / totalStockholderEquity
                                            @break
  
                                          @case('Financial Leverage')
                                          totalAssets / totalStockholderEquity
                                            @break
  
                                          @default
                                        @endswitch
                                      </div>
                                    </i>
  
                                  </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Valuation Ratios'])
                  <div class="col-md-12 mt-2">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('valuation_ratios_alert') }}">{{ $ratios['Valuation Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['valuation_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['valuation_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_valuation_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['valuation_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_valuation_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_valuation_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['valuation_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Valuation Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                      <div class="tooltip-content">
                                        @switch($item['title'])
                                          @case('P/E')
                                          market price of 1 share of stock / epsActual
                                            @break
  
                                          @case('P/CF')
                                          market price of 1 share of stock / ((freeCashFlow + capitalExpenditures)/ commonStockSharesOutstanding)
                                            @break
  
                                          @case('P/S')
                                          market price of 1 share of stock / (totalRevenue / commonStockSharesOutstanding)
                                          @break
                                          
                                          @case('P/BV')
                                          market price of 1 share of stock / ((totalAssets - totalLiab) / commonStockSharesOutstanding) 
                                            @break
  
                                          @default
                                        @endswitch
                                      </div>
                                    </i>
  
                                  </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Price Per Share Ratios'])
                  <div class="col-md-12 mt-2">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('price_per_share_ratios_alert') }}">{{ $ratios['Price Per Share Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['price_per_share_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['price_per_share_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_price_per_share_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['price_per_share_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_price_per_share_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_price_per_share_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['price_per_share_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Price Per Share Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                      <div class="tooltip-content">
                                        @switch($item['title'])
                                          @case('Cash Flow Per Share')
                                          (totalCashFromOperatingActivities - preferredStockTotalEquity (if present) (times .015 for quarterly calculation) or (times .06 for annual)) / ((commonStockSharesOutstanding (previous period) + commonStockSharesOutstanding (current period) / 2)
                                            @break
  
                                          @case('EBITDA / Share')
                                          ebitda / ((commonStockSharesOutstanding (previous period) + commonStockSharesOutstanding (current period) / 2)
                                            @break
  
                                          @case('Dividend Per Share')
                                          dividendsPaid / commonStockSharesOutstanding
                                          @break
  
                                          @default
                                        @endswitch
                                      </div>
                                    </i>
  
                                  </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Dividend-Related Ratios'])
                  <div class="col-md-12">
                      <table class="table table-sort table-hover full-width-table mb-0 mt-4">
                          <tr>
                              <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('dividend_payout_ratios_alert') }}">{{ $ratios['Dividend-Related Ratios']['heading'] }}</span>
                                  @if( !empty(@$alertsData['dividend_payout_ratios_alert']) )
                                  <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                      @if( count(@$alertsData['dividend_payout_ratios_alert']) > 1 )
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                              <div id="alertCarousel_dividend_payout_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                  
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner">
                                                      @foreach(@$alertsData['dividend_payout_ratios_alert'] as $item1)
      
                                                          @if($loop->first)
                                                              <div class="item active">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @else
                                                              <div class="item">
                                                                  {!! $item1 !!}
                                                              </div>
                                                          @endif
                                                          
                                                      @endforeach
                                                      
                                                  </div>
      
                                                  <!-- Left and right controls -->
                                                  <a class="left carousel-control" href="#alertCarousel_dividend_payout_ratios_alert" data-slide="prev">
                                                  <i class="bx bx-skip-previous"></i>
                                                  <span class="sr-only">Previous</span>
                                                  </a>
                                                  <a class="right carousel-control" href="#alertCarousel_dividend_payout_ratios_alert" data-slide="next">
                                                  <i class="bx bx-skip-next"></i>
                                                  <span class="sr-only">Next</span>
                                                  </a>
                                              </div>
                                          </div>
                                      @else
                                          <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                              {!! @$alertsData['dividend_payout_ratios_alert'][0] !!}
                                          </div>
                                      @endif
                                  </i>
                                @endif
                              </h6></td>
                          </tr>
                          @foreach($ratios['Dividend-Related Ratios']['rows'] as $item)
                              <tr>
                                  <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                                  <i class="bx bx-info-circle ratioSlider has-tooltip">
                                      <div class="tooltip-content">
                                        @switch($item['title'])
                                          @case('Dividend Payout Ratio')
                                          dividendsPaid / netIncomeApplicableToCommonShares
                                            @break
  
                                          @case('Dividend Retention Rate')
                                          (netIncomeApplicableToCommonShares - dividendsPaid) / netIncomeApplicableToCommonShares
                                            @break
  
                                          @case('Sustainable Growth Rate')
                                          Dividend Retention Rate / Return on Common Equity (both calculated above)
                                          @break
  
                                          @default
                                        @endswitch
                                      </div>
                                    </i>
  
                                  </td>
  
                                  <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              @endif
              @if($ratios['Liquidity Ratios'])
              <div class="col-md-12 ">
                  <table class="table table-sort table-hover full-width-table mb-0 mt-4 bottom-table">
                      <tr>
                          <td colspan="2"><h6><span class="tippy mx-auto px-0" id="{{ getAlertId('liquidity_ratios_alert') }}">{{ $ratios['Liquidity Ratios']['heading'] }}</span>
                              @if( !empty(@$alertsData['liquidity_ratios_alert']) )
                              <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                  @if( count(@$alertsData['liquidity_ratios_alert']) > 1 )
                                      <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                          <div id="alertCarousel_liquidity_ratios_alert" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                              
                                              <!-- Wrapper for slides -->
                                              <div class="carousel-inner">
                                                  @foreach(@$alertsData['liquidity_ratios_alert'] as $item1)
  
                                                      @if($loop->first)
                                                          <div class="item active">
                                                              {!! $item1 !!}
                                                          </div>
                                                      @else
                                                          <div class="item">
                                                              {!! $item1 !!}
                                                          </div>
                                                      @endif
                                                      
                                                  @endforeach
                                                  
                                              </div>
  
                                              <!-- Left and right controls -->
                                              <a class="left carousel-control" href="#alertCarousel_liquidity_ratios_alert" data-slide="prev">
                                              <i class="bx bx-skip-previous"></i>
                                              <span class="sr-only">Previous</span>
                                              </a>
                                              <a class="right carousel-control" href="#alertCarousel_liquidity_ratios_alert" data-slide="next">
                                              <i class="bx bx-skip-next"></i>
                                              <span class="sr-only">Next</span>
                                              </a>
                                          </div>
                                      </div>
                                  @else
                                      <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                          {!! @$alertsData['coverage_ratios_alert'][0] !!}
                                      </div>
                                  @endif
                              </i>
                            @endif
                          </h6></td>
                      </tr>
                      @foreach($ratios['Liquidity Ratios']['rows'] as $item)
                          <tr>
                              <td><span class="tippy mx-auto px-0" id="{{ getAlertId($item['identifier']) }}">{{ $item['title'] }}</span>
                                  @if( !empty(@$alertsData[$item['identifier']]) )
                                    <i class="bx bx-info-circle financialSlider alertSlider has-tooltip">
                                        @if( count(@$alertsData[$item['identifier']]) > 1 )
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">
                                                <div id="alertCarousel_{{$item['identifier']}}" class="carousel" data-interval="false" data-ride="carousel" data-wrap="false">
                                                    
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        @foreach(@$alertsData[$item['identifier']] as $item1)
        
                                                            @if($loop->first)
                                                                <div class="item active">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    {!! $item1 !!}
                                                                </div>
                                                            @endif
                                                            
                                                        @endforeach
                                                        
                                                    </div>
        
                                                    <!-- Left and right controls -->
                                                    <a class="left carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="prev">
                                                    <i class="bx bx-skip-previous"></i>
                                                    <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#alertCarousel_{{$item['identifier']}}" data-slide="next">
                                                    <i class="bx bx-skip-next"></i>
                                                    <span class="sr-only">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        @else
                                            <div class="tooltip-content" style="margin-top:0px; z-index:1000;">    
                                                {!! @$alertsData[$item['identifier']][0] !!}
                                            </div>
                                        @endif
                                    </i>
                                  @endif
                              <i class="bx bx-info-circle ratioSlider has-tooltip">
                                  <div class="tooltip-content">
                                    @switch($item['title'])
                                      @case('Current Ratio')
                                      totalCurrentAssets / totalCurrentLiabilities
                                        @break
  
                                      @case('Quick Ratio')
                                      (cashAndShortTermInvestments + netReceivables) / totalCurrentLiabilities
                                        @break
  
                                      @case('Cash Ratio')
                                      cashAndShortTermInvestments / totalCurrentLiabilities
                                      @break
                                      
                                      @case('Defensive Interval Ratio')
                                      (cashAndShortTermInvestments + netReceivables) / (totalOperatingExpenses / 90 (quarterly) or 365 (yearly))
                                      @break
  
                                      @case('Cash Conversion Cycle')
                                      DOH + DSO - DPO (all 3 calculated under activity ratios)
                                        @break
  
                                      @default
                                    @endswitch
                                  </div>
                                </i>
  
                              </td>
  
                              <td class="text-end">{!! amountRatio(@$item['value']) !!}</td>
                          </tr>
                      @endforeach
                  </table>
              </div>
              @endif
          </div>
        </div>
      @else
          @include('front.layouts.partials.loader_b')
      @endif
    </div>
  </div>
  