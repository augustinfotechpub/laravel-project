<div>
    <div class="stock-summary-banner py-0" wire:init="initStockSummary">
        <div class="container-fluid">
            @if($initStockSummary)
                
                <div class="row mx-auto w-100 key-industry-library">
                    <div class="col-md-12 col-sm-12 mt-1 left-keyconcept" wire:init="getLibraryInformation">
                        @if($showIndustryInfo)
                            @include('livewire.partials.key-industry-info-library')
                        @endif
                    </div>
                </div>
                    
                <script type="text/javascript">
                    function fixtooltip(elem) {
                        if (elem.length) {
                            var left = (elem[0].offsetLeft - elem[0].scrollLeft + elem[0].clientLeft);
                            if (!elem.find('.tooltip-content').hasClass('lengthy-tooltip') && left >= 1000) {
                                elem.find('.tooltip-content').addClass('lengthy-tooltip');
                            }
                        }
                    }
                    $(document).ready(function() {
                        $('.has-tooltip').mouseover(function() {
                            fixtooltip($(this));
                        });
                        
                        setTimeout(() => {
                            $('.has-tooltip').each(function() {
                                fixtooltip($(this));
                            });
                        }, 1500);
                    })
                </script>
            @else
                <div class="row mx-auto w-100 key-industry-library">
                    <div class="col-md-12 col-sm-12 mt-1 sleft-keyconcept" wire:init="getLibraryInformation">
                        @include('front.layouts.partials.loader_b')
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

