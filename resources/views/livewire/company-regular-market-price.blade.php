<div>
    <div {{--wire:poll="2s"--}}>
        <div class="card">
            <div class="card-body">
                {{ "$ ". $company->price->regularMarketPrice->raw }}
                <span style="color:{{ $company->price->regularMarketChange->fmt>0?'green':'red' }}">
                                            {{ "$ ". $company->price->regularMarketChange->fmt }}
            </span>
                <span style="color:{{ $company->price->regularMarketChangePercent->fmt > 0?'green':'red' }};">
                  ({{ $company->price->regularMarketChangePercent->fmt }})
                  <i class="fa fa-arrow-up"
                     style="color:{{ $company->price->regularMarketChangePercent->fmt > 0?'green':'red' }};"></i>
            </span>
            </div>
        </div>
        <div class="card mt-4">
            <div class="card-body">
                {{ $company->price->exchangeName }} {{ date('H:i:s', $company->price->regularMarketTime) }}
                {{ $company->price->exchangeDataDelayedBy?'Delayed':null }}
            </div>
        </div>
    </div>
</div>
