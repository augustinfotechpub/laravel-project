<div>
    {{-- Brief intro part --}}
    <div class="row mt-md-4">
    {{--@if($initCharting)
        <div class="col-md-12">
            <div class="row">
                --}}{{--Dividends and Shares--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <h5 class="mb-0">Dividends and Shares</h5>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                @foreach($this->charts['dividendsAndShares'] as $title=>$value)
                                    <tr>
                                        <td>{{ $title }}</td>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Dividends and Shares--}}{{--

                --}}{{--Estimates--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="mb-0">Estimates</h5><small>amount in millions</small>
                                </div>
                                <div class="col-md-6 text-end">
                                    <a class="btn btn-{{ ($charts['estimates']['period'] == 'quarter')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('quarter')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Quarterly
                                    </a>
                                    <button class="btn btn-secondary btn-sm py-0 px-1" wire:loading
                                            wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                        Loading...
                                    </button>
                                    <a class="btn btn-{{ ($charts['estimates']['period'] == 'FY')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('FY')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Annual
                                    </a>
                                    --}}{{--<a class="btn btn-{{ ($charts['estimates']['period'] == 'ttm')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('ttm')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        TTM
                                    </a>--}}{{--
                                </div>
                            </div>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                @if ($charts['estimates']['header'])
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        @foreach($charts['estimates']['dates'] as $date)
                                            <th>{{ $date }}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @endif
                                    @foreach($this->charts['estimates']['items'] as $items)
                                        <tr>
                                            @foreach($items as $item)
                                                @if ($loop->first)
                                                    <td><b>{{ $item }}</b></td>
                                                @else
                                                    <td>{!! $item !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Estimates--}}{{--

                --}}{{--Income Statement--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h5 class="mb-0">Income Statement</h5>
                                    <small>amount in millions</small>
                                </div>
                                <div class="col-md-6 text-end">
                                    <a class="btn btn-{{ ($charts['incomeStatement']['period'] == 'quarter')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('quarter')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Quarterly
                                    </a>
                                    <button class="btn btn-secondary btn-sm py-0 px-1" wire:loading
                                            wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                        Loading...
                                    </button>
                                    <a class="btn btn-{{ ($charts['incomeStatement']['period'] == 'FY')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('FY')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Annual
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                <thead>
                                <tr>
                                    @foreach($charts['balanceSheet']['thead_prepend'] as $thead)
                                        <th>{{ $thead }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($this->charts['incomeStatement']['items'] as $items)
                                    <tr>
                                        @foreach($items as $item)
                                            @if ($loop->first)
                                                <td><b>{{ $item }}</b></td>
                                            @else
                                                <td>{!! $item !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> --}}{{--End Income Statement--}}{{--

                --}}{{--Balance Sheet--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <h5 class="mb-0">Balance Sheet</h5>
                                    <small>amount in millions</small>
                                </div>
                                <div class="col-md-6 text-end">
                                    <a class="btn btn-{{ ($charts['balanceSheet']['period'] == 'quarter')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('quarter')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Quarterly
                                    </a>
                                    <button class="btn btn-secondary btn-sm py-0 px-1" wire:loading
                                            wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                        Loading...
                                    </button>
                                    <a class="btn btn-{{ ($charts['balanceSheet']['period'] == 'FY')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('FY')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Annual
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                <thead>
                                <tr>
                                    @foreach($charts['balanceSheet']['thead_prepend'] as $thead)
                                        <th>{{ $thead }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($this->charts['balanceSheet']['items'] as $items)
                                    <tr>
                                        @foreach($items as $item)
                                            @if ($loop->first)
                                                <td><b>{{ $item }}</b></td>
                                            @else
                                                <td>{!! $item !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> --}}{{--End Balance Sheet--}}{{--

                --}}{{--Cash Flow Statement--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-7">
                                    <h5 class="mb-0">Cash Flow Statement</h5>
                                    <small>amount in millions</small>
                                </div>
                                <div class="col-md-5 text-end">
                                    <a class="btn btn-{{ ($charts['cashFlowStatement']['period'] == 'quarter')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('quarter')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Quarterly
                                    </a>
                                    <button class="btn btn-secondary btn-sm py-0 px-1" wire:loading
                                            wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                        Loading...
                                    </button>
                                    <a class="btn btn-{{ ($charts['cashFlowStatement']['period'] == 'FY')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('FY')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Annual
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                <thead>
                                <tr>
                                    @foreach($charts['balanceSheet']['thead_prepend'] as $thead)
                                        <th>{{ $thead }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($this->charts['cashFlowStatement']['items'] as $items)
                                    <tr>
                                        @foreach($items as $item)
                                            @if ($loop->first)
                                                <td><b>{{ $item }}</b></td>
                                            @else
                                                <td>{!! $item !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Cash Flow Statement--}}{{--

                --}}{{--Current Valuation--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="mb-0">Current Valuation</h5>
                                    <small>amount in millions</small>
                                </div>
                                <div class="col-md-6 text-end">
                                    <a class="btn btn-{{ ($charts['currentValuation']['period'] == 'quarter')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('quarter')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Quarterly
                                    </a>
                                    <button class="btn btn-secondary btn-sm py-0 px-1" wire:loading
                                            wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                        Loading...
                                    </button>
                                    <a class="btn btn-{{ ($charts['currentValuation']['period'] == 'FY')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('FY')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Annual
                                    </a>
                                    --}}{{--<a class="btn btn-{{ ($charts['currentValuation']['period'] == 'ttm')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('ttm')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        TTM
                                    </a>--}}{{--
                                </div>
                            </div>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                @if ($charts['currentValuation']['header'])
                                    <thead>
                                    <tr>
                                        @foreach($charts['balanceSheet']['thead_prepend'] as $thead)
                                            <th>{{ $thead }}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @endif
                                    @foreach($this->charts['currentValuation']['items'] as $items)
                                        <tr>
                                            @foreach($items as $item)
                                                @if ($loop->first)
                                                    <td><b>{{ $item }}</b></td>
                                                @else
                                                    <td>{!! $item !!}</td>
                                                @endif
                                            @endforeach
                                        </tr>
                                    @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Current Valuation--}}{{--

                --}}{{--Liquidity And Solvency--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="mb-0">Liquidity And Solvency</h5>
                                    <small>amount in millions</small>
                                </div>
                                <div class="col-md-6 text-end">
                                    <a class="btn btn-{{ ($charts['liquidityAndSolvency']['period'] == 'quarter')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('quarter')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Quarterly
                                    </a>
                                    <button class="btn btn-secondary btn-sm py-0 px-1" wire:loading
                                            wire:target="changePeriod">
                                        <span class="spinner-grow spinner-grow-sm" role="status"
                                              aria-hidden="true"></span>
                                        Loading...
                                    </button>
                                    <a class="btn btn-{{ ($charts['liquidityAndSolvency']['period'] == 'FY')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('FY')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        Annual
                                    </a>
                                    --}}{{--<a class="btn btn-{{ ($charts['liquidityAndSolvency']['period'] == 'ttm')?'primary disabled':'secondary' }} btn-sm py-0 px-1"
                                       wire:click="changePeriod('ttm')"
                                       wire:loading.remove="changePeriod" style="font-size: 13px;">
                                        TTM
                                    </a>--}}{{--
                                </div>
                            </div>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                @if ($charts['liquidityAndSolvency']['header'])
                                    <thead>
                                    <tr>
                                        @foreach($charts['balanceSheet']['thead_prepend'] as $thead)
                                            <th>{{ $thead }}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @endif
                                    @foreach($this->charts['liquidityAndSolvency']['items'] as $items)
                                        <tr>
                                            @foreach($items as $item)
                                                @if ($loop->first)
                                                    <td><b>{{ $item }}</b></td>
                                                @else
                                                    <td>{!! $item !!}</td>
                                                @endif
                                            @endforeach
                                            <td>
                                                <button class="btn btn-info btn-sm py-0">
                                                    <i class="fa fa-chart-line text-white"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Liquidity And Solvency--}}{{--

                --}}{{--Employee Count Metrics--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <h5 class="mb-0">Employee Count Metrics</h5>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                @foreach($this->charts['employeeCountMetrics'] as $title=>$value)
                                    <tr>
                                        <td>{{ $title }}</td>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Employee Count Metrics--}}{{--
                --}}{{--Advanced Metrics--}}{{--
                <div class="col-md-4 mt-md-4 mt-3">
                    <div class="card shadow">
                        <div class="card-header">
                            <h5 class="mb-0">Advanced Metrics</h5>
                        </div>
                        <div class="card-body scrollable-card-body p-0" style="height:204px;">
                            <table class="table table-sort table-hover table-sort mb-0">
                                @foreach($this->charts['advancedMetrics'] as $title=>$value)
                                    <tr>
                                        <td>{{ $title }}</td>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                --}}{{--End Advanced Metrics--}}{{--
            </div>
        </div>
    @else
        @include('front.layouts.partials.loader_b')
    @endif--}}
    <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div id="tradingview_3c9bc"></div>
            <div class="tradingview-widget-copyright">
                <a href="https://in.tradingview.com/symbols/NASDAQ-{{$this->symbol}}/"
                   rel="noopener" target="_blank"><span class="blue-text">{{$this->symbol}} Chart</span></a>
            </div>
            <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
            <script type="text/javascript">
                new TradingView.widget(
                    {
                        "width": screen.width - 40,
                        "height": screen.height - 400,
                        "symbol": "{{ getProfileSymbol() }}",
                        "timezone": "Etc/UTC",
                        "theme": "light",
                        "style": "1",
                        "locale": "in",
                        "toolbar_bg": "#f1f3f6",
                        "enable_publishing": false,
                        "withdateranges": true,
                        "range": "YTD",
                        "hide_side_toolbar": false,
                        "allow_symbol_change": true,
                        "details": true,
                        "hotlist": true,
                        "calendar": true,
                        "container_id": "tradingview_3c9bc"
                    }
                );
            </script>
        </div>
        <!-- TradingView Widget END -->
    </div>
    @include('front.layouts.partials.chart-modal')
</div>
