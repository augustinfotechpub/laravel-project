<div wire:init="initComponent">
    <div class="mb-4">
        <select class="form-select" aria-label="Default select example" wire:model="newSymbol">
            <option value="AMZN">AMZN</option>
            <option value="FB">FB</option>
            <option value="GOOG">GOOG</option>
            <option value="TSLA">TSLA</option>
            <option value="NFLX">NFLX</option>
        </select>
    </div>
    <div class="row mb-4 d-none">
        <div class="col-md-12 btn-group">
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == '1min'?'active':null }}"
                    wire:click="changePeriod('1min')">1min
            </button>
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == '5min'?'active':null }}"
                    wire:click="changePeriod('5min')">5min
            </button>
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == '15min'?'active':null }}"
                    wire:click="changePeriod('15min')">15min
            </button>
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == '30min'?'active':null }}"
                    wire:click="changePeriod('30min')">30min
            </button>
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == '1hour'?'active':null }}"
                    wire:click="changePeriod('1hour')">1hour
            </button>
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == '4hour'?'active':null }}"
                    wire:click="changePeriod('4hour')">4hour
            </button>
            <button class="btn btn-info btn-sm px-2 py-1 text-white {{ $is_active == 'daily'?'active':null }}"
                    wire:click="changePeriod('daily')">Daily
            </button>
        </div>
    </div>
    <div class="chart-btns-row">
        <div class="dropdown chart-dropdown">
            <a href="#" class="chart-dropdown-btn" id="chartDrop1" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="ri-add-box-line"></i>
                Indicators
            </a>
            <div class="dropdown-menu" aria-labelledby="chartDrop1">
                <form action="#" class="form search-form">
                    <div class="input-group one-line-group">
                        <button class="btn btn-secondary" type="button" id="button-addon1"><i class="ri-search-line"></i></button>
                        <input type="text" class="form-control" id="searchIndicators" placeholder="Search indicators">
                    </div>
                </form>
                <div class="scrollable-card-body">
                    <h5 class="dropdown-title">Commonly used</h5>
                    <a href="#" class="dropdown-link">Moving Average</a>
                    <a href="#" class="dropdown-link">Moving Average Envelope</a>
                    <a href="#" class="dropdown-link">Moving Average Deviation</a>
                    <a href="#" class="dropdown-link">Bollinger Bands</a>
                    <a href="#" class="dropdown-link">RSI</a>
                    <a href="#" class="dropdown-link">MACD</a>

                    <h5 class="dropdown-title">All Indicators</h5>
                    <a href="#" class="dropdown-link">Accumulation/Distribution</a>
                </div>
            </div>
        </div>
        <!--/Indicators DropDown-->
        <div class="dropdown chart-dropdown">
            <a href="#" class="chart-dropdown-btn" id="chartDrop1" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="ri-add-box-line"></i>
                Comparison
            </a>
            <div class="dropdown-menu" aria-labelledby="chartDrop1">
                <form action="#" class="form search-form">
                    <div class="input-group one-line-group">
                        <button class="btn btn-secondary" type="button" id="button-addon1"><i class="ri-search-line"></i></button>
                        <input type="text" class="form-control" id="searchIndicators" placeholder="Search indicators">
                    </div>
                </form>
                <div class="symbols-drop-btns">
                    <a href="#"><i class="ri-add-circle-line"></i>DOW</a>
                    <a href="#"><i class="ri-add-circle-line"></i>NASDAQ</a>
                    <a href="#"><i class="ri-add-circle-line"></i>S&P 500</a>
                </div>
                <h5 class="dropdown-title mb-0">Recently viewed symbols</h5>
                <div class="scrollable-card-body">
                    <div class="row mx-auto g-0 view-symbols-row">
                        <h6 class="col-12">AAPL</h6>
                        <div class="col-8"><p>Apple Inc.</p></div>
                        <div class="col-4"><p>Equity-NasdaqGS</p></div>
                    </div>
                    <div class="row mx-auto g-0 view-symbols-row">
                        <h6 class="col-12">ES=F</h6>
                        <div class="col-8"><p>Apple Inc.</p></div>
                        <div class="col-4"><p>Future-CME</p></div>
                    </div>
                </div>
            </div>
        </div>
        <!--/Comparison DropDown -->
        <div class="dropdown chart-dropdown datepicker-dropdown">
            <a href="#" class="chart-dropdown-btn" id="chartDrop1" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="ri-calendar-line"></i>
                Date Range
            </a>
            <div class="dropdown-menu" aria-labelledby="chartDrop1">
                <form action="#" class="form datepicker-form">
                    <div class="row g-2">
                        <div class="col-sm-6">
                            <div class="input-group one-line-group">
                                <span class="input-group-text">From</span>
                                <input type="date" class="form-control" id="searchIndicators" placeholder="01/01/2020">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group one-line-group">
                                <span class="input-group-text">To</span>
                                <input type="date" class="form-control" id="searchIndicators" placeholder="31/12/2020">
                            </div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <button type="button" class="btn btn-sm btn-primary fw-bold py-1 px-4">Apply</button>
                            <button type="button" class="btn btn-sm btn-outline-primary fw-bold py-1 px-4 ms-3">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--/Date Range DropDown-->
        <div class="dropdown chart-dropdown">
            <a href="#" class="chart-dropdown-btn" id="chartDrop1" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="ri-ruler-line"></i>
                Interval
                <span class="small d-flex align-items-center text-muted ms-2">
                30 mins
                <i class="ri-arrow-down-s-line me-0"></i>
            </span>
            </a>

            <div class="dropdown-menu" aria-labelledby="chartDrop1" style="min-width:auto;">
                <div class="scrollable-card-body">
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('1mins')">1 mins</a>
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('5mins')">5 mins</a>
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('15mins')">15 mins</a>
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('30mins')">30 mins</a>
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('1hour')">1 hour</a>
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('4hour')">4 hour</a>
                    <a href="#" class="dropdown-link {{ $is_active == '1min'?'active':null }}" wire:click="changePeriod('daily')">daily</a>
                </div>
            </div>
        </div>
        <!--/Indicators DropDown-->
    </div>
    <hr class="mt-1">
    <div class="row">
        <div wire:loading.remove>
            <div id="fundamental-chart" class="position-relative" style="z-index: 101;"></div>
        </div>
        <div wire:loading>
            @include('front.layouts.partials.loader')
        </div>
    </div>
</div>
