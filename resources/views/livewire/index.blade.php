<div xmlns:livewire="">
    <main id="main">
        <section class="inner-page p-0">
            <livewire:partials.navbar/>
            <livewire:stock-ticker/>
            @if($content_component != 'library')
            <livewire:stock-summary :lbcontent="$content_component"/>
            @else
            <livewire:stock-summary-library/>
            @endif
            <div class="container-fluid">
                @switch($content_component)
                    @case('app-main')
                    <livewire:app-main/>
                    @break
                    @case('markets')
                    <livewire:markets/>
                    @break
                    @case('stock-screener')
                    <livewire:stock-screener.stock-screener/>
                    @break
                    @case('key-parties')
                    @php($key = \Illuminate\Support\Str::random(32))
                    <livewire:key-parties.key-parties :symbol="$symbol" :key="$key"/>
                    @break
                    @case('financials')
                    <livewire:financials.financials/>
                    @break
                    @case('charting')
                    <livewire:charting :symbol="$symbol"/>
                    @break
                    @case('ratios')
                    <livewire:ratios :symbol="$symbol"/>
                    @break
                    @case('financial-models')
                    <livewire:financial-models :symbol="$symbol"/>
                    @break
                    @case('sec-fillings')
                    <livewire:sec-fillings/>
                    @break
                    @case('network')
                    <livewire:network :symbol="$symbol"/>
                    @break
                    @case('calender')
                    <livewire:calender :symbol="$symbol"/>
                    @break
                @endswitch
            </div>
        </section>
    </main>
    <livewire:notification/>
</div>
