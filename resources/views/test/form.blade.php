<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <button class="btn btn-success addTemplate">Add More</button>
    <div class="definitionItems">
    </div>
</div>
@include('test.add-item')

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var item = $('.template').clone().last();
        item.removeClass('d-none');
        item.appendTo('.definitionItems');

        var counter = 1
        $(document).on('click', '.addTemplate', function () {
            var item = item = $('.template').clone().last();
            item.removeClass('d-none');
            item.find('.tag').attr('name', 'tag[' + counter + '][]');
            item.find('.sic').attr('name', 'sic[' + counter + '][]');
            item.find('.symbol').attr('name', 'symbol[' + counter + '][]');
            item.appendTo('.definitionItems');
            counter++;
        })
        $(document).on('click', '.removeTemplate', function () {
            var len = $('.template').length;
            if (len > 2) {
                $(this).closest('.template').remove();
            }
        });


        $(document).on('click', '.addTag', function () {
            var item = $(this).parent('div').clone();
            item.find('.addTag').addClass('d-none');
            item.find('.removeTag').removeClass('d-none');
            var row = $(this).parent('div').parent('div').parent('div').find('.rowTag');
            item.appendTo(row);
        });
        $(document).on('click', '.removeTag', function () {
            $(this).parent('div').remove();
        });


        $(document).on('click', '.addSic', function () {
            var item = $(this).parent('div').clone();
            item.find('.addSic').addClass('d-none');
            item.find('.removeSic').removeClass('d-none');
            var row = $(this).parent('div').parent('div').parent('div').find('.rowSic');
            item.appendTo(row);
        });
        $(document).on('click', '.removeSic', function () {
            $(this).parent('div').remove();
        });

        $(document).on('click', '.addSymbol', function () {
            var item = $(this).parent('div').clone();
            item.find('.addSymbol').addClass('d-none');
            item.find('.removeSymbol').removeClass('d-none');
            var row = $(this).parent('div').parent('div').parent('div').find('.rowSymbol');
            item.appendTo(row);
        });
        $(document).on('click', '.removeSymbol', function () {
            $(this).parent('div').remove();
        });


    });
</script>
</body>
</html>
