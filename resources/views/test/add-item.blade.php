<div class="d-none template">
    <div class="row definition-item-row border-top my-2">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12 mt-3">
                    <label>Tags</label>
                    <div class="input-group-sm input-group my-1">
                        <input type="text" class="form-control tag" name="tag[0][]">
                        <button class="btn btn-sm btn-success float-right addTag" type="button">Add</button>
                        <button class="btn btn-sm btn-danger float-right removeTag d-none" type="button">Remove</button>
                    </div>
                </div>
                <div class="col-md-12 rowTag">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <label>Sics</label>
                    <div class="input-group-sm input-group my-1">
                        <input type="text" class="form-control sic" name="sic[0][]">
                        <button class="btn btn-sm btn-success float-right addSic" type="button">Add</button>
                        <button class="btn btn-sm btn-danger float-right removeSic d-none" type="button">Remove</button>
                    </div>
                </div>
                <div class="col-md-12 rowSic">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <label>Symbols</label>
                    <div class="input-group-sm input-group my-1">
                        <input type="text" class="form-control symbol" name="symbol[0][]">
                        <button class="btn btn-sm btn-success float-right addSymbol" type="button">Add</button>
                        <button class="btn btn-sm btn-danger float-right removeSymbol d-none" type="button">Remove</button>
                    </div>
                </div>
                <div class="col-md-12 rowSymbol">

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-sm btn-danger float-right mt-3 text-white removeTemplate">Remove</a>
                </div>
                <div class="col-md-12 form-group">
                    <label for="content[]">Content</label>
                    <textarea name="content[]" class="form-control" id="content[]" cols="70" rows="5"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>


{{--<div id="item-tag-template" class="d-none">
    <div class="input-group-sm input-group my-1 item-tag">
        <input type="text" class="form-control" placeholder="Add Tags"
               aria-label="Tags" aria-describedby="basic-addon2">
        <div class="input-group-sm input-group-append">
            <button class="btn btn-danger remove-tag" type="button">Remove</button>
        </div>
    </div>
</div>
<div id="item-sic-template" class="d-none">
    <div class="input-group-sm input-group my-1 item-sic">
        <input type="text" class="form-control" placeholder="Add Sic"
               aria-label="Sic" aria-describedby="basic-addon2">
        <div class="input-group-sm input-group-append">
            <button class="btn btn-danger remove-sic" type="button">Remove</button>
        </div>
    </div>
</div>
<div id="item-symbol-template" class="d-none">
    <div class="input-group-sm input-group my-1 item-symbol">
        <input type="text" class="form-control" placeholder="Add Symbol"
               aria-label="Symbol" aria-describedby="basic-addon2">
        <div class="input-group-sm input-group-append">
            <button class="btn btn-danger remove-symbol" type="button">Remove</button>
        </div>
    </div>
</div>--}}
