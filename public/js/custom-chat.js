
chatPeopleBodyEle = $("#chatPeopleBody");
window.prepareContacts = function (contact) {

  console.log(contact);

    console.log('check complete');
    var contactDetail = contact.is_group ? contact.group : contact.user;
    var contactId = contact.is_group ? contact.group_id : contact.user_id;
    var showStatus = true;
    var showUserStatus = false;

    if (contact.is_group && contactDetail.removed_from_group) {
      contactDetail = contact.group_details;
    }

    var template = jQuery('#tmplConversationsList');
    /*var helpers = {
      getMessageByItsTypeForChatList: getMessageByItsTypeForChatList,
      getLocalDate: getLocalDate,
      checkUserStatus: checkUserStatus,
      checkForMyContact: checkForMyContact,
      getDraftMessage: getDraftMessage
    };
    var data = {
      showStatus: showStatus,
      showUserStatus: showUserStatus,
      contactId: contactId,
      contact: contact,
      contactDetail: contactDetail,
      is_online: !contact.is_group ? contact.user.is_online : 0
    };*/
    // var contactElementHtml = template.render(data, helpers);

    if (contact.unread_count > 0) {
      // totalUnreadConversations += 1;
      // updateUnreadMessageCount(0);
    }

    // return contactElementHtml;
  };

$.ajax({
  type: 'GET',
  url: groupsList,
  success: function success(result) {
    if (result.success) {
      $.each(result.data, function (i, group) {
        // listenForGroupUpdates(group.id);
      });
    }
  },
  error: function error(_error) {
    console.log(_error);
  }
});
$.ajax({
  type: 'GET',
  url: conversationListURL,
  success: function success(data) {
    if (data.success) {
      $('#infyLoader').hide();
      var latestConversations = data.data.conversations;

      if (latestConversations.length === 0) {
        // noConversationYetEle.show();
        noConversationYet = true;
      }

      chatPeopleBodyEle.append(latestConversations.map(prepareContacts).join(''));
      // searchUsers();
      // loadTooltip();

      if (data.data.conversations.length > 9) {
        var $loadMoreBtn = $('#loadMoreConversationBtn').clone();
        $('#chatPeopleBody').append($loadMoreBtn);
        $loadMoreBtn.addClass('active');
        $loadMoreBtn.show();
      }
    }
  },
  error: function error(_error2) {
    console.log(_error2);
  }
}); //GET Archive conversations list
