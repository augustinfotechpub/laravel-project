var balanceSheetChartId = $('#balance-sheet-chart');
var cashFlowChartId = $('#cash-flow-chart');

var areaSeriesForIS;
var dataForIS;

var areaSeriesForBS;
var dataForBS;

var areaSeriesForCF;
var dataForCF;
var counter = 0;

// For Charting Chart
var areaSeriesForChartingChart;
var dataForChartingChart;


// For Active Industry Chart
var activeIndustryChart;
var activeIndustryChartData;

window.addEventListener('initBSTooltip', event => {
    initTooltip();
});

document.addEventListener('livewire:load', function () {
    // Your JS here.
    initTooltip();
})

function initTooltip() {
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });
}

$(document).ready(function () {
    // For Charting Chart
    $(document).on('click', '.switcher-item-for-charting-chart', function () {
        areaSeriesForChartingChart.setData(dataForChartingChart[$(this).data('chart-id')]);
    });

    $(document).on('click', '.switcher-item-for-is', function () {
        areaSeriesForIS.setData(dataForIS.dataSerieses[$(this).data('key')]);
    });

    $(document).on('click', '.switcher-item-for-bs', function () {
        areaSeriesForBS.setData(dataForBS.dataSerieses[$(this).data('key')]);
    });

    $(document).on('click', '.switcher-item-for-cf', function () {
        areaSeriesForCF.setData(dataForCF.dataSerieses[$(this).data('key')]);
    });

    $(document).on('click', '.switcher', function (e) {
        $('.switcher').removeClass('highlighted');
        $(this).addClass('highlighted');
    });
});


// For Charting
window.addEventListener('updateFormADBGColor', event => {
    $('#fillingDetailModal').find('tr').each(function () {
        $(this).find('td').each(function (i, item) {
            if (i === 6 || i === 5) {
                if ($(item).text() === 'A') {
                    $(this).closest('tr').css('background-color', '#c1eac1');
                }
                if ($(item).text() === 'D') {
                    $(this).closest('tr').css('background-color', '#f5baba');
                }
            }
        });
    });
});

window.addEventListener('activeIndustryChart', event => {
    activeIndustryChartData = event.detail;
    initActiveIndustryChart();
    // window.activeIndustryChart.update();
});

window.addEventListener('chartingChartEvent', event => {
    dataForChartingChart = event.detail;
    var container = document.getElementById('chartModalBody');
    $('#dataForChartingChart').empty();
    var chart = initChart(container);
    areaSeriesForChartingChart = addAreaSeries(chart);
    var flag = true;
    for (var xyz in dataForChartingChart) {
        if (flag) {
            areaSeriesForChartingChart.setData(dataForChartingChart[xyz]);
            flag = false;
        }
    }
});

window.addEventListener('incomeStatementChart', event => {
    dataForIS = event.detail.data;
    var container = document.getElementById('income-statement-chart');
    $('#income-statement-chart').empty();
    var chart = initChart(container);
    areaSeriesForIS = addAreaSeries(chart);
    areaSeriesForIS.setData(dataForIS.dataSerieses['costOfRevenue']);
});

window.addEventListener('balanceSheetChart', event => {
    console.log(event.detail);
    dataForBS = event.detail.data;
    var container = document.getElementById('balance-sheet-chart');
    $('#balance-sheet-chart').empty();
    var chart = initChart(container);
    areaSeriesForBS = addAreaSeries(chart);
    areaSeriesForBS.setData(dataForBS.dataSerieses['accountPayables']);
});

window.addEventListener('initTradingViewChart', event => {
    var profile = event.detail.profile;
    new TradingView.widget(
        {
            "autosize": true,
            "symbol": profile.exchangeShortName + ':' + profile.symbol,
            "interval": "D",
            "timezone": "Etc/UTC",
            "theme": "light",
            "style": "3",
            "locale": "en",
            "toolbar_bg": "#f1f3f6",
            "enable_publishing": false,
            "withdateranges": true,
            "allow_symbol_change": true,
            "container_id": "tradingview_34970"
        }
    );
});

window.addEventListener('cashFlowChart', event => {
    dataForCF = event.detail.data;
    console.log("Cash Flow Stat: ");
    console.log(dataForCF);
    var container = document.getElementById('cash-flow-chart');
    $('#cash-flow-chart').empty();
    var chart = initChart(container);
    areaSeriesForCF = addAreaSeries(chart);
    areaSeriesForCF.setData(dataForCF.dataSerieses['accountsPayables']);
});

window.addEventListener('incomeStatementChartLoaded', event => {
    $(event.detail).each(function (index, data) {
        new Chart($('#incomeStatementChart' + index), {
            type: 'bar',
            data: {
                labels: data.years,
                datasets: [{
                    data: data.datasets
                }]
            },
            options: {
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: false //this will remove only the label
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            display: false //this will remove only the label
                        }
                    }],
                }
            }
        });
    });
});

function addAreaSeries(chart) {
    return chart.addAreaSeries({
        topColor: 'rgba(0, 150, 136, 0.56)',
        bottomColor: 'rgba(0, 150, 136, 0.04)',
        lineColor: 'rgba(0, 150, 136, 1)',
        lineWidth: 2,
    });
}

function initChart(container) {
    const width = container.offsetWidth - 20;
    var height = width / 2;
    height = height > 300 ? 300 : height;
    return LightweightCharts.createChart(container, {
        width: width,
        height: height,
        rightPriceScale: {
            scaleMargins: {
                top: 0.2,
                bottom: 0.2,
            },
            borderVisible: false,
        },
        timeScale: {
            borderVisible: false,
        },
        layout: {
            backgroundColor: '#ffffff',
            textColor: '#333',
        },
        grid: {
            horzLines: {
                color: '#eee',
            },
            vertLines: {
                color: '#ffffff',
            },
        },
    });
}

function businessDayToString(businessDay) {
    return businessDay.year + '-' + businessDay.month + '-' + businessDay.day;
}

function initActiveIndustryChart() {
    $('#activeIndustryChartDiv').html('<canvas id="activeIndustryChart" style="height: 400px;"></canvas>')
    var ctx = document.getElementById('activeIndustryChart').getContext('2d');
    activeIndustryChart = new Chart(ctx, {
        type: 'line',
        data: activeIndustryChartData,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

Livewire.onLoad(() => {
    /*$('[data-bs-toggle="tooltip"]').tooltip()*/
});

document.addEventListener('livewire:load', function () {
    $('.flip').click(function () {
        $(this)
            /*.closest('tbody')*/
            .next('tbody')
            .toggle('fast');
    });
});

window.addEventListener('personOwnedAmtChart', event => {
    var canvas = document.getElementById('personOwnedAmtChart');
    var ctx = canvas.getContext("2d");
    fitToContainer(canvas);
    var config = {
        type: 'pie',
        options: {
            maintainAspectRatio: true,
            legend: {
                display: true,
                position: 'left',
            },
        }
    };
    var chart = new Chart(ctx, config);

    console.log(event.detail);
    config.data = {
        datasets: [{
            data: event.detail.dataset.data,
            backgroundColor: event.detail.dataset.backgroundColor,
        }],
        labels: event.detail.labels
    };
    chart.update();
});

window.addEventListener('profile', event => {
    console.log('Investigating Profile::', event.detail);
});

window.addEventListener('personTransactionsChart', event => {
    console.log(event.detail);
    /*var options = {
        series: [{
            name: event.detail.name,
            data: event.detail.data
        }],
        chart: {
            type: 'bar',
            height: 350,
            zoom: {
                type: 'x',
                enabled: true,
                autoScaleYaxis: true
            },
        },
        plotOptions: {
            bar: {
                horizontal: false,
                /!*columnWidth: '55%',*!/
                /!*endingShape: 'rounded'*!/
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            /!*width: 2,*!/
            colors: ['transparent']
        },
        xaxis: {
            categories: event.detail.labels,
        },
        yaxis: {
            title: {
                text: "Securities owned"
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val + " shares"
                }
            }
        }
    };*/

    var options = {
        series: [{
            type: 'area',
            name: event.detail.name,
            data: event.detail.data
        }],
        chart: {
            height: 300,
            type: 'line',
        },
        stroke: {
            curve: 'smooth',
            width: 1,
        },
        fill: {
            type: 'solid',
            opacity: [0.35, 1],
        },
        labels: event.detail.labels,
        markers: {
            size: 0
        },
        yaxis: [
            {
                title: {
                    text: 'Securities owned',
                },
            }
        ],
        tooltip: {
            shared: true,
            intersect: false,
            y: {
                formatter: function (y) {
                    if (typeof y !== "undefined") {
                        return y.toFixed(0) + " shares";
                    }
                    return y;
                }
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#personTransactionsChart"), options);
    chart.render();
});

window.addEventListener('segInfoAndGeoCharts', event => {
    console.log(event.detail);
    var data = event.detail;
    var options = {
        series: data.chartData.series,
        chart: {
            width: 380,
            type: 'pie',
        },
        labels: data.chartData.labels,
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                },
                plotOptions: {
                    pie: {
                        size: 200
                    }
                }
            }
        }]
    };
    new ApexCharts(document.querySelector("#segInfoAndGeoChart"), options).render();
});

window.addEventListener('productsAndServices', event => {
    console.log(event.detail);
    try {
        $(event.detail).each(function (key, data) {
            var options = {
                series: data.chartData.series,
                chart: {
                    width: 380,
                    type: 'pie',
                },
                labels: data.chartData.labels,
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                }]
            };
            new ApexCharts(document.querySelector("#productsAndServices" + key), options).render();
        });
    } catch (e) {

    }
});

window.addEventListener('personHoldingsChart', event => {
    $('#chsw').removeClass('d-none');
    var canvas = document.getElementById('personHoldingsChart');
    var ctx = canvas.getContext("2d");
    fitToContainer(canvas);
    var config = {
        type: 'pie',
        options: {
            maintainAspectRatio: true,
            legend: {
                display: true,
                position: 'left',
            },
        }
    };
    var chart = new Chart(ctx, config);

    console.log(event.detail);
    config.data = {
        datasets: [{
            data: event.detail.dataset.data,
            backgroundColor: event.detail.dataset.backgroundColor,
        }],
        labels: event.detail.labels
    };
    chart.update();
});

function fitToContainer(canvas) {
    canvas.style.width = '100%';
    canvas.style.height = 300; // '100%';
    canvas.width = canvas.offsetWidth;
    canvas.height = 300; // canvas.offsetHeight;
}

window.addEventListener('alert', event => {
    console.log(event.detail);
    toastr[event.detail.type](event.detail.message,
        event.detail.title ?? '');
    toastr.options = {
        "closeButton": true,
        "progressBar": true,
    }
});

window.addEventListener('alertTooltip', event => {
    console.log(event.detail);
    let alert = event.detail;
    tippy('#' + alert.id, {
        content: alert.content,
        allowHTML: true,
    });
});

document.addEventListener("DOMContentLoaded", () => {
    Livewire.hook('component.initialized', (component) => {
        getStockDetails();
        /*$(".tippy").each(function (index) {
            let id = $(this).attr('id');
            let identifier = id.slice(0, -11);
            let selector = '#' + id;
            getTippyAlert(selector, identifier);
        });*/
    });
});
