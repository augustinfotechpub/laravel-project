$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {

    if (jQuery('input[name="_token"]').length > 0) {
        function refreshToken() {
            $.get('/refresh-csrf').done(function (data) {
                jQuery('input[name="_token"]').val(data);
            });
        }
        setInterval(refreshToken, 180000); // 1 hour 
    }

    /*// ********** Facebook *********
    window.fbAsyncInit = function () {
        FB.init({
            appId: '401457531104854',
            xfbml: true,
            version: 'v9.0'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));*/
    // *********** End Facebook Js *********
});

function openContent(evt, tabContentId) {
    var i, tabContent, tabLinks;
    tabContent = document.getElementsByClassName("tabContent");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("tabLinks");
    for (i = 0; i < tabLinks.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace(" active", "");
    }
    document.getElementById(tabContentId).style.display = "block";
    evt.currentTarget.className += " active";
}


getStockDetails();

$(document).ready(function () {
    // setInterval(function () {
    getStockDetails();
    // }, 10000);

    /*$('#alertCarousel').on('slide.bs.carousel', function() {
        if( jQuery(".item.active").is(":last-child") ){
            jQuery("#alertCarousel").find(".left.carousel-control").addClass("disabled");
        } else {
            jQuery("#alertCarousel").find(".left.carousel-control").removeClass("disabled");
        }

        if( jQuery(".item.active").is(":first-child") ){
            jQuery("#alertCarousel").find(".right.carousel-control").addClass("disabled");
        } else {
            jQuery("#alertCarousel").find(".right.carousel-control").removeClass("disabled");
        }
    });*/


    $('#main').on('hover', '.has-tooltip', function () {
        if (jQuery(this).find(".carousel-inner .item.active").is(":last-child")) {
            jQuery(this).find(".right.carousel-control").addClass("disabled");
            jQuery(this).find(".left.carousel-control").removeClass("disabled");
        } else if (jQuery(this).find(".carousel-inner .item.active").is(":first-child")) {
            jQuery(this).find(".left.carousel-control").addClass("disabled");
            jQuery(this).find(".right.carousel-control").removeClass("disabled");
        } else {
            jQuery(this).find(".right.carousel-control").removeClass("disabled");
            jQuery(this).find(".left.carousel-control").removeClass("disabled");
        }
    });

    $('#main').on('slid.bs.carousel', '.carousel', function () {
        if (jQuery(this).find(".carousel-inner .item.active").is(":last-child")) {
            jQuery(this).find(".right.carousel-control").addClass("disabled");
            jQuery(this).find(".left.carousel-control").removeClass("disabled");
        } else if (jQuery(this).find(".carousel-inner .item.active").is(":first-child")) {
            jQuery(this).find(".left.carousel-control").addClass("disabled");
            jQuery(this).find(".right.carousel-control").removeClass("disabled");
        } else {
            jQuery(this).find(".right.carousel-control").removeClass("disabled");
            jQuery(this).find(".left.carousel-control").removeClass("disabled");
        }
    });

});

function getStockDetails() {
    fetch($app_url + '/app/stock-details') // Any output from the script will go to the "result" div
        .then(response => response.json())
        .then(response => {
            if (response.status === 200) {
                // console.log(response);
                $('#stock-details-3524').html(response.data.view);
                tippy('#delayed-tooltip', {
                    content: "Quotes are delayed by 15 min"
                });
            } else if (response.status === 100) {
                // console.log(response);
            }
        })
        .catch(error => {

        });
}

$(document).ready(function () {
    $(document).on('hover', '.tippy', function () {
        let id = $(this).attr('id');
        let identifier = id.slice(0, -11);
        let selector = '#' + id;
        if (!$(selector).data('tippy-status'))
            $(selector).data('tippy-status', 'init');

        if ($(selector).data('tippy-status') === 'init')
            getTippyAlert(selector, identifier);
    });
    // Himanshu Bansal add this code of jquery for industry-information on 15-june-2021
    jQuery(document).on("mouseenter", ".left-keyconcept .has-tooltip", function (e) {
        var $PosTop = $(this).offset().top - $('body').offset().top;
        var $PosLeft = $(this).offset().left - $('body').offset().left;

        $(this).find(".tooltip-content").css({ "top": $PosTop - 12 + "px", "left": $PosLeft + "px" });
    });

    jQuery(document).on("mouseenter", ".right-Industryinformation .has-tooltip", function (e) {
        var $PosTop = $(this).offset().top - $('body').offset().top;
        var $PosRight = ($(window).width() - ($(this).offset().left + $(this).outerWidth()));

        // var $PosRight = $(this).offset().right - $('body').offset().right;
        // console.log($(this).offset().right);
        $(this).find(".tooltip-content").css({ "top": $PosTop - 12 + "px", "right": $PosRight + "px" });
    });

    /*$( "#searchCompany" ).autocomplete({

        source: function( request, response ) {
            $.ajax({
              method: "get",
              url: "company",
              dataType: "jsonp",
              cors: true,
              headers: {
                'Access-Control-Allow-Origin': '*',
              },
              success: function( data ) {
                alert("Hi");
                response( $.map( ["abcd", "xyz"], function( item ) {
                    console.log(item);
                    // return  item; 
                }));
              }
            })
        }
    });*/

    $(function () {
        /*$.getJSON("/company?searchKey="+$("#searchCompany").val(), function(data) {
            autoComplete = [];
            for (var i = 0, len = data.length; i < len; i++) {
                autoComplete.push(data[i].title);
            }

            $( "#searchCompany" ).autocomplete({
                source: autoComplete
            });
        });*/

        request = null;

        $("#searchCompany").on("keyup", function (e) {
            if (e.which === 13) {
                $("#searchCompanyBTN").trigger("click");
            }
        });

        $("#searchCompanyBTN").on("click", function (e) {

            var searchKey = $("#searchCompany").val();

            if (request != null) {
                request.abort();
            }
            if (searchKey.length > 0) {
                request = $.ajax({
                    method: "get",
                    url: "company/" + searchKey,
                    cors: true,
                    beforeSend: function () {
                        jQuery(".show_loading").show();
                    },
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                    success: function (data) {
                        jQuery(".show_loading").hide();
                        $("#searchContainer").html('');
                        if (data != "") {
                            var addedCompany = [];
                            $.each(data, function (i, v) {
                                $("#searchContainer").show();
                                if (v.companyNameLong.includes("CIK")) {
                                    if (v.ticker != null) {
                                        if (addedCompany.indexOf(v.cik.toString()) == -1) {
                                            $("#searchContainer").append('<div href="#" class="search-hint list-group-item cursor-pointer company-item" data-attr-value=' + v.cik + '>' + v.companyNameLong.split(" ").slice(0, -2).join(' ') + '</div>');
                                        }

                                        addedCompany.push(v.cik);
                                    }
                                }
                                else {
                                    $("#searchContainer").append('<div href="#" class="search-hint list-group-item cursor-pointer company-item" data-attr-value=' + v.cik + '>' + v.companyNameLong + ' (' + v.ticker + ')' + '</div>');
                                }
                            });

                        } else {
                            $("#searchContainer").show();
                            $("#searchContainer").append('<div href="#" class="search-hint list-group-item cursor-pointer">No Data Found</div>');
                        }
                    }
                });
            } else {
                jQuery(".show_loading").hide();
                $("#searchContainer").html('');
            }

        });

        $(document).on("click", ".company-item", function (e) {
            jQuery(".show_loading").show();
            var cikKey = $(this).data('attr-value');
            document.getElementById("cikvalue").value = cikKey;
            $("#myForm").submit(); // Submit the form
        });

        $(document).ready(function () {

            var $submit = $("#submit").hide(),
                $cbs = $('input[name="check"]').click(function () {
                    $submit.toggle($cbs.is(":checked"));
                });

        });

        $(document).on("click", ".myTab button", function (e) {
            $(this).parents('.tooltip-content').find(".myTabContent .tab-pane").removeClass('active');
            $(this).parents('.tooltip-content').find($(this).attr('data-bs-target')).addClass('active');
        });

        jQuery("body").on("click", ".search-hint", function () {
            jQuery(".fixed.top-0.right-0.bottom-0.left-0").trigger("click");
        });

        jQuery(document).on("mouseup", window, function (e) {
            var container = jQuery(".company-search");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                jQuery("#searchContainer").html('');
                jQuery("#searchCompany").val('');
            }

        });

    });

    /*$( ".tippy" ).each(function( index ) {
        let id = $(this).attr('id');
        let identifier = id.slice(0, -11);
        let selector = '#' + id;
        getTippyAlert(selector, identifier);
    });*/
})

function getTippyAlert(selector, identifier) {
    var url = $app_url + '/get-alert/' + identifier;
    tippy(selector, {
        arrow: true,
        animation: 'fade',
        followCursor: 'horizontal',
        // lazy: true,
        allowHTML: true,
        onCreate(instance) {
            fetch(url)
                .then((response) => response.json())
                .then((response) => {
                    // console.log("Alert Response: ");
                    // console.log(response);
                    if (response.status === 200) {
                        instance.setContent(response.data.content);
                        $(selector).data('tippy-status', 'active');
                        instance.hide();
                    } else {
                        instance.destroy(true);
                        $(selector).data('tippy-status', 'disabled');
                    }
                })
                .catch((error) => {
                    instance._error = error;
                    // instance.setContent(`Request failed. ${error}`);
                    instance.destroy(true);
                    $(selector).data('tippy-status', 'disabled');
                })
                .finally(() => {
                    instance._isFetching = false;
                });
        },
    });
}

/*$(document).on('hover', '[data-tippy-content]', function () {
    var content = $(this).attr('data-tippy-content');
    tippy($(this));
})*/
function countDownExpiry(date, currDate) {
    var countDownDate = new Date(date).getTime();
    var i = 0;
    var x = setInterval(function () {
        i++;
        var now = ((new Date(currDate).getTime()) - (60000));
        var nowD = new Date(currDate);

        $dateSeconds = nowD.getSeconds();
        nowD.setSeconds($dateSeconds + i);
        now = nowD.getTime();

        // console.log(now);
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // console.log(days + " "+hours + " "+minutes + " "+seconds+ " ");

        document.getElementById("expiryDate").innerHTML = days + "D " + hours + "H "
            + minutes + "M " + seconds + "S ";

        if (distance < 0) {
            clearInterval(x);
            document.getElementById("expiryDate").innerHTML = "EXPIRED";
            location.reload();
        }
    }, 1000);
}

/**22-12-2022 cancel survey modal */

$(document).on('click', '#cancel-survey-btn', function () {
    jQuery('#cancelSurveyModal').show();
});
$(document).on('click', '.close', function () {
    jQuery('#cancelSurveyModal').hide();

});
// $(window).click(function(e) {
//     if(jQuery('#cancelSurveyModal').length > 0){
//         jQuery('#cancelSurveyModal').hide();

//     }

// });

// var modal = document.getElementById("cancelSurveyModal");
        
//         // Get the button that opens the modal
//         var btn = document.getElementById("myBtn");
        
//         // Get the <span> element that closes the modal
//         var span = document.getElementsByClassName("close")[0];
        
//         // When the user clicks the button, open the modal 
//         btn.onclick = function() {
//           modal.style.display = "block";
//         }
        
//         // When the user clicks on <span> (x), close the modal
//         span.onclick = function() {
//           modal.style.display = "none";
//         }

//         // When the user clicks anywhere outside of the modal, close it
//         window.onclick = function(event) {
//           if (event.target == modal) {
//             modal.style.display = "none";
//           }
//         }
