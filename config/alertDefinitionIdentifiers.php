<?php
return [
    // All alert for Market sections
    [
        'identifier' => 'market_alert',
        'title' => 'Market Alert',
        'info' => 'Market alert for main navigation bar',
    ],
    [
        'identifier' => 'indices_alert',
        'title' => ' -- Indices Alert',
        'info' => 'Indices alert under Market',
    ],
    [
        'identifier' => 'commodities_alert',
        'title' => ' -- Commodities Alert',
        'info' => 'Commodities alert under Market',
    ],
    [
        'identifier' => 'forex_alert',
        'title' => ' -- Forex Alert',
        'info' => 'Forex alert under Market',
    ],
    [
        'identifier' => 'bonds_alert',
        'title' => ' -- Bonds Alert',
        'info' => 'Bonds alert under Market',
    ],
    [
        'identifier' => 'futures_alert',
        'title' => ' -- Futures Alert',
        'info' => 'Futures alert under Market',
    ],
    [
        'identifier' => 'currencies_alert',
        'title' => ' -- Currencies Alert',
        'info' => 'Currencies alert under Market',
    ],
    [
        'identifier' => 'stocks_alert',
        'title' => ' -- Stocks Alert',
        'info' => 'Stocks alert under Market',
    ],
    [
        'identifier' => 'cryptocurrencies_alert',
        'title' => ' -- Cryptocurrencies Alert',
        'info' => 'Cryptocurrencies alert under Market',
    ],

    // All alert for Home sections
    [
        'identifier' => 'home_alert',
        'title' => 'Home Alert',
        'info' => 'Home alert for main navigation bar',
    ],
    [
        'identifier' => 'profile_alert',
        'title' => ' -- Profile Alert',
        'info' => 'Profile alert under Home',
    ],
    [
        'identifier' => 'news_feed_alert',
        'title' => ' -- News Feed Alert',
        'info' => 'News Feed alert under Home',
    ],
    [
        'identifier' => 'network_alert',
        'title' => ' -- Network Alert',
        'info' => 'Network alert under Home',
    ],

    // All alert for Key Parties sections
    [
        'identifier' => 'key_parties_alert',
        'title' => 'Key Parties Alert',
        'info' => 'Key Parties alert for main navigation bar',
    ],
    [
        'identifier' => 'executive_management_alert',
        'title' => ' -- Executive Management Alert',
        'info' => 'Executive Management alert under Key Parties',
    ],
    [
        'identifier' => 'analyst_ratings_alert',
        'title' => ' -- Analyst Ratings Alert',
        'info' => 'Analyst Ratings alert under Key Parties',
    ],
    [
        'identifier' => 'ownership_interest_alert',
        'title' => ' -- Ownership Interest Alert',
        'info' => 'Ownership Interest alert under Key Parties',
    ],
    [
        'identifier' => 'percentage_ownership_alert',
        'title' => ' -- Percentage Ownership Alert',
        'info' => 'Percentage Ownership alert under Key Parties',
    ],


    // Financials main navbar alert
    [
        'identifier' => 'financials_alert',
        'title' => 'Financials Alert',
        'info' => 'Financials alert for main navigation bar',
    ],

    // Financials -> Ket Stats -> Sub navbar alert
    /*[
        'identifier' => 'key_stats_sub_navbar_alert',
        'title' => ' -- Key Stats Alert',
        'info' => 'Key Stats alert for sub navigation bar under Financials',
    ],*/
    // Financials -> Key Stats  -> income statement -> all alert
    /*[
        'identifier' => 'revenue_key_stats_IS_alert',
        'title' => ' ---- Revenue Alert',
        'info' => 'Revenue alert under Financials - Key stats - Income Statement ',
    ],
    [
        'identifier' => 'costOfRevenue_key_stats_IS_alert',
        'title' => ' ---- Cost Of Revenue Alert',
        'info' => 'Cost Of Revenue alert under Financials - Key stats - Income Statement ',
    ],
    [
        'identifier' => 'grossProfit_key_stats_IS_alert',
        'title' => ' ---- Gross Profit Alert',
        'info' => 'Gross Profit alert under Financials - Key stats - Income Statement ',
    ],
    [
        'identifier' => 'operatingExpenses_key_stats_IS_alert',
        'title' => ' ---- Operating Expenses Alert',
        'info' => 'Operating Expenses alert under Financials - Key stats - Income Statement ',
    ],
    [
        'identifier' => 'interestExpense_key_stats_IS_alert',
        'title' => ' ---- Interest Expense Alert',
        'info' => 'Interest Expense alert under Financials - Key stats - Income Statement ',
    ],
    [
        'identifier' => 'operatingIncome_key_stats_IS_alert',
        'title' => ' ---- Operating Income Alert',
        'info' => 'Operating Income alert under Financials - Key stats - Income Statement ',
    ],
    [
        'identifier' => 'netIncome_key_stats_IS_alert',
        'title' => ' ---- Net Income Alert',
        'info' => 'Net Income alert under Financials - Key stats - Income Statement ',
    ],

    // Financials -> Key Stats  -> Balance Sheet -> all alert
    [
        'identifier' => 'cashAndCashEquivalents_key_stats_BS_alert',
        'title' => ' ---- Cash And Cash Equivalents Alert',
        'info' => 'Cash And Cash Equivalents alert under Financials - Key stats - Balance Sheet ',
    ],
    [
        'identifier' => 'netReceivables_key_stats_BS_alert',
        'title' => ' ---- Net Receivables Alert',
        'info' => 'Net Receivables alert under Financials - Key stats - Balance Sheet ',
    ],
    [
        'identifier' => 'inventory_key_stats_BS_alert',
        'title' => ' ---- Inventory Alert',
        'info' => 'Inventory alert under Financials - Key stats - Balance Sheet ',
    ],
    [
        'identifier' => 'totalCurrentAssets_key_stats_BS_alert',
        'title' => ' ---- Total Current Assets Alert',
        'info' => 'Total Current Assets alert under Financials - Key stats - Balance Sheet ',
    ],
    [
        'identifier' => 'propertyPlantEquipmentNet_key_stats_BS_alert',
        'title' => ' ---- Property Plant Equipment Net Alert',
        'info' => 'Property Plant Equipment Net alert under Financials - Key stats - Balance Sheet ',
    ],
    [
        'identifier' => 'totalAssets_key_stats_BS_alert',
        'title' => ' ---- Total Assets Alert',
        'info' => 'Total Assets alert under Financials - Key stats - Balance Sheet ',
    ],
    [
        'identifier' => 'retainedEarnings_key_stats_BS_alert',
        'title' => ' ---- Retained Earnings Alert',
        'info' => 'Retained Earnings alert under Financials - Key stats - Balance Sheet ',
    ],


    // Financials -> Key Stats  -> Cash flow statement -> all alert
    [
        'identifier' => 'depreciationAndAmortization_key_stats_CS_alert',
        'title' => ' ---- Depreciation And Amortization Alert',
        'info' => 'Depreciation And Amortization alert under Financials - Key stats - Cash Flow Statement ',
    ],
    [
        'identifier' => 'changeInWorkingCapital_key_stats_CS_alert',
        'title' => ' ---- Change In Working Capital Alert',
        'info' => 'Change In Working Capital alert under Financials - Key stats - Cash Flow Statement ',
    ],
    [
        'identifier' => 'netCashProvidedByOperatingActivities_key_stats_CS_alert',
        'title' => ' ---- Net Cash Provided By Operating Activities Alert',
        'info' => 'Net Cash Provided By Operating Activities alert under Financials - Key stats - Cash Flow Statement ',
    ],
    [
        'identifier' => 'otherInvestingActivites_key_stats_CS_alert',
        'title' => ' ---- Other Investing Activities Alert',
        'info' => 'Other Investing Activities alert under Financials - Key stats - Cash Flow Statement ',
    ],
    [
        'identifier' => 'otherFinancingActivites_key_stats_CS_alert',
        'title' => ' ---- Other Financing Activites Alert',
        'info' => 'Other Financing Activites alert under Financials - Key stats - Cash Flow Statement ',
    ],
    [
        'identifier' => 'operatingCashFlow_key_stats_CS_alert',
        'title' => ' ---- Operating Cash Flow Alert',
        'info' => 'Operating Cash Flow alert under Financials - Key stats - Cash Flow Statement ',
    ],*/


    // Financials -> Income statement -> Sub navbar alert
    [
        'identifier' => 'income_statement_sub_navbar_alert',
        'title' => ' -- Income Statement Alert',
        'info' => 'Income Statement alert for sub navigation bar under Financials',
    ],
    [
        'identifier' => 'income_statement_table_alert',
        'title' => ' -- Income Statement Table Header Alert',
        'info' => 'Income Statement alert for table under Financials',
    ],

    // Financials -> Income statement -> all alert
    [
        'identifier' => 'totalRevenue_income_statement_alert',
        'title' => ' ---- Total Revenue Alert',
        'info' => 'Total Revenue alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'costOfRevenue_income_statement_alert',
        'title' => ' ---- Cost of Revenue Alert',
        'info' => 'Cost of Revenue alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'grossProfit_income_statement_alert',
        'title' => ' ---- Gross Profit Alert',
        'info' => 'Gross Profit alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'totalOperatingExpenses_income_statement_alert',
        'title' => ' ---- Total Operating Expense Alert',
        'info' => 'Total Operating Expenses alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'sellingGeneralAdministrative_income_statement_alert',
        'title' => ' ---- Selling General and Administrative Alert',
        'info' => 'Selling General and Administrative alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'researchDevelopment_income_statement_alert',
        'title' => ' ---- Research & Development Alert',
        'info' => 'Research & Development alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'depreciationAndAmortization_income_statement_alert',
        'title' => ' ---- Depreciation and Amortization',
        'info' => 'Depreciation and Amortization alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'operatingIncome_income_statement_alert',
        'title' => ' ---- Operating Income Alert',
        'info' => 'Operating Income alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'interestIncome_income_statement_alert',
        'title' => ' ---- Interest Income Alert',
        'info' => 'Interest Income alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'nonOperatingIncomeNetOther_income_statement_alert',
        'title' => ' ---- Other Non-Operating Income (Net)',
        'info' => 'Other Non-Operating Income (Net) alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'nonRecurring_income_statement_alert',
        'title' => ' ---- Non-Recurring Items Alert',
        'info' => 'Non-Recurring Items alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'totalOtherIncomeExpenseNet_income_statement_alert',
        'title' => ' ---- Total Other Income Expense (Net)',
        'info' => 'Total Other Income Expense (Net) alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'ebitda_income_statement_alert',
        'title' => ' ---- EBITDA',
        'info' => 'EBITDA alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'depreciationAndAmortization_income_statement_alert',
        'title' => ' ---- Depreciation and Amortization',
        'info' => 'Depreciation and Amortization alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'ebit_income_statement_alert',
        'title' => ' ---- EBIT Alert',
        'info' => 'EBIT alert under Financials - Income Statement',
    ],
    /*[
        'identifier' => 'accountPayables_balance_statement_alert',
        'title' => ' ---- Payables And Accrued Expenses Alert',
        'info' => 'Payables And Accrued Expenses alert under Financials - Income Statement',
    ],*/
    [
        'identifier' => 'interestExpense_income_statement_alert',
        'title' => ' ---- Interest Expense',
        'info' => 'Interest Expense alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'incomeTaxExpense_income_statement_alert',
        'title' => ' ---- Income Tax Expense',
        'info' => 'Income Tax Expense alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'netIncome_income_statement_alert',
        'title' => ' ---- Net Income Alert',
        'info' => 'Net Income alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'netIncomeFromContinuingOps_income_statement_alert',
        'title' => ' ---- Net Income From Continuing Operations Alert',
        'info' => 'Net Income From Continuing Operations alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'preferredStockAndOtherAdjustments_income_statement_alert',
        'title' => ' ---- Preferred Stock and Other Adjustments Alert',
        'info' => 'Preferred Stock and Other Adjustments alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'netIncomeApplicableToCommonShares_income_statement_alert',
        'title' => ' ---- Net Income Applicable to Common Shareholders',
        'info' => 'Net Income Applicable to Common Shareholders alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'eps_income_statement_alert',
        'title' => ' ---- EPS Alert',
        'info' => 'EPS alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'dol_income_statement_alert',
        'title' => ' ---- Degree of Operating Leverage Alert',
        'info' => 'Degree of Operating Leverage alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'dfl_income_statement_alert',
        'title' => ' ---- Degree of Financial Leverage Alert',
        'info' => 'Degree of Financial Leverage alert under Financials - Income Statement',
    ],
    [
        'identifier' => 'dtl_income_statement_alert',
        'title' => ' ---- Degree of Total Leverage Alert',
        'info' => 'Degree of Total Leverage alert under Financials - Income Statement',
    ],


    // Financials -> Balance sheet -> Sub navbar alert
    [
        'identifier' => 'balance_sheet_sub_navbar_alert',
        'title' => ' -- Balance Sheet Alert',
        'info' => 'Balance Sheet alert for sub navigation bar under Financials',
    ],
    [
        'identifier' => 'balance_sheet_table_alert',
        'title' => ' -- Balance Sheet Table Header Alert',
        'info' => 'Balance Sheet alert for table under Financials',
    ],

    // Financials -> Balance sheet -> all alert
    [
        'identifier' => 'totalAssets_balance_statement_alert',
        'title' => ' ---- Total Assets Alert',
        'info' => 'Total Assets alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'totalCurrentAssets_balance_statement_alert',
        'title' => ' ---- Total Current Assets Alert',
        'info' => 'Total Current Assets alert under Financials - Balance sheet',
    ],    
    [
        'identifier' => 'cash_balance_statement_alert',
        'title' => ' ----  Cash Alert',
        'info' => ' Cash alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'cashAndShortTermInvestments_balance_statement_alert',
        'title' => ' ----  Cash And Cash Equivalents Alert',
        'info' => ' Cash And Cash Equivalents alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'shortTermInvestments_balance_statement_alert',
        'title' => ' ----  Short-Term Investments Alert',
        'info' => ' Short-Term Investments alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'netReceivables_balance_statement_alert',
        'title' => ' ----  Receivables (net) Alert',
        'info' => ' Receivables (net) alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'inventory_balance_statement_alert',
        'title' => ' ----  Inventory Alert',
        'info' => ' Inventory alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'otherCurrentAssets_balance_statement_alert',
        'title' => ' ----  Other Current Assets Alert',
        'info' => ' Other Current Assets alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'nonCurrentAssetsTotal_balance_statement_alert',
        'title' => ' ----  Total Non-Current Assets Alert',
        'info' => ' Total Non-Current Assets alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'grossPPE_balance_statement_alert',
        'title' => ' ----  Gross PPE Alert',
        'info' => ' Gross PPE alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'accumulatedDepreciation_balance_statement_alert',
        'title' => ' ----  Accumulated Depreciation Alert',
        'info' => ' Accumulated Depreciation alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'propertyPlantEquipment_balance_statement_alert',
        'title' => ' ----  PPE (net) Alert',
        'info' => ' PPE (net) alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'accumulatedAmortization_balance_statement_alert',
        'title' => ' ----  Accumulated Amortization Alert',
        'info' => ' Accumulated Amortization alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'goodwill_balance_statement_alert',
        'title' => ' ----  Goodwill Alert',
        'info' => ' Goodwill alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'IntangibleAssets_balance_statement_alert',
        'title' => ' ----  Intangible Assets Alert',
        'info' => ' Intangible Assets alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'longTermInvestments_balance_statement_alert',
        'title' => ' ----  Long-Term Investments Alert',
        'info' => ' Long-Term Investments alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'nonCurrrentAssetsOther_balance_statement_alert',
        'title' => ' ----  Other Non-Current Assets Alert',
        'info' => ' Other Non-Current Assets alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'otherAssets_balance_statement_alert',
        'title' => ' ----  Other Assets Alert',
        'info' => ' Other Assets alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'totalLiab_balance_statement_alert',
        'title' => ' ----  Total Liabilities Alert',
        'info' => ' Total Liabilities alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'totalCurrentLiabilities_balance_statement_alert',
        'title' => ' ----  Total Current Liabilities Alert',
        'info' => ' Total Current Liabilities alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'accountsPayable_balance_statement_alert',
        'title' => ' ----  Accounts Payable Alert',
        'info' => ' Accounts Payable alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'shortTermDebt_balance_statement_alert',
        'title' => ' ----  Total Short-Term Debt Alert',
        'info' => ' Total Short-Term Debt alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'otherCurrentLiab_balance_statement_alert',
        'title' => ' ----  Other Current Liabilities Alert',
        'info' => ' Other Current Liabilities alert under Financials - Balance sheet',
    ],

    [
        'identifier' => 'nonCurrentLiabilitiesTotal_balance_statement_alert',
        'title' => ' ----  Total Non-Current Liabilities Alert',
        'info' => ' Total Non-Current Liabilities alert under Financials - Balance sheet',
    ],




    [
        'identifier' => 'deferredLongTermLiab_balance_statement_alert',
        'title' => ' ----  Non-Current Deferred Liabilities Alert',
        'info' => ' Non-Current Deferred Liabilities alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'longTermDebtTotal_balance_statement_alert',
        'title' => ' ----  Total Long-Term Debt Alert',
        'info' => ' Total Long-Term Debt alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'capitalLeaseObligations_balance_statement_alert',
        'title' => ' ----  Capital Lease Obligations Alert',
        'info' => ' Capital Lease Obligations alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'nonCurrentLiabilitiesOther_balance_statement_alert',
        'title' => ' ----  Other Non-Current Liabilities Alert',
        'info' => ' Other Non-Current Liabilities alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'negativeGoodwill_balance_statement_alert',
        'title' => ' ----  Negative Goodwill Alert',
        'info' => ' Negative Goodwill alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'totalStockholderEquity_balance_statement_alert',
        'title' => ' ----  Total Stockholders’ Equity',
        'info' => ' Total Stockholders’ Equity alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'commonStockTotalEquity_balance_statement_alert',
        'title' => ' ----  Common Stockholders Equity Alert',
        'info' => ' Common Stockholders Equity alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'commonStockSharesOutstanding_balance_statement_alert',
        'title' => ' ----  Common Shares Outstanding Alert',
        'info' => ' Common Shares Outstanding alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'preferredStockTotalEquity_balance_statement_alert',
        'title' => ' ----  Total Non-Current Liabilities Alert',
        'info' => ' Total Preferred Stock alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'preferredStockRedeemable_balance_statement_alert',
        'title' => ' ----  Redeemable Preferred Stock Alert',
        'info' => ' Redeemable Preferred Stock alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'warrants_balance_statement_alert',
        'title' => ' ----  Warrants Alert',
        'info' => ' Warrants alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'additionalPaidInCapital_balance_statement_alert',
        'title' => ' ----  Additional Paid-In Capital Alert',
        'info' => ' Additional Paid-In Capital alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'retainedEarnings_balance_statement_alert',
        'title' => ' ----  Retained Earnings Alert',
        'info' => ' Retained Earnings alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'treasuryStock_balance_statement_alert',
        'title' => ' ----  Treasury Stock Alert',
        'info' => ' Treasury Stock alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'dividendsPaid_balance_statement_alert',
        'title' => ' ----  Dividends Paid Alert',
        'info' => ' Dividends Paid alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'dividendShare_balance_statement_alert',
        'title' => ' ----  Dividend Per Share Alert',
        'info' => ' Dividend Per Share alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'dividendYield_balance_statement_alert',
        'title' => ' ----  Dividend Yield Alert',
        'info' => ' Dividend Yield alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'accumulatedOtherComprehensiveIncome_balance_statement_alert',
        'title' => ' ----  AOCI Alert',
        'info' => ' AOCI alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'liabilitiesAndStockholdersEquity_balance_statement_alert',
        'title' => ' ----  Total Liabilities and Stockholders Equity Alert',
        'info' => ' Total Liabilities and Stockholders Equity alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'netDebt_balance_statement_alert',
        'title' => ' ----  Debt (net) Alert',
        'info' => ' Debt (net) alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'netWorkingCapital_balance_statement_alert',
        'title' => ' ----  Working Capital (net) Alert',
        'info' => ' Working Capital (net) alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'netTangibleAssets_balance_statement_alert',
        'title' => ' ----  Tangible Assets (net) Alert',
        'info' => ' Tangible Assets (net) alert under Financials - Balance sheet',
    ],
    [
        'identifier' => 'BookValue_balance_statement_alert',
        'title' => ' ----  Book Value Alert',
        'info' => ' Book Value alert under Financials - Balance sheet',
    ],




    // Financials -> Cash Flow Statement sheet -> Sub navbar alert
    [
        'identifier' => 'cash_flow_statement_sub_navbar_alert',
        'title' => ' -- Cash Flow Statement Alert',
        'info' => 'Cash Flow Statement alert for sub navigation bar under Financials',
    ],
    [
        'identifier' => 'cash_flow_statement_table_alert',
        'title' => ' -- Cash Flow Statement Table Header Alert',
        'info' => 'Cash Flow Statement alert for table under Financials',
    ],

    // Financials -> Cash Flow Statement -> all alert
    [
        'identifier' => 'totalCashFromOperatingActivities_cash_flow_statement_alert',
        'title' => ' ---- Cash Flows From Operating Activities Alert',
        'info' => 'Cash Flows From Operating Activities alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'netIncomeFromContinuingOps_cash_flow_statement_alert',
        'title' => ' ---- Net Income From Continuing Operations Alert',
        'info' => 'Net Income From Continuing Operations alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'depreciationAndAmortization_cash_flow_statement_alert',
        'title' => ' ---- Depreciation and Amortization Alert',
        'info' => 'Depreciation and Amortization alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'changeInWorkingCapital_cash_flow_statement_alert',
        'title' => ' ---- Working Capital Changes Alert',
        'info' => 'Working Capital Changes alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'totalCurrentAssets_cash_flow_statement_alert',
        'title' => ' ---- Changes in Current Assets Alert',
        'info' => 'Changes in Current Assets alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'cashAndCashEquivalentsChanges_cash_flow_statement_alert',
        'title' => ' ---- Changes in Cash & Cash Equivalents Alert',
        'info' => 'Change in Inventory alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'changeToAccountReceivables_cash_flow_statement_alert',
        'title' => ' ---- Changes in Accounts Receivable Alert',
        'info' => 'Changes in Accounts Receivable alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'changeToInventory_cash_flow_statement_alert',
        'title' => ' ---- Changes In Inventory Alert',
        'info' => 'Changes In Inventory alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'accountsPayable_cash_flow_statement_alert',
        'title' => ' ---- Changes in Accounts Payable Alert',
        'info' => 'Changes in Accounts Payable alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'cashFlowsOtherOperating_cash_flow_statement_alert',
        'title' => ' ---- Other Operating Cash Flows Alert',
        'info' => 'Other Operating Cash Flows alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'changeToOperatingActivities_cash_flow_statement_alert',
        'title' => ' ---- Change to Operating Activities Alert',
        'info' => 'Change to Operating Activities alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'totalCashflowsFromInvestingActivities_cash_flow_statement_alert',
        'title' => ' ---- Cash Flows From Investing Activities Alert',
        'info' => 'Cash Flows From Investing Activities alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'propertyPlantAndEquipmentGross_cash_flow_statement_alert',
        'title' => ' ---- Change in PPE Alert',
        'info' => 'Change in PPE alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'capitalExpenditures_cash_flow_statement_alert',
        'title' => ' ---- CAPEX Alert',
        'info' => 'CAPEX alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'otherCashflowsFromInvestingActivities_cash_flow_statement_alert',
        'title' => ' ---- Other Cash Flows From Investing Activities Alert',
        'info' => 'Other Cash Flows From Investing Activities alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'totalCashFromFinancingActivities_cash_flow_statement_alert',
        'title' => ' ---- Cash Flow From Financing Activities Alert',
        'info' => 'Cash Flow From Financing Activities alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'longTermDebtTotal_cash_flow_statement_alert',
        'title' => ' ---- Changes in Long-Term Debt Alert',
        'info' => 'Changes in Long-Term Debt alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'shortTermDebt_cash_flow_statement_alert',
        'title' => ' ---- Changes in Short-Term Debt Issuance Alert',
        'info' => 'Changes in Short-Term Debt Issuance alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'commonStockSharesOutstanding_cash_flow_statement_alert',
        'title' => ' ---- Common Stock Issuance Alert',
        'info' => 'Common Stock Issuance alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'dividendsPaid_cash_flow_statement_alert',
        'title' => ' ---- Dividends Paid Alert',
        'info' => 'Dividends Paid alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'beginPeriodCashFlow_cash_flow_statement_alert',
        'title' => ' ---- Issuance of Capital Stock Alert',
        'info' => 'Beginning of Period Cash Flow alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'endPeriodCashFlow_cash_flow_statement_alert',
        'title' => ' ---- End of Period Cash Flow Alert',
        'info' => 'End of Period Cash Flow alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'changeInCash_cash_flow_statement_alert',
        'title' => ' ---- Changes in Cash Alert',
        'info' => 'Changes in Cash alert under Financials - Cash Flow Statement',
    ],
    [
        'identifier' => 'freeCashFlow_cash_flow_statement_alert',
        'title' => ' ---- Free Cash Flow Alert',
        'info' => 'Free Cash Flow alert under Financials - Cash Flow Statement',
    ],

    // Financials -> Statement of Comprehensive Income -> Sub navbar alert
    [
        'identifier' => 'comprehensive_income_sub_navbar_alert',
        'title' => ' -- Comprehensive Income Alert',
        'info' => 'Comprehensive Income alert for sub navigation bar under Financials',
    ],
    // Financials -> Statement of Shareholder Equity -> Sub navbar alert
    [
        'identifier' => 'shareholder_equity_sub_navbar_alert',
        'title' => ' -- Shareholder Equity Alert',
        'info' => 'Shareholder Equity alert for sub navigation bar under Financials',
    ],

    // Financials -> Comparable -> Sub navbar alert
    [
        'identifier' => 'comparables_sub_navbar_alert',
        'title' => ' -- Comparables Alert',
        'info' => 'Comparables alert for sub navigation bar under Financials',
    ],

    // Financials -> Comparable -> Standard -> Balance sheet -> all alert
    [
        'identifier' => 'totalAssets_vertical_balance_sheet_alert',
        'title' => ' ---- Total Assets Standard Alert',
        'info' => 'Total Assets alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'totalCurrentAssets_vertical_balance_sheet_alert',
        'title' => ' ---- Current Assets Standard Alert',
        'info' => 'Current Assets alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'cashAndCashEquivalents_vertical_balance_sheet_alert',
        'title' => ' ---- Cash And Cash Equivalents Standard Alert',
        'info' => 'Cash And Cash Equivalents alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'cashAndCashEquivalentsAndDueFromBanks_vertical_balance_sheet_alert',
        'title' => ' ---- Cash and Cash Equivalents & Due from Banks Standard Alert',
        'info' => 'Cash and Cash Equivalents & Due from Banks alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'netLoans_vertical_balance_sheet_alert',
        'title' => ' ---- Net Loans Standard Alert',
        'info' => 'Net Loans alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'realEstateOwned_vertical_balance_sheet_alert',
        'title' => ' ---- Real Estate Owned Standard Alert',
        'info' => 'Real Estate Owned alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'mortgageLoans_vertical_balance_sheet_alert',
        'title' => ' ---- Mortgage Loans Standard Alert',
        'info' => 'Mortgage Loans alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'policyLoans_vertical_balance_sheet_alert',
        'title' => ' ---- Policy Loans Standard Alert',
        'info' => 'Policy Loans alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'investmentOthers_vertical_balance_sheet_alert',
        'title' => ' ---- Investment - Others Standard Alert',
        'info' => 'Investment - Others alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'interestBearingDeposits_vertical_balance_sheet_alert',
        'title' => ' ---- Interest Bearing Deposits Standard Alert',
        'info' => 'Interest Bearing Deposits alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'moneyMarketInvestments_vertical_balance_sheet_alert',
        'title' => ' ---- Money Market Investments Standard Alert',
        'info' => 'Money Market Investments alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'netReceivables_vertical_balance_sheet_alert',
        'title' => ' ---- Receivables Standard Alert',
        'info' => 'Receivables alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'inventory_vertical_balance_sheet_alert',
        'title' => ' ---- Inventory Standard Alert',
        'info' => 'Inventory alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'otherCurrentAssets_vertical_balance_sheet_alert',
        'title' => ' ---- Other Current Assets Standard Alert',
        'info' => 'Other Current Assets alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'totalNonCurrentAssets_vertical_balance_sheet_alert',
        'title' => ' ---- Total non-current Assets Standard Alert',
        'info' => 'Total non-current Assets alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'propertyPlantEquipmentNet_vertical_balance_sheet_alert',
        'title' => ' ---- Net PPE Standard Alert',
        'info' => 'Net PPE alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'shortTermInvestmentsBanks_vertical_balance_sheet_alert',
        'title' => ' ---- Short-Term Investments - Banks Standard Alert',
        'info' => 'Short-Term Investments - Banks alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'federalFundsSold_vertical_balance_sheet_alert',
        'title' => ' ---- Federal Funds Sold Standard Alert',
        'info' => 'Federal Funds Sold alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'longTermInvestments_vertical_balance_sheet_alert',
        'title' => ' ---- Investments And Advances Standard Alert',
        'info' => 'Investments And Advances alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'investmentsInFedHomeLoanBank_vertical_balance_sheet_alert',
        'title' => ' ---- Investments in FedHomeLoanBank Standard Alert',
        'info' => 'Investments in FedHomeLoanBank alert under Financials - Comparable - Standard - Balance sheet',
    ],
    [
        'identifier' => 'investmentInRealEstate_vertical_balance_sheet_alert',
        'title' => ' ---- Investment in Real Estate Standard Alert',
        'info' => 'Investment in Real Estate alert under Financials - Comparable - Standard - Balance sheet',
    ],
    /////////////////////////////////

    // All alert for Ratios sections
    [
        'identifier' => 'ratios_alert',
        'title' => 'Ratios Alert',
        'info' => 'Ratios alert for main navigation bar',
    ],
    [
        'identifier' => 'coverage_ratios_alert',
        'title' => 'Coverage Ratios Alert',
        'info' => 'Coverage Ratios alert for ratio',
    ],
    [
        'identifier' => 'interest_coverage_ratios_alert',
        'title' => 'Interest Coverage Alert',
        'info' => 'Interest Coverage alert for Coverage ratio in ratio page',
    ],

    [
        'identifier' => 'fixed_charge_coverage_ratios_alert',
        'title' => 'Fixed Charge Coverage Alert',
        'info' => 'Fixed Charge Coverage alert for Coverage ratio in ratio page',
    ],
    [
        'identifier' => 'activity_ratios_alert',
        'title' => 'Activity Ratios Alert',
        'info' => 'Activity Ratios alert for ratio',
    ],
    [
        'identifier' => 'inventory_turnover_ratios_alert',
        'title' => 'Inventory Turnover Alert',
        'info' => 'Inventory Turnover alert for ratio page',
    ],
    [
        'identifier' => 'days_of_inventory_on_hand_doh_ratios_alert',
        'title' => 'Days of Inventory on Hand (DOH) Alert',
        'info' => 'Days of Inventory on Hand (DOH) alert for ratio page',
    ],
    [
        'identifier' => 'receivables_turnover_ratios_alert',
        'title' => 'Receivables Turnover Alert',
        'info' => 'Receivables Turnover alert for ratio page',
    ],
    [
        'identifier' => 'days_of_sales_outstanding_dso_ratios_alert',
        'title' => 'Days of Sales Outstanding (DSO) Alert',
        'info' => 'Days of Sales Outstanding (DSO) alert for ratio page',
    ],
    [
        'identifier' => 'payables_turnover_ratios_alert',
        'title' => 'Payables Turnover Alert',
        'info' => 'Payables Turnover alert for ratio page',
    ],
    [
        'identifier' => 'days_payable_outstanding_dpo_ratios_alert',
        'title' => 'Days Payable Outstanding (DPO) Alert',
        'info' => 'Days Payable Outstanding (DPO) alert for ratio page',
    ],
    [
        'identifier' => 'working_capital_turnover_ratios_alert',
        'title' => 'Working Capital Turnover Alert',
        'info' => 'Working Capital Turnover alert for ratio page',
    ],
    [
        'identifier' => 'fixed_asset_turnover_ratios_alert',
        'title' => 'Fixed Asset Turnover Alert',
        'info' => 'Fixed Asset Turnover alert for ratio page',
    ],
    [
        'identifier' => 'total_asset_turnover_ratios_alert',
        'title' => 'Total Asset Turnover Alert',
        'info' => 'Total Asset Turnover alert for ratio page',
    ],
    [
        'identifier' => 'performance_ratios_alert',
        'title' => 'Performance Ratios Alert',
        'info' => 'Performance Ratios alert for ratio page',
    ],
    [
        'identifier' => 'cash_flow_to_revenue_ratios_alert',
        'title' => 'Cash Flow to Revenue Alert',
        'info' => 'Cash Flow to Revenue alert for ratio page',
    ],
    [
        'identifier' => 'cash_return_on_assets_ratios_alert',
        'title' => 'Cash Return on Assets Alert',
        'info' => 'Cash Return on Assets alert for ratio page',
    ],
    [
        'identifier' => 'cash_return_on_equity_ratios_alert',
        'title' => 'Cash Return on Equity Alert',
        'info' => 'Cash Return on Equity alert for ratio page',
    ],
    [
        'identifier' => 'cash_to_operating_income_ratios_alert',
        'title' => 'Cash to Operating Income Alert',
        'info' => 'Cash to Operating Income alert for ratio page',
    ],
    [
        'identifier' => 'cash_flow_per_share_ratios_alert',
        'title' => 'Cash Flow Per Share Alert',
        'info' => 'Cash Flow Per Share alert for ratio page',
    ],
    [
        'identifier' => 'debt_payment_ratios_alert',
        'title' => 'Debt Payment Alert',
        'info' => 'Debt Payment alert for ratio page',
    ],
    [
        'identifier' => 'dividend_payment_ratios_alert',
        'title' => 'Dividend Payment Alert',
        'info' => 'Dividend Payment alert for ratio page',
    ],
    [
        'identifier' => 'investing_and_financing_ratios_alert',
        'title' => 'Investing and Financing Alert',
        'info' => 'Investing and Financing alert for ratio page',
    ],
    [
        'identifier' => 'debt_coverage_ratios_alert',
        'title' => 'Debt Coverage Alert',
        'info' => 'Debt Coverage alert for ratio page',
    ],
    [
        'identifier' => 'performance_interest_coverage_ratios_alert',
        'title' => 'Performance Interest Coverage Alert',
        'info' => 'Interest Coverage alert for ratio page',
    ],
    [
        'identifier' => 'reinvestment_ratios_alert',
        'title' => 'Reinvestment Alert',
        'info' => 'Reinvestment alert for ratio page',
    ],
    [
        'identifier' => 'credit_ratios_alert',
        'title' => 'Credit Ratios Alert',
        'info' => 'Credit Ratios alert for ratio page',
    ],
    [
        'identifier' => 'ebit_interest_coverage_ratios_alert',
        'title' => 'EBIT Interest Coverage Alert',
        'info' => 'EBIT Interest Coverage alert for ratio page',
    ],
    [
        'identifier' => 'ebitda_interest_coverage_ratios_alert',
        'title' => 'EBITDA Interest Coverage Alert',
        'info' => 'EBITDA Interest Coverage alert for ratio page',
    ],
    [
        'identifier' => 'return_on_capital_ratios_alert',
        'title' => 'Return On Capital Alert',
        'info' => 'Return On Capital alert for ratio page',
    ],
    [
        'identifier' => 'free_operating_cash_flow_to_debt_ratios_alert',
        'title' => 'Free Operating Cash Flow to Debt Alert',
        'info' => 'Free Operating Cash Flow to Debt alert for ratio page',
    ],
    [
        'identifier' => 'discretionary_cash_flow_to_debt_ratios_alert',
        'title' => 'Discretionary Cash Flow to Debt ALert',
        'info' => 'Discretionary Cash Flow to Debt alert for ratio page',
    ],
    [
        'identifier' => 'cash_flow_net_to_capex_ratios_alert',
        'title' => 'Cash Flow (net) to CAPEX Alert',
        'info' => 'Cash Flow (net) to CAPEX alert for ratio page',
    ],
    [
        'identifier' => 'return_on_sale_ratios_alert',
        'title' => 'Return_On_Sale Ratios Alert',
        'info' => 'Return_On_Sale Ratios alert for ratio page',
    ],
    [
        'identifier' => 'gross_profit_margin_ratios_alert',
        'title' => 'Gross Profit Margin Alert',
        'info' => 'Gross Profit Margin alert for ratio page',
    ],
    [
        'identifier' => 'operating_margin_ratios_alert',
        'title' => 'Operating Margin Alert',
        'info' => 'Operating Margin alert for ratio page',
    ],
    [
        'identifier' => 'pre_tax_margin_ratios_alert',
        'title' => 'Pre-Tax Margin ALert',
        'info' => 'Pre-Tax Margin alert for ratio page',
    ],
    [
        'identifier' => 'net_income_profit_margin_ratios_alert',
        'title' => 'Net Income (Profit) Margin Alert',
        'info' => 'Net Income (Profit) Margin alert for ratio page',
    ],
    [
        'identifier' => 'return_on_investment_ratios_alert',
        'title' => 'Return_On_Investment Ratios Alert',
        'info' => 'Return_On_Investment Ratios alert for ratio page',
    ],
    [
        'identifier' => 'operating_roa_ratios_alert',
        'title' => 'Operating ROA Alert',
        'info' => 'Operating ROA alert for ratio page',
    ],
    [
        'identifier' => 'roa_ratios_alert',
        'title' => 'ROA Alert',
        'info' => 'ROA alert for ratio page',
    ],
    [
        'identifier' => 'return_on_total_capital_ratios_alert',
        'title' => 'Return on Total Capital Alert',
        'info' => 'Return on Total Capital alert for ratio page',
    ],
    [
        'identifier' => 'return_on_total_equity_ratios_alert',
        'title' => 'Return on Total Equity Alert',
        'info' => 'Return on Total Equity alert for ratio page',
    ],
    [
        'identifier' => 'return_on_common_equity_ratios_alert',
        'title' => 'Return on Common Equity Alert',
        'info' => 'Return on Common Equity alert for ratio page',
    ],
    [
        'identifier' => 'solvency_ratios_alert',
        'title' => 'Solvency Ratios Alert',
        'info' => 'Solvency Ratios alert for ratio page',
    ],
    [
        'identifier' => 'debt_to_asset_ratio_ratios_alert',
        'title' => 'Debt-to-Asset Ratio Alert',
        'info' => 'Debt-to-Asset Ratio alert for ratio page',
    ],
    [
        'identifier' => 'debt_to_capital_ratio_ratios_alert',
        'title' => 'Debt-to-Capital Ratio Alert',
        'info' => 'Debt-to-Capital Ratio alert for ratio page',
    ],
    [
        'identifier' => 'debt_to_equity_ratios_alert',
        'title' => 'Debt-to-Equity Alert',
        'info' => 'Debt-to-Equity alert for ratio page',
    ],
    [
        'identifier' => 'financial_leverage_ratios_alert',
        'title' => 'Financial Leverage Alert',
        'info' => 'Financial Leverage alert for ratio page',
    ],
    [
        'identifier' => 'valuation_ratios_alert',
        'title' => 'Valuation Ratios Alert',
        'info' => 'Valuation Ratios alert for ratio page',
    ],
    [
        'identifier' => 'p/e_ratios_alert',
        'title' => 'P/E Alert',
        'info' => 'P/E alert for ratio page',
    ],
    [
        'identifier' => 'p/cf_ratios_alert',
        'title' => 'P/CF Alert',
        'info' => 'P/CF alert for ratio page',
    ],
    [
        'identifier' => 'p/s_ratios_alert',
        'title' => 'P/S Alert',
        'info' => 'P/S alert for ratio page',
    ],
    [
        'identifier' => 'p/bv_ratios_alert',
        'title' => 'P/BV Alert',
        'info' => 'P/BV alert for ratio page',
    ],
    [
        'identifier' => 'price_per_share_ratios_alert',
        'title' => 'Price Per Share Ratios Alert',
        'info' => 'Price Per Share Ratios alert for ratio page',
    ],
    [
        'identifier' => 'ebitda/share_ratios_alert',
        'title' => 'EBITDA/Share Alert',
        'info' => 'EBITDA/Share alert for ratio page',
    ],
    [
        'identifier' => 'dividend_per_share_ratios_alert',
        'title' => 'Dividend Per Share Alert',
        'info' => 'Dividend Per Share alert for ratio page',
    ],
    [
        'identifier' => 'dividend_payout_ratios_alert',
        'title' => 'Dividend Payout Ratios Alert',
        'info' => 'Dividend Payout Ratios alert for ratio page',
    ],
    [
        'identifier' => 'dividend_payout_ratio_ratios_alert',
        'title' => 'Dividend Payout Ratio Alert',
        'info' => 'Dividend Payout Ratio alert for ratio page',
    ],
    [
        'identifier' => 'dividend_retention_ratio_ratios_alert',
        'title' => 'Dividend Retention Ratio Alert',
        'info' => 'Dividend Retention Ratio alert for ratio page',
    ],
    [
        'identifier' => 'sustainable_growth_rate_ratios_alert',
        'title' => 'Sustainable Growth Rate Alert',
        'info' => 'Sustainable Growth Rate alert for ratio page',
    ],
    [
        'identifier' => 'liquidity_ratios_alert',
        'title' => 'Liquidity Ratios Alert',
        'info' => 'Liquidity Ratios alert for ratio page',
    ],
    [
        'identifier' => 'current_ratio_ratios_alert',
        'title' => 'Current Ratio Alert',
        'info' => 'Current Ratio alert for ratio page',
    ],
    [
        'identifier' => 'quick_ratio_ratios_alert',
        'title' => 'Quick Ratio Alert',
        'info' => 'Quick Ratio alert for ratio page',
    ],
    [
        'identifier' => 'cash_ratio_ratios_alert',
        'title' => 'Cash Ratio Alert',
        'info' => 'Cash Ratio alert for ratio page',
    ],
    [
        'identifier' => 'defensive_interval_ratio_ratios_alert',
        'title' => 'Defensive Interval Ratio Alert',
        'info' => 'Defensive Interval Ratio alert for ratio page',
    ],
    [
        'identifier' => 'cash_conversion_cycle_ratios_alert',
        'title' => 'Cash Conversion Cycle Alert',
        'info' => 'Cash Conversion Cycle alert for ratio page',
    ],

    // All alert for Charting / Technical Analysis sections
    [
        'identifier' => 'charting_technical_analysis_alert',
        'title' => 'Charting / Technical Analysis Alert',
        'info' => 'Charting / Technical Analysis alert for main navigation bar',
    ],

    // All alert for Regulatory Filing sections
    [
        'identifier' => 'regulatory_filing_alert',
        'title' => 'Regulatory Filing Alert',
        'info' => 'Regulatory Filing alert for main navigation bar',
    ],

    // All alert for Calendars sections
    [
        'identifier' => 'calendars_alert',
        'title' => 'Calendars Alert',
        'info' => 'Calendars alert for main navigation bar',
    ],

    // All alert for Network sections
    [
        'identifier' => 'networks_alert',
        'title' => 'Network Alert',
        'info' => 'Network alert for main navigation bar',
    ],

    // All alert for Screening sections
    [
        'identifier' => 'screening_alert',
        'title' => 'Screening Alert',
        'info' => 'Screening alert for main navigation bar',
    ],

    // All alert for Comparable Tab
    [
        'identifier' => 'standard_comparable_alert',
        'title' => 'Standard Comparable Alert',
        'info' => 'Standard Comparable alert for comparable tab',
    ],
    [
        'identifier' => 'vertical_comparable_alert',
        'title' => 'Vertical Comparable Alert',
        'info' => 'Vertical Comparable alert for comparable tab',
    ],
    [
        'identifier' => 'interfirm_comparable_alert',
        'title' => 'InterFirm Comparable Alert',
        'info' => 'InterFirm Comparable alert for comparable tab',
    ],
    [
        'identifier' => 'intrafirm_comparable_alert',
        'title' => 'IntraFirm Comparable Alert',
        'info' => 'IntraFirm Comparable alert for comparable tab',
    ],
    //End alert for Comparable Tab

];
