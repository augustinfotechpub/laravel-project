<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_REDIRECT_URL'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        "client_secret" => env('GOOGLE_CLIENT_SECRET'),
        "redirect" => env('GOOGLE_REDIRECT_URL')
    ],


    'linkedin' => [
        'client_id' => env('LINKEDIN_CLIENT_ID'),
        'client_secret' => env('LINKEDIN_CLIENT_SECRET'),
        'redirect' => env('LINKEDIN_REDIRECT_URL'),
    ],

    'financial_modeling_prep' => [
        'api_key' => env('FINANCIAL_MODELING_PREP', '07a740dd14d66b2b561e73a8d593e964'),
        'uri' => [
            'v3' => "https://financialmodelingprep.com/api/v3/",
            'v4' => "https://financialmodelingprep.com/api/v4/",
        ],
    ],

    'edgar_financial' => [
        'api_token' => env('EDGAR_FINANCIAL', 'OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX'),
        'uri' => [
            'eod_url' => "https://eodhistoricaldata.com/"
        ],
    ],

    'edgar_sec_api' => [
        'api_key' => env('EDGAR_SEC_API', '5a8308125158c73e67677cb9a4696e4c9b76e8ea22d77576e2a925b291157a89')
    ],

    'iex_cloud' => [
        'keys' => [
            'secret' => env('IEX_CLOUD_SECRET', "sk_9b147116f0b24edd8de9294d458f4eb7"),
            'publishable' => env('IEX_CLOUD_PUBLISHABLE', "pk_2c1ec817a1af4f45a589a9f4ca829eb2"),
        ],
        'base_url' => 'https://cloud.iexapis.com/',
        'version' => [
            'stable' => 'stable/',
            'v1' => 'v1/',
            'beta' => 'beta'
        ]
    ],

    'eod_key_parties' => [
        'api_key' => env('EOD_KEY_PARTIES','60ec6a9aeff734.69550837')
    ],
];
