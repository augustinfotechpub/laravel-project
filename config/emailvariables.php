<?php

return [
    'user_welcome_email' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'USER_NAME' => 'Name of the User',
        'USER_EMAIL' => 'Email of the user',
        'SUBSCRIPTION_PLAN_TITLE' => 'Name of subscription plan',
        'SUBSCRIPTION_PLAN_PRICE' => ' Price of subscription plan',
        'SUBSCRIPTION_PLAN_DURATION' => 'Duration of subscription plan',
    ],

    'user_forgot_password' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'RESET_LINK' => 'Reset Link of the User',
        'USER_NAME' => 'Name of the User',
        'USER_EMAIL' => 'Email of the user',
    ],

    'user_email_verification' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'ACTIVATION_LINK' => 'Email verification link',
        'USER_NAME' => 'Name of the User',
        'USER_EMAIL' => 'Email of the user',
    ],

    'admin_forgot_password' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'RESET_LINK' => 'Reset Link of the admin',
        'USER_NAME' => 'Name of the admin',
        'USER_EMAIL' => 'Email of the admin',
    ],

    'user_monthly_survey' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'USER_NAME' => 'Name of the admin',
        'USER_EMAIL' => 'Email of the admin',
    ],

    'user_subscription_alert' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'UPGRADE_URL' => 'Upgrade URL for new plan for the User',
        'USER_NAME' => 'Name of the user',
        'USER_EMAIL' => 'Email of the user',
        'EXPIRED_DATE' => 'Subsciption Expire Date',

    ],

    // 23 dec 2022 
    'user_cancel_subscription_survey' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'USER_NAME' => 'Name of the user',
        'USER_EMAIL' => 'Email of the user',
    ],
    'admin_cancel_subscription_survey' => [
        'APP_NAME' => 'Application Name',
        'APP_URL' => 'Application Url. e.g. https://www.' . env('APP_URL'),
        'APP_LOGO' => 'Application Logo',
        'USER_NAME' => 'Name of the user',
        'USER_EMAIL' => 'Email of the user',
        'ADMIN_NAME' => 'Admin name',
        'ADMIN_EMAIL' => 'Admin email',
    ],

];

