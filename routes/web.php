<?php

use App\Http\Controllers\Front\HomeController;
use App\Models\BalanceSheetStatement;
use App\Models\CashFlowStatement;
use App\Models\Company;
use App\Models\Definition;
use App\Models\MapperCikName;
use App\Services\FinancialModelingPrep;
use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\ChatController;
use App\Http\Controllers\API\UserAPIController;
use App\Http\Controllers\API\BlockUserAPIController;
use App\Http\Controllers\API\GroupAPIController;
use App\Http\Controllers\API\ChatAPIController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\API\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('user/email-verify/{token}', [App\Http\Controllers\Front\UserController::class, 'emailVerify'])->name('user.email_verification');
Route::get('get-alert/{identifier}', [\App\Http\Controllers\AlertController::class, 'getAlert']);
Route::get('pdf/{symbol}/earning-transcript', [HomeController::class, 'pdfTranscript'])->name('pdf');
Route::get('sub', [\App\Http\Controllers\Front\SubscriptionPlanController::class, 'index']);
$pageRoutes = [];
Route::get('/', [App\Http\Controllers\Front\HomeController::class, 'index'])->name("home");
Route::get('earning-call-transcript/{symbol}/{quarter}/{year}', [App\Http\Controllers\Front\HomeController::class, 'showEarningTranscript'])->name("earning_call_transcript");

Route::get('data', [\App\Http\Controllers\Front\KeywordController::class, 'keyword']);
Route::get('/app', [App\Http\Controllers\Front\HomeController::class, 'app'])->name("app_main")->middleware('auth','privacyPolicy', 'user.activated');

Route::get('app/stock-details', [App\Http\Controllers\Front\HomeController::class, 'stockDetails'])->name('stock-details')->middleware('auth','privacyPolicy', 'user.activated');

Route::get('/chart/{symbol}', [App\Http\Controllers\Front\HomeController::class, 'chart'])->name("chart");

//Route::get('/messages', ['uses' => 'Front/MessagesController@index', 'as' => 'messages']);

// user registration route
Route::get('user/register', [\App\Http\Controllers\Front\UserController::class, 'register'])->name('user.register');
Route::get('user/welcome', [\App\Http\Controllers\Front\UserController::class, 'welcome'])->name('user.welcome')->middleware('auth');
Route::post('user/register/store', [App\Http\Controllers\Front\UserController::class, 'store'])->name('user.register.store');

Route::get('user/make-subscription/{id}', [App\Http\Controllers\Front\UserController::class, 'registerWithSocialite'])->name('user.make_subscription');
Route::post('user/make-subscription/store', [App\Http\Controllers\Front\UserController::class, 'makeSubscription'])->name('user.makeSubscription.store');

// Login Route
Route::get('user/login', [\App\Http\Controllers\Front\Auth\LoginController::class, 'index'])->name('user.login');
Route::post('user/login', [\App\Http\Controllers\Front\Auth\LoginController::class, 'authenticate'])->name('user.authenticate');
Route::get('user/logout', [\App\Http\Controllers\Front\Auth\LoginController::class, 'logout'])->name('user.logout');

//forgot password route
Route::get('user/forgot-password', [\App\Http\Controllers\Front\Auth\ForgotPasswordController::class, 'showForgotPasswordForm'])->name('user.forgot_password.create');
Route::post('user/forgot-password', [\App\Http\Controllers\Front\Auth\ForgotPasswordController::class, 'sendPasswordResetLink'])->name('user.forgot_password.send');
Route::get('user/reset-password/{token}', [\App\Http\Controllers\Front\Auth\ForgotPasswordController::class, 'resetPasswordForm'])->name('user.reset_password.create');
Route::post('user/reset-password', [\App\Http\Controllers\Front\Auth\ForgotPasswordController::class, 'passwordReset'])->name('user.reset_password.update');

Route::get('user/auth/{provider}', [\App\Http\Controllers\Front\Auth\LoginController::class, 'redirectToProvider'])->name('user.social.auth');
Route::get('user/auth/{provider}/callback', [\App\Http\Controllers\Front\Auth\LoginController::class, 'handleProviderCallback']);

Route::post('contact-request/store', [\App\Http\Controllers\Front\ContactRequestController::class, 'store'])->name('user.contact_request.store');

Route::get('user/subscription', [App\Http\Controllers\Front\SubscriptionPlanController::class, 'index'])->name('user.subscription');
Route::get('user/subscription/change-plan', [App\Http\Controllers\Front\SubscriptionPlanController::class, 'changePlan'])->name('user.subscription.chang_plan');
Route::post('user/subscription/store', [App\Http\Controllers\Front\SubscriptionPlanController::class, 'makeSubscription'])->name('user.subscription.store');

/*Route::get('/fillings-list', [App\Http\Controllers\Front\HomeController::class, 'fillingsList'])->name("app.filling-list");*/

Route::get('heat-map', [\App\Http\Controllers\Front\UserController::class, 'heatMap'])->name('heat-map');

Route::group(['middleware' => ['user.activated', 'auth']], function () {
// Route::middleware('auth')->group(function () {
    Route::get('user/profile', [\App\Http\Controllers\Front\UserController::class, 'profile'])->name('user.profile');
    Route::get('user/profile/edit', [\App\Http\Controllers\Front\UserController::class, 'editProfile'])->name('user.profile.edit');
    Route::post('user/profile/update', [\App\Http\Controllers\Front\UserController::class, 'updateProfile'])->name('user.profile.update');
    
});

Route::prefix('markets')->group(function () {
    Route::get('indices', function () {
        return view('livewire.markets.indices');
    })->name('markets.indices');

    Route::get('commodities', function () {
        return view('livewire.markets.commodities');
    })->name('markets.commodities');

    Route::get('forex', function () {
        
        return view('livewire.markets.forex');
    })->name('markets.forex');

    Route::get('bonds', function () {
        return view('livewire.markets.bonds');
    })->name('markets.bonds');

    Route::get('futures', function () {
        return view('livewire.markets.futures');
    })->name('markets.futures');

    Route::get('currencies', function () {
        return view('livewire.markets.currencies');
    })->name('markets.currencies');

    Route::get('stocks', function () {
        return view('livewire.markets.stocks');
    })->name('markets.stocks');

    Route::get('cryptocurrencies', function () {
        return view('livewire.markets.cryptocurrencies');
    })->name('markets.cryptocurrencies');
});

Route::prefix('calendar')->group(function () {
    Route::get('economic', function () {
        return view('livewire.calendar.economic');
    })->name('calendar.economic');
});


Route::get('stock-screener', function () {
    return view('livewire.stock-screener.widget');
})->name('stock-screener-widget');

Route::get('full-screen', function () {
    return view('livewire.partials.full-screen');
})->name('full-screen');

Route::prefix('widget')->group(function () {
    Route::get('stock-summary', function () {
        return view('livewire.widgets.stock-summary');
    })->name('widget.stock-summary');

    Route::get('company-profile', function () {
        return view('livewire.widgets.company-profile');
    })->name('widget.company-profile');

    Route::get('stocks-ticker', function () {
        return view('livewire.widgets.stock-ticker');
    })->name('stocks-ticker');
});

Route::get('search/{searchString?}', function ($searchString = '') {
    $res = MapperCikName::search($searchString)->first();
    dd($res);
});

Route::get('test', function (Request $request) {
    $zacksService = new \App\Services\ZacksService();
    $rows = $zacksService->getFinancials("JPM", 'A', 'balanceSheet');
    foreach ($rows as $date => $row) {
        $year = Carbon::parse($date);
        $ret = BalanceSheetStatement::query()->whereYear('date', $year)->where(['period' => 'FY', 'symbol' => "JPM"])->update($row);
        echo $ret . "<br/>";
    }
});

Route::get('full-screen/chart', function () {
    return view('front.charting');
})->name('full_chart');

Route::get('home-screen/chart', function () {
    return view('livewire.widgets.home-screen-chart');
})->name('home-screen.chart');

Route::get('/yeardatabs', [App\Http\Controllers\Front\HomeController::class, 'yearDatabs'])->name("year_databs");
Route::get('/yeardatais', [App\Http\Controllers\Front\HomeController::class, 'yearDatais'])->name("year_datais");
Route::get('/yeardatacf', [App\Http\Controllers\Front\HomeController::class, 'yearDatacf'])->name("year_datacf");
Route::get('/company/{searchKey}', [App\Http\Controllers\Front\HomeController::class, 'searchCompany'])->name("searchCompany");
Route::post('/select/company', [App\Http\Controllers\Front\HomeController::class, 'selectCompany'])->name("selectCompany");
Route::get('/select/company', [App\Http\Controllers\Front\HomeController::class, 'selectCompany'])->name("selectCompany");

Route::get('/funds', [App\Http\Controllers\Front\HomeController::class, 'holder']);




/* Network-Chat routes */
Route::get('upgrade-to-v3-4-0', function () {
    try {

        \Artisan::call('migrate', [
            '--path' => '/database/migrations/2020_10_19_133700_move_all_existing_devices_to_new_table.php',
            '--force' => true
        ]);

        return 'You are successfully migrated to v3.4.0';
    } catch (Exception $exception){
        return $exception->getMessage();
    }
});

Route::get('/upgrade-to-v4-3-0', function () {
    try {
        Artisan::call('db:seed', ['--class' => 'CreatePermissionSeeder']);

        return 'You are successfully seeded to v4.3.0';
    } catch (Exception $exception){
        return $exception->getMessage();
    }
});


Auth::routes();
Route::get('activate', [AuthController::class, 'verifyAccount']);

Route::get('/home', [HomeController::class, 'index']);
Route::post('update-language', [UserController::class, 'updateLanguage'])->middleware('auth');
Route::group(['middleware' => ['user.activated', 'auth']], function () {
    //view routes
    Route::get('/conversations', [ChatController::class, 'index'])->name('conversations');
    Route::get('profile', [UserController::class, 'getProfile']);
    Route::group(['namespace' => 'API'], function () {
        Route::get('logout', [Auth\LoginController::class, 'logout']);
        //get all user list for chat
        Route::get('users-list', [UserAPIController::class, 'getUsersList']);
        Route::get('get-users', [UserAPIController::class, 'getUsers']);
        Route::delete('remove-profile-image', [UserAPIController::class, 'removeProfileImage']);
        /** Change password */
        Route::post('change-password', [UserAPIController::class, 'changePassword']);
        Route::get('conversations/{ownerId}/archive-chat', [UserAPIController::class, 'archiveChat']);

        Route::get('get-profile', [UserAPIController::class,'getProfile']);
        Route::post('profile', [UserAPIController::class,'updateProfile'])->name('update.profile');
        Route::post('update-last-seen', [UserAPIController::class,'updateLastSeen']);

        Route::post('send-message',
            [ChatAPIController::class, 'sendMessage'])->name('conversations.store')->middleware('sendMessage');
        Route::get('users/{id}/conversation', [UserAPIController::class,'getConversation']);
        Route::get('conversations-list', [ChatAPIController::class, 'getLatestConversations']);
        Route::get('conversations-list-user', [ChatAPIController::class, 'getLatestConversationsForUesr']);
        Route::get('archive-conversations', [ChatAPIController::class, 'getArchiveConversations']);
        Route::post('read-message', [ChatAPIController::class, 'updateConversationStatus']);
        Route::post('file-upload', [ChatAPIController::class, 'addAttachment'])->name('file-upload');
        Route::post('image-upload', [ChatAPIController::class, 'imageUpload'])->name('image-upload');
        Route::get('conversations/{userId}/delete', [ChatAPIController::class, 'deleteConversation']);
        Route::post('conversations/message/{conversation}/delete', [ChatAPIController::class, 'deleteMessage']);
        Route::post('conversations/{conversation}/delete', [ChatAPIController::class, 'deleteMessageForEveryone']);
        Route::get('/conversations/{conversation}', [ChatAPIController::class, 'show']);
        Route::post('send-chat-request', [ChatAPIController::class, 'sendChatRequest'])->name('send-chat-request');
        Route::post('accept-chat-request', [ChatAPIController::class, 'acceptChatRequest'])->name('accept-chat-request');
        Route::post('decline-chat-request', [ChatAPIController::class, 'declineChatRequest'])->name('decline-chat-request');

        /** Web Notifications */
        Route::put('update-web-notifications', [UserAPIController::class,'updateNotification']);

        /** BLock-Unblock User */
        Route::put('users/{user}/block-unblock', [BlockUserAPIController::class, 'blockUnblockUser']);
        Route::get('blocked-users', [BlockUserAPIController::class, 'blockedUsers']);

        /** My Contacts */
        Route::get('my-contacts', [UserAPIController::class,'myContacts'])->name('my-contacts');

        /** Groups API */
        Route::post('groups', [GroupAPIController::class, 'create']);
        Route::post('groups/{group}', [GroupAPIController::class, 'update']);
        Route::get('groups', [GroupAPIController::class, 'index']);
        Route::get('groups/{group}', [GroupAPIController::class, 'show']);
        Route::put('groups/{group}/add-members', [GroupAPIController::class, 'addMembers']);
        Route::delete('groups/{group}/members/{user}', [GroupAPIController::class, 'removeMemberFromGroup']);
        Route::delete('groups/{group}/leave', [GroupAPIController::class, 'leaveGroup']);
        Route::delete('groups/{group}/remove', [GroupAPIController::class, 'removeGroup']);
        Route::put('groups/{group}/members/{user}/make-admin', [GroupAPIController::class, 'makeAdmin']);
        Route::put('groups/{group}/members/{user}/dismiss-as-admin', [GroupAPIController::class, 'dismissAsAdmin']);
        Route::get('users-blocked-by-me', [BlockUserAPIController::class, 'blockUsersByMe']);

        Route::get('notification/{notification}/read', [NotificationController::class, 'readNotification']);
        Route::get('notification/read-all', [NotificationController::class, 'readAllNotification']);

        /** Web Notifications */
        Route::put('update-web-notifications', [UserAPIController::class, 'updateNotification']);
        Route::put('update-player-id', [UserAPIController::class, 'updatePlayerId']);
        //set user custom status route
        Route::post('set-user-status', [UserAPIController::class, 'setUserCustomStatus'])->name('set-user-status');
        Route::get('clear-user-status', [UserAPIController::class, 'clearUserCustomStatus'])->name('clear-user-status');

        //report user
        Route::post('report-user', [ReportUserController::class, 'store'])->name('report-user.store');
    });
});

// users
Route::group(['middleware' => ['permission:manage_users', 'auth', 'user.activated']], function () {
    Route::resource('users', 'UserController');
    Route::post('users/{user}/active-de-active', [UserController::class, 'activeDeActiveUser'])
        ->name('active-de-active-user');
    Route::post('users/{user}/update', [UserController::class, 'update']);
    Route::delete('users/{user}/archive', [UserController::class, 'archiveUser']);
    Route::post('users/restore', [UserController::class, 'restoreUser']);
});

// roles
Route::group(['middleware' => ['permission:manage_roles', 'auth', 'user.activated']], function () {
    Route::resource('roles', 'RoleController');
    Route::post('roles/{role}/update', [RoleController::class, 'update'])->name('roles.update');
});

// settings
Route::group(['middleware' => ['permission:manage_settings', 'auth', 'user.activated']], function () {
    Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
    Route::post('settings', [SettingsController::class, 'update'])->name('settings.update');
});

// reported-users
Route::group(['middleware' => ['permission:manage_reported_users', 'auth', 'user.activated']], function () {
    Route::resource('reported-users', 'ReportUserController');
});

// meetings
Route::group(['middleware' => ['permission:manage_meetings', 'auth', 'user.activated']], function () {
    Route::resource('meetings', 'MeetingController');
    Route::get('meetings/{meeting}/change-status', [MeetingController::class, 'changeMeetingStatus']);
});

Route::group(['middleware' => ['permission:manage_meetings', 'auth', 'user.activated']], function () {
    Route::get('member/meetings', [MeetingController::class, 'showMemberMeetings'])->name('meetings.member_index');
});

Route::group(['middleware' => ['web']], function () {

    Route::get('login/{provider}', [Auth\SocialAuthController::class, 'redirect']);
    Route::get('login/{provider}/callback', [Auth\SocialAuthController::class, 'callback']);
});

/* End of network-chat routes */

Route::get('/checkbox/{check}', [App\Http\Controllers\Front\HomeController::class, 'checkbox'])->name("checkbox.submit");
Route::get('/survey', [App\Http\Controllers\Front\UserController::class, 'submitSurvey'])->name("survey");
Route::post('/survey', [App\Http\Controllers\Front\UserController::class, 'saveSurvey'])->name("surveySubmit");
Route::get('/requestactivate/{token}', [App\Http\Controllers\Front\UserController::class, 'requestActivate'])->name("requestactivate");

Route::get('storage/conversation/{filename}', function ($filename)
{
    $path = storage_path('app/conversation/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/refresh-csrf', function(){
    return csrf_token();
});

/*
* Additional features
* Date 12-5-2022
*/
Route::post('user/disclosure/submit', [App\Http\Controllers\Front\UserController::class, 'submitDisclosure'])->name('user.disclosure.submit');
Route::get('user/disclosure', [App\Http\Controllers\Front\UserController::class, 'disclosure'])->name('user.disclosure');
Route::post('user/introduction/submit', [\App\Http\Controllers\Front\UserController::class, 'submitIntroduction'])->name('user.introduction.submit');
Route::get('user/introduction', [\App\Http\Controllers\Front\UserController::class, 'introduction'])->name('user.introduction');

Route::get('monthlysurvey', [\App\Http\Controllers\Front\UserController::class, 'monthly_survey']);

Route::get('landing', [HomeController::class, 'landing'])->name('landing');
Route::get('subscriptionalert',[\App\Http\Controllers\Front\UserController::class, 'subscriptionAlert']);

Route::get('daily-subscription-expire', [App\Http\Controllers\Front\SubscriptionPlanController::class, 'dailySubscriptionExpire']);


Route::get('/test', function () {
    Artisan::call('sync:financial-statements');
    //
});

Route::get('/sec-sync-company', function () {
    Artisan::call('sync:companies');
});
Route::get('/fm-sync-company', function () {
    Artisan::call('sync:cik-names');
});

Route::get('/new-company-api', function () {
    Artisan::call('sync:newcompanies');
});

Route::get('/new-company-desc', function () {
    Artisan::call('sync:newcompaniesdescription');
});

Route::get('/new-Eodhistoricaldata', function () {
    Artisan::call('sync:Eodhistoricaldata');
});

Route::get('/company-keywords', function () {
    Artisan::call('sync:cik-keywords');
});
Route::get('/sec-synccompany', function () {
    Artisan::call('sync:company');
});
Route::get('/get-keywords', function () {
    Artisan::call('sync:keywords');
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
Route::get('/cron-financial-statements', function () {
    Artisan::call('sync:financial-statements');
});

/*
* 
* Date 15-12-2022
*/
Route::post('/cancellation-survey', [App\Http\Controllers\Front\UserController::class, 'saveCancellationSurvey'])->name("CancellationSurveySubmit");