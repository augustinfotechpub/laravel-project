<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ChatAPIController;
use App\Http\Controllers\API\UserAPIController;

// login & logout route
Route::get('/', [\App\Http\Controllers\Admin\Auth\LoginController::class, 'index']);
Route::get('login', [\App\Http\Controllers\Admin\Auth\LoginController::class, 'index'])->name('admin.login');
Route::post('login/authenticate', [\App\Http\Controllers\Admin\Auth\LoginController::class, 'authenticate'])->name('admin.login.authenticate');
Route::get('logout', [\App\Http\Controllers\Admin\Auth\LoginController::class, 'logout'])->name('admin.logout');

// Forgot Password Route
Route::get('forgot-password/create', [\App\Http\Controllers\Admin\Auth\ForgotPasswordController::class, 'showForgotPasswordForm'])->name('admin.forgot_password.create');
Route::post('forgot-password/email/send', [\App\Http\Controllers\Admin\Auth\ForgotPasswordController::class, 'sendPasswordResetLink'])->name('admin.forgot_password.send');
Route::get('password/reset/{token}', [\App\Http\Controllers\Admin\Auth\ForgotPasswordController::class, 'resetPasswordForm'])->name('admin.password_reset.create');
Route::post('password/reset', [\App\Http\Controllers\Admin\Auth\ForgotPasswordController::class, 'passwordReset'])->name('admin.password_reset.store');


Route::middleware('admin')->group(function () {
    // Dashboard Route
    Route::get('dashboard', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('sync', [\App\Http\Controllers\Admin\UserController::class, 'syncIndex'])->name('admin.sync.index');
    Route::post('sync', [\App\Http\Controllers\Admin\UserController::class, 'syncSubmit'])->name('admin.sync.submit');

    // Chat
    Route::get('conversations-list-user', [ChatAPIController::class, 'getLatestConversationsForUesr']);
    Route::get('/conversations', [ChatController::class, 'index'])->name('adminConversations');
    Route::get('users/{id}/conversation', [UserAPIController::class,'getConversationForAdmin']);

    //User Routes
    Route::get('survey', [\App\Http\Controllers\Admin\DefinitionController::class, 'survey'])->name('admin.survey.list');
    Route::get('survey/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'surveyShow'])->name('admin.survey.show');

    Route::get('users', [\App\Http\Controllers\Admin\UserController::class, 'index'])->name('admin.users.list');
    Route::get('user/show/{id}', [\App\Http\Controllers\Admin\UserController::class, 'show'])->name('admin.user.show');
    Route::get('user/chat', [\App\Http\Controllers\Admin\UserController::class, 'showchat'])->name('admin.user.showchat');
    Route::delete('user/delete/{id}', [\App\Http\Controllers\Admin\UserController::class, 'destroy'])->name('admin.user.delete');


    // Site Configuration
    Route::get('configurations', [\App\Http\Controllers\Admin\SiteConfigurationController::class, 'index'])->name('admin.configurations');
    Route::post('configuration/update', [\App\Http\Controllers\Admin\SiteConfigurationController::class, 'update'])->name('admin.configuration.update');
    Route::post('configuration/remove/video', [\App\Http\Controllers\Admin\SiteConfigurationController::class, 'remove_video'])->name('admin.configuration.remove_video');

    // Subscription plans route
    Route::get('subscriptions', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'index'])->name('admin.subscriptions_plan.list');
    Route::get('subscription/create', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'create'])->name('admin.subscription_plan.create');
    Route::post('subscription/store', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'store'])->name('admin.subscription_plan.store');
    Route::get('subscription/edit/{id}', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'edit'])->name('admin.subscription_plan.edit');
    Route::post('subscription/update', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'update'])->name('admin.subscription_plan.update');
    Route::get('subscription/show/{id}', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'show'])->name('admin.subscription_plan.show');
    Route::delete('subscription/delete/{id}', [\App\Http\Controllers\Admin\SubscriptionPlanController::class, 'delete'])->name('admin.subscription_plan.delete');


    // Subscription routes
    Route::get('customer/subscriptions', [\App\Http\Controllers\Admin\CustomerSubscriptionController::class, 'index'])->name('admin.subscriptions.list');
    Route::get('customer/subscription/show/{id}', [\App\Http\Controllers\Admin\CustomerSubscriptionController::class, 'show'])->name('admin.subscription.show');
    Route::delete('customer/subscription/delete/{id}', [\App\Http\Controllers\Admin\CustomerSubscriptionController::class, 'delete'])->name('admin.subscription.delete');

    //Email Templates Routes
    Route::get('email/templates', [\App\Http\Controllers\Admin\EmailTemplateController::class, 'index'])->name('admin.email_templates.list');
    Route::get('email/template/create', [\App\Http\Controllers\Admin\EmailTemplateController::class, 'create'])->name('admin.email_template.create');
    Route::post('email/template/store', [\App\Http\Controllers\Admin\EmailTemplateController::class, 'store'])->name('admin.email_template.store');
    Route::get('email/template/edit/{id}', [\App\Http\Controllers\Admin\EmailTemplateController::class, 'edit'])->name('admin.email_template.edit');
    Route::post('email/template/update', [\App\Http\Controllers\Admin\EmailTemplateController::class, 'update'])->name('admin.email_template.update');
    Route::delete('email/template/delete/{id}', [\App\Http\Controllers\Admin\EmailTemplateController::class, 'delete'])->name('admin.email_template.delete');


    //Contact Request
    Route::get('contact-requests', [\App\Http\Controllers\Admin\ContactRequestController::class, 'index'])->name('admin.contact_requests.list');
    Route::get('contact-request/show/{id}', [\App\Http\Controllers\Admin\ContactRequestController::class, 'show'])->name('admin.contact_request.show');
    Route::delete('contact-request/delete/{id}', [\App\Http\Controllers\Admin\ContactRequestController::class, 'destroy'])->name('admin.contact_request.delete');

    //Key Definition
    Route::get('alerts', [\App\Http\Controllers\Admin\DefinitionController::class, 'index'])->name('admin.alerts.list');

    Route::post('alerts/import', [\App\Http\Controllers\Admin\DefinitionController::class, 'importAlerts'])->name('admin.alerts.import');

    Route::get('alert/create', [\App\Http\Controllers\Admin\DefinitionController::class, 'create'])->name('admin.alert.create');
    Route::post('alert/store', [\App\Http\Controllers\Admin\DefinitionController::class, 'store'])->name('admin.alert.store');
    Route::get('alert/edit/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'edit'])->name('admin.alert.edit');
    Route::post('alert/update/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'update'])->name('admin.alert.update');
    Route::delete('alert/delete/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'delete'])->name('admin.alert.delete');
    Route::get('alert/show/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'show'])->name('admin.alert.show');

    Route::get('alert/tag/delete/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'deleteTag'])->name('tag.delete');
    Route::get('alert/sic/delete/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'deleteSic'])->name('sic.delete');
    Route::get('alert/symbol/delete/{id}', [\App\Http\Controllers\Admin\DefinitionController::class, 'deleteSymbol'])->name('symbol.delete');


    //Key Definition Mapping
    Route::get('key-alert-mapping',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'index'])->name('admin.key_alert_mapping.list');
    Route::get('key-alert-mapping/create',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'create'])->name('admin.key_alert_mapping.create');
    Route::post('key-alert-mapping/store',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'store'])->name('admin.key_alert_mapping.store');
    Route::get('key-alert-mapping/show/{id}',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'show'])->name('admin.key_alert_mapping.show');
    Route::get('key-alert-mapping/edit/{id}',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'edit'])->name('admin.key_alert_mapping.edit');
    Route::post('key-alert-mapping/update/{id}',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'update'])->name('admin.key_alert_mapping.update');
    Route::delete('key-alert-mapping/delete/{id}',[\App\Http\Controllers\Admin\KeyDefinitionMappingController::class,'delete'])->name('admin.key_alert_mapping.delete');

    Route::resource('companies', 'CompanyController', ['as' => 'admin']);

    Route::get('newsletter', [\App\Http\Controllers\Admin\UserController::class, 'newsLetter'])->name('admin.newsletter');
    Route::post('newsletterEmail', [\App\Http\Controllers\Admin\UserController::class, 'newsLetterEmail'])->name('admin.newsletteremail');
    Route::post('newsletterEmailAll', [\App\Http\Controllers\Admin\UserController::class, 'newsLetterEmailAll']);
    Route::post('newsletterEmailUpdate', [\App\Http\Controllers\Admin\UserController::class, 'newsletterEmailUpdate']);

    Route::get('cancel-survey-feedback', [\App\Http\Controllers\Admin\UserController::class, 'cancelSubscriptionFeedback'])->name('admin.cancel-survey-feedback');

});
